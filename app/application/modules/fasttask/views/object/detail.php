<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("fasttask/object/edit/$id");
    } else {
        $form_link = create_url("fasttask/object/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>

<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_object_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_object');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>

    $(function(){
        var w = $('#modal_object_detail').data('kendoWindow');
        w.setOptions({width: '950px'});
        w.center();
        $('.select2').select2({ width: 'resolve' });
        $('#frm_object_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_object_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });

        $('.pos-list').on('click', '.btn-remove-position', function(e){
            e.preventDefault();
            if (!confirm('<?php echo lang('object_msg_delete_position'); ?>')) return false;
            var i = $(this);
            i.parents('tr').first().remove();
        });

        $('.btn-add-post').click(function(e){
            e.preventDefault();
            var lat = $.trim($('#input-add-lat').val());
            var lon = $.trim($('#input-add-lon').val());
            if ((lat.length == 0) || (lon.length == 0)) {
                return false;
            }

            var html = '<tr>';
            html += '<td>';
            html += '<input type="text" name="lat[]" value="' + lat + '" class="form-control pos-input" placeholder="Latitude ...">';
            html += '</td>';
            html += '<td>';
            html += '<input type="text" name="lon[]" value="' + lon + '" class="form-control pos-input" placeholder="Longitude ...">';
            html += '</td>';
            html += '<td width="60" class="text-center"><a href="javascript: void(0);" class="btn btn-sm btn-danger btn-remove-position" loading="0"><i class="glyphicon glyphicon-remove"></i> Remove</a></td>';
            html += '</tr>';
            $('.pos-list .table tbody').prepend(html);
        });
    })
</script>

<style type="text/css">
.pos-add-control {margin-bottom: 5px;}
.btn-add-post {width: 102px; padding: 4px 5px;}
.pos-list {}
.pos-list .table td {border: none;}
.pos-list .pos-input { border: none; box-shadow: none; border-bottom: 1px dashed #EAEAEA; padding: 0px; height: 25px;}
.input-add {float: left; width: 250px; margin-right: 5px;}
</style>

<div id="frm_object_detail_wraper">
    <?php echo form_open($form_link,array('class'=>'form-horizontal','id'=>'frm_object_detail'));?>
    <?php if (isset($msg)) : ?>
    <div class="alert alert-success"><?php echo $msg; ?></div>
    <?php endif; ?>

    <div class="form-group">
        <label class="col-md-3 control-label" for="name"><?php echo lang('object_lbl_object_name');?><span class="star">&nbsp;*</span></label>
        <div class="col-md-8">
            <?php echo form_input(array('id'=>'name','name'=>'name','value'=>set_value('name', isset($data->name) ? $data->name : ''),'type'=>'text','placeholder'=>lang("object_lbl_object_name"),'class'=>'form-control input-md'));?>
            <div class="help-block ps_err">
                <?php echo form_error('name'); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label" for="name"><?php echo lang('object_lbl_group_name');?></label>
        <div class="col-md-8">
                <select name="group" class="select2" style="width: 612px;">
                    <option value=""><?php echo lang('object_lbl_group_place')?></option>
                        <?php if (isset($object_groups) && !empty($object_groups)) : ?>
                            <?php foreach ($object_groups as $key => $value) : ?>
                                <option value="<?php echo $value->id; ?>" <?php echo set_select('group', $value->id, (isset($data->id_group) AND ($value->id==$data->id_group))); ?> ><?php echo $value->name; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                </select>
            <div class="help-block ps_err">
                <?php echo form_error('group'); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label" for="position"><?php echo lang('object_lbl_position');?><span class="star">&nbsp;*</span></label>
        <div class="col-md-8">
            <?php echo form_input(array('id'=>'position','name'=>'position','value'=>set_value('position', isset($data->position) ? $data->position : ''),'type'=>'hidden','placeholder'=>lang("object_lbl_position"),'class'=>'form-control input-md'));?>
            <?php
            $data_position = isset($data->position) ? $data->position : false;
            if (isset($position)) {
                $data_position = $position;
            }

            if ($data_position) {
                $data_position = str_replace(['LINESTRING(', ')'], ['', ''], $data_position);
                $data_position = explode(',', $data_position);
            }
            ?>
            <div class="pos-add-control">
                <input type="text" class="form-control input-add" id="input-add-lat" placeholder="Latitude ..." />
                <input type="text" class="form-control input-add" id="input-add-lon" placeholder="Longitude ..." />
                <a class="btn btn-md btn-primary btn-add-post" loading="0"><?php echo lang('btn_add_new'); ?></a>
                <div class="clearfix"></div>
            </div>
            <div class="pos-list" style="max-height: 400px; overflow: auto;">
                <table class="table table-hover table-condensed table-responsive table-bordered2">
                    <tbody>
                        <?php if (is_array($data_position) AND !empty($data_position)) : ?>
                        <?php foreach ($data_position as $key => $value) :
                            $value = explode(' ', $value);
                            $value = array_map('trim', $value); ?>
                        <?php if ((count($value) == 2) AND ($value[0] != '') AND ($value[1] != '') ) ?>
                        <tr>
                            <td><?php echo form_input('lat[]', (isset($value[0]) ? $value[0] : ''), 'class="form-control pos-input" placeholder="Latitude ..."' ); ?></td>
                            <td><?php echo form_input('lon[]', (isset($value[1]) ? $value[1] : ''), 'class="form-control pos-input" placeholder="Longitude ..."' ); ?></td>
                            <td width="80" class="text-center">
                                <a href="javascript: void(0);" class="btn btn-sm btn-danger btn-remove-position" loading="0"><i class="glyphicon glyphicon-remove"></i> <?php echo lang('btn_delete')?></a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="help-block ps_err">
                <?php echo form_error('lat'); ?>
                <?php echo form_error('lon'); ?>
                <?php echo form_error('position'); ?>
            </div>
        </div>
    </div>
    <hr />
    <div class="form-group">
        <div class="col-md-12 text-center">
            <button id="btn_save" name="btn_save" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
            <a id="btn_cancel" name="btn_cancel" href="javascript:close_modal('#modal_object_detail');" class="btn btn-default"><?php echo lang('btn_cancel');?></a>
        </div>
    </div>
    <?php echo form_close();?>
</div>