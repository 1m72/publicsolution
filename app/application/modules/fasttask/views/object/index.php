<script type="text/javascript">
    $(function() {
        init_modal('#modal_object_detail');
        init_modal('#modal_object_delete');

        var column_properties = $.parseJSON('<?php echo $column_properties; ?>');
        var grid_config = {
            'target': '#grid_object',
            'url': '<?php echo current_url(); ?>',
            'limit': <?php echo isset($limit) ? intval($limit) : 0; ?>,
            'columns': [
                { field: "option", title: '<?php echo lang('grid_number');?>',filterable: false, sortable: false, width: column_properties.no_width, template: function(data) {return grid_number = grid_number + 1;}},
                { field: "place_name", title: '<?php echo lang('object_grid_group'); ?>', width: column_properties.group_name_width, },
                { field: "obj_name", title: '<?php echo lang('object_grid_name')?>'},
                { field: "created_at", title: '<?php echo lang('grid_created_at'); ?>',type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", width: column_properties.created_at_width, },
                { field: "updated_at", title: '<?php echo lang('grid_updated_at'); ?>',type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", width: column_properties.updated_at_width, },
                { field: "option", title: 'Option', attributes:{style:"text-align:center"}, filterable: false, sortable: false, width: column_properties.option_width, template: function(data) {
                        html = "<a _modal='#modal_object_detail' _title='<?php echo lang('object_object_detail'); ?>' href='<?php echo create_url('fasttask/object/edit'); ?>/"+data.id+"' class='sys_modal_detail btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span>&nbsp;<?php echo lang('btn_edit'); ?></a>&nbsp;";
                        html = html + "<a _id_grid = '#grid_object' _modal='#modal_object_delete' _title='<?php echo lang('delete_confirm'); ?>' href='<?php echo create_url('fasttask/object/delete'); ?>/"+data.id+"' class='sys_modal_delete btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span>&nbsp;<?php echo lang('btn_delete'); ?></a>";
                        return html;
                    }
                }
            ]
        };
        create_grid(grid_config);
    });
</script>
<div id="grid_object"></div>
<script id="toolbar_template" type="text/x-kendo-template">
    <div>
        <span class="pull-left">
            <h4>
                &nbsp;<span class="glyphicon glyphicon-user"></span>
                &nbsp;<strong><?php echo lang('object_list_object');?></strong>
            </h4>
        </span>
        <span class="pull-right">
            <a _modal="\\#modal_object_detail" _title='<?php echo lang('object_object_detail'); ?>' href="<?php echo create_url('fasttask/object/add'); ?>" class="sys_modal_detail btn btn-primary btn-default"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new');?></a>
        </span>
    </div>
</script>
<script id="delete-confirmation" type="text/x-kendo-template">
    <div class="row">
        <span class="col-md-2"></span>
        <span class="col-md-8">
            <p class="delete-message"><?php echo lang('object_msg_delete');?></p>
        </span>
        <span class="col-md-2"></span>
    </div>
    <div class="row">
        <span class="col-md-7"></span>
        <span class="col-md-5">
            <span class="pull-right">
                <button class="delete-confirm btn btn-danger"><?php echo lang('btn_yes');?></button>
                <a href="#" class="delete-cancel btn btn-default"><?php echo lang('btn_no');?></a>
            </span>
        </span>
    </div>
</script>