<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("fasttask/superior/edit/$id");
    } else {
        $form_link = create_url("fasttask/superior/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_superior_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_superior');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>

    function company_refresh() {
        $.ajax({
           type: 'GET',
           url: '<?php echo create_url('fasttask/superior/list_company');?>',
           data: {id_company: $("#id_company").val()},
           datatype: 'html',
           cache:false,
           success: function(data){
                $('#select_company').html(data);
                $('#select_company .select2').select2();
           }
        });
    }

    function activity_refresh() {
        $.ajax({
            type: 'GET',
            url: '<?php echo create_url('fasttask/superior/list_activity');?>',
            data: {id_activity: $("#id_activity").val()},
            datatype: 'html',
            cache:false,
            success: function(data){
                $('#select_activity').html(data);
                $('#select_activity .select2').select2();
            }
        });
    }

    function change_activity(){
        $.ajax({
            type: 'GET',
            url: '<?php echo create_url('fasttask/superior/activitybyid');?>',
            data: {id_activity: $("#id_activity").val(), id_default: $("#id_default").val()},
            datatype: 'html',
            cache:false,
            success: function(data){
                $('#select_activity_default').html(data);
                $('#select_activity_default .select2').select2();
            }
        });
    }

    $(function(){
        var w = $('#modal_superior_detail').data('kendoWindow');
        w.setOptions({width: '900px'});
        w.center();

        $("#birthday").kendoDatePicker();
        $('.select2').select2({ width: 'resolve' });

        $('#frm_superior_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_superior_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });

        $('#modal_superior_detail').on('change', '.activity', function(e){
            i = $(this);
            target = $('.activity_default[value="'+i.val()+'"]');
            if (i.is(":checked")) {
                target.removeAttr('disabled');
            } else {
                target.attr('disabled', 'disabled');
            }
        });
    });
</script>

<div id="frm_superior_detail_wraper" width="800px;">
    <form class="form-horizontal" id="frm_superior_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <div class="row-fluid">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-5 control-label" for="first_name"><?php echo lang('lbl_first_name');?><span class="star">&nbsp;*</span></label>
                    <div class="col-md-7">
                        <input id="first_name" name="first_name" value="<?php echo set_value('first_name', isset($worker->first_name) ? $worker->first_name : ''); ?>" type="text" placeholder="<?php echo lang('holder_first_name');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('first_name'); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-5 control-label" for="last_name"><?php echo lang('lbl_last_name');?><span class="star">&nbsp;*</span></label>
                    <div class="col-md-7">
                        <input id="last_name" name="last_name" value="<?php echo set_value('last_name', isset($worker->last_name) ? $worker->last_name : ''); ?>" type="text" placeholder="<?php echo lang('holder_last_name');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('last_name'); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-5 control-label" for="phone"><?php echo lang('lbl_phone');?></label>
                    <div class="col-md-7">
                        <input id="phone" name="phone" value="<?php echo set_value('phone', isset($worker->phone) ? $worker->phone : ''); ?>" type="text" placeholder="<?php echo lang('holder_phone');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('phone'); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-5 control-label" for="address"><?php echo lang('lbl_address');?></label>
                    <div class="col-md-7">
                        <input id="address" name="address" value="<?php echo set_value('address', isset($worker->address) ? $worker->address : ''); ?>" type="text" placeholder="<?php echo lang('holder_address');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('address'); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-5 control-label" for="birthday"><?php echo lang('lbl_birthday');?></label>
                    <div class="col-md-7">
                        <input id="birthday" name="birthday" value="<?php echo set_value('birthday', isset($worker->birthday) ? $worker->birthday : ''); ?>"  style="width: 150px;">
                        <div class="help-block ps_err">
                            <?php echo form_error('birthday'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-4 control-label" for="personal_code"><?php echo lang('lbl_personal_code');?></label>
                    <div class="col-md-7">
                        <input id="personal_code" name="personal_code" value="<?php echo set_value('personal_code', isset($worker->personal_code) ? $worker->personal_code : ''); ?>" type="text" placeholder="<?php echo lang('holder_personal_code');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('personal_code'); ?>
                        </div>
                    </div>
                </div>

                <!-- NFC Code -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nfc-code"><?php echo lang('lbl_nfc_code');?></label>
                    <div class="col-md-7">
                        <select id="nfc_code" name="nfc_code" class="select2" style="width: 225px;">
                            <option value=""><?php echo lang('lbl_select_nfc_code');?></option>
                                <?php if (isset($nfc_code) AND !empty($nfc_code)) : ?>
                                <?php foreach ($nfc_code as $key => $value) : ?>
                                    <option value="<?php echo $value->nfc_code; ?>" <?php echo set_select('nfc_code', $value->nfc_code, (isset($worker->nfc_code) AND ($worker->nfc_code == $value->nfc_code) )); ?>><?php echo $value->code; ?></option>
                                <?php endforeach; ?>
                                <?php endif; ?>
                        </select>
                        <div class="help-block ps_err">
                            <?php echo form_error('nfc_code'); ?>
                        </div>
                    </div>
                </div>
                <!--// NFC Code -->

                <!-- Company -->
                <div class="form-group" style="height: 98px; ">
                    <label class="col-md-4 control-label" for="company"><?php echo lang('lbl_company');?><span class="star">&nbsp;*</span></label>
                    <div class="col-md-7" id="select_company" >
                        <select id="id_company" name="id_company" class="select2" style="width: 225px; height: 30px;">
                            <option value=""><?php echo lang('lbl_select_company');?></option>
                            <?php if (isset($company) AND !empty($company)) : ?>
                            <?php foreach ($company as $key => $value) : ?>
                            <option value="<?php echo $value->id; ?>" <?php echo set_select('id_company', $value->id, (isset($worker->id_company) AND ($worker->id_company == $value->id) )); ?>><?php echo $value->name; ?></option>
                            <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <span>
                            <a href="<?php echo create_url('sys/company/add') . '?callback=company_refresh'; ?>" class="sys_modal_detail btn btn-xs btn-info" _title="<?php echo lang('cpn_lbl_company_detail'); ?>" _modal='#modal_company_detail' target="_blank"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new')?></a>
                        </span>
                        <div class="help-block ps_err">
                            <?php echo form_error('id_company'); ?>
                        </div>
                    </div>
                </div>
                <!--// Company -->

                <!-- Price -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="company"><?php echo lang('superior_lbl_price');?></label>
                    <div class="col-md-7" id="price" >
                        <div class="input-group">
                            <span class="input-group-addon">&euro;</span>
                            <input id="price" name="price" value="<?php echo set_value('price', isset($worker->price) ? $worker->price : ''); ?>" type="text" placeholder="<?php echo lang('worker_holder_price');?>" class="form-control input-md">
                        </div>
                        <div class="help-block ps_err">
                            <?php echo form_error('price'); ?>
                        </div>
                    </div>
                </div>
                <!--// Price -->
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="row-fluid">
            <div class="col-md-6">
                <!-- Activity -->
                <div class="form-group">
                    <label class="col-md-5 control-label" for="id_default" style2="width: 21%;"><?php echo lang('crumb_activity');?><span class="star">&nbsp;*</span></label>
                    <div class="col-md-7" id="select_activity_default" style2="width: 79%;">
                        <?php if (isset($activities) AND !empty($activities)) : ?>
                        <?php if (0) : ?>
                        <div>
                            <span class="pull-left">&nbsp;</span>
                            <span class="pull-right">
                                <em> <?php echo lang('lbl_activity_default');?> </em>
                            </span>
                        </div>
                        <div class="clearfix"></div>
                        <?php endif; ?>

                        <div style="border: 1px solid #CCC; height: 180px; overflow: auto;padding: 6px 8px;">
                            <?php foreach ($activities as $key => $value) : ?>
                            <div>
                                <span class="pull-left">
                                    <?php $check = set_checkbox('activity', $value->id, ( isset($worker_activities) AND is_array($worker_activities) AND in_array($value->id, $worker_activities) ) ? true : false ); ?>
                                    <?php echo form_checkbox('activity[]', $value->id, $check, 'class="activity"'); ?>
                                    <?php echo isset($value->name) ? $value->name : ''; ?>
                                </span>

                                <?php if (0) : ?>
                                <span class="pull-right">
                                    <?php echo form_radio('activity_default', $value->id, set_select('activity_default', (isset($worker_activity_default) AND (in_array($value->id ,$worker_activity_default))) ? true : false ), 'class="activity_default" disabled="disabled"'); ?>
                                </span>
                                <?php endif; ?>
                            </div>
                            <div class="clearfix"></div>
                            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>
                        <div class="help-block ps_err">
                            <?php echo form_error('activity'); ?>
                        </div>
                    </div>
                </div>
                <!--// Activity -->
            </div>
            <div class="col-md-6">
                <!-- Module -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nfc-code" style2="width: 21%;"><?php echo lang('lbl_module');?></label>
                    <div class="col-md-7" style2="width: 79%;">
                        <?php if (0): ?>
                        <select id="module" name="module[]" multiple="multiple" class="select2" style="width: 100%;">
                            <?php if (isset($modules) AND !empty($modules)) : ?>
                            <?php foreach ($modules as $key => $value) : ?>
                                <option value="<?php echo $key; ?>" <?php echo set_select('module', $key, (isset($worker_modules) AND in_array($key, $worker_modules) )); ?>><?php echo $value; ?></option>
                            <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <?php endif; ?>

                        <ul style="border: 1px solid #CCC; height: 180px; overflow: auto;padding: 6px 8px;">
                            <?php if (isset($modules) AND !empty($modules)) : ?>
                            <?php foreach ($modules as $key => $value) : ?>
                                <?php $check = set_checkbox('module', $key, ( isset($worker_modules) AND is_array($worker_modules) AND in_array($key, $worker_modules) ) ? true : false ); ?>
                                <li><?php echo form_checkbox('module[]', $key, $check ); ?> <?php echo $value; ?></li>
                            <?php endforeach; ?>
                            <?php endif; ?>
                        </ul>

                        <div class="help-block ps_err">
                            <?php echo form_error('module'); ?>
                        </div>
                    </div>
                </div>
                <!--// Module -->
            </div>
        </div>
        <div class="clearfix"></div>
        <hr style="margin-top: 0px; " />
        <div class="col-md-12 text-center">
            <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
            <a class="btn btn-default" href="javascript:close_modal('#modal_superior_detail');"><?php echo lang('btn_cancel');?></a>
        </div>
    </form>
</div>