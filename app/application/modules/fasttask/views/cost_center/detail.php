<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("fasttask/cost_center/edit/$id");
    } else {
        $form_link = create_url("fasttask/cost_center/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_costcenter_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_costcenter');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>

    $(function(){
        $('#frm_costcenter_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_costcenter_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });

    });
</script>

<div id="frm_costcenter_detail_wraper">
    <form class="form-horizontal" id="frm_costcenter_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-4 control-label" for="title"><?php echo lang('lbl_title');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-5">
                <input id="title" name="title" value="<?php echo set_value('title', isset($data->title) ? $data->title : ''); ?>" type="text" placeholder="<?php echo lang('holder_title');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('title'); ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="btn_save"></label>
            <div class="col-md-8">
                <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a class="btn btn-default" href="javascript:close_modal('#modal_costcenter_detail');"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>
    </form>
</div>