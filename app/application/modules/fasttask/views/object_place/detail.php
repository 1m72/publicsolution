<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("fasttask/object_place/edit/$id");
    } else if(isset($level_text) AND ($level_text>0)){
        $form_link = create_url("fasttask/object_place/add/$id/$level_text");
    } else{
        $form_link = create_url("fasttask/object_place/add");
    }
?>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_ob_place_detail');
            refresh_treeview('#treeview-left');
        },
        timeout_dialog
    );
    <?php endif; ?>
    $(function(){
        var w = $('#modal_ob_place_detail').data('kendoWindow');
        w.setOptions({width: '900px'});
        w.center();

        $('#frm_ob_place_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_ob_place_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });
    });
</script>
<div id="frm_ob_place_detail_wraper">
    <form method="post" class="form-horizontal" id="frm_ob_place_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <?php if (isset($err)) : ?>
        <div class="alert alert-danger"><?php echo $err; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-3 control-label" for="title"><?php echo lang('ob_place_lbl_title');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-6">
                <input id="title" name="title" value="<?php echo set_value('title', isset($data->name) ? $data->name : ''); ?>" type="text" placeholder="<?php echo lang('ob_place_holder_name');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('title'); ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-6">
                <button id="btn_save" name="btn_save" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a id="btn_cancel" name="btn_cancel" href="javascript:close_modal('#modal_ob_place_detail');" class="btn btn-default"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>
    </form>
</div>