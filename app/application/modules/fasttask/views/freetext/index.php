<script type="text/javascript">
init_modal('#modal_freetxt_detail');
init_modal('#modal_freetxt_delete');
</script>
<div>
    <div class="freetext-section">
        <span class="pull-left add_root">
            <a class="sys_modal_detail btn btn-primary btn-default" href="<?php echo create_url('fasttask/freetext/add');?>" _title="Worker detail" _modal="#modal_freetxt_detail"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add new</a>
        </span>
        <div class="clearfix"></div>
        <div id="treeview-left"></div>
        <script id="treeview-template" type="text/kendo-ui-template">
            #: item.title #
              # if (item.level_text <3) {#
                <a _modal="\\#modal_freetxt_detail" _title='<?php echo lang('freetxt_lbl_detail'); ?>' href="<?php echo create_url('fasttask/freetext/add');?>/#= item.id #/#= item.level_text #" class='sys_modal_detail glyphicon glyphicon-plus color-add' ></a>
              # } #
                <a _modal="\\#modal_freetxt_detail" _title='<?php echo lang('freetxt_lbl_detail'); ?>' href="<?php echo create_url('fasttask/freetext/edit');?>/#= item.id #" class='sys_modal_detail glyphicon glyphicon-pencil color-edit' ></a>
              # if (item.level_text >1) { #
                <a _id_treeview="\\#treeview-left" _modal="\\#modal_freetxt_delete" _title='<?php echo lang('delete_confirm'); ?>' href="<?php echo create_url('fasttask/freetext/delete'); ?>/#= item.id #" _data_id="#= item.id #" class='treeview_modal_delete glyphicon glyphicon-remove color-delete'></a>
              # }#
        </script>
    </div>
    <script type="text/javascript">
        var dataSource = new kendo.data.HierarchicalDataSource({
          transport: {
            read: {
              url: "<?php echo create_url('fasttask/freetext/treeviewText');?>",
              dataType: "json"
            }
          },
          schema: {
            model: {
              id: "id",
              parent_id:"parent_id",
              level_text:"level_text",
              title:"title",
              created_at:"created_at",
              updated_at:"updated_at",
              deleted_at:"deleted_at",
              children:"items",
              expanded:"true"
            }
          }
        });
        $("#treeview-left").kendoTreeView({
          template: kendo.template($("#treeview-template").html()),
          dataSource: dataSource,
          dataTextField: "title",
          change : function(e) {
            $("#treeview-left").find("span.active-node").removeClass("active-node");
            $("#treeview-left").data("kendoTreeView").select().find("span.k-state-selected").removeClass("k-state-selected");//.addClass("active-node");
          }
        });
    </script>
</div>
<script id="delete-confirmation" type="text/x-kendo-template">
    <div class="row">
        <span class="col-md-2"></span>
        <span class="col-md-8">
            <p class="delete-message"><?php echo lang('freetxt_msg_question_delete');?></p>
        </span>
        <span class="col-md-2"></span>
    </div>
    <div class="row">
        <span class="col-md-7"></span>
        <span class="col-md-5">
            <span class="pull-right">
                <button class="delete-confirm btn btn-danger"><?php echo lang('btn_yes');?></button>
                <a href="#" class="delete-cancel btn btn-default"><?php echo lang('btn_no');?></a>
            </span>
        </span>
    </div>
</script>