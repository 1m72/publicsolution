<script type="text/javascript">
    $(function(){
        projekktor('video, audio', {
            playerFlashMP4: '<?php echo assets('js/projekktor/swf/Jarisplayer/jarisplayer.swf'); ?>',
            playerFlashMP3: '<?php echo assets('js/projekktor/swf/Jarisplayer/jarisplayer.swf'); ?>f'
        });
    });
</script>
<div style="padding: 10px;">
    <div class="tabbable tabs-left">
        <ul class="nav nav-tabs">
            <li class="<?php echo (isset($tab_select) AND ($tab_select == 'message')) ? 'active' : ''; ?>"><a href="#tab_message" data-toggle="tab"> <?php echo $this->lang->line('lbl_message')?></a></li>
            <li class="<?php echo (isset($tab_select) AND ($tab_select == 'image')) ? 'active' : ''; ?>"><a href="#tab_image" data-toggle="tab"><?php echo $this->lang->line('lbl_image')?></a></li>
            <li class="<?php echo (isset($tab_select) AND ($tab_select == 'voice')) ? 'active' : ''; ?>"><a href="#tab_voice" data-toggle="tab"><?php echo $this->lang->line('lbl_voice_meno')?></a></li>
            <li class="<?php echo (isset($tab_select) AND ($tab_select == 'video')) ? 'active' : ''; ?>"><a href="#tab_video" data-toggle="tab"><?php echo $this->lang->line('lbl_video')?></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane <?php echo (isset($tab_select) AND ($tab_select == 'message')) ? 'active' : ''; ?>" id="tab_message">
                <div class="row">
                    <div class="col-md-9">
                        <?php if (isset($message) AND !empty($message) AND is_array($message)) : ?>
                        <?php
                            foreach ($message as $key => $value) {
                                echo '<span class="label label-default">' . date('d.m.Y H:i', strtotime( get_value('time', $value) )) . '</span>&nbsp;';
                                echo get_value('data', $value);
                                echo (($key + 1) < count($message)) ? '<hr style="padding: 0px; margin: 10px; border: none;" />' : '';
                            }
                        ?>
                        <?php else: ?>
                        <div><?php echo $this->lang->line('lbl_no_data');?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane <?php echo (isset($tab_select) AND ($tab_select == 'voice')) ? 'active' : ''; ?>" id="tab_voice">
                <div class="row">
                    <div class="col-md-9">
                        <?php if(isset($voice) && !empty($voice)):?>
                        <a class="pull-left btn btn-primary btn-xs" target="_blank" href="<?php echo isset($voice[0]['data']) ? $voice[0]['data'] : '#'; ?>" style="margin-bottom: 7px;"><span class="glyphicon glyphicon-cloud-download"></span>&nbsp;Click here to download</a>

                        <?php if (isset($voice[0]['time'])) : ?>
                        <span class="pull-right label label-success" style="padding: 5px 10px; font-size: 12px; "><?php echo date('d.m.Y H:i', strtotime($voice[0]['time'])); ?></span>
                        <?php endif; ?>

                        <audio controls class="projekktor" width="920" height="150" poster="poster.jpg">
                            <?php foreach ($voice as $vi):?>
                            <source src="<?php echo $vi['data'];?>" type="audio/mp3" ></source>
                            <?php endforeach;?>
                        </audio>
                        <?php else:?>
                        <div><?php echo $this->lang->line('lbl_no_data');?></div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <div class="tab-pane <?php echo (isset($tab_select) AND ($tab_select == 'video')) ? 'active' : ''; ?>" id="tab_video">
                <div class="row">
                    <div class="col-md-9">
                        <?php if(isset($video) && !empty($video)):?>
                        <a class="pull-left btn btn-primary btn-xs" target="_blank" href="<?php echo isset($video[0]['data']) ? $video[0]['data'] : '#'; ?>" style="margin-bottom: 7px;"><span class="glyphicon glyphicon-cloud-download"></span>&nbsp;Click here to download</a>

                        <?php if (isset($video[0]['time'])) : ?>
                        <span class="pull-right label label-success" style="padding: 5px 10px; font-size: 12px; "><?php echo date('d.m.Y H:i', strtotime($video[0]['time'])); ?></span>
                        <?php endif; ?>

                        <video class="projekktor" width="920" height="400">
                            <?php foreach ($video as $vd):?>
                            <source src="<?php echo $vd['data'];?>" ></source>
                            <?php endforeach;?>
                        </video>
                        <?php else:?>
                        <div><?php echo $this->lang->line('lbl_no_data');?></div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <div class="tab-pane <?php echo (isset($tab_select) AND ($tab_select == 'image')) ? 'active' : ''; ?>" id="tab_image">
                <div class="row">
                    <?php if(isset($image) && !empty($image)) : ?>
                    <div class="col-sm-4" style="padding: 0px;height: 400px; overflow: auto;">
                        <?php $i = 0; foreach ($image as $item) : ?>
                        <div class="col-sm-4">
                            <img class="thumbnail" width="89" height="85" alt="" src="<?php echo get_value('thumbs', $item);?>" onclick="$('#imgview').attr('src',$(this).attr('src'));">
                        </div>
                        <?php $i++; endforeach; ?>
                    </div>
                    <div class="col-sm-5" style="width: 45%; padding: 0px;">
                        <?php if(isset($image) && !empty($image)) :?>
                        <img width="430" height="auto" style="border: 0px solid #ddd; border-radius: 0px; -webkit-transition: all .2s ease-in-out; transition: all .2s ease-in-out;" id="imgview" alt="" src="<?php echo $image[0]['thumbs'];?>">
                        <?php endif;?>
                    </div>
                    <?php else: ?>
                    <div class="col-md-9">
                        <div><?php echo $this->lang->line('lbl_no_data');?></div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>