<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("fasttask/employee_group/edit/$id");
    } else {
        $form_link = create_url("fasttask/employee_group/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_employee_group_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_employee_group');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>

    function cost_center_refresh() {
        $.ajax({
           type: 'GET',
           url: '<?php echo create_url('fasttask/employee_group/list_cost_center');?>',
           data: {id_cost_center: $("#id_cost_center").val()},
           datatype: 'html',
           cache:false,
           success: function(data){
                $('#select_costcenter').html(data);
                $('#select_costcenter .select2').select2();
           }
        });
    }

    $(function(){
        $('.select2').select2({ width: 'resolve' });

        $('#frm_employee_group_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_employee_group_detail').html(res);
                },
                error: function(err) {}
            });
        });

        $('#select_costcenter').keyup(function(e){
            e.preventDefault();
            var text_search_employee_gp = $('#select_costcenter').val();
            var listCheckboxEmployeeGp = $('#costcenter .checkbox');
            $.each(listCheckboxEmployeeGp,function(index){
                var item_employee_gp = listCheckboxEmployeeGp.eq(index);
                var text = item_employee_gp.text().trim();
                var check = false;
                if(typeof text!= 'undefined' && text!=null){
                    var re = new RegExp(text_search_employee_gp, "ig");
                    var check = text.match(re);
                    if(typeof check !='undefined' && check!=null && check !=''){
                        item_employee_gp.css({display:'block'});
                    }else{
                        item_employee_gp.css({display:'none'});
                    }
                }
            });
        });
    });
</script>
    <form class="form-horizontal" id="frm_employee_group_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <?php if (isset($err)) : ?>
        <div class="alert alert-danger"><?php echo $err; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-4 control-label" for="title"><?php echo lang('lbl_title');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-5">
                <input id="title" title="title" name="title" value="<?php echo set_value('title', isset($data->title) ? $data->title : ''); ?>" type="text" placeholder="<?php echo lang('activity_holder_title');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('title'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="cost_center"><?php echo lang('lbl_cost_center');?></label>
            <div class="col-md-5" id="costcenter">
                <div class="search">
                    <input id="select_costcenter" placeholder="<?php echo lang('employee_group_search_placeholder'); ?>" style="float: left; margin-right: 7px;" class="form-control input-md" value="">
                </div>
                <div style="height: 200px; overflow: auto; border: 1px solid #ECECEC; padding: 5px 10px; margin-top: 5px;" id="checkWorker">
                    <?php if(isset($cost_center) AND !empty($cost_center)): ?>
                        <?php foreach ($cost_center as $key => $value):?>
                            <div class="checkbox">
                                <input class="id_costcenter" type="checkbox" data-name="id_costcenter[]" name="id_costcenter[]" value="<?php echo $key; ?>" <?php echo set_checkbox('id_costcenter[]', $key); ?> <?php if(isset($data->id_cost_center) && in_array($key, $data->id_cost_center)) echo 'checked="checked"';?> id="id_costcenter_<?php echo $key; ?>"/>
                                <label for="id_costcenter_<?php echo $key ?>" class="lbl_worker"><?php echo $value;?></label>
                            </div>
                        <?php endforeach;?>
                    <?php endif; ?>
                </div>

                <div class="help-block ps_err">
                    <?php// echo form_error('id_costcenter'); ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <label class="col-md-4 control-label"></label>
            <div class="col-md-5"> <a href="<?php echo create_url('fasttask/cost_center/add') . '?callback=cost_center_refresh'; ?>" class="sys_modal_detail btn btn-xs btn-info" _title="<?php echo lang('lbl_costcenter_detail'); ?>" _modal='#modal_costcenter_detail' target="_blank"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new')?></a> </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="btn_save"></label>
            <div class="col-md-8">
                <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a class="btn btn-default" href="javascript:close_modal('#modal_employee_group_detail');"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>
    </form>
</div>