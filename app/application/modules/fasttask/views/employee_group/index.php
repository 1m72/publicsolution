<script type="text/javascript">
    var current_url    = '<?php echo current_url(); ?>';
    var current_state  = <?php echo (intval($this->input->get('module')) > 0) ? 1 : 0; ?>;
    var module         = '?module=<?php echo intval($this->input->get('module')); ?>';
    var module_not     = '?module_not=<?php echo intval($this->input->get('module')); ?>';
    var grid_url       = current_url + module;
    $(function() {
        init_modal('#modal_employee_group_detail');
        init_modal('#modal_employee_group_delete');
        init_modal('#modal_costcenter_detail');

        var column_properties = $.parseJSON('<?php echo $column_properties; ?>');
        var grid_config = {
            'target': '#grid_employee_group',
            'url': grid_url,
            'limit': <?php echo isset($limit) ? intval($limit) : 0; ?>,
            'columns': [
                { field: "order", title: '<?php echo lang('grid_number');?>', filterable: false, sortable: false, width: column_properties.order_width, template: function(order){return grid_number = grid_number + 1;} },
                { field: "title", title: '<?php echo lang('lbl_title'); ?>'},
                { field: "created_at", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", title: '<?php echo lang('grid_created_at')?>', width: column_properties.created_width },
                { field: "updated_at", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", title: '<?php echo lang('grid_updated_at')?>', width: column_properties.updated_width },
                { field: "option", title: '<?php echo lang('grid_option')?>', filterable: false, sortable: false, width: column_properties.option_width, attributes:{style: "text-align:center"},template: function(data) {
                            html = "<a _modal='#modal_employee_group_detail' _title='<?php echo lang('lbl_employee_group_detail'); ?>' href='<?php echo create_url('fasttask/employee_group/edit'); ?>/"+data.id+"' class='sys_modal_detail btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span>&nbsp;<?php echo lang('btn_edit')?></a>&nbsp;";
                            html = html + "<a _id_grid= '#grid_employee_group' _modal='#modal_employee_group_delete' _title='<?php echo lang('delete_confirm'); ?>' href='<?php echo create_url('fasttask/employee_group/delete'); ?>/"+data.id+"' class='sys_modal_delete btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span>&nbsp;<?php echo lang('btn_delete')?></a>";
                            return html;
                    }
                }
            ]
        };
        create_grid(grid_config);
    });
</script>

<div id="grid_employee_group"></div>
<script id="toolbar_template" type="text/x-kendo-template">
    <div>
        <span class="pull-left">
            <h4>
                &nbsp;<span class="glyphicon glyphicon-user"></span>
                &nbsp;<strong><?php echo lang('lbl_list_employee_group');?></strong>
            </h4>
        </span>
        <span class="pull-right">
            <a _modal="\\#modal_employee_group_detail" _title='<?php echo lang('lbl_employee_group_detail'); ?>' href="<?php echo create_url('fasttask/employee_group/add'); ?>" class="sys_modal_detail btn btn-primary btn-default"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new');?></a>
        </span>
    </div>
</script>

<script id="delete-confirmation" type="text/x-kendo-template">
    <div class="row">
        <span class="col-md-2"></span>
        <span class="col-md-8">
            <p class="delete-message"><?php echo lang('msg_question_delete');?></p>
        </span>
        <span class="col-md-2"></span>
    </div>
    <div class="row">
        <span class="col-md-7"></span>
        <span class="col-md-5">
            <span class="pull-right">
                <button class="delete-confirm btn btn-danger"><?php echo lang('btn_yes');?></button>
                <a href="#" class="delete-cancel btn btn-default"><?php echo lang('btn_no');?></a>
            </span>
        </span>
    </div>
</script>