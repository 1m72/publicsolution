<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Object Controller
 * @author Le Thi Nhung <le.nhung@kloon.vn>
 * @created 2013-11-21
 */

class Object extends MY_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();

        // Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        // Language
        $this->load->language('object');

        // Breakcrumbs
        $this->breadcrumb->append_crumb(lang('crumb_object'), create_url('sys/object'));

        // id_city
        $this->_id_city = current_city();

        // limit of grid
        $this->_limit   = config_item('grid_limit');
    }

    function index() {
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_OBJECT);
            return;
        }
        // Grid columns width
        if (current_language(FALSE) == 'de') {
            $column_properties = array(
            'no_width'         => 50,
            'id_width'         => 40,
            'group_name_width' => 200,
            'updated_at_width' => 120,
            'created_at_width' => 100,
            'option_width'     => 200,
            );
        } else {
            $column_properties = array(
            'no_width'         => 50,
            'id_width'         => 40,
            'group_name_width' => 200,
            'updated_at_width' => 100,
            'created_at_width' => 100,
            'option_width'     => 140,
            );
        }
        $this->data['column_properties'] = json_encode($column_properties);
        // View
        $this->load_assets('grid_view.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        $this->data['limit'] = $this->_limit;
        $this->view          = 'object/index';
        $this->layout        = 'layouts/module';
    }

    function add() {
        $this->load->helper('form');
        $this->_update();
    }

    function edit($id = 0) {
        $this->load->helper('form');
        $this->_edit_flag = true;
        $this->_update($id);
    }

    function _update($id = NULL) {
        $this->load_assets('select2/select2.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('select2/select2.min.js', ASSET_JS);
        if (!$this->input->is_ajax_request()) {
            redirect(create_url('sys/object'));
        }

        // Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }
        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            // Post data
            $id_group = $this->input->post('group');
            $name     = $this->input->post('name');
            $lat      = $this->input->post('lat');
            $lon      = $this->input->post('lon');

            // Validation
            $this->form_validation->set_rules('name', lang('object_lbl_object_name'), 'required');
           if ($this->form_validation->run() === TRUE) {
                $position = 'LINESTRING(';

                // Position
                if (is_array($lat) AND !empty($lat)) {
                    foreach ($lat as $key => $value) {
                        if (is_numeric($value) AND isset($lon[$key]) AND is_numeric($lon[$key]) ) {
                            $position .= "{$value} {$lon[$key]},";
                        }
                    }
                }
                $position = trim($position, ',');
                $position .= ')';
                $this->data['position'] = $position;

                $api_param   = array(
                    'id'       => $id,
                    'id_city'  => $this->_id_city,
                    'id_group' => $id_group,
                    'position' => $position,
                    'name'     => $name,
                );
                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_results = $this->rest->put('web/object?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_results = $this->rest->post('web/object?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status  = $this->rest->status();
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = $this->lang->line('object_msg_save_successful');
                    $flag = TRUE;
                }
            }
        }

        // Data
        if ($this->_edit_flag) {
            $api_param = array(
                'id_object' => $id,
                'id_city'   => $this->_id_city
            );
            $api_results = $this->rest->get('web/object', $api_param, 'json');
            $api_status  = $this->rest->status();
            if ( isset($api_results->data) AND !empty($api_results->data) ) {
                $data               = $api_results->data;
                $this->data['data'] = $data[0];
            }
        }

        // object group
        $this->data['object_groups']   = $this->_get_object_groups();
        $this->data['edit_flag'] = $this->_edit_flag;
        $this->data['flag']      = $flag;
        $this->layout = false;
        $this->view   = 'object/detail';
    }

    function delete($id = 0) {
        $return = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 0) {
                $api_param = array(
                    'id' => $id
                );
                $api_results = $this->rest->delete('web/object?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    = $this->lang->line('object_msg_delete_successful');
                } else {
                    $return['status'] = false;
                    $return['msg']    = $this->lang->line('err_has_an_error_when_update_database');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }

    // citys
    function _get_citys() {
        $citys      = array();
        $api_param  = array();
        $api_result_citys = $this->rest->get('web/city/indexall');
        $api_status_citys = $this->rest->status();
        if ( isset($api_result_citys->status) AND ($api_result_citys->status == REST_STATUS_SUCCESS) AND (isset($api_result_citys->data)) AND (!empty($api_result_citys->data)) ) {
            foreach ($api_result_citys->data as $key => $value) {
                $citys[$value->id] = $value;
            }
        }
        return $citys;
    }

    // object group
    function _get_object_groups(){
        $object_groups    = array();
        $api_param  = array();
        $api_result = $this->rest->get('web/object_group/index?id_city='.$this->_id_city);
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $object_groups[$value->id] = $value;
            }
        }
        return $object_groups;
    }

    function _get_refs() {
        return array(
            1 => array('id' => 1, 'name' => 'Ref 1', 'updated_at' => date('Y-m-d H:i:s'), 'created_at' => date('Y-m-d H:i:s')),
            2 => array('id' => 2, 'name' => 'Ref 2', 'updated_at' => date('Y-m-d H:i:s'), 'created_at' => date('Y-m-d H:i:s')),
            3 => array('id' => 3, 'name' => 'Ref 3', 'updated_at' => date('Y-m-d H:i:s'), 'created_at' => date('Y-m-d H:i:s')),
            4 => array('id' => 4, 'name' => 'Ref 4', 'updated_at' => date('Y-m-d H:i:s'), 'created_at' => date('Y-m-d H:i:s')),
            5 => array('id' => 5, 'name' => 'Ref 5', 'updated_at' => date('Y-m-d H:i:s'), 'created_at' => date('Y-m-d H:i:s')),
            6 => array('id' => 6, 'name' => 'Ref 6', 'updated_at' => date('Y-m-d H:i:s'), 'created_at' => date('Y-m-d H:i:s')),
            7 => array('id' => 7, 'name' => 'Ref 7', 'updated_at' => date('Y-m-d H:i:s'), 'created_at' => date('Y-m-d H:i:s')),
            8 => array('id' => 8, 'name' => 'Ref 8', 'updated_at' => date('Y-m-d H:i:s'), 'created_at' => date('Y-m-d H:i:s')),
        );
    }
}
