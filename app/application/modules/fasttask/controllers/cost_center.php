<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author le.nhung@kloon.vn
 * @copyright 2015
 */
class Cost_center extends MY_Controller {
    private $_id_city   = 1;
    private $_limit     = 10;
    private $_offset    = 0;
    private $_edit_flag = false;

    function __construct(){
        parent:: __construct();

        // Language
        $this->load->language('cost_center');

        // Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        // Breakcrumbs
        $this->breadcrumb->append_crumb(lang('lbl_costcenter'), create_url('fasttask/cost_center'));

        // id_city
        $this->_id_city = current_city();

        // limit of grid
        $this->_limit   = config_item('grid_limit');
    }


    function index(){
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_COST_CENTER);
            return;
        }

        // Grid columns width
        if (current_language(FALSE) == 'de') {
            $column_properties = array(
                'order_width'   => 30,
                'id_width'      => 50,
                'title_width'   => 120,
                'updated_width' => 120,
                'created_width' => 100,
                'option_width'  => 200,
            );
        } else {
            $column_properties = array(
                'order_width'   => 30,
                'id_width'      => 50,
                'title_width'   => 120,
                'updated_width' => 100,
                'created_width' => 100,
                'option_width'  => 140,
            );
        }
        $this->data['column_properties'] = json_encode($column_properties);

        // View
        $this->load_assets('grid_view.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        $this->data['limit'] = $this->_limit;
        $this->view          = 'cost_center/index';
        $this->layout        = 'layouts/module';
    }

    function add() {
        $this->_update();
    }

    function edit($id = 0) {
        $this->_edit_flag = true;
        $this->_update($id);
    }
    function _update($id = NULL) {

        if (!$this->input->is_ajax_request()) {
            redirect(create_url('fasttask/cost_center'));
        }

        // Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
                if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }

        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Post data
            $title       = trim($this->input->post('title'));
            // Validation
            $this->form_validation->CI =& $this;
            $this->form_validation->set_rules('title', lang('lbl_title'), 'required');

            if ($this->form_validation->run() === TRUE) {
                $api_param   = array(
                    'id'    =>  $id,
                    'title' => $title,
                );

                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_result      = $this->rest->put('web/v1/cost_center/index?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_result = $this->rest->post('web/v1/cost_center/index?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status = $this->rest->status();
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = lang('msg_costcenter_saved');
                    $flag = TRUE;
                } else {
                    $this->data['err'] = lang('err_api_save');
                }
            } else {
                $this->data['err'] = validation_errors();
            }
        }

        // Data
        if ($this->_edit_flag) {
            $api_param = array(
                'id'      => $id,
                'id_city' => $this->_id_city,
            );
            $api_result = $this->rest->get('web/v1/cost_center/index', $api_param, 'json');
            $api_status  = $this->rest->status();
            if ( isset($api_result->data) AND !empty($api_result->data) ) {
                $data               = $api_result->data;
                $this->data['data'] = $data[0];
            }
        }

        // View
        $this->data['edit_flag'] = $this->_edit_flag;
        $this->data['flag']      = $flag;
        $this->layout            = false;
        $this->view              = 'cost_center/detail';
    }

    function delete($id = 0) {
        $return = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 0) {
                $api_param = array(
                    'id' => $id
                );
                $api_result = $this->rest->delete('web/v1/cost_center?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    =  lang('msg_costcenter_has_been_deleted');
                } else {
                    $return['status'] = false;
                    $return['msg']    = lang('err_has_an_error_when_update_database');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }
}
