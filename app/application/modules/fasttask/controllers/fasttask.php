<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Visualization Controller
 * @author chientran <tran.duc.chien@kloon.vn>
 * @created 23 May 2013
 */
class Fasttask extends MY_Controller {
    function __construct() {
        parent::__construct();

        // Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        // Language
        $this->load->language('winterdienst', $this->_global_language);

        // id_city
        $this->_id_city = current_city();

        $nav_module_current = isset($this->data['nav_module_current']) ? $this->data['nav_module_current'] : false;
        if ($nav_module_current) {
            $nav_module_current = intval($nav_module_current);
            $nav_module_current = ($nav_module_current > 0) ? $nav_module_current : 1;
        } else {
            $nav_module_current = 1;
        }
        $this->data['nav_module_current'] = $nav_module_current;
    }

    public function index() {
        if ($this->input->is_ajax_request()) {
            $id_fasttask = intval($this->input->get('id_fasttask'));
            $data        = array();
            $message     = array();
            $voice       = array();
            $image       = array();
            $video       = array();
            $api_param   = array(
                'id_city'     => $this->_id_city,
                'id_fasttask' => $id_fasttask
            );
            $api_result = $this->rest->get('web/v1/fasttask/information', $api_param, 'json');
            $api_status = $this->rest->status();
            if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
                $data = $api_result->data;
                foreach ($data as $key => $value) {
                    $type = get_value('type', $value);
                    switch ($type) {
                        case INFO_VOICE:
                            $voice[] = $value;
                            break;

                        case INFO_VIDEO:
                            $video[] = $value;
                            break;

                        case INFO_MESSAGE:
                            $message[] = $value;
                            break;

                        case INFO_IMAGE:
                            $image[] = $value;
                            break;
                    }
                }
            }
            $this->data['message']    = json_decode(json_encode($message), true);
            $this->data['voice']      = json_decode(json_encode($voice), true);
            $this->data['image']      = json_decode(json_encode($image), true);
            $this->data['video']      = json_decode(json_encode($video), true);
            $this->data['data']       = $data;
            $this->data['type']       = 'info';
            $this->data['tab_select'] = 'message';
            $this->layout             = false;
            $this->view               = 'information';
            return;
        }

        // Breadcrumb
        $this->breadcrumb->append_crumb('Fasttask', create_url('fasttask/visualization'));

        // Assets
        $this->load_assets('winterdienst.css', ASSET_CSS);
        $this->load_assets('select2/select2.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('projekktor/themes/maccaco/projekktor.style.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('select2/select2.min.js', ASSET_JS);
        $this->load_assets('jquery.slimscroll.min.js', ASSET_JS);
        $this->load_assets('openlayer/OpenLayers.js', ASSET_JS);
        $this->load_assets('projekktor/projekktor-1.3.00.min.js', ASSET_JS);
        $this->load_assets('functions.js', ASSET_JS);
        $this->load_assets('openlayer.js', ASSET_JS);
        // $this->load_assets('winterdienst.js', ASSET_JS);

        // Params
        $module = intval($this->input->get('module'));
        $this->data['module'] = $module;

        // Worker
        $workers    = array();
        $api_param  = array(
            'id_city'   => $this->_id_city,
            // 'id_module' => $module
        );
        $api_result = $this->rest->get('web/v1/worker/index', $api_param, 'json');
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $workers[$value->id] = $value;
            }
        }
        $this->data['workers'] = $workers;

        // Machine
        // $machines  = array();
        // $api_param = array(
        //     'id_city'   => $this->_id_city,
        //     'id_module' => $module,
        // );
        // $api_result = $this->rest->get('web/v1/machine/index', $api_param, 'json');
        // $api_status = $this->rest->status();
        // if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
        //     foreach ($api_result->data as $key => $value) {
        //         $machines[$value->id] = $value;
        //     }
        // }
        // $this->data['machines'] = $machines;

        // Street
        // $streets    = array();
        // $api_param  = array(
        //     'id_city'   => $this->_id_city,
        //     'id_module' => $module,
        // );
        // $api_result = $this->rest->get('web/v1/street/index', $api_param, 'json');
        // $api_status = $this->rest->status();
        // if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
        //     foreach ($api_result->data as $key => $value) {
        //         $streets[] = $value;
        //     }
        // }
        // $this->data['streets'] = $streets;

        // Param
        $api_param = array(
            'id_city'   => $this->_id_city,
            'id_module' => $module
            // 'offset'  => 0,
            // 'limit'   => 30
        );

        // by_date param
        $by_date = $this->input->get('by_date');
        if ($by_date === false) {
            $by_date = date('d.m.Y');
        } else {
            $by_date = trim($by_date);
        }
        $by_date_array = explode('.', $by_date);
        $by_date_timestamp = FALSE;
        if ( is_array($by_date_array) AND (count($by_date_array) == 3) ) {
            $by_date_timestamp = strtotime("{$by_date_array[2]}-{$by_date_array[1]}-{$by_date_array[0]}");
            $tmp               = date('d.m.Y',  $by_date_timestamp);
            if ($by_date === $tmp) {
                $api_param['day']      = date('Y-m-d', $by_date_timestamp);
                $this->data['by_date'] = $by_date;
            }
        }
        $this->data['current_time'] = $by_date_timestamp ? $by_date_timestamp : time();

        // by_worker param
        $by_worker = intval($this->input->get('by_worker'));
        if ($by_worker <= 0) {
            $by_worker = FALSE;
        } else {
            $api_param['worker']     = $by_worker;
            $this->data['by_worker'] = $by_worker;
        }

        // by_machine param
        // $by_machine = intval($this->input->get('by_machine'));
        // if ($by_machine <= 0) {
        //     $by_machine = FALSE;
        // } else {
        //     $api_param['machine']     = $by_machine;
        //     $this->data['by_machine'] = $by_machine;
        // }

        // by_street
        $by_street = $this->input->get('by_street');
        if (!isset($by_street) || empty($by_street)) {
            $by_street = FALSE;
        } else {
            $api_param['street']     = $by_street;
            $this->data['by_street'] = $by_street;
        }

        // GPS
        $result     = array();
        $api_result = $this->rest->get('web/v1/fasttask', $api_param, 'json');
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            $result = $api_result->data;
        }
        $this->data['result'] = $result;

        // View
        $this->data['defaul_coordinate'] = $this->defaul_coordinate();
        $this->data['module']            = $module;
        $this->data['module_list_array'] = module_list_array();
        $this->view                      = 'index';
        $this->layout                    = 'layouts/module';
    }

    function defaul_coordinate() {
        $api_param = array(
            'id_city' => $this->_id_city
        );
        $api_result = $this->rest->get('web/city', $api_param, 'json');
        $api_status = $this->rest->status();
        $return = array();
        if ( isset($api_result->status) AND ($api_result->status == TRUE) AND isset($api_result->data) AND !empty($api_result->data) ) {
            $data = $api_result->data[0];
            $return = array(
                'lat' => isset($data->lat) ? $data->lat : '',
                'lon' => isset($data->lon) ? $data->lon : '',
            );
        }
        return $return;
    }
}