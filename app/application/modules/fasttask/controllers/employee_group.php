<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author le.nhung@kloon.vn
 * @copyright 2015
 */
class Employee_group extends MY_Controller {
    private $_id_city   = 1;
    private $_limit     = 10;
    private $_offset    = 0;
    private $_edit_flag = false;

    function __construct(){
        parent:: __construct();

        // Language
        $this->load->language('employee_group');
        $this->load->language('cost_center');
        // Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        // Breakcrumbs
        $this->breadcrumb->append_crumb(lang('lbl_employee_group'), create_url('fasttask/employee_group'));

        // id_city
        $this->_id_city = current_city();

        // limit of grid
        $this->_limit   = config_item('grid_limit');
    }


    function index(){
        //asset
        $this->load_assets('select2/select2.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('select2/select2.min.js', ASSET_JS);

        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_EMPLOYEE_GOUP);
            return;
        }

        // Grid columns width
        if (current_language(FALSE) == 'de') {
            $column_properties = array(
                'order_width'       => 30,
                'id_width'          => 50,
                'cost_center_width' => 120,
                'updated_width'     => 120,
                'created_width'     => 100,
                'option_width'      => 200,
            );
        } else {
            $column_properties = array(
                'order_width'       => 30,
                'id_width'          => 50,
                'cost_center_width' => 100,
                'updated_width'     => 100,
                'created_width'     => 100,
                'option_width'      => 140,
            );
        }
        $this->data['column_properties'] = json_encode($column_properties);

        // View
        $this->load_assets('grid_view.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        $this->data['limit'] = $this->_limit;
        $this->view          = 'employee_group/index';
        $this->layout        = 'layouts/module';
    }

    function add() {
        $this->_update();
    }

    function edit($id = 0) {
        $this->_edit_flag = true;
        $this->_update($id);
    }

    function _update($id = NULL) {

        if (!$this->input->is_ajax_request()) {
            redirect(create_url('fasttask/employee_group'));
        }

        // Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
                if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }

        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Post data
            $title         = $this->input->post('title');
            $id_costcenter = $this->input->post('id_costcenter');

            // Validation
            $this->form_validation->CI =& $this;
            $this->form_validation->set_rules('title', lang('lbl_title'), 'required');

            if ($this->form_validation->run() === TRUE) {

                $api_param   = array(
                    'id_city'        => $this->_id_city,
                    'title'          => $title,
                );
                if(isset($id_costcenter) && !empty($id_costcenter)){
                    $id_costcenter = implode(",",$id_costcenter);
                    $api_param['id_cost_center'] = $id_costcenter;
                }
                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_result      = $this->rest->put('web/v1/employee_group/index?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_result = $this->rest->post('web/v1/employee_group/index?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status = $this->rest->status();
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = lang('msg_employee_group_saved');
                    $flag = TRUE;
                } else {
                    $this->data['err'] = lang('err_api_save');
                }
            } else {
                //$this->data['err'] = validation_errors();
            }
        }

        # Data
        if ($this->_edit_flag) {
            $api_param = array(
                'id'      => $id,
                'id_city' => $this->_id_city,
            );
            $api_result = $this->rest->get('web/v1/employee_group/index', $api_param, 'json');
            $api_status  = $this->rest->status();
            if ( isset($api_result->data) AND !empty($api_result->data) ) {
                $data               = $api_result->data;
                $this->data['data'] = $data[0];
            }
        }
        #cost center
        $cost_center  = array();
        $api_param = array(
           'id_city' => $this->_id_city,
        );
        if ($this->_edit_flag) {
            $api_param['id_employee_group'] = $id;
        }
        $api_result = $this->rest->get('web/v1/cost_center/assigned',$api_param);
        $api_status = $this->rest->status();

        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $cost_center[$value->id] = $value->title;
            }

        }
        $this->data['cost_center'] = $cost_center;

        // View
        $this->data['edit_flag'] = $this->_edit_flag;
        $this->data['flag']      = $flag;
        $this->layout            = false;
        $this->view              = 'employee_group/detail';
    }

    function delete($id = 0) {
        $return = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 0) {
                $api_param = array(
                    'id' => $id
                );
                $api_result = $this->rest->delete('web/v1/employee_group?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    =  lang('msg_employee_group_has_been_deleted');
                } else {
                    $return['status'] = false;
                    $return['msg']    = lang('err_has_an_error_when_update_database');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }

    /**
     * @description : get list cost center
     * @function    : list_cost_center
     * @author      : Le Thi Nhung(le.nhung@kloon.vn)
     * create       : 2015-06-16
     * @param      :
     */
/*    function list_cost_center() {
        $cost_center = array();
        $api_param   = array(
            'id_city'=> $this->_id_city
        );
        $api_result_costcenter = $this->rest->get('web/v1/cost_center/index', $api_param);
        if ( isset($api_result_costcenter->status) AND ($api_result_costcenter->status == REST_STATUS_SUCCESS) AND (isset($api_result_costcenter->data)) AND (!empty($api_result_costcenter->data)) ) {
            foreach ($api_result_costcenter->data as $key => $value) {
                $cost_center[$value->id] = $value;
            }
        }
        return $cost_center;
    }*/

    function list_cost_center($id_employee_group = false){

        $this->load->helper('form');
        $cost_center        = $this->input->get('id_cost_center');
        $cost_centers      = array();
        $assigned = '';
        if($id_employee_group){
            $assigned = '&id_employee_group='.$id_employee_group;
        }
        $api_result     = $this->rest->get('web/v1/cost_center/assigned?id_city='.$this->_id_city.$assigned);
        $api_status     = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $cost_centers[$value->id]  = $value ->title;
            }
        }
        echo form_dropdown('id_cost_center', $cost_centers, $cost_center , 'class="select2" style="width: 250px" multiple="true"');
    }
}
