<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author le.nhung@kloon.vn
 * @copyright 2015
 */
class Superior extends MY_Controller {
    private $_id_city   = 1;
    private $_limit     = 10;
    private $_offset    = 0;
    private $_edit_flag = false;

    function __construct(){
        parent:: __construct();

        // Language
        $this->load->language('superior');
        $this->load->language('company');
        $this->load->language('activity');

        // Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        // Breakcrumbs
        $this->breadcrumb->append_crumb(lang('lbl_superiors'), create_url('fasttask/superior'));

        // id_city
        $this->_id_city = current_city();

        // limit of grid
        $this->_limit   = config_item('grid_limit');
    }


    function index(){
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_WORKER);
            return;
        }

        // Grid columns width
        if (current_language(FALSE) == 'de') {
            $column_properties = array(
                'order_width'      => 30,
                'id_width'         => 50,
                'first_name_width' => 120,
                'last_name_width'  => 120,
                'fullname_width'   => 150,
                'code_width'       => 120,
                'price_width'      => 80,
                'phone_width'      => 130,
                'nfc_width'        => 200,
                'company_width'    => 150,
                'updated_width'    => 120,
                'created_width'    => 100,
                'option_width'     => 200,
            );
        } else {
            $column_properties = array(
                'order_width'      => 30,
                'id_width'         => 50,
                'first_name_width' => 80,
                'last_name_width'  => 100,
                'fullname_width'   => 150,
                'code_width'       => 120,
                'price_width'      => 80,
                'phone_width'      => 130,
                'nfc_width'        => 200,
                'company_width'    => 150,
                'updated_width'    => 100,
                'created_width'    => 100,
                'option_width'     => 140,
            );
        }
        $this->data['column_properties'] = json_encode($column_properties);

        // View
        $this->load_assets('grid_view.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        $this->data['limit'] = $this->_limit;
        $this->view          = 'superior/index';
        $this->layout        = 'layouts/module';
    }

    function add() {
        $this->_update();
    }

    function edit($id = 0) {
        $this->_edit_flag = true;
        $this->_update($id);
    }

    /*
     * @description Validate for activity
     * @function id_activity_check
     * @author le Thi Nhung(le.nhung@kloon.vn)
     * @create 2015-05-28
     * @param $id_activity(array id activity)
     */
    public function id_activity_check($id_activity)
    {
        if(count($id_activity)<2)
        {
            $this->form_validation->set_message('id_activity_check', lang('message_activity'));
            return FALSE;
        }
        else{
            return TRUE;
        }
    }

    function _update($id = NULL) {
       // Assets
        $this->load_assets('select2/select2.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('select2/select2.min.js', ASSET_JS);

        if (!$this->input->is_ajax_request()) {
            redirect(create_url('fasttask/superior'));
        }

        // Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
                if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }

        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Post data
            $first_name       = trim($this->input->post('first_name'));
            $last_name        = trim($this->input->post('last_name'));
            $phone            = trim($this->input->post('phone'));
            $address          = trim($this->input->post('address'));
            $birthday         = trim($this->input->post('birthday'));
            $personal_code    = trim($this->input->post('personal_code'));
            $nfc_code         = trim($this->input->post('nfc_code'));
            $company          = intval($this->input->post('id_company'));
            $leader           = intval($this->input->post('leader'));
            $price            = floatval($this->input->post('price'));
            $module           = $this->input->post('module');
            $activity         = $this->input->post('activity');
            $activity_default = $this->input->post('activity_default');

            // Validation
            $this->form_validation->CI =& $this;
            $this->form_validation->set_rules('activity', lang('lbl_activity'), 'required');
            $this->form_validation->set_rules('address', lang('lbl_address'), 'trim');
            $this->form_validation->set_rules('birthday', lang('lbl_birthday'), 'trim');
            $this->form_validation->set_rules('first_name', lang('lbl_first_name'), 'required');
            $this->form_validation->set_rules('id_company', lang('lbl_company'), 'required|integer');
            $this->form_validation->set_rules('last_name', lang('lbl_last_name'), 'required');
            $this->form_validation->set_rules('module', lang('lbl_module'), 'required');
            $this->form_validation->set_rules('nfc_code', lang('lbl_nfc_code'), 'trim');
            $this->form_validation->set_rules('personal_code', lang('lbl_personal_code'), 'trim');
            $this->form_validation->set_rules('phone', lang('lbl_phone'), 'trim');
            $this->form_validation->set_rules('price', lang('superior_lbl_price'), 'numeric|greater_than[0]');

            if ($this->form_validation->run() === TRUE) {
                $api_param   = array(
                    'id'               =>  $id,
                    'id_ref'           => '',
                    'id_city'          => $this->_id_city,
                    'id_company'       => $company,
                    'module'           => $module,
                    'nfc_code'         => $nfc_code,
                    'personal_code'    => $personal_code,
                    'price'            => $price,
                    'first_name'       => $first_name,
                    'last_name'        => $last_name,
                    'address'          => $address,
                    'phone'            => $phone,
                    'birthday'         => $birthday,
                    'activity'         => $activity,
                    'activity_default' => $activity[0],
                );

                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_result      = $this->rest->put('web/v2/superior/index?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_result = $this->rest->post('web/v2/superior/index?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status = $this->rest->status();
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = lang('msg_superior_saved');
                    $flag = TRUE;
                } else {
                    $this->data['err'] = lang('err_api_save');
                }
            } else {
                $this->data['err'] = validation_errors();
            }
        }

        // Data
        if ($this->_edit_flag) {
            $api_param = array(
                'id_worker' => $id,
                'id_city'   => $this->_id_city
            );
            $api_result = $this->rest->get('web/v2/superior/index', $api_param, 'json');
            $api_status  = $this->rest->status();
            if ( isset($api_result->data) AND !empty($api_result->data) ) {
                $this->data['worker']            = $api_result->data->worker;
                $this->data['worker_modules']    = $api_result->data->module;
                $this->data['worker_activities'] = $api_result->data->activity;
            }
        }

        // Company
        $company    = array();
        $api_param  = array(
            'id_city'=> $this->_id_city
        );
        $api_result_companies = $this->rest->get('web/v1/company/index', $api_param);
        $api_status_companies = $this->rest->status();
        if ( isset($api_result_companies->status) AND ($api_result_companies->status == REST_STATUS_SUCCESS) AND (isset($api_result_companies->data)) AND (!empty($api_result_companies->data)) ) {
            foreach ($api_result_companies->data as $key => $value) {
                $company[$value->id] = $value;
            }
        }
        $this->data['company'] = $company;

        // NFC
        $nfc_code  = array();
        $api_param = array(
            'id_worker' => $id,
            'id_city'   => $this->_id_city,
            'active'    => false
        );
        $api_result = $this->rest->get('web/v1/nfc/index',$api_param);
        $api_status = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $nfc_code[$value->id] = $value;
            }

        }
        $this->data['nfc_code'] = $nfc_code;

        // Activities
        $activities = array();
        $api_result = $this->rest->get('web/v1/activity/index?id_city='.$this->_id_city);
        $api_status = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $activities[$value->id] = $value;
            }
        }
        $this->data['activities'] = $activities;

        // Modules
        $modules = array();
        $api_result = $this->rest->get('web/v1/module/index?id_city='.$this->_id_city);
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $modules[$value->id] = $value->title;
            }
        }
        $this->data['modules']   = $modules;

        // View
        $this->data['edit_flag'] = $this->_edit_flag;
        $this->data['flag']      = $flag;
        $this->layout            = false;
        $this->view              = 'superior/detail';
    }

    function delete($id = 0) {
        $return = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 0) {
                $api_param = array(
                    'id_worker' => $id
                );
                $api_result = $this->rest->delete('web/v1/superior?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    =  lang('msg_superior_has_been_deleted');
                } else {
                    $return['status'] = false;
                    $return['msg']    = lang('err_has_an_error_when_update_database');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }

    function _get_companies($id_city) {
        $company    = array();
        $api_param  = array(
            'id_city'=> $this->_id_city
        );
        $api_result_companies = $this->rest->get('web/v1/company/index', $api_param);
        $api_status_companies = $this->rest->status();
        if ( isset($api_result_companies->status) AND ($api_result_companies->status == REST_STATUS_SUCCESS) AND (isset($api_result_companies->data)) AND (!empty($api_result_companies->data)) ) {
            foreach ($api_result_companies->data as $key => $value) {
                $company[$value->id] = $value;
            }
        }
        return $company;
    }

    /**
     * @description : get all id activity of worker
     * @function    : _idacticityofworker
     * @author      : Le Thi Nhung(le.nhung@kloon.vn)
     * create       : 2015-05-28
     *
     * @param      : $id(id worker)
     */
    function _idacticityofworker($id){
        #array id activity of worker
        $idacticityofworker = array();
        $api_param = array(
                'id_worker' => $id,
                'id_city'   => $this->_id_city
        );
        $api_result = $this->rest->get('web/v2/superior/w_index',$api_param);
        $api_status = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $idacticityofworker= $value->activity;
            }

        }
        return $idacticityofworker;
    }

    /**
     * @description : get activity where id_activity=1,2,3
     * @function    : _activityofworker
     * @author      : Le Thi Nhung(le.nhung@kloon.vn)
     * create       : 2015-05-28
     *
     * @param      :
     */
    function _activityofworker($id_activity){
        $result = array();
        $api_param  = array(
            'id'=>$id_activity,
            'id_city'    =>$this->_id_city,
        );
        $api_result = $this->rest->get('web/v1/activity',$api_param);
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            $result = $api_result->data;
        }
        return $result;
    }

    /**
     * @description : get activity by id_city
     * @function    : _activityofcity
     * @author      : Le Thi Nhung(le.nhung@kloon.vn)
     * create       : 2015-05-28
     *
     * @param      :
     */
    function _activityofcity(){
        $activities    = array();
        $api_result = $this->rest->get('web/v1/activity/index?id_city='.$this->_id_city);
        $api_status = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $activities[$value->id] = $value;
            }

        }
        return $activities;
    }

    /**
     * @description : get activity by id_activity and id_city
     * @function    : activitybyid
     * @author      : Le Thi Nhung(le.nhung@kloon.vn)
     * create       : 2015-05-28
     *
     * @param       : id_activity
     */
    function activitybyid($id_activity=null){
        $this->load->helper('form');
        if(empty($id_activity)){
            $id_activity = $this->input->get('id_activity');
        }
        if(isset($id_activity) && !empty($id_activity)){
            $id_activity = implode(",",$id_activity);
        }
        $id_default     = $this->input->get('id_default');
        $activities = array('' => lang('lbl_select_default'),);
        if($id_activity!=0){
            $api_param  = array(
                'id'=>$id_activity,
                'id_city'    =>$this->_id_city,
            );
            $api_result = $this->rest->get('web/v1/activity',$api_param);
            $api_status = $this->rest->status();
            if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
                foreach ($api_result->data as $key => $value) {
                    $activities+= array(
                            $value->id =>$value->name,
                    );
                }
            }
        }
        echo form_dropdown('id_default',$activities, $id_default, 'id="id_default" class="select2" style="width: 250px;"');
    }

    function list_company(){
        $this->load->helper('form');
        $company        = $this->input->get('id_company');
        $companies      = array();
        $api_result     = $this->rest->get('web/v1/company/index?id_city='.$this->_id_city);
        $api_status     = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $companies[$value->id]  = $value -> name;
            }
        }
        echo form_dropdown('id_company', $companies, $company , 'class="select2" style="width: 250px"');
    }

    function list_activity(){
        $this->load->helper('form');
        $id_activity = $this->input->get('id_activity');
        $activity      = array();
        $api_result     = $this->rest->get('web/v1/activity/index?id_city='.$this->_id_city);
        $api_status     = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                 $activity[$value->id] = $value->name;
            }
        }
        echo form_dropdown('id_activity[]', $activity, $id_activity, 'id="id_activity" class="select2" style="width: 250px" multiple="true" onchange="change_activity()"');
    }
}
