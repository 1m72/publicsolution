<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Freetext controller
 *
 * @author Nhung le <le.nhung@kloon.vn>
 * @created 2015-05-29
 */

class Freetext extends MY_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();

         # Language
        $this->load->language('freetext');

        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        # Breakcrumbs
        $this->breadcrumb->append_crumb(lang('freetxt_lbl'), create_url('fasttask/freetext'));

        # id_city
        $this->_id_city = current_city();

        # limit of grid
        $this->_limit   = config_item('grid_limit');
    }

    function index() {
        $this->load_assets('functions.js', ASSET_JS);
        # View
        $this->load_assets('grid_view.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        $this->view          = 'freetext/index';
        $this->layout        = 'layouts/module';
    }

    function add($id=NULL, $level_text=NULL) {
        $this->_update($id,$level_text);
    }

    function edit($id = 0) {
        $this->_edit_flag = true;
        $this->_update($id);
    }

    function _update($id = NULL, $level_text=NULL) {

        if (!$this->input->is_ajax_request()) {
            redirect(create_url('fasttask/freetext'));
        }

        # Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }
        if (!($this->_edit_flag) && !is_null($level_text)) {
            $level_text = intval($level_text);
            if ($level_text < 0) {
                $level_text = 0;
            }
            $this->data['level_text'] = $level_text+1;
        }
        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            # Post data
            $title         = trim($this->input->post('title'));

            $modules = array();
            // Form validate - freetext
            $this->form_validation->set_rules('title', lang('freetxt_lbl_title'), 'required');
            if ($this->form_validation->run() === TRUE) {
                // api params
                $api_param   = array(
                    'title'      => $title,
                );

                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_result     = $this->rest->put('web/v1/freetext/index?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    if(!is_null($level_text)){
                         $api_param['level']     = $level_text;
                         $api_param['parent_id'] = $id;
                    } else{
                         $api_param['level']     = 1;
                         $api_param['parent_id'] = 0;
                    }
                    //var_dump($api_param);die;
                    $api_result = $this->rest->post('web/v1/freetext/index?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status = $this->rest->status();

                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = lang('freetxt_msg_information_has_been_saved');
                    $flag              = true;
                } else {
                    $this->data['err'] = lang('err_api_save');
                    $flag              = false;
                }
            } else {}
        }

        # Data
        if ($this->_edit_flag) {
            $api_param = array(
                'id'      => $id,
                'id_city' => $this->_id_city,
            );
            $api_result = $this->rest->get('web/v1/freetext/index', $api_param, 'json');
            $api_status  = $this->rest->status();
            if ( isset($api_result->data) AND !empty($api_result->data) ) {
                $data               = $api_result->data;
                $this->data['data'] = $data[0];
            }
        }
        // View
        $this->data['edit_flag']  = $this->_edit_flag;
        $this->data['flag']       = $flag;
        $this->layout             = false;
        $this->view               = 'freetext/detail';
    }

    function delete($id = 0) {
        $return = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 0) {
                $api_param = array(
                    'id' => $id
                );
                $api_result = $this->rest->delete('web/v1/freetext/index?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    = lang('freetxt_msg_has_been_deleted');
                } else {
                    $return['status'] = false;
                    $return['msg']    = lang('err_has_an_error_when_update_database');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }
    function freetextOfLevel($level){
        $this->load->helper('form');
        $arr_level      = array();
        $api_result     = $this->rest->get('web/v1/freetext/index?id_city='.$this->_id_city.'&level='.$level);
        $api_status     = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                 $arr_level[$value->id] = $value->title;
            }
        }
        return $arr_level;
        //echo form_dropdown('level[]', $arr_level, $level, 'id="level" class="select2" style="width: 250px" multiple="true" onchange="change_level()"');

    }
    function listLevel(){
        $this->load->helper('form');
        $levels      = array();
        $api_result     = $this->rest->get('web/v1/freetext/index?id_city='.$this->_id_city);
        $api_status     = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                 $levels[] = $value->level;
            }
        }
        $levels = array_unique($levels);
        return $levels;
    }
    function treeviewText(){
        $this->load->helper('form');
        $data_result      = array();
        $api_result     = $this->rest->get('web/v1/freetext/index?id_city='.$this->_id_city);
        $api_status     = $this->rest->status();
        $data_new = array();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
           $data_result = $api_result->data;
            foreach ($data_result as $k => $v) {
                $v->level_text = $v->level;
                unset($v->level);
                $data_new[$v->id] = $v;
            }
            foreach ($data_new as $key => $value) {
                $parent_id = $value->parent_id;
                if($parent_id!=0 && isset($data_new[$parent_id])){
                   $data_new[$parent_id]->items[]= $value;
                }
            }
            foreach ($data_new as $k1 => $v1) {
                if($v1->level_text!=1){
                    unset($data_new[$k1]);
                }
            }
        }
        echo json_encode(array_values($data_new));
    }
}
