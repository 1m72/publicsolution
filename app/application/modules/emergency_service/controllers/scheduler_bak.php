<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *  Scheduler Controller
 * @author le.nhung@kloon.vn
 * @copyright 2014-05-17
 */
class Scheduler extends MY_Controller {

    private $_id_city   = 1;
    private $_edit_flag = false;
    private $_id_module = 1;
    private $_type      = 1;

    function __construct(){
        parent:: __construct();

        # Language
        $this->load->language('scheduler');
        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        # Breakcrumbs
        $this->breadcrumb->append_crumb($this->lang->line('sd_lbl_schedule'), create_url('emergency_service/scheduler'));

        # id_city
        $this->_id_city = current_city();

        # limit of grid
        $this->_limit   = config_item('grid_limit');
    }
    /*
     * @description get start week and end week
     * @function getWeekForDate
     * @author le Thi Nhung(le.nhung@kloon.vn)
     * @create 2014-05-26
     * @param
     */
    public function getWeekForDate($date, $weekStartSunday = false){
        $timestamp = strtotime($date);
        // Week starts on Sunday
        if($weekStartSunday){
            $start = (date("D", $timestamp) == 'Sun') ? date('Y-m-d', $timestamp) : date('Y-m-d', strtotime('Last Sunday', $timestamp));
            $end = (date("D", $timestamp) == 'Sat') ? date('Y-m-d', $timestamp) : date('Y-m-d', strtotime('Next Saturday', $timestamp));
        } else { // Week starts on Monday
            $start = (date("D", $timestamp) == 'Mon') ? date('Y-m-d', $timestamp) : date('Y-m-d', strtotime('Last Monday', $timestamp));
            $end = (date("D", $timestamp) == 'Sun') ? date('Y-m-d', $timestamp) : date('Y-m-d', strtotime('Next Sunday', $timestamp));
        }

        return array('start' => $start, 'end' => $end);
    }
    # random color
    function randomColor() {
        $possibilities = array(1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F" );
        shuffle($possibilities);
        $color = "#";
        for($i=1;$i<=6;$i++){
            $color .= $possibilities[rand(0,14)];
        }
        return $color;
    }
    # get color from string
    public function stringToColorCode($str) {
        $code = dechex(crc32($str));
        $code = substr($code, 0, 6);
        return $code;
    }
    /*
     * @description list scheduler of worker
     * @function list_scheduler
     * @author le Thi Nhung(le.nhung@kloon.vn)
     * @create 2014-05-17
     * @param
     */
    public function list_scheduler($date_change = null,$worker_id=null)
    {
        #worktime
        $this->load->helper("date");
        $mess = "faill";
        $date_change = $this->input->get('date_change');
        $worker_id   = $this->input->get('worker_id');
        $group_id    = $this->input->get('group_id');
        $starttime = '';
        $endtime   = '';
        #start time now
        $timezone = session_login(FALSE)->timezone;
        $now = time();
        $local_gmt = local_to_gmt($now); //thoi gian hien tai voi mui gio 0

        $daylight_saviing =true;
        $gmt_local = gmt_to_local($local_gmt, $timezone,$daylight_saviing ); //tgian hien tai ung voi mui gio cua nguoi dung
        $date_format=date("Y-m-d H:i:s",$gmt_local);
        $time = explode(" ", $date_format);
        #end time now
        if ($date_change === false) {
            $weekForDate = $this->getWeekForDate($date_format, $weekStartSunday = true);
            $starttime   = $weekForDate['start']." ".$time[1];
            $endtime     = $weekForDate['end']." ".$time[1];
        } else {
            $date_change = explode(" (", $date_change);
            $date_change = new DateTime($date_change[0]);
            $date_change = $date_change->format('Y-m-d H:i:s');
            $weekForDate = $this->getWeekForDate($date_change,$weekStartSunday = true);
            $starttime   = $weekForDate['start']." ".$time[1];
            $endtime     = $weekForDate['end']." ".$time[1];
        }
        $api_param  = array(
            'id_city'   => $this->_id_city,
            'starttime' => $starttime,
            'endtime'   => $endtime,
            'type'      =>$this->_type,
            'id_module' =>$this->_id_module,
        );
        # if select worker_id
        if ($worker_id !== false) {
            $worker_id = implode(',', $worker_id);
            $api_param['worker_id'] =$worker_id;
        }
        # if select worker_id
        # if select group_id
        if ($group_id !== false) {
            $group_id = implode(',', $group_id);
            $api_param['group_id'] =$group_id;
        }
        # if select group_id
        $api_result_worktime = $this->rest->get('web/v2/worktime/index', $api_param);
        $api_status          = $this->rest->status();
        $worktime            = array();

        if ( isset($api_result_worktime->status) AND ($api_result_worktime->status == REST_STATUS_SUCCESS) AND (isset($api_result_worktime->data)) AND (!empty($api_result_worktime->data)) ) {
          $worktime = $api_result_worktime->data;
            foreach ($worktime as $key => $value) {
                $arrgroup  =  $value->id_group;
                $arrworker  =  $value->id_worker;
                $total_idgroup=0;
                $total_idworker=0;
                if(!empty($arrworker)){
                    foreach ($arrworker as $key1 => $value1) {
                        $total_idworker += $value1->id;
                    }
                    if(empty($arrgroup) ){
                        $total ="W".$total_idworker;
                    }
                }
                if(!empty($arrgroup)){
                    foreach ($arrgroup as $key2 => $value2) {
                        $total_idgroup += $value2->id;
                    }
                    if(empty($arrworker)) {
                        $total ="G".$total_idgroup;
                    }
                }
                if(!empty($arrworker) AND !empty($arrgroup)){
                    $total = $total_idworker+$total_idgroup;
                }
                $value->color ="#".$this->stringToColorCode($total);
            }
        }
        $worktime = array();
        echo json_encode($worktime);

    }
    function index(){
        # View
        $this->load_assets('kendo.all.min.js', ASSET_KENDO_JS."new_js/");
        $this->load_assets('kendo.default.min.css', ASSET_KENDO_CSS."/new_css");
        $this->load_assets('bootstrap-multiselect.js', ASSET_JS);
        $this->load_assets('jquery.bootstrap-growl.js', ASSET_JS);
        $this->load_assets('grid_view.js', ASSET_JS);

        $this->load_assets('select2/select2.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('select2/select2.min.js', ASSET_JS);
        $this->load_assets('bootstrap-multiselect.css', ASSET_CSS);

        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        #worker
        $worker = array();
        $api_param= array(
            'id_module'  =>$this->_id_module,
        );
        $api_results_workers = $this->rest->get("web/v2/worker/index?id_city=".$this->_id_city,$api_param,"json");
        if(isset($api_results_workers->status) AND ($api_results_workers->status== REST_STATUS_SUCCESS) AND (isset($api_results_workers->data)) AND (!empty($api_results_workers->data)) ){
            foreach ($api_results_workers->data as $key => $value) {
                $worker[$value->worker->id] = $value->worker->first_name." ". $value->worker->last_name;
            }
        }
        $group = array();
        $api_result_groups =  $this->rest->get("web/v2/group/index?id_city=".$this->_id_city,"json");
        if(isset($api_result_groups->status) AND ($api_result_groups->status==REST_STATUS_SUCCESS) AND (isset($api_result_groups->data)) AND (!empty($api_result_groups->data))){
            foreach ($api_result_groups->data as $key => $value) {
                $group[$value->id] = $value->name;
            }
        }
        #end worker

        $timezone = session_login(FALSE)->timezone;
        $now = time();
        $local_gmt = local_to_gmt($now); //thoi gian hien tai voi mui gio 0

        $daylight_saviing =true;
        $gmt_local = gmt_to_local($local_gmt, $timezone,$daylight_saviing ); //tgian hien tai ung voi mui gio cua nguoi dung
        $date_now=date("Y-m-d H:i:s",$gmt_local);

        $this->data['date_now'] = $date_now;
        $this->data['worker'] = $worker;
        $this->data['group']  = $group;
        $this->data['limit'] = $this->_limit;
        $this->view          = 'scheduler/index';
        $this->layout        = 'layouts/emergency_service';
    }
    function edit($id=0) {
        $id=$_GET["id"];
        $this->_edit_flag = true;
        $this->_update($id);
    }

    function add($starttime=null,$endtime=null){
        if(isset($_GET["starttime"])){
            $starttime = $_GET["starttime"];
        }
        if(isset($_GET["endtime"])){
            $endtime   = $_GET["endtime"];
        }
        $this->_update($id=null,$starttime, $endtime);
    }
    /*
    * @description check date assign
    * @function date_check
    * @author Le Thi Nhung (le.nhung@kloon.vn)
    * @creat 2014-06-06
    */
    function date_check($endtime,$params) {
        list($starttime,$date_now) = explode(',', $params);
        $result = true;

        $check_starttime = (strtotime($starttime) - strtotime($date_now)) / (24 * 60 * 60);
        $check_endtime = (strtotime($endtime) - strtotime($date_now)) / (24 * 60 * 60);

        if($check_starttime > 5 AND $check_endtime >5){
            if ($endtime <= $starttime) {
                $this->form_validation->set_message('date_check',lang('sd_msg_date'));
                $result = false;
            }
        }
        else{
            $this->form_validation->set_message('date_check',lang('sd_msg_date_datenow'));
            $result= false;
        }
        return $result;
    }
    public function workergroup_check(){
        $this->form_validation->set_message('workergroup_check',lang('sd_validate_workergroup'));
        return false;
    }

    /*
     * @description update scheduler
     * @function _update
     * @author le Thi Nhung(le.nhung@kloon.vn)
     * @create 2014-05-20
     * @param
     */
    function _update($id=null,$starttime=null,$endtime=null) {
       # Assets
        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }
        $this->load->library('form_validation');
        $worker = array();
        $group    = array();
        $worker_edit     = array();
        $worker_edit_id  = array();
        $group_edit     = array();
        $group_edit_id  = array();
        $flag = FALSE;
        $timezone = session_login(FALSE)->timezone;
        $now = time();
        $local_gmt = local_to_gmt($now); //thoi gian hien tai voi mui gio 0

        $daylight_saviing =true;
        $gmt_local = gmt_to_local($local_gmt, $timezone,$daylight_saviing ); //tgian hien tai ung voi mui gio cua nguoi dung
        $date_now=date("Y-m-d H:i:s",$gmt_local);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->CI =& $this;
            $title      = $this->input->post('title');
            $starttime  = $this->input->post('starttime');
            $endtime    = $this->input->post('endtime');
            $des        = $this->input->post('des');
            $group_id   = $this->input->post("group_id");
            $worker_id  = $this->input->post("worker_id");

            $this->form_validation->set_rules("title",lang("sd_lbl_title"),"required");
            $this->form_validation->set_rules("starttime", lang("sd_lbl_starttime"),"required");
            $this->form_validation->set_rules("endtime",lang("sd_lbl_endtime"),'required|callback_date_check['.$starttime.','.$date_now.']');
            $this->form_validation->set_rules("des",lang("sd_lbl_des"),"trim");

            if(!empty($worker_id)){
                $this->form_validation->set_rules("worker_id", lang("sd_lbl_worker"), "required");
            }
            if(!empty($group_id)){
                $this->form_validation->set_rules("group_id", lang("sd_lbl_group"), "required");
            }
            if (empty($worker_id) AND empty($group_id)) {
                $this->form_validation->set_rules("group_id", lang("sd_lbl_group"), "callback_workergroup_check");
                $this->form_validation->set_rules("worker_id", lang("sd_lbl_worker"), "callback_workergroup_check");
            }
            $array_worker_id = array();
            $array_group_id  = array();
            if(isset($worker_id) && !empty($worker_id)){
                $array_worker_id = implode(',', $worker_id);
            }
            if(isset($group_id) && !empty($group_id)){
                $array_group_id = implode(',', $group_id);
            }
            if($this->form_validation->run()==TRUE){
                #start format date from 06/27/2014 02:00 AM to 2014-06-27 02:00:00
                $starttime_param= new DateTime($starttime);
                $starttime_param = $starttime_param->format('Y-m-d H:i:s');

                $endtime_param= new DateTime($endtime);
                $endtime_param = $endtime_param->format('Y-m-d H:i:s');
                #end format date
                $api_param = array(
                    'title'     =>$title,
                    'starttime' =>$starttime_param,
                    'endtime'   =>$endtime_param,
                    'des'       =>$des,
                    'group_id'  =>$array_group_id,
                    'worker_id' =>$array_worker_id,
                    'id_module' =>$this->_id_module,
                    'type'      =>$this->_type,
                    'timezone'  =>session_login(FALSE)->timezone,
                );
                if($this->_edit_flag){
                    $api_param['id'] = $id;
                    $api_results = $this->rest->put("web/v2/worktime/index?id_city=".$this->_id_city,$api_param,'json');
                }
                else{
                    $api_results =  $this->rest->post("web/v2/worktime/index?id_city=".$this->_id_city,$api_param,'json');
                }
                $api_status  = $this->rest->status();
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = lang('sd_msg_schedule_saved');
                    $flag = TRUE;
                }else {
                    $this->data['err'] = lang('err_api_save');
                }
            }
            else {
                $this->data['err'] = validation_errors();
            }
        }
        #detail scheduler
        if ($this->_edit_flag) {
            $api_param  = array(
                'id_city'    => $this->_id_city,
                'id'         =>$id,
                'type'      =>$this->_type,
                'id_module' =>$this->_id_module,
            );
            $api_result_worktime = $this->rest->get('web/v2/worktime/index', $api_param);
            $api_status = $this->rest->status();
            if ( isset($api_result_worktime->status) AND ($api_result_worktime->status == REST_STATUS_SUCCESS) AND (isset($api_result_worktime->data)) AND (!empty($api_result_worktime->data)) ) {
                $worktime = $api_result_worktime->data[0];
                $this->data['scheduler'] = $worktime;
                foreach ($worktime->id_worker as $key => $value) {
                    $worker_edit[$value->id] = $value->first_name." ".$value->last_name;

                    $worker_edit_id [$key] = $value->id;
                    $this->data['worker_edit_id'] = $worker_edit_id;
                }
                foreach ($worktime->id_group as $key => $value) {
                    $group_edit[$value->id] = $value->name;

                    $group_edit_id [$key] = $value->id;
                    $this->data['group_edit_id'] = $group_edit_id;
                }
                $starttime_check  = $worktime->starttime;
                $endtime_check    = $worktime->endtime;
            }
        }
        else{
            $starttime  = explode(" (", $starttime);
            $endtime    = explode(" (", $endtime);
            $starttime  = new DateTime($starttime[0]);
            $endtime    = new DateTime($endtime[0]);
            $this->data['starttime']  = $starttime->format('Y-m-d H:i:s');
            $this->data['endtime']    = $endtime->format('Y-m-d H:i:s');
            $starttime_check  =  $this->data['starttime'];
            $endtime_check    =  $this->data['endtime'];
        }
        #end detail scheduler
        $api_param = array(
            "starttime"  => $starttime_check,
            "endtime"    => $endtime_check,
            "id_city"    => $this->_id_city,
        );
        $api_results_w_g = $this->rest->get("web/v2/worktime/worktime_check",$api_param,'json');
        if(isset($api_results_w_g->status) AND ($api_results_w_g->status== REST_STATUS_SUCCESS) AND (isset($api_results_w_g->worker)) AND (!empty($api_results_w_g->worker)) ){
            foreach ($api_results_w_g->worker as $key => $value) {
                $worker[$value->id] = $value->first_name." ". $value->last_name;
            }
            $this->data["worker"]  = $worker + $worker_edit;
        }
        if(isset($api_results_w_g->status) AND ($api_results_w_g->status== REST_STATUS_SUCCESS) AND (isset($api_results_w_g->group)) AND (!empty($api_results_w_g->group)) ){
            foreach ($api_results_w_g->group as $key => $value) {
                $group[$value->id] = $value->name;
            }
            $this->data["group"]  = $group + $group_edit;
        }
        $this->view          = 'scheduler/detail';
        # end check add
        $this->data['edit_flag'] = $this->_edit_flag;
        $this->data['flag']      = $flag;
        $this->layout        = false;
    }

    /*
     * @description delete scheduler
     * @function index_delete
     * @author le Thi Nhung(le.nhung@kloon.vn)
     * @create 2014-05-28
     * @param
     */
    function delete($id = 0) {
        $id = $_GET['id'];
        $return = array();
        $id = intval($id);
        if ($id > 0) {
            $api_param = array(
                'id' => $id
            );
            $api_results = $this->rest->delete('web/v2/worktime?id_city='.$this->_id_city, $api_param, 'json');
            if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                $return['status'] = true;
                $return['msg']    =  $this->lang->line('sd_msg_has_been_deleted');
            } else {
                $return['status'] = false;
                $return['msg']    = $this->lang->line('err_has_an_error_when_update_database');
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }
}