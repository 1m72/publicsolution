<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Group extends MY_Controller{
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();
         # Language
        $this->load->language('group');
        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));
        # Breakcrumbs
        $this->breadcrumb->append_crumb($this->lang->line('group_lbl'), create_url('sys/group'));
        # id_city
        $this->_id_city = current_city();
        # limit of grid
        $this->_limit   = config_item('grid_limit');
        $this->load_assets('group.css', ASSET_CSS);
    }

    function index(){
        if ($this->input->is_ajax_request()) {
           $this->_kendoui_process(GET_GROUP);
           return;
        }

        if (current_language(FALSE) == 'de') {
            $column_properties = array(
                'order_width'             => 50,
                'id_width'                => 100,
                'name_width'              => 200,
                'updated_width'           => 120,
                'created_width'           => 100,
                'option_width'            => 200,
            );
        } else {
            $column_properties = array(
                'order_width'             => 50,
                'id_width'                => 100,
                'name_width'              => 200,
                'updated_width'           => 100,
                'created_width'           => 100,
                'option_width'            => 140,
            );
        }
        $this->data['column_properties']    = json_encode($column_properties);
        $this->load_assets('select2/select2.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('select2/select2.min.js', ASSET_JS);
        $this->load_assets('bootstrap-switch3/bootstrap-switch.min.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('bootstrap-switch3/bootstrap-switch.min.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        # worker by group
        $worker_by_group    = array();
        $api_param          = array(
            'id_city'       => $this->_id_city
        );
        $api_result         = $this->rest->get('web/v2/group/worker_by_group',$api_param,'json');
        if(isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            $worker_by_group = $api_result->data;
        }
        $this->data['worker_by_group'] = json_decode(json_encode($worker_by_group), true);

        $this->data['group']           = $this->getGroupParent();
        $this->data['group_by_group']  = $this->getAllGroup();
        $this->data['worker']          = $this->getWorker();
        $this->data['limit'] = $this->_limit;
        $this->view          = 'group/index';
        $this->layout        = 'layouts/module';
    }
    function add(){
        $this->_update();
    }
    function edit($id = 0){
        $this->_edit_flag = true;
        $this->_update($id);
    }
    function _update($id = null){
        if(!$this->input->is_ajax_request()){
            redirect(create_url('sys/group'));
        }
        $this->load->library('form_validation');
        if(!is_null($id)){
            $id = intval($id);
            if ($id > 0){
                $this->data['id'] = $id;
            }else{
                $id = 0;
            }
        }
        $flag = FALSE;
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $name      = trim($this->input->post('name'));
            $id_parent = $this->input->post('check_parent');

            #Validation
            $this->form_validation->set_rules('name',lang('lbl_name'),'required');
            $this->form_validation->set_rules('check_parent', lang('group_subgroup'), 'trim');
            $api_param = array(
                'name' => $name
            );
            if($id_parent !== false){
                $id_parent = $this->input->post('parent');
                $id_worker = $this->input->post('worker');
                if(isset($id_worker) && !empty($id_worker)){
                    $id_worker   = implode(',', $id_worker);
                }
                $api_param['id_parent']=$id_parent;
                $api_param['id_worker']=$id_worker;
                $this->form_validation->set_rules('worker[]', lang("lbl_chk_worker"), "required");
                $this->form_validation->set_rules('parent',lang('lbl_chk_activity'),"required");
            }
            if ($this->form_validation->run() === TRUE) {
                if($this->_edit_flag){
                    $api_param['id'] = $id;
                    $api_results     = $this->rest->put('web/v2/group/index?id_city='.$this->_id_city,$api_param,'json');
                }else{
                    $api_results        = $this->rest->post('web/v2/group/index?id_city='.$this->_id_city,$api_param,'json');
                }
                $api_status = $this->rest->status();
                if(isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS)){
                    $this->data['msg']      = $this->lang->line('group_msg_saved');
                    $flag                   = TRUE;
                    $this->data['form_data']['id']          = $id;
                    $this->data['form_data']['group_name']  = $name;
                    if($id_parent !== false){
                        $this->data['form_data']['group']   = $id_parent;
                        $this->data['form_data']['worker']  = $id_worker;
                    }
                } else{
                    $this->data['err']      = $this->lang->line('err_api_save');
                }
            }
        }
        #data
        if ($this->_edit_flag) {
            $api_param = array(
                'id'        => $id,
                'id_city'   => $this->_id_city,
            );

            $api_results    = $this->rest->get('web/v2/group/index',$api_param,'json');
            $api_status     = $this->rest->status();
            if(isset($api_results->data) AND !empty($api_results->data)){
                $data                   = $api_results->data;
                $this->data['data']     = $data;
            }
        }
        #Worker
        $worker_array   = array();
        $worker         = array();
        $api_result     = $this->rest->get('web/v2/worker/index?id_city='.$this->_id_city);
        $api_status     = $this->rest->status();
        if(isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            $worker = $api_result->data;
            foreach ($worker as $key => $value) {
                $item = $value->worker;
                $worker_array[$item->id] = $item;
            }
        }
        $this->data['worker']       = $worker;
        $this->data['worker_array'] = $worker_array;
        $this->data['group']        = $this->getGroupParent();
        $this->data['edit_flag']    = $this->_edit_flag;
        $this->data['flag']         = $flag;
        $this->layout               = false;
        $this->view                 = 'group/detail';
    }
    function loadGroup(){
        # worker by group
        $worker_by_group    = array();
        $api_param          = array(
            'id_city'       => $this->_id_city
        );
        $api_result         = $this->rest->get('web/v2/group/worker_by_group',$api_param,'json');
        if(isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            $worker_by_group = $api_result->data;
        }
        $this->data['worker_by_group'] = json_decode(json_encode($worker_by_group), true);

        $this->data['group_by_group']   = $this->getAllGroup();
        $this->data['worker']           = $this->getWorker();
        $this->data['group']            = $this->getGroupParent();
        $this->layout                   = false;
        $this->view                     = 'group/loadGroup';
    }
    function delete($id = 0) {
        $return = array();
        $sttDelete = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 0) {
                $api_param = array(
                    'id' => $id
                );
                $api_result = $this->rest->delete('web/v2/group?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $sttDelete = TRUE;
                    $return['msg_delete']    = lang('group_msg_has_been_deleted');
                } else {
                    $return['status'] = false;
                    $return['msg_delete']    = lang('err_has_an_error_when_update_database');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }

    function remove_worker($id_worker = 0, $id_group = 0) {
        $return = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id_worker = intval($id_worker);
            $id_group  = intval($id_group);
            if ($id_worker > 0 && $id_group > 0) {
                $api_param = array(
                    'id_worker' => $id_worker,
                    'id_group'  => $id_group,
                );
                $api_result = $this->rest->delete('web/v2/group/remove_worker_in_group?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    = lang('group_msg_has_been_deleted');
                } else {
                    $return['status'] = false;
                    $return['msg']    = lang('err_has_an_error_when_update_database');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }

    function getGroupParent(){
        $group          = array();
        $api_param      = array(
            'is_parent' => 1,
            'id_city'   => $this->_id_city,
        );
        $api_result     = $this->rest->get('web/v2/group/index',$api_param,'json');
        $api_status     = $this->rest->status();
        if(isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            $group = $api_result->data;
        }
        return $group;
    }
    function getWorker(){
        $worker         = array();
        $api_result     = $this->rest->get('web/v2/worker/index?id_city='.$this->_id_city);
        $api_status     = $this->rest->status();
        if(isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            $worker = $api_result->data;
        }
        return $worker;
    }
    function getAllGroup(){
        # all group
        $group_by_group     = array();
        $api_param          = array(
            'id_city'       => $this->_id_city,
            'is_parent'     => 0
        );
        $api_result     = $this->rest->get('web/v2/group/index',$api_param,'json');
        $api_status         = $this->rest->status();
        if(isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            $group_by_group = $api_result->data;
        }
        return $group_by_group;
    }
}