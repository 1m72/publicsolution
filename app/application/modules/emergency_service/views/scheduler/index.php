<div class="filter-wg" id="filter-wg">
    <span class="pull-left">
        <h4>
            &nbsp;<span class="glyphicon glyphicon-user"></span>
            &nbsp;<strong><?php echo lang('sd_lbl_list');?></strong>
        </h4>
    </span>
    <div class="drop-filter">
        <select id="filter-worktime" class="select2-worker" multiple="multiple" style="width:200px;">
            <?php if (isset($worker) AND !empty($worker)) : ?>
                <optgroup label="Worker" id="filter-worker" >
                    <?php foreach ($worker as $key => $value) : ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                    <?php endforeach; ?>
                </optgroup>
            <?php endif; ?>
            <?php if (isset($group) AND !empty($group)) : ?>
                <optgroup label="Group" id="filter-group">
                    <?php foreach ($group as $key => $value) : ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                    <?php endforeach; ?>
                </optgroup>
            <?php endif; ?>
        </select>
    </div>
</div>
<div id="scheduler" style="clear:both">
</div>
<script id="event-template" type="text/x-kendo-template">
        <div class = "template-publicsolution" style='background-color: #: color #; '>#: title #</div>
</script>
<script type="text/javascript">
$(function() {
    var date_change;
    var action_change;
    var worker;
    var worktime=133;
    var dataWorktime=[];
    var arrWorker = [];
    var arrGroup = [];
    function refresh_scheduler(target) {
        $("#scheduler").data("kendoScheduler").dataSource.read();
        $("#scheduler").data('kendoScheduler').refresh();
    }
    init_modal('#modal_scheduler_detail');
    var schedulerControl = $("#scheduler").data("kendoScheduler");
    var _dataSource = new kendo.data.SchedulerDataSource({
                      batch: true,
                        transport: {
                            read: {
                                type: 'GET',
                                url:'<?php echo create_url("emergency_service/scheduler/list_scheduler");?>',
                                dataType: "json"
                            },
                            parameterMap: function(options, operation) {
                                if (operation=="read") {
                                    var arrWorker_length = arrWorker.length;
                                    var arrGroup_length  = arrGroup.length;
                                   if(action_change==true){
                                        if(arrWorker_length>0 || arrGroup_length>0){
                                            return { date_change: date_change,worker_id: arrWorker,group_id:arrGroup };
                                        }
                                        else{
                                            return { date_change: date_change };
                                        }
                                    }
                                    else{
                                        if(arrWorker_length>0 || arrGroup_length>0){
                                            return {worker_id: arrWorker,group_id:arrGroup };
                                        }
                                    }
                                }
                            }
                        },
                        schema: {
                            model: {
                                fields: {
                                    id: { from: "id", type: "number" },
                                    title: { from: "title", defaultValue: "dsvđ", validation: { required: true } },
                                    start: { type: "datetime", from: "starttime" },
                                    end: { type: "datetime", from: "endtime" },
                                    worker_id: { from: "worker_id" },
                                    group_id: { from: "group_id" },
                                    des: { from: "des" },
                                    color:{from:"color"}
                                }
                            }
                        },
                    });
    var _addScheduler = function(e) {
            e.preventDefault();
            var starttime = e.event.start;
            var endtime = e.event.end;
            var date_now = '<?php echo $date_now;?>';

            date_now = date_now.split(" ");
            var ymd = date_now[0].split("-");
            var his = date_now[1].split(":");
            date_now = new Date(ymd[0],ymd[1],ymd[2],his[0],his[1],his[2]);
            date_now = date_now.getTime();
            starttime_obj = new Date(starttime.getFullYear(), starttime.getMonth()+1,starttime.getDate(),starttime.getHours(), starttime.getMinutes(),starttime.getSeconds());
            start = starttime_obj.getTime();
            check_day = (start - date_now) / (1000*24 * 60 * 60);

            if(check_day>5){
                $('body').find('.bootstrap-growl').remove();
                $.ajax({
                    type: 'GET',
                    url:'<?php echo create_url("emergency_service/scheduler/add");?>',
                    data: {starttime: starttime,endtime:endtime},
                    success: function(data){
                        open_modal('#modal_scheduler_detail', data, '<?php echo lang("sd_schedule_detail");?>');
                    },
                    error: function(err){
                        alert(_alert_error);
                    }
                });
            }
            else{
                $('body').find('.bootstrap-growl').remove();
                $.bootstrapGrowl('<?php echo lang("sd_msg_create");?>', {
                  ele: 'body',
                  type: 'warning',
                  offset: {from: 'top', amount: 100},
                  align: 'center',
                  width: 'auto',
                  delay: 9000,
                  allow_dismiss: true,
                  stackup_spacing: 10
                });
            }
        }
    var _editScheduler = function(e)
        {
            e.preventDefault();
            var scheduler_id = e.event.id;
            $.ajax({
                type: 'GET',
                url:'<?php echo create_url("emergency_service/scheduler/edit");?>',
                data: {id: scheduler_id},
                success: function(data){
                    open_modal('#modal_scheduler_detail', data, '<?php echo lang("sd_schedule_detail");?>');
                },
                error: function(err){
                    alert(_alert_error);
                }
            });
        }
    var _removeScheduler = function(e){
            e.preventDefault();
            var scheduler_id = e.event.id;
            $.ajax({
                type: 'GET',
                url: '<?php echo create_url("emergency_service/scheduler/delete");?>',
                data: {id:scheduler_id},
                success: function(data){
                    refresh_scheduler('#scheduler');
                },
                error: function(err){
                    alert(_alert_error);
                }
            });
        }
    var _navigationScheduler = function(e) {
        //var a = kendo.format("{0:d}", e.date);
        if(e.action=="next" || e.action =="previous"){
                date_change = e.date;
                action_change = true;
                refresh_scheduler('#scheduler');
            }
        }
    var _resourceScheduler =[{field: "id",dataSource: [{text:"dfdfd",value: worktime , color: '#b55b14'},]}];

    $("#scheduler").kendoScheduler({
        //date: new Date("2014/05/25"),   //value default for start
        //startTime: new Date("2013/6/13 07:00 AM"),
        height: 600,
        views: [
            { type: "day", eventTemplate: $("#event-template").html(),},
            { type: "workWeek", eventTemplate: $("#event-template").html(),},
            { type: "week", eventTemplate: $("#event-template").html(),selected: true,},
            { type: "month", eventTemplate: $("#event-template").html(),},
            "agenda"
        ],

        messages: {
            cancel: "<?php echo lang('btn_no'); ?>",
            destroy: "<?php echo lang('btn_yes'); ?>",
            deleteWindowTitle: "<?php echo lang('delete_confirm'); ?>",
        },

        dataSource: _dataSource,
        editable: {
            move: false,
            resize: false,
            confirmation: "<?php echo lang('sd_delete_confirmation'); ?>"
        },
        add: _addScheduler,
        edit: _editScheduler,
        remove: _removeScheduler,
        navigate: _navigationScheduler,
        resources:_resourceScheduler
    });
    $("#filter-worktime").multiselect({
        includeSelectAllOption:true,
        numberDisplayed: 3,
        selectAllText: "Select all",
        selectAllValue: 'multiselect-all',
        enableFiltering: true,
        maxHeight: 300,
        minWidth: 500,
        buttonWidth:200,
        filterPlaceholder: '<?php echo lang("sd_search"); ?>',
    });
    $('.select2-worker').select2({ width: 'resolve' });

     $("#filter-worktime").change(function(){
        var optionSelected = $(this).find("option");
        optionSelected.each(function(index){
            var value = optionSelected.eq(index).attr('value');
            var lablegroup = optionSelected.eq(index).parent().attr("id");
            if(optionSelected.eq(index).is(':selected')){
                if(lablegroup=="filter-worker"){
                    if($.inArray(value, arrWorker) <= -1){
                        arrWorker.push(value);
                    }
                }
                else if(lablegroup=="filter-group"){
                    if($.inArray(value, arrGroup) <= -1){
                        arrGroup.push(value);
                    }
                }
            }else{
                if(lablegroup=="filter-worker"){
                    if($.inArray(value, arrWorker) > -1){
                        var i = arrWorker.indexOf(value);
                        arrWorker.splice(i, 1);
                    }
                }
                else if(lablegroup=="filter-group"){
                    if($.inArray(value, arrGroup) > -1){
                        var i = arrGroup.indexOf(value);
                        arrGroup.splice(i, 1);
                    }
                }
            }
        });
        refresh_scheduler('#scheduler');
    });

});
</script>

