<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("emergency_service/scheduler/edit")."?id=".$id;
    } else {
        $form_link = create_url("emergency_service/scheduler/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
    #start format date from  2014-06-27 02:00:00 to 06/27/2014 02:00 AM
    if(isset($scheduler->starttime)){
        $starttime_detail = new DateTime($scheduler->starttime);
        $starttime_detail = $starttime_detail->format('m/d/Y g:i a');
    }
    else{
        $starttime_detail = new DateTime($starttime);
        $starttime_detail = $starttime_detail->format('m/d/Y g:i a');
    }

    if(isset($scheduler->endtime)){
        $endtime_detail = new DateTime($scheduler->endtime);
        $endtime_detail = $endtime_detail->format('m/d/Y g:i a');
    }
    else{
        $endtime_detail = new DateTime($endtime);
        $endtime_detail = $endtime_detail->format('m/d/Y g:i a');
    }
    #end format

?>

<script type="text/javascript">
    function refresh_scheduler(target) {
        $("#scheduler").data("kendoScheduler").dataSource.read();
        $("#scheduler").data('kendoScheduler').refresh();
    }
    <?php if (isset($flag) AND ($flag) ) : ?>
        setTimeout(
            function(){
                close_modal('#modal_scheduler_detail');
                <?php echo $callback ? "$callback();" : "refresh_scheduler('#scheduler');"; ?>
            },
            timeout_dialog
        );
    <?php endif; ?>
    $(function(){
        <?php if(isset($scheduler->edit) AND !($scheduler->edit)):?>
            $('.readonly').hide();
        <?php endif; ?>
        $("#starttime").kendoDateTimePicker();
        $("#endtime").kendoDateTimePicker();
        $('.select2').select2({ width: 'resolve' });
        $('#frm_scheduler_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#dialog').html(res);
                },
                error: function(err) {}
            });
        });
    });
</script>
<div id="dialog">
    <form class="form-horizontal" id="frm_scheduler_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-4 control-label" for="titile"><?php echo lang('sd_lbl_title');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-5">
                <input id="title" name="title" value="<?php echo set_value('title', isset($scheduler->title) ? $scheduler->title : ''); ?>" type="text" placeholder="<?php echo  lang('sd_lbl_title')?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error("title");?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="starttime"><?php echo lang('sd_lbl_starttime');?></label>
            <div class="col-md-5">
                <input id="starttime" name="starttime" value="<?php echo set_value('starttime', isset($starttime_detail)? $starttime_detail:'') ?>"  style="width: 250px;" data-role="datetimepicker">
                <div class="help-block ps_err">
                    <?php echo form_error("starttime");?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="endtime"><?php echo lang('sd_lbl_endtime');?></label>
            <div class="col-md-5">
                <input id="endtime" name="endtime" value="<?php echo set_value('endtime', isset($endtime_detail)? $endtime_detail:'') ?>"  style="width: 250px;" data-role="datetimepicker">
                <div class="help-block ps_err">
                    <?php echo form_error("endtime");?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="worker_id"><?php echo lang('sd_lbl_worker');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-5" >
                <select id="worker_id" name="worker_id[]" multiple="true" class="select2" style="width: 250px;" placeholder="<?php echo lang('sd_lbl_worker');?>">
                        <?php if (isset($worker) AND !empty($worker)) : ?>
                            <?php foreach ($worker as $key => $value) : ?>
                                <option value="<?php echo $key; ?>" <?php echo set_select('worker_id', $key, (isset($worker_edit_id) AND in_array($key, $worker_edit_id) )); ?>><?php echo $value; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                </select>
                <div class="help-block ps_err">
                    <?php echo form_error('worker_id'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="group_id"><?php echo lang('sd_lbl_group');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-5">
                <select id="group_id" name="group_id[]" multiple="true" data-placeholder="<?php echo lang('sd_lbl_group');?>" class="select2" style="width: 250px;">
                        <?php if (isset($group) AND !empty($group)) : ?>
                        <?php foreach ($group as $key => $value) : ?>
                    <option value="<?php echo $key; ?>" <?php echo set_select('group_id', $key, (isset($group_edit_id) AND in_array($key, $group_edit_id) )); ?>><?php echo $value; ?></option>
                        <?php endforeach; ?>
                        <?php endif; ?>
                </select>
                <div class="help-block ps_err">
                    <?php echo form_error('group_id'); ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="des"><?php echo lang('sd_lbl_des');?></label>
            <div class="col-md-5">
                <textarea id="des" name="" class="form-control table-textarea" rows="3" value="<?php echo set_value('des', isset($scheduler->des) ? $scheduler->des : ''); ?>" placeholder="<?php echo lang('sd_lbl_des') ?>"></textarea>
                <div class="help-block ps_err">
                    <?php echo form_error('des'); ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="btn_save"></label>
            <div class="col-md-8">
                <button class="btn btn-primary readonly"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a class="btn btn-default" href="javascript:close_modal('#modal_scheduler_detail');"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>
    </form>
</div>
<script id="delete-confirmation" type="text/x-kendo-template">
    <div class="row">
        <span class="col-md-2"></span>
        <span class="col-md-8">
            <p class="delete-message"><?php echo lang('msg_question_delete');?></p>
        </span>
        <span class="col-md-2"></span>
    </div>
    <div class="row">
        <span class="col-md-7"></span>
        <span class="col-md-5">
            <span class="pull-right">
                <button class="delete-confirm btn btn-danger"><?php echo lang('btn_yes');?></button>
                <a href="#" class="delete-cancel btn btn-default"><?php echo lang('btn_no');?></a>
            </span>
        </span>
    </div>
</script>