<?php
    /*if (isset($flag) AND ($flag)) {
        echo "<pre>";var_dump($form_data);
    }*/
    /*echo "<pre>";
    var_dump($group);*/
?>
<div id="frm_group_detail_wraper">
    <?php
        $edit_flag = isset($edit_flag) ? $edit_flag : false;
        if ( $edit_flag AND isset($id) AND ($id > 0) ) {
            $form_link = create_url("emergency_service/group/edit/$id");
        } else {
            $form_link = create_url("emergency_service/group/add");
        }
        $callback  = $this->input->get('callback');
        if ($callback) {
            $form_link .= "?callback=$callback";
        }

        $check_parent = (isset($data[0]->id_parent) AND ($data[0]->id_parent > 0)) ? 'checked="checked"' : '';
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $check_parent = set_checkbox('check_parent', 1);
        }
    ?>
    <script type="text/javascript">
        function reloadGroup(){
            $('#demo').html('');
            $.ajax({
                url:'<?php echo create_url("emergency_service/group/loadGroup"); ?>',
                type:'GET',
                success:function(data){
                    //data =HTML;
                    $('#demo').html(data);
                }
            });
        }
        $(function(){
            <?php if (isset($data[0]->id_parent) AND ($data[0]->id_parent == 0) ) : ?>
                $('.check_group').css({display:'none'});
            <?php endif; ?>
            $('.select2').select2({ width: 'resolve' });
            $('#frm_group_detail').submit(function(e){
                e.preventDefault();
                i = $(this);
                $.ajax({
                    url: i.attr('action'),
                    type: 'POST',
                    data: i.serialize(),
                    success: function(res) {
                        $('#frm_group_detail_wraper').html(res);
                        reloadGroup();
                    },
                    error: function(err) {}
                });
            });
            $('[name="check_parent"]').bootstrapSwitch();
            $('[name="check_parent"]').on('switchChange.bootstrapSwitch', function(event, state) {
                if(state){
                    $('.worker_wraper').slideDown();
                } else{
                    $('.worker_wraper').slideUp();
                }
            });
            $('#select_worker').keyup(function(e){
                e.preventDefault();
                var text_search_worker = $('#select_worker').val();
                var listCheckboxWorker = $('#worker .checkbox');
                $.each(listCheckboxWorker,function(index){
                    var item_worker = listCheckboxWorker.eq(index);
                    var text = item_worker.text().trim();
                    var check = false;
                    if(typeof text!= 'undefined' && text!=null){
                        var re = new RegExp(text_search_worker, "ig");
                        var check = text.match(re);
                        if(typeof check !='undefined' && check!=null && check !=''){
                            item_worker.css({display:'block'});
                        }else{
                            item_worker.css({display:'none'});
                        }
                    }
                });
            });
            $('#btn_cancel').click(function(){
                $(".g-detail").css({display:"none"});
            });
        });
    </script>
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo lang('group_lbl_detail'); ?></h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" id="frm_group_detail" action="<?php echo $form_link; ?>">
                <?php if (isset($msg)) : ?>
                <div class="alert alert-success"><?php echo $msg; ?></div>
                <?php endif; ?>
                <?php if (isset($err)) : ?>
                <div class="alert alert-danger"><?php echo $err; ?></div>
                <?php endif; ?>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="name"><?php echo lang('group_name'); ?><span class="star">&nbsp;*</span></label>
                    <div class="col-md-6">
                        <input id="name" name="name" value="<?php echo set_value('name',isset($data[0]->name) ? $data[0]->name : '');?>" type="text" placeholder="<?php echo lang('group_name_placeholder'); ?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('name'); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group check_group" >
                    <label class="col-md-3 control-label" for="name" style=""><?php echo lang('group_subgroup');?></label>
                    <div class="col-md-6">
                        <input name="check_parent" type="checkbox" value="1" <?php echo $check_parent; ?> />
                    </div>
                </div>
                <div class="worker_wraper" style="display: <?php echo !empty($check_parent) ? 'block' : 'none'; ?>;">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="id_parent"><?php echo lang('group_parent');?><span class="star">&nbsp;*</span></label>
                        <div class="col-md-8">
                            <select id="parent" name="parent" class="select2" style="width: 305px;" data-name="parent">
                                <option value=""><?php echo lang('group_lbl_select_parent_group');?></option>
                                <?php if (isset($group) AND !empty($group)) : ?>
                                <?php foreach ($group as $group_key => $group_value) : ?>
                                    <option value="<?php echo $group_value->id; ?>" <?php echo set_select('parent', $group_value->id); ?> <?php if(isset($data[0]->id_parent) && $group_value->id == $data[0]->id_parent) echo 'selected="selected"'; ?> ><?php echo $group_value->name; ?></option>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                            <div class="help-block ps_err err_worker">
                                <?php echo form_error('parent'); ?>
                            </div>

                            <?php if (isset($data[0]->id_parent) AND (intval($data[0]->id_parent) == 0) ) : ?>
                            <div class="help-block alert alert-warning" style="display: block; font-size: 12px; padding: 5px 10px; overflow: auto;">
                                <span class="pull-left glyphicon glyphicon-warning-sign" style="font-size: 15px; margin-right: 10px; "></span>
                                <span><?php if(isset($data)) echo str_replace('[GROUP_NAME]', $data[0]->name, lang('group_waring_change_group')); ?></span>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('group_worker'); ?><span class="star">&nbsp;*</span></label>
                        <div class="col-md-8" id="worker">
                            <div class="search">
                                <input id="select_worker" placeholder="<?php echo lang('group_search_placeholder'); ?>" style="float: left; margin-right: 7px;" class="form-control input-md" value="">
                            </div>
                            <div style="height: 200px; overflow: auto; border: 1px solid #ECECEC; padding: 5px 10px; margin-top: 5px;" id="checkWorker">
                                <?php if(isset($worker) AND !empty($worker)): ?>
                                    <?php foreach ($worker as $worker_key => $worker_value): $a = $worker_value->worker; ?>
                                        <div class="checkbox">
                                            <input class="worker" type="checkbox" data-name="worker[]" name="worker[]" value="<?php echo $a->id ?>" <?php echo set_checkbox('worker[]', $a->id); ?> <?php if(isset($data[0]->id_worker) && in_array($a->id, $data[0]->id_worker)) echo 'checked="checked"';?> id="id_worker_<?php echo $a->id; ?>"/>
                                            <label for="id_worker_<?php echo $worker_key ?>" class="lbl_worker"><?php echo $a->first_name.'&nbsp;'.$a->last_name;?></label>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif; ?>
                            </div>
                            <div class="help-block ps_err err_worker">
                                <?php echo form_error('worker[]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="btn_save"></label>
                    <div class="col-md-6">
                        <button id="btn_save" name="btn_save" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                        <a id="btn_cancel" name="btn_cancel"  class="btn btn-default"><?php echo lang('btn_cancel');?></a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>