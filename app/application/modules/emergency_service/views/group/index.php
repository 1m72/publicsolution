<div class="row-fluid">
    <div class="col-xs-4 col-sm-4 col-md-3" id="group_wraper">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="group-h">
                    <span><?php echo lang('group_lbl'); ?></span>
                    <span class="pull-right">
                        <a class="sys_modal_detail btn btn-primary btn-sm btn-add"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new');?></a>
                    </span>
                </div>
            </div>
            <div class="panel-body">
                <div class="group-w">
                    <div class="group-body">
                        <ul class="nav nav-list list" id="demo">
                            <li><div><label class="tree-toggler nav-header"><?php echo lang('lbl_list_worker');?></label></div>
                                <ul class="nav nav-list tree">
                                    <?php if(isset($worker) AND !empty($worker)): ?>
                                    <?php foreach ($worker as $worker_key => $worker_value): $a = $worker_value->worker;?>
                                        <li><a class="test" href="#" id="worker_<?php echo $a->id; ?>" data-id="<?php echo $a->id; ?>"><?php echo $a->first_name.'&nbsp;'.$a->last_name;?></a></li>
                                    <?php endforeach;?>
                                    <?php endif; ?>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <?php if(isset($group) AND !empty($group)) : ?>
                            <?php foreach($group as $group_key => $group_value): ?>
                            <li class="g-parent" id= "group_<?php echo $group_value->id; ?>" data-id="<?php echo $group_value->id; ?>">
                                <div class="d-parent">
                                    <label class="pull-left tree-toggler nav-header" id= "labelGroup_<?php echo $group_value->id; ?>"><?php echo $group_value->name; ?> </label>
                                    <a class='pull-right sys_modal_detail delete_group'><span class='glyphicon glyphicon-remove'></span>&nbsp;<?php echo lang('btn_remove')?></a>
                                    <a class='pull-right sys_modal_detail edit' data-id="<?php echo  $group_value->id; ?>"><span class='glyphicon glyphicon-pencil'></span>&nbsp;<?php echo lang('btn_edit')?></a>
                                </div>
                                <div class="clearfix"></div>
                                <ul class="nav nav-list tree u-child" id="parent<?php echo $group_value->id; ?>">
                                    <?php if(isset($group_by_group) AND !empty($group_by_group)) : ?>
                                    <?php foreach($group_by_group as $group_by_group_key => $group_by_group_value): ?>
                                    <?php if($group_by_group_value->id_parent == $group_value->id) : ?>
                                    <li class="g-child" id="subgroup_<?php echo $group_by_group_value->id; ?>" data-id="<?php echo $group_by_group_value->id; ?>">
                                        <div class="d-child">
                                            <label class="pull-left tree-toggler nav-header" id= "labelGroup_<?php echo $group_value->id; ?>" style="margin-right: -30px !important;"><?php echo $group_by_group_value->name; ?></label>
                                            <a class='pull-right sys_modal_detail delete_group'><span class='glyphicon glyphicon-remove'></span>&nbsp;<?php echo lang('btn_remove')?></a>
                                            <a class='pull-right sys_modal_detail edit' data-id="<?php echo $group_by_group_value->id; ?>"><span class='glyphicon glyphicon-pencil'></span>&nbsp;<?php echo lang('btn_edit')?></a>
                                        </div>
                                        <div class="clearfix"></div>
                                        <ul class="nav nav-list tree u-worker">
                                            <?php if (isset($worker_by_group[$group_by_group_value->id]['worker']) AND is_array($worker_by_group[$group_by_group_value->id]['worker']) AND !empty($worker_by_group[$group_by_group_value->id]['worker'])) : ?>
                                            <?php foreach ($worker_by_group[$group_by_group_value->id]['worker'] as $key => $value) : ?>
                                            <?php if (isset($value['first_name'])) : ?>
                                                <li class="l-worker" id="worker_<?php echo $value['id']; ?>" data-id="<?php echo $value['id']; ?>">
                                                    <label class="tree-toggler nav-header nav-tree">
                                                        <?php echo $value['first_name'] . '&nbsp;' . $value['last_name']; ?>
                                                        <a class='pull-right sys_modal_detail delete'><span class='glyphicon glyphicon-remove'></span>&nbsp;<?php echo lang('btn_remove')?></a>
                                                    </label>
                                                </li>
                                            <?php endif; ?>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </ul>
                                    </li>
                                    <?php endif;?>
                                    <?php endforeach;?>
                                    <?php endif;?>
                                </ul>
                            </li>
                            <?php endforeach;?>
                            <?php endif;?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-8 col-sm-8 col-md-9" id="group_detail_wraper">
        <div class="g-detail" style="display:none;">
            <?php echo $this->load->view('group/detail',TRUE); ?>
        </div>
        <div class="g-delete" style="display:none;">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo lang('delete_confirm'); ?></h3>
                </div>
                <div class="panel-body">
                    <div class="d-delete">
                        <div class="row">
                            <span class="col-md-2"></span>
                            <span class="col-md-8">
                                <p class="delete-message"><?php echo lang('group_msg_question_delete'); ?></p>
                            </span>
                            <span class="col-md-2"></span>
                        </div>
                    <div class="row">
                        <span >
                            <button class="delete-confirm btn btn-danger d-ok"><?php echo lang('btn_yes');?></button>
                            <a href="#" class="delete-cancel btn btn-default d-cacel"><?php echo lang('btn_no');?></a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="msg_success">
             <div class="help-block alert alert-success" style="display: block; font-size: 12px; padding: 5px 10px; overflow: auto;">
                <span class="pull-left glyphicon glyphicon-ok" style="font-size: 15px; margin-right: 10px; "></span>
                <span><?php echo lang('msg_delete_success'); ?></span>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('label.tree-toggler').click(function () {
            $(this).parent().parent().children('ul.tree').toggle(300);
        });
        $('.btn-add').click(function(e){
            e.preventDefault();
            $.ajax({
                type: 'GET',
                url: '<?php echo create_url("emergency_service/group/add");?>',
                success: function(res) {
                    $(".g-detail").html(res).css({display:"block"});
                    $('.g-delete').css({display:"none"});
                    $('.msg_success').css({display:"none"});
                },
                error: function(err) {}
            });
        });
       $('.edit').click(function(e){
            e.preventDefault();
            var $this = $(this);
            $.ajax({
                type: 'GET',
                url: '<?php echo create_url("emergency_service/group/edit");?>/'+ $this.attr('data-id'),
                success: function(res) {
                    $(".g-detail").html(res).css({display:"block"});
                    $('.g-delete').css({display:"none"});
                    $('.msg_success').css({display:"none"});
                },
                error: function(err) {}
            });
        });
        $('.delete').click(function(){
            $('.g-delete').css({display:"block"});
            $(".g-detail").css({display:"none"});
            $('.msg_success').css({display:"none"});
            workerDelete = $(this).parent().parent();
            var w = workerDelete.attr('data-id');
            var g = $(this).parent().parent().parent().parent().attr('data-id');
            $('.d-ok').click(function(e){
                e.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo create_url("emergency_service/group/remove_worker");?>/'+w+'/'+g,
                    success: function(res) {
                        $('.g-delete').css({display:"none"});
                        $('.msg_success').css({display:"block"});
                        workerDelete.remove();
                    },
                    error: function(err) {}
                });
            });
        });
        $('.delete_group').click(function(){
            groupDelete = $(this).parent().parent();
            var g = groupDelete.attr('data-id');
            $('.g-delete').css({display:"block"});
            $(".g-detail").css({display:"none"});
            $('.msg_success').css({display:"none"});
            $('.d-ok').click(function(e){
                e.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo create_url("emergency_service/group/delete");?>/'+g,
                    success: function(res) {
                        $('.g-delete').css({display:"none"});
                        $('.msg_success').css({display:"block"});
                        groupDelete.remove();
                    },
                    error: function(err) {}
                });
            });
        });
        $('.d-cacel').click(function(){
            $('.g-delete').css({display:"none"});
        });
    });
</script>