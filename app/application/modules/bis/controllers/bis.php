<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Bis extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->layout = 'layouts/bis';
    }

    function index() {
        $this->view = 'index';
    }

    function index2() {
        $this->view = 'index2';
    }
}
