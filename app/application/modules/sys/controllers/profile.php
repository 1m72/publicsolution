<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Profile Controller
 * @author Chien Tran (tran.duc.chien@kloon.vn)
 * @created 17 Dec 2013
 */

class Profile extends MY_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();

        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        # Breakcrumbs
        $this->breadcrumb->append_crumb(lang('crumb_user'), create_url('sys/user'));

        # Language
        $this->load->language('user');
        $this->load->language('profile');

        # id_city
        $this->_id_city = current_city();
    }

    function index() {
        # Load assets
        $this->load_assets('bootstrap-switch/bootstrap-switch.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('bootstrap-switch/bootstrap-switch.js', ASSET_JS);

        # Language
        $this->load->language('city');

        # Formvalidation
        $this->load->library('form_validation');

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        }

        # View
        $this->view = 'home/comming';
    }
}