<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @author nguyen.quynh.chi@kloon.vn
 * @copyright 2013
 */

class Company extends MY_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();

        # Language
        $this->load->language('company');

        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        # Breakcrumbs
        $this->breadcrumb->append_crumb($this->lang->line('cpn_lbl_company'), create_url('sys/company'));

        # id_city
        $this->_id_city = current_city();

        # limit of grid
        $this->_limit   = config_item('grid_limit');
    }

    function index() {
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_COMPANY);
            return;
        }
        # Grid columns width
        if (current_language(FALSE) == 'de') {
            $column_properties = array(
                'order_w'             => 50,
                'id_w'                => 100,
                'name_w'              => 200,
                'city_w'              => 150,
                'updated_w'           => 120,
                'created_w'           => 100,
                'option_w'            => 200,
            );
        } else {
            $column_properties = array(
                'order_w'             => 50,
                'id_w'                => 100,
                'name_w'              => 200,
                'city_w'              => 150,
                'updated_w'           => 100,
                'created_w'           => 100,
                'option_w'            => 140,
        );
        }
        $this->data['column_properties'] = json_encode($column_properties);
        # View
        $this->load_assets('grid_view.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        $this->data['limit'] = $this->_limit;
        $this->view          = 'company/index';
        $this->layout        = 'layouts/module';
    }

    function add() {
        $this->_update();
    }

    function edit($id = 0) {
        $this->_edit_flag = true;
        $this->_update($id);
    }

    function _update($id = NULL) {
        if (!$this->input->is_ajax_request()) {
            redirect(create_url('sys/company'));
        }

        # Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }

        $flag = FALSE;

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            # Post data
            $name        = trim($this->input->post('name'));
            $description = trim($this->input->post('description'));

            # Validation
            $this->form_validation->set_rules('name', $this->lang->line('cpn_lbl_name'), 'required');
            $this->form_validation->set_rules('description', $this->lang->line('cpn_lbl_description'), 'trim');
            if ($this->form_validation->run() === TRUE) {
                $api_param   = array(
                    'id_company'  => $id,
                    'name'        => $name,
                    'id_city'     => $this->_id_city, # $city
                    'description' => $description,
                );

                if ($this->_edit_flag) {
                    $api_param['id_company']     = $id;
                    $api_results = $this->rest->put('web/company?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_results = $this->rest->post('web/company?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status  = $this->rest->status();
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = $this->lang->line('cpn_msg_company_information_has_been_saved');
                    $flag = TRUE;
                } else {
                    write_log('Save company error_Data: '.json_encode($api_param), 'app_company');
                }
            }
        }

        # Data
        if ($this->_edit_flag) {
            $api_param = array(
                'id_company' => $id,
                'id_city'    => $this->_id_city
            );
            $api_results = $this->rest->get('web/company', $api_param, 'json');
            $api_status  = $this->rest->status();
            $data = array();
            if ( isset($api_results->data) AND !empty($api_results->data) ) {
                $data = $api_results->data;
                $data = $data[0];
            }
            $this->data['company'] = $data;
        }

        # City
        $city       = array();
        $api_result = $this->rest->get('web/city/indexall');
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $city[$value->id] = $value;
            }
        }
        $this->data['city'] = $city;

        $this->data['flag']      = $flag;
        $this->data['edit_flag'] = $this->_edit_flag;
        $this->layout            = false;
        $this->view              = 'company/detail';
    }

    function delete($id = 0) {
        $return = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 0) {
                $api_param = array(
                    'id_company' => $id
                );
                $api_results = $this->rest->delete('web/company?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    =  $this->lang->line('cpn_msg_company_has_been_deleted');
                } else {
                    $return['status'] = false;
                    $return['msg']    = $this->lang->line('err_has_an_error_when_update_database');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }

    /**
     * @description : get company by city
     * @function    : companybycity
     * @author      : Le Thi Nhung(le.nhung@kloon.vn)
     * create       : 2013-11-27
     * @param      : id_city
     **/
    function companybycity(){
        #companies by city
        $this->load->helper('form');

        $id_city   = intval($this->input->get('id_city'));
        $companies = array(
            '' => lang('lbl_select_company'),
        );
        if($id_city!=0){
            $api_param  = array();
            $api_result_companies = $this->rest->get('web/company/index?id_city='.$id_city);
            $api_status_companies = $this->rest->status();
            if ( isset($api_result_companies->status) AND ($api_result_companies->status == REST_STATUS_SUCCESS) AND (isset($api_result_companies->data)) AND (!empty($api_result_companies->data)) ) {
                foreach ($api_result_companies->data as $key => $value) {
                    $companies    += array(
                            $value->id =>$value->name,
                    );
                }
            }
        }

        echo form_dropdown('company',$companies, 0, 'class="select2" style="width: 245px;"');
    }
    function add_company($task=''){
        $task = $task;
        if ($this->input->is_ajax_request()) {
            # Libs
            $this->load->library('form_validation');
            $flag = FALSE;
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                # Post data
                $name        = trim($this->input->post('name'));
                $description = trim($this->input->post('description'));
                # Validation
                $this->form_validation->set_rules('name', $this->lang->line('cpn_lbl_name'), 'required');
                $this->form_validation->set_rules('description', $this->lang->line('cpn_lbl_description'), 'trim');
                if ($this->form_validation->run() === TRUE) {
                    $api_param   = array(
                        'id_company'  => '',
                        'name'        => $name,
                        'id_city'     => $this->_id_city, # $city
                        'description' => $description,
                    );
                    $api_results = $this->rest->post('web/company?id_city='.$this->_id_city, $api_param, 'json');
                    $api_status  = $this->rest->status();
                    if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                        $this->data['msg'] = $this->lang->line('cpn_msg_company_information_has_been_saved');
                        $flag   = TRUE;
                        $dialog = "company";
                        $this->data['dialog']    = $dialog;
                    }
                }
            }
            # Data
            $this->data['flag']      = $flag;
            $this->layout = false;
            $this->view   = 'company/add_company';
        } else {
            redirect(create_url('sys/'.$task));
        }
    }
}