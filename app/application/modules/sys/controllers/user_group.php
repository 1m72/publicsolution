<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User_group Controller
 * @author Le Thi Nhung (le.nhung@kloon.vn)
 * @created 2013-12-31
 */

class User_group extends MY_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();
        if (session_login(FALSE)->id != 1) {
            redirect(create_url('no_permison'));
        }
        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        # Breakcrumbs
        $this->breadcrumb->append_crumb(lang('crumb_user_group'), create_url('sys/user_group'));

        # Language
        $this->load->language('user_group');

        # id_city
        $this->_id_city = current_city();

        # limit of grid
        $this->_limit   = config_item('grid_limit');
    }

    function index() {
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_USERS_GROUP);
            return;
        }
       # Grid columns width
        if (current_language(FALSE) == 'de') {
            $column_properties = array(
                'no_width'         =>50 ,
                'updated_at_width' => 120,
                'created_at_width' => 100,
                'option_width'     =>200,
            );
        } else {
            $column_properties = array(
                'no_width'         => 50 ,
                'updated_at_width' => 100,
                'created_at_width' => 100,
                'option_width'     => 140,
            );
        }
        $this->data['column_properties'] = json_encode($column_properties);
        # View
        $this->load_assets('grid_view.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        $this->data['limit'] = $this->_limit;
        $this->view          = 'user_group/index';
        $this->layout        = 'layouts/module';
    }

    function add() {
        $this->_update();
    }

    function edit($id = 0) {
        $this->_edit_flag = true;
        $this->_update($id);
    }

    function _update($id = NULL) {
        if (!$this->input->is_ajax_request()) {
            redirect(create_url('sys/user_group'));
        }

        # Libs
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }
        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            # Post data
            $name = trim($this->input->post('name'));
            # Validation
            $this->form_validation->set_rules('name',lang('lbl_groupname'), 'required');
            if ($this->form_validation->run() === TRUE) {
                $api_param   = array(
                    'name'            => $name,
                );
                if ($this->_edit_flag) {
                    $api_param['id_group'] = $id;
                    $api_results = $this->rest->put('web/user_group?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_results = $this->rest->post('web/user_group?id_city='.$this->_id_city, $api_param, 'json');
                }

                $api_status  = $this->rest->status();
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = $this->lang->line('msg_usergroup_saved');
                    $flag = TRUE;
                }
                else {
                    $this->data['err'] = lang('err_api_save');
                }
            }
        }

        # Data
        if ($this->_edit_flag) {
            $api_param = array(
                'id_city'    => $this->_id_city,
                'id_group' => $id,

            );
            $api_results = $this->rest->get('web/user_group', $api_param, 'json');
            $api_status  = $this->rest->status();
            if ( isset($api_results->data) AND !empty($api_results->data) ) {
                $data               = $api_results->data;
                $this->data['data'] = $data[0];
            }
        }
        $this->data['edit_flag'] = $this->_edit_flag;
        $this->data['flag']      = $flag;
        $this->layout = false;
        $this->view   = 'user_group/detail';
    }

    function delete($id = 0) {
        $return = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 0) {
                $api_param = array(
                    'id' => $id
                );
                $api_results = $this->rest->delete('web/user_group?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    = lang('msg_usergroup_deleted');
                } else {
                    $return['status'] = false;
                    $return['msg']    = lang('err_api_save');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }
    function list_usergroup(){
        $this->load->helper('form');
        $groups         = array();
        $api_result     = $this->rest->get('web/user_group/index?id_city='.$this->_id_city);
        $api_status     = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $groups      += array(
                    $value->id =>$value->name,
                );
            }
        }
        echo form_dropdown('group', $groups, 0, 'class="select2" style="width: 250px"');
    }
}