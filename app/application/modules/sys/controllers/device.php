<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author nguyen.quynh.chi@kloon.vn
 * @copyright 2013
 */

class Device extends MY_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();

        # Language
        $this->load->language('device');

        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        # Breakcrumbs
        $this->breadcrumb->append_crumb($this->lang->line('dv_lbl_device'), create_url('sys/device'));

        # id_city
        $this->_id_city = current_city();

        # limit of grid
        $this->_limit   = config_item('grid_limit');
    }

    function index() {
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_DEVICE);
            return;
        }
        # Grid columns width
        if (current_language(FALSE) == 'de') {
            $column_properties = array(
                'order_w'             => 50,
                'id_w'                => 100,
                'actived_w'           => 80,
                'updated_w'           => 120,
                'created_w'           => 100,
                'option_w'            => 200,
            );
        } else {
            $column_properties = array(
                'order_w'             => 50,
                'id_w'                => 100,
                'actived_w'           => 70,
                'updated_w'           => 100,
                'created_w'           => 100,
                'option_w'            => 140,
            );
        }
        $this->data['column_properties'] = json_encode($column_properties);

        $this->load_assets('grid_view.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        # View
        $this->data['limit'] = $this->_limit;
        $this->view          = 'device/index';
        $this->layout        = 'layouts/module';
    }

    function add() {
        $this->_update();
    }
}