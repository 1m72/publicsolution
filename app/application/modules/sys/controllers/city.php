<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * City Controller
 * @author Le Thi Nhung <le.nhung@kloon.vn>
 * @created 2013-11-15
 */

class City extends MY_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();

        if (session_login(FALSE)->id != 1) {
            redirect(create_url('no_permison'));
        }

        # Language
        $this->load->language('city');

        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        # Breakcrumbs
        $this->breadcrumb->append_crumb(lang('crumb_city'), create_url('sys/city'));

        # id_city
        $this->_id_city = config_item('id_city');

        # limit of grid
        $this->_limit   = config_item('grid_limit');
    }

    function index() {
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_CITY);
            return;
        }
        if (current_language(FALSE) == 'de') {
            $column_properties = array(
                'no_width'            =>50 ,
                'city_database_width' =>110 ,
                'city_storage_width'  =>130 ,
                'updated_at_width'    =>120,
                'created_at_width'    =>100,
                'option_width'        =>200,
            );
        } else {
            $column_properties = array(
                'no_width'            =>50 ,
                'city_database_width' =>100 ,
                'city_storage_width'  =>100 ,
                'updated_at_width'    =>100,
                'created_at_width'    =>100,
                'option_width'        =>140,
            );
        }
        $this->data['column_properties'] = json_encode($column_properties);
        # View
        $this->load_assets('grid_view.js', ASSET_JS);
    	if($this->data['current_language']=='de'){
        	$this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        $this->data['limit'] = $this->_limit;
        $this->view          = 'city/index';
        $this->layout        = 'layouts/module';
    }

    function add() {
        $this->_update();
    }

    function edit($id = 0) {
        $this->_edit_flag = true;
        $this->_update($id);
    }

    function _update($id = NULL) {
        $this->load_assets('bootstrap-switch/bootstrap-switch.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('bootstrap-switch/bootstrap-switch.js', ASSET_JS);
        # Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }
        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            # Post data
            $name               = trim($this->input->post('name'));
            $passcode           = trim($this->input->post('passcode'));

            #database config
            $hostname           = trim($this->input->post('hostname'));
            $username           = trim($this->input->post('username'));
            $password           = trim($this->input->post('password'));
            $passconf           = $this->input->post('passconf');
            $database           = trim($this->input->post('database'));
            $port               = $this->input->post('port');
            $check_database     = $this->input->post('check_database');
            $check_storage      = $this->input->post('check_storage');

            # storage config
            $storage_type        = trim($this->input->post('storage_type'));
            $storage_ip          = trim($this->input->post('storage_ip'));
            $storage_username    = trim($this->input->post('storage_username'));
            $storage_password    = trim($this->input->post('storage_password'));
            $storage_port        = trim($this->input->post('storage_port'));
            $storage_home_dir    = trim($this->input->post('storage_home_dir'));
            #
            if($check_database=="on")
                $check_database =1;
            else {
                $check_database =0;
            }
            if($check_storage=="on")
                $check_storage =1;
            else {
                $check_storage =0;
            }
            # Validation
            $this->form_validation->set_rules('name', lang('city_lbl_city_name'), 'required');
            $this->form_validation->set_rules('passcode', lang('city_lbl_passcode'), 'required');
            if($check_database==1){
                $this->form_validation->set_rules('password', lang('city_lbl_password'), 'matches[passconf]');
            }
            if($check_storage==1){
                $this->form_validation->set_rules('storage_password', lang('city_lbl_password'), 'matches[storage_passconf]');
            }
            if ($this->form_validation->run() === TRUE) {
                $api_param   = array(

                    'cityname'          => $name,
                    'passcode'          => $passcode,
                    'database'          => $check_database,
                    'storage'           => $check_storage,
                    'database_hostname' => $hostname,
                    'database_username' => $username,
                    'database_password' => $password,
                    'database_database' => $database,
                    'database_port'     => $port,
                    'storage_type'      => $storage_type,
                    'storage_ip'        => $storage_ip,
                    'storage_username'  => $storage_username,
                    'storage_password'  => $storage_password,
                    'storage_port'      => $storage_port,
                    'storage_home_dir'  => $storage_home_dir,
                );
                if ($this->_edit_flag) {
                    $api_param['id_city'] = $id;
                    $api_results = $this->rest->put('web/city?id_city='.$id, $api_param, 'json');
                } else {
                    $api_results = $this->rest->post('web/city?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status  = $this->rest->status();
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = $this->lang->line('city_msg_save_successful');
                    $flag = TRUE;
                }
            }
        }

        # Edit
        if ($this->_edit_flag) {
            # Data
            $api_param = array(
                'id_city' => $id,
            );
            $api_results = $this->rest->get('web/city/index', $api_param, 'json');
            $api_status  = $this->rest->status();
            if ( isset($api_results->data) AND !empty($api_results->data) ) {
                $data               = $api_results->data;
                $this->data['data'] = $data[0];
            }
            # Config

        }
        $this->data['flag']      = $flag;
        $this->data['edit_flag'] = $this->_edit_flag;
        $this->layout = false;
        $this->view   = 'city/detail';
    }

    function delete($id = 0) {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 0) {
                $api_param = array(
                    'id_city' => $id
                );
                $api_results = $this->rest->delete('web/city?id_city='.$id,$api_param,'json');
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    = $this->lang->line('city_msg_delete_successful');
                } else {
                    $return['status'] = false;
                    $return['msg']    = $this->lang->line('err_has_an_error_when_update_database');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }
}