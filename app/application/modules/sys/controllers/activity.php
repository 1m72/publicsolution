<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Activity controller
 *
 * @author Chien Tran <tran.duc.chien@kloon.vn>
 * @created 18 Dec 2013
 */

class Activity extends MY_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();

         # Language
        $this->load->language('activity');

        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        # Breakcrumbs
        $this->breadcrumb->append_crumb($this->lang->line('activity_lbl'), create_url('sys/activity'));

        # id_city
        $this->_id_city = current_city();

        # limit of grid
        $this->_limit   = config_item('grid_limit');
    }

    function index() {
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_ACTIVITY);
            return;
        }

         if (current_language(FALSE) == 'de') {
            $column_properties = array(
                'order_width'             => 50,
                'id_width'                => 100,
                'name_width'              => 200,
                'color_width'             => 100,
                'updated_width'           => 120,
                'created_width'           => 100,
                'option_width'            => 200,
            );
        } else {
            $column_properties = array(
                'order_width'             => 50,
                'id_width'                => 100,
                'name_width'              => 200,
                'color_width'             => 100,
                'updated_width'           => 100,
                'created_width'           => 100,
                'option_width'            => 140,
            );
        }
        $this->data['column_properties'] = json_encode($column_properties);

        # View
        $this->load_assets('grid_view.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        $this->data['limit'] = $this->_limit;
        $this->view          = 'activity/index';
        $this->layout        = 'layouts/module';
    }

    function add() {
        $this->_update();
    }

    function edit($id = 0) {
        $this->_edit_flag = true;
        $this->_update($id);
    }

    function _update($id = NULL) {
        if (!$this->input->is_ajax_request()) {
            redirect(create_url('sys/activity'));
        }

        # Libs
        $this->load->library('form_validation');
        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }
        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            # Post data
            $name  = trim($this->input->post('name'));
            $color = trim($this->input->post('color'));

            # Validation
            $this->form_validation->set_rules('name', $this->lang->line('activity_lbl_name'), 'required');
            if ($this->form_validation->run() === TRUE) {
                $api_param   = array(
                    'name'  => $name,
                    'color' => $color
                );

                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_results = $this->rest->put('web/activity/index?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_results = $this->rest->post('web/activity/index?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status = $this->rest->status();
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = $this->lang->line('activity_msg_information_has_been_saved');
                    $flag = TRUE;
                } else {
                    $this->data['err'] = $this->lang->line('err_api_save');
                }
            }
        }

        # Data
        if ($this->_edit_flag) {
            $api_param = array(
                'id'      => $id,
                'id_city' => $this->_id_city,
            );
            $api_results = $this->rest->get('web/activity/index', $api_param, 'json');
            $api_status  = $this->rest->status();
            if ( isset($api_results->data) AND !empty($api_results->data) ) {
                $data               = $api_results->data;
                $this->data['data'] = $data[0];
            }
        }
        $this->data['flag']      = $flag;
        $this->data['edit_flag'] = $this->_edit_flag;
        $this->layout = false;
        $this->view   = 'activity/detail';
    }

    function delete($id = 0) {
        $return = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 0) {
                $api_param = array(
                    'id' => $id
                );
                $api_results = $this->rest->delete('web/activity?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    = $this->lang->line('activity_msg_has_been_deleted');
                } else {
                    $return['status'] = false;
                    $return['msg']    = $this->lang->line('err_has_an_error_when_update_database');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }
}