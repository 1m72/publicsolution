<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gps extends MY_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();
        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));
        # id_city
        $this->_id_city = config_item('id_city');
        # limit of grid
        $this->_limit   = config_item('grid_limit');
    }

    function index(){
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_WORKER_GPS);
            return;
        }

        $column_properties = array(
            'no_width'      => 50,
            'id_gps_width'  => 60,
            'id_cron_width' => 70,
            'ontime_width'  => 160,
            'created_width' => 160,
            'status_width'  => 80,
            'total_width'   => 60,
            'option_width'  => 100,
        );
        $this->data['column_properties'] = json_encode($column_properties);
        $this->load_assets('grid_view.js', ASSET_JS);

        $this->data['limit'] = $this->_limit;
        $this->view          = 'gps/index';
        $this->layout        = 'layouts/module';
    }

    function pusbjob(){
        $status = 'fail';
        if ($this->input->is_ajax_request() AND ($_SERVER['REQUEST_METHOD'] == 'POST') ) {
            $id_gps = $this->input->post('id_gps');
            if(intval($id_gps) > 0){
                $api_param = array(
                    'id_gps' => $id_gps
                );
                $api_result = $this->rest->post('web/v2/gps/add_push_job?id_city=' . $this->_id_city, $api_param, 'json');
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $status = 'ok';
                }
            }
        }
        echo $status;
    }
}