<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * @author : Le Thi Nhung(le.nhung@kloon.vn)
 * @creat  :  2014-08-27
 */

class Material extends MY_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();

        # Language
        $this->load->language('material');

        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        # Breakcrumbs
        $this->breadcrumb->append_crumb(lang('material_lbl_name'), create_url('sys/material'));

        # id_city
        $this->_id_city = current_city();

        # limit of grid
        $this->_limit   = config_item('grid_limit');
    }

    function index() {
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_MATERIAL);
            return;
        }

        # Load assets
        $this->load_assets('functions.js', ASSET_JS);

        # Gird columns width
        if(current_language(FALSE) == 'de'){
            $column_properties = array(
                'id_width'        => 70,
                'order_width'     => 30,
                'unit_width'      => 150,
                'updated_width'   => 120,
                'created_width'   => 100,
                'option_width'    => 200,
            );
        } else{
            $column_properties = array(
                'id_width'        => 70,
                'order_width'     => 30,
                'unit_width'      => 100,
                'updated_width'   => 100,
                'created_width'   => 100,
                'option_width'    => 140,
            );
        }
        $this->data['column_properties'] = json_encode($column_properties);

        # View
        $this->load_assets('grid_view.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        $this->data['limit'] = $this->_limit;
        $this->view          = 'material/index';
        $this->layout        = 'layouts/module';
    }

    function add() {
        $this->_update();
    }

    function edit($id = 0) {
        $this->_edit_flag = true;
        $this->_update($id);
    }
/*
 * @author : Le Thi Nhung(le.nhung@kloon.vn)
 * @creat  :  2014-08-27
 */
    function _update($id = NULL) {
        if (!$this->input->is_ajax_request()) {
            redirect(create_url('sys/material'));
        }

        # Assets
        $this->load_assets('select2/select2.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('select2/select2.min.js', ASSET_JS);

        # Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }

        if ($this->_edit_flag) {
            $api_param = array(
                'id'      => $id,
                'id_city' => $this->_id_city
            );
            $api_result = $this->rest->get('web/v1/material/index',$api_param);
            $api_status = $this->rest->status();
            if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
                $this->data['data'] = isset($api_result->data[0]) ? $api_result->data[0] : array();
            }
        }

        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            # Post data
            $name = trim($this->input->post('name'));
            $unit = trim($this->input->post('unit'));

            # Validation
            $this->form_validation->CI =& $this;
            $this->form_validation->set_rules('name', lang('material_lbl_name'), 'required|trim');
            $this->form_validation->set_rules('unit', lang('material_lbl_unit'), 'required|trim');

            if ($this->form_validation->run() === TRUE) {
                $api_param = array(
                    'name' => $name,
                    'unit' => $unit,
                );
                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_results     = $this->rest->put('web/v1/material/index?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_results = $this->rest->post('web/v1/material/index?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status  = $this->rest->status();
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $flag              = TRUE;
                    $this->data['msg'] = lang('material_msg_information_has_been_saved');
                }
                else {
                    $this->data['msg'] = lang('err_api_save');
                }
            } else {
                $this->data['error'] = validation_errors();
            }
        }

        $this->data['edit_flag'] = $this->_edit_flag;
        $this->data['flag']      = $flag;
        $this->layout            = false;
        $this->view              = 'material/detail';
    }

    function delete($id = 0) {
        $return = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 3) {
                $api_param = array(
                    'id' => $id
                );
                $api_results = $this->rest->delete('web/v1/material?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    = lang('material_msg_has_been_deleted');
                } else {
                    $return['status'] = false;
                    $return['msg']    = lang('err_has_an_error_when_update_database');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }

    function _process() {

        # Header response as JSON
        header('Content-Type: application/json');

        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        $api_param = json_decode(file_get_contents('php://input'), true);

        if (empty($api_param)) {
            $api_param = $_REQUEST;
        }
        $api_param['key'] = GET_MATERIAL;

        $api_result = $this->rest->post(config_item('kendoui_join').'?id_city='.$this->_id_city, $api_param, 'json');
        $api_status = $this->rest->status();

        $api_result->request = $api_param;
        echo json_encode($api_result);
    }
}
