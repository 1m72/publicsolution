<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Add-on controller
 *
 * @author Chien Tran <tran.duc.chien@kloon.vn>
 * @created 17 Dec 2013
 */

class Nfc extends MY_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();

         # Language
        $this->load->language('nfc');

        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        # Breakcrumbs
        $this->breadcrumb->append_crumb(lang('nfc_lbl'), create_url('sys/nfc'));

        # id_city
        $this->_id_city = current_city();

        # limit of grid
        $this->_limit   = config_item('grid_limit');
    }

    function index() {
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_NFC);
            return;
        }
        # Grid columns width
        if (current_language(FALSE) == 'de') {
            $column_properties = array(
                'order_width'      => 50,
                'id_width'         => 100,
                'name_width'       => 300,
                'value_width'      => '',
                'city_active_width'=> 100,
                'updated_width'    => 120,
                'created_width'    => 100,
                'option_width'     => 100,
            );
        } else {
            $column_properties = array(
                'order_width'      => 50,
                'id_width'         => 100,
                'name_width'       => 300,
                'value_width'      => '',
                'city_active_width'=> 110,
                'updated_width'    => 100,
                'created_width'    => 100,
                'option_width'     => 100,
            );
        }
        $this->data['column_properties'] = json_encode($column_properties);

        # View
        $this->load_assets('grid_view.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        $this->data['limit'] = $this->_limit;
        $this->view          = 'nfc/index';
        $this->layout        = 'layouts/module';
    }

    function _add() {
        $this->_update();
    }

    function edit($id = 0) {
        $this->_edit_flag = true;
        $this->_update($id);
    }

    function _update($id = NULL) {
        if (!$this->input->is_ajax_request()) {
            redirect(create_url('sys/nfc'));
        }

        # Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }
        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            # Post data
            $name = trim($this->input->post('name'));

            # Validation
            $this->form_validation->set_rules('name', lang('nfc_lbl_name'), 'required');
            if ($this->form_validation->run() === TRUE) {
                $api_param   = array(
                    'code' => $name
                );

                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_results     = $this->rest->put('web/nfc/index?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_results = $this->rest->post('web/nfc/index?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status = $this->rest->status();
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = lang('nfc_msg_information_has_been_saved');
                    $flag = TRUE;
                }else{
                    $this->data['err'] = lang('err_api_save');
                }
            }
        }

        # Data
        if ($this->_edit_flag) {
            $api_param = array(
                'id'      => $id,
                'id_city' => $this->_id_city,
            );
            $api_results = $this->rest->get('web/nfc/index', $api_param, 'json');
            $api_status  = $this->rest->status();
            if ( isset($api_results->data) AND !empty($api_results->data) ) {
                $this->data['data'] = $api_results->data;
            }
        }
        $this->data['edit_flag'] = $this->_edit_flag;
        $this->data['flag']      = $flag;
        $this->layout            = false;
        $this->view              = 'nfc/detail';
    }

    function _delete($id = 0) {
        $return = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 0) {
                $api_param = array(
                    'id' => $id
                );
                $api_results = $this->rest->delete('web/nfc?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    = lang('nfc_msg_has_been_deleted');
                } else {
                    $return['status'] = false;
                    $return['msg']    = lang('err_has_an_error_when_update_database');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }

    function generate() {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($this->input->is_ajax_request()) {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('total', lang('nfc_total'), 'required|interger|greater_than[0]|less_than[200]');
                if ($this->form_validation->run() !== false) {
                    $total  = intval($this->input->post('total'));
                    $api_params = array(
                        'total'   => $total,
                    );
                    $api_results = $this->rest->post('web/v1/nfc/nfc_add?id_city=' . $this->_id_city, $api_params, 'json');
                    $api_status  = $this->rest->status();
                    if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                        $this->data['msg']  = lang('nfc_msg_information_has_been_saved');
                        $this->data['flag'] = true;
                    } else {
                        $this->data['err'] = lang('err_api_save');
                    }
                }
            } else {
                redirect(create_url('sys/nfc'));
            }
        }

        $this->layout = false;
        $this->view   = 'nfc/generate';
    }
}
