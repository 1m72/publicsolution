<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Add-on controller
 *
 * @author Chien Tran <tran.duc.chien@kloon.vn>
 * @created 17 Dec 2013
 */

class Addon extends MY_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();

         # Language
        $this->load->language('addon');

        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        # Breakcrumbs
        $this->breadcrumb->append_crumb(lang('addon_lbl'), create_url('sys/addon'));

        # id_city
        $this->_id_city = current_city();

        # limit of grid
        $this->_limit   = config_item('grid_limit');
    }

    function index() {
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_ADDON);
            return;
        }

        # Load assets
        $this->load_assets('functions.js', ASSET_JS);
        $this->load_assets('select2/select2.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('select2/select2.min.js', ASSET_JS);

        if (current_language(FALSE) == 'de') {
            $column_properties = array(
                'order_w'             => 50,
                'id_w'                => 100,
                'identifier_w'        => 100,
                'name_w'              => 220,
                'updated_w'           => 120,
                'created_w'           => 100,
                'option_w'            => 200,
            );
        } else {
            $column_properties = array(
                'order_w'             => 50,
                'id_w'                => 100,
                'identifier_w'        => 100,
                'name_w'              => 220,
                'updated_w'           => 100,
                'created_w'           => 100,
                'option_w'            => 140,
            );
        }
        $this->data['column_properties'] = json_encode($column_properties);

        # View
        $this->load_assets('grid_view.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        $this->data['limit'] = $this->_limit;
        $this->view          = 'addon/index';
        $this->layout        = 'layouts/module';
    }

    function add() {
        $this->_update_v4();
    }

    function edit($id = 0) {
        $this->_edit_flag = true;
        $this->_update_v4($id);
    }

    function _update_v4($id = NULL) {
        if (!$this->input->is_ajax_request()) {
            redirect(create_url('sys/addon'));
        }

        # Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }
        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            # Post data
            $identifier   = trim($this->input->post('identifier'));
            $name         = trim($this->input->post('name'));
            $chk_module   = $this->input->post('chk_module');
            $chk_activity = $this->input->post('chk_activity');

            $modules = array();
            // Form validate - activities
            $this->form_validation->set_rules('identifier', lang('lbl_identifier'), 'required');
            $this->form_validation->set_rules('name', lang('addon_lbl'), 'required');
            $this->form_validation->set_rules('chk_module', lang('lbl_module'), 'required');
            $this->form_validation->set_rules('chk_activity', lang('crumb_activity'), 'required');
            if ($this->form_validation->run() === TRUE) {
                if (is_array($chk_module) AND !empty($chk_module)) {
                    // Activity
                    $activities = array();
                    if (is_array($chk_activity) AND !empty($chk_activity)) {
                        foreach ($chk_activity as $key => $value) {
                            $tmp = explode('_', $value);
                            if (is_array($tmp) AND (count($tmp) == 2)) {
                                $modules[$tmp[0]]['activiti'][] = $tmp[1];
                            }
                        }
                    }

                    // activity default
                    foreach ($chk_module as $module_key => $module_value) {
                        $rdo_activity_default = $this->input->post('rdo_activity_default_' . $module_value);
                        if ($rdo_activity_default !== false) {
                            $modules[$module_value]['activiti_default'] = $rdo_activity_default;
                        }

                        if (isset($modules[$module_value]['activiti'][0]) AND (($rdo_activity_default === false) OR (!in_array($rdo_activity_default, $modules[$module_value]['activiti']))) ) {
                            $modules[$module_value]['activiti_default'] = $modules[$module_value]['activiti'][0];
                        }
                    }
                }

                // api params
                $api_param   = array(
                    'identifier' => $identifier,
                    'name'       => $name,
                    'module'     => $modules
                );

                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_result     = $this->rest->put('web/v2/addon/index?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_result = $this->rest->post('web/v2/addon/index?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status = $this->rest->status();

                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = lang('addon_msg_information_has_been_saved');
                    $flag              = true;
                } else {
                    $this->data['err'] = lang('err_api_save');
                    $flag              = false;
                }
            } else {}
        }

        # Data
        if ($this->_edit_flag) {
            $api_param = array(
                'id'      => $id,
                'id_city' => $this->_id_city,
            );
            $api_result = $this->rest->get('web/machine_add_on/index', $api_param, 'json');
            $api_status  = $this->rest->status();
            if ( isset($api_result->data) AND !empty($api_result->data) ) {
                $data               = $api_result->data;
                $this->data['data'] = $data[0];
            }

            # Module selected
            if (!isset($this->data['module_selected'])) {
                $api_param = array(
                    'id'      => $id,
                    'id_city' => $this->_id_city
                );
                $api_result = $this->rest->get('web/v2/addon/index',$api_param);
                $api_status = $this->rest->status();
                if ( isset($api_result->data) AND !empty($api_result->data) ) {
                    $this->data['module_activities'] = json_decode(json_encode($api_result->data), true);
                }
            }
        }

        # Modules
        $modules = array();
        $api_result = $this->rest->get('web/v1/module/index?id_city='.$this->_id_city);
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $modules[$value->id] = $value->title;
            }
        }
        $this->data['modules']   = $modules;

        # Activity
        $activities       = array();
        $api_result     = $this->rest->get('web/activity/index?id_city='.$this->_id_city);
        $api_status     = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $activities[$value->id]   = $value;
            }
        }
        $this->data['activities'] = $activities;

        // View
        $this->data['edit_flag']  = $this->_edit_flag;
        $this->data['flag']       = $flag;
        $this->layout             = false;
        $this->view               = 'addon/detail_v4';
    }

    function _update_v3($id = NULL) {
        if (!$this->input->is_ajax_request()) {
            redirect(create_url('sys/addon'));
        }

        # Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }
        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            # Post data
            $identifier      = trim($this->input->post('identifier'));
            $name            = trim($this->input->post('name'));
            $module_selected = json_decode(trim($this->input->post('module_selected')), true);
            $module_parse    = array();
            if (is_array($module_selected) AND !empty($module_selected)) {
                foreach ($module_selected as $key => $value) {
                    if (is_array($value)) {
                        $module_parse[$key]['activiti'] = $value;
                    }
                }
            }

            # Validation
            $this->form_validation->set_rules('name', lang('addon_lbl_name'), 'required');
            $this->form_validation->set_rules('module', lang('addon_lbl_name'), 'required');
            if ($this->form_validation->run() === TRUE) {
                $api_param   = array(
                    'identifier' => $identifier,
                    'name'       => $name,
                    'module'     => $module_parse
                );

                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_result     = $this->rest->put('web/v2/addon/index?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_result = $this->rest->post('web/v2/addon/index?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status = $this->rest->status();

                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = lang('addon_msg_information_has_been_saved');
                    $flag = TRUE;
                }
            } else {
                $this->data['module_selected'] = $module_selected;
            }
        }

        # Data
        if ($this->_edit_flag) {
            $api_param = array(
                'id'      => $id,
                'id_city' => $this->_id_city,
            );
            $api_result = $this->rest->get('web/machine_add_on/index', $api_param, 'json');
            $api_status  = $this->rest->status();
            if ( isset($api_result->data) AND !empty($api_result->data) ) {
                $data               = $api_result->data;
                $this->data['data'] = $data[0];
            }

            # Module selected
            if (!isset($this->data['module_selected'])) {
                $api_param = array(
                    'id_addon' => $id,
                    'id_city'  => $this->_id_city
                );
                $api_result = $this->rest->get('web/v2/addon/index',$api_param);
                $api_status = $this->rest->status();

                $module_selected_tmp = isset($api_result->data) ? json_decode(json_encode($api_result->data), true) : array();
                $module_selected = array();
                if (is_array($module_selected_tmp) AND !empty($module_selected_tmp)) {
                    foreach ($module_selected_tmp as $key => $value) {
                        $module_selected[$key] = isset($value['activiti']) ? $value['activiti'] : array();
                    }
                }
                $this->data['module_selected'] = $module_selected;
            }
        }

        # Modules
        $modules = array();
        $api_result = $this->rest->get('web/v1/module/index?id_city='.$this->_id_city);
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $modules[$value->id] = $value->title;
            }
        }
        $this->data['modules']   = $modules;

        # Activity
        $activity       = array();
        $api_result     = $this->rest->get('web/activity/index?id_city='.$this->_id_city);
        $api_status     = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $activity[$value->id]   = $value;
            }
        }
        $this->data['activity'] = $activity;

        $this->data['edit_flag'] = $this->_edit_flag;
        $this->data['flag']      = $flag;
        $this->layout            = false;
        $this->view              = 'addon/detail_v3';
    }

    function _update_v2($id = NULL) {
        if (!$this->input->is_ajax_request()) {
            redirect(create_url('sys/addon'));
        }

        # Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }
        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            # Post data
            $identifier = trim($this->input->post('identifier'));
            $name       = trim($this->input->post('name'));

            # Validation
            $this->form_validation->set_rules('name', lang('addon_lbl_name'), 'required');
            if ($this->form_validation->run() === TRUE) {
                $api_param   = array(
                    'identifier' => $identifier,
                    'name'       => $name
                );

                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_result = $this->rest->put('web/machine_add_on/index?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_result = $this->rest->post('web/machine_add_on/index?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status = $this->rest->status();

                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = lang('addon_msg_information_has_been_saved');
                    $flag = TRUE;
                }
            }
        }

        # Data
        if ($this->_edit_flag) {
            $api_param = array(
                'id'      => $id,
                'id_city' => $this->_id_city,
            );
            $api_result = $this->rest->get('web/machine_add_on/index', $api_param, 'json');
            $api_status  = $this->rest->status();
            if ( isset($api_result->data) AND !empty($api_result->data) ) {
                $data                  = $api_result->data;
                $this->data['data'] = $data[0];
            }
        }
        $this->data['edit_flag'] = $this->_edit_flag;
        $this->data['flag']      = $flag;
        $this->layout = false;
        $this->view   = 'addon/detail';
    }

    function delete($id = 0) {
        $return = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 0) {
                $api_param = array(
                    'id' => $id
                );
                $api_result = $this->rest->delete('web/machine_add_on?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    = lang('addon_msg_has_been_deleted');
                } else {
                    $return['status'] = false;
                    $return['msg']    = lang('err_has_an_error_when_update_database');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }
}
