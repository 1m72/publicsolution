<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Addon controller
 *
 * @author nguyen.quynh.chi@kloon.vn
 * @copyright 2013
 * @modified : 04 Mar 2014 by Chien Tran <tran.duc.chien@kloon.vn>
 */

class Machine extends MY_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();

        # Language
        $this->load->language('machine');
        $this->load->language('activity');

        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        # Breakcrumbs
        $this->breadcrumb->append_crumb($this->lang->line('mc_lbl_machine'), create_url('sys/machine'));

        # id_city
        $this->_id_city = current_city();

        # limit of grid
        $this->_limit   = config_item('grid_limit');
    }

    function index() {
        # Load assets
        $this->load_assets('functions.js', ASSET_JS);

        $id_module = $this->input->get('module');
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_MACHINE, $id_module);
            return;
        }

        # Gird columns width
        if(current_language(FALSE) == 'de'){
            $column_properties = array(
                'order_width'           => 50,
                'id_width'              => 100,
                'identifier_width'      => 120,
                'name_width'            => 200,
                'code_width'            => 150,
                'updated_width'         => 120,
                'created_width'         => 100,
                'option_width'          => 200,
            );
        } else{
            $column_properties = array(
                'order_width'           => 50,
                'id_width'              => 100,
                'identifier_width'      => 120,
                'name_width'            => 200,
                'code_width'            => 150,
                'updated_width'         => 100,
                'created_width'         => 100,
                'option_width'          => 140,
            );
        }
        $this->data['column_properties'] = json_encode($column_properties);

        # View
        $this->load_assets('grid_view.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        $this->data['limit']     = $this->_limit;
        $this->data['id_module'] = $id_module;
        $this->view              = 'machine/index';
        $this->layout            = 'layouts/module';
    }

    function add() {
        $this->_update();
    }

    function edit($id = 0, $id_module = NULL) {
        $this->_edit_flag = true;
        $this->_update($id, $id_module);
    }

    public function id_activity_check($id_activity)
    {
        if(count($id_activity)<2)
        {
            $this->form_validation->set_message('id_activity_check', lang('message_activity'));
            return FALSE;
        }
        else{
            return TRUE;
        }
    }

    function _update($id = null, $id_module = null) {
        $this->_update_v4($id, $id_module);
    }

    function _update_v2($id = NULL) {
        # Assets
        $this->load_assets('select2/select2.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('select2/select2.min.js', ASSET_JS);

        if (!$this->input->is_ajax_request()) {
            redirect(create_url('sys/machine'));
        }

        # Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }

        # Post processing
        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            # Post data
            $identifier       = trim($this->input->post('identifier'));
            $name             = trim($this->input->post('name'));
            $nfc_code         = trim($this->input->post('nfc_code'));
            $id_addon         = $this->input->post('id_addon');
            $description      = trim($this->input->post('description'));
            $id_activity      = $this->input->post('hd_select');
            $id_default       = $this->input->post('id_default');
            $activity_default = json_decode(trim($this->input->post('activity_default')), true);
            $module_selected  = json_decode(trim($this->input->post('module_selected')), true);
            $module_parse     = array();
            if (is_array($module_selected) AND !empty($module_selected)) {
                foreach ($module_selected as $key => $value) {
                    if (is_array($value)) {
                        $module_parse[$key]['activiti'] = $value;
                    }
                }
            }
            if (is_array($activity_default) AND !empty($activity_default)) {
                foreach ($activity_default as $key => $value) {
                    if (isset($module_parse[$key])) {
                        $module_parse[$key]['activiti_default'] = $value;
                    }
                }
            }

            # Validation
            $this->form_validation->CI =& $this;
            $this->form_validation->set_rules('name', $this->lang->line('mc_lbl_machine_name'), 'required|trim');
            $this->form_validation->set_rules('nfc_code', $this->lang->line('mc_lbl_nfc_code'), 'trim');
            $this->form_validation->set_rules('description', $this->lang->line('mc_lbl_description'), 'trim');

            $this->data['id_default']    = $id_default;
            $this->data['machine_addon'] = $id_addon;
            $this->data['activities']    = $id_activity;

            if(isset($id_addon) && !empty($id_addon)){
                $id_addon = implode(",",$id_addon);
            }

            if ($this->form_validation->run() === TRUE) {
                $api_param   = array(
                    'id_machine'  => $id,
                    'identifier'  => $identifier,
                    'name'        => $name,
                    'module'      => $module_parse,
                    'nfc_code'    => $nfc_code,
                    'id_addon'    => $id_addon,
                    'id_city'     => $this->_id_city,
                    'id_activity' => $id_activity,
                    'id_default'  => $id_default,
                    'description' => $description,
                );
                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_results     = $this->rest->put('web/v2/machine/index?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_results = $this->rest->post('web/v2/machine/index?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status  = $this->rest->status();
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = $this->lang->line('mc_msg_machine_information_has_been_saved');
                    $flag = TRUE;
                }
                else {
                    $this->data['msg'] = $this->lang->line('err_api_save');
                }
            } else {
                $this->data['module_selected']  = $module_selected;
                $this->data['activity_default'] = $activity_default;
                $this->data['error']            = validation_errors();
            }
        }

        $this->data['id_default'] = 0;
        if ($this->_edit_flag) {
            $machine_addon = array();
            $api_param = array(
                'id_machine' => $id,
                'id_city'   => $this->_id_city
            );
            $api_result = $this->rest->get('web/v1/machine/m_index',$api_param);
            $api_status = $this->rest->status();
            $activity_select = array();
            if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
                $data                     = $api_result->data;
                $this->data['machine']    = $data[0];
                $this->data['id_default'] = $data[0]->machine->id_default;
                foreach ($api_result->data as $key => $value){
                    $activity_select = $value->activity;
                    $machine_addon   = array_unique($value->addon);
                }
            }

            # Module selected
            $api_param = array(
                'id_machine' => $id,
                'id_city'   => $this->_id_city
            );
            $api_result = $this->rest->get('web/v2/machine/index',$api_param);
            $api_status = $this->rest->status();

            if (!isset($this->data['module_selected'])) {
                $module_selected_tmp = isset($api_result->data) ? json_decode(json_encode($api_result->data), true) : array();
                $module_selected = array();
                $activity_default = array();
                if (is_array($module_selected_tmp) AND !empty($module_selected_tmp)) {
                    foreach ($module_selected_tmp as $key => $value) {
                        $module_selected[$key] = isset($value['activiti']) ? $value['activiti'] : array();
                        if (isset($value['activiti_default'])) {
                            $activity_default[$key] = $value['activiti_default'];
                        }
                    }
                }
                $this->data['module_selected']  = $module_selected;
                $this->data['activity_default'] = $activity_default;
            }

            $this->data['machine_addon']    = $machine_addon;
            $this->data['activity_select']  = implode(',', array_unique($activity_select));
        }

        # NFC
        $nfc_code    = array();
        $api_param = array(
            'id_machine' => $id,
            'id_city'    => $this->_id_city,
            'active'     => false
        );
        $api_result = $this->rest->get('web/nfc/index',$api_param, 'json');
        $api_status = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $nfc_code[$value->id] = $value;
            }
        }
        $this->data['nfc_code'] = $nfc_code;

        # Activity
        $activity       = array();
        $api_result     = $this->rest->get('web/v1/activity/index?id_city='.$this->_id_city);
        $api_status     = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $activity[$value->id]   = $value;
            }
        }
        $this->data['activity'] = $activity;

        # Addon
        $addon = array();
        $api_result = $this->rest->get('web/v1/machine_add_on/index?id_city='.$this->_id_city);
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $addon[$value->id] = $value;
            }
        }
        $this->data['addon']     = $addon;

        # Modules
        $modules = array();
        $api_result = $this->rest->get('web/v1/module/index?id_city='.$this->_id_city);
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $modules[$value->id] = $value->title;
            }
        }
        $this->data['modules']      = json_decode(json_encode($modules), true);

        $this->data['edit_flag'] = $this->_edit_flag;
        $this->data['flag']      = $flag;
        $this->layout            = false;
        $this->view              = 'machine/detail_v2';
    }

    function _update_v3($id = NULL) {
        # Assets
        $this->load_assets('select2/select2.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('select2/select2.min.js', ASSET_JS);

        if (!$this->input->is_ajax_request()) {
            redirect(create_url('sys/machine'));
        }

        # Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }

        # Post processing
        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            # Post data
            $txt_identifier       = trim($this->input->post('txt_identifier'));
            $txt_name             = trim($this->input->post('txt_name'));
            $sel_nfc              = trim($this->input->post('sel_nfc'));
            $txt_description      = trim($this->input->post('txt_description'));
            $chk_module           = $this->input->post('chk_module');
            $chk_activity         = $this->input->post('chk_activity');
            $chk_addon            = $this->input->post('chk_addon');

            # Validation
            $this->form_validation->CI =& $this;
            $this->form_validation->set_rules('txt_name', $this->lang->line('mc_lbl_machine_name'), 'required|trim');
            $this->form_validation->set_rules('txt_description', $this->lang->line('mc_lbl_description'), 'trim');
            $this->form_validation->set_rules('chk_module', $this->lang->line('crumb_modules'), 'required');
            $this->form_validation->set_rules('chk_activity', $this->lang->line('crumb_activity'), '');
            $this->form_validation->set_rules('chk_addon', $this->lang->line('crumb_addon'), '');

            # modules
            $modules = array();
            if (is_array($chk_module) AND !empty($chk_module)) {
                # Form validate - activities
                $this->form_validation->set_rules('chk_activity', $this->lang->line('crumb_activity'), 'required');

                # Activity
                $activities = array();
                if (is_array($chk_activity) AND !empty($chk_activity)) {
                    foreach ($chk_activity as $key => $value) {
                        $tmp = explode('_', $value);
                        if (is_array($tmp) AND (count($tmp) == 2)) {
                            $activities[$tmp[0]][] = $tmp[1];
                        }
                    }
                }

                # Addon
                $addons = array();
                if (is_array($chk_addon) AND !empty($chk_addon)) {
                    foreach ($chk_addon as $key => $value) {
                        $tmp = explode('_', $value);
                        if (is_array($tmp) AND (count($tmp) == 2)) {
                            $addons[$tmp[0]][] = $tmp[1];
                        }
                    }
                }

                foreach ($chk_module as $key => $value) {
                    $field_activity_default = "rdo_activity_default_{$value}";

                    # Form validate - rdo_activity_default
                    if (isset($activities[$value]) AND is_array($activities[$value]) AND !empty($activities[$value])) {
                        $this->form_validation->set_rules($field_activity_default, $this->lang->line('lbl_activity_default'), 'required');
                    } else {
                        $this->form_validation->set_rules($field_activity_default, $this->lang->line('lbl_activity_default'), '');
                    }

                    # Activity_default
                    $rdo_activity_default = $this->input->post($field_activity_default);
                    $activity_default = array();
                    if (is_array($rdo_activity_default) AND !empty($rdo_activity_default)) {
                        foreach ($rdo_activity_default as $key_activity_default => $value_activity_default) {
                            $tmp = explode('_', $value_activity_default);
                            if (is_array($tmp) AND (count($tmp) == 2)) {
                                $activity_default[$tmp[0]] = $tmp[1];
                            }
                        }
                    }

                    $tmp = array(
                        'id_machine' => $id,
                        'id_module'  => $value,
                    );
                    if (isset($addons[$value])) {
                        $tmp['addon'] = $addons[$value];
                    }
                    if (isset($activities[$value])) {
                        $tmp['activity'] = $activities[$value];
                    }
                    if (isset($activity_default[$value])) {
                        $tmp['activity_default'] = $activity_default[$value];
                    }
                    $modules[$value] = $tmp;
                }
            }

            if ($this->form_validation->run() === TRUE) {
                $api_param   = array(
                    'id_city'     => $this->_id_city,
                    'id_machine'  => $id,
                    'identifier'  => $txt_identifier,
                    'nfc_code'    => $sel_nfc,
                    'name'        => $txt_name,
                    'description' => $txt_description,
                    'module'      => $modules
                );
                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_results = $this->rest->put('web/v2/machine/index?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_results = $this->rest->post('web/v2/machine/index?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status = $this->rest->status();
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = $this->lang->line('mc_msg_machine_information_has_been_saved');
                    $flag = TRUE;
                }
                else {
                    $this->data['msg'] = $this->lang->line('err_api_save');
                }
            } else {
                #$this->data['error'] = validation_errors();
            }
        }

        # Modules
        $modules = array();
        $api_result = $this->rest->get('web/v1/module/index?id_city='.$this->_id_city);
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $modules[$value->id] = $value->title;
            }
        }
        $this->data['modules']   = json_decode(json_encode($modules), true);

        # Edit processing
        $this->data['id_default'] = 0;
        if ($this->_edit_flag) {
            $machine_addon = array();
            $api_param = array(
                'id_machine' => $id,
                'id_city'    => $this->_id_city
            );
            $api_result = $this->rest->get('web/v1/machine/m_index',$api_param);
            $api_status = $this->rest->status();
            $activity_select = array();
            if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
                $data                     = $api_result->data;
                $this->data['machine']    = $data[0];
                $this->data['id_default'] = $data[0]->machine->id_default;
                foreach ($api_result->data as $key => $value){
                    $activity_select = $value->activity;
                    $machine_addon   = array_unique($value->addon);
                }
            }

            # Module addon select
            $id_modules   = array_keys($modules);
            $machine_addon = array();
            $api_param = array(
                'id_city'    => $this->_id_city,
                'id_machine' => $id
            );
            $api_result = $this->rest->get('web/v2/machine/addon_machine', $api_param, 'json');
            $api_status = $this->rest->status();
            if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
                foreach ($api_result->data as $key => $value) {
                    $machine_addon[$key] = $value->addon;
                }
            }
            $this->data['machine_addon'] = $machine_addon;

            # Module activities select
            $machine_activities = array();
            $api_param = array(
                'id_city'    => $this->_id_city,
                'id_machine' => $id
            );
            $api_result = $this->rest->get('web/v2/machine/activities_machine', $api_param, 'json');
            $api_status = $this->rest->status();
            if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
                foreach ($api_result->data as $key => $value) {
                    $tmp = json_decode(json_encode($value), true);
                    $machine_activities[$value->id_module] = $tmp;
                }
            }
            $this->data['machine_activities'] = $machine_activities;
        }

        # NFC
        $nfc  = array();
        $api_param = array(
            'id_machine' => $id,
            'id_city'    => $this->_id_city,
        );
        $api_result = $this->rest->get('web/v1/nfc/index',$api_param, 'json');
        $api_status = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $nfc[$value->id] = $value;
            }
        }
        $this->data['nfc'] = $nfc;

        # All activities
        $activities = array();
        $api_result = $this->rest->get('web/v1/activity/index?id_city='.$this->_id_city);
        $api_status = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $activities[$value->id]   = $value;
            }
        }
        $this->data['activities'] = $activities;

        # All addons
        $addon = array();
        $api_param = array(
            'id_city'   => $this->_id_city,
            'id_module' => isset($id_module) ? implode(',', $id_modules) : ''
        );
        $api_result = $this->rest->get('web/v2/addon/addon_module', $api_param, 'json');
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $addon[$value->id_module][$value->id] = $value;
            };
        }
        $this->data['addon'] = $addon;

        # View data
        $this->data['edit_flag'] = $this->_edit_flag;
        $this->data['flag']      = $flag;
        $this->layout            = false;
        $this->view              = 'machine/detail_v3';
    }

    function _update_v4($id = NULL, $id_module = NULL) {
        # Assets
        $this->load_assets('select2/select2.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('select2/select2.min.js', ASSET_JS);

        if (!$this->input->is_ajax_request()) {
            redirect(create_url('sys/machine'));
        }

        # Libs
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }

        $this->data['id_module'] = $id_module;

        # Modules
        $modules = array();
        $api_result = $this->rest->get('web/v1/module/index?id_city='.$this->_id_city);
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $modules[$value->id] = $value->title;
            }
        }
        $this->data['modules']   = json_decode(json_encode($modules), true);

        # Post processing
        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            # Post data
            $txt_identifier       = trim($this->input->post('txt_identifier'));
            $txt_name             = trim($this->input->post('txt_name'));
            $sel_nfc              = trim($this->input->post('sel_nfc'));
            $txt_description      = trim($this->input->post('txt_description'));
            $chk_module           = $this->input->post('chk_module');
            $chk_activity         = $this->input->post('chk_activity');
            $chk_addon            = $this->input->post('chk_addon');

            # Validation
            $this->form_validation->CI =& $this;
            $this->form_validation->set_rules('txt_name', $this->lang->line('mc_lbl_machine_name'), 'required|trim');
            $this->form_validation->set_rules('txt_description', $this->lang->line('mc_lbl_description'), 'trim');
            $this->form_validation->set_rules('chk_module', $this->lang->line('crumb_modules'), 'required');
            $this->form_validation->set_rules('chk_activity', $this->lang->line('crumb_activity'), '');
            $this->form_validation->set_rules('chk_addon', $this->lang->line('crumb_addon'), '');

            # modules
            $modules = array();
            if (is_array($chk_module) AND !empty($chk_module)) {
                # Form validate - activities
                $this->form_validation->set_rules('chk_activity', $this->lang->line('crumb_activity'), 'required');

                # Activity
                $activities = array();
                if (is_array($chk_activity) AND !empty($chk_activity)) {
                    foreach ($chk_activity as $key => $value) {
                        $tmp = explode('_', $value);
                        if (is_array($tmp) AND (count($tmp) == 2)) {
                            $activities[$tmp[0]][] = $tmp[1];
                        }
                    }
                }

                # Addon
                $addons = array();
                if (is_array($chk_addon) AND !empty($chk_addon)) {
                    foreach ($chk_addon as $key => $value) {
                        $tmp = explode('_', $value);
                        if (is_array($tmp) AND (count($tmp) == 2)) {
                            $addons[$tmp[0]][] = $tmp[1];
                        }
                    }
                }

                foreach ($chk_module as $key => $value) {
                    $field_activity_default = "rdo_activity_default_{$value}";

                    # Form validate - rdo_activity_default
                    if (isset($activities[$value]) AND is_array($activities[$value]) AND !empty($activities[$value])) {
                        $this->form_validation->set_rules($field_activity_default, $this->lang->line('lbl_activity_default'), 'required');
                    } else {
                        $this->form_validation->set_rules($field_activity_default, $this->lang->line('lbl_activity_default'), '');
                    }

                    # Activity_default
                    $rdo_activity_default = $this->input->post($field_activity_default);
                    $activity_default = array();
                    if (is_array($rdo_activity_default) AND !empty($rdo_activity_default)) {
                        foreach ($rdo_activity_default as $key_activity_default => $value_activity_default) {
                            $tmp = explode('_', $value_activity_default);
                            if (is_array($tmp) AND (count($tmp) == 2)) {
                                $activity_default[$tmp[0]] = $tmp[1];
                            }
                        }
                    }

                    $tmp = array(
                        'id_machine' => $id,
                        'id_module'  => $value,
                    );
                    if (isset($addons[$value])) {
                        $tmp['addon'] = $addons[$value];
                    }
                    if (isset($activities[$value])) {
                        $tmp['activity'] = $activities[$value];
                    }
                    if (isset($activity_default[$value])) {
                        $tmp['activity_default'] = $activity_default[$value];
                    }
                    $modules[$value] = $tmp;
                }
            }

            if ($this->form_validation->run() === TRUE) {
                $api_param   = array(
                    'id_city'     => $this->_id_city,
                    'id_machine'  => $id,
                    'identifier'  => $txt_identifier,
                    'nfc_code'    => $sel_nfc,
                    'name'        => $txt_name,
                    'description' => $txt_description,
                    'module'      => $modules
                );
                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_results = $this->rest->put('web/v2/machine/index?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_results = $this->rest->post('web/v2/machine/index?id_city='.$this->_id_city, $api_param, 'json');
                }
                $api_status = $this->rest->status();
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = $this->lang->line('mc_msg_machine_information_has_been_saved');
                    $flag = TRUE;
                } else {
                    $this->data['msg'] = $this->lang->line('err_api_save');
                }
            } else {
                // $this->data['error'] = validation_errors();
            }
        }

        # Edit processing
        $this->data['id_default'] = 0;
        if ($this->_edit_flag) {
            $machine_addon = array();
            $api_param = array(
                'id_machine' => $id,
                'id_city'    => $this->_id_city
            );
            $api_result = $this->rest->get('web/v1/machine/m_index',$api_param);
            $api_status = $this->rest->status();
            $activity_select = array();
            if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
                $data                     = $api_result->data;
                $this->data['machine']    = $data[0];
                $this->data['id_default'] = $data[0]->machine->id_default;
                foreach ($api_result->data as $key => $value){
                    $activity_select = $value->activity;
                    $machine_addon   = array_unique($value->addon);
                }
            }

            # Module addon select
            $id_modules   = array_keys($modules);
            $machine_addon = array();
            $api_param = array(
                'id_city'    => $this->_id_city,
                'id_machine' => $id
            );
            $api_result = $this->rest->get('web/v2/machine/addon_machine', $api_param, 'json');
            $api_status = $this->rest->status();
            if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
                foreach ($api_result->data as $key => $value) {
                    $machine_addon[$key] = $value->addon;
                }
            }
            $this->data['machine_addon'] = $machine_addon;

            # Module activities select
            $machine_activities = array();
            $api_param = array(
                'id_city'    => $this->_id_city,
                'id_machine' => $id
            );
            $api_result = $this->rest->get('web/v2/machine/activities_machine', $api_param, 'json');
            $api_status = $this->rest->status();
            if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
                foreach ($api_result->data as $key => $value) {
                    $tmp = json_decode(json_encode($value), true);
                    $machine_activities[$value->id_module] = $tmp;
                }
            }
            $this->data['machine_activities'] = $machine_activities;
        }

        # NFC
        $nfc = array();
        $api_param = array(
            'id_machine' => $id,
            'id_city'    => $this->_id_city,
            'active'     => false
        );
        $api_result = $this->rest->get('web/v1/nfc/index', $api_param);
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $nfc[$value->id] = $value;
            }
        }
        $this->data['nfc'] = $nfc;

        # All activities
        $activities = array();
        $api_result = $this->rest->get('web/v1/activity/index?id_city='.$this->_id_city);
        $api_status = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                $activities[$value->id]   = $value;
            }
        }
        $this->data['activities'] = $activities;

        # All addons
        $addon = array();
        $api_param = array(
            'id_city'   => $this->_id_city,
            'id_module' => isset($id_module) ? implode(',', $id_modules) : ''
        );
        $api_result = $this->rest->get('web/v2/addon/addon_module', $api_param, 'json');
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $addon[$value->id_module][$value->id] = $value;
            };
        }
        $this->data['addon'] = $addon;

        # View data
        $this->data['edit_flag'] = $this->_edit_flag;
        $this->data['flag']      = $flag;
        $this->layout            = false;
        $this->view              = 'machine/detail_v4';
    }

    function delete($id = 0) {
        $return = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 0) {
                $api_param = array(
                    'id' => $id
                );
                $api_results = $this->rest->delete('web/v1/machine?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_results->status) AND ($api_results->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    = $this->lang->line('mc_msg_machine_has_been_deleted');
                } else {
                    $return['status'] = false;
                    $return['msg']    = $this->lang->line('err_has_an_error_when_update_database');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }

    function list_activity(){
        $this->load->helper('form');

        $id_activity = $this->input->get('id_activity');
        $activity      = array();
        $api_result     = $this->rest->get('web/v1/activity/index?id_city='.$this->_id_city);
        $api_status     = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data))){
            foreach ($api_result->data as $key => $value){
                 $activity[$value->id] = $value->name;
            }
        }
        echo form_dropdown('id_activity[]', $activity, $id_activity, 'id="id_activity" class="select2" style="width: 250px" multiple="true" onchange="change_activity()"');
    }

    function _process() {

        # Header response as JSON
        header('Content-Type: application/json');

        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        $api_param = json_decode(file_get_contents('php://input'), true);

        if (empty($api_param)) {
            $api_param = $_REQUEST;
        }
        $api_param['key'] = GET_MACHINE;

        $api_result = $this->rest->post(config_item('kendoui_join').'?id_city='.$this->_id_city, $api_param, 'json');
        $api_status = $this->rest->status();

        $api_result->request = $api_param;
        echo json_encode($api_result);
    }
}
