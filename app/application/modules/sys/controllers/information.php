<?php
class Information extends MX_Controller{
    function __construct(){
        parent:: __construct();

        // Init API server
        $this->load->library('rest');
        $this->rest->initialize(array('server' => config_item('api_web')));
    }

    function index() {
        # Params
        $id      = isset($_GET['id']) ? $_GET['id'] : '';
        $id_city = isset($_GET['id_city']) ? $_GET['id_city'] : '';
        $id_gps  = isset($_GET['id_gps']) ? $_GET['id_gps'] : '';
        $lat     = isset($_GET['lat']) ? $_GET['lat'] : '';
        $lon     = isset($_GET['lon']) ? $_GET['lon'] : '';

        $api_param  = array(
            'id_city' => $id_city,
            'id'      => $id,
            'id_gps'  => $id_gps,
            'lat'     => $lat,
            'lon'     => $lon
        );
        $api_result = $this->rest->get('web/v1/infor/detail', $api_param, 'json');
        $api_status = $this->rest->status();
        $data = array();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
            $data = $api_result->data[0];
        }
        $this->load->view('information/index', $data);
    }

    function information($id_city = FALSE, $id_employee = FALSE, $date = FALSE, $file_name = FALSE){
        # Cache Respone
        $this->output->cache(config_item('cache_viewinfor_information'));

        $data = array();
        $file_path = config_item('upload_dir') . $id_city . '/' . date('Y/m/d', strtotime($date)) . '/' . $id_employee . '/' . $file_name;
        if($file_path != FALSE){
            $index = $this->fn_lastIndexOf($file_path,'.');
            $type = '';
            if($index >= 0){
                $type = substr($file_path, $index);
            }
            $data['detail'] = array(
                'path'  => $file_path,
                'type'  => $type,
            );
        }
        $this->load->view('information/information',$data);
    }

    function fn_lastIndexOf($string,$item){
        $index = strpos(strrev($string) , strrev($item));
        if ($index){
            $index = strlen($string)-strlen($item) - $index;
            return $index;
        } else {
            return -1;
        }
    }

    function get_street()
    {
        #$_street_data = file_get_contents('http://nominatim.openstreetmap.org/reverse?format=json&lat=48.0897526&lon=11.82467208&zoom=18&addressdetails=1');
        #$_street_array = json_decode($_street_data,TRUE);
        #var_dump($_street_array['address']['road']);
        #echo trim('23.342343534095435049534 23.342343534095435049534 2014-10-10 10:10:10');
        $json = '{"number": 12345678901234567890}';
        var_dump(json_decode($json));
        var_dump(json_decode($json, false, 512, JSON_BIGINT_AS_STRING));
    }

    function test(){
        $a = 'sdasdasdas';
        echo json_encode($a);
    }
}
