<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User Controller
 * @author Le Thi Nhung (le.nhung@kloon.vn)
 * @created 11 Nov 2013
 */

class User extends MY_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    public function __construct() {
        parent::__construct();

        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        # Breakcrumbs
        $this->breadcrumb->append_crumb(lang('crumb_user'), create_url('sys/user'));

        # Language
        $this->load->language('user');
        $this->load->language('user_group');
        $this->load->language('company');
        $this->load->language('winterdienst', $this->_global_language);

        # id_city
        $this->_id_city = current_city();

        # limit of grid
        $this->_limit   = config_item('grid_limit');
    }

    function index() {
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(GET_USERS);
            return;
        }
        if (current_language(FALSE) == 'de') {
            $column_properties = array(
                'order_width'      => 50,
                'username_width'   =>150 ,
                'group_name_width' =>100 ,
                'email_width'      =>100 ,
                'updated_at_width' => 120,
                'created_at_width' => 100,
                'option_width'     =>200,
            );
        } else {
            $column_properties = array(
                'order_width'      => 50,
                'username_width'   => 100,
                'group_name_width' => 100,
                'email_width'      => 100,
                'updated_at_width' => 100,
                'created_at_width' => 100,
                'option_width'     => 140,
            );
        }
        $this->data['column_properties'] = json_encode($column_properties);
        # View
        $this->load_assets('grid_view.js', ASSET_JS);
        if($this->data['current_language']=='de'){
            $this->load_assets('lang/kendo.de-DE.js', ASSET_KENDO_JS);
        }
        $this->data['limit'] = $this->_limit;
        $this->view          = 'user/index';
        $this->layout        = 'layouts/module';
    }

    function add() {
        $this->_update();
    }

    function edit($id = 0) {
        $this->_edit_flag = true;
        $this->_update($id);
    }

    function _update($id = NULL) {
        $this->load_assets('select2/select2.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('select2/select2.min.js', ASSET_JS);

        if (!$this->input->is_ajax_request()) {
            redirect(create_url('sys/user'));
        }

        # Libs
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        if (!is_null($id)) {
            $id = intval($id);
            if ($id > 0) {
                $this->data['id'] = $id;
            } else {
                $id = 0;
            }
        }
        # Data
        if ($this->_edit_flag) {
            $api_param = array(
                'id' => $id,
                'id_city'    => $this->_id_city
            );
            $api_result = $this->rest->get('web/user', $api_param, 'json');
            $api_status = $this->rest->status();
            if ( isset($api_result->data) AND !empty($api_result->data) ) {
                $data               = $api_result->data;
                $this->data['data'] = $data[0];
            }
        }
        $flag = FALSE;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            # Post data
            $username = trim($this->input->post('username'));
            $email    = trim($this->input->post('email'));
            $group    = intval($this->input->post('group'));
            $company  = intval($this->input->post('company'));
            $password = $this->input->post('password');
            $passconf = $this->input->post('passconf');

            # Validation
            $this->form_validation->CI =& $this;
            if ($this->_edit_flag) {
                if (isset($data[0]->username) AND $data[0]->username!=$username) {
                    $this->form_validation->set_rules('username', lang('lbl_username'), 'callback_unique_check[username,'.lang("err_unique_username").']');
                }
                if (isset($data[0]->email) AND $data[0]->email!=$email) {
                    $this->form_validation->set_rules('email', lang('lbl_email'), 'required|valid_email|callback_unique_check[email,'.lang("err_unique_email").']');
                }
            } else{
                $this->form_validation->set_rules('username', lang('lbl_username'), 'callback_unique_check[username,'.lang("err_unique_username").']');
                $this->form_validation->set_rules('email', lang('lbl_email'), 'required|valid_email|callback_unique_check[email,'.lang("err_unique_email").']');
            }
            $this->form_validation->set_rules('company',lang('lbl_company'), 'required');
            $this->form_validation->set_rules('group',lang('lbl_group'), 'required');

            if ($this->_edit_flag) {
                $this->form_validation->set_rules('password', lang('lbl_password'), '|matches[passconf]');
                $this->form_validation->set_rules('passconf', lang('lbl_password_confirm'), 'matches[password]');
            } else {
                $this->form_validation->set_rules('password', lang('lbl_password'), 'required|matches[passconf]');
                $this->form_validation->set_rules('passconf', lang('lbl_password_confirm'), 'required');
            }

            if ($this->form_validation->run() === TRUE) {
                $api_param   = array(
                    'id'            => $id,
                    'id_user_group' => $group,
                    'id_company'    => $company,
                    'id_city'       => $this->_id_city,
                    'username'      => $username,
                    'email'         => $email,
                    'password'      => $password,
                    'passconf'      => $passconf,
                );
                if ($this->_edit_flag) {
                    $api_param['id'] = $id;
                    $api_result = $this->rest->put('web/user?id_city='.$this->_id_city, $api_param, 'json');
                } else {
                    $api_result = $this->rest->post('web/user?id_city='.$this->_id_city, $api_param, 'json');
                }

                $api_status  = $this->rest->status();
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $this->data['msg'] = lang('msg_user_saved');
                    $flag = TRUE;
                }
                else {
                    $this->data['err'] = lang('err_api_save');
                }
            }
        }

        # User groups
        $this->data['groups']   = $this->_get_groups();

        #companies by city
        $this->data['companies']   = $this->_get_companies($this->_id_city);
        $this->data['edit_flag'] = $this->_edit_flag;
        $this->data['flag']      = $flag;
        $this->layout = false;
        $this->view   = 'user/detail';
    }

    function delete($id = 0) {
        $return = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = intval($id);
            if ($id > 0) {
                $api_param = array(
                    'id' => $id
                );
                $api_result = $this->rest->delete('web/user?id_city='.$this->_id_city, $api_param, 'json');
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                    $return['status'] = true;
                    $return['msg']    = lang('msg_user_deleted');
                } else {
                    $return['status'] = false;
                    $return['msg']    = lang('err_api_save');
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
        echo json_encode($return);
    }

    #companies
    function _get_companies($id_city) {
        $companies = array();
        $api_param = array(
            'id_city' => $id_city,
        );
        $api_result_companies = $this->rest->get('web/company/index',$api_param);
        $api_status_companies = $this->rest->status();
        if ( isset($api_result_companies->status) AND ($api_result_companies->status == REST_STATUS_SUCCESS) AND (isset($api_result_companies->data)) AND (!empty($api_result_companies->data)) ) {
            foreach ($api_result_companies->data as $key => $value) {
                $companies[$value->id] = $value;
            }
        }
        return $companies;
    }

    # Groups
    function _get_groups() {
        $groups     = array();
        $api_param  = array();
        $api_result_groups = $this->rest->get('web/user_group/indexall?id_city='.$this->_id_city);
        $api_status_groups = $this->rest->status();
        if ( isset($api_result_groups->status) AND ($api_result_groups->status == REST_STATUS_SUCCESS) AND (isset($api_result_groups->data)) AND (!empty($api_result_groups->data)) ) {
            foreach ($api_result_groups->data as $key => $value) {
                $groups[$value->id] = $value;
            }
        }
        return $groups;
    }

    # Citys
    function _get_citys() {
        $citys      = array();
        $api_param  = array();
        $api_result_citys = $this->rest->get('web/v1/city/indexall');
        $api_status_citys = $this->rest->status();
        if ( isset($api_result_citys->status) AND ($api_result_citys->status == REST_STATUS_SUCCESS) AND (isset($api_result_citys->data)) AND (!empty($api_result_citys->data)) ) {
            foreach ($api_result_citys->data as $key => $value) {
                $citys[$value->id] = $value;
            }
        }
        return $citys;
    }

    function _valid_defaul($field) {
        if (intval($field) == 0) {
            $this->form_validation->set_message('_valid_defaul', lang('required'));
            return false;
        }
        return true;
    }

    function unique_check($check,$params) {
        list($fieldname, $message) = explode(',', $params);
        $api_param  = array(
            "fieldname" => $fieldname,
            "check"     => $check,
            "id_city"   => $this->_id_city,
        );
        $api_result = $this->rest->get('web/user/unique_check',$api_param);
        if($api_result->result==false){
            $this->form_validation->set_message('unique_check',$message);
        }
        return $api_result->result;
    }

    function connect_database_check(){
        $api_param = array('id_city'=>$this->_id_city);
        $api_result= $this->rest->get('web/v1/city/connect',$api_param,'json');
        if(!empty($api_result)){
            $msg = "ket noi kha thi";
        }
        else {
            $msg = "ket noi khong kha thi";
        }
        return $msg;
    }

    function _create_password($password,$salt){
        $api_param  = array(
            "password"  =>$password,
            "salt"      =>$salt,
            "id_city"   =>$this->_id_city,
        );
        $api_result_citys = $this->rest->get('web/user/check_password',$api_param,'json');
        return $api_result_citys->password;
    }

    function old_password_check($old_password){
        $check_password = $this->_create_password($old_password, session_login(FALSE)->salt);
        if($check_password!=session_login(FALSE)->password){
            $this->form_validation->set_message('old_password_check', lang('msg_old_password'));
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Profile
     * @param  string $type
     * @updated : Chien Tran
     */
    function profile($type = NULL) {
        # Load assets
        $this->load_assets('bootstrap-switch/bootstrap-switch.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('bootstrap-switch/bootstrap-switch.js', ASSET_JS);
        $this->load_assets('bootstrap-tokenfield.js', ASSET_JS);
        $this->load_assets('bootstrap-tokenfield.css', ASSET_CSS);
        $this->load_assets('openlayer/OpenLayers.js', ASSET_JS);

        # Language
        $this->load->language('city');
        $this->load->language('profiler');

        # Formvalidation
        $this->load->helper(array('form', 'url', 'date'));
        $this->load->library('form_validation');
        $this->load->helper('string');

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->CI =& $this;

            switch ($type) {
                case 'database':
                    $hostname       = $this->input->post('hostname');
                    $username       = $this->input->post('username');
                    $password       = $this->input->post('password');
                    $passconf       = $this->input->post('passconf');
                    $database       = $this->input->post('database');
                    $port           = $this->input->post('port');
                    $check_database = $this->input->post('check_database');
                    $check_database = 1;

                    $this->form_validation->set_rules('hostname', lang('city_lbl_hostname'), 'trim|required');
                    $this->form_validation->set_rules('username', lang('city_lbl_username'), 'trim|required');
                    $this->form_validation->set_rules('password', lang('city_lbl_password'), 'trim|required');
                    $this->form_validation->set_rules('port', lang('city_lbl_password'), 'trim|required');
                    $this->form_validation->set_rules('database', lang('city_lbl_database'), 'trim|required');
                    $this->form_validation->set_rules('password', lang('city_lbl_password'), 'required|matches[passconf]');
                    $this->form_validation->set_rules('passconf', lang('city_lbl_password'), 'required');

                    if ($this->form_validation->run() === TRUE) {
                        $api_param   = array(
                            'hostname' => $hostname,
                            'username' => $username,
                            'password' => $password,
                            'passconf' => $passconf,
                            'database' => $database,
                            'port'     => $port,
                        );
                        $api_result = $this->rest->put('web/v1/city?id_city='.$this->_id_city, $api_param, 'json');
                        $api_status = $this->rest->status();
                        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                            $this->data['msg'] = lang('msg_database_save');
                        } else {
                            $this->data['err'] = lang('err_api_save');
                        }
                    }
                    break;

                case 'storage':
                    # storage config
                    $check_storage    = $this->input->post('check_storage');
                    $check_storage    = ($check_storage == 'on') ? true : false;
                    $storage_type     = $this->input->post('storage_type');
                    $storage_ip       = $this->input->post('storage_ip');
                    $storage_username = $this->input->post('storage_username');
                    $storage_password = $this->input->post('storage_password');
                    $storage_passconf = $this->input->post('storage_passconf');
                    $storage_port     = $this->input->post('storage_port');
                    $storage_home_dir = $this->input->post('storage_home_dir');

                    $this->form_validation->set_rules('storage_type', lang('city_lbl_type'), 'trim|required');
                    $this->form_validation->set_rules('storage_ip', lang('city_lbl_ip'), 'trim|required');
                    $this->form_validation->set_rules('storage_username', lang('city_lbl_username'), 'trim|required');
                    $this->form_validation->set_rules('storage_port', lang('city_lbl_port'), 'trim|required');
                    $this->form_validation->set_rules('storage_home_dir', lang('city_lbl_home_dir'), 'trim|required');
                    $this->form_validation->set_rules('storage_password', lang('city_lbl_password'), 'trim|required|matches[storage_passconf]');
                    $this->form_validation->set_rules('storage_passconf', lang('city_lbl_password'), 'trim|required');

                    if ($this->form_validation->run() === TRUE) {
                        $api_result = $this->rest->put('web/v1/city?id_city='.$this->_id_city, $api_param, 'json');
                        $api_status = $this->rest->status();
                        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                            $this->data['msg'] = lang('msg_storage_save');
                        } else {
                            $this->data['err'] = lang('err_api_save');
                        }
                    }
                    break;

                case 'notification':
                    $check_notification = $this->input->post('check_notification');
                    $check_notification = $check_notification == 'on' ? true : false;
                    $notification_email = $this->input->post('notification_email');
                    $extra_email_array  = array();
                    $extra_email_array  = explode(", ",$notification_email[0]);

                    $extra_email = null;
                    foreach ($extra_email_array as $key => $value) {
                        if(filter_var($value, FILTER_VALIDATE_EMAIL)==false){
                            unset($extra_email_array[$key]);
                        }
                    }
                    $extra_email = implode(",",$extra_email_array);

                    $api_param = array(
                        'id'            => session_login(FALSE)->id,
                        'notification'  => $check_notification,
                        'extra_email'   => $extra_email,
                    );

                    $api_result = $this->rest->put('web/user?id_city='.$this->_id_city, $api_param, 'json');

                    $api_status = $this->rest->status();
                    if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                        $this->data['msg'] = lang('profile_msg_notification_save');
                        session_login(FALSE)->notification  = $check_notification;
                        session_login(FALSE)->extra_email   = $extra_email;
                    } else{
                        $this->data['err'] = lang('err_api_save');
                    }
                    break;

                case 'timezone' :
                    $timezone = $this->input->post('timezone');
                    $api_param = array(
                        'id'       => session_login(FALSE)->id,
                        'timezone' => $timezone
                    );
                    $api_result = $this->rest->put('web/user?id_city='.$this->_id_city, $api_param, 'json');
                    $api_status = $this->rest->status();
                    if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                        $this->data['msg']             = lang('profile_msg_notification_save');
                        session_login(FALSE)->timezone = $timezone;
                    } else{
                        $this->data['err'] = lang('err_api_save');
                    }
                    break;

                case 'post-service':
                    $url      = $this->input->post('url');
                    $dir_path = $this->input->post('dir_path');
                    $this->form_validation->set_rules('url', lang('profile_post_service_url'), 'trim|required|valid_url');
                    $this->form_validation->set_rules('dir_path', lang('profile_post_service_dir'), 'trim|required');
                    if ($this->form_validation->run() !== false) {
                        $api_param = array(
                            'url'      => $url,
                            'dir_path' => $dir_path
                        );
                        $api_result = $this->rest->put('web/v1/city/service?id_city='.$this->_id_city, $api_param, 'json');
                        $api_status = $this->rest->status();
                        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                            $this->data['msg']             = lang('profile_post_service_msg');
                        } else{
                            $this->data['err'] = lang('err_api_save');
                        }
                    } else {
                        $this->data['err'] = lang('err_api_save');
                    }
                    break;

                case 'option':
                    $check_material  = $this->input->post('check_material');
                    $check_material  = ($check_material == 'on') ? true : false;
                    $check_selection = $this->input->post('check_selection');
                    $check_selection = ($check_selection == 'on') ? true : false;

                    $api_param = array(
                        'id_city'  => $this->_id_city,
                        'material' => $check_material,
                        'selection'=> $check_selection,
                    );
                    $api_result = $this->rest->put('web/v1/city?id_city='.$this->_id_city, $api_param, 'json');
                    $api_status = $this->rest->status();
                    if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                        $this->data['msg'] = lang('city_msg_save_successful');
                    }
                    break;

                case 'change-password':
                    // Post data
                    $old_password = $this->input->post('old_password');
                    $password     = $this->input->post('password');
                    $passconf     = $this->input->post('passconf');

                    // Validate
                    $this->form_validation->set_rules('old_password', lang('lbl_old_password'), 'required');
                    $this->form_validation->set_rules('password', lang('lbl_password'), 'required|matches[passconf]');
                    $this->form_validation->set_rules('passconf', lang('lbl_password_confirm'), 'required|matches[password]');

                    if ($this->form_validation->run() !== false) {
                        $api_param = array(
                            'id'       => session_login(FALSE)->id,
                            'password' => $password,
                            'passconf' => $passconf,
                        );
                        $api_result = $this->rest->put('web/user?id_city='.$this->_id_city, $api_param, 'json');
                        $api_status = $this->rest->status();
                        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                            $this->data['msg'] = lang('msg_user_saved');
                            $flag = TRUE;
                        } else {
                            $this->data['err'] = lang('err_api_save');
                        }
                    } else {
                    }
                    break;

                case NULL:
                    $city_name = $this->input->post('general_txt_city');
                    $this->form_validation->set_rules('general_txt_city', lang('lbl_city'), 'trim|required|xss_clean');
                    if ($this->form_validation->run() === true) {
                        $api_param = array(
                            'id'        => session_login(FALSE)->id,
                            'city_name' => $city_name,
                        );
                        $api_result = $this->rest->put('web/city?id_city='.$this->_id_city, $api_param, 'json');
                        $api_status = $this->rest->status();
                        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                            $this->data['msg'] = lang('msg_user_saved');
                            $flag = TRUE;
                        } else {
                            $this->data['err'] = lang('err_api_save');
                        }
                    } else {}
                    break;

                default:
                    $old_password = $this->input->post('old_password');
                    $new_password = $this->input->post('password');
                    $passconf     = $this->input->post('passconf');
                    $this->form_validation->set_rules('old_password', lang('lbl_password'), 'trim|required|callback_old_password_check');
                    $this->form_validation->set_rules('password', lang('lbl_password'), 'trim|required|matches[passconf]');
                    $this->form_validation->set_rules('passconf', lang('lbl_new_password_confirm'), 'trim|required');
                    break;
            }
        }

        $data = array();
        $api_result = $this->rest->get('web/v1/city?id_city=' . $this->_id_city, array(), 'json');
        $api_status = $this->rest->status();
        if ( isset($api_result->data) AND !empty($api_result->data) ) {
            $data = $api_result->data;
            $data = $data[0];

            // post-service decode
            if (isset($data->post_service) AND !empty($data->post_service)) {
                $data->post_service = json_decode($data->post_service);
            }

            // database decode
            if (isset($data->cf_database) AND !empty($data->cf_database)) {
                $data->cf_database = json_decode($data->cf_database);
            }
        }
        $this->data['data'] = $data;
        $this->data['type'] = $type;
        $this->view         = 'user/profile';
    }

    function checkconnect() {
        $_hostname  = $_POST['hostname'];
        $_username  = $_POST['username'];
        $_password  = $_POST['password'];
        $_database  = $_POST['database'];
        $_port      = $_POST['port'];
        $api_param = array(
                'hostname'=>$_hostname,
                'username'=>$_username,
                'password'=>$_password,
                'database'=>$_database,
                'port'    =>$_port
        );
        $api_result= $this->rest->post('web/v1/city/connect?id_city='.$this->_id_city,$api_param,'json');

        if(isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS)){
            $return = "success";
        }else {
            $return = "fail";
        }
        echo $return;
    }

    function save_long_lat(){
        $long   = $this->input->get('lon');
        $lat    = $this->input->get('lat');
        $api_param = array(
            'lon' => $long,
            'lat' => $lat,
        );
        $api_result = $this->rest->put('web/v1/city?id_city='.$this->_id_city, $api_param, 'json');
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
            $return =  lang('profile_msg_coordinate_save');
        } else {
            $return = lang('err_has_an_error_when_update_database');
        }
        echo $return;
    }
}
