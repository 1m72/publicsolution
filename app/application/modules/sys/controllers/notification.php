<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Notification Controller
 * @author chientran <tran.duc.chien@kloon.vn>
 * @created 1 Nov 2013
 */
class Notification extends MY_Controller {
    private $_id_city = 1;
    private $_limit       = 20;

    public function __construct() {
        parent::__construct();
        $this->load->helper('text');

        // $this->_id_city
        $this->_id_city = current_city();
    }

    function post_service() {
        // Language
        $this->load->language('notification');

        $page = intval($this->input->get('page')) ? $this->input->get('page') : 1;

        // API params
        $api_params = array(
            'id_city' => $this->_id_city,
            'limit'   => $this->_limit,
            'offset'  => (($page - 1) * $this->_limit)
        );

        // Query string
        $filter_time = $this->input->get('time');
        $by_time     = explode('.', $filter_time);
        if (is_array($by_time) AND (count($by_time) == 3)) {
            $by_time = strtotime("{$by_time[2]}-{$by_time[1]}-{$by_time[0]}");
            if ($by_time !== false) {
                $api_params['time'] = date('Y-m-d', $by_time);
            }
        }

        $data  = array();
        $total = 0;
        $api_result = $this->rest->get('web/v2/post_service', $api_params, 'json');
        $api_status = $this->rest->status();
        if (isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND isset($api_result->data)) {
            $data  = isset($api_result->data) ? $api_result->data : array();
            $total = $api_result->total;
        }
        // _debug($data);die;

        // Lib
        $this->load->library('pagination');
        $config['base_url']             = current_url_with_params(array(), array('page'));
        $config['first_link']           = $config['base_url'];
        $config['total_rows']           = $total;
        $config['per_page']             = $this->_limit;
        $config['page_query_string']    = true;
        $config['query_string_segment'] = 'page';
        $config['use_page_numbers']     = TRUE;
        $config['num_links']            = 5;
        $config['full_tag_open']        = '<ul class="pagination pagination-md" style="margin: 20px 0px 0px 0px">';
        $config['full_tag_close']       = '</ul>';
        $config['first_link']           = '<<';
        $config['first_tag_open']       = '<li>';
        $config['first_tag_close']      = '</li>';
        $config['last_link']            = '>>';
        $config['last_tag_open']        = '<li>';
        $config['last_tag_close']       = '</li>';
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';
        $config['next_link']            = '&gt;';
        $config['next_tag_open']        = '<li>';
        $config['next_tag_close']       = '</li>';
        $config['prev_link']            = '&lt;';
        $config['prev_tag_open']        = '<li>';
        $config['prev_tag_close']       = '</li>';
        $config['cur_tag_open']         = '<li class="active"><a href="javascript: void(0);">';
        $config['cur_tag_close']        = '</a></li>';
        $this->pagination->initialize($config);

        $this->data['filter_time'] = $filter_time;
        $this->data['result']      = $data;
        $this->view                = 'notification/post_service';
    }
}