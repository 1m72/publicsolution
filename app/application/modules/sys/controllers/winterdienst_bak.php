<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Winterdienst Controller
 * @author chientran <tran.duc.chien@kloon.vn>
 * @created 5 Nov 2013
 */
class Winterdienst extends MY_Controller {
    private $_id_city             = 1;
    private $_app_winterdienst    = 1;
    private $_app_street_cleaning = 2;
    private $_app_street_checking = 3;
    function __construct() {
        parent::__construct();

        # Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        # Language
        $this->load->language('winterdienst', $this->_global_language);

        # id_city
        $this->_id_city = current_city();

        $nav_module_current = isset($this->data['nav_module_current']) ? $this->data['nav_module_current'] : false;
        if ($nav_module_current) {
            $nav_module_current = intval($nav_module_current);
            $nav_module_current = ($nav_module_current > 0) ? $nav_module_current : 1;
        } else {
            $nav_module_current = 1;
        }
        $this->data['nav_module_current'] = $nav_module_current;
    }

    function detail_by_gps(){
        $id_gps = $_GET['id_wkgps'];
        if(!isset($id_gps)  OR empty($id_gps)){
            echo json_encode(array());
        }
        $api_param = array(
            'id_city' => $this->_id_city,
            'id_wkgps'=> $id_gps,
        );
        $data    = array();
        $api_result  = $this->rest->get('web/v2/gps/record_detail_by_gps', $api_param, 'json');
        $api_status  = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            $data = $api_result->data;
        }
        if(isset($data) AND !empty($data)){
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }

    function index($app = NULL) {
        $this->index2($app);
        return;

        # Breadcrumb
        $this->breadcrumb->append_crumb('Winterdienst', create_url('sys/visualization'));

        # Assets
        $this->load_assets('winterdienst.css', ASSET_CSS);
        $this->load_assets('select2/select2.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('projekktor/themes/maccaco/projekktor.style.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('select2/select2.min.js', ASSET_JS);
        $this->load_assets('jquery.slimscroll.min.js', ASSET_JS);
        $this->load_assets('openlayer/OpenLayers.js', ASSET_JS);
        $this->load_assets('projekktor/projekktor-1.3.00.min.js', ASSET_JS);
        $this->load_assets('functions.js', ASSET_JS);
        // $this->load_assets('winterdienst.js', ASSET_JS);
        $this->load_assets('openlayer.js', ASSET_JS);

        # Worker
        $workers    = array();
        $api_param  = array(
            'id_city' => $this->_id_city
        );
        $api_result = $this->rest->get('web/v1/worker/index', $api_param, 'json');
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $workers[$value->id] = $value;
            }
        }
        $this->data['workers'] = $workers;

        # Machine
        $machines  = array();
        $api_param = array(
            'id_city' => $this->_id_city
        );
        $api_result = $this->rest->get('web/v1/machine/index', $api_param, 'json');
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $machines[$value->id] = $value;
            }
        }
        $this->data['machines'] = $machines;

        # Street
        $streets    = array();
        $api_param  = array(
            'id_city' => $this->_id_city
        );
        $api_result = $this->rest->get('web/v1/street/index', $api_param, 'json');
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $streets[] = $value;
            }
        }
        $this->data['streets'] = $streets;

        # GPS
        $gps       = array();
        $api_param = array(
            'id_city' => $this->_id_city,
            // 'offset'  => 0,
            // 'limit'   => 30
        );

        # Modules
        $module = intval($this->input->get('module'));
        if ($module AND (module_list_array($module) !== false)) {
            $api_param['id_module'] = $module;
            $this->data['module']   = $module;
        }

        # by_date param
        $by_date = $this->input->get('by_date');
        if ($by_date === false) {
            $by_date = date('d.m.Y');
        } else {
            $by_date = trim($by_date);
        }
        $by_date_array = explode('.', $by_date);
        $by_date_timestamp = FALSE;
        if ( is_array($by_date_array) AND (count($by_date_array) == 3) ) {
            $by_date_timestamp = strtotime("{$by_date_array[2]}-{$by_date_array[1]}-{$by_date_array[0]}");
            $tmp               = date('d.m.Y',  $by_date_timestamp);
            if ($by_date === $tmp) {
                $api_param['day']      = date('Y-m-d', $by_date_timestamp);
                $this->data['by_date'] = $by_date;
            }
        }
        $this->data['current_time'] = $by_date_timestamp ? $by_date_timestamp : time();

        # by_worker param
        $by_worker = intval($this->input->get('by_worker'));
        if ($by_worker <= 0) {
            $by_worker = FALSE;
        } else {
            $api_param['worker']     = $by_worker;
            $this->data['by_worker'] = $by_worker;
        }

        # by_worker param
        $by_machine = intval($this->input->get('by_machine'));
        if ($by_machine <= 0) {
            $by_machine = FALSE;
        } else {
            $api_param['machine']     = $by_machine;
            $this->data['by_machine'] = $by_machine;
        }

        # by_street
        $by_street = $this->input->get('by_street');
        if (!isset($by_street) || empty($by_street)) {
            $by_street = FALSE;
        } else {
            $api_param['street']     = $by_street;
            $this->data['by_street'] = $by_street;
        }

        # Get gps data with condition
        #$api_result = $this->rest->get('web/gps/index_gps', $api_param, 'json');
        $api_result = $this->rest->get('web/v2/gps/index', $api_param, 'json');
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            $gps = $api_result->data;
        }
        $this->data['gps'] = $gps;

        # GPS data for javascript
        $gps_data   = array();
        $activities = array();
        foreach ($gps as $key => $value) {
            $worker_info = isset($workers[$value->id_worker]) ? $workers[$value->id_worker] : false;
            $item = array(
                'worker'    => $worker_info ? $worker_info->first_name . ' ' . $worker_info->last_name : '',
                'starttime' => '',
                'endtime'   => '',
                'date'      => date('d.m.Y', strtotime($value->ontime)),
                'position'  => $this->_position_process($value->position),
                'status'    => isset($value->status) ? $value->status : '',
                'activity'  => isset($value->activity) ? $value->activity : '',
                'infor'     => isset($value->information) ? $value->information : ''
            );
            if (isset($value->activity)) {
                foreach ($value->activity as $sub_key => $sub_value) {
                    $activities[] = $sub_value;
                }
            }
            $gps_data[$value->id] = $item;
        }
        $this->data['gps_position_data'] = $gps_data;

        # Default map
        $this->data['defaul_coordinate'] = $this->defaul_coordinate();

        # View
        $this->data['module_list_array'] = module_list_array();
        $this->view = 'winterdienst/index';
    }

    function index2($app = NULL){
        # Breadcrumb
        $this->breadcrumb->append_crumb('Winterdienst', create_url('sys/visualization'));

        # Assets
        $this->load_assets('winterdienst.css', ASSET_CSS);
        $this->load_assets('select2/select2.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('projekktor/themes/maccaco/projekktor.style.css', ASSET_CSS, ASSET_JS);
        $this->load_assets('select2/select2.min.js', ASSET_JS);
        $this->load_assets('jquery.slimscroll.min.js', ASSET_JS);
        $this->load_assets('openlayer/OpenLayers.js', ASSET_JS);
        $this->load_assets('projekktor/projekktor-1.3.00.min.js', ASSET_JS);
        $this->load_assets('functions.js', ASSET_JS);
        $this->load_assets('openlayer.js', ASSET_JS);

        # Worker
        $workers    = array();
        $api_param  = array(
            'id_city' => $this->_id_city
        );
        $api_result = $this->rest->get('web/v1/worker/index', $api_param, 'json');
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $workers[$value->id] = $value;
            }
        }
        $this->data['workers'] = $workers;

        # Machine
        $machines  = array();
        $api_param = array(
            'id_city' => $this->_id_city
        );
        $api_result = $this->rest->get('web/v1/machine/index', $api_param, 'json');
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $machines[$value->id] = $value;
            }
        }
        $this->data['machines'] = $machines;

        # Street
        $streets    = array();
        $api_param  = array(
            'id_city' => $this->_id_city
        );
        $api_result = $this->rest->get('web/v1/street/index', $api_param, 'json');
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            foreach ($api_result->data as $key => $value) {
                $streets[] = $value;
            }
        }
        $this->data['streets'] = $streets;

        # Param
        $api_param = array(
            'id_city' => $this->_id_city,
            // 'offset'  => 0,
            // 'limit'   => 30
        );

        # Modules
        $module = intval($this->input->get('module'));
        if ($module AND (module_list_array($module) !== false)) {
            $api_param['id_module'] = $module;
            $this->data['module']   = $module;
        }

        # by_date param
        $by_date = $this->input->get('by_date');
        if ($by_date === false) {
            $by_date = date('d.m.Y');
        } else {
            $by_date = trim($by_date);
        }
        $by_date_array = explode('.', $by_date);
        $by_date_timestamp = FALSE;
        if ( is_array($by_date_array) AND (count($by_date_array) == 3) ) {
            $by_date_timestamp = strtotime("{$by_date_array[2]}-{$by_date_array[1]}-{$by_date_array[0]}");
            $tmp               = date('d.m.Y',  $by_date_timestamp);
            if ($by_date === $tmp) {
                $api_param['day']      = date('Y-m-d', $by_date_timestamp);
                $this->data['by_date'] = $by_date;
            }
        }
        $this->data['current_time'] = $by_date_timestamp ? $by_date_timestamp : time();

        # by_worker param
        $by_worker = intval($this->input->get('by_worker'));
        if ($by_worker <= 0) {
            $by_worker = FALSE;
        } else {
            $api_param['worker']     = $by_worker;
            $this->data['by_worker'] = $by_worker;
        }

        # by_worker param
        $by_machine = intval($this->input->get('by_machine'));
        if ($by_machine <= 0) {
            $by_machine = FALSE;
        } else {
            $api_param['machine']     = $by_machine;
            $this->data['by_machine'] = $by_machine;
        }

        # by_street
        $by_street = $this->input->get('by_street');
        if (!isset($by_street) || empty($by_street)) {
            $by_street = FALSE;
        } else {
            $api_param['street']     = $by_street;
            $this->data['by_street'] = $by_street;
        }

        # GPS
        $gps       = array();
        $api_result = $this->rest->get('web/v2/gps/record_gps', $api_param, 'json');
        $api_status = $this->rest->status();
        if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
            $gps = $api_result->data;
        }
        $this->data['gps'] = $gps;
        $gps_data   = array();
        $activities = array();
        foreach ($gps as $key => $value) {
            $worker_info = isset($workers[$value->id_worker]) ? $workers[$value->id_worker] : false;
            $item = array(
                'worker'    => $worker_info ? $worker_info->first_name . ' ' . $worker_info->last_name : '',
                'starttime' => '',
                'endtime'   => '',
                'date'      => date('d.m.Y', strtotime($value->ontime)),
                'position'  => $this->_position_process($value->position),
                'status'    => isset($value->status) ? $value->status : ''
            );
            $gps_data[$value->id] = $item;
        }
        $this->data['gps_data'] = $gps_data;

        # Default
        $this->data['defaul_coordinate'] = $this->defaul_coordinate();

        # View
        $this->data['module_list_array'] = module_list_array();
        $this->view                      = 'winterdienst/index2';
        $this->layout                    = 'layouts/module';
    }

    function detail() {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $linestring = trim($this->input->post('linestring'));
            $gps        = intval($this->input->post('gps'));
            $return     = array();
            $linestring_array = explode('(', $linestring);
            if ( is_array($linestring_array) AND (count($linestring_array) > 1) ) {
                switch ($linestring_array[0]) {
                    case 'LINESTRING':
                        $data = trim(trim($linestring_array[1], ')'));
                        $data = explode(',', $data);
                        if (!empty($data)) {
                            foreach ($data as $key => $value) {
                                $item     = explode(' ', trim($value));
                                $item[]   = $gps;
                                $return[] = $item;
                            }
                        }
                        break;

                    default:
                        # code...
                        break;
                }
            }
            /*$return = array(
                array('105.825', '21.039'),
                array('105.828', '21.033'),
            );*/
            echo json_encode($return);
            $this->view   = FALSE;
            $this->layout = FALSE;
        } else {
            redirect(create_url('sys/visualization'));
        }
    }

    function information() {
        $this->layout = FALSE;

        if ($this->input->is_ajax_request() AND ($_SERVER['REQUEST_METHOD'] == 'POST') ) {
            $id_gps         = $this->input->post('gps');
            $start_time     = $this->input->post('start_time');
            $end_time       = $this->input->post('end_time');
            $latitude       = $this->input->post('latitude');
            $longtitude     = $this->input->post('longtitude');
            $type           = $this->input->post('type');
            #echo $latitude . ' --- ' . $longtitude;
            #die;
            if(!isset($type) || empty($type)) {
                $type = 'info';
            }

            if ($id_gps) {
                $id_gps      = intval($id_gps);
                $api_param = array(
                        'id_city'       => $this->_id_city,
                        'id_gps'        => $id_gps,
                        'start_time'    => $start_time,
                        'end_time'      => $end_time,
                        'lat'           => $latitude,
                        'lon'           => $longtitude
                );

                # get location
                $api_result = $this->rest->get('web/v1/infor/get_location_by_activity', $api_param, 'json');
                $api_status = $this->rest->status();
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->location)) AND (!empty($api_result->location)) ) {
                    $location = $api_result->location;
                    $this->data['location']     = $location;
                }

                # get information
                $api_result = $this->rest->get('web/v1/infor/get_information_by_activiti', $api_param, 'json');
                $api_status = $this->rest->status();

                $message = array();
                $voice   = array();
                $image   = array();
                $video   = array();
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
                    $data = $api_result->data;
                    foreach ($data as $value)
                    {
                        $item = array(
                            'type'          => isset($value->type) ? $value->type : '',
                            'time'          => isset($value->time) ? $value->time : '',
                            'data'          => isset($value->data) ? $value->data : '',
                            'updated_at'    => isset($value->updated_at) ? $value->updated_at : '',
                            'created_at'    => isset($value->created_at) ? $value->created_at : '',
                        );
                        if (isset($value->thumbs)) {
                            $item['thumbs'] = $value->thumbs;
                        }

                        switch ($value->type)
                        {
                            case INFO_MESSAGE:
                                array_push($message,$item);
                                break;
                            case INFO_VOICE:
                                array_push($voice,$item);
                                break;
                            case INFO_IMAGE:
                                array_push($image,$item);
                                break;
                            case INFO_VIDEO:
                                array_push($video,$item);
                                break;
                            default:
                                break;
                        }
                    }
                    $this->data['message'] = $message;
                    $this->data['voice']   = $voice;
                    $this->data['image']   = $image;
                    $this->data['video']   = $video;
                    $this->data['worker']  = $api_result->worker;
                    $this->data['object']  = $api_result->object;
                    $this->data['start']   = $api_result->starttime;
                    $this->data['end']     = $api_result->endtime;
                }

                # View
                $tab_select = trim($this->input->post('select'));
                if (!in_array($tab_select, array('message', 'voice', 'video', 'image'))) {
                    $tab_select = 'message';
                }

                $this->data['tab_select'] = $tab_select;
                $this->data['type']       = $type;
                $this->view               = 'winterdienst/information';
            }
            }
            }

    function _information() {
        $this->layout = FALSE;

        if ($this->input->is_ajax_request() AND ($_SERVER['REQUEST_METHOD'] == 'POST') ) {
            $id_gps      = $this->input->post('gps');
            $id_activity = $this->input->post('activity'); # id_worker_activity
            $id_act      = $this->input->post('act'); # id_activity
            $latitude    = $this->input->post('latitude');
            $longtitude  = $this->input->post('longtitude');
            $type        = $this->input->post('type');

            if ($id_gps AND $id_activity !== false) {
                $id_gps      = intval($id_gps);
                $id_activity = intval($id_activity);
                $api_param = array(
                    'id_city'               => $this->_id_city,
                    'id_worker_gps'         => $id_gps,
                    'id_worker_activiti'    => $id_activity,
                    'latitude'              => $latitude,
                    'longtitude'            => $longtitude
                );
                $api_result = $this->rest->get('web/infor/infor_by_gps_activity', $api_param, 'json');
                $api_status = $this->rest->status();

                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->location)) AND (!empty($api_result->location)) ) {
                    $this->data['location'] = isset($api_result->location) ? $api_result->location : array();
                }

                $message = array();
                $voice   = array();
                $image   = array();
                $video   = array();
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
                    $data = $api_result->data;
                    foreach ($data as $value)
                    {
                        $item = array(
                            'type'       => isset($value->type) ? $value->type : '',
                            'time'       => isset($value->time) ? $value->time : '',
                            'data'       => isset($value->data) ? $value->data : '',
                            'updated_at' => isset($value->updated_at) ? $value->updated_at : '',
                            'created_at' => isset($value->created_at) ? $value->created_at : '',
                        );
                        if ($isset($value->thumbs)) {
                            $item['thumbs'] = $value->thumbs;
                        }

                        switch ($value->type)
                        {
                            case INFO_MESSAGE:
                                array_push($message,$item);
                                break;
                            case INFO_VOICE:
                                array_push($voice,$item);
                                break;
                            case INFO_IMAGE:
                                array_push($image,$item);
                                break;
                            case INFO_VIDEO:
                                array_push($video,$item);
                                break;
                            default:
                                break;
                        }
                    }
                    $this->data['message'] = $message;
                    $this->data['voice']   = $voice;
                    $this->data['image']   = $image;
                    $this->data['video']   = $video;
                    $this->data['worker']  = $api_result->worker;
                    $this->data['object']  = $api_result->object;
                    $this->data['start']   = $api_result->starttime;
                    $this->data['end']     = $api_result->endtime;
                }

                # View
                $tab_select = trim($this->input->post('select'));
                if (!in_array($tab_select, array('message', 'voice', 'video', 'image'))) {
                    $tab_select = 'message';
                }
                $this->data['tab_select'] = $tab_select;
                $this->data['type']       = $type;
                $this->view               = 'winterdienst/information';
            }
        }
    }

    function information_() {
        $id_worker_gps = intval($id_worker_gps);
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($id_worker_gps > 0) {
                $api_param  = array(
                    'id_worker_gps' => $id_worker_gps,
                    'id_city' => $this->_id_city
                );
                $api_result = $this->rest->get('web/infor/group_infor', $api_param, 'json');
                $api_status = $this->rest->status();

                $message    = array();
                $voice      = array();
                $image      = array();
                $video      = array();
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
                    $data       = $api_result->data;
                    $data_view  = $data[$index];
                    $data_infor = $data_view->infor;
                    foreach ($data_infor as $value)
                    {
                        $item = array(
                            'id'            => $value->id,
                            'id_city'       => $value->id_city,
                            'id_config'     => $value->id_config,
                            'id_worker_gps' => $value->id_worker_gps,
                            'type'          => $value->type,
                            'data'          => $value->data,
                            'updated_at'    => $value->updated_at,
                            'created_at'    => $value->created_at
                        );

                        switch ($value->type)
                        {
                            case INFO_MESSAGE:
                                array_push($message,$item);
                                break;
                            case INFO_VOICE:
                                array_push($voice,$item);
                                break;
                            case INFO_IMAGE:
                                array_push($image,$item);
                                break;
                            case INFO_VIDEO:
                                array_push($video,$item);
                                break;
                            default:
                                break;
                        }
                    }
                    #
                    $this->data['message'] = $message;
                    $this->data['voice']   = $voice;
                    $this->data['image']   = $image;
                    $this->data['video']   = $video;
                    $this->data['worker']  = $api_result->worker;
                    $this->data['object']  = $api_result->object;
                    $this->data['start']   = $api_result->starttime;
                    $this->data['end']     = $api_result->endtime;
                }

                $this->layout = false;
                $this->view   = 'winterdienst/information';
            } else {
                die('Err');
            }
        } else {
            $return = array();
            $api_param = array(
                'id_worker_gps' => $id_worker_gps,
                'id_city' => $this->_id_city
            );
            $api_result = $this->rest->get('web/infor/group_infor', $api_param, 'json');
            $api_status = $this->rest->status();
            if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) ) {
                $return = array(
                    'status' => true,
                    'data'   => array(
                        'total'     => $api_result->count,
                        'message'   => $api_result->message,
                        'video'     => $api_result->video,
                        'image'     => $api_result->image,
                        'voice'     => $api_result->voice,
                        'infor'     => $api_result->data,
                    )
                );
            } else {
                $return = array(
                    'status' => false
                );
            }
            echo json_encode($return);
        }
    }

    function geostreet() {
        $position_data = trim($this->input->post('data'));
        $position_data = trim($position_data, ",");
        $position_data = explode(',', $position_data);
        $result = array();
        foreach ($position_data as $key => $value) {
            $tmp = explode('|', $value);
            if (is_array($tmp) AND (count($tmp) == 3)) {
                $street_name = $this->_geostreet_service($tmp[1], $tmp[2]);
                if (isset($street_name[0]->name)) {
                    $street_name = $street_name[0]->name;
                } else {
                    $street_name = lang('unavailable');
                }
                $result[] = $tmp[0].'|'.$street_name;
            }
        }
        echo json_encode($result);
    }

    function _geostreet_service($latitude, $longtitude) {
        # http://services.gisgraphy.com/street/streetsearch?format=XML&from=1&to=1&lat=47.278238&lng=8.346222'
        $this->rest->initialize(array('server' => 'http://services.gisgraphy.com'));
        $api_param = array(
            'lat'    => $latitude,
            'lng'    => $longtitude,
            'format' => 'json',
            'from'   => 1,
            'to'     => 1,
        );
        $api_result = $this->rest->get('street/streetsearch', $api_param);
        return isset($api_result->result) ? $api_result->result : FALSE;
    }

    function _information_detail($id = 0) {
    }

    function _position_process($linestring) {
        $linestring_array = explode('(', $linestring);
        $return           = array();
        if ( is_array($linestring_array) AND (count($linestring_array) > 1) ) {
            switch ($linestring_array[0]) {
                case 'LINESTRING':
                    $data = trim(trim($linestring_array[1], ')'));
                    $data = explode(',', $data);
                    if (!empty($data)) {
                        foreach ($data as $key => $value) {
                            $item     = explode(' ', trim($value));
                            $return[] = $item;
                        }
                    }
                    break;

                default:
                    # code..
                    break;
            }
        }
        return $return;
    }

    function defaul_coordinate() {
        $api_param = array(
            'id_city' => $this->_id_city
        );
        $api_result = $this->rest->get('web/city', $api_param, 'json');
        $api_status = $this->rest->status();
        $return = array();
        if ( isset($api_result->status) AND ($api_result->status == TRUE) AND isset($api_result->data) AND !empty($api_result->data) ) {
            $data = $api_result->data[0];
            $return = array(
                'lat' => isset($data->lat) ? $data->lat : '',
                'lon' => isset($data->lon) ? $data->lon : '',
            );
        }
        return $return;
    }

    function gps_print($id_gps = 0) {
        $flag = false;
        if ($id_gps > 0) {
            $api_param  = array(
                'id_city' => $this->_id_city,
                'id_gps'  => $id_gps
            );
            $api_result = $this->rest->get('web/gps/gps_by_id', $api_param, 'json');
            $api_status = $this->rest->status();
            if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
                $flag = true;
                $this->data['data'] = $api_result->data[0];

                $api_param  = array(
                    'id_city' => $this->_id_city,
                    'id_gps'  => $id_gps
                );
                $api_result = $this->rest->get('web/gps/activity_by_wkgps', $api_param, 'json');
                $api_status = $this->rest->status();
                if ( isset($api_result->status) AND ($api_result->status == REST_STATUS_SUCCESS) AND (isset($api_result->data)) AND (!empty($api_result->data)) ) {
                    $this->data['activities'] = $api_result->data;
                } else {
                    $this->data['msg'] = lang('error_gps_activity_empty');
                }
            } else {
                $flag = false;
                $this->data['msg'] = lang('error_gps_activity_empty');
            }
        }

        if (!$flag) {
            $this->data['msg'] = lang('error_gps_404');
        }

        $this->data['status'] = $flag;
        $this->layout         = false;
        $this->view           = 'winterdienst/print';
    }

    function download() {
        $link = 'http://media.bis-office.com/1/2014/02/19/2/9abb1a8f6fd2897320140219150903_movie0.mp4';
        $data = file_get_contents($link);
        $name = 'mytext.txt';
        force_download($name, $data);
    }
}