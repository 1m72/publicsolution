<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Home Controller
 * @author chientran <tran.duc.chien@kloon.vn>
 * @created 1 Nov 2013
 */
class Home extends MY_Controller {
    public function __construct() {
        parent::__construct();

        # Language
        $this->load->language('home');

    }

    public function index() {
        $this->view = 'home/index';
    }

    function test() {
        // Run some setup
        $this->rest->initialize(array('server' => config_item('api_web')));

        // Pull in an array of tweets
        $api_param = array(
            'username' => 'admin',
            'password' => '123456'
        );

        # Call API. Available method :
        #   -> $this->rest->post
        #   -> $this->rest->get
        #   -> $this->rest->put
        #   -> $this->rest->delete
        $api_results = $this->rest->post('validate_code', $api_param, 'json');

        # Debug request
        $this->rest->debug();
    }

    function mail() {
        var_dump(mail('saobang_8908@yahoo.com.vn', 'Test email', 'Hello'));
    }

    function language() {
        #$this->load->helper('language');
        echo lang('email_format');
    }

    function error_404() {
        $this->breadcrumb->append_crumb('404', current_url());
        $this->view = 'home/error_404';
    }

    function error_403() {
        $this->breadcrumb->append_crumb('403', current_url());
        $this->view = 'home/error_403';
    }
}