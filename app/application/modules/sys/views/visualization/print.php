<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="http://publicsolution.dev/assets/v3/css/bootstrap.min.css">
    <title></title>
</head>
<body style="padding: 10px;">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <table class="table table-hover table-striped table-condensed" style="min-width: 1000px;">
                <thead>
                    <?php if (!isset($status) OR ($status != false)) : ?>
                    <tr>
                        <th colspan="6">
                            <div style="line-height: 25px;">
                                <span style="float: left; width: 100px;"><?php echo lang('time'); ?></span>
                                <span>: <?php echo isset($data->ontime) ? date('d.m.Y H:i', strtotime($data->ontime)) : ''; ?></span>
                            </div>
                            <div style="line-height: 25px;">
                                <span style="float: left; width: 100px;"><?php echo lang('crumb_worker'); ?></span>
                                <span>: <?php echo isset($data->first_name) ? ($data->first_name . ' ' .$data->last_name) : ''; ?></span>
                            </div>
                            <div style="line-height: 25px;">
                                <span style="float: left; width: 100px;"><?php echo lang('crumb_machine'); ?></span>
                                <span>: <?php echo isset($data->mcname) ? $data->mcname : ''; ?></span>
                            </div>
                        </th>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <th width="60">#</th>
                        <th width="170"><?php echo lang('lbl_start');?></th>
                        <th width="170"><?php echo lang('lbl_end');?></th>
                        <th width="100"><?php echo lang('lbl_duration');?></th>
                        <th width="180"><?php echo lang('lbl_activity');?></th>
                        <th><?php echo lang('lbl_street');?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (isset($activities) AND !empty($activities)) : ?>
                    <?php $counter = 0; foreach ($activities as $key => $value) : $counter++;?>
                    <tr>
                        <td><?php echo $counter; ?></td>
                        <td><?php echo $value->starttime;?></td>
                        <td><?php echo $value->endtime;?></td>
                        <td>
                            <?php
                            $tmp = strtotime($value->endtime) - strtotime($value->starttime);
                            echo $tmp ? sprintf("%02d%s%02d%s%02d",floor($tmp/3600), ':', ($tmp/60)%60, ':', $tmp%60) : '';
                            ?>
                        </td>
                        <td><?php echo $value->acname;?></td>
                        <td><?php echo $value->street;?></td>
                    </tr>
                    <?php endforeach; ?>
                    <?php else: ?>
                    <tr>
                        <td colspan="6"><?php echo isset($msg) ? $msg : ''; ?></td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>