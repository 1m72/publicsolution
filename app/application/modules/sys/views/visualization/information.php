<div class="modal-dialog" style="width: <?php echo (isset($type) AND ($type === 'info')) ? '1000px' : '800px'; ?>;">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="pull-right btn btn-sm btn-warning" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i>&nbsp;<?php echo $this->lang->line('lbl_close')?></button>
            <button type="button" class="close hide" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel"> <?php echo $this->lang->line('lbl_winterdienst_detail')?></h4>
        </div>
        <div class="modal-body">
            <?php if (isset($type) AND ($type === 'info')) : ?>
                <script type="text/javascript">
                    $(function(){
                        projekktor('video, audio', {
                            playerFlashMP4: '<?php echo assets('js/projekktor/swf/Jarisplayer/jarisplayer.swf'); ?>',
                            playerFlashMP3: '<?php echo assets('js/projekktor/swf/Jarisplayer/jarisplayer.swf'); ?>f'
                        });
                    });
                </script>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-3"><?php echo lang('lbl_worker');?></div>
                            <div class="col-sm-9"><?php echo isset($worker) ? $worker : '';?></div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-3"><?php echo lang('lbl_info');?></div>
                            <div class="col-sm-9"><?php echo isset($message) ? count($message) : '';?></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php if (0) : ?>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-3"><?php echo lang('lbl_object');?></div>
                            <div class="col-sm-9"><?php echo isset($object) ? $object : ''?></div>
                        </div>
                    </div>
                    <?php endif; ?>

                    <div class="col-sm-6">
                        <div class="row">
                        	<?php if(isset($start) && !empty($start)):?>
                            <div class="col-sm-3"><?php echo lang('lbl_date');?></div>
                            <div class="col-sm-9"><?php echo date('Y-m-d',strtotime($start));?></div>
                            <?php endif;?>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-3"><?php echo lang('lbl_video');?></div>
                            <div class="col-sm-9"><?php echo isset($video) ? count($video) : '';?></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">
                        	<?php if(isset($start) && !empty($start)):?>
                            <div class="col-sm-3"><?php echo lang('lbl_start');?></div>
                           	<div class="col-sm-9"><?php echo date('H:i:s',strtotime($start));?></div>
                           	<?php endif;?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-3"><?php echo lang('lbl_image');?></div>
                            <div class="col-sm-9"><?php echo isset($image) ? count($image) : '';?></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">
                        	<?php if(isset($start) && !empty($start)):?>
                            <div class="col-sm-3"><?php echo lang('lbl_end');?></div>
                            <div class="col-sm-9"><?php echo date('H:i:s',strtotime($end));?></div>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-3"><?php echo lang('lbl_voice');?></div>
                            <div class="col-sm-9"><?php echo isset($voice) ? count($voice) : '';?></div>
                        </div>
                    </div>
                </div>
                <hr />

                <?php if (0) : ?>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-3">Date:</div>
                            <div class="col-sm-9"><?php echo date('Y-m-d',strtotime($start));?></div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body" id="modal_gps_information">
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs">
                        <li class="<?php echo (isset($tab_select) AND ($tab_select == 'message')) ? 'active' : ''; ?>"><a href="#tab_message" data-toggle="tab"> <?php echo $this->lang->line('lbl_message')?></a></li>
                        <li class="<?php echo (isset($tab_select) AND ($tab_select == 'image')) ? 'active' : ''; ?>"><a href="#tab_image" data-toggle="tab"><?php echo $this->lang->line('lbl_image')?></a></li>
                        <li class="<?php echo (isset($tab_select) AND ($tab_select == 'voice')) ? 'active' : ''; ?>"><a href="#tab_voice" data-toggle="tab"><?php echo $this->lang->line('lbl_voice_meno')?></a></li>
                        <li class="<?php echo (isset($tab_select) AND ($tab_select == 'video')) ? 'active' : ''; ?>"><a href="#tab_video" data-toggle="tab"><?php echo $this->lang->line('lbl_video')?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane <?php echo (isset($tab_select) AND ($tab_select == 'message')) ? 'active' : ''; ?>" id="tab_message">
                            <div class="row">
                                <div class="col-md-9">
                                    <?php if (isset($message) AND !empty($message) AND is_array($message)) : ?>
                                    <?php
                                        foreach ($message as $key => $value) {
                                            echo isset($value['time']) ? ('<span class="label label-default">' . date('d.m.Y H:i', strtotime($value['time'])) . '</span>&nbsp;') : '';
                                            echo isset($value['data']) ? $value['data'] : '';
                                            echo (($key + 1) < count($message)) ? '<hr style="padding: 0px; margin: 10px; border: none;" />' : '';
                                        }
                                    ?>
                                    <?php else: ?>
                                    <div><?php echo $this->lang->line('lbl_no_data');?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane <?php echo (isset($tab_select) AND ($tab_select == 'image')) ? 'active' : ''; ?>" id="tab_image">
                            <div class="row">
                                <?php if(isset($image) && !empty($image)) : ?>
                                <div class="col-sm-4" style="padding: 0px;height: 400px; overflow: auto;">
                                    <?php $i = 0; foreach ($image as $item) : ?>
                                    <div class="col-sm-4">
                                        <img class="thumbnail" width="89" height="85" alt="" src="<?php echo $item['thumbs'];?>" onclick="$('#imgview').attr('src',$(this).attr('src'));">
                                    </div>
                                    <?php $i++; endforeach; ?>
                                </div>
                                <div class="col-sm-5" style="width: 45%; padding: 0px;">
                                    <?php if(isset($image) && !empty($image)) :?>
                                    <img width="430" height="auto" style="border: 0px solid #ddd; border-radius: 0px; -webkit-transition: all .2s ease-in-out; transition: all .2s ease-in-out;" id="imgview" alt="" src="<?php echo $image[0]['thumbs'];?>">
                                    <?php endif;?>
                                </div>
                                <?php else: ?>
                                <div class="col-md-9">
                                    <div><?php echo $this->lang->line('lbl_no_data');?></div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="tab-pane <?php echo (isset($tab_select) AND ($tab_select == 'voice')) ? 'active' : ''; ?>" id="tab_voice">
                            <div class="row">
                                <div class="col-md-9">
                                    <?php if(isset($voice) && !empty($voice)):?>
                                    <a class="pull-left btn btn-primary btn-xs" target="_blank" href="<?php echo isset($voice[0]['data']) ? $voice[0]['data'] : '#'; ?>" style="margin-bottom: 7px;"><span class="glyphicon glyphicon-cloud-download"></span>&nbsp;Click here to download</a>

                                    <?php if (isset($voice[0]['time'])) : ?>
                                    <span class="pull-right label label-success" style="padding: 5px 10px; font-size: 12px; "><?php echo date('d.m.Y H:i', strtotime($voice[0]['time'])); ?></span>
                                    <?php endif; ?>

                                    <audio controls class="projekktor" width="700" height="150" poster="poster.jpg">
                                        <?php foreach ($voice as $vi):?>
                                        <source src="<?php echo $vi['data'];?>" type="audio/mp3" ></source>
                                        <?php endforeach;?>
                                    </audio>
                                    <?php else:?>
                                    <div><?php echo $this->lang->line('lbl_no_data');?></div>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane <?php echo (isset($tab_select) AND ($tab_select == 'video')) ? 'active' : ''; ?>" id="tab_video">
                            <div class="row">
                                <div class="col-md-9">
                                    <?php if(isset($video) && !empty($video)):?>
                                    <a class="pull-left btn btn-primary btn-xs" target="_blank" href="<?php echo isset($video[0]['data']) ? $video[0]['data'] : '#'; ?>" style="margin-bottom: 7px;"><span class="glyphicon glyphicon-cloud-download"></span>&nbsp;Click here to download</a>

                                    <?php if (isset($video[0]['time'])) : ?>
                                    <span class="pull-right label label-success" style="padding: 5px 10px; font-size: 12px; "><?php echo date('d.m.Y H:i', strtotime($video[0]['time'])); ?></span>
                                    <?php endif; ?>

                                    <video class="projekktor" width="700" height="400" poster="poster.jpg">
                                        <?php foreach ($video as $vd):?>
                                        <source src="<?php echo $vd['data'];?>" ></source>
                                        <?php endforeach;?>
                                    </video>
                                    <?php else:?>
                                    <div><?php echo $this->lang->line('lbl_no_data');?></div>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <script type="text/javascript">
                    $(function(){
                        tmp = $(window).height();
                        $('#modal_information').css('overflow', 'hidden');
                        $('#wraper_list').css('height', (tmp - 200) + 'px');
                    });
                </script>
                <div class="row">
                    <div class="col-sm-12 col-md-12" id="wraper_list" style="height: 100%; overflow: auto; padding: 0px; ">
                        <table class="table table-hover table-condensed">
                            <thead>
                                <tr>
                                    <th><?php echo lang('time'); ?></th>
                                    <th width="150"><?php echo lang('latitude'); ?></th>
                                    <th width="150"><?php echo lang('longitude'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($location) AND !empty($location) AND is_array($location)) : ?>
                                <?php foreach ($location as $key => $value) : ?>
                                <tr>
                                    <td><?php echo isset($value->time) ? date('d.m.Y H:i:s', strtotime($value->time)) : ''; ?></td>
                                    <td><?php echo isset($value->lat) ? $value->lat : ''; ?></td>
                                    <td><?php echo isset($value->lon) ? $value->lon : ''; ?></td>
                                </tr>
                                <?php endforeach; ?>
                                <?php else: ?>
                                <tr> <td colspan="3" style="text-align: center;"><?php echo $this->lang->line('lbl_no_data');?></td> </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
