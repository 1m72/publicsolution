<script type="text/javascript">
    var lat = '<?php echo (isset($defaul_coordinate['lat']) ? $defaul_coordinate['lat'] : '');?>';
    var lon = '<?php echo (isset($defaul_coordinate['lon']) ? $defaul_coordinate['lon'] : '');?>';

    var map,gps_data,activity_count;
    var gps_information_url = '<?php echo create_url('sys/visualization/information'); ?>';
    var current_date = '<?php echo isset($current_time) ? $current_time : time(); ?>';
    gps_data = $.parseJSON(<?php echo (isset($gps_data) AND !empty($gps_data)) ? ("'".json_encode($gps_data)."'") : "'{}'"; ?>);

    function map_information_detail_back(id_gps,id_wk_activiti,lat,lon,tab) {
        loading();
        $.ajax({
            url: '<?php echo create_url('sys/visualization/information'); ?>',
            type: 'POST',
            data: {gps: id_gps, activity: id_wk_activiti, latitude: lat, longtitude: lon, select: tab},
            success: function(data){
                loading(false);
                $('#modal_information').html(data);
                $('#modal_information').modal({
                    backdrop: 'static'
                });
            },
            error: function(e){
                loading(false);
            }
        });
    }

    function map_information_detail(id_gps,start,end,lat,lon,tab) {
        loading();
        $.ajax({
            url: '<?php echo create_url('sys/visualization/information'); ?>',
            type: 'POST',
            data: {gps: id_gps, start_time: start,end_time: end, latitude: String(lat) , longtitude: String(lon) , select: tab,type: 'info'},
            success: function(data){
                loading(false);
                $('#modal_information').html(data);
                $('#modal_information').modal({
                    backdrop: 'static'
                });
            },
            error: function(e){
                loading(false);
            }
        });
    }

    $(function(){
        setTimeout( function(){
            map = new OpenLayers.Map("map");
            load_map(map,lat,lon);
        }, 800);

        $('.select2').select2({ width: 'resolve' });
        $(".select2").on("change", function(e) {
            auto_refresh();
        });

        $(".k_dropdownlist").kendoDropDownList();
        $(".k_datepicker").kendoDatePicker({
            format: "dd.MM.yyyy",
            change: function() {
                auto_refresh();
            }
        });

        $('.gps_detail').on('click', function(e){
            $('#wrap_activities').fadeIn();
            i = $(this);
            if (i.attr('_active') == 1) {
                return false;
            } else {
                $('.gps_detail').removeAttr("_active");
                i.attr("_active", 1);
            }
            $('.act-loading').show();
            $('.gps_detail').removeClass('success');
            i.addClass('success');
            $('#tab_raw table tbody').html('<tr><td colspan="8" class="text-center"><?php echo lang('loading'); ?></td></tr>');
            index = i.attr('_index');
            position = gps_data[index]['position'];
            gps_data_status = gps_data[index]['status'];
            if (position != 'undefined') {
                _line(map,position,index);
                $.ajax({
                    url:"<?php echo create_url('sys/visualization/detail_by_gps'); ?>",
                    type: 'GET',
                    data: {id_wkgps : index},
                    success:function(result){
                        var gps_detail = $.parseJSON(result);
                        var gps_detail_activity = gps_detail['activity'];
                        var information = gps_detail['information'];
                        if (gps_detail_activity != 'undefined' && gps_detail_activity != null) {
                            display_activity(gps_detail_activity, gps_data_status);
                        } else {
                            $('#tab_raw table tbody').html('<tr><td colspan="8" class="text-center"><?php echo lang('lbl_empty'); ?></td></tr>');
                        }
                        if (information != 'undefined') {
                            _information(map, information, index)
                        }
                        $('.act-loading').fadeOut();
                    },
                    error: function(err){
                        $('.act-loading').fadeOut();
                    }
                });
            }
            btn_view_detail = $('#btn_view_detail');
            btn_view_detail.attr('href', btn_view_detail.attr('href') + '/' + index);
        });

        $('#modal_information').on('hidden.bs.modal', function () {
            $(this).removeData('bs.modal');
        });

        $('#modal_information').on('shown.bs.modal', function () {
            data = $('#gps_information').html();
            $(data).find('ul:first').removeAttr('style');
            $('#modal_gps_information').html(data).fadeIn();
        });

        $('#tab_raw').on('click', '.information_viewer', function(e){
            e.preventDefault();
            i = $(this);
            loading();
            $.ajax({
                url: '<?php echo create_url('sys/visualization/information'); ?>',
                type: 'POST',
                data: {gps: i.parent().attr('_gps'), start_time: i.parent().attr('_start'), end_time: i.parent().attr('_end'), select: i.attr('type'), type: i.attr('_type')},
                success: function(data){
                    loading(false);
                    $('#modal_information').html(data);
                    $('#modal_information').modal({
                        backdrop: 'static'
                    });
                },
                error: function(e){loading(false);}
            });
        });
    });

    var id_old = '';
    function active_activiti(id){
        try{
            var arrleyer = map.getLayersByName("Polyline_activiti");
            for(var ile = 0; ile < arrleyer.length; ile++){
                map.removeLayer(arrleyer[ile]);
            }
        }
        catch(err){}

        var color       = $('#'+id).attr('_color');
        var position    = $('#'+id).attr('_position_ac');
        var point_array = position.split(",");
        var count       = point_array.length;
        if(count > 0)
        {
            var center = (count - count%2)/2;
            var lat = '0';
            var lon = '0';
            var _w = 3;
            if(id_old != id){
                _w = 8;
            }
            var style = {
                    strokeColor: color,
                    strokeOpacity: 1,
                    strokeWidth: _w
            };
            var lineLayer = new OpenLayers.Layer.Vector('Polyline_activiti');
            var polyline = new Array();

            for (var i = 0; i < count; i++) {
                var point_item = point_array[i];
                var item_array = point_item.split(" ");
                polyline.push(new OpenLayers.Geometry.Point(item_array[1],item_array[0]));
                if(i == center){
                    lat = item_array[1];
                    lon = item_array[0];
                }
            };

            var line      = new OpenLayers.Geometry.LineString(polyline);
            line          = line.transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
            lineFeature   = new OpenLayers.Feature.Vector(line, null, style);
            lineLayer.addFeatures([lineFeature]);
            map.addLayer(lineLayer);
            var zoom = map.getZoom();// 13;
            var lonLat = new OpenLayers.LonLat(lat,lon);
            lonLat.transform(
                new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                map.getProjectionObject() // to Spherical Mercator Projection
            );
            map.setCenter (lonLat,zoom);
        }

        if(id_old != id){
            $('#tab_raw tr').each(function(index){
                i = $(this);
                $(this).attr('class','gps_detail');
            });
            $('#'+id).attr('class','gps_detail success');
            id_old = id;
        } else {
            $('#'+id).attr('class','gps_detail');
            id_old = '';
        }
    }

    function get_distance_latlon(lat1,lon1,lat2,lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2-lat1);  // deg2rad below
        var dLon = deg2rad(lon2-lon1);
        var a =  Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *  Math.sin(dLon/2) * Math.sin(dLon/2) ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c; // Distance in km
        return d*1000;
    }

    function deg2rad(deg) {
      return deg * (Math.PI/180)
    }

    function replace_all(find, replace, str) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    function map_click_active_activiti(click_lat,click_lon){
        var _click_lat = replace_all("'","",click_lat);
        var _click_lon = replace_all("'","",click_lon);
        var _check = false;
        $("#tb_activiti tbody tr").each( function (index) {
            var _id = $(this).attr('id');
            if(_id != 'undefined'){
                var _name = $(this).attr('_name');
                var _position = $(this).attr('_position_ac');
                if(_position != 'undefined'){
                    var _item_array = _position.split(",");
                    var _item_count = _item_array.length;
                    if(_item_count > 0){
                        var _item_fist          = _item_array[0];
                        var _item_fist_array    = _item_fist.split(" ");

                        var _item_end           = _item_array[_item_count - 1];
                        var _item_end_array     = _item_end.split(" ");

                        var _distance_fist_end  = get_distance_latlon(_item_fist_array[0],_item_fist_array[1],_item_end_array[0],_item_end_array[1]);

                        var _distance_fist      = get_distance_latlon(_click_lat,_click_lon,_item_fist_array[0],_item_fist_array[1]);
                        var _distance_end       = get_distance_latlon(_click_lat,_click_lon,_item_end_array[0],_item_end_array[1]);

                        if ( (_check != true) && ((_distance_fist + _distance_end) - _distance_fist_end < 15) ){
                            active_activiti(_id);
                            _check = true;
                        } else {
                            for (var i = 0; i < _item_array.length; i++) {
                                var _latlon = _item_array[i];
                                var _latlon_array = _latlon.split(" ");
                                var _lat_item = _latlon_array[0];
                                var _lon_item = _latlon_array[1];
                                var _distance = get_distance_latlon(_click_lat,_click_lon,_lat_item,_lon_item);
                                if ( (_check != true) && (_distance < 20) ){
                                    active_activiti(_id);
                                    _check = true;
                                }
                            };
                        }
                    }
                }
            }
        });
    }

    function display_activity(data, status) {
        var tab_raw = '';
        if(status == 'success'){
            var count = data.length;
            if (count > 0) {
                for(var i = 0;i < count;i++)
                {
                    tmp = data[i]['id'];
                    pos_lat = data[i]['latitude'];
                    pos_long = data[i]['longtitude'];
                    tab_raw += '<tr id="position_'+i+'" class="gps_detail" _color="'+data[i]['color']+'" _position_ac="'+data[i]['position']+'" position="'+pos_lat+'|'+pos_long+'" onclick="active_activiti(this.id);" style="cursor:pointer;">';
                    tab_raw += '<td>' + (i + 1) + '</td>';
                    tab_raw += '<td>' + data[i]['start'] + '</td>';
                    tab_raw += '<td>' + data[i]['end'] + '</td>';
                    tab_raw += '<td>' + data[i]['duration'] + '</td>';
                    tab_raw += '<td>' + data[i]['duration_total'] + '</td>';
                    tab_raw += '<td><div style="background: '+ data[i]['color'] +';height: 16px; width: 15px;float: left;margin-top: 1px;margin-right: 2px;"></div>' + data[i]['acname'] + '</td>';
                    tab_raw += '<td>' + data[i]['street'] + '</td>';
                    tab_raw += '<td _gps="'+data[i]['id_wkgps']+'" _start="'+data[i]['starttime']+'" _end="'+data[i]['endtime']+'" style="text-align: right; padding-right: 10px;">';
                    if(( parseInt(data[i]['message']) + parseInt(data[i]['image']) + parseInt(data[i]['voice']) + parseInt(data[i]['video']) ) > 0) {
                        tab_raw += '<a href="javascript:void();"  _type="info" class="information_viewer btn btn-xs btn-primary"><span class="glyphicon glyphicon-send"></span>&nbsp;<?php echo lang('lbl_information')?></a>&nbsp;';
                    }
                    tab_raw += '<a href="javascript:void();" _type="detail" class="information_viewer btn btn-xs btn-info"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<?php echo lang('btn_detail')?></a>';
                    tab_raw += '</td>';
                    tab_raw += '</tr>';
                }
            } else {
                tab_raw += '<tr><td colspan="8" align="center"><?php echo lang('lbl_empty')?></td></tr>';
            }

            /*slim_scroll = $('#information_tab_content');
            if (count > 13) {
                slim_scroll.slimScroll({height: '400px'});
                slim_scroll.css({overflow: 'hidden', height: '400px'});
                slim_scroll.parent().css({overflow: 'hidden', height: '400px'});
            } else {
                slim_scroll.slimScroll({destroy: true});
                slim_scroll.css({overflow: 'overflow', height: ''});
                slim_scroll.parent().css({overflow: 'overflow', height: ''});
            }*/
        } else {
            tab_raw += '<tr>';
            tab_raw += '<td colspan="8"><?php echo lang('lbl_data_processing')?></td>';
            tab_raw += '</tr>';
        }
        $('#tab_raw table tbody').html(tab_raw);
        //$('#tab_raw').prepend('<tbody>' + tab_raw + '</tbody>');
    }

    $(function(){
         $('.ps_scroll').each(function(index){
            i = $(this);
            tmp = $(this).attr('_height');
            if (typeof (tmp) != 'undefined') {
                i.slimScroll({height: tmp});
            } else {
                i.slimScroll();
            }
        });

        $('#by_date').on('click', '.step_day', function(e){
            e.preventDefault();
            i = $(this);
            step = parseInt(i.attr('step'));

            var new_date = strtotime(step + ' day', current_date);
            current_date = new_date;
            $('[name="by_date"]').val(date('d.m.Y', new_date));

            auto_refresh();
        });

        $('#frm_filter').submit(function(){
            loading();
        });
    });

    function auto_refresh() {
        setTimeout(function(){$('#frm_filter').submit();}, 150);
    }
</script>

<style type="text/css">
#filter_inner {background: #FFF; border-bottom: none; border-top: none; padding: 5px;}
#body_wrap [class^="col-"]{padding: 0px; }
#wrap_activity [class^="col-"]{padding: 0px; }
#panel_gps_record {border: none;}
.act-loading {position: absolute; display: none;}
.act-loading-mask {top: 0px; left: 0px; width: 100%; height: 100%; background: #FFF; opacity: 0.85; display: none;}
.act-loading-text {top: 20px; left: 50%; z-index: 2; color: #000; font-size: 18px; overflow: hidden; display: none;}
</style>

<div class="col-xs-12 col-md-12">
    <form class="form-horizontal" method="get" id="frm_filter">
        <div class="row" id="filter_wrap">
            <div class="col-xs-12 col-md-12" id="filter_inner">
                <div class="col-xs-12 col-md-3 filter_item" id="by_date">
                    <label><?php echo $this->lang->line('lbl_by_date');?></label>
                    <a href="javascript:;" class="step_day" step="-1"><i class="glyphicon glyphicon-chevron-left"></i></a>&nbsp;
                    <input name="by_date" class="auto_refresh k_datepicker" value="<?php echo isset($by_date) ? $by_date : ''; ?>" style="width: 120px;" />&nbsp;
                    <a href="javascript:;" class="step_day" step="1"><i class="glyphicon glyphicon-chevron-right"></i></a>
                </div>

                <div class="col-xs-12 col-md-3 filter_item">
                    <label><?php echo $this->lang->line('lbl_by_worker');?></label>
                    <select name="by_worker" class="auto_refresh k_dropdownlist2 select2" style="width: 150px;">
                        <option value=""><?php echo $this->lang->line('lbl_select_worker');?></option>
                        <?php if (isset($workers) AND !empty($workers)) : ?>
                            <?php foreach ($workers as $key => $value) : ?>
                                <option value="<?php echo $value->id; ?>" <?php echo (isset($by_worker) AND ($by_worker == $value->id)) ? 'selected="selected"' : ''; ?>><?php echo $value->first_name . ' ' . $value->last_name; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>

                <div class="col-xs-12 col-md-3 filter_item">
                    <label><?php echo $this->lang->line('lbl_by_machine');?></label>
                    <select name="by_machine" class="auto_refresh k_dropdownlist2 select2" style="width: 150px;">
                        <option value=""><?php echo $this->lang->line('lbl_select_machine');?></option>
                        <?php if (isset($machines) AND !empty($machines)) : ?>
                            <?php foreach ($machines as $key => $value) : ?>
                                <option value="<?php echo $value->id; ?>" <?php echo (isset($by_machine) AND ($by_machine == $value->id) ) ? 'selected="selected"' : ''; ?>><?php echo $value->name; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>

                <div class="col-xs-12 col-md-3 filter_item">
                    <label style="float: left;"><?php echo lang('lbl_street');?></label>
                    <input type="text" name="by_street" class="form-control input-md" placeholder="<?php echo lang('lbl_select_street');?>" value="<?php echo isset($by_street) ? $by_street : ''; ?>" style="width: 150px; height: 28px; float: left;"/>
                </div>

                <div class="col-xs-12 col-md-2 filter_item" style="padding-left: 0px;">
                    <button value="OK GO" class="btn btn-primary btn-sm" style="height: 28px; "><?php echo lang('btn_ok');?></button>
                </div>
            </div>
            <div class="col-xs-12 col-md-12 hide">
                <div class="panel panel-default" style2="margin-bottom: 0px; border-bottom: 0px;">
                    <div class="panel-body">
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="body_wrap">
            <div class="col-xs-12 col-md-5" style2="padding-right: 0px; border-right: 0px;">
                <div class="panel panel-default" id="panel_gps_record" style="margin-bottom: 0px;">
                    <div class="panel-heading">
                        <?php echo $this->lang->line('lbl_gps_record')?>
                        <span class="pull-right dropdown hide" style="font-weight: normal;">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                <?php echo (isset($module) AND isset($module_list_array[$module])) ? $module_list_array[$module] : lang('lbl_select_module'); ?>&nbsp;
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <?php foreach ($module_list_array as $key => $value) : ?>
                                <li><a href="<?php echo current_url() . '?module=' . $key; ?>"> <?php echo $value; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </span>
                        <span class="pull-right hide">
                            <?php echo form_dropdown('module', $module_list_array, (isset($module) ? $module : false), 'class="select2" style="width: 180px;"'); ?>
                        </span>
                    </div>
                    <div class="panel-body ps_scroll" _height="380px" id="panel_gps_record_body" style="padding: 0px; max-height: 380px; overflow: auto;">
                        <table class="table table-striped2 table-hover" style="margin-bottom: 0px;">
                            <?php if (isset($gps) AND (!empty($gps)) ) : ?>
                            <?php foreach ($gps as $key => $value) : ?>
                            <tr class="gps_detail" _index="<?php echo $value->id; ?>">
                                <td width="130"><?php echo strtotime($value->ontime) ? date('d.m.Y H:i', strtotime($value->ontime)) : ''; ?> </td>
                                <td><?php echo $value->mcname; ?></td>
                                <td><?php echo $value->first_name.' '.$value->last_name; ?></td>
                                <td width="30">
                                    <a target="_blank" href="<?php echo create_url('sys/visualization/gps_print/'.$value->id); ?>" title="<?php echo lang('lbl_print_tip'); ?>">
                                        <span class="glyphicon glyphicon-print"></span>
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php else: ?>
                            <tr> <td> <p align="center"><?php echo $this->lang->line('lbl_empty');?></p> </td> </tr>
                            <?php endif; ?>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-7" style2="padding-left: 0px; border-left: 0px;">
                <div class="panel panel-default" style="border-left: 0px; margin-bottom: 0px;">
                    <?php if (0) : ?>
                    <div class="panel-heading"><?php echo $this->lang->line('lbl_map');?></div>
                    <?php endif; ?>
                    <div class="panel-body" style="padding: 0px;">
                        <div style="height2: 380px; height: 410px;" id="map"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="wrap_activity">
            <div class="col-xs-12 col-md-12">
                <div id="wrap_activities" style="display: none;">
                    <div class="panel panel-default" style="border: none; margin-bottom: 0px;">
                        <div class="panel-heading hide"><?php echo lang('lbl_tab_activities'); ?></div>
                        <div class="panel-body" id="information_tab_content" style="position: relative;">
                            <div id="tab_raw" style="max-height: 450px; min-height: 60px; overflow: auto;">
                                <table id="tb_activiti" class="table table-hover" style="background: #FFF; margin-bottom: 0px; border-left: none; border-right: none;">
                                    <thead>
                                        <tr class="active">
                                            <th width="30">#</th>
                                            <th width="80"><?php echo lang('lbl_start');?></th>
                                            <th width="80"><?php echo lang('lbl_end');?></th>
                                            <th width="80"><?php echo lang('lbl_duration');?></th>
                                            <th width="80"><?php echo lang('lbl_duration_total');?></th>
                                            <th width="200"><?php echo lang('lbl_activity');?></th>
                                            <th><?php echo lang('lbl_street');?></th>
                                            <th width="200" style="text-align: right; padding-right: 10px;"><?php echo lang('lbl_information');?></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="act-loading act-loading-mask"></div>
                            <div class="act-loading act-loading-text"> <img src="<?php echo base_url('assets/images/loading2.gif'); ?>"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal fade" id="modal_information"></div>