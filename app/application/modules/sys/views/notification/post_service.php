<script type="text/javascript">
    $(function(){
        $(".k_datepicker").kendoDatePicker({
            format: "dd.MM.yyyy"
        });

        $('#post_service').on('change', '#filter_time', function(e){
            e.preventDefault();
            $('#frm_post_service').submit();
        });
    });
</script>
<div class="col-md-12">
    <div class="row-fluid">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container" style="border-top: none; border-bottom: none;">
            <div class="hide col-lg-2 col-md-2 col-sm-12 col-xs-12 bhoechie-tab-menu">
                <div class="list-group">
                    <a class="go list-group-item text-right active" href="<?php echo create_url('sys/notification/post_service'); ?>">Post service</a>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab2" style2="min-height: 300px; ">
                <div class="bhoechie-tab-content active">
                    <form action="<?php echo current_url(); ?>" method="get" id="frm_post_service">
                        <div id="post_service">
                            <table class="table table-hover table table-condensed">
                                <thead>
                                    <tr>
                                        <th colspan="5">
                                            <span class="pull-left">
                                                <h3><strong><?php echo lang('notification_lbl_header'); ?></strong></h3>
                                            </span>
                                            <span class="pull-right" style="font-weight: normal; margin: 20px 0px 10px 0px; ">
                                                <span><?php echo lang('notification_lbl_filter_date'); ?></span>
                                                <span> <input name="time" id="filter_time" class="k_datepicker" value="<?php echo isset($filter_time) ? $filter_time : ''; ?>" placeholder="" /> </span>
                                            </span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th><?php echo lang('notification_lbl_uri'); ?></th>
                                        <th class="text-center" width="300"><?php echo lang('notification_lbl_result'); ?></th>
                                        <th class="text-center" width="150"><?php echo lang('notification_lbl_last_request'); ?></th>
                                        <th class="text-center" width="50"><?php echo lang('notification_lbl_retry'); ?></th>
                                        <th class="text-center" width="100"><?php echo lang('notification_lbl_attachments'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (isset($result) AND is_array($result) AND !empty($result)) : ?>
                                    <?php foreach ($result as $key => $value) : ?>
                                    <tr>
                                        <td><a href="<?php echo $value->uri; ?>" target="_blank"><?php echo $value->uri; ?></a></td>
                                        <td class="text-center"><?php echo word_limiter($value->response_data, 8); ?></td>
                                        <td class="text-center"><?php echo date('d.m.Y H:i:s', strtotime($value->request_at)); ?></td>
                                        <td class="text-center"><?php echo $value->retry; ?></td>
                                        <td class="text-center">
                                            <a href="<?php echo config_item('media_web') . str_replace('uploads/', '', $value->request_data); ?>" class="btn btn-sm btn-warning" target="_blank">
                                                <span class="glyphicon glyphicon-save"></span> <?php echo lang('notification_lbl_download'); ?>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                    <?php else: ?>
                                    <tr><td colspan="5" class="text-center"><?php echo lang('msg_data_not_available'); ?></td></tr>
                                    <?php endif; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="5">
                                            <span class="pull-right">
                                                <?php echo $this->pagination->create_links(); ?>
                                            </span>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>