<style type="text/css">
.bootstrap-tagsinput input {width: 350px !important;}
.bootstrap-tagsinput {margin-bottom: 0px;}
</style>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container" style="border-top: none; border-bottom: none;">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
            <div class="list-group">
                <a href="#" class="list-group-item active text-right">Email notification <span class="glyphicon glyphicon-plane"></span></a>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab" style="min-height: 205px; padding-top: 15px;">
            <?php if (isset($msg)) : ?>
            <div class="alert alert-success"><?php echo $msg; ?></div>
            <?php endif; ?>

            <?php if (isset($err)) : ?>
            <div class="alert alert-danger"><?php echo $err; ?></div>
            <?php endif; ?>

            <div class="bhoechie-tab-content active">
                <form method="POST" class="form-horizontal" id="frm_user_profiler" action="<?php  echo current_url_with_params();?>">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="email"><?php echo $this->lang->line('module_lbl_email');?></label>
                        <div class="col-md-6">
                            <input id="email" name="email" value="<?php echo set_value('email', isset($data->email) ? implode(',', explode(';', $data->email))  : ''); ?>" type="text" placeholder="<?php echo $this->lang->line('module_lbl_email');?>" class="form-control input-md" data-role="tagsinput" style="width: 200px;">
                            <span class="help-block"><?php echo lang('module_lbl_tip_email'); ?></span>
                            <div class="help-block ps_err">
                                <?php echo form_error('email'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">&nbsp;</label>
                        <div class="col-md-6">
                            <button id="btn_save" _keep="1" name="btn_save" value="save" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="bhoechie-tab-content">
                <center>
                    <h1 class="glyphicon glyphicon-road"></h1>
                    <h2>Cooming ...</h2>
                    <h3>Comming ...</h3>
                </center>
            </div>
            <div class="bhoechie-tab-content">
                <center>
                    <h1 class="glyphicon glyphicon-home"></h1>
                    <h2>Cooming ...</h2>
                    <h3>Comming ...</h3>
                </center>
            </div>
            <div class="bhoechie-tab-content">
                <center>
                    <h1 class="glyphicon glyphicon-cutlery"></h1>
                    <h2>Cooming ...</h2>
                    <h3>Comming ...</h3>
                </center>
            </div>
            <div class="bhoechie-tab-content">
                <center>
                    <h1 class="glyphicon glyphicon-credit-card"></h1>
                    <h2>Cooming ...</h2>
                    <h3>Comming ...</h3>
                </center>
            </div>
        </div>
    </div>
</div>
