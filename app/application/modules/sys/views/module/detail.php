<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("sys/module/edit/$id");
    } else {
        $form_link = create_url("sys/module/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_module_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_module');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>

    $(function(){
        $('.select2').select2({ width: 'resolve' });
        $('#frm_module_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_module_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });
    });
</script>
<div id="frm_module_detail_wraper">
    <form class="form-horizontal" id="frm_module_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-3 control-label" for="title"><?php echo lang('module_lbl_title');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-6">
                <input id="title" name="title" value="<?php echo set_value('title', isset($data->title) ? $data->title : ''); ?>" type="text" placeholder="<?php echo lang('module_holder_title');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('title'); ?>
                </div>
            </div>
        </div>

        <?php if (0) : ?>
        <div class="form-group">
            <label class="col-md-3 control-label" for="activity"><?php echo lang('lbl_module');?></label>
            <div class="col-md-9">
                <select id="module" multiple="multiple" class="select2" style="width: 457px; border-bottom: none;">
                    <?php foreach ($modules as $key => $value) : ?>
                    <option value="<?php echo $key; ?>" <?php echo (isset($module_selected) AND (isset($module_selected[$key])) AND is_array($module_selected[$key]) ) ? 'selected="selected"' : ''; ?>><?php echo $value; ?></option>
                    <?php endforeach; ?>
                </select>
                <div style="margin-right: 10px; overflow: auto; border: 0px solid #aaa; padding-top: 7px;">
                    <div id="panel-left" style="float: left; min-height: 10px; width: 200px; "></div>
                    <div id="panel-right" style="float: left; width: 250px; border-left: 1px solid #aaa; "></div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-3 control-label" for="btn_save"></label>
            <div class="col-md-6">
                <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a class="btn btn-default" href="javascript:close_modal('#modal_module_detail');"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>
    </form>
</div>