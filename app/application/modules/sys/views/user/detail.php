<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("sys/user/edit/$id");
    } else {
        $form_link = create_url("sys/user/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>

<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
        setTimeout(
        function(){
            close_modal('#modal_user_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_user');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>

    var select_company = '#select_company';
    function company_refresh() {
        $.ajax({
           type: 'GET',
           url: '<?php echo create_url('sys/worker/list_company');?>',
           datatype: 'html',
           cache:false,
           success: function(data){
                $('#select_company').html(data);
                $('#select_company .select2').select2();
           }
        });
    }
    function usergroup_refresh() {
        $.ajax({
           type: 'GET',
           url: '<?php echo create_url('sys/user_group/list_usergroup');?>',
           datatype: 'html',
           cache:false,
           success: function(data){
                $('#select_usergroup').html(data);
                $('#select_usergroup .select2').select2();
           }
        });
    }
    $(function(){
        $('.select2').select2({ width: 'resolve' });

        $('.toggle_password').click(function(e){
            e.preventDefault();
            i = $(this);
            target = i.attr('_target');
            if ($(target).attr('type') != 'password') {
                $(target).attr('type', 'password');
                i.text('<?php echo $this->lang->line("btn_show"); ?>');
            } else {
                $(target).attr('type', 'text');
                i.text('<?php echo $this->lang->line("btn_hide"); ?>');
            }
        });

        $('#frm_user_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_user_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });
    });

</script>
<div id="frm_user_detail_wraper">
   <form class="form-horizontal" id="frm_user_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <?php if (isset($err)) : ?>
        <div class="alert alert-danger"><?php echo $err; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-4 control-label" for="username"><?php echo lang('lbl_username'); ?><span class="star">&nbsp;*</span></label>
            <div class="col-md-5">
                <input id="username" name="username" value="<?php echo set_value('username', isset($data->username) ? $data->username : ''); ?>" type="text" placeholder="<?php echo lang('lbl_username'); ?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('username'); ?>
                    <?php echo isset($err_username)? $err_username:''; ?>

                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="password"><?php echo $this->lang->line('lbl_password');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-5">
                <input id="password" name="password" autocomplete="off" value="" type="password" placeholder="<?php echo $this->lang->line('lbl_password');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('password'); ?>
                </div>
            </div>
            <div class="col-md-2">
                 <button id="btn_showpassword" name="btn_showpassword" _target="#password,#passconf" class="toggle_password btn btn-default" ><?php echo $this->lang->line('btn_hide'); ?></button>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="passconf"><?php echo $this->lang->line('lbl_password_confirm');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-5">
                <input id="passconf" name="passconf" value="" type="password" placeholder="<?php echo $this->lang->line('lbl_password_confirm');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('passconf'); ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="email"><?php echo lang('lbl_email');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-5">
                <input id="email" name="email" value="<?php echo set_value('email', isset($data->email) ? $data->email : ''); ?>" type="text" placeholder="<?php echo lang('lbl_email');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('email'); ?>
                    <?php echo isset($err_email)? $err_email:''; ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="group"><?php echo lang('lbl_group');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-5" id = "select_usergroup">
                <select id="group" name="group" class="select2" style="width: 245px;">
                    <option value=""><?php echo lang('lbl_select_group'); ?></option>
                    <?php if ( isset($groups) AND !empty($groups) ) : ?>
                    <?php foreach ($groups as $key => $value) : ?>
                    <option value="<?php echo $value->id; ?>" <?php echo set_select('group', $value->id, (isset($data->id_user_group) AND ($data->id_user_group == $value->id) )); ?> ><?php echo $value->name; ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <div class="help-block ps_err">
                    <?php echo form_error('group'); ?>
                </div>
            </div>
            <div class="col-md-2" style="padding: 0px;">
                <a href="<?php echo create_url('sys/user_group/add') . '?callback=usergroup_refresh'; ?>" class="sys_modal_detail btn btn-xs btn-info" _title="<?php echo lang('lbl_usergroup_detail'); ?>" _modal='#modal_usergroup_detail' target="_blank"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new')?></a>
            </div>
        </div>
        <?php if(0):?>
        <div class="form-group">
            <label class="col-md-4 control-label" for="city"><?php echo lang('lbl_city');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-5">
                <select id="city" name="city" class="select2" style="width: 245px;">
                    <option value=""><?php echo lang('lbl_select_city'); ?></option>
                    <?php if ( isset($citys) AND !empty($citys) ) : ?>
                    <?php foreach ($citys as $key => $value) : ?>
                    <option value="<?php echo $value->id; ?>" <?php echo set_select('city', $value->id, (isset($data->id_city) AND ($data->id_city == $value->id) )); ?>><?php echo $value->name; ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <div class="help-block ps_err">
                    <?php echo form_error('city'); ?>
                </div>
            </div>
        </div>
        <?php endif;?>
        <div class="form-group">
            <label class="col-md-4 control-label" for="company"><?php echo lang('lbl_company');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-5" id="select_company" >
                    <select id="company" name="company" class="select2" style="width: 245px;">
                        <option value=""><?php echo lang('lbl_select_company'); ?></option>
                        <?php if ( isset($companies) AND !empty($companies) ) : ?>
                        <?php foreach ($companies as $key => $value) : ?>
                        <option class="optselect" value="<?php echo $value->id; ?>" <?php echo set_select('company', $value->id, (isset($data->id_company) AND ($data->id_company == $value->id) )); ?>><?php echo $value->name; ?></option>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <div class="help-block ps_err">
                    <?php echo form_error('company'); ?>
                    </div>
            </div>
            <div class="col-md-2" style="padding: 0px;">
                <a href="<?php echo create_url('sys/company/add') . '?callback=company_refresh'; ?>" class="sys_modal_detail btn btn-xs btn-info" _title="<?php echo lang('cpn_lbl_company_detail'); ?>" _modal='#modal_company_detail' target="_blank"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new')?></a>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="btn_save"></label>
            <div class="col-md-8">
                <button id="btn_save" name="btn_save" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change'); ?></button>
                <a id="btn_cancel" name="btn_cancel" href="javascript:close_modal('#modal_user_detail');" class="btn btn-default"><?php echo lang('btn_cancel'); ?></a>
            </div>
        </div>
    </form>
</div>