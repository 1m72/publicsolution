<?php
    $check_database     = (isset($data->database) AND ($data->database==1) ) ? true : false;
    $check_storage      = (isset($data->storage) AND ($data->storage==1) ) ? true : false;
    $check_notification = (isset(session_login(false)->notification) AND session_login(false)->notification==1) ? true : false;
    $check_material     = (isset($data->material) AND $data->material==1) ? true : false;
    $check_selection    = (isset($data->selection) AND $data->selection==1) ? true : false;
?>
<style type="text/css">
label {font-weight: normal; line-height: 30px;}
.form-control {height: 35px;}
.form-control.tokenfield {height: auto; padding: 7px;}
.btn {line-height: 25px;}
</style>
<script type="text/javascript">
    var lon_move;
    var lat_move;
    var zoom;

    $(function(){
        //start default coordinate
        var lon = '<?php echo empty($data->lon) ? 0 : $data->lon;?>';
        var lat = '<?php echo empty($data->lat) ? 0 : $data->lat;?>';
        var map = new OpenLayers.Map("map");
        var epsg4326 = new OpenLayers.Projection("EPSG:4326");
        var epsg900913 = new OpenLayers.Projection("EPSG:900913");

        var osm = new OpenLayers.Layer.OSM();
        var vectors = new OpenLayers.Layer.Vector("Vector Layer");
        map.addLayers([osm, vectors]);
        var center = new OpenLayers.LonLat(lon,lat).transform(epsg4326, epsg900913);
        var customer = new OpenLayers.LonLat(lon,lat).transform(epsg4326, epsg900913);
        var point  = new OpenLayers.Geometry.Point(customer.lon,customer.lat);
        var marker = new OpenLayers.Feature.Vector(point,null,{
            externalGraphic: "<?php echo base_url('assets/images/marker.png');?>",
            graphicWidth: 20,
            graphicHeight: 20,
            fillOpacity: 1
        });
        vectors.addFeatures([marker]);
        var drag = new OpenLayers.Control.DragFeature(vectors, {autoActivate: true, onComplete: onCompleteMove});

        zoom = get_cookie('default_zoom');
        if ((zoom == undefined) || (zoom == '') ) {
            zoom = 5;
        }
        map.addControl(drag);
        map.setCenter(center, zoom);
        map.events.register("zoomend", map, zoomChanged);

        function onCompleteMove(feature) {
            var point_transformed;
            if (feature) {
                point_transformed = feature.clone().geometry.transform(epsg900913, epsg4326);
                lon_move = point_transformed.x;
                lat_move  = point_transformed.y;
            }
        }

        function zoomChanged() {
            zoom = map.getZoom();
            set_cookie('default_zoom', zoom, 1);
        }

        //save point
        $(".savelonlat").click(function(){
            $('#default-coordinate-msg').hide().removeClass('alert-success alert-danger');
            $.ajax({
                type: 'GET',
                url: '<?php echo create_url('sys/user/save_long_lat');?>',
                data: {lon:lon_move,lat:lat_move},
                cache:false,
                success:function(data){
                    var bt_close = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    var msg_save_succes = '<?php echo lang("profile_msg_coordinate_save");?>'
                    $('#savelonlat').button('reset');
                    if(data == msg_save_succes){
                        $('#default-coordinate-msg').addClass('alert-success').text('<?php echo lang('profile_msg_coordinate_save');?>').fadeIn().delay(3000).fadeOut();
                    }else{
                        $('#default-coordinate-msg').addClass('alert-danger').text('<?php echo lang('err_has_an_error_when_update_database');?>').fadeIn().delay(3000).fadeOut();
                    }
                    center = new OpenLayers.LonLat(lon_move,lat_move).transform(epsg4326, epsg900913);
                    zoom = get_cookie('default_zoom');
                    if ((zoom == undefined) || (zoom == '') ) {
                        zoom = 5;
                    }
                    map.setCenter(center, zoom);
               },
            });
        });
        //end default coordinate

        $('#notification_email').tokenfield();
        $('#notification_email')
            .on('beforeCreateToken', function (e) {
                var token = e.token.value.split('|')
                e.token.value = token[1] || token[0]
                e.token.label = token[1] ? token[0] + ' (' + token[1] + ')' : token[0]
            })
            .on('afterCreateToken', function (e) {
                // Über-simplistic e-mail validation
                var re = /\S+@\S+\.\S+/
                var valid = re.test(e.token.value)
                if (!valid) {
                  $(e.relatedTarget).addClass('invalid')
                }
            })
            .on('beforeEditToken', function (e) {
                if (e.token.label !== e.token.value) {
                  var label = e.token.label.split(' (')
                  e.token.value = label[0] + '|' + e.token.value
                }
            })
            .tokenfield();

        $("#btn_test").click(function(){
            var _hostname           = $('#hostname').val();
            var _username           = $('#username').val();
            var _password           = $('#password').val();
            var _passconf           = $('#passconf').val();
            var _database           = $('#database').val();
            var _port               = $('#port').val();
            var _check_database     = $('#check_database').val();
            if(_password == _passconf){
                var _url = '<?php echo create_url('sys/user/checkconnect'); ?>';
                $.ajax({
                    url: _url,
                    type: 'POST',
                    data: {hostname:_hostname, username:_username, password:_password, database:_database, port:_port},
                    success: function(data) {
                        if(data.length > 0){
                            var bt_close = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            if(data == 'success'){
                                $('#alert_connect').remove();
                                $('#frm_profile_wraper').prepend('<div id="alert_connect" class="alert alert-success"><?php echo lang('connect_success');?>' + bt_close + '</div>');
                            }else{
                                $('#alert_connect').remove();
                                $('#frm_profile_wraper').prepend('<div id="alert_connect" class="alert alert-warning"><?php echo lang('connect_fail');?>' + bt_close + '</div>');
                            }
                        }else{
                            $('#alert_connect').remove();
                            $('#frm_profile_wraper').prepend('<div id="alert_connect" class="alert alert-warning"><?php echo lang('connect_fail');?>' + bt_close + '</div>');
                        }
                    },
                    error: function(err) {}
                });
            }
        });

        $('.toggle_password').click(function(e){
            e.preventDefault();
            i = $(this);
            target = i.attr('_target');
            if ($(target).attr('type') != 'password') {
                $(target).attr('type', 'password');
                i.text('<?php echo lang("btn_show");?>');
            } else {
                $(target).attr('type', 'text');
                i.text('<?php echo lang("btn_hide");?>');
            }
        });

        $('#modal_profile').on('hidden.bs.modal', function () {
            $(this).removeData('bs.modal');
        })
    });

    $('#frm_user_profiler').submit(function(e){
        e.preventDefault();
        i = $(this);
        $.ajax({
            url: i.attr('action'),
            type: 'POST',
            data: i.serialize(),
            success: function(res) {
                $('#frm_profile_wraper').html(res);
                var _alert=$('.alert');
                if(_alert.text()!=''){
                    setTimeout(
                        function(){
                            dialog_edit.close();
                             grid_refresh();
                        },
                    timeout_dialog
                   );
                }
            },
            error: function(err) {}
        });
    });
</script>

<div class="col-md-12">
    <div class="row-fluid">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container" style="border-top: none; border-bottom: none;">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 bhoechie-tab-menu">
                <div class="list-group">
                    <?php
                        $type = isset($type) ? $type : false;
                        $type_label = array(
                            false                => lang('profile_general'),
                            'change-password'    => lang('profile_change_password'),
                            'database'           => lang('profile_database'),
                            'storage'            => lang('profile_storage'),
                            'notification'       => lang('profile_notification'),
                            'timezone'           => lang('profile_timezone'),
                            'post-service'       => lang('profile_post_service'),
                            'option'             => lang('profile_option'),
                            'default-coordinate' => lang('profile_default_coordinate'),
                        );
                    ?>
                    <a class="go list-group-item text-right <?php echo (!$type) ? 'active' : '';  ?>" href="<?php echo create_url('sys/user/profile'); ?>"><?php echo lang('profile_general'); ?></a>
                    <a class="go list-group-item text-right <?php echo ($type == 'change-password') ? 'active' : ''; ?>" href="<?php echo create_url('sys/user/profile/change-password'); ?>"><?php echo lang('profile_change_password'); ?></a>
                    <a class="go list-group-item text-right <?php echo ($type == 'database') ? 'active' : '';  ?>" href="<?php echo create_url('sys/user/profile/database'); ?>"><?php echo lang('profile_database'); ?></a>
                    <a class="go list-group-item text-right <?php echo ($type == 'storage') ? 'active' : '';?>" href="<?php echo create_url('sys/user/profile/storage'); ?>"><?php echo lang('profile_storage'); ?></a>
                    <a class="go list-group-item text-right <?php echo ($type == 'notification') ? 'active' : '';?>" href="<?php echo create_url('sys/user/profile/notification'); ?>"><?php echo lang('profile_notification'); ?></a>
                    <a class="go list-group-item text-right <?php echo ($type == 'timezone') ? 'active' : '';?>" href="<?php echo create_url('sys/user/profile/timezone'); ?>"><?php echo lang('profile_timezone'); ?></a>
                    <a class="go list-group-item text-right <?php echo ($type == 'post-service') ? 'active' : '';?>" href="<?php echo create_url('sys/user/profile/post-service'); ?>"><?php echo lang('profile_post_service'); ?></a>
                    <a class="go list-group-item text-right <?php echo ($type == 'option') ? 'active' : '';?>" href="<?php echo create_url('sys/user/profile/option'); ?>"><?php echo lang('profile_option'); ?></a>
                    <a class="go list-group-item text-right <?php echo ($type == 'default-coordinate') ? 'active' : '';?>" href="<?php echo create_url('sys/user/profile/default-coordinate'); ?>"><?php echo lang('profile_default_coordinate'); ?></a>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 bhoechie-tab" style="min-height: 370px; ">
                <div class="bhoechie-tab-content active">
                    <h3><?php echo isset($type_label[$type]) ? $type_label[$type] : '';?></h3><hr />
                    <?php if($type == 'default-coordinate'): ?>
                        <div>
                            <strong><?php echo lang('lbl_map');?></strong>
                            <div class="pull-right">
                                <span class="alert alert-success" id="default-coordinate-msg" style="padding: 8px 10px; display: none;">123123</span>
                                <a id="savelonlat" class="savelonlat btn btn-xs btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change'); ?></a>
                            </div>
                        </div>
                        <div style="height: 380px;" id="map"></div>
                        <div style="height: 15px;"></div>
                    <?php else: ?>
                        <form method="POST" class="form-horizontal" id="frm_user_profiler" action="<?php  echo current_url();?>">
                            <?php if (isset($msg)) : ?>
                            <div class="alert alert-success"><?php echo $msg; ?></div>
                            <?php endif; ?>

                            <?php if (isset($err)) : ?>
                            <div class="alert alert-danger"><?php echo $err; ?></div>
                            <?php endif; ?>

                            <?php if ($type == 'post-service') : ?>
                            <?php
                                $post_service_url      = isset($data->post_service->url) ? $data->post_service->url : '';
                                $post_service_dir_path = isset($data->post_service->dir_path) ? $data->post_service->dir_path : '';
                            ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="url"><?php echo lang('profile_post_service_url');?></label>
                                <div class="col-md-6">
                                    <input id="url" name="url" value="<?php echo set_value('url', $post_service_url); ?>" type="text" placeholder="<?php echo lang('profile_post_service_url');?>" class="form-control input-md">
                                    <div class="help-block ps_err">
                                        <?php echo form_error('url'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="dir_path"><?php echo lang('profile_post_service_dir');?></label>
                                <div class="col-md-6">
                                    <input id="dir_path" name="dir_path" value="<?php echo set_value('dir_path', $post_service_dir_path); ?>" type="text" placeholder="<?php echo lang('profile_post_service_dir');?>" class="form-control input-md">
                                    <div class="help-block">
                                        <div style="">Preview dir path : <strong>ab/<span style="color: red;"><?php echo set_value('dir_path', $post_service_dir_path); ?></span>/an</strong> </div>
                                    </div>
                                    <div class="help-block ps_err"> <?php echo form_error('dir_path'); ?> </div>
                                </div>
                            </div>
                            <?php endif; ?>

                            <?php if($type == "database") : ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="database"><?php echo lang('city_lbl_database');?></label>
                                    <div class="col-md-3">
                                        <?php if (0) : ?>
                                        <div id="database-switch" _target="#database_group" class="on_off make-switch" data-animated="false" data-on-label="<?php echo lang('city_btn_on'); ?>" data-off-label = '<?php echo lang('city_btn_off');?>'>
                                            <input name="check_database" type="checkbox" <?php echo $check_database ? 'checked="checked"' : '';;?>>
                                        </div>
                                        <?php endif; ?>
                                        <?php if($type == "database"): ?>
                                            <a id="btn_test" name="btn_test" value="test_connect" class="btn btn-sm btn-default" data-loading-text="Wait ...">
                                                 <i class="glyphicon glyphicon-stats"></i>&nbsp;<?php echo lang('test_connect');?>
                                            </a>
                                        <?php endif;?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="hostname"><?php echo lang('city_lbl_hostname');?></label>
                                    <div class="col-md-6">
                                        <input id="hostname" name="hostname" value="<?php echo set_value('db_hostname', isset($data->db_hostname) ? $data->db_hostname : ''); ?>" type="text" placeholder="<?php echo lang('city_lbl_hostname');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('hostname'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="username"><?php echo lang('city_lbl_username');?></label>
                                    <div class="col-md-6">
                                        <input id="username" name="username" value="<?php echo set_value('db_username', isset($data->db_username) ? $data->db_username : ''); ?>" type="text" placeholder="<?php echo lang('city_lbl_username');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('username'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="password"><?php echo lang('city_lbl_password');?></label>
                                    <div class="col-md-6">
                                        <input id="password" name="password" autocomplete="off" value="<?php echo set_value('db_password', isset($data->db_password) ? $data->db_password : '');?>" type="password" placeholder="<?php echo lang('city_lbl_password');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('password'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                         <button id="btn_showpassword" name="btn_showpassword" _target="#password,#passconf" class="toggle_password btn btn-default" ><?php echo lang('btn_hide'); ?></button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="passconf"><?php echo lang('city_lbl_password_confirm');?></label>
                                    <div class="col-md-6">
                                        <input id="passconf" name="passconf" value="<?php echo set_value('db_passconf', isset($data->db_password) ? $data->db_password : '');?>" type="password" placeholder="<?php echo lang('city_lbl_password_confirm');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('passconf'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="database"><?php echo lang('city_lbl_database');?></label>
                                    <div class="col-md-6">
                                        <input id="database" name="database" value="<?php echo set_value('db_database', isset($data->db_database) ? $data->db_database : ''); ?>" type="text" placeholder="<?php echo lang('city_lbl_database');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('database'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="port"><?php echo lang('city_lbl_port');?></label>
                                    <div class="col-md-6">
                                        <input id="port" name="port" value="<?php echo set_value('db_port', isset($data->db_port) ? $data->db_port : ''); ?>" type="text" placeholder="<?php echo lang('city_lbl_port');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('port'); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if ($type == 'storage') : ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="storage"><?php echo lang('city_lbl_storage');?></label>
                                    <div class="col-md-6">
                                        <div id="storage-switch" _target="#storage_group" class="on_off make-switch" data-animated="false" data-on-label="<?php echo lang('city_btn_on'); ?>" data-off-label = '<?php echo lang('city_btn_off');?>'>
                                            <input name="check_storage" type="checkbox" <?php echo $check_storage ? 'checked="checked"' : '';?>>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="type"><?php echo lang('city_lbl_type');?></label>
                                    <div class="col-md-6">
                                        <input id="storage_type" name="storage_type" value="<?php echo set_value('type', isset($data->st_type) ? $data->st_type : ''); ?>" type="text" placeholder="<?php echo lang('city_lbl_type');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('storage_type'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="ip"><?php echo lang('city_lbl_ip');?></label>
                                    <div class="col-md-6">
                                        <input id="storage_ip" name="storage_ip" value="<?php echo set_value('st_ip', isset($data->st_ip) ? $data->st_ip : ''); ?>" type="text" placeholder="<?php echo lang('city_lbl_ip');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('storage_ip'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="st_username"><?php echo lang('city_lbl_username');?></label>
                                    <div class="col-md-6">
                                        <input id="storage_username" name="storage_username" value="<?php echo set_value('st_username', isset($data->st_username) ? $data->st_username : ''); ?>" type="text" placeholder="<?php echo lang('city_lbl_username');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('storage_username'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="st_password"><?php echo lang('city_lbl_password');?></label>
                                    <div class="col-md-6">
                                        <input id="storage_password" name="storage_password" autocomplete="off" value="<?php echo set_value('st_password', isset($data->st_password) ? $data->st_password : '');?>" type="password" placeholder="<?php echo lang('city_lbl_password');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('storage_password'); ?>
                                        </div>
                                    </div>
                                   <div class="col-md-2">
                                         <button id="btn_st_showpassword" name="btn_st_showpassword" _target="#storage_password,#storage_passconf" class="toggle_password btn btn-default" ><?php echo lang('btn_hide');?></button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="st_password"><?php echo lang('city_lbl_password_confirm');?></label>
                                    <div class="col-md-6">
                                        <input id="storage_passconf" name="storage_passconf" value="<?php echo set_value('st_passconf', isset($data->st_password) ? $data->st_password : '');?>" type="password" placeholder="<?php echo lang('city_lbl_password_confirm');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('storage_passconf'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="port"><?php echo lang('city_lbl_port');?></label>
                                    <div class="col-md-6">
                                        <input id="storage_port" name="storage_port" value="<?php echo set_value('st_port', isset($data->st_port) ? $data->st_port : ''); ?>" type="text" placeholder="<?php echo lang('city_lbl_port');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('storage_port'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="st_home_dir"><?php echo lang('city_lbl_home_dir');?></label>
                                    <div class="col-md-6">
                                        <input id="storage_home_dir" name="storage_home_dir" value="<?php echo set_value('st_home_dir', isset($data->st_home_dir) ? $data->st_home_dir : ''); ?>" type="text" placeholder="<?php echo lang('city_lbl_home_dir');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('storage_home_dir'); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if ($type == "change-password") : ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="old_password"><?php echo lang('lbl_password');?></label>
                                    <div class="col-md-5">
                                        <input id="old_password" name="old_password" autocomplete="off" value="" type="password" placeholder="<?php echo lang('lbl_password');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('old_password'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="password"><?php echo lang('lbl_new_password');?></label>
                                    <div class="col-md-5">
                                        <input id="password" name="password" autocomplete="off" value="" type="password" placeholder="<?php echo lang('lbl_new_password');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('password'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="passconf"><?php echo lang('lbl_new_password_confirm');?></label>
                                    <div class="col-md-5">
                                        <input id="passconf" name="passconf" value="" type="password" placeholder="<?php echo lang('lbl_new_password_confirm');?>" class="form-control input-md">
                                        <div class="help-block ps_err">
                                            <?php echo form_error('passconf'); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if ($type == 'notification') : ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="notification"><?php echo lang('profile_notification');?></label>
                                    <div class="col-md-6">
                                        <div id="notification-switch" class="on_off make-switch" data-animated="false" data-on-label="<?php echo lang('city_btn_on'); ?>" data-off-label = '<?php echo lang('city_btn_off');?>'>
                                            <input name="check_notification" type="checkbox" <?php echo $check_notification ? 'checked="checked"' : '';?>>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="notification_email"><?php echo lang('lbl_email');?></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="notification_email" name = "notification_email[]" placeholder="<?php echo lang('profile_placeholder_email'); ?>" value="<?php echo isset(session_login(FALSE)->extra_email) ? session_login(FALSE)->extra_email : ''; ?>" />
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if ($type == 'timezone') : ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="material"><?php echo lang('profile_timezone_chosser');?></label>
                                    <div class="col-md-6">
                                        <?php echo timezone_menu( isset(session_login(false)->timezone) ? session_login(false)->timezone : false, 'form-control input-md', 'timezone'); ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if ($type == 'option') : ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="material"><?php echo lang('profile_material');?></label>
                                    <div class="col-md-6">
                                        <div id="material-switch" class="on_off make-switch" data-animated="false" data-on-label="<?php echo lang('city_btn_on'); ?>" data-off-label = '<?php echo lang('city_btn_off');?>'>
                                            <input name="check_material" type="checkbox" <?php echo $check_material ? 'checked="checked"' : '';?>>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="selection"><?php echo lang('profile_selection');?></label>
                                    <div class="col-md-6">
                                        <div id="selection-switch" class="on_off make-switch" data-animated="false" data-on-label="<?php echo lang('city_btn_on'); ?>" data-off-label = '<?php echo lang('city_btn_off');?>'>
                                            <input name="check_selection" type="checkbox" <?php echo $check_selection ? 'checked="checked"' : '';?>>
                                        </div>
                                    </div>
                                </div>
                            <?php endif;?>

                            <?php if ($type === false) : ?>
                                <style type="text/css">
                                .form-control-static {line-height: 30px;}
                                </style>
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="username"><?php echo lang('lbl_username');?></label>
                                        <div class="col-md-6">
                                            <p class="form-control-static"><?php echo isset(session_login(FALSE)->username) ? session_login(FALSE)->username : ''; ?></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="email"><?php echo lang('lbl_email');?></label>
                                        <div class="col-md-6">
                                            <p class="form-control-static"><?php echo isset(session_login(FALSE)->email) ? session_login(FALSE)->email : ''; ?></p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="city"><?php echo lang('lbl_city');?></label>
                                        <div class="col-md-6">
                                            <input name="general_txt_city" value="<?php echo isset($data->name) ? $data->name : ''; ?>" type="text" class="form-control" id="general_txt_city" placeholder="Email" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="created_at"><?php echo lang('lbl_created_at');?></label>
                                        <div class="col-md-6">
                                            <p class="form-control-static"><?php echo isset(session_login(FALSE)->created_at) ? session_login(FALSE)->created_at : ''; ?></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="last_login"><?php echo lang('lbl_last_login');?></label>
                                        <div class="col-md-6">
                                            <p class="form-control-static"><?php echo (isset(session_login(FALSE)->last_login) && strtotime(session_login(FALSE)->last_login)!='') ?session_login(FALSE)->last_login: ''; ?></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-primary"><?php echo lang('btn_save_change'); ?></button>
                                        </div>
                                    </div>
                                </form>
                            <?php else: ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">&nbsp;</label>
                                    <div class="col-md-6">
                                        <button id="btn_save" name="btn_save" value="save" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change'); ?></button>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </form>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>