<script type="text/javascript">
    $(function(){
        init_modal('#modal_material_detail');
        init_modal('#modal_material_delete');

        var column_properties = $.parseJSON('<?php echo $column_properties; ?>');
        var grid_config = {
            'target': '#grid_material',
            'url'   : '<?php echo current_url(); ?>',
            'limit' : <?php echo isset($limit) ? intval($limit) : 0; ?>,
            'columns': [
                { field: "order", title: '<?php echo lang('grid_number');?>', filterable: false, sortable: false, width: column_properties.order_width, template: function(order){return grid_number = grid_number + 1;} },
                { field: "name", title: '<?php echo lang('material_lbl_name')?>'},
                { field: "unit", title: '<?php echo lang('material_lbl_unit')?>', width:column_properties.unit_width },
                { field: "created_at", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", title: '<?php echo lang('grid_created_at')?>', width: column_properties.created_width },
                { field: "updated_at", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", title: '<?php echo lang('grid_updated_at')?>', width: column_properties.updated_width },
                { field: "option", title: '<?php echo lang('grid_option'); ?>', filterable: false, sortable: false, width: column_properties.option_width, attributes:{style: "text-align:center"}, template: function(data) {
                        flag = data.id > 3 ? true : false;
                        html = '';
                        html = "<a _modal='#modal_material_detail' _title='<?php echo lang('material_lbl_detail'); ?>' href='<?php echo create_url('sys/material/edit'); ?>/"+data.id+"' class='sys_modal_detail btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span>&nbsp;<?php echo lang('btn_edit')?></a>&nbsp;";
                        html = html + "<a _id_grid='#grid_material' _modal='#modal_material_delete' _title='<?php echo lang('delete_confirm'); ?>' href='<?php echo create_url('sys/material/delete'); ?>/"+data.id+"' class='sys_modal_delete btn btn-danger btn-xs " + (data.id > 3 ? '' : 'disabled') + "'><span class='glyphicon glyphicon-remove'></span>&nbsp;<?php echo lang('btn_delete')?></a>";
                        return html;
                    }
                }
            ]
        };
        create_grid(grid_config);
    });
</script>

<div id="grid_material"></div>
<script id="toolbar_template" type="text/x-kendo-template">
    <div>
        <span class="pull-left">
            <h4>
                &nbsp;<span class="glyphicon glyphicon-user"></span>
                &nbsp;<strong><?php echo lang('material_lbl_list');?></strong>
            </h4>
        </span>
        <span class="pull-right">
            <a _modal="\\#modal_material_detail" _title='<?php echo lang('material_lbl_detail'); ?>' href="<?php echo create_url('sys/material/add'); ?>" class="sys_modal_detail btn btn-primary btn-default"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new');?></a>
        </span>
    </div>
</script>
<script id="delete-confirmation" type="text/x-kendo-template">
    <div class="row">
        <span class="col-md-2"></span>
        <span class="col-md-8">
            <p class="delete-message"><?php echo lang('material_msg_question_delete'); ?></p>
        </span>
        <span class="col-md-2"></span>
    </div>
    <div class="row">
        <span class="col-md-7"></span>
        <span class="col-md-5">
            <span class="pull-right">
                <button class="delete-confirm btn btn-danger"><?php echo lang('btn_yes');?></button>
                <a href="#" class="delete-cancel btn btn-default"><?php echo lang('btn_no');?></a>
            </span>
        </span>
    </div>
</script>