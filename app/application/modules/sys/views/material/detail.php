<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("sys/material/edit/$id");
    } else {
        $form_link = create_url("sys/material/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_material_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_material');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>

    $(function(){
        $('.select2').select2({ width: 'resolve' });
        $('#frm_material_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_material_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });
    });
</script>
<div id="frm_material_detail_wraper">
    <form class="form-horizontal" id="frm_material_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-3 control-label" for="name"><?php echo lang('material_lbl_name');?></label>
            <div class="col-md-6">
                <input id="name" name="name" value="<?php echo set_value('name', isset($data->name) ? $data->name : ''); ?>" type="text" placeholder="<?php echo lang('material_holder_name');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('name'); ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="unit"><?php echo lang('material_lbl_unit');?></label>
            <div class="col-md-6">
                <input id="unit" name="unit" value="<?php echo set_value('unit', isset($data->unit) ? $data->unit : ''); ?>" type="text" placeholder="<?php echo lang('material_holder_unit');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('unit'); ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="btn_save"></label>
            <div class="col-md-6">
                <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a class="btn btn-default" href="javascript:close_modal('#modal_material_detail');"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>
    </form>
</div>