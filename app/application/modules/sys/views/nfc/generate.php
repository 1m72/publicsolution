<?php
    $form_link = create_url("sys/nfc/generate");
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<div id="frm_nfc_detail_wraper">
    <script type="text/javascript">
        <?php if (isset($flag) AND ($flag) ) : ?>
        setTimeout(
            function(){
                close_modal('#modal_nfc_generate');
                <?php echo $callback ? "$callback();" : "refresh_grid('#grid_nfc');"; ?>
            },
            timeout_dialog
        );
        <?php endif; ?>

        $(function(){
            $('#frm_detail').submit(function(e){
                e.preventDefault();
                i = $(this);
                $.ajax({
                    url: i.attr('action'),
                    type: 'POST',
                    data: i.serialize(),
                    success: function(res) {
                        $('#frm_nfc_detail_wraper').html(res);
                    },
                    error: function(err) {}
                });
            });
        });
    </script>
    <form class="form-horizontal" id="frm_detail" action="<?php echo $form_link;?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <?php if (isset($err)) : ?>
        <div class="alert alert-danger"><?php echo $err; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-4 control-label" for="total"><?php echo lang('nfc_total');?></label>
            <div class="col-md-6">
                <input id="total" name="total" value="<?php echo set_value('total', 20); ?>" type="text" placeholder="<?php echo lang('nfc_holder_total');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('total'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="btn_save"></label>
            <div class="col-md-8">
                <button id="btn_save" name="btn_save" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a class="btn btn-default" href="javascript:close_modal('#modal_nfc_generate');"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>
    </form>
</div>