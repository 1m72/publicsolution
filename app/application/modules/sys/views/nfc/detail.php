<?php
    $form_link = create_url("sys/nfc/generate");
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<div id="frm_nfc_detail_wraper">
    <script type="text/javascript">
        <?php if (isset($flag) AND ($flag) ) : ?>
        setTimeout(
            function(){
                close_modal('#modal_nfc_detail');
                <?php echo $callback ? "$callback();" : "refresh_grid('#grid_nfc');"; ?>
            },
            timeout_dialog
        );
        <?php endif; ?>

        $(function(){
            $('#frm_detail').submit(function(e){
                e.preventDefault();
                i = $(this);
                $.ajax({
                    url: i.attr('action'),
                    type: 'POST',
                    data: i.serialize(),
                    success: function(res) {
                        $('#frm_nfc_detail_wraper').html(res);
                    },
                    error: function(err) {}
                });
            });
        });
    </script>
    <form class="form-horizontal" id="frm_detail" action="<?php  if(isset($id) && (intval($id) > 0)) echo create_url('sys/nfc/edit/' . (isset($id) ? $id : '') ); else echo create_url('sys/nfc/add');?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-4 control-label" for="name"><?php echo lang('nfc_lbl');?></label>
            <div class="col-md-5">
                <input id="name" name="name" value="<?php echo set_value('name', isset($data->code) ? $data->code : ''); ?>" type="text" placeholder="<?php echo lang('nfc_holder_name');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('name'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="btn_save"></label>
            <div class="col-md-8">
                <button id="btn_save" name="btn_save" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a class="btn btn-default" href="javascript:close_modal('#modal_nfc_detail');"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>
    </form>
</div>