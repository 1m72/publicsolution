<script type="text/javascript">
    $ (function(){
        init_modal('#modal_nfc_generate');
        init_modal('#modal_nfc_detail');

        var column_properties = $.parseJSON('<?php echo $column_properties; ?>');
        var grid_config = {
            'target': '#grid_nfc',
            'url': '<?php echo current_url(); ?>',
            'limit': <?php echo isset($limit) ? intval($limit) : 0; ?>,
            'columns': [
                { field: "order", title: '<?php echo lang('grid_number');?>', filterable: false, sortable: false, width: column_properties.order_width, template: function(order){return grid_number = grid_number + 1;} },
                { field: "nfc_code", title: '<?php echo lang('nfc_lbl_name')?>', width:column_properties.name_width},
                { field: "code", title: '<?php echo lang('nfc_lbl_value')?>'},
                { field: "active", title: '<?php echo lang('nfc_lbl_actived')?>', filterable: false, sortable: false, width: column_properties.city_active_width, template: "#=active==0?'<span class=\"label label-success\"><?php echo lang('btn_on'); ?></span>':'<span class=\"label label-default\"><?php echo lang('btn_off'); ?></span>'#"},
                { field: "created_at", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", title: '<?php echo lang('grid_created_at')?>', width: column_properties.created_width },
                { field: "updated_at", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", title: '<?php echo lang('grid_updated_at')?>', width: column_properties.updated_width },
                { field: "option", title: '<?php echo lang('grid_option'); ?>', filterable: false, sortable: false, width: column_properties.option_width, attributes:{style: "text-align:center"}, template: function(data) {
                        html = "<a _modal='#modal_nfc_detail' _title='<?php echo lang('nfc_lbl_detail'); ?>' href='<?php echo create_url('sys/nfc/edit'); ?>/"+data.id+"' class='sys_modal_detail btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span>&nbsp;<?php echo lang('btn_edit')?></a>";
                        return html;
                    }
                }
            ]
        }
        create_grid(grid_config);
    });
</script>

<div id="grid_nfc"></div>
<script id="toolbar_template" type="text/x-kendo-template">
    <div>
        <span class="pull-left">
            <h4>
                &nbsp;<span class="glyphicon glyphicon-user"></span>
                &nbsp;<strong><?php echo lang('nfc_lbl_list');?></strong>
            </h4>
        </span>
        <span class="pull-right">
            <a _modal="\\#modal_nfc_generate" _title='<?php echo lang('btn_add_new'); ?>' href="<?php echo create_url('sys/nfc/generate'); ?>" class="sys_modal_detail btn btn-primary btn-default"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new');?></a>
        </span>
    </div>
</script>
<script id="delete-confirmation" type="text/x-kendo-template">
    <div class="row">
        <span class="col-md-2"></span>
        <span class="col-md-8">
            <p class="delete-message"><?php echo lang('nfc_msg_question_delete'); ?></p>
        </span>
        <span class="col-md-2"></span>
    </div>
    <div class="row">
        <span class="col-md-7"></span>
        <span class="col-md-5">
            <span class="pull-right">
                <button class="delete-confirm btn btn-danger"><?php echo lang('btn_yes');?></button>
                <a href="#" class="delete-cancel btn btn-default"><?php echo lang('btn_no');?></a>
            </span>
        </span>
    </div>
</script>