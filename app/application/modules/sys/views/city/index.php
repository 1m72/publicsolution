<script type="text/javascript">
    $(function() {
        init_modal('#modal_city_detail');
        init_modal('#modal_city_delete');

        var column_properties = $.parseJSON('<?php echo $column_properties; ?>');
        var grid_config = {
            'target': '#grid_city',
            'url': '<?php echo current_url(); ?>',
            'limit': <?php echo isset($limit) ? intval($limit) : 0; ?>,
            'columns':[
                { field: "number", title: '<?php echo lang('grid_number');?>', filterable: false, sortable: false,width: column_properties.no_width, template: function(data) {return grid_number = grid_number + 1;} },
                { field: "name", title: '<?php echo lang('city_grid_name'); ?>', },
                { field: "city_database", title: '<?php echo lang('city_grid_database'); ?>', filterable: false, sortable: false, width: column_properties.city_database_width, template: "#=city_database==1?'<span class=\"label label-success\"><?php echo lang('btn_on'); ?></span>':'<span class=\"label label-default\"><?php echo lang('btn_off'); ?></span>'#"},
                { field: "city_storage", title: '<?php echo lang('city_grid_storage'); ?>', filterable: false, sortable: false, width: column_properties.city_storage_width, template: "#=city_storage==1?'<span class=\"label label-success\"><?php echo lang('btn_on'); ?></span>':'<span class=\"label label-default\"><?php echo lang('btn_off'); ?></span>'#"},
                { field: "created_at", title: '<?php echo lang('grid_created_at'); ?>',type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", width: column_properties.created_at_width,  },
                { field: "updated_at", title: '<?php echo lang('grid_updated_at'); ?>',type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", width: column_properties.updated_at_width,  },
                { field: "option", title: '<?php echo lang('grid_option'); ?>', attributes:{style: "text-align:center"},filterable: false, sortable: false, width: column_properties.option_width, template: function(data) {
                        html = "<a _modal='#modal_city_detail' _title='<?php echo lang('city_city_detail'); ?>' href='<?php echo create_url('sys/city/edit'); ?>/"+data.id+"' class='sys_modal_detail btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span>&nbsp;<?php echo lang('btn_edit'); ?></a>&nbsp;";
                        html = html + "<a _id_grid = '#grid_city' _modal='#modal_city_delete' _title='<?php echo lang('delete_confirm'); ?>' href='<?php echo create_url('sys/city/delete'); ?>/"+data.id+"' class='sys_modal_delete btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span>&nbsp;<?php echo lang('btn_delete'); ?></a>";
                        return html;
                    }
                }
            ]
        };
        create_grid(grid_config);
    });
</script>
<div id="grid_city"></div>
<script id="toolbar_template" type="text/x-kendo-template">
    <div>
        <span class="pull-left">
            <h4>
                &nbsp;<span class="glyphicon glyphicon-user"></span>
                &nbsp;<strong><?php echo lang('city_list_city');?></strong>
            </h4>
        </span>
        <span class="pull-right">
            <a _modal="\\#modal_city_detail" _title='<?php echo lang('city_city_detail'); ?>' href="<?php echo create_url('sys/city/add'); ?>" class="sys_modal_detail btn btn-primary btn-default"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new');?></a>
        </span>
    </div>
</script>
<script id="delete-confirmation" type="text/x-kendo-template">
    <div class="row">
        <span class="col-md-2"></span>
        <span class="col-md-8">
            <p class="delete-message"><?php echo lang('city_msg_delete');?></p>
        </span>
        <span class="col-md-2"></span>
    </div>
    <div class="row">
        <span class="col-md-7"></span>
        <span class="col-md-5">
            <span class="pull-right">
                <button class="delete-confirm btn btn-danger"><?php echo lang('btn_yes');?></button>
                <a href="#" class="delete-cancel btn btn-default"><?php echo lang('btn_no');?></a>
            </span>
        </span>
    </div>
</script>