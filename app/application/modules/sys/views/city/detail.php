<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("sys/city/edit/$id");
    } else {
        $form_link = create_url("sys/city/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<?php
    $check_database = ''; if(isset($data->database) &&($data->database==1)) $check_database = 'checked';
    $check_storage  = ''; if(isset($data->storage)  &&($data->storage==1)) $check_storage = 'checked';
    $database_password_type  ="password";
?>
<style>
    #info_db_st{
    max-height:450px;
    overflow-y:auto;
    overflow-x:hidden;
    margin-bottom: 15px;
}
</style>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_city_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_city');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>

    $(function(){
        $('.toggle_password').click(function(e){
            e.preventDefault();
            i = $(this);
            target = i.attr('_target');
            if ($(target).attr('type') != 'password') {
                $(target).attr('type', 'password');
                i.text('<?php echo $this->lang->line("btn_show");?>');
            } else {
                $(target).attr('type', 'text');
                i.text('<?php echo $this->lang->line("btn_hide");?>');
            }
        });
        // on off
        $('#storage_group').hide();
        $('#database_group').hide();

        var check_storage='<?php echo $check_storage?>';
        var check_database='<?php echo $check_database?>';

        if(check_storage==''){
            $('#storage_group').hide();
        }
        else{
            $('#storage_group').show();
        }

        if(check_database==''){
            $('#database_group').hide();
        }
        else{
            $('#database_group').show();
        }

        $('.on_off').on('switch-change',function(e,data){
            var status = data.value;
            e.preventDefault();
            i = $(this);
            target = i.attr('_target');
            if(status==false){
                $(target).hide();
            }
            else{
                $(target).show();
            }
        });

        $('#frm_city_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_city_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });
    });
</script>
<div id="frm_city_detail_wraper">
    <form class="form-horizontal" id="frm_city_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>
        <div id="info_db_st">
            <div class="form-group">
                <label class="col-md-4 control-label" for="name"><?php echo $this->lang->line('city_lbl_city_name');?><span class="star">&nbsp;*</span></label>
                <div class="col-md-5">
                    <input id="name" name="name" value="<?php echo set_value('name', isset($data->name) ? $data->name : ''); ?>" type="text" placeholder="<?php echo $this->lang->line('city_lbl_city_name')?>" class="form-control input-md">
                    <div class="help-block ps_err">
                        <?php echo form_error('name'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="passcode"><?php echo $this->lang->line('city_lbl_passcode');?><span class="star">&nbsp;*</span></label>
                <div class="col-md-5">
                    <input id="passcode" name="passcode" autocomplete="off" value = "<?php echo set_value('passcode', isset($data->passcode) ? $data->passcode : ''); ?>" type="text" placeholder="<?php echo $this->lang->line('city_lbl_passcode');?>" class="form-control input-md">
                    <div class="help-block ps_err">
                        <?php echo form_error('passcode'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                     <button id="btn_showpasscode" name="btn_showpasscode" _target="#passcode" class="toggle_password btn btn-default" ><?php echo $this->lang->line('btn_hide');?></button>
                </div>
            </div>
            <hr />

            <div class="form-group">
                <label class="col-md-4 control-label" for="database"><?php echo $this->lang->line('city_lbl_database');?></label>
                <div class="col-md-5">
                    <div id="database-switch" _target="#database_group" class="on_off make-switch" data-animated="false" data-on-label="<?php echo lang('city_btn_on'); ?>" data-off-label = '<?php echo lang('city_btn_off');?>'>
                        <input name="check_database" type="checkbox" <?php echo $check_database;?>>
                    </div>
                </div>
            </div>

            <div id= "database_group">
                <div class="form-group">
                    <label class="col-md-4 control-label" for="hostname"><?php echo $this->lang->line('city_lbl_hostname');?></label>
                    <div class="col-md-5">
                        <input id="hostname" name="hostname" value="<?php echo set_value('db_hostname', isset($data->db_hostname) ? $data->db_hostname : ''); ?>" type="text" placeholder="<?php echo $this->lang->line('city_lbl_hostname');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('hostname'); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="username"><?php echo $this->lang->line('city_lbl_username');?></label>
                    <div class="col-md-5">
                        <input id="username" name="username" value="<?php echo set_value('db_username', isset($data->db_username) ? $data->db_username : ''); ?>" type="text" placeholder="<?php echo $this->lang->line('city_lbl_username');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('username'); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="password"><?php echo $this->lang->line('city_lbl_password');?><span class="star">&nbsp;*</span></label>
                    <div class="col-md-5">
                        <input id="password" name="password" autocomplete="off" value="<?php echo set_value('db_password', isset($data->db_password) ? $data->db_password : '');?>" type="password" placeholder="<?php echo $this->lang->line('city_lbl_password');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('password'); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                         <button id="btn_showpassword" name="btn_showpassword" _target="#password,#passconf" class="toggle_password btn btn-default" ><?php echo $this->lang->line('btn_hide'); ?></button>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="passconf"><?php echo $this->lang->line('city_lbl_password_confirm');?><span class="star">&nbsp;*</span></label>
                    <div class="col-md-5">
                        <input id="passconf" name="passconf" value="<?php echo set_value('db_passconf', isset($data->db_password) ? $data->db_password : '');?>" type="password" placeholder="<?php echo $this->lang->line('city_lbl_password_confirm');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('passconf'); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="database"><?php echo $this->lang->line('city_lbl_database');?></label>
                    <div class="col-md-5">
                        <input id="database" name="database" value="<?php echo set_value('db_database', isset($data->db_database) ? $data->db_database : ''); ?>" type="text" placeholder="<?php echo $this->lang->line('city_lbl_database');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('database'); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="port"><?php echo $this->lang->line('city_lbl_port');?></label>
                    <div class="col-md-5">
                        <input id="port" name="port" value="<?php echo set_value('db_port', isset($data->db_port) ? $data->db_port : ''); ?>" type="text" placeholder="<?php echo $this->lang->line('city_lbl_port');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('port'); ?>
                        </div>
                    </div>
                </div>
            </div>
           <hr />
            <div class="form-group">
                <label class="col-md-4 control-label" for="storage"><?php echo $this->lang->line('city_lbl_storage');?></label>
                <div class="col-md-5">
                    <div id="storage-switch" _target="#storage_group" class="on_off make-switch" data-animated="false" data-on-label="<?php echo lang('city_btn_on'); ?>" data-off-label = '<?php echo lang('city_btn_off');?>'>
                        <input name="check_storage" type="checkbox" <?php echo $check_storage;?>>
                    </div>
                </div>
            </div>

            <div id= "storage_group">
                <div class="form-group">
                    <label class="col-md-4 control-label" for="type"><?php echo $this->lang->line('city_lbl_type');?></label>
                    <div class="col-md-5">
                        <input id="storage_type" name="storage_type" value="<?php echo set_value('type', isset($data->st_type) ? $data->st_type : ''); ?>" type="text" placeholder="<?php echo $this->lang->line('city_lbl_type');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('type'); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="ip"><?php echo $this->lang->line('city_lbl_ip');?></label>
                    <div class="col-md-5">
                        <input id="storage_ip" name="storage_ip" value="<?php echo set_value('st_ip', isset($data->st_ip) ? $data->st_ip : ''); ?>" type="text" placeholder="<?php echo $this->lang->line('city_lbl_ip');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('storage_ip'); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="st_username"><?php echo $this->lang->line('city_lbl_username');?></label>
                    <div class="col-md-5">
                        <input id="storage_username" name="storage_username" value="<?php echo set_value('st_username', isset($data->st_username) ? $data->st_username : ''); ?>" type="text" placeholder="<?php echo $this->lang->line('city_lbl_username');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('username_s'); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="st_password"><?php echo $this->lang->line('city_lbl_password');?></label>
                    <div class="col-md-5">
                        <input id="storage_password" name="storage_password" autocomplete="off" value="<?php echo set_value('st_password', isset($data->st_password) ? $data->st_password : '');?>" type="password" placeholder="<?php echo $this->lang->line('city_lbl_password');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('storage_password'); ?>
                        </div>
                    </div>
                   <div class="col-md-2">
                         <button id="btn_st_showpassword" name="btn_st_showpassword" _target="#storage_password,#storage_passconf" class="toggle_password btn btn-default" ><?php echo $this->lang->line('btn_hide');?></button>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="st_password"><?php echo $this->lang->line('city_lbl_password_confirm');?></label>
                    <div class="col-md-5">
                        <input id="storage_passconf" name="storage_passconf" value="<?php echo set_value('st_passconf', isset($data->st_password) ? $data->st_password : '');?>" type="password" placeholder="<?php echo $this->lang->line('city_lbl_password_confirm');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('storage_passconf'); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="port"><?php echo $this->lang->line('city_lbl_port');?></label>
                    <div class="col-md-5">
                        <input id="storage_port" name="storage_port" value="<?php echo set_value('st_port', isset($data->st_port) ? $data->st_port : ''); ?>" type="text" placeholder="<?php echo $this->lang->line('city_lbl_port');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('storage_port'); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="st_home_dir"><?php echo $this->lang->line('city_lbl_home_dir');?></label>
                    <div class="col-md-5">
                        <input id="storage_home_dir" name="storage_home_dir" value="<?php echo set_value('st_home_dir', isset($data->st_home_dir) ? $data->st_home_dir : ''); ?>" type="text" placeholder="<?php echo $this->lang->line('city_lbl_home_dir');?>" class="form-control input-md">
                        <div class="help-block ps_err">
                            <?php echo form_error('storage_home_dir'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="btn_save"></label>
            <div class="col-md-8">
                <button id="btn_save" name="btn_save" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo $this->lang->line('btn_save_change');?></button>
                <a id="btn_cancel" href="javascript:close_modal('#modal_city_detail');" name="btn_cancel" class="btn btn-default"><?php echo $this->lang->line('btn_cancel');?></a>
            </div>
        </div>
    </form>
</div>