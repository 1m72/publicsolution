<style type="text/css">
    #frm_machine_detail_wraper .nav>li>a {padding: 5px;}
    .tab-content .checkbox {line-height: 20px;}
</style>
<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("sys/machine/edit/$id");
    } else {
        $form_link = create_url("sys/machine/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_machine_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_machine');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>

    $(function(){
        $('.select2').select2({ width: 'resolve' });
        $('#frm_machine_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_machine_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });

        $('#frm_machine_detail').on('click', '.chk_module', function(e){
            i = $(this);
            module_validate(i.val());
        });

        $('#frm_machine_detail').on('change', '.chk_activity', function(){
            i = $(this);
            if (i.is(':checked')) {
                $('.rdo_activity_default[value="'+i.val()+'"]').removeAttr('disabled');
            } else {
                y = $('.rdo_activity_default[value="'+i.val()+'"]');
                if (y.is(':checked')) {
                    $('.rdo_activity_default:not([disabled])').attr('checked', 'checked');
                }
                y.attr('disabled', 'disabled');
            }
        });

        setTimeout(function() {
            $('#frm_machine_detail .chk_module').each(function(index){
                i = $(this);
                module_validate(i.val());
                $('.btn').button('reset');;
            });
        }, 300);
    });

    function module_validate(id_module) {
        i = $('.chk_module[value="'+id_module+'"]');
        if (i.is(':checked')) {
            $('#tab_' + i.val()).find('.chk_activity, .chk_addon, .rdo_activity_default').removeAttr('disabled');
        } else {
            $('#tab_' + i.val()).find('.chk_activity, .chk_addon, .rdo_activity_default').attr('disabled', 'disabled');
        }

        $('#tab_' + i.val()).find('.chk_activity:not(:checked)').each(function(index){
            y = $(this);
            $('#tab_' + i.val()).find('.rdo_activity_default[value="' +y.val()+ '"]').attr('disabled', 'disabled');
        });
    }
</script>
<div id="frm_machine_detail_wraper">
    <form class="form-horizontal" id="frm_machine_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>
        <?php if (isset($error)) : ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php endif; ?>

        <!-- identifier -->
        <div class="form-group">
            <label class="col-md-3 control-label" for="txt_identifier"><?php echo lang('lbl_identifier');?></label>
            <div class="col-md-6">
                <input id="txt_identifier" name="txt_identifier" value="<?php echo set_value('txt_identifier', isset($machine->machine->identifier) ? $machine->machine->identifier : ''); ?>" type="text" placeholder="<?php echo lang('holder_identifer');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('txt_identifier'); ?>
                </div>
            </div>
        </div>
        <!--// identifier -->

        <!-- Machine -->
        <div class="form-group">
            <label class="col-md-3 control-label" for="name"><?php echo lang('mc_lbl_machine');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-6">
                <input id="txt_name" name="txt_name" value="<?php echo set_value('txt_name', isset($machine->machine->name) ? $machine->machine->name : ''); ?>" type="text" placeholder="<?php echo lang('mc_holder_name');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('txt_name'); ?>
                </div>
            </div>
        </div>
        <!--// Machine -->

        <?php if (0) : ?>
        <div class="form-group hide">
            <label class="col-md-3 control-label" for="module"><?php echo lang('lbl_module');?></label>
            <div class="col-md-9">
                <select id="module" multiple="multiple" class="select2" style="width: 457px; border-bottom: none;">
                    <?php foreach ($modules as $key => $value) : ?>
                    <option value="<?php echo $key; ?>" <?php echo (isset($module_selected) AND (isset($module_selected[$key])) AND is_array($module_selected[$key]) ) ? 'selected="selected"' : ''; ?>><?php echo $value; ?></option>
                    <?php endforeach; ?>
                </select>
                <div style="margin-right: 10px; overflow: auto; border: 0px solid #aaa; padding-top: 7px;">
                    <div id="panel-left" style="float: left; min-height: 10px; width: 200px; "></div>
                    <div id="panel-right" style="float: left; width: 250px; border-left: 1px solid #aaa; "></div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <!-- NFC code -->
        <div class="form-group">
            <label class="col-md-3 control-label" for="nfc-code"><?php echo lang('mc_lbl_nfc_code');?></label>
            <div class="col-md-6">
                <select id="sel_nfc" name="sel_nfc" class="select2" style="width: 300px;">
                    <option><?php echo lang('mc_lbl_select_nfc_code');?></option>
                    <?php if (isset($nfc) AND !empty($nfc)) : ?>
                    <?php foreach ($nfc as $key => $value) : ?>
                    <option value="<?php echo $value->nfc_code; ?>" <?php echo set_select('sel_nfc', $value->nfc_code, (isset($machine->machine->nfc_code) AND ($machine->machine->nfc_code == $value->nfc_code) )); ?>><?php echo $value->code; ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <div class="help-block ps_err">
                    <?php echo form_error('sel_nfc'); ?>
                </div>
            </div>
        </div>
        <!--// NFC code -->

        <div class="form-group">
              <label class="col-md-3 control-label" for="txt_description"><?php echo lang('mc_lbl_description');?></label>
              <div class="col-md-6">
                    <textarea class="form-control input-md" id="txt_description" name="txt_description" style="width: 300px; height: auto;"  ><?php echo set_value('txt_description', isset($machine->machine->description) ? $machine->machine->description : ''); ?></textarea>
                    <div class="help-block ps_err">
                        <?php echo form_error('txt_description'); ?>
                    </div>
              </div>
        </div>

        <?php if (isset($modules) AND is_array($modules) AND !empty($modules) AND 0) : ?>
        <!-- Module -->
        <div class="form-group">
            <label class="col-md-3 control-label" for="chk_module"><?php echo lang('crumb_modules');?></label>
            <div class="col-md-9" style="padding: 3px 0px 0px 15px; margin: 0px; ">
                <?php echo form_error('chk_module', '<span class="help-block ps_err">', '</span>'); ?>
                <?php
                    $flag = false;
                    $rdo_activity_default_error = '';
                    foreach ($modules as $module_key => $module_value) {
                        if (isset($_POST['chk_module']) AND in_array($module_key, $_POST['chk_module'])) {
                            $tmp = 'rdo_activity_default_'.$module_key;
                            if ((form_error($tmp) != '') AND !$flag) {
                                $rdo_activity_default_error = form_error($tmp, '<span class="help-block ps_err">' . $module_value . ': ', '</span>');
                                $flag = true;
                            }
                        }
                    }
                    echo $rdo_activity_default_error;
                ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-1 control-label">&nbsp;</label>
            <div class="col-md-10" style="padding: 0px;">
                <?php foreach ($modules as $module_key => $module_value) : ?>
                <div class="row hide">
                    <div class="col-md-12" style="padding-bottom: 10px;">
                        <div class="checkbox"><label><input class="chk_module" type="checkbox" name="chk_module[]" value="<?php echo $module_key; ?>" <?php echo set_checkbox('chk_module', $module_key, ((isset($machine_activities[$module_key]) OR 1) ? true : false) ); ?>> Machine used for this module</label></div>
                    </div>
                </div>
                <div class="row <?php echo (is_null($id_module) OR (intval($module_key) !== intval($id_module))) ? 'hide' : ''; ?>">
                    <!-- activities -->
                    <div class="col-md-6">
                        <strong>
                            <?php echo lang('crumb_activity'); ?>
                            <span class="pull-right" style="font-size: small;font-style: italic;font-weight: normal;"><?php echo lang('lbl_activity_default'); ?></span>
                        </strong>
                        <div style="height: 150px; overflow: auto; border: 1px solid #D6D3D3; padding: 0px 5px 9px 5px; margin-top: 5px;">
                            <?php if (isset($activities) AND is_array($activities) AND !empty($activities)) : ?>
                            <?php foreach ($activities as $activity_key => $activity_value) : ?>
                            <div class="checkbox">
                                <label><input class="chk_activity" type="checkbox" name="chk_activity[]" value="<?php echo "{$module_key}_{$activity_value->id}"; ?>" <?php echo set_checkbox('chk_activity', "{$module_key}_{$activity_value->id}", ((isset($machine_activities[$module_key]['activiti']) AND in_array($activity_value->id, $machine_activities[$module_key]['activiti'])) ? true : false) ); ?>> <?php echo $activity_value->name; ?></label>
                                <input type="radio" class="rdo_activity_default pull-right" name="rdo_activity_default_<?php echo $module_key; ?>[]" value="<?php echo "{$module_key}_{$activity_value->id}"; ?>" class="pull-right" <?php echo set_radio('rdo_activity_default_'.$module_key, "{$module_key}_{$activity_value->id}", ((isset($machine_activities[$module_key]['activiti_default']) AND ($activity_value->id == $machine_activities[$module_key]['activiti_default']) ) ? true : false)); ?>>
                            </div>
                            <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <?php if (isset($_POST['chk_module']) AND in_array($module_key, $_POST['chk_module'])) : ?>
                        <div class="help-block ps_err">
                            <?php echo form_error('chk_activity'); ?>
                        </div>
                        <?php endif; ?>
                    </div>
                    <!--// activities -->

                    <!-- addons -->
                    <div class="col-md-6">
                        <strong><?php echo lang('crumb_addon'); ?></strong>
                        <div style="height: 150px; overflow: auto; border: 1px solid #D6D3D3; padding: 0px 5px 9px 5px; margin-top: 5px;">
                            <?php if (isset($addon[$module_key]) AND is_array($addon[$module_key]) AND !empty($addon[$module_key])) : ?>
                            <?php foreach ($addon[$module_key] as $addon_key => $addon_value) : ?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="chk_addon" name="chk_addon[]" value="<?php echo "{$module_key}_{$addon_value->id}"; ?>" <?php echo set_checkbox('chk_addon', "{$module_key}_{$addon_value->id}", ( isset($machine_addon[$module_key]) AND is_array($machine_addon[$module_key]) AND in_array($addon_value->id, $machine_addon[$module_key]) ) ? true : false); ?>> <?php echo $addon_value->name; ?>
                                </label>
                            </div>
                            <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!--// addons -->
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <!--// Module -->
        <?php endif; ?>

        <!-- Module -->
        <div class="form-group">
            <label class="col-md-1 control-label">&nbsp;</label>
            <div class="col-md-10" style="padding: 0px;">
                <?php echo form_error('chk_module', '<div class="alert alert-danger">', '</div>'); ?>
                <?php
                    $flag = false;
                    $rdo_activity_default_error = '';
                    foreach ($modules as $module_key => $module_value) {
                        if (isset($_POST['chk_module']) AND in_array($module_key, $_POST['chk_module'])) {
                            $tmp = 'rdo_activity_default_'.$module_key;
                            if ((form_error($tmp) != '') AND !$flag) {
                                $rdo_activity_default_error = form_error($tmp, $module_value . ': ', '');
                                $flag = true;
                            }
                        }
                    }

                    if (!empty($rdo_activity_default_error)) {
                        echo '<div class="alert alert-danger">'. $rdo_activity_default_error . '</div>';
                    }
                ?>
                <?php if (isset($modules) AND is_array($modules) AND !empty($modules)) : ?>
                    <ul id="machine_module_tab" class="nav nav-tabs">
                        <?php
                            $counter = 0;
                            foreach ($modules as $key => $value) :
                                if (is_null($id_module) OR ($key == $id_module)) :
                                $counter++;
                        ?>
                        <li class="<?php echo $counter == 1 ? 'active' : ''; ?>"><a href="#tab_<?php echo $key; ?>" data-toggle="tab"><?php echo $value; ?></a></li>
                        <?php
                        endif;
                        endforeach; ?>
                    </ul>

                    <div class="tab-content" style="border: 1px solid #DDD;padding: 10px;border-top: none;">
                        <!-- tab module -->
                        <?php
                            $counter = 0;
                            foreach ($modules as $module_key => $module_value) :
                                if (is_null($id_module) OR ($module_key == $id_module)) :
                                $counter++;
                        ?>

                        <div class="tab-pane fade in <?php echo $counter == 1 ? 'active' : ''; ?>" id="tab_<?php echo $module_key; ?>">
                            <div class="row">
                                <div class="col-md-12" style="padding-bottom: 10px;">
                                    <div class="checkbox"><label><input class="chk_module" type="checkbox" name="chk_module[]" value="<?php echo $module_key; ?>" <?php echo set_checkbox('chk_module', $module_key, (isset($machine_activities[$module_key]) ? true : false) ); ?>> <?php echo lang('mc_cbo_used_for_module'); ?><span class="star">&nbsp;*</span></label></div>
                                </div>
                            </div>
                            <div class="row">
                                <!-- activities -->
                                <div class="col-md-6">
                                    <strong>
                                        <?php echo lang('crumb_activity'); ?><span class="star">&nbsp;*</span>
                                        <span class="pull-right" style="font-size: small;font-style: italic;font-weight: normal;"><?php echo lang('lbl_activity_default'); ?></span>
                                    </strong>
                                    <div style="height: 150px; overflow: auto; border: 1px solid #D6D3D3; padding: 0px 5px 9px 5px; margin-top: 5px;">
                                        <?php if (isset($activities) AND is_array($activities) AND !empty($activities)) : ?>
                                        <?php foreach ($activities as $activity_key => $activity_value) : ?>
                                        <div class="checkbox">
                                            <label><input class="chk_activity" type="checkbox" name="chk_activity[]" value="<?php echo "{$module_key}_{$activity_value->id}"; ?>" <?php echo set_checkbox('chk_activity', "{$module_key}_{$activity_value->id}", ((isset($machine_activities[$module_key]['activiti']) AND in_array($activity_value->id, $machine_activities[$module_key]['activiti'])) ? true : false) ); ?>> <?php echo $activity_value->name; ?></label>
                                            <input type="radio" class="rdo_activity_default pull-right" name="rdo_activity_default_<?php echo $module_key; ?>[]" value="<?php echo "{$module_key}_{$activity_value->id}"; ?>" class="pull-right" <?php echo set_radio('rdo_activity_default_'.$module_key, "{$module_key}_{$activity_value->id}", ((isset($machine_activities[$module_key]['activiti_default']) AND ($activity_value->id == $machine_activities[$module_key]['activiti_default']) ) ? true : false)); ?>>
                                        </div>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                    <?php if (isset($_POST['chk_module']) AND in_array($module_key, $_POST['chk_module'])) : ?>
                                    <div class="help-block ps_err">
                                        <?php echo form_error('chk_activity'); ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <!--// activities -->

                                <!-- addons -->
                                <div class="col-md-6">
                                    <strong><?php echo lang('crumb_addon'); ?></strong>
                                    <div style="height: 150px; overflow: auto; border: 1px solid #D6D3D3; padding: 0px 5px 9px 5px; margin-top: 5px;">
                                        <?php if (isset($addon[$module_key]) AND is_array($addon[$module_key]) AND !empty($addon[$module_key])) : ?>
                                        <?php foreach ($addon[$module_key] as $addon_key => $addon_value) : ?>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="chk_addon" name="chk_addon[]" value="<?php echo "{$module_key}_{$addon_value->id}"; ?>" <?php echo set_checkbox('chk_addon', "{$module_key}_{$addon_value->id}", ( isset($machine_addon[$module_key]) AND is_array($machine_addon[$module_key]) AND in_array($addon_value->id, $machine_addon[$module_key]) ) ? true : false); ?>> <?php echo $addon_value->name; ?>
                                            </label>
                                        </div>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <!--// addons -->
                            </div>
                        </div>
                        <?php
                        endif;
                            endforeach; ?>
                        <!--// tab module -->
                    </div>
                <?php endif; ?>

            </div>
        </div>
        <!--// Module -->

        <div class="form-group">
            <label class="col-md-3 control-label" for="btn_save"></label>
            <div class="col-md-6">
                <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a class="btn btn-default" href="javascript:close_modal('#modal_machine_detail');"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>
    </form>
</div>