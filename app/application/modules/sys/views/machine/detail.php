<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("sys/machine/edit/$id");
    } else {
        $form_link = create_url("sys/machine/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_machine_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_machine');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>

    function activity_refresh() {
        $.ajax({
           type: 'GET',
           url: '<?php echo create_url('sys/machine/list_activity');?>',
           data: {id_activity: $("#id_activity").val()},
           datatype: 'html',
           cache:false,
           success: function(data){
                $('#select_activity').html(data);
                $('#select_activity .select2').select2();
           }
        });
    }

    function change_activity(){
            var _hdid = $('#hd_select').val();
            var _hdarray = _hdid.split(",");
            var _check = '';
            $('#id_activity :selected').each(function(i, selected){
                var itemValue=$(selected).val();
                _check = _check + ',' + itemValue;
            });
            var _checkarray = _check.split(",");

            for(var j = 0; j < _checkarray.length; j++){
                var _check_item = _checkarray[j];
                if(_hdid.lastIndexOf(_check_item) < 0){
                    if(_hdid.length > 0)
                        _hdid = _hdid + ',' + _check_item;
                    else
                        _hdid = _check_item;
                }
            }

            for(var jj = 0; jj < _hdarray.length; jj++){
                var _hd_item = _hdarray[jj];
                if(_check.lastIndexOf(_hd_item) < 0){
                    if(_hdid.lastIndexOf(',' + _hd_item) != -1)
                        _hdid = _hdid.replace(',' + _hd_item,"");
                    else{
                        if(_hdid.lastIndexOf(_hd_item + ',') != -1)
                            _hdid = _hdid.replace(_hd_item + ',',"");
                        else
                            _hdid = _hdid.replace(_hd_item,"");
                    }
                }
            }
            $('#hd_select').val(_hdid);
            var _activity_default = '<?php echo $id_default; ?>';
            $('#id_default').empty().append('<option value=""><?php echo lang('lbl_select_default');?></option>');
            $('#id_activity :selected').each(function(i, selected){
                var item=$(selected).text();
                var itemValue=$(selected).val();
                $("#id_default").append('<option value="'+ itemValue +'">'+ item +'</option> ');
            });
            if(parseInt(_activity_default) > 0)
                $('#id_default').select2().select2('val',_activity_default);
            else
                $('#id_default').select2().select2('val','');
        }

    $(function(){
        $('.select2').select2({ width: 'resolve' });
        var _activity_default = '<?php echo $id_default; ?>';
        $('#id_default').empty().append('<option value=""><?php echo lang('lbl_select_default');?></option>');
        $('#id_activity :selected').each(function(i, selected){
            var item=$(selected).text();
            var itemValue=$(selected).val();
            $("#id_default").append('<option value="'+ itemValue +'" >'+ item +'</option> ');
        });
        if(parseInt(_activity_default) > 0) {
            $('#id_default').select2().select2('val',_activity_default);
        } else {
            $('#id_default').select2().select2('val','');
        }

        $('#frm_machine_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_machine_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });
    });
</script>

<div id="frm_machine_detail_wraper">
    <form class="form-horizontal" id="frm_machine_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-4 control-label" for="identifier"><?php echo lang('lbl_identifier');?></label>
            <div class="col-md-5">
                <input id="identifier" name="identifier" value="<?php echo set_value('identifier', isset($machine->machine->identifier) ? $machine->machine->identifier : ''); ?>" type="text" placeholder="<?php echo lang('holder_identifer');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('identifier'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="name"><?php echo lang('mc_lbl_machine');?></label>
            <div class="col-md-5">
                <input id="name" name="name" value="<?php echo set_value('name', isset($machine->machine->name) ? $machine->machine->name : ''); ?>" type="text" placeholder="<?php echo lang('mc_holder_name');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('name'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="nfc-code"><?php echo lang('mc_lbl_nfc_code');?></label>
            <div class="col-md-4">
                <select id="nfc_code" name="nfc_code" class="select2" style="width: 250px;">
                    <option><?php echo lang('mc_lbl_select_nfc_code');?></option>
                    <?php if (isset($nfc_code) AND !empty($nfc_code)) : ?>
                    <?php foreach ($nfc_code as $key => $value) : ?>
                    <option value="<?php echo $value->nfc_code; ?>" <?php echo set_select('nfc_code', $value->nfc_code, (isset($machine->machine->nfc_code) AND ($machine->machine->nfc_code == $value->nfc_code) )); ?>><?php echo $value->code; ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <div class="help-block ps_err">
                    <?php echo form_error('nfc_code'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="id_addon"><?php echo lang('mc_lbl_addon');?></label>
            <div class="col-md-4">
                <select id="id_addon" name="id_addon[]" class="select2" style="width: 250px;" multiple="multiple" data-placeholder="<?php echo lang('mc_lbl_select_addon')?>" >
                    <?php if (isset($addon) AND !empty($addon)) : ?>
                    <?php foreach ($addon as $key => $value) : ?>
                    <option value="<?php echo $value->id; ?>" <?php  echo (isset($machine_addon) AND (!empty($machine_addon)) AND ((in_array($value->id, $machine_addon))))?'selected="selected"':'';?>><?php echo "{$value->identifier} {$value->name}"; ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <div class="help-block ps_err">
                    <?php echo form_error('id_addon'); ?>
                </div>
            </div>
        </div>

        <input type="hidden" id="hd_select" name="hd_select" value="<?php echo isset($activity_select) ? $activity_select : '';?>"/>
        <div class="form-group">
            <label class="col-md-4 control-label" for="activity"><?php echo lang('lbl_activity');?></label>
            <div class="col-md-5" id="select_activity">
                <select id="id_activity" name="id_activity[]" class="select2" multiple="multiple" data-placeholder="<?php echo lang('lbl_select_activity')?>" style="width: 250px;" onchange="change_activity()">
                    <?php if (isset($activity) AND !empty($activity)) : ?>
                    <?php foreach ($activity as $key => $value) : ?>
                    <option value="<?php echo $value->id; ?>" data="<?php echo $value->name; ?>" <?php  echo (isset($activities) AND (!empty($activities)) AND ((in_array($value->id, $activities))))?'selected="selected"':'';?>><?php echo $value->name; ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <div class="help-block ps_err">
                    <?php echo form_error('id_activity'); ?>
                </div>
            </div>
            <div class="col-md-2" style="padding: 0px;">
                 <a href="<?php echo create_url('sys/activity/add') . '?callback=activity_refresh'; ?>" class="sys_modal_detail btn btn-xs btn-info" _title="<?php echo lang('activity_lbl_detail'); ?>" _modal='#modal_activity_detail' target="_blank"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new')?></a>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="id_default"><?php echo lang('lbl_activity_default');?></label>
            <div class="col-md-4">
                <select id="id_default" name="id_default" class="select2" style="width: 250px;"></select>
                <div class="help-block ps_err">
                    <?php echo form_error('id_default'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
              <label class="col-md-4 control-label" for="description"><?php echo lang('mc_lbl_description');?></label>
              <div class="col-md-5">
                    <textarea class="form-control input-md" id="description" name="description" style="width: 250px; height: auto;"  ><?php echo set_value('description', isset($machine->machine->description) ? $machine->machine->description : ''); ?></textarea>
                    <div class="help-block ps_err">
                        <?php echo form_error('description'); ?>
                    </div>
              </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="btn_save"></label>
            <div class="col-md-8">
                <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a class="btn btn-default" href="javascript:close_modal('#modal_machine_detail');"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>

    </form>
</div>