<script type="text/javascript">
    var current_url    = '<?php echo current_url(); ?>';
    var current_state  = <?php echo (intval($this->input->get('module')) > 0) ? 1 : 0; ?>;
    var module         = '?module=<?php echo intval($this->input->get('module')); ?>';
    var module_not     = '?module_not=<?php echo intval($this->input->get('module')); ?>';
    var grid_url       = current_url + module;
    $(function(){
        init_modal('#modal_machine_detail');
        init_modal('#modal_machine_delete');
        init_modal('#modal_activity_detail');

        var column_properties = $.parseJSON('<?php echo $column_properties; ?>');
        var grid_config = {
            'target': '#grid_machine',
            'url'   : grid_url,
            'limit' : <?php echo isset($limit) ? intval($limit) : 0; ?>,
            'columns': [
                { field: "order", title: '<?php echo lang('grid_number');?>', filterable: false, sortable: false, width: column_properties.order_width, template: function(order){return grid_number = grid_number + 1;} },
                { field: "identifier", title: '<?php echo lang('lbl_identifier')?>', width: column_properties.identifier_width, },
                { field: "name", title: '<?php echo lang('mc_lbl_machine_name')?>', },
                { field: "nfc_code", title: '<?php echo lang('mc_lbl_nfc_code')?>', width: column_properties.code_width, },
                <?php if (0) : ?>
                { field: "description", title: '<?php echo lang('mc_lbl_description')?>' },
                <?php endif; ?>
                { field: "created_at", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", title: '<?php echo lang('grid_created_at')?>', width: column_properties.created_width },
                { field: "updated_at", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", title: '<?php echo lang('grid_updated_at')?>', width: column_properties.updated_width },
                { field: "option", title: '<?php echo lang('grid_option'); ?>', filterable: false, sortable: false, width: column_properties.option_width, attributes:{style: "text-align:center"}, template: function(data) {
                        if(current_state > 0){
                            html = "<a _modal='#modal_machine_detail' _title='<?php echo lang('mc_lbl_machine_detail'); ?>' href='<?php echo create_url('sys/machine/edit'); ?>/"+data.id+"/<?php echo $id_module; ?>' class='sys_modal_detail btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span>&nbsp;<?php echo lang('btn_edit')?></a>&nbsp;";
                            html = html + "<a _id_grid = '#grid_machine' _modal='#modal_machine_delete'  _title='<?php echo lang('delete_confirm'); ?>' href='<?php echo create_url('sys/machine/delete'); ?>/"+data.id+"' class='sys_modal_delete btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span>&nbsp;<?php echo lang('btn_delete')?></a>";
                            return html;
                        } else {
                            html = "<a _modal='#modal_machine_detail' _title='<?php echo lang('mc_lbl_assign_more'); ?>' href='<?php echo create_url('sys/machine/edit'); ?>/"+data.id+"/<?php echo intval($this->input->get('module')); ?>' class='sys_modal_detail btn btn-success btn-xs'><span class='glyphicon glyphicon-ok'></span>&nbsp;<?php echo lang('mc_lbl_assign_more'); ?></a>&nbsp;";
                            return html;
                        }
                    }
                }
            ]
        };
        create_grid(grid_config);

        $('body').on('click', '#assign_machine_detail', function(e){
            e.preventDefault();
            i = $(this);
            grid_url = current_url;
            if (current_state == 0) {
                i.html('<span class="glyphicon glyphicon-ok"></span>&nbsp;<?php echo lang('mc_lbl_assign_more'); ?>');
                i.removeClass('btn-warning');
                i.addClass('btn-success');
                grid_url = grid_url + module;
            } else {
                i.html('<span class="glyphicon glyphicon-random"></span>&nbsp;<?php echo lang('mc_lbl_show_machne_assigned'); ?>');
                i.removeClass('btn-success');
                i.addClass('btn-warning');
                grid_url = grid_url + module_not;
            }
            var grid = $("#grid_machine").data("kendoGrid");
            grid.dataSource.transport.options.read.url = grid_url;
            refresh_grid('#grid_machine');
            current_state = (current_state == 1) ? 0 : 1;
        });
    });
</script>

<div id="grid_machine"></div>
<script id="toolbar_template" type="text/x-kendo-template">
    <div>
        <span class="pull-left">
            <h4>
                &nbsp;<span class="glyphicon glyphicon-user"></span>
                &nbsp;<strong><?php echo lang('mc_lbl_list_machine');?></strong>
            </h4>
        </span>
        <span class="pull-right">
            <a id="assign_machine_detail" loading="0" data-toggle="tooltip" data-placement="bottom" _title='<?php echo lang('mc_lbl_machine_detail'); ?>' href="javascript: void(0);" class="sys_tooltip btn btn-primary btn-success"><i class="glyphicon glyphicon-ok"></i>&nbsp;<?php echo lang('mc_lbl_assign_more'); ?></a>
            <a _modal="\\#modal_machine_detail" _title='<?php echo lang('mc_lbl_machine_detail'); ?>' href="<?php echo create_url('sys/machine/add'); ?>" class="sys_modal_detail btn btn-primary btn-default"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new');?></a>
        </span>
    </div>
</script>
<script id="delete-confirmation" type="text/x-kendo-template">
    <div class="row">
        <span class="col-md-2"></span>
        <span class="col-md-8">
            <p class="delete-message"><?php echo lang('mc_msg_question_delete_machine'); ?></p>
        </span>
        <span class="col-md-2"></span>
    </div>
    <div class="row">
        <span class="col-md-7"></span>
        <span class="col-md-5">
            <span class="pull-right">
                <button class="delete-confirm btn btn-danger"><?php echo lang('btn_yes');?></button>
                <a href="#" class="delete-cancel btn btn-default"><?php echo lang('btn_no');?></a>
            </span>
        </span>
    </div>
</script>