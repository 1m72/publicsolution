<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("sys/machine/edit/$id");
    } else {
        $form_link = create_url("sys/machine/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_machine_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_machine');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>

    function activity_refresh() {
        $.ajax({
           type: 'GET',
           url: '<?php echo create_url('sys/machine/list_activity');?>',
           data: {id_activity: $("#id_activity").val()},
           datatype: 'html',
           cache:false,
           success: function(data){
                $('#select_activity').html(data);
                $('#select_activity .select2').select2();
           }
        });
    }

    function change_activity(){
        var _hdid = $('#hd_select').val();
        var _hdarray = _hdid.split(",");
        var _check = '';
        $('#id_activity :selected').each(function(i, selected){
            var itemValue=$(selected).val();
            _check = _check + ',' + itemValue;
        });
        var _checkarray = _check.split(",");

        for(var j = 0; j < _checkarray.length; j++){
            var _check_item = _checkarray[j];
            if(_hdid.lastIndexOf(_check_item) < 0){
                if(_hdid.length > 0)
                    _hdid = _hdid + ',' + _check_item;
                else
                    _hdid = _check_item;
            }
        }

        for(var jj = 0; jj < _hdarray.length; jj++){
            var _hd_item = _hdarray[jj];
            if(_check.lastIndexOf(_hd_item) < 0){
                if(_hdid.lastIndexOf(',' + _hd_item) != -1)
                    _hdid = _hdid.replace(',' + _hd_item,"");
                else{
                    if(_hdid.lastIndexOf(_hd_item + ',') != -1)
                        _hdid = _hdid.replace(_hd_item + ',',"");
                    else
                        _hdid = _hdid.replace(_hd_item,"");
                }
            }
        }
        $('#hd_select').val(_hdid);
        var _activity_default = '<?php echo $id_default; ?>';
        $('#id_default').empty().append('<option value=""><?php echo lang('lbl_select_default');?></option>');
        $('#id_activity :selected').each(function(i, selected){
            var item=$(selected).text();
            var itemValue=$(selected).val();
            $("#id_default").append('<option value="'+ itemValue +'">'+ item +'</option> ');
        });
        if(parseInt(_activity_default) > 0)
            $('#id_default').select2().select2('val',_activity_default);
        else
            $('#id_default').select2().select2('val','');
    }

    $(function(){
        $('.select2').select2({ width: 'resolve' });
        var _activity_default = '<?php echo $id_default; ?>';
        $('#id_default').empty().append('<option value=""><?php echo lang('lbl_select_default');?></option>');
        $('#id_activity :selected').each(function(i, selected){
            var item=$(selected).text();
            var itemValue=$(selected).val();
            $("#id_default").append('<option value="'+ itemValue +'" >'+ item +'</option> ');
        });
        if(parseInt(_activity_default) > 0) {
            $('#id_default').select2().select2('val',_activity_default);
        } else {
            $('#id_default').select2().select2('val','');
        }

        $('#frm_machine_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $('#module_selected').val(json_encode(module_selected));
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_machine_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });
    });

    try {
        var src, des, my, treeview;
        <?php
            $tmp = array();
            if (isset($activity) AND is_array($activity) AND !empty($activity)) {
                foreach ($activity as $key => $value) {
                    $tmp[$value->id] = array(
                        'key'  => $value->id,
                        'text' => $value->name
                    );
                }
            }
        ?>
        var activities = $.parseJSON('<?php echo json_encode($tmp); ?>');

        var modules = {};
        <?php if (isset($modules) AND !empty($modules)) : ?>
        modules = $.parseJSON('<?php echo json_encode($modules); ?>');
        <?php endif; ?>

        var module_selected = {};
        <?php if (isset($module_selected) AND !empty($module_selected)) : ?>
        module_selected = json_decode('<?php echo json_encode($module_selected); ?>', true);
        <?php endif; ?>

        var activity_default = {};
        <?php if (isset($activity_default) AND !empty($activity_default)) : ?>
        activity_default = json_decode('<?php echo json_encode($activity_default); ?>', true);
        <?php endif; ?>

        $(function(){
            $('#module').change(function(e) {
                treeview = $('#panel-left').data("kendoTreeView");

                added = e.added;
                if ( (added != undefined) && (modules[added.id] != undefined) ) {
                    module_selected[added.id] = new Array();
                    result = treeview.append([
                        {text: added.text}
                    ]);
                    result.attr('_index_module', added.id);
                }

                removed = e.removed;
                if ( (removed != undefined) && (modules[removed.id] != undefined) ) {
                    module_selected[removed.id] = new Array();
                    uid = $('li[_index_module="'+removed.id+'"]').attr('data-uid');
                    node = treeview.findByUid(uid);
                    treeview.remove(node);

                    activity_default[removed.id] = undefined;
                    $('#activity_default').val(json_encode(activity_default));
                }
            });

            $("#panel-left").kendoTreeView();
            $("#panel-right").kendoTreeView({
                dragAndDrop: true,
                dataSource: [],
                drop: function(e){
                    e.preventDefault();
                    src = e.sourceNode;
                    des = e.destinationNode;
                    my = des;
                    treeview = $('#panel-left').data('kendoTreeView');
                    checker = false;
                    var target = treeview.findByUid($(des).attr('data-uid'));
                    if (e.dropPosition == 'over') {
                        des_item = $(des);
                        if (des_item.parent('ul').hasClass('k-treeview-lines')) {
                            checker = true;
                        }
                    }

                    id_module = $(des).attr('_index_module');
                    id_activity = $(src).attr('_index_activity');
                    console.log(id_module);
                    exist = $(des).find('li[_index_activity="'+id_activity+'"]').length;
                    if (checker && (exist == 0)) {
                        result = treeview.append({text: $(src).text()}, target);
                        $(result.selector).attr('_index_activity', id_activity);
                        $(result.selector).attr('_index_module', id_module);
                        result.find('div:first').prepend('<input class="set_default_activity" type="radio" value="'+id_activity+'" name="activity_default_'+id_module+'" _index_module="'+id_module+'" />');
                        result.find('div:first').append('<a class="remove_node_activity glyphicon glyphicon-remove" href="javascript:;" _index_module="'+id_module+'" _index_activity="'+id_activity+'"></a>');
                        if (module_selected[id_module] == undefined) {
                            module_selected[id_module] = new Array();
                        }
                        module_selected[id_module].push(id_activity);
                    } else {
                        e.setValid(false);
                        e.preventDefault();
                    }
                }
            });

            treeview_right = $('#panel-right').data('kendoTreeView');
            for (activity_key in activities) {
                result = treeview_right.append({text: activities[activity_key].text});
                result.attr('_index_activity', activities[activity_key].key);
            }

            treeview_left = $('#panel-left').data('kendoTreeView');
            // modules
            for (module_key in module_selected) {
                if (modules[module_key] != undefined) {
                    result = treeview_left.append({text: modules[module_key]});
                    result.attr('_index_module', module_key);
                }
            }

            // modules activities
            for (module_key in module_selected) {
                if (modules[module_key] != undefined) {
                    uid = $('li[_index_module="'+module_key+'"]').attr('data-uid');
                    target = treeview_left.findByUid(uid);
                    for (activity_key in module_selected[module_key]) {
                        item = module_selected[module_key][activity_key];
                        result = treeview_left.append({text: activities[item].text}, target);
                        result.attr('_index_activity', item);
                        result.attr('_index_module', module_key);
                        checked = '';
                        if ((activity_default[module_key] != undefined) && (activity_default[module_key] == item) ) {
                            checked = 'checked="checked"';
                        }
                        result.find('div:first').prepend('<input class="set_default_activity" type="radio" value="'+item+'" name="activity_default_'+module_key+'" _index_module="'+module_key+'" ' + checked + ' />');
                        result.find('div:first').append('<a class="remove_node_activity glyphicon glyphicon-remove" href="javascript:;" _index_module="'+module_key+'" _index_activity="'+item+'"></a>');
                    }
                }
            }

            $('#panel-left').on('click', '.remove_node_activity', function(e){
                e.preventDefault();
                i = $(this);
                if (confirm('Are you sure want to remove this activity ?')) {
                    id_module   = i.attr('_index_module');
                    id_activity = i.attr('_index_activity');
                    uid         = $('li[_index_activity="'+id_activity+'"][_index_module="'+id_module+'"]').attr('data-uid');
                    console.log('uid: ' + uid);
                    node        = treeview_left.findByUid(uid);

                    for (var i = 0; i < module_selected[id_module].length; i++) {
                        if (module_selected[id_module][i] == id_activity) {
                            module_selected[id_module].splice(i, 1);
                        }
                    };

                    activity_default_item = $('[name="activity_default_'+id_module+'"]');
                    activity_default_selected = $('[name="activity_default_'+id_module+'"]:checked');
                    console.log(activity_default_selected);

                    if ( (activity_default_selected == undefined) || ( (activity_default_item.length > 0) && (parseInt(activity_default_selected.val()) == parseInt(id_activity)) ) ) {
                        activity_default_item.first().attr('checked', 'checked');
                    }

                    if ( (parseInt(activity_default_selected.val()) == parseInt(id_activity)) && (activity_default_item.length > 0) ) {
                    }

                    treeview_left.remove(node);
                }
            });

            $('#panel-left').on('change', '.set_default_activity', function(e){
                i = $(this);
                set_activity_default(i.attr('_index_module'), i.val());
            });
        });

        function set_activity_default(id_module, id_activity) {
            activity_default[id_module] = id_activity.toString();
            $('#activity_default').val(json_encode(activity_default));
        }
    } catch (err) {
        console.log(err);
    }
</script>
<script type="text/javascript">

</script>

<div id="frm_machine_detail_wraper">
    <form class="form-horizontal" id="frm_machine_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-3 control-label" for="identifier"><?php echo lang('lbl_identifier');?></label>
            <div class="col-md-6">
                <input id="identifier" name="identifier" value="<?php echo set_value('identifier', isset($machine->machine->identifier) ? $machine->machine->identifier : ''); ?>" type="text" placeholder="<?php echo lang('holder_identifer');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('identifier'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="name"><?php echo lang('mc_lbl_machine');?></label>
            <div class="col-md-6">
                <input id="name" name="name" value="<?php echo set_value('name', isset($machine->machine->name) ? $machine->machine->name : ''); ?>" type="text" placeholder="<?php echo lang('mc_holder_name');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('name'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="activity"><?php echo lang('lbl_module');?></label>
            <div class="col-md-9">
                <select id="module" multiple="multiple" class="select2" style="width: 457px; border-bottom: none;">
                    <?php foreach ($modules as $key => $value) : ?>
                    <option value="<?php echo $key; ?>" <?php echo (isset($module_selected) AND (isset($module_selected[$key])) AND is_array($module_selected[$key]) ) ? 'selected="selected"' : ''; ?>><?php echo $value; ?></option>
                    <?php endforeach; ?>
                </select>
                <div style="margin-right: 10px; overflow: auto; border: 0px solid #aaa; padding-top: 7px;">
                    <div id="panel-left" style="float: left; min-height: 10px; width: 200px; "></div>
                    <div id="panel-right" style="float: left; width: 250px; border-left: 1px solid #aaa; "></div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="nfc-code"><?php echo lang('mc_lbl_nfc_code');?></label>
            <div class="col-md-6">
                <select id="nfc_code" name="nfc_code" class="select2" style="width: 300px;">
                    <option><?php echo lang('mc_lbl_select_nfc_code');?></option>
                    <?php if (isset($nfc_code) AND !empty($nfc_code)) : ?>
                    <?php foreach ($nfc_code as $key => $value) : ?>
                    <option value="<?php echo $value->nfc_code; ?>" <?php echo set_select('nfc_code', $value->nfc_code, (isset($machine->machine->nfc_code) AND ($machine->machine->nfc_code == $value->nfc_code) )); ?>><?php echo $value->code; ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <div class="help-block ps_err">
                    <?php echo form_error('nfc_code'); ?>
                </div>
            </div>
        </div>

        <?php if (0) : ?>
        <div class="form-group">
            <label class="col-md-3 control-label" for="id_addon"><?php echo lang('mc_lbl_addon');?></label>
            <div class="col-md-6">
                <select id="id_addon" name="id_addon[]" class="select2" style="width: 300px;" multiple="multiple" data-placeholder="<?php echo lang('mc_lbl_select_addon')?>" >
                    <?php if (isset($addon) AND !empty($addon)) : ?>
                    <?php foreach ($addon as $key => $value) : ?>
                    <option value="<?php echo $value->id; ?>" <?php  echo (isset($machine_addon) AND (!empty($machine_addon)) AND ((in_array($value->id, $machine_addon))))?'selected="selected"':'';?>><?php echo "{$value->identifier} {$value->name}"; ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <div class="help-block ps_err">
                    <?php echo form_error('id_addon'); ?>
                </div>
            </div>
        </div>

        <input type="hidden" id="hd_select" name="hd_select" value="<?php echo isset($activity_select) ? $activity_select : '';?>"/>
        <div class="form-group">
            <label class="col-md-3 control-label" for="activity"><?php echo lang('lbl_activity');?></label>
            <div class="col-md-6" id="select_activity">
                <select id="id_activity" name="id_activity[]" class="select2" multiple="multiple" data-placeholder="<?php echo lang('lbl_select_activity')?>" style="width: 300px;" onchange="change_activity()">
                    <?php if (isset($activity) AND !empty($activity)) : ?>
                    <?php foreach ($activity as $key => $value) : ?>
                    <option value="<?php echo $value->id; ?>" data="<?php echo $value->name; ?>" <?php  echo (isset($activities) AND (!empty($activities)) AND ((in_array($value->id, $activities))))?'selected="selected"':'';?>><?php echo $value->name; ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <div class="help-block ps_err">
                    <?php echo form_error('id_activity'); ?>
                </div>
            </div>
            <div class="col-md-2" style="padding: 0px;">
                 <a href="<?php echo create_url('sys/activity/add') . '?callback=activity_refresh'; ?>" class="sys_modal_detail btn btn-xs btn-info" _title="<?php echo lang('activity_lbl_detail'); ?>" _modal='#modal_activity_detail' target="_blank"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new')?></a>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="id_default"><?php echo lang('lbl_activity_default');?></label>
            <div class="col-md-6">
                <select id="id_default" name="id_default" class="select2" style="width: 300px;"></select>
                <div class="help-block ps_err">
                    <?php echo form_error('id_default'); ?>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <div class="form-group">
              <label class="col-md-3 control-label" for="description"><?php echo lang('mc_lbl_description');?></label>
              <div class="col-md-6">
                    <textarea class="form-control input-md" id="description" name="description" style="width: 300px; height: auto;"  ><?php echo set_value('description', isset($machine->machine->description) ? $machine->machine->description : ''); ?></textarea>
                    <div class="help-block ps_err">
                        <?php echo form_error('description'); ?>
                    </div>
              </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="btn_save"></label>
            <div class="col-md-6">
                <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a class="btn btn-default" href="javascript:close_modal('#modal_machine_detail');"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>
        <input type="hidden" name="module_selected" id="module_selected" value="<?php echo isset($module_selected) ? htmlentities(json_encode($module_selected)) : ''; ?>" />
        <input type="hidden" name="activity_default" id="activity_default" value="<?php echo isset($activity_default) ? htmlentities(json_encode($activity_default)) : ''; ?>" />
    </form>
</div>