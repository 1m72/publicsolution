<?php // echo "<pre>"; var_dump($addon);?>
<?php
    $form_link = create_url("sys/machine/add_assign/" . $id_module);
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<style type="text/css">
.machine_activity_checkbox {}
.machine_activity_wrap {overflow: auto; padding-left: 25px;}
.machine_activity_block {min-height:150px; max-height: 150px; overflow: auto; border: 1px solid #D6D3D3; padding: 0px 5px 9px 5px; margin-top: 5px;}
.machine_addon_block {min-height:150px; max-height: 150px; overflow: auto; border: 1px solid #D6D3D3; padding: 0px 5px 9px 5px; margin-top: 5px;}
</style>
<script type="text/javascript">
    <?php if (isset($flag_assign) AND ($flag_assign) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_assign_machine');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_machine');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>
    $(function(){
        $('#frm_machine_assign').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_machine_assign_wraper').html(res);
                },
                error: function(err) {}
            });
        });
        $('.chk_machine').bootstrapSwitch('size', 'mini');
        $('.chk_machine').on('switchChange.bootstrapSwitch', function(event, state) {
            i = $(this);
            if (state) {
                $('#machine_activity_row_' + i.val()).removeClass('hide');
            } else {
                $('#machine_activity_row_' + i.val()).addClass('hide');
            }
        });

        $('#frm_machine_assign_edit').on('change', '.chk_activity', function(){
            var i = $(this);
            var id=$(this).attr('data-id');
            if (i.is(':checked')) {
                $('#rdo_activity_defaul_'+id).removeAttr('disabled');
            } else {
                y = $('#rdo_activity_defaul_'+id);
                if (y.is(':checked')) {
                    $('.rdo_activity_default:not([disabled])').attr('checked', 'checked');
                }
                y.attr('disabled', 'disabled');
            }
        });
    });
</script>
<div id="frm_machine_assign_wraper">
    <form id="frm_machine_assign" action="<?php echo create_url("sys/machine/add_assign").'/'. $id_module; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <?php if (isset($error)) : ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php endif; ?>

        <table class="table table-condensed table-hover2 table-responsive" style="margin-bottom: 0px;">
            <thead>
                <tr>
                    <th width="40" class="text-center">#</th>
                    <th>Fullname</th>
                    <th width="100" class="text-center">Code</th>
                    <th width="100" class="text-center">Option</th>
                </tr>
            </thead>
            <tbody>
                <?php if (isset($list_machine) AND !empty($list_machine)) : ?>
                <?php
                $counter = 0;
                foreach ($list_machine as $key => $value) :
                    $counter++;
                    $chk_state          = set_checkbox('chk_machine[]', $value->id);
                    var_dump($chk_state);

                    $value_id           = isset($value->id) ? $value->id : '';
                    $value_name         = isset($value->name) ? $value->name : '';
                    $value_machine_code = isset($value->machine_code) ? $value->machine_code : '';
                    $value_identifier   = isset($value->price) ? $value->price : '';
                ?>
                <tr>
                    <td><?php echo $counter; ?></td>
                    <td><?php echo $value_name; ?></td>
                    <td width="100" class="text-center"><?php echo $value_machine_code; ?></td>
                    <td class="text-center">
                        <input class="chk_machine" name="chk_machine[]" type="checkbox" value="<?php echo $value_id; ?>" id="chk_<?php echo $value_id; ?>" data-id="<?php echo $value_id; ?>" <?php echo $chk_state; ?>/>
                    </td>
                </tr>
                <tr class="machine_activity_row <?php echo !empty($chk_state) ? '' : 'hide'; ?>" id="machine_activity_row_<?php echo $value_id; ?>">
                    <td colspan="5">
                        <div class="machine_activity_wrap">
                            <div class="row-fluid">
                                <div class="col-md-6">
                                    <strong>
                                        <span>Activity </span>
                                        <span class="star">&nbsp;*</span>
                                        <span class="pull-right" style="font-size: small;font-style: italic;font-weight: normal;">Activity default</span>
                                    </strong>
                                    <div class="machine_activity_block">
                                        <?php if (isset($activities) AND !empty($activities)) : ?>
                                        <?php foreach ($activities as $activity_key => $activity_value) :
                                        $activity_value_id    = isset($activity_value->id) ? $activity_value->id : '';
                                        $activity_value_name = isset($activity_value->name) ? $activity_value->name : '';
                                        ?>
                                        <div class="checkbox">
                                            <label><input class="chk_activity" type="checkbox" name="chk_activity_<?php echo $value_id; ?>[]" data-id="<?php echo $activity_value_id; ?>" value="<?php echo $activity_value_id; ?>"> <?php echo $activity_value_name; ?></label>
                                            <input type="radio" class="rdo_activity_default pull-right" id="rdo_activity_default_<?php echo $activity_value_id; ?>" name="rdo_activity_default_<?php echo $value_id;?>" value="<?php echo $activity_value_id; ?>">
                                        </div>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="help-block ps_err">
                                        <?php echo form_error('chk_activity_'.$value_id.'[]'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="machine_content" id="machine_">
                                        <strong>
                                            <?php echo lang('crumb_addon'); ?><span class="star">&nbsp;*</span>
                                        </strong>
                                        <div style="min-height:150px; max-height: 150px; overflow: auto; border: 1px solid #D6D3D3; padding: 0px 5px 9px 5px; margin-top: 5px;">
                                            <?php if (isset($addon) AND is_array($addon) AND !empty($addon)) : ?>
                                            <?php foreach ($addon as $addon_key => $addon_value) : ?>
                                            <div class="checkbox">
                                                <label><input class="chk_addon" type="checkbox" name="chk_addon_<?php echo $value_id; ?>[]" data-id="<?php echo "$addon_value->id"; ?>" value="<?php echo "$addon_value->id"; ?>" <?php echo set_checkbox('chk_addon[]',$addon_value->id);?> <?php if(isset($data->addon) && in_array($addon_value->id, $data->addon)) echo 'checked="checked"';?>> <?php echo $addon_value->name; ?></label>
                                            </div>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                        <div class="help-block ps_err">
                                            <?php echo form_error('chk_addon'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="5" class="text-center">
                        <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                        <a class="btn btn-default" href="javascript:close_modal('#modal_assign_machine');"><?php echo lang('btn_cancel');?></a>
                    </td>
                </tr>
                <?php else: ?>
                <tr> <td colspan="5" class="text-center"><?php echo lang('msg_not_machine'); ?></td> </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </form>
</div>
