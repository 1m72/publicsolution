<html>
<head>
    <title>Information detail</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(ASSET_JS . 'projekktor/themes/maccaco/projekktor.style.css'); ?>">
    <!--$this->load_assets('projekktor/themes/maccaco/projekktor.style.css', ASSET_CSS, ASSET_JS);-->
    <script type="text/javascript" src="<?php echo base_url(ASSET_JS . 'jquery-1.10.2.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url(ASSET_JS . 'projekktor/projekktor-1.3.00.min.js'); ?>"></script>
    <!--$this->load_assets('projekktor/projekktor-1.3.00.min.js', ASSET_JS);-->
    <script type="text/javascript">
        $(function(){
            projekktor('video, audio', {
                playerFlashMP4: '<?php echo assets('js/projekktor/swf/Jarisplayer/jarisplayer.swf'); ?>',
                playerFlashMP3: '<?php echo assets('js/projekktor/swf/Jarisplayer/jarisplayer.swf'); ?>f'
            });
        });
    </script>
    <style type="text/css">
        .projekktor{
            width: 100%;
        }
    </style>
</head>
<body>
<div id="view">
<?php
if(isset($detail) AND !empty($detail)){
    $path = isset($detail['path']) ? $detail['path'] : FALSE;
    $type = isset($detail['type']) ? $detail['type'] : FALSE;
    switch ($type) {
        case '.3gp':
            echo '<audio controls class="projekktor" width="100%" height="100%" poster="poster.jpg">';
            echo '<source src="' . $path . '" type="audio/mp3" ></source>';
            echo '</audio>';
            break;

        case '.mp4':
            echo '<video class="projekktor" width="100%" height="100%" poster="poster.jpg">';
            echo '<source src="' . $path . '" ></source>';
            echo '</video>';
            break;

        case '.jpg':
            echo '<img width="100%" height="auto" style="border: 0px solid #ddd; border-radius: 0px; -webkit-transition: all .2s ease-in-out; transition: all .2s ease-in-out;" id="imgview" alt="" src="' . $path . '">';
            break;

        default:
            # code...
            break;
    }
}
?>
</div>
</body>
</html>