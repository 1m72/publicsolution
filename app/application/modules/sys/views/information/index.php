<?php
$path         = isset($data) ? $data : '';
$type         = isset($type) ? $type : '';
$lat          = isset($lat) ? $lat : '';
$lon          = isset($lon) ? $lon : '';
$lat_lon_html = '';
if ($type) {
    $height = "40px";
    $html   = '';
    switch ($type) {
        case INFO_VOICE:
            $html .= '<audio controls class=\"projekktor\" width=\"100%\" height=\"100%\" poster=\"poster.jpg\">';
            $html .= '<source src=\"' . $path . '\" type=\"audio/mp3\" ></source>';
            $html .= '</audio>';
            $height = "200px";
            break;

        case INFO_VIDEO:
            $height = "400px";
            $html .= '<video class=\"projekktor\" width=\"100%\" height=\"100%\" poster=\"poster.jpg\">';
            $html .= '<source src=\"' . $path . '\" ></source>';
            $html .= '</video>';
            break;

        case INFO_IMAGE:
            $height = "400px";
            $html .= '<img width=\"100%\" height=\"auto\" style=\"border: 0px solid #ddd; border-radius: 0px; -webkit-transition: all .2s ease-in-out; transition: all .2s ease-in-out;\" id=\"imgview\" alt=\"\" src=\"' . (isset($thumbs) ? $thumbs : '') . '\">';
            break;

        case INFO_MESSAGE:
            $height = "100px";
            $html .= '<div>' .$path. '</div>';
            break;

        default:
            # code...
            break;
    }

    $street = isset($street) ? $street : '';
    $time   = isset($time) ? $time : '';
    $lat    = isset($lat) ? $lat : '';
    $lon    = isset($lon) ? $lon : '';
    $html .= '<div style=\"margin-top: 10px;\">';
    $html .= ($type != INFO_MESSAGE) ? ('<div>- Original : <a target=\"_blank\" href=\"'. $path .'\">Klicken Sie hier zur Ansicht</a>' . '</div>') : '';
    $html .= '<div>- Straße         : ' . $street . '</div>';
    $html .= '<div>- Datum          : ' . $time . '</div>';
    $html .= '<div>- GPS-Koordinate : ' . $lat . ' - ' . $lon . '</div>';
    $html .= '<div>';
} else {
    $lat          = $this->input->get('lat');
    $lon          = $this->input->get('lon');
    $lat_lon_html = "<div style='line-height: 25px; margin-top: 7px;'>GPS: {$lat} - {$lon}</div>";
}
?>
<html>
    <head>
        <title><?php echo lang('lbl_public_solution'); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <style type="text/css">
            body {margin: 0px; padding: 0px;}
            .projekktor{width: 100%; }
            #popup_contentDiv {overflow: visible;}
        </style>
        <script type="text/javascript" src="<?php echo base_url(ASSET_JS . 'jquery-1.10.2.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url(ASSET_JS . 'openlayer/OpenLayers.js'); ?>"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(ASSET_JS . 'projekktor/themes/maccaco/projekktor.style.css'); ?>">
        <script type="text/javascript" src="<?php echo base_url(ASSET_JS . 'projekktor/projekktor-1.3.00.min.js'); ?>"></script>
        <script type="text/javascript">
            $(function(){
                projekktor('video, audio', {
                    playerFlashMP4: '<?php echo assets('js/projekktor/swf/Jarisplayer/jarisplayer.swf'); ?>',
                    playerFlashMP3: '<?php echo assets('js/projekktor/swf/Jarisplayer/jarisplayer.swf'); ?>f'
                });
            });
        </script>
        <script type="text/javascript">
        $(function(){
            //'49.664264', '10.542148'
            var _lat = '<?php echo isset($lat) ? $lat : ''; ?>';
            var _lon = '<?php echo isset($lon) ? $lon : ''; ?>';
            var map = new OpenLayers.Map("map");
            var layer = new OpenLayers.Layer.OSM( "Simple OSM Map");
            var vector = new OpenLayers.Layer.Vector('vector');
            map.addLayers([layer, vector]);
            var lonLat = new OpenLayers.LonLat(_lon,_lat);
            lonLat.transform(
                new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                map.getProjectionObject() // to Spherical Mercator Projection
            );
            var markers = new OpenLayers.Layer.Markers( "Markers" );
            map.addLayer(markers);
            marker = new OpenLayers.Marker(lonLat);
            markers.addMarker(marker);
            var zoom = 15;
            map.setCenter (lonLat, zoom);

            // var feature = evt.feature;
            <?php if ($type !== false) : ?>
            var popup = new OpenLayers.Popup.FramedCloud("popup",
                lonLat,
                null,
                "<div style='width: 300px; height2: <?php echo $height; ?>;'><?php echo $html; ?><?php echo $lat_lon_html; ?> </div>",
                null,
                true
            );
            map.addPopup(popup);
            <?php endif; ?>
        });
        </script>
    </head>
    <body>
        <div id="map"></div>
    </body>
</html>