<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("sys/addon/edit/$id");
    } else {
        $form_link = create_url("sys/addon/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_addon_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_addon');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>
    $(function(){
        $('#frm_addon_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_addon_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });
    });
</script>
<div id="frm_addon_detail_wraper">
    <form class="form-horizontal" id="frm_addon_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-4 control-label" for="identifier"><?php echo lang('lbl_identifier');?></label>
            <div class="col-md-5">
                <input id="identifier" name="identifier" value="<?php echo set_value('identifier', isset($data->identifier) ? $data->identifier : ''); ?>" type="text" placeholder="<?php echo lang('holder_identifer');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('identifier'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="name"><?php echo lang('addon_lbl');?></label>
            <div class="col-md-5">
                <input id="name" name="name" value="<?php echo set_value('name', isset($data->name) ? $data->name : ''); ?>" type="text" placeholder="<?php echo lang('addon_holder_name');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('name'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="btn_save"></label>
            <div class="col-md-8">
                <button id="btn_save" name="btn_save" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a id="btn_cancel" name="btn_cancel" href="javascript:close_modal('#modal_addon_detail');" class="btn btn-default"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>

    </form>
</div>