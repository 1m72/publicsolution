<script type="text/javascript">
    var current_url    = '<?php echo current_url(); ?>';
    var current_state  = <?php echo (intval($this->input->get('module')) > 0) ? 1 : 0; ?>;
    var module         = '?module=<?php echo intval($this->input->get('module')); ?>';
    var module_not     = '?module_not=<?php echo intval($this->input->get('module')); ?>';
    var grid_url       = current_url + module;
    $(function() {
        init_modal('#modal_addon_detail');
        init_modal('#modal_addon_delete');

        var column_properties = $.parseJSON('<?php echo $column_properties; ?>');
        var grid_config = {
            'target': '#grid_addon',
            'url': grid_url,
            'limit': <?php echo isset($limit) ? intval($limit) : 0; ?>,
            'columns': [
                { field: "order", title: '<?php echo lang('grid_number');?>', filterable: false, sortable: false, width: column_properties.order_w, template: function(order){return grid_number = grid_number + 1;} },
                { field: "identifier", title: '<?php echo lang('lbl_identifier')?>', width: column_properties.identifier_w, },
                { field: "name", title: '<?php echo lang('addon_lbl_name')?>'},
                { field: "created_at", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", title: '<?php echo lang('grid_created_at')?>', width: column_properties.created_w },
                { field: "updated_at", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", title: '<?php echo lang('grid_updated_at')?>', width: column_properties.updated_w },
                { field: "option", title: '<?php echo lang('grid_option')?>', filterable: false, sortable: false, width: column_properties.option_w, attributes:{style: "text-align:center"},template: function(data) {
                        html = "<a _modal='#modal_addon_detail' _title='<?php echo lang('addon_lbl_detail'); ?>' href='<?php echo create_url('sys/addon/edit'); ?>/"+data.id+"' class='sys_modal_detail btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span>&nbsp;<?php echo lang('btn_edit');?></a>&nbsp;";
                        html = html + "<a _id_grid = '#grid_addon' _modal='#modal_addon_delete' _title='<?php echo lang('delete_confirm'); ?>' href='<?php echo create_url('sys/addon/delete'); ?>/"+data.id+"' class='sys_modal_delete btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span>&nbsp;<?php echo lang('btn_delete');?></a>";
                        return html;
                    }
                }
            ]
        };
        create_grid(grid_config);

        $('body').on('click', '#assign_addon_detail', function(e){
            i = $(this);
            grid_url = current_url;
            if (current_state == 0) {
                i.html('<span class="glyphicon glyphicon-ok"></span>&nbsp;<?php echo lang('addon_lbl_assign_more'); ?>');
                i.removeClass('btn-warning');
                i.addClass('btn-success');
                grid_url = grid_url + module;
            } else {
                i.html('<span class="glyphicon glyphicon-random"></span>&nbsp;<?php echo lang('addon_lbl_show_machne_assigned'); ?>');
                i.removeClass('btn-success');
                i.addClass('btn-warning');
                grid_url = grid_url + module_not;
            }
            var grid = $("#grid_addon").data("kendoGrid");
            grid.dataSource.transport.options.read.url = grid_url;
            refresh_grid('#grid_addon');
            current_state = (current_state == 1) ? 0 : 1;
        });
    });
</script>
<div id="grid_addon"></div>
<script id="toolbar_template" type="text/x-kendo-template">
    <div>
        <span class="pull-left">
            <h4>
                &nbsp;<span class="glyphicon glyphicon-user"></span>
                &nbsp;<strong><?php echo lang('addon_lbl_list');?></strong>
            </h4>
        </span>
        <span class="pull-right">
            <a id="assign_addon_detail" loading="0" class="btn btn-primary btn-success"><i class="glyphicon glyphicon-ok"></i>&nbsp;<?php echo lang('addon_lbl_assign_more'); ?></a>
            <a _modal="\\#modal_addon_detail" _title='<?php echo lang('addon_lbl_detail'); ?>' href="<?php echo create_url('sys/addon/add'); ?>" class="sys_modal_detail btn btn-primary btn-default"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new');?></a>
        </span>
    </div>
</script>
<script id="delete-confirmation" type="text/x-kendo-template">
    <div class="row">
        <span class="col-md-2"></span>
        <span class="col-md-8">
            <p class="delete-message"><?php echo lang('addon_msg_question_delete'); ?></p>
        </span>
        <span class="col-md-2"></span>
    </div>
    <div class="row">
        <span class="col-md-7"></span>
        <span class="col-md-5">
            <span class="pull-right">
                <button class="delete-confirm btn btn-danger"><?php echo lang('btn_yes');?></button>
                <a href="#" class="delete-cancel btn btn-default"><?php echo lang('btn_no');?></a>
            </span>
        </span>
    </div>
</script>