<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("sys/addon/edit/$id");
    } else {
        $form_link = create_url("sys/addon/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_addon_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_addon');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>
    $(function(){
        $('#frm_addon_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $('#module_selected').val(json_encode(module_selected));
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_addon_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });
    });

    try {
        var src, des, my, treeview;
        $('.select2').select2();
        <?php
            $tmp = array();
            if (isset($activity) AND is_array($activity) AND !empty($activity)) {
                foreach ($activity as $key => $value) {
                    $tmp[$value->id] = array(
                        'key'  => $value->id,
                        'text' => $value->name
                    );
                }
            }
        ?>
        var activities = $.parseJSON('<?php echo json_encode($tmp); ?>');

        var modules = {};
        <?php if (isset($modules) AND !empty($modules)) : ?>
        modules = $.parseJSON('<?php echo json_encode($modules); ?>');
        <?php endif; ?>

        var module_selected = {};
        <?php if (isset($module_selected) AND !empty($module_selected)) : ?>
        module_selected = json_decode('<?php echo json_encode($module_selected); ?>', true);
        <?php endif; ?>

        $(function(){
            $('#module').change(function(e) {
                treeview = $('#panel-left').data("kendoTreeView");

                added = e.added;
                if ( (added != undefined) && (modules[added.id] != undefined) ) {
                    module_selected[added.id] = new Array();
                    result = treeview.append([
                        {text: added.text}
                    ]);
                    result.attr('_index_module', added.id);
                }

                removed = e.removed;
                if ( (removed != undefined) && (modules[removed.id] != undefined) ) {
                    module_selected[removed.id] = new Array();
                    uid = $('li[_index_module="'+removed.id+'"]').attr('data-uid');
                    node = treeview.findByUid(uid);
                    treeview.remove(node);
                }
            });

            $("#panel-left").kendoTreeView();
            $("#panel-right").kendoTreeView({
                dragAndDrop: true,
                dataSource: [],
                drop: function(e){
                    e.preventDefault();
                    src = e.sourceNode;
                    des = e.destinationNode;
                    my = des;
                    treeview = $('#panel-left').data('kendoTreeView');
                    checker = false;
                    var target = treeview.findByUid($(des).attr('data-uid'));
                    if (e.dropPosition == 'over') {
                        des_item = $(des);
                        if (des_item.parent('ul').hasClass('k-treeview-lines')) {
                            checker = true;
                        }
                    }

                    id_module = $(des).attr('_index_module');
                    id_activity = $(src).attr('_index_activity');
                    console.log(id_module);
                    exist = $(des).find('li[_index_activity="'+id_activity+'"]').length;
                    if (checker && (exist == 0)) {
                        result = treeview.append({text: $(src).text()}, target);
                        result.find('div:first').append('<a class="remove_node_activity glyphicon glyphicon-remove" href="javascript:;" _index_module="'+id_module+'" _index_activity="'+id_activity+'"></a>');
                        $(result.selector).attr('_index_activity', id_activity);
                        if (module_selected[id_module] == undefined) {
                            module_selected[id_module] = new Array();
                        }
                        module_selected[id_module].push(id_activity);
                    } else {
                        e.setValid(false);
                        e.preventDefault();
                    }
                }
            });

            treeview_right = $('#panel-right').data('kendoTreeView');
            for (activity_key in activities) {
                result = treeview_right.append({text: activities[activity_key].text});
                result.attr('_index_activity', activities[activity_key].key);
            }

            treeview_left = $('#panel-left').data('kendoTreeView');
            // modules
            for (module_key in module_selected) {
                if (modules[module_key] != undefined) {
                    result = treeview_left.append({text: modules[module_key]});
                    result.attr('_index_module', module_key);
                }
            }

            // modules activities
            for (module_key in module_selected) {
                if (modules[module_key] != undefined) {
                    uid = $('li[_index_module="'+module_key+'"]').attr('data-uid');
                    target = treeview_left.findByUid(uid);
                    for (activity_key in module_selected[module_key]) {
                        item = module_selected[module_key][activity_key];
                        result = treeview_left.append({text: activities[item].text}, target);
                        result.attr('_index_activity', item);
                        result.find('div:first').append('<a class="remove_node_activity glyphicon glyphicon-remove" href="javascript:;" _index_module="'+module_key+'" _index_activity="'+item+'"></a>');
                    }
                }
            }

            $('#panel-left').on('click', '.remove_node_activity', function(e){
                e.preventDefault();
                i = $(this);
                if (confirm('Are you sure want to remove this activity ?')) {
                    id_module   = i.attr('_index_module');
                    id_activity = i.attr('_index_activity');
                    uid         = $('li[_index_activity="'+id_activity+'"]').attr('data-uid');
                    node        = treeview_left.findByUid(uid);
                    treeview_left.remove(node);

                    for (var i = 0; i < module_selected[id_module].length; i++) {
                        if (module_selected[id_module][i] == id_activity) {
                            module_selected[id_module].splice(i, 1);
                        }
                    };
                }
            });
        });
    } catch (err) {
        console.log(err);
    }
</script>
<div id="frm_addon_detail_wraper">
    <form class="form-horizontal" id="frm_addon_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-3 control-label" for="identifier"><?php echo lang('lbl_identifier');?></label>
            <div class="col-md-6">
                <input id="identifier" name="identifier" value="<?php echo set_value('identifier', isset($data->identifier) ? $data->identifier : ''); ?>" type="text" placeholder="<?php echo lang('holder_identifer');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('identifier'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="name"><?php echo lang('addon_lbl');?></label>
            <div class="col-md-6">
                <input id="name" name="name" value="<?php echo set_value('name', isset($data->name) ? $data->name : ''); ?>" type="text" placeholder="<?php echo lang('addon_holder_name');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('name'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-1 control-label">&nbsp;</label>
            <div class="col-md-10" style="padding: 0px;">
                <?php if (isset($modules) AND is_array($modules) AND !empty($modules)) : ?>
                    <ul id="myTab" class="nav nav-tabs">
                        <?php
                            //var_dump($modules);
                            $counter = 0;
                            foreach ($modules as $key => $value) :
                                if (!isset($id_module) OR ($key == $id_module)) :
                                $counter++;
                        ?>
                        <li class="<?php echo $counter == 1 ? 'active' : ''; ?>"><a href="#tab_<?php echo $key; ?>" data-toggle="tab"><?php echo $value; ?></a></li>
                        <?php
                        endif;
                        endforeach; ?>
                    </ul>

                    <div class="tab-content" style="border: 1px solid #DDD;padding: 10px;border-top: none;">
                        <!-- tab module -->
                        <?php
                            $counter = 0;
                            foreach ($modules as $module_key => $module_value) :
                                if (!isset($id_module) OR ($module_key == $id_module)) :
                                $counter++;
                        ?>
                        <div class="tab-pane fade in <?php echo $counter == 1 ? 'active' : ''; ?>" id="tab_<?php echo $module_key; ?>">
                            <div class="row">
                                <div class="col-md-12" style="padding-bottom: 10px;">
                                    <div class="checkbox"><label><input class="chk_module" type="checkbox" name="chk_module[]" value="<?php echo $module_key; ?>" <?php echo set_checkbox('chk_module', $module_key, (isset($machine_activities[$module_key]) ? true : false) ); ?>> Machine used for this module</label></div>
                                </div>
                            </div>
                            <div class="row">
                                <!-- activities -->
                                <div class="col-md-6">
                                    <strong>
                                        <?php echo lang('crumb_activity'); ?>
                                        <span class="pull-right" style="font-size: small;font-style: italic;font-weight: normal;"><?php echo lang('lbl_activity_default'); ?></span>
                                    </strong>
                                    <div style="height: 150px; overflow: auto; border: 1px solid #D6D3D3; padding: 0px 5px 9px 5px; margin-top: 5px;">
                                        <?php if (isset($activities) AND is_array($activities) AND !empty($activities)) : ?>
                                        <?php foreach ($activities as $activity_key => $activity_value) : ?>
                                        <div class="checkbox">
                                            <label><input class="chk_activity" type="checkbox" name="chk_activity[]" value="<?php echo "{$module_key}_{$activity_value->id}"; ?>" <?php echo set_checkbox('chk_activity', "{$module_key}_{$activity_value->id}", ((isset($machine_activities[$module_key]['activiti']) AND in_array($activity_value->id, $machine_activities[$module_key]['activiti'])) ? true : false) ); ?>> <?php echo $activity_value->name; ?></label>
                                            <input type="radio" class="rdo_activity_default pull-right" name="rdo_activity_default_<?php echo $module_key; ?>[]" value="<?php echo "{$module_key}_{$activity_value->id}"; ?>" class="pull-right" <?php echo set_radio('rdo_activity_default_'.$module_key, "{$module_key}_{$activity_value->id}", ((isset($machine_activities[$module_key]['activiti_default']) AND ($activity_value->id == $machine_activities[$module_key]['activiti_default']) ) ? true : false)); ?>>
                                        </div>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                    <?php if (isset($_POST['chk_module']) AND in_array($module_key, $_POST['chk_module'])) : ?>
                                    <div class="help-block ps_err">
                                        <?php echo form_error('chk_activity'); ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <!--// activities -->

                                <!-- addons -->
                                <div class="col-md-6">
                                    <strong><?php echo lang('crumb_addon'); ?></strong>
                                    <div style="height: 150px; overflow: auto; border: 1px solid #D6D3D3; padding: 0px 5px 9px 5px; margin-top: 5px;">
                                        <?php if (isset($addon[$module_key]) AND is_array($addon[$module_key]) AND !empty($addon[$module_key])) : ?>
                                        <?php foreach ($addon[$module_key] as $addon_key => $addon_value) : ?>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="chk_addon" name="chk_addon[]" value="<?php echo "{$module_key}_{$addon_value->id}"; ?>" <?php echo set_checkbox('chk_addon', "{$module_key}_{$addon_value->id}", ( isset($machine_addon[$module_key]) AND is_array($machine_addon[$module_key]) AND in_array($addon_value->id, $machine_addon[$module_key]) ) ? true : false); ?>> <?php echo $addon_value->name; ?>
                                            </label>
                                        </div>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <!--// addons -->
                            </div>
                        </div>
                        <?php
                        endif;
                            endforeach; ?>
                        <!--// tab module -->
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="btn_save"></label>
            <div class="col-md-6">
                <button id="btn_save" name="btn_save" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a id="btn_cancel" name="btn_cancel" href="javascript:close_modal('#modal_addon_detail');" class="btn btn-default"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>

        <input type="hidden" name="module_selected" id="module_selected" value="<?php echo isset($module_selected) ? json_encode($module_selected) : ''; ?>" />
    </form>
</div>