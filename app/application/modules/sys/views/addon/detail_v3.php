<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("sys/addon/edit/$id");
    } else {
        $form_link = create_url("sys/addon/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_addon_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_addon');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>
    $(function(){
        $('#frm_addon_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $('#module_selected').val(json_encode(module_selected));
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_addon_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });
    });

    try {
        var src, des, my, treeview;
        $('.select2').select2();
        <?php
            $tmp = array();
            if (isset($activity) AND is_array($activity) AND !empty($activity)) {
                foreach ($activity as $key => $value) {
                    $tmp[$value->id] = array(
                        'key'  => $value->id,
                        'text' => $value->name
                    );
                }
            }
        ?>
        var activities = $.parseJSON('<?php echo json_encode($tmp); ?>');

        var modules = {};
        <?php if (isset($modules) AND !empty($modules)) : ?>
        modules = $.parseJSON('<?php echo json_encode($modules); ?>');
        <?php endif; ?>

        var module_selected = {};
        <?php if (isset($module_selected) AND !empty($module_selected)) : ?>
        module_selected = json_decode('<?php echo json_encode($module_selected); ?>', true);
        <?php endif; ?>

        $(function(){
            $('#module').change(function(e) {
                treeview = $('#panel-left').data("kendoTreeView");

                added = e.added;
                if ( (added != undefined) && (modules[added.id] != undefined) ) {
                    module_selected[added.id] = new Array();
                    result = treeview.append([
                        {text: added.text}
                    ]);
                    result.attr('_index_module', added.id);
                }

                removed = e.removed;
                if ( (removed != undefined) && (modules[removed.id] != undefined) ) {
                    module_selected[removed.id] = new Array();
                    uid = $('li[_index_module="'+removed.id+'"]').attr('data-uid');
                    node = treeview.findByUid(uid);
                    treeview.remove(node);
                }
            });

            $("#panel-left").kendoTreeView();
            $("#panel-right").kendoTreeView({
                dragAndDrop: true,
                dataSource: [],
                drop: function(e){
                    e.preventDefault();
                    src = e.sourceNode;
                    des = e.destinationNode;
                    my = des;
                    treeview = $('#panel-left').data('kendoTreeView');
                    checker = false;
                    var target = treeview.findByUid($(des).attr('data-uid'));
                    if (e.dropPosition == 'over') {
                        des_item = $(des);
                        if (des_item.parent('ul').hasClass('k-treeview-lines')) {
                            checker = true;
                        }
                    }

                    id_module = $(des).attr('_index_module');
                    id_activity = $(src).attr('_index_activity');
                    console.log(id_module);
                    exist = $(des).find('li[_index_activity="'+id_activity+'"]').length;
                    if (checker && (exist == 0)) {
                        result = treeview.append({text: $(src).text()}, target);
                        result.find('div:first').append('<a class="remove_node_activity glyphicon glyphicon-remove" href="javascript:;" _index_module="'+id_module+'" _index_activity="'+id_activity+'"></a>');
                        $(result.selector).attr('_index_activity', id_activity);
                        if (module_selected[id_module] == undefined) {
                            module_selected[id_module] = new Array();
                        }
                        module_selected[id_module].push(id_activity);
                    } else {
                        e.setValid(false);
                        e.preventDefault();
                    }
                }
            });

            treeview_right = $('#panel-right').data('kendoTreeView');
            for (activity_key in activities) {
                result = treeview_right.append({text: activities[activity_key].text});
                result.attr('_index_activity', activities[activity_key].key);
            }

            treeview_left = $('#panel-left').data('kendoTreeView');
            // modules
            for (module_key in module_selected) {
                if (modules[module_key] != undefined) {
                    result = treeview_left.append({text: modules[module_key]});
                    result.attr('_index_module', module_key);
                }
            }

            // modules activities
            for (module_key in module_selected) {
                if (modules[module_key] != undefined) {
                    uid = $('li[_index_module="'+module_key+'"]').attr('data-uid');
                    target = treeview_left.findByUid(uid);
                    for (activity_key in module_selected[module_key]) {
                        item = module_selected[module_key][activity_key];
                        result = treeview_left.append({text: activities[item].text}, target);
                        result.attr('_index_activity', item);
                        result.find('div:first').append('<a class="remove_node_activity glyphicon glyphicon-remove" href="javascript:;" _index_module="'+module_key+'" _index_activity="'+item+'"></a>');
                    }
                }
            }

            $('#panel-left').on('click', '.remove_node_activity', function(e){
                e.preventDefault();
                i = $(this);
                if (confirm('Are you sure want to remove this activity ?')) {
                    id_module   = i.attr('_index_module');
                    id_activity = i.attr('_index_activity');
                    uid         = $('li[_index_activity="'+id_activity+'"]').attr('data-uid');
                    node        = treeview_left.findByUid(uid);
                    treeview_left.remove(node);

                    for (var i = 0; i < module_selected[id_module].length; i++) {
                        if (module_selected[id_module][i] == id_activity) {
                            module_selected[id_module].splice(i, 1);
                        }
                    };
                }
            });
        });
    } catch (err) {
        console.log(err);
    }
</script>
<div id="frm_addon_detail_wraper">
    <form class="form-horizontal" id="frm_addon_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-3 control-label" for="identifier"><?php echo lang('lbl_identifier');?></label>
            <div class="col-md-6">
                <input id="identifier" name="identifier" value="<?php echo set_value('identifier', isset($data->identifier) ? $data->identifier : ''); ?>" type="text" placeholder="<?php echo lang('holder_identifer');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('identifier'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="name"><?php echo lang('addon_lbl');?></label>
            <div class="col-md-6">
                <input id="name" name="name" value="<?php echo set_value('name', isset($data->name) ? $data->name : ''); ?>" type="text" placeholder="<?php echo lang('addon_holder_name');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('name'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="activity"><?php echo lang('lbl_module');?></label>
            <div class="col-md-9">
                <select id="module" name="module" multiple="multiple" class="select2" style="width: 457px; border-bottom: none;">
                    <?php foreach ($modules as $key => $value) : ?>
                    <option value="<?php echo $key; ?>" <?php echo (isset($module_selected) AND (isset($module_selected[$key])) AND is_array($module_selected[$key]) ) ? 'selected="selected"' : ''; ?>><?php echo $value; ?></option>
                    <?php endforeach; ?>
                </select>
                <div class="help-block ps_err">
                    <?php echo form_error('module'); ?>
                </div>
                <div style="margin-right: 10px; overflow: auto; border: 0px solid #aaa; padding-top: 7px;">
                    <div id="panel-left" style="float: left; min-height: 10px; width: 200px; "></div>
                    <div id="panel-right" style="float: left; width: 250px; border-left: 1px solid #aaa; "></div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="btn_save"></label>
            <div class="col-md-6">
                <button id="btn_save" name="btn_save" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a id="btn_cancel" name="btn_cancel" href="javascript:close_modal('#modal_addon_detail');" class="btn btn-default"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>

        <input type="hidden" name="module_selected" id="module_selected" value="<?php echo isset($module_selected) ? json_encode($module_selected) : ''; ?>" />
    </form>
</div>