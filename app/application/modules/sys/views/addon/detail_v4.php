<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("sys/addon/edit/$id");
    } else {
        $form_link = create_url("sys/addon/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_addon_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_addon');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>
    $(function(){
        $('#frm_addon_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_addon_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });

        $('#frm_addon_detail').on('click', '.chk_module', function(e){
            i = $(this);
            module_validate(i.val());
        });

        $('#frm_addon_detail').on('change', '.chk_activity', function(){
            i = $(this);
            if (i.is(':checked')) {
                $('.rdo_activity_default[value="'+i.val()+'"]').removeAttr('disabled');
            } else {
                y = $('.rdo_activity_default[value="'+i.val()+'"]');
                if (y.is(':checked')) {
                    $('.rdo_activity_default:not([disabled])').attr('checked', 'checked');
                }
                y.attr('disabled', 'disabled');
            }
        });

        setTimeout(function() {
            $('#frm_addon_detail .chk_module').each(function(index){
                i = $(this);
                module_validate(i.val());
                $('.btn').button('reset');;
            });
        }, 300);
    });

    function module_validate(id_module) {
        i = $('.chk_module[value="'+id_module+'"]');
        if (i.is(':checked')) {
            $('#tab_' + i.val()).find('.chk_activity, .rdo_activity_default').removeAttr('disabled');
        } else {
            $('#tab_' + i.val()).find('.chk_activity, .rdo_activity_default').attr('disabled', 'disabled');
        }

        $('#tab_' + i.val()).find('.chk_activity:not(:checked)').each(function(index){
            y = $(this);
            $('#tab_' + i.val()).find('.rdo_activity_default[value="' +y.val()+ '"]').attr('disabled', 'disabled');
        });
    }
</script>
<div id="frm_addon_detail_wraper">
    <form class="form-horizontal" id="frm_addon_detail" action="<?php echo $form_link; ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <?php if (isset($err)) : ?>
        <div class="alert alert-danger"><?php echo $err; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-3 control-label" for="identifier"><?php echo lang('lbl_identifier');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-6">
                <input id="identifier" name="identifier" value="<?php echo set_value('identifier', isset($data->identifier) ? $data->identifier : ''); ?>" type="text" placeholder="<?php echo lang('holder_identifer');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('identifier'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="name"><?php echo lang('addon_lbl');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-6">
                <input id="name" name="name" value="<?php echo set_value('name', isset($data->name) ? $data->name : ''); ?>" type="text" placeholder="<?php echo lang('addon_holder_name');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('name'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label"><?php echo lang('lbl_module');?>&nbsp;<span class="star">&nbsp;*</span></label>
            <div class="col-md-8">
                <ul id="machine_module_tab" class="nav nav-tabs">
                    <?php $counter = 0; foreach ($modules as $key => $value) : ?>
                    <li class="<?php echo ($counter == 0) ? 'active' : ''; ?>"><a href="#tab_<?php echo $key; ?>" data-toggle="tab"><?php echo $value; ?></a></li>
                    <?php $counter++; endforeach; ?>
                </ul>

                <div class="tab-content" style="border: 1px solid #DDD;padding: 10px;border-top: none;">
                    <!-- tab module -->
                    <?php $counter = 0; foreach ($modules as $module_key => $module_value) :
                    $module_checked = isset($module_activities[$module_key]) ? true : false;
                    $module_checked = set_checkbox('chk_module', $module_key, $module_checked);
                    ?>
                    <div class="tab-pane fade <?php echo ($counter == 0) ? 'in active' : ''; ?>" id="tab_<?php echo $module_key; ?>">
                        <div class="row">
                            <div class="col-md-12" style="padding-bottom: 10px;">
                                <div class="checkbox"><label><input class="chk_module" type="checkbox" name="chk_module[]" value="<?php echo $module_key; ?>" <?php echo $module_checked; ?>> <?php echo lang('addon_cbo_used_for_module'); ?></label></div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- activities -->
                            <div class="col-md-12">
                                <div>
                                    <strong class="pull-left"> <?php echo lang('crumb_activity'); ?> </strong>
                                    <?php if (0) : ?>
                                    <span class="pull-right" style="font-size: small;font-style: italic;font-weight: normal;">Activity default</span>
                                    <?php endif; ?>
                                </div>
                                <div class="clearfix"></div>
                                <div style="height: 120px; overflow: auto; border: 1px solid #ECECEC; padding: 5px 10px; margin-top: 5px;">
                                    <?php if (isset($activities) AND !empty($activities)) : ?>
                                    <?php foreach ($activities as $activity_key => $activity_value) :
                                    $activity_checked = (isset($module_activities[$module_key]['activiti']) AND is_array($module_activities[$module_key]['activiti']) AND in_array($activity_key, $module_activities[$module_key]['activiti'])) ? true : false;
                                    $activity_checked = set_checkbox('chk_activity', "{$module_key}_{$activity_key}", $activity_checked);
                                    ?>
                                    <div class="checkbox">
                                        <label><input class="chk_activity" type="checkbox" name="chk_activity[]" value="<?php echo "{$module_key}_{$activity_key}"; ?>" <?php echo $activity_checked; ?>> <?php echo $activity_value->name; ?></label>
                                        <?php if (0) : ?>
                                        <input type="radio" class="rdo_activity_default pull-right" name="rdo_activity_default_<?php echo $module_key; ?>" value="<?php echo "{$activity_key}"; ?>" disabled="disabled">
                                        <?php endif; ?>
                                    </div>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <!--// activities -->
                        </div>
                    </div>
                    <?php $counter++; endforeach; ?>
                    <!--// tab module -->
                </div>

                <div class="help-block ps_err">
                    <?php echo form_error('chk_module'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="btn_save"></label>
            <div class="col-md-6">
                <button id="btn_save" name="btn_save" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                <a id="btn_cancel" name="btn_cancel" href="javascript:close_modal('#modal_addon_detail');" class="btn btn-default"><?php echo lang('btn_cancel');?></a>
            </div>
        </div>
    </form>
</div>