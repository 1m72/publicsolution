<script>
    var grid_instance;
    var dialog_edit;
    var dialog_edit_id = '#dialog_edit';
    var id_grid = '#grid';
    var  number = 0;

    function grid_refresh() {
        $(id_grid).data("kendoGrid").dataSource.read();
        $(id_grid).data('kendoGrid').refresh();
    }

    $(function() {
        grid_limit = <?php echo isset($limit) ? intval($limit) : 0; ?>;

        $('#dialog_add_activity').kendoWindow({
            actions: ["Pin", "Close"],
            draggable: false,
            modal: true,
            pinned: false,
            resizable: false,
            width: "650px",
            title: "<?php echo lang('lbl_add_new');?>",
            visible: false
        });

        dialog_edit = $(dialog_edit_id).kendoWindow({
            actions: ["Pin", "Close"],
            draggable: false,
            modal: true,
            pinned: false,
            resizable: false,
            width: "650px",
            title: "<?php echo lang('lbl_worker_detail');?>",
        }).data("kendoWindow");

        $('body').on('click', '.btn_close_dialog1', function(e){
            e.preventDefault();
            dialog_edit.close();
        });

        $('body').on('click', '.btn_delete', function(e){
            e.preventDefault();
            i = $(this);
            var kendoWindow = $("<div />").kendoWindow({
                title: "<?php echo lang('delete_confirm')?>",
                resizable: false,
                width: 450,
                modal: true
            });

            kendoWindow.data("kendoWindow")
                .content($("#delete-confirmation").html())
                .center().open();

            kendoWindow
                .find(".delete-confirm, .delete-cancel")
                .click(function() {
                    if ($(this).hasClass("delete-confirm")) {
                        $.ajax({
                            type: 'POST',
                            url: i.attr('href'),
                            dataType: 'json',
                            success: function(data){
                                if (data.status == true) {
                                    var grid = $(id_grid).data("kendoGrid");
                                    var row = grid.select();
                                    var uid = row.data("uid");
                                    row.remove();
                                    grid_refresh();
                                }
                            },
                            error: function(err){}
                        });
                    }

                    kendoWindow.data("kendoWindow").close();
                })
                .end()
        });

       $('body').on('click', '.btn_edit', function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                type: 'GET',
                url: i.attr('href'),
                success: function(data){
                    dialog_edit.content(data);
                    dialog_edit.center();
                    dialog_edit.open();
                },
                error: function(err){
                    alert('error');
                }
            });
        });
        <?php if ( isset($current_language) AND ($current_language == 'de') ) : ?>
            var order_w             = 50;
            var id_w                = 80;
            var first_name_w        = 120;
            var last_name_w         = 120;
            var code_w         		= 100;
            var phone_w             = 130;
            var nfc_w               = 100;
            var company_w           = 150;
            var updated_w           = 120;
            var created_w           = 120;
            var option_w            = 200;
        <?php else : ?>
            var order_w             = 50;
            var id_w                = 80;
            var first_name_w        = 100;
            var last_name_w         = 100;
            var code_w         		= 100;
            var phone_w             = 130;
            var nfc_w               = 100;
            var company_w           = 150;
            var updated_w           = 120;
            var created_w           = 120;
            var option_w            = 140;
        <?php endif; ?>
        grid_instance = $(id_grid).kendoGrid({
            dataSource: {
                transport: {
                    read: "<?php echo current_url(); ?>",
                    "contentType":"application\/json",
                    "type":"POST"
                },
                schema: {
                    data: "data",
                    total: "total"
                },
                pageSize: grid_limit,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            columns: [
                { field: "order", title: '<?php echo lang('grid_number');?>', filterable: false, sortable: false, width: order_w, template: function(order){
                       return number = number + 1;
                    }
                },
                { field: "first_name", title: '<?php echo lang('lbl_first_name'); ?>', width: first_name_w, },
                { field: "last_name", title: '<?php echo lang('lbl_last_name'); ?>', width: last_name_w,  },
                { field: "nfc_code", title: '<?php echo lang('wk_lbl_nfc_code'); ?>', width: code_w,  },
                <?php if (0) : ?>
                { field: "phone", title: '<?php echo lang('lbl_phone'); ?>', width: phone_w, },
                <?php endif; ?>
                { field: "address", title: '<?php echo lang('lbl_address'); ?>' },
                { field: "birthday", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", title: '<?php echo lang('lbl_birthday');?>' },
                { field: "company_name", title: '<?php echo lang('lbl_company'); ?>', width: company_w,  },
                { field: "option", title: '<?php echo lang('grid_option'); ?>', filterable: false, sortable: false, width: option_w, attributes:{style: "text-align:center"}, template: function(data) {
                        html = "<a id='btn_edit_"+data.id+"' href='<?php echo create_url('sys/worker/edit'); ?>/"+data.id+"' class='btn_edit btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span>&nbsp;<?php echo lang('btn_edit')?></a>&nbsp;";
                        html = html + "<a _id='"+data.id+"' href='<?php echo create_url('sys/worker/delete'); ?>/"+data.id+"' class='btn_delete btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span>&nbsp;<?php echo lang('btn_delete')?></a>";
                        return html;
                    }
                }
            ],
            toolbar: kendo.template($("#toolbar_template").html()),
            selectable: "row",
            filterable: true,
            groupable: true,
            scrollable: true,
            reorderable: true,
            columnMenu: true,
            sortable: {
                mode: "multiple",
                allowUnsort: true
            },
            navigatable: true,
	        height:grid_height ,
            pageable: {
                pageSize : grid_limit,
                pageSizes: true,
                /*input: true,*/
                refresh: true,
                messages: {
                    itemsPerPage: "<?php echo lang('lbl_item_per_page');?>"
                }
            },
            dataBinding: function(e) {
                var grid     = $(id_grid).data("kendoGrid");
                grid_limit   = grid.dataSource.pageSize();
                current_page = grid.dataSource.page();
                number       = (current_page - 1) * grid_limit;
            }
        });
    });
</script>

<div class="col-xs-12 col-md-12">
    <div id="grid"></div>
</div>

<div id="dialog_edit" style="display: none;"></div>
<div id="dialog_delete" style="display: none;"></div>
<div id="dialog_add_activity" style="display: none;"></div>
<script id="toolbar_template" type="text/x-kendo-template">
    <div>
        <span class="pull-left">
            <h4>
                &nbsp;<span class="glyphicon glyphicon-user"></span>
                &nbsp;<strong><?php echo lang('lbl_list_worker');?></strong>
            </h4>
        </span>
        <span class="pull-right">
            <a href="<?php echo create_url('sys/worker/add'); ?>" class="btn btn-primary btn_edit btn-default"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new');?></a>
        </span>
    </div>
</script>

<script id="delete-confirmation" type="text/x-kendo-template">
    <div class="row">
        <span class="col-md-2"></span>
        <span class="col-md-8">
            <p class="delete-message"><?php echo lang('msg_question_delete');?></p>
        </span>
        <span class="col-md-2"></span>
    </div>
    <div class="row">
        <span class="col-md-7"></span>
        <span class="col-md-5">
            <span class="pull-right">
                <button class="delete-confirm btn btn-danger"><?php echo lang('btn_yes');?></button>
                <a href="#" class="delete-cancel btn btn-default"><?php echo lang('btn_no');?></a>
            </span>
        </span>
    </div>
</script>