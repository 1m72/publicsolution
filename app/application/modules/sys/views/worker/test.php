<?php if (0) : ?>
<?php
    $form_link = create_url("sys/worker/test");
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<script type="text/javascript">
    $(function(){
        $('[name="worker[]"]').bootstrapSwitch('size', 'mini');
        $('#frm_test').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_test_wraper').html(res);
                },
                error: function(err) {}
            });
        });
    });
</script>
<div id="frm_test_wraper" width="800px;">
    <form class="form-horizontal" id="frm_test" action="<?php echo $form_link; ?>" method="post" accept-charset="utf-8">
        <?php for ($i=0; $i < 10; $i++) : ?>
        <div>
            <input type="checkbox" name="cbo[]" value="<?php echo $i; ?>" <?php echo set_checkbox('cbo[]', $i); ?>>
        </div>
        <?php endfor; ?>
        <input type="submit" value="Test" />
    </form>
</div>
<?php endif; ?>

<?php if (1) : ?>
<script type="text/javascript">
    $(function(){
        // $('[name="worker[]"]').bootstrapSwitch('size', 'mini');
        str = $('#frm_test').serialize();
        console.log('aaaa');
        $('#frm_test').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                type: "POST",
                data: $('#frm_test').serialize(),
                success: function(result){
                    $('#frm_test_wraper').html(result);
                }
            });
        });
    });
</script>
<div id="frm_test_wraper">
    <form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8" id="frm_test">
        <div>
            <input type="checkbox" name="cbo[]" value="1" <?php echo set_checkbox('cbo', 1); ?>/> Test 1
        </div>
        <div>
            <input type="checkbox" name="cbo[]" value="2" <?php echo set_checkbox('cbo', 2); ?>/> Test 2
        </div>
        <div>
            <input type="checkbox" name="cbo[]" value="3" <?php echo set_checkbox('cbo', 3); ?>/> Test 3
        </div>
        <div>
            <input type="checkbox" name="cbo[]" value="4" <?php echo set_checkbox('cbo', 4); ?>/> Test 4
        </div>
        <div>
            <input type="submit" name="submit" value="Submit"  />
        </div>
        <input type="hidden" name="checker" value="<?php echo time(); ?>" />
    </form>
</div>
<?php endif; ?>