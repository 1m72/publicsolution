<script type="text/javascript">
    $(function() {
        var column_properties = $.parseJSON('<?php echo $column_properties; ?>');
        var grid_config = {
            'target': '#grid_device',
            'url': '<?php echo current_url(); ?>',
            'limit': <?php echo isset($limit) ? intval($limit) : 0; ?>,
            'columns': [
                { field: "order", title: '<?php echo lang('grid_number');?>', filterable: false, sortable: false, width: column_properties.order_w, template: function(order){return grid_number = grid_number + 1;} },
                <?php if (0) : ?>
                { field: "id", title: '<?php //echo lang('dv_lbl_id');?>', width: column_properties.id_w, },
                <?php endif; ?>
                { field: "id_device", title: '<?php echo lang('dv_lbl_device');?>' },
                <?php if (0) : ?>
                { field: "id_unique", title: 'Unique' },
                <?php endif; ?>
                { field: "actived", title: '<?php echo lang('dv_lbl_actived');?>',filterable: false, sortable: false, width: column_properties.actived_w, attributes:{style: "text-align:center"},template: "#=actived==1?'<span class=\"label label-success\"><?php echo lang('dv_btn_on'); ?></span>':'<span class=\"label label-default\"><?php echo lang('dv_btn_off'); ?></span>'#" },
                { field: "created_at", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", width: column_properties.created_w, title: '<?php echo lang('grid_created_at');?>'},
                { field: "updated_at", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", width: column_properties.updated_w, title: '<?php echo lang('grid_updated_at');?>'},

                <?php if (0) : ?>
                { field: "option", title: '<?php echo lang('grid_option');?>', filterable: false, sortable: false, width: 150, template: function(data) {
                       html = "<a id='btn_edit_"+data.id+"' href='<?php echo create_url('sys/device/edit'); ?>/"+data.id+"' class='btn_edit btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span>&nbsp;<?php echo lang('btn_edit');?></a>&nbsp;";
                       html = html + "<a _id='"+data.id+"' href='<?php echo create_url('sys/device/delete'); ?>/"+data.id+"' class='btn_delete btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span>&nbsp;<?php echo lang('btn_delete');?></a>";
                       return html;
                   }
                }
                <?php endif; ?>
            ]
        }
        create_grid(grid_config);
    });
</script>
<div id="grid_device"></div>
<script id="toolbar_template" type="text/x-kendo-template">
    <div>
        <span class="pull-left">
            <h4>
                &nbsp;<span class="glyphicon glyphicon-user"></span>
                &nbsp;<strong><?php echo lang('dv_lbl_list_device');?></strong>
            </h4>
        </div>
    </div>
</script>
<script id="delete-confirmation" type="text/x-kendo-template">
    <div class="row">
        <span class="col-md-2"></span>
        <span class="col-md-8">
            <p class="delete-message"><?php echo lang('dv_msg_question_delete_device');?></p>
        </span>
        <span class="col-md-2"></span>
    </div>
    <div class="row">
        <span class="col-md-7"></span>
        <span class="col-md-5">
            <span class="pull-right">
                <button class="delete-confirm btn btn-default"><?php echo lang('btn_yes');?></button>
                <a href="#" class="delete-cancel btn btn-default"><?php echo lang('btn_no');?></a>
            </span>
        </span>
    </div>
</script>