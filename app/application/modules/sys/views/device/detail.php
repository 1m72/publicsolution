<div id="frm_detail_wraper">
    <script type="text/javascript">
        $(function(){
            $('.select2').select2({ width: 'resolve' });

            $('#frm_detail').submit(function(e){
                e.preventDefault();
                i = $(this);
                $.ajax({
                    url: i.attr('action'),
                    type: 'POST',
                    data: i.serialize(),
                    success: function(res) {
                        $('#frm_detail_wraper').html(res);
                    },
                    error: function(err) {}
                });
            });
        });
    </script>
    <form class="form-horizontal" id="frm_detail" action="<?php echo create_url('sys/device/edit/' . (isset($id) ? $id : '') ); ?>">
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-4 control-label" for="device"><?php echo lang('dv_lbl_name');?></label>
            <div class="col-md-5">
                <input id="device" name="device" value="<?php echo set_value('device', isset($data->device) ? $data->device : ''); ?>" type="text" placeholder="<?php echo lang('dv_holder_device');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('device'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
                <label class="col-md-4 control-label" for="city"><?php echo lang('dv_lbl_city');?></label>
                <div class="col-md-5">
                    <select name="city" class="select2" style="width: 250px;">
                        <option value=""><?php echo lang('lbl_select_city');?></option>
                            <?php if (isset($city) && !empty($city)) : ?>
                            <?php foreach ($city as $key => $value) : ?>
                            <option value="<?php echo $value->id; ?>" <?php echo ( isset($company->id_city) AND ($company->id_city == $value->id) ) ? 'selected' : ''; ?> ><?php echo $value->name; ?></option>
                            <?php endforeach; ?>
                            <?php endif; ?>
                    </select>
                    <div class="help-block ps_err">
                        <?php echo form_error('city'); ?>
                    </div>
                </div>
        </div>

        <div class="form-group">
              <label class="col-md-4 control-label" for="radios"><?php echo lang('dv_lbl_actived');?></label>
              <div class="col-md-4">
                    <div class="radio">
                        <label for="actived0">
                            <input name="actived" id="actived0" value="0"  type="radio">
                            <?php echo lang('dv_lbl_accept');?>
                        </label>
       	            </div>
                    <div class="radio">
                        <label for="actived1">
                            <input name="actived" id="actived1" value="1" type="radio">
                            <?php echo lang('dv_lbl_do_not_accept');?>
                        </label>
            	    </div>
                    <div class="help-block ps_err">
                        <?php echo form_error('actived'); ?>
                    </div>
              </div>
        </div>


        <div class="form-group">
            <label class="col-md-4 control-label" for="btn_save"></label>
            <div class="col-md-8">
                <button id="btn_save" name="btn_save" class="btn btn-primary">Save change</button>
                <a id="btn_cancel" name="btn_cancel" class="btn_close_dialog btn btn-default"><?php echo lang('btn_cancel')?></a>
            </div>
        </div>

    </form>
</div>