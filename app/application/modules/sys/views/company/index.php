<script type="text/javascript">
    $(function() {
        init_modal('#modal_company_detail');
        init_modal('#modal_company_delete');

        var column_properties = $.parseJSON('<?php echo $column_properties; ?>');
        var grid_config = {
            'target': '#grid_company',
            'url': '<?php echo current_url(); ?>',
            'limit': <?php echo isset($limit) ? intval($limit) : 0; ?>,
            'columns': [
                { field: "order", title: '<?php echo lang('grid_number');?>', filterable: false, sortable: false, width: column_properties.order_w, template: function(order){return grid_number = grid_number + 1;} },
                <?php if (0) : ?>
                { field: "id", title: '<?php //echo lang('cpn_lbl_id');?>', width: column_properties.id_w, },
                <?php endif; ?>
                { field: "name", title: '<?php echo lang('cpn_lbl_name');?>', width: column_properties.name_w, },
                <?php if (0) : ?>
                { field: "id_city", title: '<?php echo lang('cpn_lbl_city');?>', width: column_properties.city_w, },
                <?php endif; ?>
                { field: "description", title: '<?php echo lang('cpn_lbl_description');?>' },
                { field: "created_at", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", title: '<?php echo lang('grid_created_at');?>', width: column_properties.created_w, },
                { field: "updated_at", type: 'date', format: "{0:<?php echo config_item('date_format') ?>}", title: '<?php echo lang('grid_updated_at');?>', width: column_properties.updated_w, },
                { field: "option", title: '<?php echo lang('grid_option')?>', filterable: false, sortable: false, width: column_properties.option_w, attributes:{style: "text-align:center"},template: function(data) {
                        html = "<a _modal='#modal_company_detail' _title='<?php echo lang('cpn_lbl_company_detail'); ?>' href='<?php echo create_url('sys/company/edit'); ?>/"+data.id+"' class='sys_modal_detail btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span>&nbsp;<?php echo lang('btn_edit')?></a>&nbsp;";
                        html = html + "<a _id_grid = '#grid_company' _modal='#modal_company_delete' _title='<?php echo lang('delete_confirm'); ?>' href='<?php echo create_url('sys/company/delete'); ?>/"+data.id+"' class='sys_modal_delete btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span>&nbsp;<?php echo lang('btn_delete')?></a>";
                        return html;
                    }
                }
            ]
        };
        create_grid(grid_config);
    });
</script>
<div id="grid_company"></div>
<script id="toolbar_template" type="text/x-kendo-template">
    <div>
        <span class="pull-left">
            <h4>
                &nbsp;<span class="glyphicon glyphicon-user"></span>
                &nbsp;<strong><?php echo lang('cpn_lbl_list_company');?></strong>
            </h4>
        </span>
        <span class="pull-right">
            <a _modal="\\#modal_company_detail" _title='<?php echo lang('cpn_lbl_company_detail');?>' href="<?php echo create_url('sys/company/add'); ?>" class="sys_modal_detail btn btn-primary btn-default"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang('btn_add_new');?></a>
        </span>
    </div>
</script>
<script id="delete-confirmation" type="text/x-kendo-template">
    <div class="row">
        <span class="col-md-2"></span>
        <span class="col-md-8">
            <p class="delete-message"><?php echo lang('cpn_msg_question_delete_company'); ?></p>
        </span>
        <span class="col-md-2"></span>
    </div>
    <div class="row">
        <span class="col-md-7"></span>
        <span class="col-md-5">
            <span class="pull-right">
                <button class="delete-confirm btn btn-danger"><?php echo lang('btn_yes');?></button>
                <a href="#" class="delete-cancel btn btn-default"><?php echo lang('btn_no')?></a>
            </span>
        </span>
    </div>
</script>