<?php
    if ( $edit_flag AND isset($id) AND ($id > 0) ) {
        $form_link = create_url("sys/company/edit/$id");
    } else {
        $form_link = create_url("sys/company/add");
    }
    $callback  = $this->input->get('callback');
    if ($callback) {
        $form_link .= "?callback=$callback";
    }
?>
<script type="text/javascript">
    <?php if (isset($flag) AND ($flag) ) : ?>
    setTimeout(
        function(){
            close_modal('#modal_company_detail');
            <?php echo $callback ? "$callback();" : "refresh_grid('#grid_company');"; ?>
        },
        timeout_dialog
    );
    <?php endif; ?>

    $(function(){
        $('#frm_company_detail').submit(function(e){
            e.preventDefault();
            i = $(this);
            $.ajax({
                url: i.attr('action'),
                type: 'POST',
                data: i.serialize(),
                success: function(res) {
                    $('#frm_company_detail_wraper').html(res);
                },
                error: function(err) {}
            });
        });
    });
</script>
<div id="frm_company_detail_wraper">
    <form class="form-horizontal" id="frm_company_detail" action="<?php echo $form_link; ?>" >
        <?php if (isset($msg)) : ?>
        <div class="alert alert-success"><?php echo $msg; ?></div>
        <?php endif; ?>

        <?php if (isset($err)) : ?>
        <div class="alert alert-danger"><?php echo $err; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label class="col-md-4 control-label" for="name"><?php echo $this->lang->line('cpn_lbl_name');?><span class="star">&nbsp;*</span></label>
            <div class="col-md-5">
                <input id="name" name="name" value="<?php echo set_value('name', isset($company->name) ? $company->name : ''); ?>" type="text" placeholder="<?php echo $this->lang->line('cpn_holder_company_name');?>" class="form-control input-md">
                <div class="help-block ps_err">
                    <?php echo form_error('name'); ?>
                </div>
            </div>
        </div>

        <?php if (0) : ?>
        <div class="form-group">
                <label class="col-md-4 control-label" for="city"><?php echo $this->lang->line('cpn_lbl_city');?><span class="star">&nbsp;*</span></label>
                <div class="col-md-5">
                    <select name="city" class="select2" style="width: 250px;">

                        <option value=""><?php echo lang('lbl_select_city')?></option>
                            <?php if (isset($city) && !empty($city)) : ?>
                            <?php foreach ($city as $key => $value) : ?>
                            <option value="<?php echo $value->id; ?>" <?php echo ( isset($company->id_city) AND ($company->id_city == $value->id) ) ? 'selected' : ''; ?> ><?php echo $value->name; ?></option>
                            <?php endforeach; ?>
                            <?php endif; ?>
                    </select>
                    <div class="help-block ps_err">
                        <?php echo form_error('city'); ?>
                    </div>
                </div>
        </div>
        <?php endif; ?>
        <div class="form-group">
              <label class="col-md-4 control-label" for="description"><?php echo lang('cpn_lbl_description')?></label>
              <div class="col-md-5">
                    <textarea class="form-control input-md" id="description" name="description" style="width: 250px; height: auto;"  ><?php echo set_value('description', isset($company->description) ? $company->description : ''); ?></textarea>
                    <div class="help-block ps_err">
                        <?php echo form_error('description'); ?>
                    </div>
              </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="btn_save"></label>
            <div class="col-md-8">
                <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change')?></button>
                <a class="btn btn-default" href="javascript:;" onclick="close_modal('#modal_company_detail');"><?php echo lang('btn_cancel')?></a>
            </div>
        </div>

    </form>
</div>