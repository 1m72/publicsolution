<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/main.css'); ?>">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div class="bs-example bs-example-tabs">
            <ul style="list-style: none; padding: 0px; margin: 0px; position: absolute; top: 14px; right: 20px; ">
                <?php if (0) : ?>
                <li class="dropdown">
                    <a href="http://publicsolution.dev/sys/user" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" _loading="0">
                        <i class="glyphicon glyphicon-list"></i> Modules <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><span class="glyphicon glyphicon-asterisk"></span>&nbsp;Winterdienst</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;Streetchecking</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-road"></span>&nbsp;Streetcleaning</a></li>
                    </ul>
                </li>
                <?php endif; ?>
            </ul>

            <ul id="myTab" class="nav nav-tabs">
                <li class="active"><a href="#home" data-toggle="tab"><span class="glyphicon glyphicon-compressed"></span>&nbsp;Winterdienst</a></li>
                <li><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-user"></span>&nbsp;Worker</a></li>
                <li><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-magnet"></span>&nbsp;Add on</a></li>
                <li class="dropdown pull-right">
                    <a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-th-large"></i> Other <b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="myTabDrop1">
                        <li><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-retweet"></span>&nbsp;Activity</a></li>
                        <li><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-flash"></span>&nbsp;NFC</a></li>
                        <li><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-inbox"></span>&nbsp;Device</a></li>
                        <li><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-link"></span>&nbsp;Object</a></li>
                        <li><a href="#home" data-toggle="tab"><span class="glyphicon glyphicon-briefcase"></span>&nbsp;Module</a></li>
                        <li><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-hdd"></span>&nbsp;Company</a></li>
                        <li><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-th"></span>&nbsp;City</a></li>
                        <li><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-user"></span>&nbsp;User</a></li>
                        <li><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-user"></span>&nbsp;User Group</a></li>
                    </ul>
                </li>
                <?php if (0) : ?>
                <li class="pull-right"><a href="#home" data-toggle="tab"><span class="glyphicon glyphicon-wrench"></span>&nbsp;Setting</a></li>
                <?php endif; ?>
            </ul>
            <div id="myTabContent" class="tab-content" style="margin-top: 5px;">
                <div class="tab-pane fade in active" id="home">
                    <?php echo $this->load->view('machine/index'); ?>
                </div>
                <div class="tab-pane fade" id="profile">
                <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.</p>
                </div>
                <div class="tab-pane fade" id="dropdown1">
                <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
                </div>
                <div class="tab-pane fade" id="dropdown2">
                <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
                </div>
            </div>
          </div>
    </div>
</div>