<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs pull-left nav-tab-module">
            <?php if (isset($nav_modules) AND is_array($nav_modules) AND !empty($nav_modules) ): ?>
            <?php
            $module_settings  = config_item('module_settings');
            foreach ($nav_modules as $key => $value) :
            $value_id    = isset($value->id) ? $value->id : '';
            $value_title = isset($value->title) ? $value->title : '';
            $value_type  = isset($value->type) ? $value->type : '';
            $value_icon  = isset($module_settings[$value_type]['icon']) ? $module_settings[$value_type]['icon'] : '';
            ?>
            <li class="dropdown <?php echo ($nav_module_current == $value_id) ? 'module_active' : ''; ?>">
                <?php if (in_array($value_type, array(MODULE_TYPE_WINTERDIENST, MODULE_TYPE_STREETCLEANING, MODULE_TYPE_STREETCHECKING))) : ?>
                    <a href="<?php echo create_url('sys/visualization') . '?module=' . ($value_id); ?>" class="module_link dropdown-toggle" data-toggle="dropdown">
                        <i class="<?php echo $value_icon; ?>"></i>
                        <?php echo $value_title;?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo create_url('sys/visualization') . "?module={$value_id}"; ?>"><span class="glyphicon glyphicon-screenshot"></span>&nbsp;<?php echo lang('lbl_map_tracker');?></a></li>
                        <?php if (is_allow()) : ?>
                        <li class="divider"></li>
                        <li><a href="<?php echo create_url('sys/machine') . "?module={$value_id}"; ?>"><span class="glyphicon glyphicon-compressed"></span>&nbsp;<?php echo lang('crumb_machine');?></a></li>
                        <li><a href="<?php echo create_url('sys/activity') . "?module={$value_id}"; ?>"><span class="glyphicon glyphicon-retweet"></span>&nbsp;<?php echo lang('crumb_activity');?></a></li>
                        <li><a href="<?php echo create_url('sys/worker') . "?module={$value_id}"; ?>"><span class="glyphicon glyphicon-user"></span>&nbsp;<?php echo lang('crumb_worker');?></a></li>
                        <li><a href="<?php echo create_url('sys/addon') . "?module={$value_id}"; ?>"><span class="glyphicon glyphicon-magnet"></span>&nbsp;<?php echo lang('crumb_addon');?></a></li>
                        <?php endif; ?>
                    </ul>
                <?php elseif ($value_type == MODULE_TYPE_FASTTASK) : ?>
                    <a href="<?php echo create_url('fasttask/fasttask') . '?module=' . ($value_id); ?>" class="module_link dropdown-toggle" data-toggle="dropdown">
                        <i class="<?php echo $value_icon; ?>"></i>
                        <?php echo $value_title;?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo create_url('fasttask/fasttask') . "?module={$value_id}"; ?>"><span class="glyphicon glyphicon-screenshot"></span>&nbsp;<?php echo lang('lbl_map_tracker');?></a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo create_url('fasttask/cost_center') . "?module={$value_id}"; ?>"><span class="glyphicon glyphicon-usd"></span>&nbsp;Cost center</a></li>
                        <li><a href="<?php echo create_url('fasttask/freetext') . "?module={$value_id}"; ?>"><span class="glyphicon glyphicon-font"></span>&nbsp;Freetext</a></li>
                        <li><a href="<?php echo create_url('fasttask/superior') . "?module={$value_id}"; ?>"><span class="glyphicon glyphicon-user"></span>&nbsp;<?php echo lang('crumb_superior'); ?></a></li>
                        <li><a href="<?php echo create_url('fasttask/employee_group') . "?module={$value_id}"; ?>"><span class="glyphicon glyphicon-user"></span>&nbsp;Employee group</a></li>
                        <li><a href="<?php echo create_url('fasttask/object') . "?module={$value_id}"; ?>"><span class="glyphicon glyphicon-asterisk"></span>&nbsp;<?php echo lang('crumb_object');?></a></li>
                        <li><a href="<?php echo create_url('fasttask/object_place') . "?module={$value_id}"; ?>"><span class="glyphicon glyphicon-user"></span>&nbsp; Object place</a></li>
                    </ul>
                <?php elseif ($value_type == MODULE_TYPE_EMERGENCY_SERVICE) : ?>
                    <a href="<?php echo create_url('emergency_service/scheduler') . '?module=' . ($value_id); ?>" class="module_link dropdown-toggle" data-toggle="dropdown">
                        <i class="<?php echo $value_icon; ?>"></i>
                        <?php echo $value_title;?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo create_url('emergency_service/scheduler') . "?module={$value_id}"; ?>"><span class="glyphicon glyphicon-calendar"></span>&nbsp;<?php echo 'Scheduler';?></a></li>
                        <li><a href="<?php echo create_url('emergency_service/group') . "?module={$value_id}"; ?>"><span class="glyphicon glyphicon-th-list"></span>&nbsp;<?php echo 'Group';?></a></li>
                    </ul>
                <?php endif; ?>
            </li>
            <?php endforeach; ?>
            <?php endif; ?>
        </ul>

        <?php if (is_allow()) : ?>
        <ul class="hidden-xs hidden-sm nav nav-tabs pull-right">
            <?php if (0) : ?>
            <li class="<?php echo ($this->router->fetch_class() == 'activity') ? 'module_active' : ''; ?>"><a href="<?php echo create_url('sys/activity'); ?>"><span class="glyphicon glyphicon-retweet"></span>&nbsp;<?php echo lang('crumb_activity');?></a></li>
            <li class="<?php echo ($this->router->fetch_class() == 'worker') ? 'module_active' : ''; ?>"><a href="<?php echo create_url('sys/worker'); ?>"><span class="glyphicon glyphicon-user"></span>&nbsp;<?php echo lang('crumb_worker');?></a></li>
            <li class="<?php echo ($this->router->fetch_class() == 'nfc') ? 'module_active' : ''; ?>"><a href="<?php echo create_url('sys/nfc'); ?>"><span class="glyphicon glyphicon-flash"></span>&nbsp;<?php echo lang('crumb_nfc');?></a></li>
            <?php endif; ?>

            <?php $nav_components = config_item('nav_components'); ?>
            <?php if (is_array($nav_components) AND !empty($nav_components)): ?>
            <li class="dropdown">
                <a href="<?php echo create_url('sys/user'); ?>" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 22px; padding: 0px; ">
                    <i class="glyphicon glyphicon-th-large"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <?php foreach ($nav_components as $key => $value) : ?>
                    <li><a href="<?php echo create_url($key) . (isset($value['query_string']) ? ('?' . http_build_query($value['query_string'])) : ''); ?>"><span class="<?php echo $value['icon'];?>"></span>&nbsp;<?php echo lang($value['label']);?></a></li>
                    <?php endforeach; ?>
                </ul>
            </li>
            <?php endif; ?>
        </ul>
        <?php endif; ?>
    </div>
</div>
