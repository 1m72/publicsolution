<?php if ($nav_module_current !== false) : ?>
<?php
    $global_config       = global_config();
    $current_module_type = isset($global_config['modules'][$nav_module_current]->type) ? $global_config['modules'][$nav_module_current]->type : false;
?>
<ul class="nav nav-pills" id="module_submenu">
    <li class="alert label-warning hide" style="margin-bottom: 0px; padding: 5px 8px; border: none; "><span><?php echo mb_convert_case('visualization', MB_CASE_UPPER) ?></span></li>
    <?php if (in_array($current_module_type, array(MODULE_TYPE_WINTERDIENST, MODULE_TYPE_STREETCLEANING, MODULE_TYPE_STREETCHECKING))) : ?>
        <li class="<?php echo ($nav_module_sub_current == 'visualization') ? 'active' : ''; ?>" style="margin-left: 0px;">
            <a href="<?php echo create_url('sys/visualization'). "?module={$nav_module_current}"; ?>">
                <span class="glyphicon glyphicon-screenshot"></span>&nbsp;<?php echo lang('lbl_map_tracker');?>
            </a>
        </li>

        <?php if (is_allow()) : ?>
            <li class="<?php echo ($nav_module_sub_current == 'worker') ? 'active' : ''; ?>">
                <a href="<?php echo create_url('sys/worker') . "?module={$nav_module_current}"; ?>">
                    <span class="glyphicon glyphicon-user"></span>&nbsp;<?php echo lang('crumb_worker');?>
                </a>
            </li>
            <li class="<?php echo ($nav_module_sub_current == 'machine') ? 'active' : ''; ?>">
                <a href="<?php echo create_url('sys/machine') . "?module={$nav_module_current}"; ?>">
                    <span class="glyphicon glyphicon-compressed"></span>&nbsp;<?php echo lang('crumb_machine');?>
                </a>
            </li>
            <li class="<?php echo ($nav_module_sub_current == 'activity') ? 'active' : ''; ?>">
                <a href="<?php echo create_url('sys/activity') . "?module={$nav_module_current}"; ?>">
                    <span class="glyphicon glyphicon-retweet"></span>&nbsp;<?php echo lang('crumb_activity');?>
                </a>
            </li>
            <li class="<?php echo ($nav_module_sub_current == 'addon') ? 'active' : ''; ?>">
                <a href="<?php echo create_url('sys/addon') . "?module={$nav_module_current}"; ?>">
                    <span class="glyphicon glyphicon-magnet"></span>&nbsp;<?php echo lang('crumb_addon');?>
                </a>
            </li>
            <li class="<?php echo ($nav_module_sub_current == 'module') ? 'active' : ''; ?>">
                <a href="<?php echo create_url('sys/module/setting') . "?module={$nav_module_current}"; ?>">
                    <span class="glyphicon glyphicon-wrench"></span>&nbsp;<?php echo lang('lbl_module_setting');?>
                </a>
            </li>
        <?php endif; ?>
    <?php elseif ($current_module_type == MODULE_TYPE_FASTTASK ) : ?>
        <li class="<?php echo ($nav_module_sub_current == 'fasttask') ? 'active' : ''; ?>" style="margin-left: 0px;">
            <a href="<?php echo create_url('fasttask/fasttask'). "?module={$nav_module_current}"; ?>">
                <span class="glyphicon glyphicon-screenshot"></span>&nbsp;<?php echo lang('lbl_map_tracker');?>
            </a>
        </li>
        <li class="<?php echo ($nav_module_sub_current == 'cost_center') ? 'active' : ''; ?>" style="margin-left: 0px;">
            <a href="<?php echo create_url('fasttask/cost_center'). "?module={$nav_module_current}"; ?>">
                <span class="glyphicon glyphicon-usd"></span>&nbsp;Cost center
            </a>
        </li>
        <li class="<?php echo ($nav_module_sub_current == 'freetext') ? 'active' : ''; ?>" style="margin-left: 0px;">
            <a href="<?php echo create_url('fasttask/freetext'). "?module={$nav_module_current}"; ?>">
                <span class="glyphicon glyphicon-font"></span>&nbsp;Free text
            </a>
        </li>
        <li class="<?php echo ($nav_module_sub_current == 'superior') ? 'active' : ''; ?>" style="margin-left: 0px;">
            <a href="<?php echo create_url('fasttask/superior'). "?module={$nav_module_current}"; ?>">
                <span class="glyphicon glyphicon-user"></span>&nbsp;<?php echo lang('crumb_superior'); ?>
            </a>
        </li>
        <li class="<?php echo ($nav_module_sub_current == 'employee_group') ? 'active' : ''; ?>" style="margin-left: 0px;">
            <a href="<?php echo create_url('fasttask/employee_group'). "?module={$nav_module_current}"; ?>">
                <span class="glyphicon glyphicon-user"></span>&nbsp;Employee group
            </a>
        </li>
        <li class="<?php echo ($nav_module_sub_current == 'object') ? 'active' : ''; ?>" style="margin-left: 0px;">
            <a href="<?php echo create_url('fasttask/object'). "?module={$nav_module_current}"; ?>">
                <span class="glyphicon glyphicon-asterisk"></span>&nbsp;<?php echo lang('crumb_object');?>
            </a>
        </li>
        <li class="<?php echo ($nav_module_sub_current == 'object_place') ? 'active' : ''; ?>" style="margin-left: 0px;">
            <a href="<?php echo create_url('fasttask/object_place'). "?module={$nav_module_current}"; ?>">
                <span class="glyphicon glyphicon-asterisk"></span>&nbsp;<?php echo lang('crumb_object_place');?>
            </a>
        </li>
    <?php elseif ($current_module_type == MODULE_TYPE_EMERGENCY_SERVICE) : ?>
        <li class="<?php echo ($nav_module_sub_current == 'scheduler') ? 'active' : ''; ?>" style="margin-left: 0px;">
            <a href="<?php echo create_url('emergency_service/scheduler'). "?module={$nav_module_current}"; ?>">
                <span class="glyphicon glyphicon-calendar"></span>&nbsp;<?php echo lang('lbl_module_scheduler'); ?>
            </a>
        </li>
        <li class="<?php echo ($nav_module_sub_current == 'group') ? 'active' : ''; ?>" style="margin-left: 0px;">
            <a href="<?php echo create_url('emergency_service/group'). "?module={$nav_module_current}"; ?>">
                <span class="glyphicon glyphicon-th-list"></span>&nbsp;<?php echo lang('crumb_group'); ?>
            </a>
        </li>
    <?php else: ?>
    <?php endif; ?>
</ul>
<?php endif; ?>
