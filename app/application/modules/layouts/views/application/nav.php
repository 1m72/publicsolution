<nav class="navbar navbar-default bs-docs-nav" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-bootsnipp-collapse">
                <span class="sr-only"><?php echo lang('lbl_toggle_navigation'); ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo create_url('home'); ?>"><?php echo lang('lbl_public_solution'); ?></a>
        </div>
        <div class="navbar-header-breadcrumb"><?php echo $this->breadcrumb->output(); ?></div>

        <div class="collapse navbar-collapse navbar-bootsnipp-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Notification -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-globe"></span> <?php echo lang('nav_notification'); ?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="<?php echo create_url('sys/notification/post_service'); ?>">
                                <span class="glyphicon glyphicon-retweet"></span>&nbsp;
                                <?php echo lang('profile_post_service'); ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <!--// Notification -->

                <?php $nav_modules = config_item('nav_modules');
                if (is_array($nav_modules) AND !empty($nav_modules) AND false): ?>
                <li class="dropdown">
                    <a href="<?php echo create_url('sys/user'); ?>" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-list"></i>
                        <?php echo lang('crumb_modules');?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <?php foreach ($nav_modules as $key => $value) : ?>
                        <li><a href="<?php echo create_url($key) . (isset($value['query_string']) ? ('?' . http_build_query($value['query_string'])) : ''); ?>"><span class="<?php echo $value['icon'];?>"></span>&nbsp;<?php echo lang($value['label']);?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <?php endif; ?>

                <?php
                $nav_components = config_item('nav_components');
                if (is_array($nav_components) AND !empty($nav_components) AND false): ?>
                <li class="dropdown">
                    <a href="<?php echo create_url('sys/user'); ?>" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-book"></i>
                        <?php echo lang('crumb_components');?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <?php foreach ($nav_components as $key => $value) : ?>
                        <li><a href="<?php echo create_url($key) . (isset($value['query_string']) ? ('?' . http_build_query($value['query_string'])) : ''); ?>"><span class="<?php echo $value['icon'];?>"></span>&nbsp;<?php echo lang($value['label']);?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <?php endif; ?>

                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                        <img style="vertical-align: baseline;" src="<?php echo base_url('assets/images/blank.gif'); ?>" class="flag flag-<?php echo (current_language(FALSE) == 'en') ? 'us' : current_language(FALSE); ?>" />&nbsp;
                        <?php echo current_language() ? current_language() : lang('lbl_language'); ?>&nbsp;
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <?php foreach (list_language() as $key => $value) :
                        if (current_language(FALSE) != $key):
                        ?>
                        <li>
                            <a href="<?php echo current_url_with_params(array('language' => $key)); ?>">
                                <img style="vertical-align: baseline;" src="<?php echo base_url('assets/images/blank.gif'); ?>" class="flag flag-<?php echo ($key == 'en') ? 'us' : $key; ?>" />
                                <?php echo $value;?>
                            </a>
                        </li>
                        <?php
                        endif;
                        endforeach; ?>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-user"></span>
                        <?php echo session_login(false)->username; ?>&nbsp;
                        <b class="caret"></b>
                    </a>
                    <?php if (is_allow()) : ?>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo create_url('sys/user/profile'); ?>" data-toggle2="modal" data-target="#modal_profile" data-backdrop="static">
                                <span class="glyphicon glyphicon-wrench"></span>&nbsp;
                                <?php echo lang('lbl_profile');?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo create_url('sys/user/profile/change-password'); ?>" data-toggle2="modal" data-target="#modal_profile" data-backdrop="static">
                                <span class="glyphicon glyphicon-lock"></span>&nbsp;
                                <?php echo lang('profile_change_password');?>
                            </a>
                        </li>
                        <?php if (0) : ?>
                        <li role="presentation" class="divider"></li>
                        <li>
                            <div style="padding: 3px 20px; color: black;">
                                <span><?php echo lang('profile_timezone'); ?> : <?php echo isset(session_login(false)->timezone) ? session_login(false)->timezone : ''; ?></span>
                            </div>
                        </li>
                        <?php endif; ?>
                    </ul>
                    <?php endif; ?>
                </li>
                <li>
                    <a href="<?php echo create_url('logout'); ?>">
                        <span class="glyphicon glyphicon-log-out"></span>&nbsp;
                        <?php echo lang('lbl_logout'); ?>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="modal fade" id="modal_profile"></div>