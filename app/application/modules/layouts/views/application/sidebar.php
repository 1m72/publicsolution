<div class="list-group" id="sidebar">
    <div class="panel panel-default" style="min-height: 250px; margin-bottom: 0px; border-bottom: 0px;">
        <div id="menu_programme_heute" class="panel-collapse in">
            <div class="panel-body">
                <?php if (isset($nav_modules) AND is_array($nav_modules) AND !empty($nav_modules) ): ?>
                <?php
                $module_settings  = config_item('module_settings');
                foreach ($nav_modules as $key => $value) :
                $value_id    = isset($value->id) ? $value->id : '';
                $value_title = isset($value->title) ? $value->title : '';
                $value_type  = isset($value->type) ? $value->type : '';
                $value_icon  = isset($module_settings[$value_type]['icon']) ? $module_settings[$value_type]['icon'] : '';
                if ($value_type == MODULE_TYPE_EMERGENCY_SERVICE) {
                    $value_link  = create_url('emergency_service/scheduler') . '?module=' . $value_id;
                } else {
                    $value_link  = create_url('sys/visualization') . '?module=' . $value_id;
                }
                ?>
                <div>
                    <i class="glyphicon glyphicon-plus"></i>
                    <a href="<?php echo $value_link;; ?>" title="<?php echo $value_title; ?>"><?php echo $value_title;?></a>
                </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div id="menu_aufgaben" class="panel-collapse collapse">
            <div class="panel-body">
                <?php foreach (config_item('nav_components') as $key => $value) :
                $flag = TRUE;
                if ( in_array($key, array('sys/city', 'sys/user','sys/user_group')) AND ( (session_login() == FALSE) OR (session_login(FALSE)->id != 1) ) ) {
                    $flag = FALSE;
                }
                if ($flag) : ?>
                <div>
                    <span class="glyphicon glyphicon-plus"></span>&nbsp;
                    <a href="<?php echo create_url($key); ?>"><?php echo lang($value['label']);?></a>
                </div>
                <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
        <div id="menu_posteingang_40" class="panel-collapse collapse">
            <div class="panel-body">
                <div>
                    <i class="glyphicon glyphicon-plus"></i>
                    <a href="#">Menu Item</a>
                </div>
            </div>
        </div>
        <div id="menu_einrichtung" class="panel-collapse collapse">
            <div class="panel-body">
                <div>
                    <i class="glyphicon glyphicon-plus"></i>
                    <a href="#">Menu Item</a>
                </div>
            </div>
        </div>
        <div id="menu_communication" class="panel-collapse collapse">
            <div class="panel-body">
                <div>
                    <i class="glyphicon glyphicon-plus"></i>
                    <a href="#">Menu Item</a>
                </div>
            </div>
        </div>
        <div id="menu_6" class="panel-collapse collapse">
            <div class="panel-body">
                <div>
                    <i class="glyphicon glyphicon-plus"></i>
                    <a href="#">Menu Item</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#menu_programme_heute" data-parent="#sidebar" class="sidebar_menu_item list-group-item active"><?php echo $this->lang->line('lbl_programme_heute');?></a>
    <a href="#menu_aufgaben" data-parent="#sidebar" class="sidebar_menu_item list-group-item"><?php echo $this->lang->line('lbl_aufgaben');?></a>
    <a href="#menu_posteingang_40" data-parent="#sidebar" class="sidebar_menu_item list-group-item"><?php echo $this->lang->line('lbl_posteingang_40');?></a>
    <a href="#menu_einrichtung" data-parent="#sidebar" class="sidebar_menu_item list-group-item"><?php echo $this->lang->line('lbl_einrichtung');?></a>
    <a href="#menu_communication" data-parent="#sidebar" class="sidebar_menu_item list-group-item"><?php echo $this->lang->line('lbl_communication');?></a>
</div>