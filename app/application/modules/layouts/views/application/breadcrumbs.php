<div class="col-xs-12 col-md-12">
    <span class="pull-left">
        <?php echo $this->breadcrumb->output(); ?>
    </span>
    <span  class="pull-right dropdown" style="padding-top: 8px;">
        <?php
            $router = $this->router->fetch_module() . '/' . $this->router->fetch_class() . '/' . $this->router->fetch_method();
            $modules_include = array(
                'sys/user_group/index',
                'sys/user/index',
                'sys/city/index',
                'sys/company/index',
                'sys/machine/index',
                'sys/activity/index',
                'sys/addon/index',
                'sys/nfc/index',
                'sys/device/index',
                'sys/object/index',
                'sys/worker/index',
            );

        if ( in_array($router, $modules_include) ) : ?>
        <a href="<?php echo create_url('sys/user'); ?>" class="dropdown-toggle" data-toggle="dropdown">
            <i class="glyphicon glyphicon-th"></i>
            <?php echo lang('crumb_modules');?>
        </a>
        <ul class="dropdown-menu">
            <?php foreach (config_item('nav_modules') as $key => $value) :
            $flag = TRUE;
            if ( in_array($key, array('sys/city', 'sys/user','sys/user_group')) AND ( (session_login() == FALSE) OR (session_login(FALSE)->id != 1) ) ) {
                $flag = FALSE;
            }
            if ($flag) : ?>
            <li><a href="<?php echo create_url($key); ?>"><span class="<?php echo $value['icon'];?>"></span>&nbsp;<?php echo lang($value['label']);?></a></li>
            <?php endif; ?>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>
    </span>
</div>