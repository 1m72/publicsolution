<head>
    <title><?php echo lang('lbl_public_solution'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if (0) : ?>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
    <?php endif; ?>
    <meta http-equiv="Content-type" content="text/html; charset=<?php echo (current_language(FALSE) == 'de') ? 'utf-8' : 'utf-8'; ?>" />
    <meta name="language" content="<?php echo current_language(FALSE); ?>" />
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(ASSET_BOOTSTRAP_CSS . 'bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(ASSET_KENDO_CSS . 'kendo.common.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(ASSET_KENDO_CSS . 'kendo.bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(ASSET_CSS . 'flags.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(ASSET_CSS . 'app.css'); ?>">
    <script type="text/javascript">
        var _root_url = '<?php echo base_url();?>';
        if(document.namespaces == null){document.createElement('namespaces'); }
        app_env = '<?php echo ENVIRONMENT; ?>';
    </script>
    <script type="text/javascript" src="<?php echo base_url(ASSET_JS . 'jquery-1.11.0.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url(ASSET_BOOTSTRAP_JS . 'bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url(ASSET_KENDO_JS . 'kendo.web.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url(ASSET_JS . 'app.js'); ?>"></script>
    <?php echo isset($global_assets) ? $global_assets : ''; ?>
</head>