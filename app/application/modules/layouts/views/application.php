<!DOCTYPE html>
<html>
    <?php echo $this->load->view('application/header', array(), true); ?>
    <body>
        <?php echo $this->load->view('application/nav', TRUE); ?>
        <div id="wrap">
            <div class="container">
                <div class="row"><?php echo $yield; ?></div>
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="text-muted credit">
                            <span><?php echo lang('language_copyright').' '.lang('lbl_public_solution'); ?> </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </body>
</html>