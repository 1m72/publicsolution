<!DOCTYPE html>
<html>
    <?php echo $this->load->view('emergency_service/header', array(), true); ?>
    <body>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(ASSET_CSS . 'module.css'); ?>">
        <?php echo $this->load->view('application/nav', array(), true); ?>
        <div id="wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 no-padding">
                        <div class="tabbable" id="module_tab">
                            <?php echo $this->load->view('module/menu', array(), true); ?>
                            <div class="tab-content tab-content-wrapper">
                                <div class="tab-pane active" id="module_container">
                                    <?php echo $this->load->view('module/sub_menu', array(), true); ?>
                                    <?php echo $yield; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="text-muted credit">
                            <span><?php echo lang('language_copyright').' '.lang('lbl_public_solution'); ?> </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </body>
</html>