<?php
class Timezone extends MX_Controller {
    function index() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        echo 'Current timezone: ' . date_default_timezone_get() . '<br />';
        $time = time();
        $gmt  = local_to_gmt($time);
        echo date('d-m-Y H:i:s', $time) . " ---> Current<br />";
        echo date('d-m-Y H:i:s', gmt_to_local($gmt, 'UTC')) . " ---> UTC <br />";
        echo date('d-m-Y H:i:s', gmt_to_local($gmt, 'UP1')) . " ---> Europe<br />";
    }
}