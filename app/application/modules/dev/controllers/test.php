<?php
class Test extends MX_Controller {
    function video_player() {
        $this->load->view('test/video_player');
    }

    function service_encode() {
        // $txt = 'Das ist der Text des Auftrages%0D%0AZeilenumbrüche,Tabs und Umlaute/Sonderzeichen sind als Prozent+HexWert kodiert z.B.%E4%F6%FC für ae, oe,ue %0D%0A3. Zeile';
        // var_dump(mb_convert_encoding($txt, "ISO-8859-1", "UTF-8"));

        $txt = 'Das ist der Text des Auftrages%0D%0AZeilenumbrüche,Tabs und Umlaute/Sonderzeichen sind als Prozent+HexWert kodiert z.B.%E4%F6%FC für ae, oe,ue %0D%0A3. Zeile';
        var_dump(html_entity_decode($this->correct_encoding($txt)));
        // var_dump($this->decode_entities($txt));

        // 0A
        var_dump(ord('0D'));
        var_dump(ord('FA'));
        var_dump(chr(ord('FA')));
        die;
    }

    function correct_encoding($text) {
        $current_encoding = mb_detect_encoding($text, 'auto');
        $text = iconv($current_encoding, 'UTF-8', $text);
        return $text;
    }

    function minify() {
        $data = array();
        $this->load->library('minify');
        $this->load->library('minify');
        $this->minify->css(array('app.css', 'flags.css'));
        $this->minify->js(array('jquery-1.11.0.min.js', 'openlayer.js', 'app.js', 'functions.js'));
        $this->load->view('dev/test/minify', $data);
    }
}