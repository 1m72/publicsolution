<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Scheduler Controller
 * @author le.nhung@kloon.vn
 * @copyright 2014-05-17
 */
class Scheduler extends MY_Controller {
    function index() {
        $this->layout = false;
        $this->view   = 'scheduler/index';
    }

    function read() {
        write_log(json_encode($_REQUEST), 'debug');
        $result = array(
            array(
                'id'                     => 1,
                'title'                  => 'Helo world 1',
                'time_start'             => '2014-08-20 17:25:00',
                'time_end'               => '2014-08-24 17:25:00',
                'IsAllDay'               => false,
                // 'OwnerID'             => 1,
                // 'RecurrenceException' => '',
                // 'RecurrenceRule'      => '',
                // 'RecurrenceID'        => 1,
                // 'Description'         => 'aaaaaaaa'
            ),
            array(
                'id'                     => 2,
                'title'                  => 'Helo world 2',
                'time_start'             => '2014-08-14 10:25:00',
                'time_end'               => '2014-08-14 17:25:00',
                'IsAllDay'               => false,
                // 'OwnerID'             => 1,
                // 'RecurrenceException' => '',
                // 'RecurrenceRule'      => '',
                // 'RecurrenceID'        => 1,
                // 'Description'         => 'aaaaaaaa'
            ),
        );
        $callback = $this->input->get('callback');
        echo $callback . '(' . json_encode($result) . ')';
    }
}
