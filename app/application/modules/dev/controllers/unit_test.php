<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('UNIT_TEST_RESULT_OK', 'OK');
define('UNIT_TEST_RESULT_FAIL', 'FAIL');

/**
 * Unit Test
 *
 * ---- Winterdienst
 * ['update_winter_service_task']  = "mobile/synchronize/update_winter_service_task";
 * ['upload_tour_data']            = "mobile/synchronize/upload_tour_data";
 * ['upload_image_record']         = "mobile/synchronize/upload_image_record";
 * ['register_push_nofitication']  = "mobile/devices/register_push_nofitication";
 * ['push_winter_service_data2']   = "mobile/devices/push_winter_service_data";
 * ['register_push_nofitication2'] = "mobile/register_push/register_push_nofitication";
 * ['push_winter_service_data']    = "mobile/push_winter_service/push_winter_service_data";
 * ---- Fasttask
 * ['initialize_place']            = "mobile/fasttask_object/initialize_place";
 * ['synchronize_place']           = "mobile/fasttask_object/synchronize_place";
 * ['initialize_object']           = "mobile/fasttask_object/initialize_object_location";
 * ['synchronize_object']          = "mobile/fasttask_object/synchronize_object_location";
 * ['create_fast_task_xml']        = "mobile/fasttask_create_fast_task/create_fast_task";
 * ['upload_fast_task_binary']     = "mobile/fasttask_create_fast_task/upload_fast_task";
 */

class Unit_test extends MY_Controller {
    private $_id_employee = 1;
    private $_id_city     = 1;
    private $api_path     = '';

    public function __construct(){
        parent::__construct();
        $this->load->library('unit_test');

        # config server
        $this->rest->initialize(array('server' => config_item('api_web')));
    }

    function index($id_city = FALSE){
        if($id_city != FALSE) {
            $this->_id_city = $id_city;
        }

        $str1 = '
        <div>
            {rows}
                <div style="margin-bottom: 15px; border-bottom: 1px solid #DDD;">
                    <div>{item}</div>
                    <div>{result}</div>
                </div>
            {/rows}
        </div>';

        $str = '
            <table border="0" cellpadding="5" cellspacing="0">
                {rows}
                    <tr style="border-bottom: 1px solid #CCC;">
                    <td style="width: 150px;">{item}</td>
                    <td>{result}</td>
                    </tr>
                {/rows}
            </table><hr />';
        $this->unit->set_template($str);

        # Mobile api
        $url = 'initialize_material';
        $param = array('id_city' => $this->_id_city);
        $result = $this->test_unit_api($url,$param,'xml');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Initialize Material',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        #['validate_code']               = "mobile/auth/validate_AccessCode";
        $url = 'validate_code';
        $param = array('access_code' => '123456', 'device_id' => 'fc089d07356ca80');
        $result = $this->test_unit_api($url,$param,'xml','post');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Validate Code',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        #['login']                        = "mobile/auth/login";

        #['settings']                    = "mobile/city/setting";
        $url = 'settings';
        $param = array('id_city' => $this->_id_city);
        $result = $this->test_unit_api($url,$param,'xml');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Settings',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        #['initialize_machine']          = "mobile/machine/initialize";
        $url = 'initialize_machine';
        $param = array('id_city' => $this->_id_city);
        $result = $this->test_unit_api($url,$param,'xml');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Initialize Machine',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        #['synchronize_machine']         = "mobile/machine/synchronize_machine";
        $url = 'synchronize_machine';
        $param = array('id_city' => $this->_id_city,'lastupdate' => '2013-09-12 00:00:00');
        $result = $this->test_unit_api($url,$param,'xml');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Synchronize Machine',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        #['initialize_employee']         = "mobile/employee/initialize_employee";
        $url = 'initialize_employee';
        $param = array('id_city' => $this->_id_city);
        $result = $this->test_unit_api($url,$param,'xml');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Initialize Employee',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        #['synchronize_employee']        = "mobile/employee/synchronize_employee";
        $url = 'synchronize_employee';
        $param = array('id_city' => $this->_id_city,'lastupdate' => '2013-09-12 00:00:00');
        $result = $this->test_unit_api($url,$param,'xml');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Synchronize Employee',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        #['initialize_activity']         = "mobile/activity/initialize_activity";
        $url = 'initialize_activity';
        $param = array('id_city' => $this->_id_city);
        $result = $this->test_unit_api($url,$param,'xml');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Initialize Activity',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        #['synchronize_activity']        = "mobile/activity/synchronize_activity";
        $url = 'synchronize_activity';
        $param = array('id_city' => $this->_id_city,'lastupdate' => '2013-09-12 00:00:00');
        $result = $this->test_unit_api($url,$param,'xml');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Synchronize Activity',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        #['initialize_material']         = "mobile/material/initialize_material";
        $url = 'initialize_material';
        $param = array('id_city' => $this->_id_city);
        $result = $this->test_unit_api($url,$param,'xml');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Initialize Material',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        #['synchronize_material']        = "mobile/material/synchronize_material";
        $url = 'synchronize_material';
        $param = array('id_city' => $this->_id_city,'lastupdate' => '2013-09-12 00:00:00');
        $result = $this->test_unit_api($url,$param,'xml');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Synchronize Material',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        #['initialize_place']            = "mobile/fasttask_object/initialize_place";
        $url = 'initialize_place';
        $param = array('id_city' => $this->_id_city);
        $result = $this->test_unit_api($url,$param,'xml');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Initialize Place',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        #['synchronize_place']           = "mobile/fasttask_object/synchronize_place";
        $url = 'synchronize_place';
        $param = array('id_city' => $this->_id_city,'lastupdate' => '2013-09-12 00:00:00');
        $result = $this->test_unit_api($url,$param,'xml');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Synchronize Place',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        #['initialize_object']           = "mobile/fasttask_object/initialize_object_location";
        $url = 'initialize_object';
        $param = array('id_city' => $this->_id_city,'range' => 5);
        $result = $this->test_unit_api($url,$param,'xml');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Initialize Object',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        #['synchronize_object']          = "mobile/fasttask_object/synchronize_object_location";
        $url = 'synchronize_object';
        $param = array('id_city' => $this->_id_city,'range' => 5,'lastupdate' => '2013-09-12 00:00:00');
        $result = $this->test_unit_api($url,$param,'xml');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Mobile Api - Synchronize Object',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );


        #[sync data]
        #['upload_tour_data']            = "mobile/synchronize/upload_tour_data";
        $url = 'upload_tour_data';
        $path_forder = '../api/sync/winter_activities/';

        if ( is_dir($path_forder) ) {
            $return = array('status' => UNIT_TEST_RESULT_OK);

            if ( ! is_writable($path_forder) ) {
                $return = array('status' => UNIT_TEST_RESULT_FAIL, 'data' => "Dir {$path_forder} is not writeable");
            }
        } else {
            $return = array('status' => UNIT_TEST_RESULT_FAIL, 'data' => "Dir {$path_forder} is not exist");
        }
        $this->unit->run($return['status'], UNIT_TEST_RESULT_OK, 'Web Api - Upload Tour Data', json_encode($return['data']));

        #['upload_image_record']         = "mobile/synchronize/upload_image_record";
        $url = 'upload_image_record';
        $path_forder = '../api/uploads/' . $this->_id_city . '/';
        if ( is_dir($path_forder) ) {
            $return = array('status' => UNIT_TEST_RESULT_OK);

            if ( ! is_writable($path_forder) ) {
                $return = array('status' => UNIT_TEST_RESULT_FAIL, 'data' => "Dir {$path_forder} is not writeable");
            }
        } else {
            $return = array('status' => UNIT_TEST_RESULT_FAIL, 'data' => "Dir {$path_forder} is not exist");
        }
        $this->unit->run($return['status'], UNIT_TEST_RESULT_OK, 'Web Api - Upload Image Record', json_encode($return['data']));

        # App api
        $param = array('id_city' => $this->_id_city);
        $url = 'web/activity/index';
        $result = $this->test_unit_api($url,$param,'json');
        $this->unit->run(
            $result['status'],
            UNIT_TEST_RESULT_OK,
            'Web Api - Activity',
            isset($result['status']) ? json_encode($result['data']) : 'data intval.'
        );

        # Custom result display
        $this->unit->set_test_items(array('test_name', 'result', 'notes'));

        # Result
        echo $this->unit->report();
    }

    function test_unit_api($url = '', $param = NULL,$format = 'xml',$type = 'get'){
        $api_results = NULL;
        switch ($type) {
            case 'get':
                $api_results = $this->rest->get($url, $param, $format);
                break;
            case 'post':
                $api_results = $this->rest->post($url, $param, $format);
                break;
            case 'put':
                $api_results = $this->rest->put($url, $param, $format);
                break;
            case 'delete':
                $api_results = $this->rest->delete($url, $param, $format);
                break;
            default:
                break;
        }

        if(isset($api_results) && !empty($api_results)){
            if($format == 'json'){
                $api_results = json_decode(json_encode($api_results),true);
            }

            if(isset($api_results['status']) && !empty($api_results['status'])){
                $return = array('status' => UNIT_TEST_RESULT_OK, 'data' => $api_results);
            } else {
                $return = array('status' => UNIT_TEST_RESULT_FAIL, 'data' => $api_results);
            }
        } else {
            $return = array('status' => UNIT_TEST_RESULT_FAIL, 'data' => 'Data Empty');
        }

        # Return
        return $return;
    }

    function check_permission($path_forder){
        $chmod  = NULL;
        try{
            $fileperms = fileperms($path_forder);
            $sprintf = sprintf('%o', $fileperms);
            $chmod = substr($sprintf,-4);
        }
        catch(Exception $ex){}
        if( isset($chmod) AND !empty($chmod) ){
            $return = array('status' => UNIT_TEST_RESULT_OK, 'data' => $chmod);
        } else {
            $return = array('status' => UNIT_TEST_RESULT_FAIL, 'data' => 'Data Empty');
        }
        return $return;
    }

    function test(){
        $item = array(
            'id_city'       => 1,
            'type'          => 'activity',
            'data'          => 'fas'
        );
        echo json_encode($item);
    }
}