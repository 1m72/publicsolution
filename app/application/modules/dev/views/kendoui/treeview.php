<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/kendoui/styles/kendo.common.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/kendoui/styles/kendo.bootstrap.min.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/kendoui/js/kendo.web.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/select2/select2.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/select2/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/functions.js"></script>
    <script type="text/javascript">
        var src, des, my, treeview;
        var modules = new Array();
        var activities = $.parseJSON('<?php echo json_encode($activities); ?>');
        $(function(){
            $('.select2').select2();
            $('#module').change(function(e) {
                select_items = $('#module').select2('data');
                treeview = $('#panel-left').data("kendoTreeView");

                added = e.added;
                if (added != null) {
                    console.log(modules[added.id]);
                    if (modules[added.id] == undefined) {
                        modules[added.id] = Array();
                        result = treeview.append([
                            {text: added.text}
                        ]);
                        result.attr('_index_module', added.id);
                        console.log(result);
                    }
                }

                removed = e.removed;
                if ( (removed != null) && (modules[removed.id] != undefined) ) {
                    modules[removed.id] = undefined;
                    uid = $('li[_index_module="'+removed.id+'"]').attr('data-uid');
                    node = treeview.findByUid(uid);
                    treeview.remove(node);
                }

                console.log(modules);
            });

            $("#panel-left").kendoTreeView();
            $("#panel-right").kendoTreeView({
                dragAndDrop: true,
                dataSource: [],
                drop: function(e){
                    e.preventDefault();
                    src = e.sourceNode;
                    des = e.destinationNode;
                    my = des;
                    treeview = $('#panel-left').data('kendoTreeView');
                    checker = false;
                    var target = treeview.findByUid($(des).attr('data-uid'));
                    if (e.dropPosition == 'over') {
                        des_item = $(des);
                        if (des_item.parent('ul').hasClass('k-treeview-lines')) {
                            checker = true;
                        }
                    }

                    id_module = $(des).attr('_index_module');
                    id_activity = $(src).attr('_index_activity');
                    exist = $(des).find('li[_index_activity="'+id_activity+'"]').length;
                    if (checker && (exist == 0)) {
                        result = treeview.append({text: $(src).text()}, target);
                        $(result.selector).attr('_index_activity', id_activity);
                        if (modules[id_module] == undefined) {
                            modules[id_module] = new Array();
                        }

                        modules[id_module].push(id_activity);
                    } else {
                        e.setValid(false);
                        e.preventDefault();
                    }
                }
            });

            treeview = $('#panel-right').data('kendoTreeView');
            for (var i = 0; i < activities.length; i++) {
                result = treeview.append({text: activities[i].text});
                result.attr('_index_activity', activities[i].key);
            };
        });
    </script>
</head>
<body>
    <div style="width: 420px; margin: 20px auto; overflow: auto;">
        <select name="module" id="module" multiple="multiple" class="select2" style="width: 400px; margin-bottom: 10px;">
            <option value="1">Winterdienst</option>
            <option value="2">Module 2</option>
            <option value="3">Module 3</option>
            <option value="4">Module 4</option>
            <option value="5">Module 5</option>
            <option value="6">Module 6</option>
            <option value="7">Module 7</option>
        </select>

        <div id="panel-left" style="float: left; min-height: 200px; width: 200px; border: 1px solid red; margin-right: 5px; "></div>
        <div id="panel-right" style="float: right; min-height: 200px; width: 200px; border: 1px solid red; "></div>
    </div>
</body>
</html>