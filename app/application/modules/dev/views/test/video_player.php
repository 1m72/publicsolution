<!DOCTYPE html>
<html>
    <head>
        <title>Public Solution Apps</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="language" content="<?php echo current_language(FALSE); ?>" />
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/projekktor/themes/maccaco/projekktor.style.css'); ?>">
        <script type="text/javascript" src="<?php echo base_url(ASSET_JS . 'jquery-1.10.2.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/projekktor/projekktor-1.3.00.min.js'); ?>"></script>
        <script type="text/javascript">
            $(function(){
                projekktor('video, audio', {
                    playerFlashMP4: '<?php echo assets('js/projekktor/swf/Jarisplayer/jarisplayer.swf'); ?>',
                    playerFlashMP3: '<?php echo assets('js/projekktor/swf/Jarisplayer/jarisplayer.swf'); ?>f'
                });
            });
        </script>
    </head>
    <body>
        <div id="player" style="border: 1px dashed red; height: 360px; width: 470px;">
            <video class="projekktor" width="470" height="360" poster="poster.jpg">
                <source src="<?php echo base_url(); ?>assets/video/1a.webm" type="video/webm"></source>
            </video>
        </div>
    </body>
</html>