<?php
 $form_link = create_url("sys/scheduler/edit");
?>
<div id="example" class="k-content">
    <div id="people">
        <input checked type="checkbox" id="alex" value="1">
        <input checked type="checkbox" id="bob" value="2">
        <input type="checkbox" id="charlie" value="3">
    </div>
    <div id="scheduler"></div>
</div>
<style type="">
.k-edit-buttons {
    display: none;
}
</style>
<div id="editor" style=" display:none">
    <div id="frm_scheduler_detail_wraper">
        <form class="form-horizontal" id="frm_scheduler_detail" action="<?php echo $form_link;?>" method="post">

            <div class="form-group">
                <label class="col-md-4 control-label" for="titile">Titile</label>
                <div class="col-md-5">
                    <input id="first_name" name="first_name" value="Titile" type="text" placeholder="titile" class="form-control input-md">
                    <div class="help-block ps_err">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="start">Start</label>
                <div class="col-md-5">
                    <input id="start" name="start" value=""  style="width: 250px;" data-role="datetimepicker">
                    <div class="help-block ps_err">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="end">End</label>
                <div class="col-md-5">
                    <input id="end" name="end" value=""  style="width: 250px;" data-role="datetimepicker">
                    <div class="help-block ps_err">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="IsAllDay">All day event</label>
                <div class="col-md-5">
                    <input id="IsAllDay" name="IsAllDay" value="" type="checkbox">
                    <div class="help-block ps_err">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="save"></label>
                <div class="col-md-8">
                    <button type="submit" class="btn btn-primary" name = "save" id="save"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<?php echo lang('btn_save_change');?></button>
                    <a class="btn btn-default k-scheduler-cancel" id ="ccc" name ="ccc"><?php echo lang('btn_cancel');?></a>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
$(function() {
    $("#scheduler").kendoScheduler({
        date: new Date("2013/6/13"),   //value default for start
        startTime: new Date("2013/6/13 07:00 AM"),
        editable: {
            template: $("#editor").html(),
        },
        messages: {

            save: "Save change",
            cancel: "Cancel",
            destroy:"yes",
            deleteWindowTitle :"Confirm",
        },
        height: 600,
        views: [
            "day",
            { type: "workWeek", selected: true },
            "week",
            "month",
            "agenda"
        ],
        // timezone: "Etc/UTC",
        dataSource: {
            batch: true,
            transport: {
                read: {
                    url:'<?php echo create_url("sys/scheduler/list_scheduler");?>',
                    dataType: "json"
                },
                update: {
                    url: "http://demos.telerik.com/kendo-ui/service/tasks/update",
                    dataType: "json"
                },
                create: {
                    url: "http://demos.telerik.com/kendo-ui/service/tasks/create",
                    dataType: "jsonp"
                },
                destroy: {
                    url: "http://demos.telerik.com/kendo-ui/service/tasks/destroy",
                    dataType: "jsonp"
                },
                parameterMap: function(options, operation) {
                    if (operation !== "read" && options.models) {
                        return {models: kendo.stringify(options.models)};
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { from: "id", type: "number" },
                        title: { from: "title", defaultValue: "dsvđ", validation: { required: true } },
                        start: { type: "datetime", from: "starttime" },
                        end: { type: "datetime", from: "endtime" },
                        startTimezone: { from: "StartTimezone" },
                        endTimezone: { from: "EndTimezone" },
                        description: { from: "des" },
                        recurrenceId: { from: "RecurrenceID" },
                        recurrenceRule: { from: "RecurrenceRule" },
                        recurrenceException: { from: "RecurrenceException" },
                        isAllDay: { type: "boolean", from: "IsAllDay" },
                    }
                }
            },
        },
    });
    $("#people :checkbox").change(function(e) {
        var checked = $.map($("#people :checked"), function(checkbox) {
            return parseInt($(checkbox).val());
        });

        var scheduler = $("#scheduler").data("kendoScheduler");

        scheduler.dataSource.filter({
            operator: function(task) {
                return $.inArray(task.ownerId, checked) >= 0;
            }
        });
    });
    $('body').on('submit', '#frm_scheduler_detail', function(e){
        e.preventDefault();
        alert("sub w");
    });
});
</script>
<style scoped>
.k-nav-current > .k-link span + span {
    max-width: 200px;
    display: inline-block;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    vertical-align: top;
}

#people {
    background: url('../../content/web/scheduler/team-schedule.png') transparent no-repeat;
    height: 115px;
    position: relative;
}
#alex {
    position: absolute;
    left: 630px;
    top: 81px;
}
#bob {
    position: absolute;
    left: 745px;
    top: 81px;
}
#charlie {
    position: absolute;
    left: 860px;
    top: 81px;
}
</style>