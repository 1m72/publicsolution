<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="http://cdn.kendostatic.com/2014.2.716/styles/kendo.common.min.css" rel="stylesheet" />
    <link href="http://cdn.kendostatic.com/2014.2.716/styles/kendo.default.min.css" rel="stylesheet" />
    <script src="http://cdn.kendostatic.com/2014.2.716/js/jquery.min.js"></script>
    <script src="http://cdn.kendostatic.com/2014.2.716/js/kendo.all.min.js"></script>
    <style type="text/css">
    .k-nav-current > .k-link span + span {max-width: 200px; display: inline-block; white-space: nowrap; text-overflow: ellipsis; overflow: hidden; vertical-align: top; }
    #team-schedule {background: url('../content/web/scheduler/team-schedule.png') transparent no-repeat; height: 115px; position: relative; }
    #people {background: url('../content/web/scheduler/scheduler-people.png') no-repeat; width: 345px; height: 115px; position: absolute; right: 0; }
    #alex {position: absolute; left: 4px; top: 81px; }
    #bob {position: absolute; left: 119px; top: 81px; }
    #charlie {position: absolute; left: 234px; top: 81px; }
    </style>
    <script type="text/javascript">
    $(function() {
        var url_read     = '<?php echo base_url('dev/scheduler/read'); ?>';
        var url_read_tmp = url_read;
        $("#scheduler").kendoScheduler({
            height: 800,
            views: ["day", "workWeek", "week", { type: "month", selected: true }, "agenda"],
            dataSource: {
                batch: true,
                transport: {
                    read: {
                        url: function(options){
                            return url_read_tmp;
                        },
                        dataType: "jsonp"
                    },
                    parameterMap: function(options, operation) {
                        if (operation !== "read" && options.models) {
                            return {models: kendo.stringify(options.models)};
                        }
                    }
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            title: { field: "title", defaultValue: "No title", validation: { required: true } },
                            start: { type: "date", field: "time_start" },
                            end: { type: "date", field: "time_end" },
                        }
                    }
                },
            },
            navigate: function(e) {
                // e.preventDefault();
                // console.log(e);
                url_read_tmp = url_read + '?param1=1&param2=2&date=';
                $("#scheduler").data("kendoScheduler").dataSource.read();
                $("#scheduler").data("kendoScheduler").refresh();
            },
            editable: {
                template: kendo.template($("#schedulerTemplate").html())
            },
            edit: function(e){
                // e.preventDefault();
            }
        });
    });
    </script>
</head>
<body>
    <div id="scheduler"></div>
    <!-- CUSTOM EDITOR TEMPLATE -->
  <script type="text/x-kendo-template" id="schedulerTemplate">
    <div id="recurrenceEditor" name="recurrenceRule" data-bind="value: recurrenceRule" />
    <script>
        var options = jQuery("\#scheduler").data("kendoScheduler").options;
        jQuery("\#recurrenceEditor").kendoRecurrenceEditor({
            start: "#=data.start#",
            timezone: options.timezone,
            messages: options.messages
        });
    <\/script>
  </script>
</body>
</html>