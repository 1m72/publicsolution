<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    #----------- LINK HELPER -----------#
    /**
     * Get server base path
     * @return Ambiguous
     * @author Chien Tran <tran.duc.chien@kloon.vn>
     * @created 4 Nov 2013
     */
    if (!function_exists('get_server_path')) {
        function get_server_path(){
            return dirname($_SERVER["DOCUMENT_ROOT"] . $_SERVER['SCRIPT_NAME']).'/';
        }
    }

    /**
     * Get link resources
     * @param  string  $resource  resource
     * @param  boolean $real_path get system real path
     * @param  int     $type      [description]
     * @return string             link to resource
     */
    if (!function_exists('assets')) {
        function assets($resource = '', $type = NULL, $real_path = FALSE){
            $return = '';
            if ($real_path) {
                $root_path = dirname($_SERVER["DOCUMENT_ROOT"] . $_SERVER['SCRIPT_NAME']).'/';
            } else {
                $type = is_null($type) ? ASSET : $type;
                $return = base_url($type.$resource);
            }
            return $return;
        }
    }

    /**
     * Create reserved url
     * @param  string $key key for match url
     * @return string url
     * @author Chien Tran <tran.duc.chien@kloon.vn>
     * @created 4 Nov 2013
     */
    if (!function_exists('create_url')) {
        function create_url($key) {
            $CI =& get_instance();
            $CI->load->config('url_reserved');
            $url_reserved = $CI->config->item('url_reserved');
            return isset($url_reserved[$key]) ? base_url($url_reserved[$key]) : base_url($key);
        }
    }

    /**
     * Get current url with params
     * @param  array  $add_param param add to current url
     * @return string Full url with params
     * @author Chien Tran <tran.duc.chien@kloon.vn>
     * @created 4 Nov 2013
     */
    if (!function_exists('current_url_with_params')) {
        function current_url_with_params($add_param = array(), $remove_param = array()) {
            $CI =& get_instance();
            $CI->load->helper('url');
            $param = $_GET;
            $param = array_merge($param, $add_param);
            if (is_array($remove_param) AND is_array($remove_param)) {
                foreach ($remove_param as $key => $value) {
                    if (isset($param[$value])) {
                        unset($param[$value]);
                    }
                }
            }
            return current_url() . '?' . http_build_query($param);
        }
    }
    #-----------// LINK HELPER -----------#

    /**
     * Get session login
     * @param string $return_boolean
     * @return boolean|Ambigous <NULL, unknown>
     * @created 4 Nov 2013
     */
    if (!function_exists('session_login')) {
        function session_login($return_boolean = true){
            if ($return_boolean){
                return isset($_SESSION[SESSION_AUTH]->id) ? true : false;
            } else {
                return isset($_SESSION[SESSION_AUTH]) ? $_SESSION[SESSION_AUTH] : NULL;
            }
        }
    }

    /**
     * Create / Destroy session login
     * @param NULL|string $user_object
     * @return boolean
     * @author Chien Tran <tran.duc.chien@kloon.vn>
     * @created 4 Nov 2013
     */
    if (!function_exists('session_holder')) {
        function session_holder($user_object = NULL){
            # CI instance
            $CI = get_instance();

            # Global server name
            $server_name = explode('.', $_SERVER["SERVER_NAME"]);
            if (is_array($server_name) && (count($server_name) >= 2)){
                $server_name = '.'.$server_name[count($server_name) - 2].'.'.$server_name[count($server_name) - 1];
            } else {
                $server_name = '/';
            }

            if (!is_null($user_object)){
                # Global cookie
                #$CI->input->set_cookie(array('name' => COOKIE_CROSS, 'value' => $user_object->first_name . '|' .$user_object->last_name . '|' . $user_object->display_name, 'expire' => '2629743', 'domain' => $server_name));

                $_SESSION[SESSION_AUTH] = $user_object;
                return true;
            } else {
                /* Remove cookie */
                #$CI->input->set_cookie(array('name' => COOKIE_AUTH, 'value' => NULL, 'expire' => ''));

                 /* Remove global cookie */
                #$CI->input->set_cookie(array('name' => COOKIE_CROSS, 'value' => NULL, 'domain' => $server_name, 'expire' => ''));

                /* Destroy session */
                $_SESSION[SESSION_AUTH] = NULL;
                unset($_SESSION[SESSION_AUTH]);
                return false;
            }
        }
    }

    if (!function_exists('current_language')) {
        function current_language($return_name = true) {
            $CI =& get_instance();
            $language = $CI->config->item('language');
            if ($return_name) {
                $all_language = list_language();
                if (isset($all_language[$language])) {
                    return $all_language[$language];
                }
                return false;
            } else {
                return $language;
            }
        }
    }

    if (!function_exists('list_language')) {
        function list_language() {
            $language_array = config_item('language_array');
            foreach ($language_array as $key => $value) {
                $language_array[$key] = lang("language_$key");
            }
            return $language_array;
        }
    }

    if (!function_exists('current_city')) {
        function current_city () {
            $session_data = session_login(FALSE);
            return isset($session_data->id_city) ? $session_data->id_city : NULL;
        }
    }

    if (!function_exists('convert_timezone')) {
        /**
         * Convert datetime by timezone
         * @param  int $timestamp   timestampt
         * @param  string $timezone CI timezone identifier
         * @return int              timestamp
         */
        function convert_timezone($timestamp = NULL, $timezone = NULL) {
            if (is_null($timestamp) || is_null($timezone)) {
                log_message('error', 'Timestamp or Timezone is invalid');
                return false;
            }

            $CI =& get_instance();
            $CI->load->helper('date');
            $daylight_saving = TRUE;
            return gmt_to_local($timestamp, $timezone, $daylight_saving);
        }
    }

    if (!function_exists('module_list_array')) {
        /**
         * Convert datetime by timezone
         * @param  int $timestamp   timestampt
         * @param  string $timezone CI timezone identifier
         * @return int              timestamp
         */
        function module_list_array($module = null) {
            $module_list_array = array(
                0 => lang('lbl_select_module'),
                1 => lang('lbl_winterdienst'),
                2 => lang('lbl_street_checking'),
                3 => lang('lbl_street_cleaning'),
            );

            if (is_null($module)) {
                return $module_list_array;
            } else {
                return isset($module_list_array[$module]) ? $module_list_array[$module] : false;
            }
        }
    }

    function delete_cache($uri_string=null) {
        $CI =& get_instance();
        $path = $CI->config->item('cache_path');
        $path = rtrim($path, DIRECTORY_SEPARATOR);

        $cache_path = ($path == '') ? APPPATH.'cache/' : $path;

        $uri =  $CI->config->item('base_url').
                $CI->config->item('index_page').
                $uri_string;

        $cache_path .= md5($uri);

        return unlink($cache_path);
    }

    function global_config($key = false, $value = null) {
        if ($key === false) {
            return $GLOBALS['PUBLIC_SOLUTION'];
        }
        elseif (!is_null($value) AND !is_null($key)) {
            return $GLOBALS['PUBLIC_SOLUTION'][$key] = $value;
        } else {
            return (is_string($key) AND  isset($GLOBALS['PUBLIC_SOLUTION'][$key])) ? $GLOBALS['PUBLIC_SOLUTION'][$key] : null;
        }
    }

    function is_allow() {
        if (session_login(false)->id_user_group != 4) return true;
        return false;
    }

    if (!function_exists('get_value')) {
        function get_value($key = '', $data = array(), $default = false) {
            if (is_array($data) OR is_object($data)) {
                $data = json_decode(json_encode($data), true);
            }
            return isset($data[$key]) ? $data[$key] : $default;
        }
    }

