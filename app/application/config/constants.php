<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',                            'rb');
define('FOPEN_READ_WRITE',                      'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',        'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',   'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',                    'ab');
define('FOPEN_READ_WRITE_CREATE',               'a+b');
define('FOPEN_WRITE_CREATE_STRICT',             'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',        'x+b');

#--- ASSETS
define('ASSET'                                  , 'assets/');
define('ASSET_JS'                               , ASSET.'js/');
define('ASSET_IMG'                              , ASSET.'img/');
define('ASSET_CSS'                              , ASSET.'css/');
define('ASSET_FONT'                             , ASSET.'fonts/');
define('ASSET_KENDO_JS'                         , ASSET.'kendoui/js/');
define('ASSET_KENDO_CSS'                        , ASSET.'kendoui/styles/');
define('ASSET_BOOTSTRAP_JS'                     , ASSET.'v3/js/');
define('ASSET_BOOTSTRAP_CSS'                    , ASSET.'v3/css/');
define('ASSET_BOOTSTRAP_FONT'                   , ASSET.'v3/fonts/');
define('ASSET_TYPE_JS'                          , 1);
define('ASSET_TYPE_CSS'                         , 2);
define('ASSET_TYPE_IMG'                         , 3);
define('ASSET_TYPE_FONT'                        , 4);

#--- REST STATUS
define('REST_STATUS_SUCCESS'                    , 'ok');
define('REST_STATUS_FAIL'                       , 'fail');
define('REST_CODE_OK'                           , 200);
define('REST_CODE_PARAM_ERR'                    , 412);
define('REST_CODE_SERVER_ERR'                   , 500);

#--- FLAG
define('BOOL_YES'                               , 'yes');
define('BOOL_NO'                                , 'no');

#--- COOKIE, SESSION
define('SESSION_AUTH'                           , 'kloon_session_publicsolution');
define('COOKIE_AUTH'                            , 'kloon_cookie_publicsolution');
define('COOKIE_CROSS'                           , 'kloon_gcookie_publicsolution');

#--- INFORMATION TYPE
define('INFO_MESSAGE'                           , 1);
define('INFO_VOICE'                             , 2);
define('INFO_IMAGE'                             , 3);
define('INFO_VIDEO'                             , 4);

#--- KENDOUI
define('GET_CITY'                               , 0);
define('GET_COMPANY'                            , 1);
define('GET_DEVICE'                             , 2);
define('GET_MACHINE'                            , 3);
define('GET_OBJECT'                             , 4);
define('GET_USERS'                              , 5);
define('GET_WORKER'                             , 6);
define('GET_ADDON'                              , 7);
define('GET_ACTIVITY'                           , 8);
define('GET_NFC'                                , 9);
define('GET_USERS_GROUP'                        , 10);
define('GET_MODULE'                             , 11);
define('GET_GROUP'                              , 14);
define('GET_MATERIAL'                           , 15);
define('GET_FREETEXT'                           , 16);
define('GET_COST_CENTER'                        , 17);
define('GET_EMPLOYEE_GOUP'                      , 18);


#--- MODULE TYPE
define('MODULE_TYPE_WINTERDIENST'               , 'winterdienst');
define('MODULE_TYPE_FASTTASK'                   , 'fasttask');
define('MODULE_TYPE_STREETCLEANING'             , 'streetcleaning');
define('MODULE_TYPE_STREETCHECKING'             , 'streetchecking');
define('MODULE_TYPE_EMERGENCY_SERVICE'          , 'emergency_service');

/* End of file constants.php */
/* Location: ./application/config/constants.php */