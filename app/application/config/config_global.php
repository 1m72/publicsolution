<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
# Session
$config['session_auth']    = 'publicsolution.kloon';

# Captcha
$config['tmp_dir']         = 'tmp/';
$config['captcha_session'] = 'kloon.captcha';
$config['captcha_dir']     = 'tmp/captcha/';

$config['language_array']  = array(
    'en' => 'English',
    'de' => 'Germany'
);

# Kendo UI
$config['kendoui']      = 'web/v1/kendoui/index';
$config['date_format']  = 'MM/dd/yyyy';
$config['id_city']      = 1;

# Modules Links
$config['nav_components'] = array(
    'sys/material' => array(
        'icon'  => 'glyphicon glyphicon-tasks',
        'label' => 'lbl_material',
    ),
    'sys/nfc' => array(
        'icon'  => 'glyphicon glyphicon-flash',
        'label' => 'crumb_nfc',
    ),
    'sys/company' => array(
        'icon'  => 'glyphicon glyphicon-hdd',
        'label' => 'crumb_company',
    ),
    'sys/device' => array(
        'icon'  => 'glyphicon glyphicon-inbox',
        'label' => 'crumb_device',
    ),
    'sys/object' => array(
        'icon'  => 'glyphicon glyphicon-link',
        'label' => 'crumb_object',
    ),
    /*'sys/city' => array(
        'icon'  => 'glyphicon glyphicon-th',
        'label' => 'crumb_city',
    ),
    'sys/user' => array(
        'icon'  => 'glyphicon glyphicon-user',
        'label' => 'crumb_user',
    ),
    'sys/user_group' => array(
        'icon'  => 'glyphicon glyphicon-user',
        'label' => 'crumb_user_group',
    ),
    'sys/module' => array(
        'icon'  => 'glyphicon glyphicon-briefcase',
        'label' => 'lbl_module',
    ),*/
    /*'sys/activity' => array(
        'icon'  => 'glyphicon glyphicon-retweet',
        'label' => 'crumb_activity',
    ),
    'sys/addon' => array(
        'icon'  => 'glyphicon glyphicon-magnet',
        'label' => 'crumb_addon',
    ),
    'sys/machine' => array(
        'icon'  => 'glyphicon glyphicon-compressed',
        'label' => 'crumb_machine',
    ),
    'sys/worker' => array(
        'icon'  => 'glyphicon glyphicon-user',
        'label' => 'crumb_worker',
    ),*/
);

# Components links
$config['nav_modules'] = array(
    'sys/winterdienst' => array(
        'id'    => 1,
        'icon'  => 'glyphicon glyphicon-asterisk',
        'label' => 'lbl_winterdienst',
    ),
    'sys/winterdienst/street_checking' => array(
        'id'           => 2,
        'icon'         => 'glyphicon glyphicon-eye-open',
        'label'        => 'lbl_street_checking',
        'query_string' => array(
            'module' => 2
        ),
    ),
    'sys/winterdienst/street_cleaning' => array(
        'id'           => 3,
        'icon'         => 'glyphicon glyphicon-road',
        'label'        => 'lbl_street_cleaning',
        'query_string' => array(
            'module' => 3
        ),
    ),
    /*'sys/fasttask' => array(
        'icon'         => 'glyphicon glyphicon-tag',
        'label'        => 'lbl_fasttask',
    ),*/
);

$config['module_settings'] = array(
    MODULE_TYPE_WINTERDIENST    => array(
        'icon' => 'glyphicon glyphicon-asterisk'
    ),
    MODULE_TYPE_STREETCHECKING => array(
        'icon' => 'glyphicon glyphicon-eye-open'
    ),
    MODULE_TYPE_STREETCLEANING => array(
        'icon' => 'glyphicon glyphicon-road'
    ),
    MODULE_TYPE_FASTTASK => array(
        'icon' => 'glyphicon glyphicon-road'
    ),
    MODULE_TYPE_EMERGENCY_SERVICE => array(
        'icon' => 'glyphicon glyphicon-road'
    ),
);

# limit of grid
$config['grid_limit']     = 15;

# Cache config
$config['cache_winterdienst_defaul_coordinate'] = 0;
$config['cache_winterdienst_gps_print']         = 0;
$config['cache_winterdienst_information']       = 0;
$config['cache_viewinfor_information']          = 0;