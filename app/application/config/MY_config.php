<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config['api_web']    = getenv('API_WEB');
$config['upload_dir'] = getenv('UPLOAD_DIR');
$config['media_web']  = getenv('MEDIA_WEB');