<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Codeigniter-Monolog integration package
 *
 * (c) Andreas Pfotenhauer <pfote@ypsilon.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/* General options */
$config['handler']         = 'file';    /* valid handlers are syslog|file|gelf|raven|pdo */
$config['name']            = 'publicsolution';
$config['threshold']       = 2;
$config['formatter']       = 'line'; /* json/line */

/* Line format */
# $config['line_format']   = '%message% %context% %extra%'; # PDO
# $config['line_format']   = '%channel%.%level_name%: %message%'; # syslog
# $config['line_format']   = "%message% %context% %extra%\n"; # Raven
$config['line_format']     = '[%datetime%] %channel%.%level_name%: %message% %extra%'.PHP_EOL; # file stream

/* pdo handler options */
$config['pdo_channel']     = 'publicsolution';
$config['pdo_hostname']    = 'localhost';
$config['pdo_username']    = 'root';
$config['pdo_password']    = 'vertrigo';
$config['pdo_dbname']      = 'publicsolution';
$config['pdo_monolog']     = 'monolog';

/* syslog handler options */
$config['syslog_channel']  = 'publicsolution';
$config['syslog_facility'] = 'local6';

/* file handler options */
$config['file_logfile']    = APPPATH . '/logs/monolog-'.date('Y-m-d').'.log';

/* GELF options */
$config['gelf_host']       = 'localhost';
$config['gelf_port']       = '12201';

/* Raven options */
$config['raven_endpoint']  = 'http://api:key@localhost/1';