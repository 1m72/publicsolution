<?php
class MY_Form_validation extends CI_Form_validation
{
    public $CI;

    /**
     * Validate URL format
     *
     * @access  public
     * @param   string
     * @return  string
     */
    function valid_url($str){
        /*$pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
        if (!preg_match($pattern, $str)){*/
        if (!filter_var($str, FILTER_VALIDATE_URL)){
            $this->set_message('valid_url', "The %s field is not correctly formatted.");
            return FALSE;
        }

        return TRUE;
    }

    function valid_datetime($str){
        if ($str != date('Y-m-d H:i:s', strtotime($str))){
            $this->set_message('valid_datetime', "The %s field is not correctly formatted.");
            return FALSE;
        }

        return TRUE;
    }
}