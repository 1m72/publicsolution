<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tool extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function env() {
        echo ENVIRONMENT;
    }
}
