<?php

$lang['profiler_database']		    = 'DATENBANK';
$lang['profiler_controller_info']   = 'KLASSE/METHOD';
$lang['profiler_benchmarks']	    = 'BENCHMARK';
$lang['profiler_queries']		    = 'FRAGE';
$lang['profiler_get_data']		    = 'DATA ERHALTEN';
$lang['profiler_post_data']		    = 'POSTDATEN';
$lang['profiler_uri_string']	    = 'URI-ZEICHENFOLGE';
$lang['profiler_memory_usage']	    = 'SPEICHERNUTZUNG';
$lang['profiler_config']		    = 'CONFIG VARIABLES';
$lang['profiler_session_data']	    = 'SITZUNGSDATEN';
$lang['profiler_headers']		    = 'HTTP ÜBERSCHRIFT';
$lang['profiler_no_db']			    = 'Derzeit ist databanktreiber nicht geladen';
$lang['profiler_no_queries']	    = 'Keine Frage wurde ausgeführt.';
$lang['profiler_no_post']		    = 'Es gibt keine Postdaten';
$lang['profiler_no_get']		    = 'Es gibt keine ERHALTEN-Daten';
$lang['profiler_no_uri']		    = 'Es gibt keine URI-Daten';
$lang['profiler_no_memory']		    = 'Speicherplatz ist nicht verfügbar';
$lang['profiler_no_profiles']	    = 'Keine Profildaten - Alle Profilabschnitte wurden deaktiviert';
$lang['profiler_section_hide']	    = 'Verbergen';
$lang['profiler_section_show']	    = 'Anzeigen';

/* End of file profiler_lang.php */
/* Location: ./system/language/english/profiler_lang.php */