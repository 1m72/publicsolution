<?php

$lang['lbl_username']             = "Benutzername";
$lang['lbl_email']                = "E-mail";
$lang['lbl_group']                = "Gruppe";
$lang['lbl_company']              = "Unternehmen";
$lang['lbl_option']               = "Option";
$lang['lbl_btn_add']              = "Neu hinzufügen";
$lang['lbl_list_user']            = "Benutzerliste";
$lang['lbl_user_detail']          = "Benutzerdetail";

#--- 19 Nov 2013
$lang['lbl_select_group']         = "---Benutzergruppe wählen---";
$lang['msg_user_deleted']         = "Der Benutzer wurde gelöscht";

#--- 21 Nov 2013
$lang['msg_user_saved']           = "Benutzer Stadtinformation wird gespeichert";
$lang['lbl_id']                   = "Identität";
$lang['msg_question_delete_user'] = "Sind Sie sicher, diesen Benutzer zu löschen?";
$lang['lbl_password']             = "Passwort";
$lang['lbl_password_confirm']     = "Passwort bestätigen";
$lang['lbl_city']                 = "Stadt";

#---2013-12-17
$lang['lbl_new_password']         = "Neues Passwort";
$lang['lbl_new_password_confirm'] = "Neues Passwort bestätigen";
$lang['lbl_last_login']           = "Letztes Anmeldung";
$lang['lbl_created_at']           = "Erstellt um";
$lang['msg_old_password']         = "Altes Passwort ist nicht richtig!";

#---2013-12-24
$lang['test_connect']             = 'Verbindung prüfen';
$lang['connect_success']          = 'Test Verbingdung erfolgt';
$lang['connect_fail']             = 'Test Verbindung ist fehlgeschlagen! Checken Sie bitte die Datenbankeinstellung!';
$lang['msg_password_changed']     = 'Ihr Passwort wurde gerade verändert!';
$lang['msg_database_save']        = 'Die Information der Datenbank wurde gespeichert!';
$lang['msg_storage_save']         = 'Die Information des Speichers wurde gespeichert!';
