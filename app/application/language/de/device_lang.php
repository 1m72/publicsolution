<?php

$lang['dv_msg_device_information_has_been_saved']   = "Geräteinformation wurde gespeichert";
$lang['dv_msg_device_has_been_deleted']             = "Gerät wurde gelöscht";
$lang['dv_lbl_device_detail']                       = "Gerätedetail";
$lang['dv_lbl_id']                                  = "Nr.";
$lang['dv_lbl_device']                              = "Gerät";
$lang['dv_lbl_actived']                             = "Aktiviert";
$lang['dv_lbl_list_device']                         = "Geräteliste";
$lang['dv_msg_question_delete_device']              = "Sind Sie sicher, dieses Gerät zu löschen?";
$lang['dv_lbl_name']                                = "Name";
$lang['dv_holder_device']                           = "Gerätename";
$lang['dv_lbl_city']                                = "Stadt";
$lang['dv_lbl_accept']                              = "Annehmen";
$lang['dv_lbl_do_not_accept']                       = "Ablehnen";
$lang['dv_btn_on']                                  = "An";
$lang['dv_btn_off']                                 = "Ab";