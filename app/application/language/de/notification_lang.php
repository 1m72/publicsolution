<?php
$lang['notification_lbl_header']       = 'Web Auftragsbuch';
$lang['notification_lbl_filter_date']  = 'Filter nach Datum';
$lang['notification_lbl_uri']          = 'URL';
$lang['notification_lbl_result']       = 'Ergebnis';
$lang['notification_lbl_last_request'] = 'Letzte Anforderung';
$lang['notification_lbl_retry']        = 'Erneut versuchen';
$lang['notification_lbl_attachments']  = 'Anhängen';
$lang['notification_lbl_download']     = 'Herunterladen';
