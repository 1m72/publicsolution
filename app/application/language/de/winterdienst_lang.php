<?php

$lang['lbl_by_date']                                         = "Nach Datum";
$lang['lbl_by_worker']                                       = "Mitarbeiter";
$lang['lbl_by_object']                                       = "Nach Objekt";
$lang['lbl_select_object']                                   = "--- Objekt auswählen---";
$lang['lbl_your_browser_does_not_support_this_audio_format'] = 'Ihr Browser unterstützt dieses Audioformat nicht.';
$lang['lbl_gps_record']                                      = "GPS-Aufzeichnung";
$lang['lbl_worker']                                          = "Arbeiter :";
$lang['lbl_object']                                          = "Objekt :";
$lang['lbl_start']                                           = "Start";
$lang['lbl_end']                                             = "Ende";
$lang['lbl_date']                                            = "Datum :";
$lang['lbl_info']                                            = "Info";
$lang['lbl_video']                                           = "Video";
$lang['lbl_image']                                           = "Photo";
$lang['lbl_voice']                                           = "Audio:";
$lang['lbl_view_detail']                                     = "Detail anschauen";
$lang['lbl_information']                                     = "Information";
$lang['lbl_map']                                             = "Karte";
$lang['lbl_empty']                                           = "--- Leer ---";
$lang['lbl_select_worker']                                   = "---- Mitarbeiter auswählen ---";
$lang['lbl_winterdienst_detail']                             = "Winterdienst Detail";
$lang['lbl_voice_meno']                                      = "Sprachaufzeichnung";
$lang['lbl_video_file_demo']                                 = "Demo Video-Datei";
$lang['lbl_close']                                           = "Schließen";

#--- 11 Nov 2013
$lang['lbl_by_machine']                                      = " Fahrzeug ";
$lang['lbl_select_machine']                                  = "---	Fahrzeug auswählen---";

#---09 Jan 2014
$lang['btn_ok']                                              = "OK";
$lang['lbl_tab_activities']                                  = "Koordinaten";
$lang['lbl_message']                                         = "Nachrichten";
$lang['lbl_activity']                                        = "Aktivität";

$lang['lbl_select_street']                                  = "--- Straße auswählen ---";
$lang['lbl_street']                                          = "Straße";

$lang['btn_detail']                                          = "Ansehen";

#--- 14 Jan 2014
$lang['lbl_no_data']                                         = "Keine Daten";

#--- 21 Jan 2014
$lang['lbl_duration']                                        = "Dauer";

#--- 26 Jan 2014
$lang['time']                                                = "Time";
$lang['latitude']                                            = "Latitude";
$lang['longitude']                                           = "Longitude";

$lang['lbl_data_processing']                                 = "Datenverarbeitung";

#--- 14 Feb 2014
$lang['lbl_print_tip']                                       = "Hier klicken, um gps-detail zu drucken";
$lang['error_gps_404']                                       = "GPS-Aufzeichnung nicht gefunden";
$lang['error_gps_activity_empty']                            = "GPS-AKtivitäten sind nicht verfügbar";

#--- 3 Mar 2014
$lang['lbl_duration_total']                                  = 'Total';