<?php

$lang['lbl_hello_superior']                = "Hallo Vorgesetzte!";
$lang['lbl_superior_index']                = "Vorgesetzteverzeichnis";

# 26-Nov-13
$lang['msg_superior_saved']                = "Informationen der Vorgesetzte wurden gespeichert";
$lang['lbl_id']                          = "Nr.";
$lang['lbl_first_name']                  = "Vorname";
$lang['holder_first_name']               = "Vorname";
$lang['lbl_last_name']                   = "Nachname";
$lang['holder_last_name']                = "Nachname";
$lang['sp_lbl_nfc_code']                 = "NFC code";
$lang['lbl_phone']                       = "Handysnummer";
$lang['holder_phone']                    = "Handysnummer";
$lang['lbl_address']                     = "Adresse";
$lang['holder_address']                  = "Adresse";
$lang['lbl_birthday']                    = "Geburtsdatum";
$lang['lbl_personal_code']               = "Personalcode";
$lang['holder_personal_code']            = "Personalcode";
$lang['lbl_nfc_code']                    = "NFC_code";
$lang['lbl_select_nfc_code']             = "--- NFC-Code auswählen---";
$lang['lbl_city']                        = "Stadt";
$lang['lbl_company']                     = "Unternehmen";
$lang['lbl_superior_detail']             = "Vorgesetztedetail";
$lang['lbl_list_superior']               = "Vorgesetzteliste";
$lang['msg_question_delete']             = "Sind Sie sicher, diesen Vorgesetzte zu löschen?";
$lang['lbl_superior']                    = "Vorgesetzte";
$lang['msg_superior_has_been_deleted']   = "Der Vorgesetzte wurde gerade gelöscht.";

#--- 21 May 2014
$lang['superior_lbl_assign_more']          = "Mehr zuordnen";
$lang['superior_lbl_show_machne_assigned'] = 'Anzeige zugeordnet';

#--- 18 Jul 2014
$lang['superior_lbl_price']                = "Preis";
$lang['superior_holder_price']             = "Preis";

$lang['superior_lbl_fullname']             = "vollständiger Name";
