<?php

$lang['city_city_detail']           = "Stadtdetail";
$lang['city_grid_id']               = "Nr.";
$lang['city_grid_name']             = "Name";
$lang['city_grid_database']         = "Datenbank";
$lang['city_grid_storage']          = "Speicher";
$lang['city_list_city']             = "Städteliste";
$lang['city_msg_delete']            = "Sind Sie sicher, diese Stadt zu löschen?";
$lang['city_lbl_city_name']         = "Stadtname";
$lang['city_lbl_passcode']          = "Passcode";
$lang['city_lbl_database']          = "Datenbank";
$lang['city_lbl_hostname']          = "Gastname";
$lang['city_lbl_username']          = "Benutzername";
$lang['city_lbl_password']          = "Kennwort";
$lang['city_lbl_password_confirm']  = "Passwort bestätigen";
$lang['city_lbl_port']              = "Anschluss";
$lang['city_lbl_storage']           = "Speicher";
$lang['city_lbl_type']              = "Type";
$lang['city_lbl_ip']                = "Ip";
$lang['city_lbl_home_dir']          = "Benutzerverzeichnis";
$lang['city_msg_save_successful']   = "Stadtinformationen werden gespeichert";
$lang['city_msg_delete_successful'] = "Stadt wird gelöscht";

#2013-12-04
$lang['city_btn_on']                = "AN";
$lang['city_btn_off']               = "AB";