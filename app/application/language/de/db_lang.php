<?php

$lang['db_invalid_connection_str']  	= 'Es ist nicht möglich, die Datenbank-Einstellungen bezogen auf die von Ihnen abgesendete Verbindung-Zeichenkette zu bestimmen.';
$lang['db_unable_to_connect']  			= 'Es ist nicht möglich, mit Ihrem Datenbank-Server bezogen auf der bereitsgestellten Einstellungen zu verbinden.';
$lang['db_unable_to_select']  			= 'Auswahl der bestimmten Datenbank: %s ist fehlgeschlagen.';
$lang['db_unable_to_create']  			= 'Erstellung der bestimmten Datenbank: %s ist fehlgeschlagen.';
$lang['db_invalid_query']  				= 'Die von Ihnen abgesendete Anfrage ist nicht gültig.';
$lang['db_must_set_table']  			= 'Sie müssen eine Datenbank-Tabelle anlegen, die für die Anfrage verwendet werden kann.';
$lang['db_must_use_set']  				= 'Zur Aktualisierung eines Eintrags verwenden sie "Set" Methode.';
$lang['db_must_use_index']  			= 'Sie müssen ein entsprechendes Inhaltverzeichnis für Stapelaktualisierung angeben.';
$lang['db_batch_missing_index']  		= 'Eine oder mehrere für Stapelaktualisierung abgesendete Zeile fehlt an bestimmten Verzeichnis .';
$lang['db_must_use_where']  			= 'Aktualisierungen sind nicht erlaubt, es sei denn, dass sie einen "Where" Satz beinhalten. ';
$lang['db_del_must_use_where']  		= 'Löschen ist nicht erlaubt, es sei denn, dass sie einen "Where" oder "Like" Satz beinhalten.';
$lang['db_field_param_missing']  		= 'Der Tabellenname und ein Parameter ist erforderlich für den Abrufen der Felder.';
$lang['db_unsupported_function']  		= 'Diese Funktion ist nicht verfügbar für die von Ihnen verwendete Datenbank .';
$lang['db_transaction_failure']  		= 'Transaktion fehlgeschlagen: Zurückrollen durchgeführt.';
$lang['db_unable_to_drop']  			= 'Es ist nicht möglich, die bestimmten Datenbank zu löschen';
$lang['db_unsuported_feature']  		= 'Nicht unterstütze Eigenschaften des Datenbank-Plattforms, das Sie gerade verwenden.';
$lang['db_unsuported_compression']  	= 'Das von Ihnen gewähte Datei Kompression-Format ist nicht durch Ihren Server unterstützt.';
$lang['db_filepath_error']  			= 'Es ist nicht möglich, Daten zu dem Datei-Pfad, das Sie abgesendet, zu schreiben.';
$lang['db_invalid_cache_path']  		= 'Der von Ihnen abgesendete Cache-Pfad ist nicht gültig oder nicht schreibbar.';
$lang['db_table_name_required']  		= 'Ein Tabellenname ist für die Durchführung erforderlich.';
$lang['db_column_name_required']  		= 'Ein Spaltenname ist für die Durchführung erforderlich';
$lang['db_column_definition_required']  = 'Eine Spaltendefinition ist für die Durchführung erforderlich.';
$lang['db_unable_to_set_charset']  		= 'Es ist nicht möglich, Zeichen-Set für Kundenverbindung einzustellen.';
$lang['db_error_heading']  				= 'Ein Datenbank-Fehler eingetreten.';

/* End of file db_lang.php */
/* Location: ./system/language/english/db_lang.php */