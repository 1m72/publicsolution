<?php

$lang['lbl_hello_worker']                = "Hallo Arbeiter!";
$lang['lbl_worker_index']                = "Arbeiterverzeichnis";

# 26-Nov-13
$lang['msg_worker_saved']                = "Informationen der Arbeiter wurden gespeichert";
$lang['lbl_id']                          = "Nr.";
$lang['lbl_first_name']                  = "Vorname";
$lang['holder_first_name']               = "Vorname";
$lang['lbl_last_name']                   = "Nachname";
$lang['holder_last_name']                = "Nachname";
$lang['wk_lbl_nfc_code']                 = "NFC code";
$lang['lbl_phone']                       = "Handysnummer";
$lang['holder_phone']                    = "Handysnummer";
$lang['lbl_address']                     = "Adresse";
$lang['holder_address']                  = "Adresse";
$lang['lbl_birthday']                    = "Geburtsdatum";
$lang['lbl_personal_code']               = "Personalcode";
$lang['holder_personal_code']            = "Personalcode";
$lang['lbl_nfc_code']                    = "NFC_code";
$lang['lbl_select_nfc_code']             = "--- NFC-Code auswählen---";
$lang['lbl_city']                        = "Stadt";
$lang['lbl_company']                     = "Unternehmen";
$lang['lbl_worker_detail']               = "Arbeiterdetail";
$lang['lbl_list_worker']                 = "Arbeiterliste";
$lang['msg_question_delete']             = "Sind Sie sicher, diesen Arbeiter zu löschen?";
$lang['lbl_worker']                      = "Arbeiter";
$lang['msg_worker_has_been_deleted']     = "Der Arbeiter wurde gerade gelöscht.";

#--- 21 May 2014
$lang['worker_lbl_assign_more']          = "Mehr zuordnen";
$lang['worker_lbl_show_machne_assigned'] = 'Anzeige zugeordnet';

#--- 18 Jul 2014
$lang['worker_lbl_price']                = "Preis";
$lang['worker_holder_price']             = "Preis";

$lang['worker_lbl_fullname']             = "vollständiger Name";
