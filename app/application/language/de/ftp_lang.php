<?php

$lang['ftp_no_connection']			= "Es ist nicht möglich, eine gültige ID-Verbindung zu lokalisieren. Bitte stellen Sie sicher, dass Sie gerade verbunden sind, bevor Sie irgend Routines-Datei durchführen.";
$lang['ftp_unable_to_connect']		= "Es ist nicht möglich, Ihr FTP über den mitgelieferte Hostnamen zu verbinden.";
$lang['ftp_unable_to_login']		= "Es ist nicht möglich, in Ihr FTP Server anzumelden. Bitte checken Sie Ihren Benutzernamen und Passwort.";
$lang['ftp_unable_to_makdir']		= "Es ist nicht möglich, das von Ihnen angegebene Verzeichnis zu erstellen.";
$lang['ftp_unable_to_changedir']	= "Es ist nicht möglich, die Verzeichnisse zu verändern.";
$lang['ftp_unable_to_chmod']		= "Es ist nicht möglich, die Dateiberechtigungen festzulegen. Bitte checken Sie Ihren Pfad. Diese Funktion ist nur in PHP 5 oder höher verfügbar.";
$lang['ftp_unable_to_upload']		= "Es ist nicht möglich, die angegebene Datei hochzuladen. Bitte checken Sie Ihren Pfad.";
$lang['ftp_unable_to_download']		= "Es ist nicht möglich, die angegebene Datei herunterzuladen. Bitte checken Sie Ihren Pfad.";
$lang['ftp_no_source_file']			= "Es ist nicht möglich, die Quelldatei zu lokalisieren. Bitte checken Sie Ihren Pfad.";
$lang['ftp_unable_to_rename']		= "Es ist nicht möglich, die Datei umzubenennen.";
$lang['ftp_unable_to_delete']		= "Es ist nicht möglich, die Datei zu löschen.";
$lang['ftp_unable_to_move']			= "Es ist nicht möglich, die Datei zu  verschieben. Bitte stellen Sie sicher, dass das Zielverzeichnis existiert.";


/* End of file ftp_lang.php */
/* Location: ./system/language/english/ftp_lang.php */