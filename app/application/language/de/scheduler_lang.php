<?php
$lang['sd_lbl_schedule']                   = "Zeitplan";
$lang['sd_msg_schedule_saved']             = "Zeitplansinformation wurde gespeichert";
$lang['sd_lbl_group']                      = "Gruppe";
$lang['sd_lbl_worker']                     = "Arbeiter";
$lang['sd_lbl_title']                      = "Titel";
$lang['sd_lbl_starttime']                  = "Start";
$lang['sd_lbl_endtime']                    = "Ende";
$lang['sd_lbl_des']                        = "Beschreibung";
$lang['sd_msg_has_been_deleted']           = "Zeitplan wurde gelöscht";
// 05-29-2014
$lang['sd_delete_confirmation']            = "Sind Sie sicher, diesen Zeitplan zu löschen?";
$lang['sd_msg_date_datenow']               = "Übertragene Zeit muss grösser als aktuelle Zeil sein";
$lang['sd_msg_date']                       = "Enddatum muss grösser als Startdatum sein";
$lang['sd_msg_create']                     = "Sie sollen die Datum planen, die 5 Tage grösser als die aktuelle Datum ist! ";
$lang['sd_validate_workergroup']           = "Das Feld Gruppe oder Arbeiter ist erforderlich";
$lang['sd_search']                         = "Suchen ...";
$lang['sd_schedule_detail']                = "Zeitplansdetail";
$lang['sd_lbl_list']                       = "Zeitplansliste";