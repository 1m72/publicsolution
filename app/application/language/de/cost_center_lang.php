<?php

$lang['lbl_hello_costcenter']            = "Hello costcenter !";
$lang['lbl_title']                       = "Title";
$lang['lbl_id']                          = "ID";
$lang['lbl_list_costcenter']             = "List Cost center";
$lang['lbl_costcenter']                  = "Cost center";
$lang['lbl_costcenter_detail']           = "Cost center detail";
$lang['msg_question_delete']             = "Are you sure want to delete this cost center?";
$lang['msg_costcenter_has_been_deleted'] = "Cost center has been deleted";
$lang['msg_costcenter_saved']            = "Cost center information has been saved";
$lang['costcenter_holder_title']         = "Title";
