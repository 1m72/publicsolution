<?php

$lang['email_must_be_array'] 			= "Die E-Mail Validerungsmethode muss ein Datenfeld als Eingabeparameter erhalten";
$lang['email_invalid_address'] 			= "Ungültige E-Mail Adresse: %s";
$lang['email_attachment_missing'] 		= "Es ist nicht möglich, den Anhang des folgenden E-Mails zu lokalisieren: %s";
$lang['email_attachment_unreadable']	= "Es ist nicht möglich, diesen Anhang zu öffnen:  %s";
$lang['email_no_recipients'] 			= "Sie müssen die Empfänger füllen: An, Cc oder Bcc";
$lang['email_send_failure_phpmail']     = "Es is nicht möglich, die E-Mail über PHP Mail() zu senden. Ihr Server könnte unkonfiguriert sein, die E-Mail über diese Methode zu senden.";
$lang['email_send_failure_sendmail'] 	= "Es is nicht möglich, die E-Mail über PHP Sendmail zu senden. Ihr Server könnte unkonfiguriert sein, die E-Mail über diese Methode zu senden.";
$lang['email_send_failure_smtp'] 		= "Es is nicht möglich, die E-Mail über PHP SMTP zu senden. Ihr Server könnte unkonfiguriert sein, die E-Mail über diese Methode zu senden.";
$lang['email_sent'] 					= "Ihre Nachricht wurde erfolgreich über das folgende Protokoll  %s gesendet.";
$lang['email_no_socket'] 				= "Es ist nicht möglich, eine Steckdose zum Sendmail zu öffnen. Bitte checken Sie die Einstellung.";
$lang['email_no_hostname'] 				= "Sie haben einen SMTP-Hostname nicht angegeben.";
$lang['email_smtp_error'] 				= "Der folgende SMTP-Fehler wurde aufgestoßen: %s";
$lang['email_no_smtp_unpw'] 			= "Fehler: Sie müssen einen SMTP-Benutzername und Passwort beauftragen.";
$lang['email_failed_smtp_login'] 		= "Fehler beim Senden des Befehls AUTH LOGIN. Fehler: %s";
$lang['email_smtp_auth_un'] 			= "Fehler bei der Authentisierung des Benutzernamen. Fehler: %s";
$lang['email_smtp_auth_pw'] 			= "Fehler bei der Authentisierung des Passworts. Fehler: %s";
$lang['email_smtp_data_failure'] 		= "Es ist nicht möglich, die Daten zu senden: %s";
$lang['email_exit_status'] 				= "Stellenwertscode des Endes: %s";


/* End of file email_lang.php */
/* Location: ./system/language/english/email_lang.php */