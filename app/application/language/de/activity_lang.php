<?php

$lang['activity_holder_id_ref']                  = "Referenz";
$lang['activity_holder_name']                    = "Aktivitätsname";
$lang['activity_lbl']                            = "Aktivität";
$lang['activity_lbl_name']                       = "Aktivitätsname";
$lang['activity_lbl_color']                      = "Farbe";
$lang['activity_lbl_detail']                     = "Aktivität Detail";
$lang['activity_lbl_id_ref']                     = "Referenz";
$lang['activity_lbl_list']                       = "Aktivitätenslist";
$lang['activity_msg_has_been_deleted']           = "Aktivität wurde gelöscht.";
$lang['activity_msg_information_has_been_saved'] = "Aktivitätsinformation wurde gespeichert.";
$lang['activity_msg_question_delete']            = "Sind Sie sicher, diese Aktivität zu löschen?";