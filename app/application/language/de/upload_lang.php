<?php

$lang['upload_userfile_not_set']        = "Es ist nicht m�glich, einen Post variabel mit Namen Userfile zu finden. ";
$lang['upload_file_exceeds_limit']      = "Die hochgelandene Datei �berschreitet die maximale zul�ssige Gr��e in deiner PHP Konfigurationsdatei.";
$lang['upload_file_exceeds_form_limit'] = "Die hochgelandene Datei �berschreitet die maximale Gr��e, die durch den Browser zugelassen wird.";
$lang['upload_file_partial']            = "Die Datei wurde nur zum Teil hochgeladen.";
$lang['upload_no_temp_directory']       = "Der Verzeichnis f�r die Zwischenspeicherung fehlt.";
$lang['upload_unable_to_write_file']    = "Die Datei konnte nicht auf die Festplatte geschrieben werden.";
$lang['upload_stopped_by_extension']    = "Das Hochladen der Datei wurde auf Grund der Dateierweiterung gestoppt.";
$lang['upload_no_file_selected']        = "Sie haben keine Datei gew�hlt, um sie hochzuladen. ";
$lang['upload_invalid_filetype']        = "Der Dateityp, den Sie gerade versuchen, hochzuladen, ist nicht erlaubt.";
$lang['upload_invalid_filesize']        = "Die Datei, die Sie gerade versuchen, hochzuladen, ist gr��er als die maximal zugelassene Dateigr�sse.";
$lang['upload_invalid_dimensions']      = "Das Bild, das Sie gerade versuchen, hochzuladen, �berschreitet die maximale H�he oder Breite.";
$lang['upload_destination_error']       = "Ein Problem ist bei der Verschiebung der hochgeladenen Datei an die Zieldestination aufgetreten.";
$lang['upload_no_filepath']             = "Der Upload-Pfad scheint nicht g�ltig zu sein.";
$lang['upload_no_file_types']           = "Sie haben noch keine erlaubte Dateitypen festgelegt.";
$lang['upload_bad_filename']            = "Der verwendete Dateiname ist bereits auf dem Server vorhanden.";
$lang['upload_not_writable']            = "Der Uploadestination-Ordner scheint nicht schreibbar zu sein. ";


/* End of file upload_lang.php */
/* Location: ./system/language/english/upload_lang.php */