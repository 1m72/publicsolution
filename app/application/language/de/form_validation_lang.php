<?php

$lang['required']			= "Das %s Feld ist erforderlich.";
$lang['isset']				= "Das %s Feld muss über einen Wert verfügen.";
$lang['valid_email']		= "Das %s Feld muss eine gültige E-Mail-Adresse beinhalten.";
$lang['valid_emails']		= "Das %s Feld muss alle gültige E-Mail-Adressen beinhalten.";
$lang['valid_url']			= "Das %s Feld muss ein gültiges URL beinhalten.";
$lang['valid_ip']			= "Das %s Feld muss ein gültiges IP beinhalten.";
$lang['min_length']			= "Das %s Feld muss über zumindest %s Zeichen verfügen.";
$lang['max_length']			= "Das %s Feld kann nicht %s Zeichen überschreiten.";
$lang['exact_length']		= "Das %s Feld muss genau  %s Zeichen beinhalten.";
$lang['alpha']				= "Das %s Feld darf nur alphaphetische Zeichen beinhalten.";
$lang['alpha_numeric']		= "Das %s Feld darf nur alphanumerische Zeichen beinhalten.";
$lang['alpha_dash']			= "Das %s Feld darf nur alphanumerische Zeichen, Unterstriche und Striche beinhalten.";
$lang['numeric']			= "Das %s Feld darf nur Nummer beinhalten.";
$lang['is_numeric']			= "Das %s Feld darf nur numerische Zeichen beinhalten.";
$lang['integer']			= "Das %s Feld darf nur Ganzzahl beinhalten.";
$lang['regex_match']		= "Das %s Feld ist nicht in dem richtigen Format.";
$lang['matches']			= "Das %s Feld stimmt nicht mit dem %s Feld überein.";
$lang['is_unique'] 			= "Das %s Feld muss einen einzigartigen Wert beinhalten. ";
$lang['is_natural']			= "Das %s Feld muss nur positive Zahlen beinhalten.";
$lang['is_natural_no_zero']	= "Das %s Feld muss eine Zahl größer als Zero beinhalten.";
$lang['decimal']			= "Das %s Feld muss eine Dezimalzahl beinhalten.";
$lang['less_than']			= "Das %s Feld muss eine Zahl beinhalten, die kleiner als %s ist.";
$lang['greater_than']		= "Das %s Fedd muss eine Zahl beinhalten, die größer als %s ist.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */