<?php

$lang['email_format']                          = "Ihre E-Mail ist ungültig";
$lang['err_captcha_invalid']                   = "Capcha ist ungültig";
$lang['lbl_aufgaben']                          = "Aufgaben";
$lang['lbl_communication']                     = "Kommunikation";
$lang['lbl_einrichtung']                       = "Einrichtung";
$lang['lbl_fasttask']                          = "Fasttask";
$lang['lbl_home']                              = "Startseite";
$lang['lbl_item']                              = "Eintrag";
$lang['lbl_language']                          = "Sprache";
$lang['lbl_login']                             = "Anmelden";
$lang['lbl_logout']                            = "Abmelden";
$lang['lbl_posteingang_40']                    = "Posteingang (40)";
$lang['lbl_profile']                           = "Profil";
$lang['lbl_programme_heute']                   = "Programme/Heute";
$lang['lbl_public_solution']                   = "BIS-Office";
$lang['lbl_snippets']                          = "Schnipsel";
$lang['lbl_toggle_navigation']                 = "Toggle Navigation";
$lang['lbl_winterdienst']                      = "Winterdienst";

// 11 Nov 2013
$lang['err_api_connection']                    = "Fehler bei der Verbindung zur Datenbank.";
$lang['err_api_save']                          = "Fehler beim Speichern in die Datenbank.";
$lang['btn_save_change']                       = "Speichern";
$lang['btn_cancel']                            = "Abbrechen";
$lang['btn_edit']                              = "Bearbeiten";
$lang['btn_delete']                            = "Löschen";
$lang['lbl_item_per_page']                     = "Einträge pro Seite";

// 2013-11-20
$lang['btn_yes']                               = "Ja";
$lang['btn_no']                                = "Nein";
$lang['btn_add_new']                           = "Hinzufügen";
$lang['err_has_an_error_when_update_database'] = "Fehler bei der Datenaktualisierung ";
$lang['grid_option']                           = "Wahl";
$lang['delete_confirm']                        = "Bestätigung";
$lang['alert_error']                           = "Fehler";
$lang['btn_show']                              = "Anzeigen";
$lang['btn_hide']                              = "Verbergen";

// 2013_11_21
$lang['btn_on']                                = "VERFÜGBAR";
$lang['btn_off']                               = "STANDARD";
$lang['grid_updated_at']                       = "Aktualisiert am";
$lang['grid_created_at']                       = "Erstellt am";
$lang['lbl_select_city']                       = "---Stadt auswählen---";
$lang['lbl_select_company']                    = "---Firma auswählen---";
$lang['lbl_select_class']                      = "---Klasse auswählen---";

// 2013-11-25
$lang['err_unique_username']                   = "Der Benutzername muss einen eindeutigen Wert beinhalten";
$lang['err_unique_email']                      = "Das E-Mail-Feld muss einen eindeutigen Wert beinhalten.";

// 2013-12-03
$lang['crumb_user']                            = "Benutzer";
$lang['crumb_city']                            = "Stadt";
$lang['crumb_company']                         = "Unternehmen";
$lang['crumb_machine']                         = "Maschine";
$lang['crumb_device']                          = "Gerät";
$lang['crumb_object']                          = "Objekt";
$lang['crumb_worker']                          = "Arbeiter";
$lang['crumb_modules']                         = "Modul";
$lang['dropdown_user']                         = "Benutzerliste";
$lang['dropdown_city']                         = "Stadtliste";
$lang['dropdown_company']                      = "Unternehmensliste";
$lang['dropdown_machine']                      = "Maschinenliste";
$lang['dropdown_device']                       = "Gerätliste";
$lang['dropdown_object']                       = "Objektliste";

// 2013-12-05
$lang['grid_number']                           = "Nr.";

// 2013-12-11
$lang['lbl_select_activity']                   = "--- Aktivität auswählen ----";

// 2013-12-12
$lang['lbl_select_default']                    = "--- Voreingestellte Aktivität auswählen ----";

// 2013-12-13
$lang['message_activity']                      = "Sie müssen mindestens 2 Aktivitäten wählen!";

// 2013-12-17
$lang['crumb_addon']                           = "Zusatz";

// 2013-12-17
$lang['crumb_activity']                        = "Aktivität";

// 2013-12-23
$lang['crumb_nfc']                             = "NFC";

// 2013-12-24
$lang['lbl_add_new']                           = "Hinzufügen";

// 2013/12/25
$lang['lbl_activity']                          = "Aktivität";
$lang['lbl_activity_default']                  = "Standardaktivität";

// 2013-12-26
$lang['grid_group_column']                     = "Spaltenkopf hier ablegen, um auf Grund der Spalte zu gruppieren.";

// 2013-12-31
$lang['crumb_user_group']                      = "Benutzergruppe";

// 2014-01-03
$lang['language_en']                           = "English";
$lang['language_de']                           = "Deutsch";

// 2014-01-09
$lang['language_copyright']                    = "&copy; 2014";

// 2014-01-10
$lang['loading']                               = "Verladung ...";
$lang['waiting']                               = "Warten ...";
$lang['unavailable']                           = "nicht verfügbar";

// 4 Mar 2014
$lang['lbl_identifier']                        = "Kennzeichnung";
$lang['holder_identifer']                      = "";

// 7 Mar 2014
$lang['lbl_module']                            = "Modul";


// 4 Apr 2014
$lang['crumb_components']                      = "Komponenten";
$lang['lbl_street_checking']                   = "Straßenkontrolle";
$lang['lbl_street_cleaning']                   = "Straßenreinigung";
$lang['lbl_select_module']                     = "--- Modul wählen ---";

// 2 May 2014
$lang['lbl_map_tracker']                       = "Visualisierung Karte";
$lang['lbl_module_setting']                    = "Moduleinstellung";

// 16 June 2014
$lang['crumb_group']                           = "Gruppe";

// 2 Jul 2014
$lang['nav_notification']                      = "Benachrichtigung";
$lang['msg_data_not_available']                = "Daten sind nicht verfügbar";

// 7 July 2014
$lang['lbl_module_scheduler']                  = "Scheduler";

// 27 August 2014
$lang['lbl_material']                          = "Material";

// 02 July 2014
$lang['crumb_superior']                        = "Vorgesetzte";
$lang['crumb_object_group']                    = "Objektgruppe";
$lang['crumb_object_place']                    = "Objekt Ort";
