<?php
$lang['module_holder_id_ref']                  = "Referenz";
$lang['module_holder_title']                   = "Modulname";
$lang['module_lbl_description']                = "Beschreibung";
$lang['module_lbl_detail']                     = "Maschinendetail";
$lang['module_lbl_id']                         = "Nr.";
$lang['module_lbl_list']                       = "Modulliste";
$lang['module_lbl_title']                      = "Modulname";
$lang['module_msg_has_been_deleted']           = "Modul wurde gelöscht";
$lang['module_msg_information_has_been_saved'] = "Informationen des Modules wurden gespeichert";
$lang['module_msg_question_delete']            = "Sind Sie sicher, dieses Modul zu löschen?";

#--- 2 May 2014
$lang['module_lbl_email']                      = "Email";
$lang['module_msg_update_email']               = "E-Mail-Benachrichtigung wurde gespeichert";

#--- 5 May 2014
$lang['module_lbl_tip_email']                  = 'Geben Sie die Emailadresse ein und drücken <strong>Enter</strong> um Email hinzuzufügen';