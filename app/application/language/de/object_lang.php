<?php

$lang['object_object_detail']           = "Objektdetail";
$lang['object_grid_id']                 = "Nr.";
$lang['object_grid_ref']                = "Referenz";
$lang['object_grid_city']               = "Stadt";
$lang['object_grid_parent']             = "Eltern";
$lang['object_grid_group']              = "Gruppe";
$lang['object_grid_position']           = "Position";
$lang['object_grid_name']               = "Name";
$lang['object_list_object']             = "Objektliste";
$lang['object_msg_delete']              = "Sind Sie sicher, dieses Objekt zu löschen?";
$lang['object_lbl_object_name']         = "Objektname";
$lang['object_lbl_ref_name']            = "Referenz";
$lang['object_lbl_city_name']           = "Stadt";
$lang['object_lbl_parent_name']         = "Eltern";
$lang['object_lbl_group_name']          = "Objektgruppe";
$lang['object_lbl_position']            = "Position";
$lang['object_msg_save_successful']     = "Objektinformationen werden gespeichert";
$lang['object_msg_delete_successful']   = "Objekt wird gelöscht";
$lang['object_lbl_select_group']        = "---Gruppe auswählen---";
$lang['object_msg_delete_position']     = "Möchtest diese Position löschen ?";
$lang['object_lbl_group_place']         = "Objekt Ort";