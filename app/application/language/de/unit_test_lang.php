<?php

$lang['ut_test_name']		= 'Testname';
$lang['ut_test_datatype']	= 'Datentyp des Testes';
$lang['ut_res_datatype']	= 'Erwarteter Datentyp';
$lang['ut_result']			= 'Ergebnis';
$lang['ut_undefined']		= 'Undefinierter Testname';
$lang['ut_file']			= 'Filename';
$lang['ut_line']			= 'Zeilennummer';
$lang['ut_passed']			= 'Angepasst';
$lang['ut_failed']			= 'Fehlgesclagen';
$lang['ut_boolean']			= 'Boolean';
$lang['ut_integer']			= 'Ganzzahl';
$lang['ut_float']			= 'Gleitkommazahl';
$lang['ut_double']			= 'Gleitkommazahl'; // can be the same as float
$lang['ut_string']			= 'Zeichenfolge';
$lang['ut_array']			= 'Datenfeld';
$lang['ut_object']			= 'Objekt';
$lang['ut_resource']		= 'Ressource';
$lang['ut_null']			= 'Null';
$lang['ut_notes']			= 'Bemerkungen';


/* End of file unit_test_lang.php */
/* Location: ./system/language/english/unit_test_lang.php */