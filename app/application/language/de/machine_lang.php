<?php

$lang['mc_holder_name']                            = "Maschinename";
$lang['mc_holder_id_ref']                          = "Referenz";
$lang['mc_lbl_id_ref']                             = "Referenz";
$lang['mc_lbl_nfc_code']                           = "NFC_code";
$lang['mc_lbl_select_nfc_code']                    = "--- NFC auswählen ---";
$lang['mc_lbl_city']                               = "Stadt";
$lang['mc_lbl_description']                        = "Beschreibung";
$lang['mc_lbl_list_machine']                       = "Maschinenliste";
$lang['mc_msg_question_delete_machine']            = "Sind Sie sicher, diese Maschine zu löschen?";
$lang['mc_lbl_id']                                 = "Nr.";
$lang['mc_lbl_machine_name']                       = "Maschinename";
$lang['mc_msg_machine_information_has_been_saved'] = "Informationen der Maschinen werden gespeichert";
$lang['mc_msg_machine_has_been_deleted']           = "Maschine wird gelöscht";
$lang['mc_lbl_machine_detail']                     = "Maschinendetail";
$lang['mc_lbl_machine']                            = "Maschine";
$lang['mc_lbl_addon']                              = "Anbauteil";
$lang['mc_lbl_select_addon']                       = "---Add-on auswählen---";

#--- 09 May 2014
$lang['mc_lbl_assign_more']                        = "Zuweisen";

#--- 12 Mar 2014
$lang['mc_lbl_show_machne_assigned']               = 'Anzeige zugeordnet';

#--- 21 May 2014
$lang['mc_cbo_used_for_module']                    = 'Maschine für dieses Modul verwendet';