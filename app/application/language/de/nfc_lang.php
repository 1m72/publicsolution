<?php

$lang['nfc_holder_id_ref']                  = "Referenz";
$lang['nfc_holder_name']                    = "NFC-Name";
$lang['nfc_lbl']                            = "NFC";
$lang['nfc_lbl_name']                       = "NFC-Code";
$lang['nfc_lbl_actived']                    = "Aktiviert";
$lang['nfc_lbl_value']                      = "Wert";
$lang['nfc_lbl_detail']                     = "NFC-Detail";
$lang['nfc_lbl_id']                         = "Identität";
$lang['nfc_lbl_id_ref']                     = "Referenz";
$lang['nfc_lbl_list']                       = "NFC-Liste";
$lang['nfc_msg_has_been_deleted']           = "NFC wurde gerade gelöscht.";
$lang['nfc_msg_information_has_been_saved'] = "Die Information des NFC wurde gespeichert.";
$lang['nfc_msg_question_delete']            = "Sind Sie sicher, dieses NFC zu löschen?";
$lang['nfc_total']                          = "Anzahl der NFC-Code";
$lang['nfc_holder_total']                   = "Geben Sie die Anzahl von NFC-Code, den Sie hinzufügen möchten";
