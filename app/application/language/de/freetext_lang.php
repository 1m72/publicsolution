<?php

$lang['freetxt_holder_id_ref']                  = "Referenz";
$lang['freetxt_holder_name']                    = "Name des Anbaus";
$lang['freetxt_lbl']                            = "Add-on";
$lang['freetxt_lbl_title']                      = "Title";
$lang['freetxt_lbl_detail']                     = "Detail des Anbaus";
$lang['freetxt_lbl_id']                         = "Nr.";
$lang['freetxt_lbl_id_ref']                     = "Referenz";
$lang['freetxt_lbl_list']                       = "Anbau-Liste";
$lang['freetxt_msg_has_been_deleted']           = "Anbau wurde gelöscht";
$lang['freetxt_msg_information_has_been_saved'] = "Die Information des Anbaus wurde gespeichert.";
$lang['freetxt_msg_question_delete']            = "Sind Sie sicher, diesen Anbau zu löschen?";
$lang['freetxt_lbl_assign_more']                = "Mehr zuordnen";
$lang['freetxt_lbl_show_machne_assigned']       = 'Anzeige zugeordnet';
$lang['freetxt_cbo_used_for_module']            = 'Anbauteil für dieses Modul verwendet';
$lang['freetxt_level1']                         = 'Parent level1';
$lang['freetxt_level2']                         = 'Parent level2';