<?php

$lang['lbl_groupname']             = "Gruppenname";
$lang['lbl_usergroup_detail']      = "Detail der Benutzergruppe";
$lang['msg_question_delete']       = "Sind Sie sicher, diese Benutzergruppe zu löschen?";
$lang['lbl_list_user_group']       = "Liste der Benutzergruppe";
$lang['msg_usergroup_deleted']     = 'Benutzergruppe wurde gerade gelöscht';
$lang['msg_usergroup_saved']       = "Informationen der Benutzergruppe wurde gespeichert";