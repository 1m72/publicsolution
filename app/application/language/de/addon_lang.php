<?php

$lang['addon_holder_id_ref']                  = "Referenz";
$lang['addon_holder_name']                    = "Name des Anbaus";
$lang['addon_lbl']                            = "Add-on";
$lang['addon_lbl_name']                       = "Name des Anbaus";
$lang['addon_lbl_detail']                     = "Detail des Anbaus";
$lang['addon_lbl_id']                         = "Nr.";
$lang['addon_lbl_id_ref']                     = "Referenz";
$lang['addon_lbl_list']                       = "Anbau-Liste";
$lang['addon_msg_has_been_deleted']           = "Anbau wurde gelöscht";
$lang['addon_msg_information_has_been_saved'] = "Die Information des Anbaus wurde gespeichert.";
$lang['addon_msg_question_delete']            = "Sind Sie sicher, diesen Anbau zu löschen?";

#--- 09 May 2014
$lang['addon_lbl_assign_more']                = "Mehr zuordnen";

#--- 12 Mar 2014
$lang['addon_lbl_show_machne_assigned']       = 'Anzeige zugeordnet';

#--- 22 May 2014
$lang['addon_cbo_used_for_module']            = 'Anbauteil für dieses Modul verwendet';