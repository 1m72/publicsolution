<?php
$lang['material_holder_unit']                    = "Einheit";
$lang['material_holder_name']                    = "name";
$lang['material_lbl_detail']                     = "Materialdetail";
$lang['material_lbl_list']                       = "Materialliste";
$lang['material_lbl_name']                       = "Materialname";
$lang['material_lbl_unit']                       = "Einheit";
$lang['material_msg_has_been_deleted']           = "Material wurde schon gelöscht";
$lang['material_msg_information_has_been_saved'] = "Information des Materials wurde gespeichert";
$lang['material_msg_question_delete']            = "Sind Sie sicher, dieses Material zu löschen?";