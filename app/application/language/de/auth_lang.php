<?php

$lang['auth_err_login_fail']      = "Ihr Benutzername oder Passwort ist ungültig.";
$lang['auth_msg_forgot_request']  = "Eine E-Mail wird zu Ihnen gesendet !";
$lang['auth_msg_forgot_validate'] = "Ein neues Passwort wird Ihnen per E-Mail zugesandt!";
$lang['auth_err_forgot_validate'] = "Die Angaben sind ungültig!";
$lang['msg_please_sign_in']       = "Bitte melden Sie sich an.";
$lang['lbl_remember_me']          = "Angemeldet bleiben";
$lang['lbl_sign_in']              = "Anmelden";
$lang['lbl_forgot_password']      = "Passwort vergessen";
$lang['lbl_goto_login_page']      = "Zur Anmeldeseite";
$lang['lbl_login']                = "Anmelden";

#2014-01-03
$lang['holder_username']          = "Bitte geben Sie Ihren Benutzername ein";
$lang['holder_password']          = "Bitte geben Sie Ihr Passwort ein";
$lang['holder_captcha']           = "Bitte geben Sie Captcha ein";