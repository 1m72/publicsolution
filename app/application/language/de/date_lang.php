<?php

$lang['date_year'] 		= "Jahr";
$lang['date_years'] 	= "Jahre";
$lang['date_month'] 	= "Monat";
$lang['date_months'] 	= "Monate";
$lang['date_week'] 		= "Woche";
$lang['date_weeks'] 	= "Wochen";
$lang['date_day'] 		= "Tag";
$lang['date_days'] 		= "Tage";
$lang['date_hour'] 		= "Stunde";
$lang['date_hours'] 	= "Stunden";
$lang['date_minute'] 	= "Minute";
$lang['date_minutes'] 	= "Minuten";
$lang['date_second'] 	= "Sekunde";
$lang['date_seconds'] 	= "Sekunden";

$lang['UM12']	= '(UTC -12:00) Bakerinsel / Howlandinsel';
$lang['UM11']	= '(UTC -11:00) Amerikanisch-Samoa, Niue';
$lang['UM10']	= '(UTC -10:00) Hawaii-Aleutian Standard Time, Cookinseln, Tahiti';
$lang['UM95']	= '(UTC -9:30) Marquesas-Inseln';
$lang['UM9']	= '(UTC -9:00) Alaska Standard Time, Gambier Islands';
$lang['UM8']	= '(UTC -8:00) Pacific Standard Time, Clipperton-Insel';
$lang['UM7']	= '(UTC -7:00) Mountain Standard Time';
$lang['UM6']	= '(UTC -6:00) Central Standard Time';
$lang['UM5']	= '(UTC -5:00) Eastern Standard Time, Western Caribbean Standard Time';
$lang['UM45']	= '(UTC -4:30) Venezuelan Standard Time';
$lang['UM4']	= '(UTC -4:00) Atlantic Standard Time, Eastern Caribbean Standard Time';
$lang['UM35']	= '(UTC -3:30) Newfoundland Standard Time';
$lang['UM3']	= '(UTC -3:00) Argentinien, Brasilien, Französisch-Guayana, Uruguay';
$lang['UM2']	= '(UTC -2:00) Südgeorgien / die Südlichen Sandwichinseln';
$lang['UM1']	= '(UTC -1:00) Azoren, Kap Verde Inseln';
$lang['UTC']	= '(UTC) Greenwich Mean Time, Westeuropäische Zeit';
$lang['UP1']	= '(UTC +1:00) Mitteleuropäische Zeit, West Africa Time';
$lang['UP2']	= '(UTC +2:00) Central Africa Time, Osteuropäische Zeit, Kaliningrad';
$lang['UP3']	= '(UTC +3:00) Moskau Zeit, East Africa Time';
$lang['UP35']	= '(UTC +3:30) Iran Standard Time';
$lang['UP4']	= '(UTC +4:00) Aserbaidschan, Samara';
$lang['UP45']	= '(UTC +4:30) Afghanistan';
$lang['UP5']	= '(UTC +5:00) Pakistan, Jekaterinburg';
$lang['UP55']	= '(UTC +5:30) Indian Standard Time, Sri Lanka';
$lang['UP575']	= '(UTC +5:45) Nepal';
$lang['UP6']	= '(UTC +6:00) Bangladesch, Bhutan, Omsk';
$lang['UP65']	= '(UTC +6:30) Kokosinseln, Myanmar';
$lang['UP7']	= '(UTC +7:00) Krasnojarsk, Kambodscha, Laos, Thailand, Vietnam';
$lang['UP8']	= '(UTC +8:00) Australian Western Standard Time, Beijing, Irkutsk';
$lang['UP875']	= '(UTC +8:45) Australian Central Western Standard Time';
$lang['UP9']	= '(UTC +9:00) Japan/Korea Standard Time, Jakutsk';
$lang['UP95']	= '(UTC +9:30) Australian Central Standard Time';
$lang['UP10']	= '(UTC +10:00) Australian Eastern Standard Time, Vladivostok';
$lang['UP105']	= '(UTC +10:30) Lord-Howe-Insel';
$lang['UP11']	= '(UTC +11:00) Magadan, Salomonen, Vanuatu';
$lang['UP115']	= '(UTC +11:30) Norfolkinsel';
$lang['UP12']	= '(UTC +12:00) Fidschi, Gilbertinseln, Region Kamtschatka, New Zealand Standard Time';
$lang['UP1275']	= '(UTC +12:45) Chatham-Inseln';
$lang['UP13']	= '(UTC +13:00) Phoenixinseln, Tonga';
$lang['UP14']	= '(UTC +14:00) Line Islands';


/* End of file date_lang.php */
/* Location: ./system/language/english/date_lang.php */