<?php

$lang['profile_general']                = 'Allgemein';
$lang['profile_change_password']        = 'Passwort ändern';
$lang['profile_database']               = 'Datenbank';
$lang['profile_storage']                = 'Speicher';
$lang['profile_notification']           = 'Benachrichtigung';
$lang['profile_msg_notification_save']  = 'Benachrichtigungen wurde gespeichert !';
$lang['profile_placeholder_email']      = 'Email tippen und Enter drücken';
$lang['profile_material']               = 'Material';
$lang['profile_selection']              = 'Auswahl erlauben';
$lang['profile_option']                 = 'Mobile Option';
$lang['profile_default_coordinate']     = 'Voreingestellte koordinate';
$lang['profile_msg_coordinate_save']    = 'Voreingestellte koordinate der Stadt wurde gespeichert!';

# 20 Feb 2014
$lang['profile_notification_timezone']  = 'Benachrichtigung und Zeitzone';
$lang['profile_timezone_chosser']       = 'Ihre Zeitzone auswählen';
$lang['profile_timezone']               = 'Zeitzone';

# 03 Jun 2014
$lang['profile_post_service']           = 'Web Auftragsbuch';
$lang['profile_post_service_url']       = 'URL';
$lang['profile_post_service_dir']       = 'Dir path';
$lang['profile_post_service_msg']       = 'Information des Postdienstes wurde gespeichert!';