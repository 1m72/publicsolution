<?php

$lang['cpn_holder_company_name']                           = "Unternehmensname";
$lang['cpn_lbl_city']                                      = "Stadt";
$lang['cpn_lbl_description']                               = "Beschreibung";
$lang['cpn_lbl_list_company']                              = "Unternehmensliste";
$lang['cpn_msg_question_delete_company']                   = "Sind Sie sicher, dieses Unternehmen zu löschen?";
$lang['cpn_lbl_name']                                      = "Name";
$lang['cpn_lbl_id']                                        = "Nummer";
$lang['cpn_msg_hello']                                     = "Hallo";
$lang['cpn_msg_company_information_has_been_saved']        = "Informationen der Unternehmung wurden gespeichert.";
$lang['cpn_msg_company_has_been_deleted']                  = "Das Unternehmen wurde gelöscht";
$lang['cpn_lbl_company']                                   = "Unternehmen";
$lang['cpn_lbl_company_detail']                            = "Einzelheiten über Unternehmen";