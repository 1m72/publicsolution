<?php
$lang['module_holder_id_ref']                  = "Id_ref";
$lang['module_holder_title']                   = "Module name";
$lang['module_lbl_description']                = "Description";
$lang['module_lbl_detail']                     = "Module detail";
$lang['module_lbl_id']                         = "ID";
$lang['module_lbl_list']                       = "List module";
$lang['module_lbl_title']                      = "Module name";
$lang['module_msg_has_been_deleted']           = "Module has been deleted";
$lang['module_msg_information_has_been_saved'] = "Module information has been saved";
$lang['module_msg_question_delete']            = "Are you sure want to delete this module?";

#--- 2 May 2014
$lang['module_lbl_email']                      = "Email";
$lang['module_msg_update_email']               = "Email notification has been saved";

#--- 5 May 2014
$lang['module_lbl_tip_email']                  = 'Input your email address and press <strong>Enter</strong> to add email';