<?php

$lang['cpn_holder_company_name']                           = "Company name";
$lang['cpn_lbl_city']                                      = "City";
$lang['cpn_lbl_description']                               = "Description";
$lang['cpn_lbl_list_company']                              = "List Company";
$lang['cpn_msg_question_delete_company']  				   = "Are you sure want to delete this company?";
$lang['cpn_lbl_name']                                      = "Name";
$lang['cpn_lbl_id']                                        = "ID";
$lang['cpn_msg_hello']                                     = "Hello";
$lang['cpn_msg_company_information_has_been_saved']        = "Company information has been saved";
$lang['cpn_msg_company_has_been_deleted']                  = "Company has been deleted";
$lang['cpn_lbl_company']                                   = "Company";
$lang['cpn_lbl_company_detail']                            = "Company detail";