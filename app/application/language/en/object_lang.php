<?php

$lang['object_object_detail']           = "Object detail";
$lang['object_grid_id']                 = "ID";
$lang['object_grid_ref']                = "Ref";
$lang['object_grid_city']               = "City";
$lang['object_grid_parent']             = "Parent";
$lang['object_grid_group']              = "Group";
$lang['object_grid_position']           = "Position";
$lang['object_grid_name']               = "Name";
$lang['object_list_object']             = "List object";
$lang['object_msg_delete']              = "Are you sure want to delete this object?";
$lang['object_lbl_object_name']         = "Object name";
$lang['object_lbl_ref_name']            = "Ref";
$lang['object_lbl_city_name']           = "City";
$lang['object_lbl_parent_name']         = "Parent";
$lang['object_lbl_group_name']          = "Group object";
$lang['object_lbl_position']            = "Position";
$lang['object_msg_save_successful']     = "Object information has been saved";
$lang['object_msg_delete_successful']   = "object has been deleted";
$lang['object_lbl_select_group']        = "---Select group object---";
$lang['object_msg_delete_position']     = "Are you sure want to delete this position ?";
$lang['object_lbl_group_place']         = "Group place";