<?php

$lang['profile_general']                = 'General';
$lang['profile_change_password']        = 'Change Password';
$lang['profile_database']               = 'Database';
$lang['profile_storage']                = 'Storage';
$lang['profile_notification']           = 'Notification';
$lang['profile_msg_notification_save']  = 'Notification information has been saved!';
$lang['profile_placeholder_email']      = 'Type email and hit enter';
$lang['profile_material']               = 'Material';
$lang['profile_selection']              = 'Allow selection';
$lang['profile_option']                 = 'Mobile option';
$lang['profile_default_coordinate']     = 'Default Coordinate';
$lang['profile_msg_coordinate_save']    = 'Default coordinate of the city has been saved!';

# 20 Feb 2014
$lang['profile_notification_timezone']  = 'Notification & time zone';
$lang['profile_timezone_chosser']       = 'Choose your time zone';
$lang['profile_timezone']               = 'Time zone';

# 03 Jun 2014
$lang['profile_post_service']           = 'Post service';
$lang['profile_post_service_url']       = 'URL';
$lang['profile_post_service_dir']       = 'Dir path';
$lang['profile_post_service_msg']       = 'Post service information has been saved!';