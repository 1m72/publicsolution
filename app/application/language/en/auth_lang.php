<?php

$lang['auth_err_login_fail']      = "Your username or password is invalid";
$lang['auth_msg_forgot_request']  = "An email has been sent to you !";
$lang['auth_msg_forgot_validate'] = "New password has been send to your email !";
$lang['auth_err_forgot_validate'] = "Your information is invalid !";
$lang['msg_please_sign_in']       = "Please sign in";
$lang['lbl_remember_me']          = "Remember me";
$lang['lbl_sign_in']              = "Sign in";
$lang['lbl_forgot_password']      = "Forgot password";
$lang['lbl_goto_login_page']      = "Go to login page";
$lang['lbl_login']                = "Login";

#2014-01-03
$lang['holder_username']          = "Enter your username ...";
$lang['holder_password']          = "Enter your password ...";
$lang['holder_captcha']           = "Enter captcha ...";