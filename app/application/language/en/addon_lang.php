<?php

$lang['addon_holder_id_ref']                  = "Id_ref";
$lang['addon_holder_name']                    = "Addon name";
$lang['addon_lbl']                            = "Addon";
$lang['addon_lbl_name']                       = "Addon name";
$lang['addon_lbl_detail']                     = "Addon detail";
$lang['addon_lbl_id']                         = "ID";
$lang['addon_lbl_id_ref']                     = "Id_ref";
$lang['addon_lbl_list']                       = "List addon";
$lang['addon_msg_has_been_deleted']           = "Addon has been deleted";
$lang['addon_msg_information_has_been_saved'] = "Addon information has been saved";
$lang['addon_msg_question_delete']            = "Are you sure want to delete this addon?";

#--- 09 May 2014
$lang['addon_lbl_assign_more']                = "Assign more";

#--- 12 Mar 2014
$lang['addon_lbl_show_machne_assigned']       = 'Display assigned';

#--- 22 May 2014
$lang['addon_cbo_used_for_module']            = 'Addon used for this module';