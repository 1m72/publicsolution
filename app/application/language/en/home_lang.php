<?php

$lang['lbl_hello_apps'] = "Hello apps !";

#2013/12/25
$lang['home_error_404']               = "404 page - The page not found.";
$lang['home_error_403']               = "403 page - You don't have permission view this page.";
$lang['home_click']                   = "Click here";
$lang['home_back_home']               = "to back homepage.";
$lang['home_comming']                 = "Comming ...";