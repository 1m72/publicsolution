<?php

$lang['lbl_hello_employee_group']            = "Hello employee group !";
$lang['lbl_title']                           = "Title";
$lang['lbl_cost_center']                     = "Cost center";
$lang['lbl_id']                              = "ID";
$lang['lbl_list_employee_group']             = "List Employee group";
$lang['lbl_employee_group']                  = "Employee group";
$lang['lbl_employee_group_detail']           = "Employee group detail";
$lang['msg_question_delete']                 = "Are you sure want to delete this employee group?";
$lang['msg_employee_group_has_been_deleted'] = "Employee group has been deleted";
$lang['msg_employee_group_saved']            = "Employee group information has been saved";
$lang['employee_group_holder_title']         = "Title";
$lang['employee_group_search_placeholder']   = 'Enter keyword to search ...';
