<?php

$lang['nfc_holder_id_ref']                  = "Id_ref";
$lang['nfc_holder_name']                    = "NFC name";
$lang['nfc_lbl']                            = "NFC";
$lang['nfc_lbl_name']                       = "NFC code";
$lang['nfc_lbl_actived']                    = "Actived";
$lang['nfc_lbl_value']                      = "Value";
$lang['nfc_lbl_detail']                     = "NFC detail";
$lang['nfc_lbl_id']                         = "ID";
$lang['nfc_lbl_id_ref']                     = "Id_ref";
$lang['nfc_lbl_list']                       = "List NFC";
$lang['nfc_msg_has_been_deleted']           = "NFC has been deleted";
$lang['nfc_msg_information_has_been_saved'] = "NFC information has been saved";
$lang['nfc_msg_question_delete']            = "Are you sure want to delete this NFC?";
$lang['nfc_total']                          = "Number of NFC code";
$lang['nfc_holder_total']                   = "Enter number of NFC code you want add";
