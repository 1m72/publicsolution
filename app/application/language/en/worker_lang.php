<?php

$lang['lbl_hello_worker']                = "Hello worker !";
$lang['lbl_worker_index']                = "Worker index";

#--- 26-Nov-13
$lang['holder_address']                  = "Address";
$lang['holder_first_name']               = "First Name";
$lang['holder_last_name']                = "Last Name";
$lang['holder_personal_code']            = "Personal Code";
$lang['holder_phone']                    = "Phone";
$lang['lbl_address']                     = "Address";
$lang['lbl_birthday']                    = "Birthday";
$lang['lbl_city']                        = "City";
$lang['lbl_company']                     = "Company";
$lang['lbl_first_name']                  = "First Name";
$lang['lbl_id']                          = "ID";
$lang['lbl_last_name']                   = "Last Name";
$lang['lbl_list_worker']                 = "List worker";
$lang['lbl_nfc_code']                    = "NFC_Code";
$lang['lbl_personal_code']               = "Personal Code";
$lang['lbl_phone']                       = "Phone";
$lang['lbl_select_nfc_code']             = "---Select NFC Code---";
$lang['lbl_worker']                      = "Worker";
$lang['lbl_worker_detail']               = "Worker detail";
$lang['msg_question_delete']             = "Are you sure want to delete this worker?";
$lang['msg_worker_has_been_deleted']     = "Worker has been deleted";
$lang['msg_worker_saved']                = "Worker information has been saved";
$lang['wk_lbl_nfc_code']                 = "NFC code";

#--- 21 May 2014
$lang['worker_lbl_assign_more']          = "Assign more";
$lang['worker_lbl_show_machne_assigned'] = 'Display assigned';

#--- 18 Jul 2014
$lang['worker_lbl_price']                = "Price";
$lang['worker_holder_price']             = "Price";
$lang['worker_lbl_fullname']             = "Full name";
