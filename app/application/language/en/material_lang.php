<?php
$lang['material_holder_unit']                    = "unit";
$lang['material_holder_name']                    = "name";
$lang['material_lbl_detail']                     = "Material detail";
$lang['material_lbl_list']                       = "List Material";
$lang['material_lbl_name']                       = "Material name";
$lang['material_lbl_unit']                       = "Unit";
$lang['material_msg_has_been_deleted']           = "Material has been deleted";
$lang['material_msg_information_has_been_saved'] = "Material information has been saved";
$lang['material_msg_question_delete']            = "Are you sure want to delete this material?";