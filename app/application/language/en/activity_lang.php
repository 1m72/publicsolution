<?php

$lang['activity_holder_id_ref']                  = "Id_ref";
$lang['activity_holder_name']                    = "Activity name";
$lang['activity_lbl']                            = "Activity";
$lang['activity_lbl_name']                       = "Activity name";
$lang['activity_lbl_color']                      = "Color";
$lang['activity_lbl_detail']                     = "Activity detail";
$lang['activity_lbl_id_ref']                     = "Id_ref";
$lang['activity_lbl_list']                       = "List activity";
$lang['activity_msg_has_been_deleted']           = "Activity has been deleted";
$lang['activity_msg_information_has_been_saved'] = "Activity information has been saved";
$lang['activity_msg_question_delete']            = "Are you sure want to delete this activity?";
$lang['activity_lbl_color']                      = "Color";
