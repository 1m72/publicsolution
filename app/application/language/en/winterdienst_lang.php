<?php

$lang['lbl_by_date']                                         = "By date";
$lang['lbl_by_worker']                                       = "By worker";
$lang['lbl_by_object']                                       = "By object";
$lang['lbl_select_object']                                   = "--- Select object ---";
$lang['lbl_your_browser_does_not_support_this_audio_format'] = 'Your browser does not support this audio format.';
$lang['lbl_gps_record']                                      = "GPS Record";
$lang['lbl_worker']                                          = "Worker :";
$lang['lbl_object']                                          = "Object :";
$lang['lbl_start']                                           = "Start";
$lang['lbl_end']                                             = "End ";
$lang['lbl_date']                                            = "Date :";
$lang['lbl_info']                                            = "Infor :";
$lang['lbl_video']                                           = "Video";
$lang['lbl_image']                                           = "Image";
$lang['lbl_voice']                                           = "Voice";
$lang['lbl_view_detail']                                     = "View detail";
$lang['lbl_information']                                     = "Information";
$lang['lbl_map']                                             = "Map";
$lang['lbl_empty']                                           = "--- Empty ---";
$lang['lbl_select_worker']                                   = "--- Select worker ---";
$lang['lbl_winterdienst_detail']                             = "Winterdienst Detail";
$lang['lbl_voice_meno']                                      = "Voice memo";
$lang['lbl_video_file_demo']                                 = "Video file demo";
$lang['lbl_close']                                           = "Close";

#--- 11 Nov 2013
$lang['lbl_by_machine']                                      = "By machine";
$lang['lbl_select_machine']                                  = "--- Select machine ---";

#---09 Jan 2014
$lang['btn_ok']                                              = "OK GO";
$lang['lbl_tab_activities']                                  = "Activities";
$lang['lbl_message']                                         = "Message";
$lang['lbl_activity']                                        = "Activity";

$lang['lbl_select_street']                                  = "--- Select street ---";
$lang['lbl_street']                                          = "Street";

$lang['btn_detail']                                          = "Detail";

#--- 14 Jan 2014
$lang['lbl_no_data']                                         = "No data";

#--- 21 Jan 2014
$lang['lbl_duration']                                        = "Duration";

#--- 26 Jan 2014
$lang['time']                                                = "Time";
$lang['latitude']                                            = "Latitude";
$lang['longitude']                                           = "Longitude";

$lang['lbl_data_processing']                                 = "data processing";

#--- 14 Feb 2014
$lang['lbl_print_tip']                                       = "Click here to print gps detail";
$lang['error_gps_404']                                       = "GPS record not found";
$lang['error_gps_activity_empty']                            = "GPS activities is not available";

#--- 3 Mar 2014
$lang['lbl_duration_total']                                  = 'Total';