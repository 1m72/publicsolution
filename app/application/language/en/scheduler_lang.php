<?php
$lang['sd_lbl_schedule']                   = "Schedule";
$lang['sd_msg_schedule_saved']             = "Schedule information has been saved";
$lang['sd_lbl_group']                      = "Group";
$lang['sd_lbl_worker']                     = "Worker";
$lang['sd_lbl_title']                      = "Title";
$lang['sd_lbl_starttime']                  = "Start";
$lang['sd_lbl_endtime']                    = "End";
$lang['sd_lbl_des']                        = "Description";
$lang['sd_msg_has_been_deleted']           = "Schedule has been deleted";
// 05-29-2014
$lang['sd_delete_confirmation']            = "Are you sure you want to delete this schedule?";
$lang['sd_msg_date_datenow']               = "Assigned time must be greater than the current time";
$lang['sd_msg_date']                       = "End date must be greater than start date";
$lang['sd_msg_create']                     = "You can only schedule the date which is 5 days greater than the current date! ";
$lang['sd_validate_workergroup']           = "The Group or Worker field is required.";
$lang['sd_search']                         = "Search ...";
$lang['sd_schedule_detail']                = "Schedule detail";
$lang['sd_lbl_list']                       = "List Schedule";