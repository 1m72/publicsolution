<?php

$lang['mc_holder_name']                            = "Machine name";
$lang['mc_holder_id_ref']                          = "Id_ref";
$lang['mc_lbl_id_ref']                             = "Id_ref";
$lang['mc_lbl_nfc_code']                           = "NFC_code";
$lang['mc_lbl_select_nfc_code']                    = "---Select NFC---";
$lang['mc_lbl_city']                               = "City";
$lang['mc_lbl_description']                        = "Description";
$lang['mc_lbl_list_machine']                       = "List Machine";
$lang['mc_msg_question_delete_machine']            = "Are you sure want to delete this machine?";
$lang['mc_lbl_id']                                 = "ID";
$lang['mc_lbl_machine_name']                       = "Machine name";
$lang['mc_msg_machine_information_has_been_saved'] = "Machine information has been saved";
$lang['mc_msg_machine_has_been_deleted']           = "Machine has been deleted";
$lang['mc_lbl_machine_detail']                     = "Machine detail";
$lang['mc_lbl_machine']                            = "Machine";
$lang['mc_lbl_addon']                              = "Add-on";
$lang['mc_lbl_select_addon']                       = "--- Select Add-on ---";

#--- 09 May 2014
$lang['mc_lbl_assign_more']                        = "Assign more";

#--- 12 Mar 2014
$lang['mc_lbl_show_machne_assigned']               = 'Display assigned';

#--- 21 May 2014
$lang['mc_cbo_used_for_module']                    = 'Machine used for this module';