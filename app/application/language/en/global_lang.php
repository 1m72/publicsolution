<?php

$lang['email_format']                          = "Your email is invalid";
$lang['err_captcha_invalid']                   = "Captcha is invalid";
$lang['lbl_aufgaben']                          = "Task";
$lang['lbl_communication']                     = "Communication";
$lang['lbl_einrichtung']                       = "Facility";
$lang['lbl_fasttask']                          = "Fasttask";
$lang['lbl_home']                              = "Home";
$lang['lbl_item']                              = "Item";
$lang['lbl_language']                          = "Language";
$lang['lbl_login']                             = "Login";
$lang['lbl_logout']                            = "Logout";
$lang['lbl_posteingang_40']                    = "Inbox (40)";
$lang['lbl_profile']                           = "Profile";
$lang['lbl_programme_heute']                   = "Program/Today";
$lang['lbl_public_solution']                   = "BIS-Office";
$lang['lbl_snippets']                          = "Snippets";
$lang['lbl_toggle_navigation']                 = "Toggle navigation";
$lang['lbl_winterdienst']                      = "Winterdienst";

// 11 Nov 2013
$lang['err_api_connection']                    = "Has an error when connecting to database";
$lang['err_api_save']                          = "Has an error when saving data";
$lang['btn_save_change']                       = "Save change";
$lang['btn_cancel']                            = "Cancel";
$lang['btn_edit']                              = "Edit";
$lang['btn_delete']                            = "Delete";
$lang['lbl_item_per_page']                     = "data items per page";

// 2013-11-20
$lang['btn_yes']                               = "Yes";
$lang['btn_no']                                = "No";
$lang['btn_add_new']                           = "Add new";
$lang['err_has_an_error_when_update_database'] = "Has an error when update database";
$lang['grid_option']                           = "Option";
$lang['delete_confirm']                        = "Confirm";
$lang['alert_error']                           = "error";
$lang['btn_show']                              = "Show";
$lang['btn_hide']                              = "Hide";

// 2013_11_21
$lang['btn_on']                                = "AVAILABLE";
$lang['btn_off']                               = "UNAVAILABLE";
$lang['grid_updated_at']                       = "Updated at";
$lang['grid_created_at']                       = "Created at";
$lang['lbl_select_city']                       = "---Select City---";
$lang['lbl_select_company']                    = "---Select Company---";
$lang['lbl_select_class']                      = "---Select Class---";

// 2013-11-25
$lang['err_unique_username']                   = "The username field must contain a unique value.";
$lang['err_unique_email']                      = "The email field must contain a unique value.";

// 2013-12-03
$lang['crumb_user']                            = "User";
$lang['crumb_city']                            = "City";
$lang['crumb_company']                         = "Company";
$lang['crumb_machine']                         = "Machine";
$lang['crumb_device']                          = "Device";
$lang['crumb_object']                          = "Object";
$lang['crumb_worker']                          = "Worker";
$lang['crumb_modules']                         = "Modules";
$lang['dropdown_user']                         = "List Users";
$lang['dropdown_city']                         = "List City";
$lang['dropdown_company']                      = "List company";
$lang['dropdown_machine']                      = "List machine";
$lang['dropdown_device']                       = "List device";
$lang['dropdown_object']                       = "List object";

// 2013-12-05
$lang['grid_number']                           = "No.";

// 2013-12-11
$lang['lbl_select_activity']                   = "--- Select Activity ---";

// 2013-12-12
$lang['lbl_select_default']                    = "---Select Activity Default---";

// 2013-12-13
$lang['message_activity']                      = "You must select at least 2 activity!";

// 2013-12-17
$lang['crumb_addon']                           = "Add on";

// 2013-12-17
$lang['crumb_activity']                        = "Activity";

// 2013-12-23
$lang['crumb_nfc']                             = "NFC";

// 2013-12-24
$lang['lbl_add_new']                           = "Add new";

// 2013-12-25
$lang['lbl_activity']                          = "Activity";
$lang['lbl_activity_default']                  = "Activity default";

// 2013-12-26
$lang['grid_group_column']                     = "Drag a column header and drop it here to group by that column";

// 2013-12-31
$lang['crumb_user_group']                      = "User Group";

// 2014-01-03
$lang['language_en']                           = "English";
$lang['language_de']                           = "Deutsch";

// 2014-01-09
$lang['language_copyright']                    = "&copy; 2014";

// 2014-01-10
$lang['loading']                               = "Loading ...";
$lang['waiting']                               = "Waiting ...";
$lang['unavailable']                           = "Unavailable";

// 4 Mar 2014
$lang['lbl_identifier']                        = "Identifier";
$lang['holder_identifer']                      = "";

// 7 Mar 2014
$lang['lbl_module']                            = "Module";


// 4 Apr 2014
$lang['crumb_components']                      = "Components";
$lang['lbl_street_checking']                   = "Streetchecking";
$lang['lbl_street_cleaning']                   = "Streetcleaning";
$lang['lbl_select_module']                     = "--- Select module ---";

// 2 May 2014
$lang['lbl_map_tracker']                       = "Visualization Map";
$lang['lbl_module_setting']                    = "Module setting";

// 16 June 2014
$lang['crumb_group']                           = "Group";

// 2 Jul 2014
$lang['nav_notification']                      = "Notification";
$lang['msg_data_not_available']                = "Data is not available";

// 7 July 2014
$lang['lbl_module_scheduler']                  = "Scheduler";

// 27 August 2014
$lang['lbl_material']                          = "Material";

// 02 July 2014
$lang['crumb_superior']                        = "Superior";
$lang['crumb_object_group']                    = "Object group";
$lang['crumb_object_place']                    = "Object place";
