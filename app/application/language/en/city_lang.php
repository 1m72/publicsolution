<?php

$lang['city_city_detail']           = "City detail";
$lang['city_grid_id']               = "ID";
$lang['city_grid_name']             = "Name";
$lang['city_grid_database']         = "Database";
$lang['city_grid_storage']          = "Storage";
$lang['city_list_city']             = "List city";
$lang['city_msg_delete']            = "Are you sure want to delete this city?";
$lang['city_lbl_city_name']         = "City name";
$lang['city_lbl_passcode']          = "Passcode";
$lang['city_lbl_database']          = "Database";
$lang['city_lbl_hostname']          = "Hostname";
$lang['city_lbl_username']          = "Username";
$lang['city_lbl_password']          = "Password";
$lang['city_lbl_password_confirm']  = "Password Confirm";
$lang['city_lbl_port']              = "Port";
$lang['city_lbl_storage']           = "Storage";
$lang['city_lbl_type']              = "Type";
$lang['city_lbl_ip']                = "Ip";
$lang['city_lbl_home_dir']          = "Home dir";
$lang['city_msg_save_successful']   = "City information has been saved";
$lang['city_msg_delete_successful'] = "City has been deleted";

#2013-12-04
$lang['city_btn_on']                = "ON";
$lang['city_btn_off']               = "OFF";
