<?php
$lang['notification_lbl_header']       = 'Web book order';
$lang['notification_lbl_filter_date']  = 'Filter by date : ';
$lang['notification_lbl_uri']          = 'URI';
$lang['notification_lbl_result']       = 'Result';
$lang['notification_lbl_last_request'] = 'Last request';
$lang['notification_lbl_retry']        = 'Retry';
$lang['notification_lbl_attachments']  = 'Attachments';
$lang['notification_lbl_download']     = 'Download';
