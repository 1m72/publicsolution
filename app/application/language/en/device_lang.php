<?php 

$lang['dv_msg_device_information_has_been_saved']   = "Device information has been saved";
$lang['dv_msg_device_has_been_deleted']             = "Device has been deleted";
$lang['dv_lbl_device_detail']                       = "Device detail";
$lang['dv_lbl_id']                                  = "ID";
$lang['dv_lbl_device']                              = "Device";
$lang['dv_lbl_actived']                             = "Actived";
$lang['dv_lbl_list_device']                         = "List Device";
$lang['dv_msg_question_delete_device']              = "Are you sure want to delete this device?";
$lang['dv_lbl_name']                                = "Name";
$lang['dv_holder_device']                           = "Device name";
$lang['dv_lbl_city']                                = "City";
$lang['dv_lbl_accept']                              = "Accept";
$lang['dv_lbl_do_not_accept']                       = "Don't accept";
$lang['dv_btn_on']                                  = "ON";
$lang['dv_btn_off']                                 = "OFF";