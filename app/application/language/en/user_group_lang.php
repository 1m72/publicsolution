<?php

$lang['lbl_groupname']             = "Group name";
$lang['lbl_usergroup_detail']      = "User group detail";
$lang['msg_question_delete']       = "Are you sure want to delete this user group?";
$lang['lbl_list_user_group']       = "List user group";
$lang['msg_usergroup_deleted']     = 'User group has been deleted';
$lang['msg_usergroup_saved']       = "User group information has been saved!";