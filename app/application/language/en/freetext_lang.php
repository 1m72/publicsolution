<?php

$lang['freetxt_holder_id_ref']                  = "Id_ref";
$lang['freetxt_holder_name']                    = "Freetext name";
$lang['freetxt_lbl']                            = "Freetext";
$lang['freetxt_lbl_title']                      = "Title";
$lang['freetxt_lbl_detail']                     = "Freetext detail";
$lang['freetxt_lbl_id']                         = "ID";
$lang['freetxt_lbl_id_ref']                     = "Id_ref";
$lang['freetxt_lbl_list']                       = "List freetext";
$lang['freetxt_msg_has_been_deleted']           = "Freetext has been deleted";
$lang['freetxt_msg_information_has_been_saved'] = "Freetext information has been saved";
$lang['freetxt_msg_question_delete']            = "Are you sure want to delete this freetext?";
$lang['freetxt_lbl_assign_more']                = "Assign more";
$lang['freetxt_lbl_show_machne_assigned']       = 'Display assigned';
$lang['freetxt_cbo_used_for_module']            = 'freetext used for this module';
$lang['freetxt_level1']                         = 'Parent level1';
$lang['freetxt_level2']                         = 'Parent level2';