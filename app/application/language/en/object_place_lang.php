<?php

$lang['ob_place_holder_id_ref']                  = "Id_ref";
$lang['ob_place_holder_name']                    = "Object place name";
$lang['ob_place_lbl']                            = "Object place";
$lang['ob_place_lbl_title']                      = "Title";
$lang['ob_place_lbl_detail']                     = "Object place detail";
$lang['ob_place_lbl_id']                         = "ID";
$lang['ob_place_lbl_id_ref']                     = "Id_ref";
$lang['ob_place_lbl_list']                       = "List Object place";
$lang['ob_place_msg_has_been_deleted']           = "Object place has been deleted";
$lang['ob_place_msg_information_has_been_saved'] = "Object place information has been saved";
$lang['ob_place_msg_question_delete']            = "Are you sure want to delete this Object place?";
$lang['ob_place_lbl_assign_more']                = "Assign more";
$lang['ob_place_lbl_show_machne_assigned']       = 'Display assigned';
$lang['ob_place_cbo_used_for_module']            = 'Object place used for this module';
$lang['ob_place_level1']                         = 'Parent level1';
$lang['ob_place_level2']                         = 'Parent level2';