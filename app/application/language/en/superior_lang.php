<?php

$lang['lbl_hello_superior']                = "Hello Superior !";
$lang['lbl_superior_index']                = "Superior index";

#--- 26-Nov-13
$lang['holder_address']                  = "Address";
$lang['holder_first_name']               = "First Name";
$lang['holder_last_name']                = "Last Name";
$lang['holder_personal_code']            = "Personal Code";
$lang['holder_phone']                    = "Phone";
$lang['lbl_address']                     = "Address";
$lang['lbl_birthday']                    = "Birthday";
$lang['lbl_city']                        = "City";
$lang['lbl_company']                     = "Company";
$lang['lbl_first_name']                  = "First Name";
$lang['lbl_id']                          = "ID";
$lang['lbl_last_name']                   = "Last Name";
$lang['lbl_list_superior']               = "List Superior";
$lang['lbl_nfc_code']                    = "NFC_Code";
$lang['lbl_personal_code']               = "Personal Code";
$lang['lbl_phone']                       = "Phone";
$lang['lbl_select_nfc_code']             = "---Select NFC Code---";
$lang['lbl_superior']                    = "Superior";
$lang['lbl_superior_detail']             = "Superior detail";
$lang['msg_question_delete']             = "Are you sure want to delete this superior?";
$lang['msg_superior_has_been_deleted']   = "Superior has been deleted";
$lang['msg_superior_saved']              = "Superior information has been saved";
$lang['sp_lbl_nfc_code']                 = "NFC code";

#--- 21 May 2014
$lang['superior_lbl_assign_more']          = "Assign more";
$lang['superior_lbl_show_machne_assigned'] = 'Display assigned';

#--- 18 Jul 2014
$lang['superior_lbl_price']                = "Price";
$lang['superior_holder_price']             = "Price";
$lang['superior_lbl_fullname']             = "Full name";
