<?php

$lang['lbl_username']             = "Username";
$lang['lbl_email']                = "Email";
$lang['lbl_group']                = "Group";
$lang['lbl_company']              = "Company";
$lang['lbl_option']               = "Option";
$lang['lbl_btn_add']              = "Add new";
$lang['lbl_list_user']            = "List users";
$lang['lbl_user_detail']          = "User detail";

#--- 19 Nov 2013
$lang['lbl_select_group']         = '--- Select user group ---';
$lang['msg_user_deleted']         = 'User has been deleted';

#--- 21 Nov 2013
$lang['msg_user_saved']           = "User information has been saved";
$lang['lbl_id']                   = "ID";
$lang['msg_question_delete_user'] = "Are you sure want to delete this user?";
$lang['lbl_password']             = "Password";
$lang['lbl_password_confirm']     = "Password Confirm";
$lang['lbl_city']                 = "City";

#---2013-12-17
$lang['lbl_new_password']         = "New password";
$lang['lbl_new_password_confirm'] = "New password confirm";
$lang['lbl_last_login']           = "Last login";
$lang['lbl_created_at']           = "Created at";
$lang['msg_old_password']         = "Old password is not correct!";

#---2013-12-24
$lang['test_connect']             = 'Test Connect';
$lang['connect_success']          = 'Test connection succeeded!';
$lang['connect_fail']             = 'Test connection failed! Please check your database settings!';
$lang['msg_password_changed']     = 'Your password has been changed !';
$lang['msg_database_save']        = 'Database information has been saved !';
$lang['msg_storage_save']         = 'Storage information has been saved !';
