/**
 * Kendoui wrapper
 * @author Chien Tran <tran.duc.chien@kloon.vn>
 */

var grid_number = 0;
function refresh_grid(target) {
    $(target).data("kendoGrid").dataSource.read();
    $(target).data('kendoGrid').refresh();
}

function create_grid(grid_config){
    var grid_instance, grid_target, grid_height, grid_limit, grid_url;

    if (grid_config.hasOwnProperty('target')) {
        grid_target = grid_config.target;
    } else {
        return false;
    }

    if (grid_config.hasOwnProperty('url')) {
        grid_url = grid_config.url;
    } else {
        return false;
    }

    if (grid_config.hasOwnProperty('columns')) {
        grid_columns = grid_config.columns;
    } else {
        return false;
    }

    if (grid_config.hasOwnProperty('height')) {
        grid_height = grid_config.height;
    } else {
        grid_height = global_grid_height;
    }

    if (grid_config.hasOwnProperty('limit')) {
        grid_limit = grid_config.limit;
    } else {
        grid_limit = '';
    }

    grid_instance = $(grid_target).kendoGrid({
        dataSource: {
            transport: {
                read: grid_url,
                "contentType":"application\/json",
                "type":"POST"
            },
            schema: {
                data: "data",
                total: "total"
            },
            pageSize: grid_limit,
            serverPaging: true,
            serverFiltering: (app_env != 'production') ? true : false,
            serverSorting: true
        },
        columns: grid_columns,
        toolbar: kendo.template($("#toolbar_template").html()),
        selectable: "row",
        filterable: (app_env != 'production') ? true : false,
        groupable: true,
        scrollable: true,
        reorderable: true,
        columnMenu: false,
        sortable: {
            mode: "multiple",
            allowUnsort: true
        },
        navigatable: true,
        height: grid_height,
        pageable: {
            pageSize : grid_limit,
            pageSizes: true,
            refresh: true
        },
        dataBinding: function(e) {
            var grid     = $(grid_target).data("kendoGrid");
            _grid_limit  = grid.dataSource.pageSize();
            current_page = grid.dataSource.page();
            grid_number  = (current_page - 1) * _grid_limit;
        }
    });

    return grid_instance;
}

function open_modal(target, content, title) {
    var modal = $(target).data('kendoWindow');
    if (modal === null) {
        return false;
    }

    if (typeof (content) != 'undefined') {
        modal.content(content);
    }

    if (typeof (title) != 'undefined') {
        modal.setOptions({
            title: title,
        });
    }

    if (modal !== null) {
        modal.center();
        modal.open();
    }
}

function close_modal(target) {
    var modal = $(target).data('kendoWindow');
    modal.close();
}

function init_modal(target, modal_title) {
    selector = target;
    selector = selector.replace('#', '');
    selector = selector.replace('.', '');
    type     = target.charAt(0);
    append   = '';
    if (type == '#') {
        append = '<div id="'+selector+'" style="display: none;"></div>';
    } else if (type == '.') {
        append = '<div class="'+selector+'" style="display: none;"></div>';
    }

    if ($(target).length == 0) {
        $('body').append(append);
    }

    if ($(target).length > 0) {
        var modal = $(target).kendoWindow({
            actions: ["Close"],
            draggable: false,
            modal: true,
            pinned: false,
            resizable: false,
            width: "650px",
            title: modal_title ,
        }).data("kendoWindow");
        return modal;
    } else {
        console.log('Init modal '+target+' fail');
    }
    return false;
}
function refresh_treeview(target) {
    $(target).data("kendoTreeView").dataSource.read();
    $(target).data('kendoTreeView').refresh();
}
/* DOM Ready */
$(function(){
    $('body').on('click', '.sys_modal_detail', function(e){
        e.preventDefault();
        i = $(this);
        $.ajax({
            type: 'GET',
            url: i.attr('href'),
            success: function(data){
                open_modal(i.attr('_modal'), data, i.attr('_title'));
            },
            error: function(err){
                alert(_alert_error);
            }
        });
    });
    $('body').on('click', '.sys_modal_delete', function(e){
        e.preventDefault();
        i = $(this);
        var kendoWindow = $("<div />").kendoWindow({
            title: i.attr('_title'),
            resizable: false,
            width: 450,
            modal: true
        });

        kendoWindow.data("kendoWindow")
            .content($("#delete-confirmation").html())
            .center().open();

        kendoWindow
            .find(".delete-confirm, .delete-cancel")
            .click(function() {
                if ($(this).hasClass("delete-confirm")) {
                    var _id_grid = i.attr('_id_grid');
                    $.ajax({
                        type: 'POST',
                        url: i.attr('href'),
                        dataType: 'json',
                        success: function(data){
                            if (data.status == true) {
                                var grid = $(_id_grid).data("kendoGrid");
                                var row = grid.select();
                                var uid = row.data("uid");
                                row.remove();
                                refresh_grid(_id_grid);
                            }
                        },
                        error: function(err){}
                    });
                }

                kendoWindow.data("kendoWindow").close();
            })
            .end()
    });
    $('body').on('click', '.treeview_modal_delete', function(e){
        e.preventDefault();
        i = $(this);
        var kendoWindow = $("<div />").kendoWindow({
            title: i.attr('_title'),
            resizable: false,
            width: 450,
            modal: true
        });

        kendoWindow.data("kendoWindow")
            .content($("#delete-confirmation").html())
            .center().open();

        kendoWindow
            .find(".delete-confirm, .delete-cancel")
            .click(function() {
                if ($(this).hasClass("delete-confirm")) {
                    var _id_treeview = i.attr('_id_treeview');
                    var _data_id = i.attr('_data_id');
                    $.ajax({
                        type: 'POST',
                        url: i.attr('href'),
                        dataType: 'json',
                        success: function(data){
                            if (data.status == true) {
                                var treeView = $(_id_treeview).data("kendoTreeView");
                                var barDataItem = treeView.dataSource.get(_data_id);
                                var barElement = treeView.findByUid(barDataItem.uid);
                                treeView.remove(barElement);
                                refresh_treeview(_id_treeview);
                            }
                        },
                        error: function(err){}
                    });
                }
                kendoWindow.data("kendoWindow").close();
            })
            .end()
    });
});