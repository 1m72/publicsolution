$(function(){
    $('label.tree-toggler').click(function () {
        $(this).parent().parent().children('ul.tree').toggle(300);
    });
    $('.btn-add').click(function(e){
        e.preventDefault();
        $.ajax({
            type: 'GET',
            url: '<?php echo create_url("sys/group/add") ;?>',
            success: function(res) {
                $(".g-detail").html(res).css({display:"block"});
            },
            error: function(err) {}
        });
    });
    $('.edit').click(function(e){
        e.preventDefault();
        i = $(this);
        $.ajax({
            type: 'GET',
            url: '<?php echo create_url("sys/group/edit");?>/'+i.attr('id'),
            success: function(res) {
                $('.g-delete').css({display:"none"});
                $('.msg_success').css({display:"none"});
                $(".g-detail").html(res).css({display:"block"});
            },
            error: function(err) {}
        });
    });
    $('.delete').click(function(){
        $('.g-delete').css({display:"block"});
        $(".g-detail").css({display:"none"});
        $('.msg_success').css({display:"none"});
        workerDelete = $(this).parent().parent();
        w=workerDelete.attr('id');
        g=$(this).parent().parent().parent().parent().attr('id');
        $(".g-detail").css({display:"none"});
        $('.d-ok').click(function(e){
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: '<?php echo create_url("sys/group/remove_worker");?>/'+w+'/'+g,
                success: function(res) {
                    $('.g-delete').css({display:"none"});
                    $('.msg_success').css({display:"block"});
                    workerDelete.remove();
                },
                error: function(err) {}
            });
        });
    });
    $('.delete_group').click(function(){
        groupDelete = $(this).parent().parent();
        g = groupDelete.attr('id');
        $('.g-delete').css({display:"block"});
        $(".g-detail").css({display:"none"});
        $('.msg_success').css({display:"none"});
        $('.d-ok').click(function(e){
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: '<?php echo create_url("sys/group/delete");?>/'+g,
                success: function(res) {
                    $('.g-delete').css({display:"none"});
                    $('.msg_success').css({display:"block"});
                    groupDelete.remove();
                },
                error: function(err) {}
            });
        });
    });
    $('.d-cacel').click(function(){
        $('.g-delete').css({display:"none"});
    });
    $('.select2').select2({ width: 'resolve' });
    $('#frm_group_detail').submit(function(e){
        e.preventDefault();
        i = $(this);
        $.ajax({
            url: i.attr('action'),
            type: 'POST',
            data: i.serialize(),
            success: function(res) {
                $('#frm_group_detail_wraper').html(res);
            },
            error: function(err) {}
        });
    });
    $('[name="check_parent"]').bootstrapSwitch();
    $('[name="check_parent"]').on('switchChange.bootstrapSwitch', function(event, state) {
        if(state){
            $('.worker_wraper').slideDown();
        } else{
            $('.worker_wraper').slideUp();
        }
    });
    $('#select_worker').keyup(function(e){
        e.preventDefault();
        var text_search_worker = $('#select_worker').val();
        var listCheckboxWorker = $('#worker .checkbox');
        $.each(listCheckboxWorker,function(index){
            var item_worker = listCheckboxWorker.eq(index);
            var text = item_worker.text().trim();
            var check = false;
            if(typeof text!= 'undefined' && text!=null){
                var re = new RegExp(text_search_worker, "ig");
                var check = text.match(re);
                if(typeof check !='undefined' && check!=null && check !=''){
                    item_worker.css({display:'block'});
                }else{
                    item_worker.css({display:'none'});
                }
            }
        });
    });
});
