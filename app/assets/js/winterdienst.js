$(function(){
    try {
        map = new OpenLayers.Map("map");
        load_map(map);

        $('.select2').select2({ width: 'resolve' });
        $(".k_dropdownlist").kendoDropDownList();
        $(".k_datepicker").kendoDatePicker({
            format: "dd.MM.yyyy"
        });
        $('.gps_detail').on('click', function(e){
            console.log('click');
            e.preventDefault();
            i = $(this);
            if (i.attr('_active') == 1) {
                return false;
            } else {
                $('.gps_detail').removeAttr("_active");
                i.attr("_active", 1);
            }
            $('.gps_detail').removeAttr('style');
            i.css('font-weight', 'bold');
            index = i.attr('_index');

            // position
            data = gps_data[index]['position'];

            // Activity
            ac_data = gps_data[index]['activity'];

            if (data != 'undefined') {
                add_linestring(map,data,ac_data);
            }

            $('#wrap_activities').fadeIn();
            if (ac_data != 'undefined') {
                display_activity(ac_data);
            }else{
                $('#tab_raw').html('');
            }

            display_info(index);

            btn_view_detail = $('#btn_view_detail');
            btn_view_detail.attr('href', btn_view_detail.attr('href') + '/' + index);
        });

        $('#modal_information').on('hidden.bs.modal', function () {
            $(this).removeData('bs.modal');
        });

        $('#modal_information').on('shown.bs.modal', function () {
            data = $('#gps_information').html();
            $(data).find('ul:first').removeAttr('style');
            $('#modal_gps_information').html(data).fadeIn();
        });
    } catch(e) {
        console.log('winterdienst.js');
        console.log(e);
    }
});

function display_info(gps) {
    try {
        $.ajax({
            type: 'POST',
            url: gps_information_url + '/' + gps,
            dataType: 'json',
            success: function(result){
                var data = result.data;
                var total_infor = data.infor.length;

                $('#total_infor').text('(' + total_infor + ')');

                var infor = data.infor;
                var tab_information = '';
                tab_information += '<table class="table table-hover" style="background: #FFF; margin-bottom: 0px;">';
                if (infor.length > 0) {
                    for(var i = 0; i < infor.length; i++)
                    {
                        var infor_detail = infor[i]['infor'];
                        if(infor_detail.length > 0)
                        {
                            tab_information += '<tr>';
                            tab_information += '<td>#' + (i + 1) + '</td>';
                            tab_information += '<td>Message : ' + infor[i]['message'] + '</td>';
                            tab_information += '<td>Video : ' + infor[i]['video'] + '</td>';
                            tab_information += '<td>Image : ' + infor[i]['image'] + '</td>';
                            tab_information += '<td>Voice : ' + infor[i]['voice'] + '</td>';
                            tab_information += '<th><a href="' + gps_information_url + '/' + gps + '/' + i + '" class="btn btn-xs btn-default btn-success pull-right" data-toggle="modal" data-target="#modal_information" data-backdrop="static" data-keyboard="false">Detail</a></th>';
                            tab_information += '</tr>';
                        }
                    }
                } else {
                    tab_information += '<tr><td align="center">--- Empty ---</td></tr>';
                }
                tab_information += '</tbody>';
                tab_information += '</table>';
                $('#tab_information').html(tab_information);

                $('#information_tab_content').slimScroll({height: $('#information_tab_content').attr('_height')});

                /*try {
                } catch (e) {
                } finally {
                    $('#information_tab_content').slimScroll({height: $('#information_tab_content').attr('_height')});
                }*/

            },
            error: function(){}
        });
    } catch(e) {
        console.log('display_info');
        console.log(e);
    }
}

function display_activity(data) {
    try {
        var count = data.length;
        var tab_raw = '';
        tab_raw += '<table class="table table-hover" style="background: #FFF; margin-bottom: 0px;">';
        if (count > 0)
        {
            tab_raw += '<thead>';
            tab_raw += '<tr>';
            tab_raw += '<th width="30">#</th>';
            tab_raw += '<th width="80">Start</th>';
            tab_raw += '<th width="80">End</th>';
            tab_raw += '<th width="">Activity</th>';
            tab_raw += '<th>Street</th>';
            tab_raw += '</tr>';
            tab_raw += '</thead>';
            tab_raw += '<tbody>';
            for(var i = 0;i < count;i++)
            {
                tab_raw += '<tr>';
                tab_raw += '<td>' + (i + 1) + '</td>';
                tab_raw += '<td>' + data[i]['starttime'] + '</td>';
                tab_raw += '<td>' + data[i]['endtime'] + '</td>';
                tab_raw += '<td>' + data[i]['acname'] + '</td>';
                tab_raw += '<td>' + data[i]['street'] + '</td>';
                tab_raw += '</tr>';
            }
        } else
        {
            tab_raw += '<tbody>';
            tab_raw += '<tr><td colspan="5" align="center">--- Empty ---</td></tr>';
        }
        tab_raw += '</tbody>';
        tab_raw += '</table>';
        $('#tab_raw').html(tab_raw);

        $('#information_tab_content').slimScroll({height: $('#information_tab_content').attr('_height')});
    } catch (e) {
        console.log('display_activity');
        console.log(e);
    }
}