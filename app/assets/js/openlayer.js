var currentPopup,lyrMarkers,lineLayer;
var markers_start_end;
var markers_info;
var popupClass = OpenLayers.Class(OpenLayers.Popup.FramedCloud, {
    "autoSize": true,
    "minSize": new OpenLayers.Size(250, 165),
    "maxSize": new OpenLayers.Size(250, 165),
    "keepInMap": true
});
var bounds = new OpenLayers.Bounds();

function load_map(map,lat,lon){
    try {
        var layer = new OpenLayers.Layer.OSM( "Simple OSM Map");
        var vector = new OpenLayers.Layer.Vector('vector');
        map.addLayers([layer, vector]);

        // add_marker(map, '-0.1279688', '51.5077286', 'Trang');
        var lonLat = new OpenLayers.LonLat(lon,lat);//'49.664264', '10.542148'
        lonLat.transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
            map.getProjectionObject() // to Spherical Mercator Projection
        );
        map.events.register("click", map, function(e) {
            //new OpenLayers.Projection("EPSG:900913"),new OpenLayers.Projection("EPSG:4326")
            var clik_position = map.getLonLatFromPixel(e.xy);
            var clik_latlon = new OpenLayers.LonLat(clik_position.lon,clik_position.lat).transform(map.getProjectionObject(),new OpenLayers.Projection("EPSG:4326"));
            var clik_lat = clik_latlon.lat;
            var clik_lon = clik_latlon.lon;
            map_click_active_activiti('\'' + clik_lat + '\'','\'' + clik_lon + '\'');
        });
        map.setCenter (lonLat, 15);
    } catch (e) {
        console.log(e);
    }
}

function add_marker(map, lat, lon, gps) {
    try {
        var lonLat = new OpenLayers.LonLat(lat, lon);
        lonLat.transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
            map.getProjectionObject() // to Spherical Mercator Projection
        );

        var zoom = 2;
        var markers = new OpenLayers.Layer.Markers( "Markers" );
        map.addLayer(markers);
        marker = new OpenLayers.Marker(lonLat);
        marker.events.register('click', marker, function(evt) {
            //display_info(gps);
        });
        markers.addMarker(marker);
        // map.setCenter (lonLat, zoom);
    } catch (e) {
        console.log(add_marker);
        console.log(e);
    }
}

// Add point
function add_points(map, infor_data){
    // Remove all layer
    try{
        map.removeLayer(lyrMarkers);

        var arrleyer = map.getLayersByName("Polyline");
        for(var ile = 0; ile < arrleyer.length; ile++){
            map.removeLayer(arrleyer[ile]);
        }

        $('.olPopup').css('display','none');

        var arrleyer = map.getLayersByName("Polyline_activiti");
        for(var ile = 0; ile < arrleyer.length; ile++){
            map.removeLayer(arrleyer[ile]);
        }

        var lat = 0;
        var lon = 0;
        // Add marker
        if(infor_data.length != 0){
            lyrMarkers = new OpenLayers.Layer.Markers("Markers");
            map.addLayer(lyrMarkers);
            /* Information*/
            for (var key in infor_data) {
                lat = infor_data[key]['lat'];
                lon = infor_data[key]['lon'];
                var _gps_id     = infor_data[key]['gps_id'];
                var _lat        = infor_data[key]['lat'];
                var _lon        = infor_data[key]['lon'];
                var _msg        = infor_data[key]['message'];
                var _img        = infor_data[key]['image'];
                var _voice      = infor_data[key]['voice'];
                var _video      = infor_data[key]['video'];
                var _data       = infor_data[key]['data'];
                var _type = 'message';
                var _check = false;
                if((parseInt(_msg) > 0) && (parseInt(_img) + parseInt(_voice) + parseInt(_video)) < 1){
                    _type = 'message';
                    _check = true;
                }
                if((parseInt(_img) > 0) && (parseInt(_msg) + parseInt(_voice) + parseInt(_video)) < 1){
                    _type = 'image';
                    _check = true;
                }
                if((parseInt(_voice) > 0) && (parseInt(_msg) + parseInt(_img) + parseInt(_video)) < 1){
                    _type = 'voice';
                    _check = true;
                }
                if((parseInt(_video) > 0) && (parseInt(_msg) + parseInt(_img) + parseInt(_voice)) < 1){
                    _type = 'video';
                    _check = true;
                }
                if(!_check){
                    _type = 'all';
                }
                if(_lat.length > 0 && _lon.length > 0){
                    addmarker(_lon,_lat,_gps_id,'infor',_type);
                }
            }
            //setCenter view
            var zoom = 13;
            var lonLat = new OpenLayers.LonLat(lon,lat);
            lonLat.transform(
                new OpenLayers.Projection("EPSG:4326"),
                map.getProjectionObject()
            );
            map.setCenter (lonLat, zoom);
        }
    } catch(e){
        console.log(add_points);
        console.log(e);
    }
}

function add_linestring(map, point_array,ac_data,infor_data) {
    // Remove all layer
    try{
        map.removeLayer(lyrMarkers);
    } catch(e){
        console.log(e);
    }

    try{
        var arrleyer = map.getLayersByName("Polyline");
        for(var ile = 0; ile < arrleyer.length; ile++){
            map.removeLayer(arrleyer[ile]);
        }

        $('.olPopup').css('display','none');
    } catch(e){
        console.log(e);
    }
    try{
        var arrleyer = map.getLayersByName("Polyline_activiti");
        for(var ile = 0; ile < arrleyer.length; ile++){
            map.removeLayer(arrleyer[ile]);
        }
    } catch(e){
        console.log(e);
    }
    try{
        // New
        var count = point_array.length;
        if(count > 0) {
            if(point_array[0][0] == 'color'){
                var style = {
                        strokeColor: 'black',
                        strokeOpacity: 1,
                        strokeWidth: 4
                };
                var center = (count - count%2)/2;
                var lat = 0;
                var lon = 0;
                if(point_array[center][0] != 'color') {
                    lat = point_array[center][1];
                    lon = point_array[center][0];
                } else {
                    lat = point_array[center + 1][1];
                    lon = point_array[center + 1][0];
                }
                var zoom = 13;
                var lonLat = new OpenLayers.LonLat(lat,lon);
                lonLat.transform(
                    new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                    map.getProjectionObject() // to Spherical Mercator Projection
                );
                /**/
                var ac_point = [];
                var ac_count = 0;
                //lineLayer = new OpenLayers.Layer.Vector('Polyline');

                for (var i = 0; i < count; i++) {
                    pos = point_array[i];

                    if(pos[0] == 'color' || i == (count - 1)) {
                        if(pos[0] == 'color' && i > 0) {
                            pos2 = point_array[i + 1];
                            polyline.push(new OpenLayers.Geometry.Point(pos2[1],pos2[0]));
                        }

                        if(i == (count - 1))
                            polyline.push(new OpenLayers.Geometry.Point(pos[1],pos[0]));

                        var line      = new OpenLayers.Geometry.LineString(polyline);
                        line          = line.transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
                        lineFeature   = new OpenLayers.Feature.Vector(line, null, style);

                        lineLayer = new OpenLayers.Layer.Vector('Polyline');
                        lineLayer.addFeatures([lineFeature]);
                        map.addLayer(lineLayer);
                    }
                    // Start activity
                    if(pos[0] == 'color') {
                        var color = pos[1];
                        color = color.replace("light_blue","#ADD8E6");
                        color = color.replace("light_green","lightgreen");
                        style = {
                            strokeColor: color,
                            strokeOpacity: 1,
                            strokeWidth: 4
                        };
                        var polyline = new Array();

                        ac_point[ac_count] = point_array[i + 1];
                        ac_count++;
                    } else {
                        if(i < (count - 1)) {
                            polyline.push(new OpenLayers.Geometry.Point(pos[1],pos[0]));
                        }
                    }
                };

                /*# MarKer*/
                lyrMarkers = new OpenLayers.Layer.Markers("Markers");
                map.addLayer(lyrMarkers);

                /* Add start, end */
                addmarker(point_array[1][1],point_array[1][0],_gps_id,'direction','start');
                addmarker(point_array[(count - 1)][1],point_array[(count - 1)][0],_gps_id,'direction','end');

                /* Information*/
                for (var key in infor_data) {
                    var _gps_id     = infor_data[key]['gps_id'];
                    var _lat        = infor_data[key]['lat'];
                    var _lon        = infor_data[key]['lon'];
                    var _msg        = infor_data[key]['message'];
                    var _img        = infor_data[key]['image'];
                    var _voice      = infor_data[key]['voice'];
                    var _video      = infor_data[key]['video'];
                    var _data       = infor_data[key]['data'];
                    var _type = 'message';
                    var _check = false;
                    if((parseInt(_msg) > 0) && (parseInt(_img) + parseInt(_voice) + parseInt(_video)) < 1){
                        _type = 'message';
                        _check = true;
                    }
                    if((parseInt(_img) > 0) && (parseInt(_msg) + parseInt(_voice) + parseInt(_video)) < 1){
                        _type = 'image';
                        _check = true;
                    }
                    if((parseInt(_voice) > 0) && (parseInt(_msg) + parseInt(_img) + parseInt(_video)) < 1){
                        _type = 'voice';
                        _check = true;
                    }
                    if((parseInt(_video) > 0) && (parseInt(_msg) + parseInt(_img) + parseInt(_voice)) < 1){
                        _type = 'video';
                        _check = true;
                    }
                    if(!_check){
                        _type = 'all';
                    }
                    if(_lat.length > 0 && _lon.length > 0){
                        addmarker(_lon,_lat,_gps_id,'infor',_type);
                    }
                }
                //setCenter view
                map.setCenter (lonLat, zoom);
            }
        }

    } catch(e){
        console.log(add_linestring);
        console.log(e);
    }
}

// Add MarKer
function addmarker(lat,lon,_gps_id,type,_type) {
    try {
        var size = new OpenLayers.Size(16, 16);
        var icon = new OpenLayers.Icon(_root_url + "assets/images/marker.png");
        if(type == 'infor'){
            switch(_type){
            case 'message':
                icon = new OpenLayers.Icon(_root_url + "assets/images/icon_message.png",size);
                break;
            case 'image':
                icon = new OpenLayers.Icon(_root_url + "assets/images/icon_image.png",size);
                break;
            case 'voice':
                icon = new OpenLayers.Icon(_root_url + "assets/images/icon_voice.png",size);
                break;
            case 'video':
                icon = new OpenLayers.Icon(_root_url + "assets/images/icon_video.png",size);
                break;
            case 'start':
                icon = new OpenLayers.Icon(_root_url + "assets/images/dd-start.png",size);
                break;
            case 'end':
                icon = new OpenLayers.Icon(_root_url + "assets/images/dd-end.png",size);
                break;
            default:
                icon = new OpenLayers.Icon(_root_url + "assets/images/flag.png",size);
            }
        } else{
            var size = new OpenLayers.Size(20, 34);
            switch(_type){
                case 'start':
                icon = new OpenLayers.Icon(_root_url + "assets/images/dd-start.png",size);
                break;
                case 'end':
                    icon = new OpenLayers.Icon(_root_url + "assets/images/dd-end.png",size);
                    break;
                default:
                    icon = new OpenLayers.Icon(_root_url + "assets/images/dd-start.png",size);
            }
        }
        var lonLat = new OpenLayers.LonLat(lat, lon);
        lonLat.transform(
            new OpenLayers.Projection("EPSG:4326"),
            map.getProjectionObject()
        );
        bounds.extend(lonLat);
        var feature = new OpenLayers.Feature(lyrMarkers, lonLat);
        feature.closeBox = true;
        feature.popupClass = popupClass;
        feature.data.popupContentHTML = '';
        feature.data.overflow = "auto";

        var marker = new OpenLayers.Marker(lonLat, icon.clone());

        var markerClick = function(evt) {
            if (currentPopup != null && currentPopup.visible()) {
                currentPopup.hide();
            }
            if (this.popup == null) {
                this.popup = this.createPopup(this.closeBox);
                map.addPopup(this.popup);
                this.popup.show();
            } else {
                this.popup.toggle();
            }
            currentPopup = this.popup;
            OpenLayers.Event.stop(evt);
        };
        if(type != 'infor')
            marker.events.register("mousedown", feature, markerClick);

        lyrMarkers.addMarker(marker);
        if(type == 'infor')
        {
            $("img.olAlphaImg:last").wrap('<a href="javascript:;" onclick="return map_information_detail(' + _gps_id + ',0,0,\'' + lat + '\',\'' + lon + '\',\'' + _type + '\');"/>');
        }
    } catch (e) {
        console.log(addmarker);
        console.log(e);
    }
}

/*
New
*/
function _add_marker(_marker, lat, lon, _gps_id, type, _type){
    try {
        var size = new OpenLayers.Size(16, 16);
        var icon = new OpenLayers.Icon(_root_url + "assets/images/marker.png");
        if(type == 'infor'){
            switch(_type){
            case 'message':
                icon = new OpenLayers.Icon(_root_url + "assets/images/icon_message.png",size);
                break;
            case 'image':
                icon = new OpenLayers.Icon(_root_url + "assets/images/icon_image.png",size);
                break;
            case 'voice':
                icon = new OpenLayers.Icon(_root_url + "assets/images/icon_voice.png",size);
                break;
            case 'video':
                icon = new OpenLayers.Icon(_root_url + "assets/images/icon_video.png",size);
                break;
            case 'start':
                icon = new OpenLayers.Icon(_root_url + "assets/images/dd-start.png",size);
                break;
            case 'end':
                icon = new OpenLayers.Icon(_root_url + "assets/images/dd-end.png",size);
                break;
            default:
                icon = new OpenLayers.Icon(_root_url + "assets/images/flag.png",size);
            }
        } else{
            var size = new OpenLayers.Size(20, 34);
            switch(_type){
                case 'start':
                icon = new OpenLayers.Icon(_root_url + "assets/images/dd-start.png",size);
                break;
                case 'end':
                    icon = new OpenLayers.Icon(_root_url + "assets/images/dd-end.png",size);
                    break;
                default:
                    icon = new OpenLayers.Icon(_root_url + "assets/images/dd-start.png",size);
            }
        }
        var lonLat = new OpenLayers.LonLat(lat, lon);
        lonLat.transform(
            new OpenLayers.Projection("EPSG:4326"),
            map.getProjectionObject()
        );
        bounds.extend(lonLat);
        var feature = new OpenLayers.Feature(_marker, lonLat);
        feature.closeBox = true;
        feature.popupClass = popupClass;
        feature.data.popupContentHTML = '';
        feature.data.overflow = "auto";

        var marker = new OpenLayers.Marker(lonLat, icon.clone());

        var markerClick = function(evt) {
            if (currentPopup != null && currentPopup.visible()) {
                currentPopup.hide();
            }
            if (this.popup == null) {
                this.popup = this.createPopup(this.closeBox);
                map.addPopup(this.popup);
                this.popup.show();
            } else {
                this.popup.toggle();
            }
            currentPopup = this.popup;
            OpenLayers.Event.stop(evt);
        };
        if(type != 'infor')
            marker.events.register("mousedown", feature, markerClick);

        _marker.addMarker(marker);
        if(type == 'infor')
        {
            $("img.olAlphaImg:last").wrap('<a href="javascript:;" onclick="return map_information_detail(' + _gps_id + ',0,0,\'' + lat + '\',\'' + lon + '\',\'' + _type + '\');"/>');
        }
    } catch (e) {
        console.log(addmarker);
        console.log(e);
    }
}

function _line(map, positions, id_gps) {
    // Remove all layer
    try{
        if(markers_start_end != null){
            map.removeLayer(markers_start_end);
        }
    } catch(e){

    }
    try{
        var arrleyer = map.getLayersByName("Polyline");
        if(arrleyer.length > 0){
            for(var ile = 0; ile < arrleyer.length; ile++){
                map.removeLayer(arrleyer[ile]);
            }
            $('.olPopup').css('display','none');
        }
    } catch(e){
        console.log(e);
    }
    try{
        var arrleyer = map.getLayersByName("Polyline_activiti");
        for(var ile = 0; ile < arrleyer.length; ile++){
            map.removeLayer(arrleyer[ile]);
        }
    } catch(e){
        console.log(e);
    }
    try{
        // New
        var count = positions.length;
        if(count > 0) {
            if(positions[0][0] == 'color'){
                var style = {
                        strokeColor: 'black',
                        strokeOpacity: 1,
                        strokeWidth: 4
                };
                var center = (count - count%2)/2;
                var lat = 0;
                var lon = 0;
                if(positions[center][0] != 'color') {
                    lat = positions[center][1];
                    lon = positions[center][0];
                } else {
                    lat = positions[center + 1][1];
                    lon = positions[center + 1][0];
                }
                var zoom = 13;
                var lonLat = new OpenLayers.LonLat(lat,lon);
                lonLat.transform(
                    new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                    map.getProjectionObject() // to Spherical Mercator Projection
                );
                //lineLayer = new OpenLayers.Layer.Vector('Polyline');
                for (var i = 0; i < count; i++) {
                    pos = positions[i];
                    if(pos[0] == 'color' || i == (count - 1)) {
                        if( (pos[0] == 'color') && (i > 0) && (i < (count - 1)) ) {
                            pos2 = positions[i + 1];
                            polyline.push(new OpenLayers.Geometry.Point(pos2[1],pos2[0]));
                        }

                        if(i == (count - 1))
                            polyline.push(new OpenLayers.Geometry.Point(pos[1],pos[0]));
                        var line      = new OpenLayers.Geometry.LineString(polyline);
                        line          = line.transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
                        lineFeature   = new OpenLayers.Feature.Vector(line, null, style);

                        lineLayer = new OpenLayers.Layer.Vector('Polyline');
                        lineLayer.addFeatures([lineFeature]);
                        map.addLayer(lineLayer);
                    }
                    // Start activity
                    if(pos[0] == 'color') {
                        var color = pos[1];
                        color = color.replace("light_blue","#ADD8E6");
                        color = color.replace("light_green","lightgreen");
                        style = {
                            strokeColor: color,
                            strokeOpacity: 1,
                            strokeWidth: 4
                        };
                        var polyline = new Array();
                    } else {
                        if(i < (count - 1)) {
                            polyline.push(new OpenLayers.Geometry.Point(pos[1],pos[0]));
                        }
                    }
                };
                 /*# MarKer*/
                markers_start_end = new OpenLayers.Layer.Markers("Markers_start_end");
                map.addLayer(markers_start_end);
                /* Add start, end */
                _add_marker(markers_start_end, positions[1][1],positions[1][0],id_gps,'direction','start');
                _add_marker(markers_start_end, positions[(count - 1)][1],positions[(count - 1)][0],id_gps,'direction','end');
                //setCenter view
                map.setCenter (lonLat, zoom);
            }
        }
    } catch(e){
        console.log(_line);
        console.log(e);
    }
}

function _information(map, information, id_gps){
    /*# MarKer*/
    try{
        if(markers_info != null){
            map.removeLayer(markers_info);
        }
    } catch(e){
        console.log(e);
    }
    markers_info = new OpenLayers.Layer.Markers("Markers_info");
    map.addLayer(markers_info);
    /* Information*/
    for (var key in information) {
        var _gps_id     = information[key]['gps_id'];
        var _lat        = information[key]['lat'];
        var _lon        = information[key]['lon'];
        var _msg        = information[key]['message'];
        var _img        = information[key]['image'];
        var _voice      = information[key]['voice'];
        var _video      = information[key]['video'];
        var _data       = information[key]['data'];
        var _type = 'message';
        var _check = false;
        if((parseInt(_msg) > 0) && (parseInt(_img) + parseInt(_voice) + parseInt(_video)) < 1){
            _type = 'message';
            _check = true;
        }
        if((parseInt(_img) > 0) && (parseInt(_msg) + parseInt(_voice) + parseInt(_video)) < 1){
            _type = 'image';
            _check = true;
        }
        if((parseInt(_voice) > 0) && (parseInt(_msg) + parseInt(_img) + parseInt(_video)) < 1){
            _type = 'voice';
            _check = true;
        }
        if((parseInt(_video) > 0) && (parseInt(_msg) + parseInt(_img) + parseInt(_voice)) < 1){
            _type = 'video';
            _check = true;
        }
        if(!_check){
            _type = 'all';
        }
        if(_lat.length > 0 && _lon.length > 0){
            _add_marker(markers_info, _lon, _lat, id_gps, 'infor', _type);
        }
    }
}