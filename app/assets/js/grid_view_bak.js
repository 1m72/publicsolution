var grid_instance;
var dialog_edit;
var dialog_edit_id = '#dialog_edit';
var auto_inc = 0;
var _title;
var _delete_confirm;
var _alert_error;
var _lbl_item_per_page;

function grid_refresh(_id_grid) {
    $(_id_grid).data("kendoGrid").dataSource.read();
    $(_id_grid).data('kendoGrid').refresh();
}

function create_grid(_id_grid,_current_url,_grid_limit,_columns){
    $(_id_grid).kendoGrid({
        dataSource: {
            transport: {
                read: _current_url,
                "contentType":"application\/json",
                "type":"POST"
            },
            schema: {
                data: "data",
                total: "total"
            },
            pageSize: _grid_limit,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        columns: _columns,
        toolbar: kendo.template($("#toolbar_template").html()),
        selectable: "row",
        filterable: true,
        groupable: true,
        scrollable: true,
        reorderable: true,
        columnMenu: true,
        sortable: {
            mode: "multiple",
            allowUnsort: true
        },
        navigatable: true,
        height: grid_height,
        pageable: {
            pageSize : _grid_limit,
            pageSizes: true,
            refresh: true,
            messages: {
                itemsPerPage: _lbl_item_per_page
            }
        },
        dataBinding: function(e) {
            var grid     = $(_id_grid).data("kendoGrid");
            _grid_limit   = grid.dataSource.pageSize();
            current_page = grid.dataSource.page();
            auto_inc     = (current_page - 1) * _grid_limit;
        }
    });
}
$(function() {
	dialog_edit	= $(dialog_edit_id).kendoWindow({
           				actions: ["Pin", "Close"],
				        draggable: false,
				        modal: true,
				        pinned: false,
				        resizable: false,
				        width: "650px",
				        title:_title ,
			    	}).data("kendoWindow");

    $('body').on('click', '.btn_close_dialog', function(e){
        e.preventDefault();
        dialog_edit.close();
    });
    $('body').on('click', '.btn_delete', function(e){
        e.preventDefault();
        i = $(this);
        var kendoWindow = $("<div />").kendoWindow({
            title: _delete_confirm,
            resizable: false,
            width: 450,
            modal: true
        });

        kendoWindow.data("kendoWindow")
            .content($("#delete-confirmation").html())
            .center().open();

        kendoWindow
            .find(".delete-confirm, .delete-cancel")
            .click(function() {
                if ($(this).hasClass("delete-confirm")) {
                    $.ajax({
                        type: 'POST',
                        url: i.attr('href'),
                        dataType: 'json',
                        success: function(data){
                            if (data.status == true) {
                                var grid = $(_id_grid).data("kendoGrid");
                                var row = grid.select();
                                var uid = row.data("uid");
                                row.remove();
                                grid_refresh(_id_grid);
                            }
                        },
                        error: function(err){}
                    });
                }

                kendoWindow.data("kendoWindow").close();
            })
            .end()
    });

   $('body').on('click', '.btn_edit', function(e){
        e.preventDefault();
        i = $(this);
        $.ajax({
            type: 'GET',
            url: i.attr('href'),
            success: function(data){
                dialog_edit.content(data);
                dialog_edit.center();
                dialog_edit.open();
            },
            error: function(err){
                alert(_alert_error);
            }
        });
    });
});