// Config
var timeout_dialog     = 800;
var global_grid_height = '650px';

$(function(){
    $('body').on('click', 'a.module_link', function(){
        i = $(this);
        window.location.href = i.attr('href');
    });

    $('.captcha_img').on('click', function(e){
        e.preventDefault();
        i = $(this);
        url = i.attr('src');
        i.attr('src', '');
        i.attr('src', url);
    });

    // Sidebar
    $('.sidebar_menu_item').on('click', function(e){
        e.preventDefault();
        i = $(this);
        var parent, target;
        parent = i.attr('data-parent');
        target = i.attr('href');

        if ($(parent + ' ' + target).is(':hidden')) {
            $(parent + ' .panel-collapse').hide();
            $(parent + ' ' + target).show();
            $('.sidebar_menu_item').removeClass('active');
            i.addClass('active');
        }
    });

    // Button loading state
    $('body').on ('click', '.btn', function () {
        i = $(this);
        if (i.attr('loading') == '0') {
            return;
        }
        i.attr('data-loading-text', 'Wait ...');
        i.button('loading');
        if (i.attr('_keep') != 1) {
            setTimeout(function () {
                i.button('reset');
            }, 1000);
        }
    });

    /*$("body").on('click', 'div.bhoechie-tab-menu>div.list-group>a', function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });

    $('body').on('click', '.go', function(e){
        i = $(this);
        window.location.href = i.attr('href');
    });*/

    setTimeout(function() {$('.sys_tooltip').tooltip();}, 1000)
});

function loading(state) {
    if ($('#bis-loading').length === 0) {
        $('body').append("<div class='k-loading-mask' id='bis-loading' style='display: none;'><div class='k-loading-text' style='display: none;'></div><div class='k-loading-image'/><div class='k-loading-color'/></div>");
    }

    if ((typeof (state) != 'undefined') && (state === false) ) {
        $('#bis-loading').hide();
        return false;
    } else {
        $('#bis-loading').show();
        return true;
    }
}

function set_cookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime()+(exdays*24*60*60*1000));
    var expires = "expires="+d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function get_cookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name)==0) return c.substring(name.length,c.length);
    }
    return "";
}