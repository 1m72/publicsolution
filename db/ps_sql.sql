CREATE USER 'ps'@'localhost' IDENTIFIED BY 'jfUeSdfsMFRFbpRU';
GRANT USAGE ON *.* TO 'ps'@'localhost' IDENTIFIED BY 'jfUeSdfsMFRFbpRU' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILEGES ON `ps\_%`.* TO 'ps'@'localhost';

#--- Production environment

CREATE USER 'ps_pro'@'localhost' IDENTIFIED BY 'UMJFMWy8Q8qTXrD6';
GRANT USAGE ON *.* TO 'ps_pro'@'localhost' IDENTIFIED BY 'UMJFMWy8Q8qTXrD6' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_pro` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_pro`.* TO 'ps_pro'@'localhost';
GRANT ALL PRIVILEGES ON `ps_pro_%`.* TO 'ps_pro'@'localhost';

CREATE USER 'ps_pro_pfin'@'localhost' IDENTIFIED BY 'cqLWXsdRrf7UxeEC';
GRANT USAGE ON *.* TO 'ps_pro_pfin'@'localhost' IDENTIFIED BY 'cqLWXsdRrf7UxeEC' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_pro_pfin` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_pro\_pfin`.* TO 'ps_pro_pfin'@'localhost';
GRANT ALL PRIVILEGES ON `ps_pro_pfin\_%`.* TO 'ps_pro_pfin'@'localhost';

CREATE USER 'ps_pro_frei'@'localhost' IDENTIFIED BY 'WRnPeYfTAaedMZdE';
GRANT USAGE ON *.* TO 'ps_pro_frei'@'localhost' IDENTIFIED BY 'WRnPeYfTAaedMZdE' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_pro_frei` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_pro\_frei`.* TO 'ps_pro_frei'@'localhost';
GRANT ALL PRIVILEGES ON `ps_pro_frei\_%`.* TO 'ps_pro_frei'@'localhost';

CREATE USER 'ps_pro_kitz'@'localhost' IDENTIFIED BY '55uHpbVQn4hCNTHn';
GRANT USAGE ON *.* TO 'ps_pro_kitz'@'localhost' IDENTIFIED BY '55uHpbVQn4hCNTHn' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_pro_kitz` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_pro\_kitz`.* TO 'ps_pro_kitz'@'localhost';
GRANT ALL PRIVILEGES ON `ps_pro_kitz\_%`.* TO 'ps_pro_kitz'@'localhost';

CREATE USER 'ps_pro_geme'@'localhost' IDENTIFIED BY 'MNCDPdPuWGUv4bMA';
GRANT USAGE ON *.* TO 'ps_pro_geme'@'localhost' IDENTIFIED BY 'MNCDPdPuWGUv4bMA' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_pro_geme` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_pro\_geme`.* TO 'ps_pro_geme'@'localhost';
GRANT ALL PRIVILEGES ON `ps_pro_geme\_%`.* TO 'ps_pro_geme'@'localhost';

CREATE USER 'ps_pro_kloo'@'localhost' IDENTIFIED BY 'NWDFafBFp5udjWNa';
GRANT USAGE ON *.* TO 'ps_pro_kloo'@'localhost' IDENTIFIED BY 'NWDFafBFp5udjWNa' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_pro_kloo` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_pro\_kloo`.* TO 'ps_pro_kloo'@'localhost';
GRANT ALL PRIVILEGES ON `ps_pro_kloo\_%`.* TO 'ps_pro_kloo'@'localhost';

CREATE USER 'ps_pro_test'@'localhost' IDENTIFIED BY 'N28tRUqZk6v8KKFQ';
GRANT USAGE ON *.* TO 'ps_pro_test'@'localhost' IDENTIFIED BY 'N28tRUqZk6v8KKFQ' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_pro_test` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_pro\_test`.* TO 'ps_pro_test'@'localhost';
GRANT ALL PRIVILEGES ON `ps_pro_test\_%`.* TO 'ps_pro_test'@'localhost';

CREATE USER 'ps_pro_demo'@'localhost' IDENTIFIED BY 'fp2khsNSnRUhBkw2';
GRANT USAGE ON *.* TO 'ps_pro_demo'@'localhost' IDENTIFIED BY 'fp2khsNSnRUhBkw2' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_pro_demo` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_pro\_demo`.* TO 'ps_pro_demo'@'localhost';
GRANT ALL PRIVILEGES ON `ps_pro_demo\_%`.* TO 'ps_pro_demo'@'localhost';

CREATE USER 'ps_pro_neuf'@'localhost' IDENTIFIED BY 'wNPHfXdqEBJfeewm';
GRANT USAGE ON *.* TO 'ps_pro_neuf'@'localhost' IDENTIFIED BY 'wNPHfXdqEBJfeewm' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_pro_neuf`;
GRANT ALL PRIVILEGES ON `ps\_pro\_neuf`.* TO 'ps_pro_neuf'@'localhost';
GRANT ALL PRIVILEGES ON `ps_pro_neuf\_%`.* TO 'ps_pro_neuf'@'localhost';

#--- Test environment

CREATE USER 'ps_test'@'localhost' IDENTIFIED BY 'UMJFMWy8Q8qTXrD6';
GRANT USAGE ON *.* TO 'ps_test'@'localhost' IDENTIFIED BY 'UMJFMWy8Q8qTXrD6' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_test` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_test`.* TO 'ps_test'@'localhost';
GRANT ALL PRIVILEGES ON `ps_test_%`.* TO 'ps_test'@'localhost';

CREATE USER 'ps_test_pfin'@'localhost' IDENTIFIED BY 'cqLWXsdRrf7UxeEC';
GRANT USAGE ON *.* TO 'ps_test_pfin'@'localhost' IDENTIFIED BY 'cqLWXsdRrf7UxeEC' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_test_pfin` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_test\_pfin`.* TO 'ps_test_pfin'@'localhost';
GRANT ALL PRIVILEGES ON `ps_test_pfin\_%`.* TO 'ps_test_pfin'@'localhost';

CREATE USER 'ps_test_frei'@'localhost' IDENTIFIED BY 'WRnPeYfTAaedMZdE';
GRANT USAGE ON *.* TO 'ps_test_frei'@'localhost' IDENTIFIED BY 'WRnPeYfTAaedMZdE' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_test_frei` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_test\_frei`.* TO 'ps_test_frei'@'localhost';
GRANT ALL PRIVILEGES ON `ps_test_frei\_%`.* TO 'ps_test_frei'@'localhost';

CREATE USER 'ps_test_kitz'@'localhost' IDENTIFIED BY '55uHpbVQn4hCNTHn';
GRANT USAGE ON *.* TO 'ps_test_kitz'@'localhost' IDENTIFIED BY '55uHpbVQn4hCNTHn' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_test_kitz` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_test\_kitz`.* TO 'ps_test_kitz'@'localhost';
GRANT ALL PRIVILEGES ON `ps_test_kitz\_%`.* TO 'ps_test_kitz'@'localhost';

CREATE USER 'ps_test_geme'@'localhost' IDENTIFIED BY 'MNCDPdPuWGUv4bMA';
GRANT USAGE ON *.* TO 'ps_test_geme'@'localhost' IDENTIFIED BY 'MNCDPdPuWGUv4bMA' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_test_geme` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_test\_geme`.* TO 'ps_test_geme'@'localhost';
GRANT ALL PRIVILEGES ON `ps_test_geme\_%`.* TO 'ps_test_geme'@'localhost';

CREATE USER 'ps_test_kloo'@'localhost' IDENTIFIED BY 'NWDFafBFp5udjWNa';
GRANT USAGE ON *.* TO 'ps_test_kloo'@'localhost' IDENTIFIED BY 'NWDFafBFp5udjWNa' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_test_kloo` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_test\_kloo`.* TO 'ps_test_kloo'@'localhost';
GRANT ALL PRIVILEGES ON `ps_test_kloo\_%`.* TO 'ps_test_kloo'@'localhost';

CREATE USER 'ps_test_test'@'localhost' IDENTIFIED BY 'N28tRUqZk6v8KKFQ';
GRANT USAGE ON *.* TO 'ps_test_test'@'localhost' IDENTIFIED BY 'N28tRUqZk6v8KKFQ' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_test_test` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_test\_test`.* TO 'ps_test_test'@'localhost';
GRANT ALL PRIVILEGES ON `ps_test_test\_%`.* TO 'ps_test_test'@'localhost';

CREATE USER 'ps_test_demo'@'localhost' IDENTIFIED BY 'fp2khsNSnRUhBkw2';
GRANT USAGE ON *.* TO 'ps_test_demo'@'localhost' IDENTIFIED BY 'fp2khsNSnRUhBkw2' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_test_demo` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_test\_demo`.* TO 'ps_test_demo'@'localhost';
GRANT ALL PRIVILEGES ON `ps_test_demo\_%`.* TO 'ps_test_demo'@'localhost';

CREATE USER 'ps_test_neuf'@'localhost' IDENTIFIED BY 'wNPHfXdqEBJfeewm';
GRANT USAGE ON *.* TO 'ps_test_neuf'@'localhost' IDENTIFIED BY 'wNPHfXdqEBJfeewm' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_test_neuf`;
GRANT ALL PRIVILEGES ON `ps\_test\_neuf`.* TO 'ps_test_neuf'@'localhost';
GRANT ALL PRIVILEGES ON `ps_test_neuf\_%`.* TO 'ps_test_neuf'@'localhost';

#--- dev environment

CREATE USER 'ps_dev'@'localhost' IDENTIFIED BY 'UMJFMWy8Q8qTXrD6';
GRANT USAGE ON *.* TO 'ps_dev'@'localhost' IDENTIFIED BY 'UMJFMWy8Q8qTXrD6' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_dev` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_dev`.* TO 'ps_dev'@'localhost';
GRANT ALL PRIVILEGES ON `ps_dev_%`.* TO 'ps_dev'@'localhost';

CREATE USER 'ps_dev_pfin'@'localhost' IDENTIFIED BY 'cqLWXsdRrf7UxeEC';
GRANT USAGE ON *.* TO 'ps_dev_pfin'@'localhost' IDENTIFIED BY 'cqLWXsdRrf7UxeEC' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_dev_pfin` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_dev\_pfin`.* TO 'ps_dev_pfin'@'localhost';
GRANT ALL PRIVILEGES ON `ps_dev_pfin\_%`.* TO 'ps_dev_pfin'@'localhost';

CREATE USER 'ps_dev_frei'@'localhost' IDENTIFIED BY 'WRnPeYfTAaedMZdE';
GRANT USAGE ON *.* TO 'ps_dev_frei'@'localhost' IDENTIFIED BY 'WRnPeYfTAaedMZdE' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_dev_frei` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_dev\_frei`.* TO 'ps_dev_frei'@'localhost';
GRANT ALL PRIVILEGES ON `ps_dev_frei\_%`.* TO 'ps_dev_frei'@'localhost';

CREATE USER 'ps_dev_kitz'@'localhost' IDENTIFIED BY '55uHpbVQn4hCNTHn';
GRANT USAGE ON *.* TO 'ps_dev_kitz'@'localhost' IDENTIFIED BY '55uHpbVQn4hCNTHn' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_dev_kitz` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_dev\_kitz`.* TO 'ps_dev_kitz'@'localhost';
GRANT ALL PRIVILEGES ON `ps_dev_kitz\_%`.* TO 'ps_dev_kitz'@'localhost';

CREATE USER 'ps_dev_geme'@'localhost' IDENTIFIED BY 'MNCDPdPuWGUv4bMA';
GRANT USAGE ON *.* TO 'ps_dev_geme'@'localhost' IDENTIFIED BY 'MNCDPdPuWGUv4bMA' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_dev_geme` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_dev\_geme`.* TO 'ps_dev_geme'@'localhost';
GRANT ALL PRIVILEGES ON `ps_dev_geme\_%`.* TO 'ps_dev_geme'@'localhost';

CREATE USER 'ps_dev_kloo'@'localhost' IDENTIFIED BY 'NWDFafBFp5udjWNa';
GRANT USAGE ON *.* TO 'ps_dev_kloo'@'localhost' IDENTIFIED BY 'NWDFafBFp5udjWNa' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_dev_kloo` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_dev\_kloo`.* TO 'ps_dev_kloo'@'localhost';
GRANT ALL PRIVILEGES ON `ps_dev_kloo\_%`.* TO 'ps_dev_kloo'@'localhost';

CREATE USER 'ps_dev_test'@'localhost' IDENTIFIED BY 'N28tRUqZk6v8KKFQ';
GRANT USAGE ON *.* TO 'ps_dev_test'@'localhost' IDENTIFIED BY 'N28tRUqZk6v8KKFQ' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_dev_test` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_dev\_test`.* TO 'ps_dev_test'@'localhost';
GRANT ALL PRIVILEGES ON `ps_dev_test\_%`.* TO 'ps_dev_test'@'localhost';

CREATE USER 'ps_dev_demo'@'localhost' IDENTIFIED BY 'fp2khsNSnRUhBkw2';
GRANT USAGE ON *.* TO 'ps_dev_demo'@'localhost' IDENTIFIED BY 'fp2khsNSnRUhBkw2' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_dev_demo` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_dev\_demo`.* TO 'ps_dev_demo'@'localhost';
GRANT ALL PRIVILEGES ON `ps_dev_demo\_%`.* TO 'ps_dev_demo'@'localhost';

CREATE USER 'ps_dev_neuf'@'localhost' IDENTIFIED BY 'wNPHfXdqEBJfeewm';
GRANT USAGE ON *.* TO 'ps_dev_neuf'@'localhost' IDENTIFIED BY 'wNPHfXdqEBJfeewm' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_dev_neuf`;
GRANT ALL PRIVILEGES ON `ps\_dev\_neuf`.* TO 'ps_dev_neuf'@'localhost';
GRANT ALL PRIVILEGES ON `ps_dev_neuf\_%`.* TO 'ps_dev_neuf'@'localhost';

#--- local environment

CREATE USER 'ps_local'@'localhost' IDENTIFIED BY 'UMJFMWy8Q8qTXrD6';
GRANT USAGE ON *.* TO 'ps_local'@'localhost' IDENTIFIED BY 'UMJFMWy8Q8qTXrD6' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_local` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_local`.* TO 'ps_local'@'localhost';
GRANT ALL PRIVILEGES ON `ps_local_%`.* TO 'ps_local'@'localhost';

CREATE USER 'ps_local_pfin'@'localhost' IDENTIFIED BY 'cqLWXsdRrf7UxeEC';
GRANT USAGE ON *.* TO 'ps_local_pfin'@'localhost' IDENTIFIED BY 'cqLWXsdRrf7UxeEC' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_local_pfin` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_local\_pfin`.* TO 'ps_local_pfin'@'localhost';
GRANT ALL PRIVILEGES ON `ps_local_pfin\_%`.* TO 'ps_local_pfin'@'localhost';

CREATE USER 'ps_local_frei'@'localhost' IDENTIFIED BY 'WRnPeYfTAaedMZdE';
GRANT USAGE ON *.* TO 'ps_local_frei'@'localhost' IDENTIFIED BY 'WRnPeYfTAaedMZdE' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_local_frei` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_local\_frei`.* TO 'ps_local_frei'@'localhost';
GRANT ALL PRIVILEGES ON `ps_local_frei\_%`.* TO 'ps_local_frei'@'localhost';

CREATE USER 'ps_local_kitz'@'localhost' IDENTIFIED BY '55uHpbVQn4hCNTHn';
GRANT USAGE ON *.* TO 'ps_local_kitz'@'localhost' IDENTIFIED BY '55uHpbVQn4hCNTHn' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_local_kitz` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_local\_kitz`.* TO 'ps_local_kitz'@'localhost';
GRANT ALL PRIVILEGES ON `ps_local_kitz\_%`.* TO 'ps_local_kitz'@'localhost';

CREATE USER 'ps_local_geme'@'localhost' IDENTIFIED BY 'MNCDPdPuWGUv4bMA';
GRANT USAGE ON *.* TO 'ps_local_geme'@'localhost' IDENTIFIED BY 'MNCDPdPuWGUv4bMA' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_local_geme` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_local\_geme`.* TO 'ps_local_geme'@'localhost';
GRANT ALL PRIVILEGES ON `ps_local_geme\_%`.* TO 'ps_local_geme'@'localhost';

CREATE USER 'ps_local_kloo'@'localhost' IDENTIFIED BY 'NWDFafBFp5udjWNa';
GRANT USAGE ON *.* TO 'ps_local_kloo'@'localhost' IDENTIFIED BY 'NWDFafBFp5udjWNa' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_local_kloo` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_local\_kloo`.* TO 'ps_local_kloo'@'localhost';
GRANT ALL PRIVILEGES ON `ps_local_kloo\_%`.* TO 'ps_local_kloo'@'localhost';

CREATE USER 'ps_local_test'@'localhost' IDENTIFIED BY 'N28tRUqZk6v8KKFQ';
GRANT USAGE ON *.* TO 'ps_local_test'@'localhost' IDENTIFIED BY 'N28tRUqZk6v8KKFQ' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_local_test` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_local\_test`.* TO 'ps_local_test'@'localhost';
GRANT ALL PRIVILEGES ON `ps_local_test\_%`.* TO 'ps_local_test'@'localhost';

CREATE USER 'ps_local_demo'@'localhost' IDENTIFIED BY 'fp2khsNSnRUhBkw2';
GRANT USAGE ON *.* TO 'ps_local_demo'@'localhost' IDENTIFIED BY 'fp2khsNSnRUhBkw2' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_local_demo` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON `ps\_local\_demo`.* TO 'ps_local_demo'@'localhost';
GRANT ALL PRIVILEGES ON `ps_local_demo\_%`.* TO 'ps_local_demo'@'localhost';

CREATE USER 'ps_local_neuf'@'localhost' IDENTIFIED BY 'wNPHfXdqEBJfeewm';
GRANT USAGE ON *.* TO 'ps_local_neuf'@'localhost' IDENTIFIED BY 'wNPHfXdqEBJfeewm' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `ps_local_neuf`;
GRANT ALL PRIVILEGES ON `ps\_local\_neuf`.* TO 'ps_local_neuf'@'localhost';
GRANT ALL PRIVILEGES ON `ps_local_neuf\_%`.* TO 'ps_local_neuf'@'localhost';
