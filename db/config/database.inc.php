<?php
date_default_timezone_set('UTC');

//----------------------------
// DATABASE CONFIGURATION
//----------------------------

/*

Valid types (adapters) are Postgres & MySQL:

'type' must be one of: 'pgsql' or 'mysql' or 'sqlite'

*/

$data = array(
    'db' => array(),
    'migrations_dir' => array(
        'default' => RUCKUSING_WORKING_BASE . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR . 'default',
        // 'local'    => RUCKUSING_WORKING_BASE . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR . 'local',
        // 'test'    => RUCKUSING_WORKING_BASE . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR . 'test',
        // 'pro'     => RUCKUSING_WORKING_BASE . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR . 'pro',
    ),
    'db_dir' => RUCKUSING_WORKING_BASE . DIRECTORY_SEPARATOR . 'schemas',
    'log_dir' => RUCKUSING_WORKING_BASE . DIRECTORY_SEPARATOR . 'logs',
    'ruckusing_base' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..'
);

// user / password
$databases = array(
    array('ps_local',      'hbPWp7AGcn5sasnv', 'local'),
    array('ps_local_pfin', 'cqLWXsdRrf7UxeEC', 'local_pfin'),
    array('ps_local_frei', 'WRnPeYfTAaedMZdE', 'local_frei'),
    array('ps_local_kitz', '55uHpbVQn4hCNTHn', 'local_kitz'),
    array('ps_local_geme', 'MNCDPdPuWGUv4bMA', 'local_geme'),
    array('ps_local_kloo', 'NWDFafBFp5udjWNa', 'local_kloo'),
    array('ps_local_test', 'N28tRUqZk6v8KKFQ', 'local_test', true),
    array('ps_local_demo', 'fp2khsNSnRUhBkw2', 'local_demo'),
    array('ps_dev',        'hbPWp7AGcn5sasnv', 'dev'),
    array('ps_dev_pfin',   'cqLWXsdRrf7UxeEC', 'dev_pfin'),
    array('ps_dev_frei',   'WRnPeYfTAaedMZdE', 'dev_frei'),
    array('ps_dev_kitz',   '55uHpbVQn4hCNTHn', 'dev_kitz'),
    array('ps_dev_geme',   'MNCDPdPuWGUv4bMA', 'dev_geme'),
    array('ps_dev_kloo',   'NWDFafBFp5udjWNa', 'dev_kloo'),
    array('ps_dev_test',   'N28tRUqZk6v8KKFQ', 'dev_test'),
    array('ps_dev_demo',   'fp2khsNSnRUhBkw2', 'dev_demo'),
    array('ps_test',       'hbPWp7AGcn5sasnv', 'test'),
    array('ps_test_pfin',  'cqLWXsdRrf7UxeEC', 'test_pfin'),
    array('ps_test_frei',  'WRnPeYfTAaedMZdE', 'test_frei'),
    array('ps_test_kitz',  '55uHpbVQn4hCNTHn', 'test_kitz'),
    array('ps_test_geme',  'MNCDPdPuWGUv4bMA', 'test_geme'),
    array('ps_test_kloo',  'NWDFafBFp5udjWNa', 'test_kloo'),
    array('ps_test_test',  'N28tRUqZk6v8KKFQ', 'test_test'),
    array('ps_test_demo',  'fp2khsNSnRUhBkw2', 'test_demo'),
    array('ps_pro',        'hbPWp7AGcn5sasnv', 'pro'),
    array('ps_pro_pfin',   'cqLWXsdRrf7UxeEC', 'pro_pfin'),
    array('ps_pro_frei',   'WRnPeYfTAaedMZdE', 'pro_frei'),
    array('ps_pro_kitz',   '55uHpbVQn4hCNTHn', 'pro_kitz'),
    array('ps_pro_geme',   'MNCDPdPuWGUv4bMA', 'pro_geme'),
    array('ps_pro_kloo',   'NWDFafBFp5udjWNa', 'pro_kloo'),
    array('ps_pro_test',   'N28tRUqZk6v8KKFQ', 'pro_test'),
    array('ps_pro_demo',   'fp2khsNSnRUhBkw2', 'pro_demo'),
);
foreach ($databases as $key => $value) {
    $user     = $value[0];
    $password = $value[1];
    $env      = isset($value[2]) ? $value[2] : 'na';
    $default  = (isset($value[3]) AND ($value[3] === true)) ? true: false;
    $data['db'][$env] = array(
        'type'        => 'mysql',
        'host'        => 'localhost',
        'port'        => 3306,
        'database'    => $user,
        'user'        => $user,
        'password'    => $password,
        //'charset'   => 'utf8',
        //'directory' => 'custom_name',
        //'socket'    => '/var/run/mysqld/mysqld.sock'
    );
    if ($default) {
        $data['db']['development'] = $data['db'][$env];
    }
}
return $data;
