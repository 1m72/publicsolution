<?php
date_default_timezone_set('UTC');

//----------------------------
// DATABASE CONFIGURATION
//----------------------------

/*

Valid types (adapters) are Postgres & MySQL:

'type' must be one of: 'pgsql' or 'mysql' or 'sqlite'

*/

$data = array(
    'db' => array(
        'development' => array(
            'type'        => 'mysql',
            'host'        => 'localhost',
            'port'        => 3306,
            'database'    => '',
            'user'        => '',
            'password'    => '',
            //'charset'   => 'utf8',
            //'directory' => 'custom_name',
            //'socket'    => '/var/run/mysqld/mysqld.sock'
        ),
        'main' => array(
            'type'        => 'mysql',
            'host'        => 'localhost',
            'port'        => 3306,
            'database'    => 'publicsolution',
            'user'        => 'publicsolution',
            'password'    => 'AUnT6cJ8HEvxzSQq',
            //'charset'   => 'utf8',
            //'directory' => 'custom_name',
            //'socket'    => '/var/run/mysqld/mysqld.sock'
        ),
        'pfinztal' => array(
            'type'        => 'mysql',
            'host'        => 'localhost',
            'port'        => 3306,
            'database'    => 'publicsolution_pfinztal',
            'user'        => 'ps_pfinztal',
            'password'    => '3CXAU7RXcDrSbHA2',
            //'charset'   => 'utf8',
            //'directory' => 'custom_name',
            //'socket'    => '/var/run/mysqld/mysqld.sock'
        ),
        'test' => array(
            'type'        => 'mysql',
            'host'        => 'localhost',
            'port'        => 3306,
            'database'    => 'publicsolution_test',
            'user'        => 'ps_test',
            'password'    => 'hbPWp7AGcn5sasnv',
            //'charset'   => 'utf8',
            //'directory' => 'custom_name',
            //'socket'    => '/var/run/mysqld/mysqld.sock'
        ),
        'freising' => array(
            'type'        => 'mysql',
            'host'        => 'localhost',
            'port'        => 3306,
            'database'    => 'publicsolution_freising',
            'user'        => 'ps_freising',
            'password'    => 'Ydnuz5maeWT8QThy',
            //'charset'   => 'utf8',
            //'directory' => 'custom_name',
            //'socket'    => '/var/run/mysqld/mysqld.sock'
        ),
        'kitzingen' => array(
            'type'        => 'mysql',
            'host'        => 'localhost',
            'port'        => 3306,
            'database'    => 'publicsolution_kitzingen',
            'user'        => 'ps_kitzingen',
            'password'    => 'BwZqfK2FNEBy4Whe',
            //'charset'   => 'utf8',
            //'directory' => 'custom_name',
            //'socket'    => '/var/run/mysqld/mysqld.sock'
        ),
        'gemeinde_kissleg' => array(
            'type'        => 'mysql',
            'host'        => 'localhost',
            'port'        => 3306,
            'database'    => 'gemeinde_kissleg',
            'user'        => 'gemeinde_kissleg',
            'password'    => 'hbzbf87cZRGsrfNH',
            //'charset'   => 'utf8',
            //'directory' => 'custom_name',
            //'socket'    => '/var/run/mysqld/mysqld.sock'
        ),

        'ps_local'      => array(
            'type'        => 'mysql',
            'host'        => 'localhost',
            'port'        => 3306,
            'database'    => 'ps_local',
            'user'        => 'ps_local',
            'password'    => 'hbPWp7AGcn5sasnv',
            //'charset'   => 'utf8',
            //'directory' => 'custom_name',
            //'socket'    => '/var/run/mysqld/mysqld.sock'
        ),
        'ps_local_pfin' => array(
            'type'        => 'mysql',
            'host'        => 'localhost',
            'port'        => 3306,
            'database'    => 'ps_local_pfin',
            'user'        => 'ps_local_pfin',
            'password'    => 'cqLWXsdRrf7UxeEC',
            //'charset'   => 'utf8',
            //'directory' => 'custom_name',
            //'socket'    => '/var/run/mysqld/mysqld.sock'
        ),
        'ps_local_frei' => array(
            'type'        => 'mysql',
            'host'        => 'localhost',
            'port'        => 3306,
            'database'    => 'ps_local_frei',
            'user'        => 'ps_local_frei',
            'password'    => 'WRnPeYfTAaedMZdE',
            //'charset'   => 'utf8',
            //'directory' => 'custom_name',
            //'socket'    => '/var/run/mysqld/mysqld.sock'
        ),
        'ps_local_kitz' => array(
            'type'        => 'mysql',
            'host'        => 'localhost',
            'port'        => 3306,
            'database'    => 'ps_local_kitz',
            'user'        => 'ps_local_kitz',
            'password'    => '55uHpbVQn4hCNTHn',
            //'charset'   => 'utf8',
            //'directory' => 'custom_name',
            //'socket'    => '/var/run/mysqld/mysqld.sock'
        ),
        'ps_local_geme' => array(
            'type'        => 'mysql',
            'host'        => 'localhost',
            'port'        => 3306,
            'database'    => 'ps_local_geme',
            'user'        => 'ps_local_geme',
            'password'    => 'MNCDPdPuWGUv4bMA',
            //'charset'   => 'utf8',
            //'directory' => 'custom_name',
            //'socket'    => '/var/run/mysqld/mysqld.sock'
        ),
        'ps_local_kloo' => array(
            'type'        => 'mysql',
            'host'        => 'localhost',
            'port'        => 3306,
            'database'    => 'ps_local_kloo',
            'user'        => 'ps_local_kloo',
            'password'    => 'NWDFafBFp5udjWNa',
            //'charset'   => 'utf8',
            //'directory' => 'custom_name',
            //'socket'    => '/var/run/mysqld/mysqld.sock'
        ),
    ),
    'migrations_dir' => array(
        'default' => RUCKUSING_WORKING_BASE . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR . 'local',
        'test'    => RUCKUSING_WORKING_BASE . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR . 'test',
        'pro'     => RUCKUSING_WORKING_BASE . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR . 'pro',
    ),
    'db_dir'         => RUCKUSING_WORKING_BASE . DIRECTORY_SEPARATOR . 'schemas',
    'log_dir'        => RUCKUSING_WORKING_BASE . DIRECTORY_SEPARATOR . 'logs',
    'ruckusing_base' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..'
);

$data['db']['development'] = $data['db']['main'];
return $data;
