<?php

class AddLevelToObjectPlaceTable extends Ruckusing_Migration_Base
{
    public function up()
    {
        if (!$this->column_exist('object_place', 'level')) {
            $this->add_column('object_place', 'level', 'integer', ['null' => false]);
        }
    }//up()

    public function down()
    {
        if ($this->column_exist('object_place', 'level')) {
            $this->remove_column('object_place', 'level');
        }
    }//down()
}