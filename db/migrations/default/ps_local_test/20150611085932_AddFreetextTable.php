<?php
/**
 * Add column free_text, employee_group_id, cost_center_id to freetext table
 * @author Chien Tran <tran.duc.chien@kloon.vn>
 * @time 2015-06-10
 */

class AddFreetextTable extends Ruckusing_Migration_Base
{
    public function up() {
        if (!$this->table_exists('freetext')) {
            $table = $this->create_table('freetext');
            $table->column('id', 'integer', array('primary_key' => true));
            $table->column('parent_id', 'integer');
            $table->column('level', 'integer');
            $table->column('title', 'string');
            $table->column('created_at', 'datetime', array('null' => false));
            $table->column('updated_at', 'datetime', array('null' => false));
            $table->column('deleted_at', 'datetime', array('null' => false));
        }
    }

    public function down() {
        if ($this->table_exists('freetext')) {
            $this->drop_table('freetext');
        }
    }
}