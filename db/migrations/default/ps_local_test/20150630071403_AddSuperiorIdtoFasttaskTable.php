<?php

class AddSuperiorIdtoFasttaskTable extends Ruckusing_Migration_Base
{
    public function up()
    {
        if (!$this->column_exist('fasttask', 'superior_id')) {
            $this->add_column('fasttask', 'superior_id', 'integer', array('after' => 'id_employee', 'null' => false));
        }
    }//up()

    public function down()
    {
        if ($this->column_exist('fasttask', 'superior_id')) {
            $this->remove_column('fasttask', 'superior_id');
        }
    }//down()
}
