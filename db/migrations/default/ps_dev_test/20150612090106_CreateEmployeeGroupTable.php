<?php

class CreateEmployeeGroupTable extends Ruckusing_Migration_Base
{
	public function up()
	{
		$table = $this->create_table('employee_group');
		$table->column('id', 'integer', array('primary' => true, 'auto_increment' => true));
		$table->column('title', 'string');
		$table->column("created_at", "datetime", array('null' => false));
		$table->column("updated_at", "datetime", array('null' => false));
		$table->column("deleted_at", "datetime", array('null' => false));
		$table->finish();
	}

	public function down()
	{
		$this->drop_table('employee_group');
	}
}
