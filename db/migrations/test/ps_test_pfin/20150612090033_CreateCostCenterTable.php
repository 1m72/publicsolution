<?php

class CreateCostCenterTable extends Ruckusing_Migration_Base
{
    public function up()
    {
        $table = $this->create_table('cost_center');
        // $table->column("id", "integer", array('primary_key' => true));
        $table->column("employee_group_id", "integer", array('null' => false));
        $table->column("title", "string");
        $table->column("created_at", "datetime", array('null' => true));
        $table->column("updated_at", "datetime", array('null' => true));
        $table->column("deleted_at", "datetime", array('null' => true));
        $table->finish();
    }

    public function down()
    {
        $this->drop_table('cost_center');
    }
}
