<?php
/**
 * Add column free_text, employee_group_id, cost_center_id to freetext table
 * @author Chien Tran <tran.duc.chien@kloon.vn>
 * @time 2015-06-10
 */

class AddFreetextCostcenterToFreetextTable extends Ruckusing_Migration_Base
{
	public function up() {
		$this->add_column('fasttask', 'free_text', 'string', array('after' => 'id_employee'));
		$this->add_column('fasttask', 'employee_group_id', 'integer', array('after' => 'id_employee'));
		$this->add_column('fasttask', 'cost_center_id', 'integer', array('after' => 'id_employee'));
	}

	public function down() {
		$this->remove_column('fasttask', 'free_text');
		$this->remove_column('fasttask', 'employee_group_id');
		$this->remove_column('fasttask', 'cost_center_id');
	}
}
