<?php

class AddSuperiorIdtoFasttaskTable extends Ruckusing_Migration_Base
{
    public function up()
    {
        $this->add_column('fasttask', 'superior_id', 'integer', array('after' => 'id_employee', 'null' => false));
    }//up()

    public function down()
    {
        $this->remove_column('fasttask', 'superior_id');
    }//down()
}
