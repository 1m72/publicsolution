-- phpMyAdmin SQL Dump
-- version 4.4.6.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 12, 2015 at 12:58 PM
-- Server version: 5.6.16-log
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `publicsolution_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(11) NOT NULL,
  `id_ref` varchar(255) NOT NULL COMMENT 'Link to Old DB',
  `id_city` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `color` varchar(20) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `addon`
--

CREATE TABLE IF NOT EXISTS `addon` (
  `id` int(11) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `id_ref` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `addon_module`
--

CREATE TABLE IF NOT EXISTS `addon_module` (
  `id` int(11) NOT NULL,
  `id_addon` int(11) DEFAULT NULL,
  `id_module` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `addon_module_activity`
--

CREATE TABLE IF NOT EXISTS `addon_module_activity` (
  `id` int(11) NOT NULL,
  `id_addon_module` int(11) DEFAULT NULL,
  `id_activity` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL,
  `id_city` int(11) NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `description` varchar(1500) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE IF NOT EXISTS `devices` (
  `id` int(11) NOT NULL,
  `id_device` varchar(255) NOT NULL,
  `id_city` int(11) NOT NULL,
  `id_unique` varchar(255) NOT NULL,
  `app` varchar(50) DEFAULT 'winterdienst',
  `notification` tinyint(1) NOT NULL DEFAULT '1',
  `actived` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_activity`
--

CREATE TABLE IF NOT EXISTS `employee_activity` (
  `id` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `id_activity` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fasttask`
--

CREATE TABLE IF NOT EXISTS `fasttask` (
  `id` int(11) NOT NULL,
  `id_task` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `id_object` int(11) NOT NULL,
  `id_city` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `cost_center_id` int(11) DEFAULT NULL,
  `employee_group_id` int(11) DEFAULT NULL,
  `free_text` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `freetext`
--

CREATE TABLE IF NOT EXISTS `freetext` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `machine`
--

CREATE TABLE IF NOT EXISTS `machine` (
  `id` int(11) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `id_ref` varchar(255) NOT NULL COMMENT 'Link to Old DB',
  `id_city` int(11) NOT NULL,
  `nfc_code` varchar(255) NOT NULL,
  `machine_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_default` int(11) NOT NULL DEFAULT '0',
  `description` varchar(1500) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `machine_activity`
--

CREATE TABLE IF NOT EXISTS `machine_activity` (
  `id` int(11) NOT NULL,
  `id_machine` int(11) NOT NULL,
  `id_add_on` int(11) NOT NULL DEFAULT '0',
  `id_activity` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `machine_addon_module`
--

CREATE TABLE IF NOT EXISTS `machine_addon_module` (
  `id` int(11) NOT NULL,
  `id_machine_module` int(11) DEFAULT NULL,
  `id_addon_module` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `machine_add_on`
--

CREATE TABLE IF NOT EXISTS `machine_add_on` (
  `id` int(11) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `id_ref` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `machine_material`
--

CREATE TABLE IF NOT EXISTS `machine_material` (
  `id` int(11) NOT NULL,
  `id_machine` int(11) NOT NULL,
  `id_material` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `machine_material_used`
--

CREATE TABLE IF NOT EXISTS `machine_material_used` (
  `id` int(11) NOT NULL,
  `id_city` int(11) NOT NULL,
  `id_material` int(11) NOT NULL,
  `id_worker_gps` int(11) NOT NULL,
  `amount` float NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `machine_module`
--

CREATE TABLE IF NOT EXISTS `machine_module` (
  `id` int(10) unsigned NOT NULL,
  `id_machine` int(11) DEFAULT NULL,
  `id_module` int(255) DEFAULT NULL,
  `id_default` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `machine_module_activity`
--

CREATE TABLE IF NOT EXISTS `machine_module_activity` (
  `id` int(11) NOT NULL,
  `id_machine_module` int(11) DEFAULT NULL,
  `id_activity` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE IF NOT EXISTS `material` (
  `id` int(11) NOT NULL,
  `id_ref` varchar(255) NOT NULL COMMENT 'Link to Old DB',
  `id_city` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `unit` varchar(50) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) unsigned NOT NULL,
  `id_parent` int(11) DEFAULT '0',
  `type` varchar(50) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nfc`
--

CREATE TABLE IF NOT EXISTS `nfc` (
  `id` int(11) NOT NULL,
  `id_company` int(11) NOT NULL,
  `code` varchar(25) NOT NULL,
  `nfc_code` varchar(255) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) NOT NULL,
  `type` varchar(25) NOT NULL,
  `intro` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `readabled_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `object`
--

CREATE TABLE IF NOT EXISTS `object` (
  `id` int(11) NOT NULL,
  `id_ref` varchar(255) NOT NULL COMMENT 'Link to Old DB',
  `id_city` int(11) NOT NULL,
  `id_group` int(11) NOT NULL COMMENT 'id_place',
  `id_parent` int(11) NOT NULL DEFAULT '0' COMMENT 'Bỏ',
  `name` varchar(255) NOT NULL,
  `lat` varchar(25) NOT NULL,
  `lon` varchar(25) NOT NULL,
  `position` mediumtext NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `object_place`
--

CREATE TABLE IF NOT EXISTS `object_place` (
  `id` int(11) NOT NULL,
  `id_ref` varchar(255) NOT NULL COMMENT 'Link to Old DB',
  `id_city` int(11) NOT NULL,
  `id_parent` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `lat` varchar(25) NOT NULL,
  `lon` varchar(25) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post_service_logs`
--

CREATE TABLE IF NOT EXISTS `post_service_logs` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `request_data` text,
  `request_extra` varchar(255) DEFAULT NULL,
  `request_at` datetime DEFAULT NULL,
  `response_data` text,
  `response_extra` text NOT NULL,
  `status` varchar(50) NOT NULL,
  `retry` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `worker`
--

CREATE TABLE IF NOT EXISTS `worker` (
  `id` int(11) NOT NULL,
  `id_ref` varchar(255) NOT NULL COMMENT 'Link to Old DB',
  `id_city` int(11) NOT NULL,
  `id_company` int(11) NOT NULL,
  `id_teamleader` int(11) NOT NULL DEFAULT '0',
  `nfc_code` varchar(255) NOT NULL,
  `personal_code` varchar(255) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `id_default` int(11) NOT NULL DEFAULT '0',
  `price` float NOT NULL DEFAULT '0',
  `address` varchar(750) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `birthday` varchar(15) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `worker_activity`
--

CREATE TABLE IF NOT EXISTS `worker_activity` (
  `id` int(11) NOT NULL,
  `id_city` int(11) NOT NULL,
  `id_wkgps` int(11) NOT NULL,
  `id_activity` int(11) NOT NULL,
  `id_task` varchar(255) NOT NULL COMMENT 'Device TaskID (Mapping upload resource)',
  `id_task_activity` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `street_old` varchar(255) NOT NULL,
  `street_resolved_at` datetime DEFAULT NULL,
  `street_resolved_by` varchar(255) DEFAULT NULL,
  `service` varchar(255) NOT NULL,
  `group_street` int(11) NOT NULL DEFAULT '0',
  `latitude` varchar(25) NOT NULL,
  `longtitude` varchar(25) NOT NULL,
  `starttime` datetime NOT NULL,
  `endtime` datetime NOT NULL,
  `end_lat_lon` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `worker_activity_saved`
--

CREATE TABLE IF NOT EXISTS `worker_activity_saved` (
  `id` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `id_activity` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `worker_gps`
--

CREATE TABLE IF NOT EXISTS `worker_gps` (
  `id` int(11) NOT NULL,
  `id_cron` int(11) NOT NULL DEFAULT '0',
  `id_city` int(11) NOT NULL,
  `id_worker` int(11) NOT NULL,
  `id_object` int(11) NOT NULL DEFAULT '0',
  `id_task` varchar(255) NOT NULL,
  `id_machine` int(11) NOT NULL,
  `app` int(11) DEFAULT NULL,
  `position` mediumtext NOT NULL,
  `position_clean` mediumtext,
  `ontime` datetime NOT NULL,
  `status` enum('pending','success','error') NOT NULL DEFAULT 'pending',
  `upload_flag` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `total_activity` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `worker_gps_split`
--

CREATE TABLE IF NOT EXISTS `worker_gps_split` (
  `id` int(11) NOT NULL,
  `id_task` varchar(255) NOT NULL,
  `id_worker_gps` int(11) NOT NULL,
  `id_task_activity` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `worker_information`
--

CREATE TABLE IF NOT EXISTS `worker_information` (
  `id` int(11) NOT NULL,
  `id_city` int(11) NOT NULL,
  `id_config` int(11) NOT NULL COMMENT 'Record config server storage in city_config table',
  `id_worker_gps` int(11) NOT NULL,
  `id_task` varchar(255) NOT NULL,
  `app` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=winter;1=fasttask',
  `type` tinyint(4) NOT NULL COMMENT '1=message, 2=voice, 3=image',
  `data` text NOT NULL,
  `rotate` varchar(255) NOT NULL,
  `lat` varchar(25) NOT NULL,
  `lon` varchar(25) NOT NULL,
  `street` varchar(255) NOT NULL,
  `id_wk_activity` int(11) NOT NULL DEFAULT '0',
  `time` datetime NOT NULL,
  `status` enum('pending','success','error') NOT NULL DEFAULT 'pending',
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `worker_module`
--

CREATE TABLE IF NOT EXISTS `worker_module` (
  `id` int(11) NOT NULL,
  `id_worker` int(11) NOT NULL,
  `id_module` int(11) NOT NULL,
  `id_default` int(11) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `worker_module_activity`
--

CREATE TABLE IF NOT EXISTS `worker_module_activity` (
  `id` int(11) NOT NULL,
  `id_worker_module` int(11) NOT NULL,
  `id_activity` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon`
--
ALTER TABLE `addon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_module`
--
ALTER TABLE `addon_module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_module_activity`
--
ALTER TABLE `addon_module_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_activity`
--
ALTER TABLE `employee_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fasttask`
--
ALTER TABLE `fasttask`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `freetext`
--
ALTER TABLE `freetext`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machine`
--
ALTER TABLE `machine`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `machine_activity`
--
ALTER TABLE `machine_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machine_addon_module`
--
ALTER TABLE `machine_addon_module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machine_add_on`
--
ALTER TABLE `machine_add_on`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machine_material`
--
ALTER TABLE `machine_material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machine_material_used`
--
ALTER TABLE `machine_material_used`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machine_module`
--
ALTER TABLE `machine_module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machine_module_activity`
--
ALTER TABLE `machine_module_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nfc`
--
ALTER TABLE `nfc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `object`
--
ALTER TABLE `object`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `object_place`
--
ALTER TABLE `object_place`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_service_logs`
--
ALTER TABLE `post_service_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `worker`
--
ALTER TABLE `worker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `worker_activity`
--
ALTER TABLE `worker_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `worker_activity_saved`
--
ALTER TABLE `worker_activity_saved`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `worker_gps`
--
ALTER TABLE `worker_gps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `worker_gps_split`
--
ALTER TABLE `worker_gps_split`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_task` (`id_task_activity`);

--
-- Indexes for table `worker_information`
--
ALTER TABLE `worker_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `worker_module`
--
ALTER TABLE `worker_module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `worker_module_activity`
--
ALTER TABLE `worker_module_activity`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `addon`
--
ALTER TABLE `addon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `addon_module`
--
ALTER TABLE `addon_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `addon_module_activity`
--
ALTER TABLE `addon_module_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_activity`
--
ALTER TABLE `employee_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fasttask`
--
ALTER TABLE `fasttask`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `freetext`
--
ALTER TABLE `freetext`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `machine`
--
ALTER TABLE `machine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `machine_activity`
--
ALTER TABLE `machine_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `machine_addon_module`
--
ALTER TABLE `machine_addon_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `machine_add_on`
--
ALTER TABLE `machine_add_on`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `machine_material`
--
ALTER TABLE `machine_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `machine_material_used`
--
ALTER TABLE `machine_material_used`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `machine_module`
--
ALTER TABLE `machine_module`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `machine_module_activity`
--
ALTER TABLE `machine_module_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `material`
--
ALTER TABLE `material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nfc`
--
ALTER TABLE `nfc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `object`
--
ALTER TABLE `object`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `object_place`
--
ALTER TABLE `object_place`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `post_service_logs`
--
ALTER TABLE `post_service_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `worker`
--
ALTER TABLE `worker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `worker_activity`
--
ALTER TABLE `worker_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `worker_activity_saved`
--
ALTER TABLE `worker_activity_saved`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `worker_gps`
--
ALTER TABLE `worker_gps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `worker_gps_split`
--
ALTER TABLE `worker_gps_split`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `worker_information`
--
ALTER TABLE `worker_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `worker_module`
--
ALTER TABLE `worker_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `worker_module_activity`
--
ALTER TABLE `worker_module_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
