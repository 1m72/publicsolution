# Migrate
php migrate db:migrate env=ps_local && \
php migrate db:migrate env=ps_dev && \
php migrate db:migrate env=ps_test && \
php migrate db:migrate env=ps_pro && \

php migrate db:migrate env=ps_local_pfin && \
php migrate db:migrate env=ps_local_frei && \
php migrate db:migrate env=ps_local_kitz && \
php migrate db:migrate env=ps_local_geme && \
php migrate db:migrate env=ps_local_kloo && \
php migrate db:migrate env=ps_local_test && \
php migrate db:migrate env=ps_local_demo && \
php migrate db:migrate env=ps_dev_pfin && \
php migrate db:migrate env=ps_dev_frei && \
php migrate db:migrate env=ps_dev_kitz && \
php migrate db:migrate env=ps_dev_geme && \
php migrate db:migrate env=ps_dev_kloo && \
php migrate db:migrate env=ps_dev_test && \
php migrate db:migrate env=ps_dev_demo && \
php migrate db:migrate env=ps_test_pfin && \
php migrate db:migrate env=ps_test_frei && \
php migrate db:migrate env=ps_test_kitz && \
php migrate db:migrate env=ps_test_geme && \
php migrate db:migrate env=ps_test_kloo && \
php migrate db:migrate env=ps_test_test && \
php migrate db:migrate env=ps_test_demo && \
php migrate db:migrate env=ps_pro_pfin && \
php migrate db:migrate env=ps_pro_frei && \
php migrate db:migrate env=ps_pro_kitz && \
php migrate db:migrate env=ps_pro_geme && \
php migrate db:migrate env=ps_pro_kloo && \
php migrate db:migrate env=ps_pro_test && \
php migrate db:migrate env=ps_pro_demo

# Schemas
php migrate db:schema env=ps_local      && \
php migrate db:schema env=ps_local_pfin && \
php migrate db:schema env=ps_local_frei && \
php migrate db:schema env=ps_local_kitz && \
php migrate db:schema env=ps_local_geme && \
php migrate db:schema env=ps_local_kloo && \
php migrate db:schema env=ps_local_test && \
php migrate db:schema env=ps_local_demo && \
php migrate db:schema env=ps_dev        && \
php migrate db:schema env=ps_dev_pfin   && \
php migrate db:schema env=ps_dev_frei   && \
php migrate db:schema env=ps_dev_kitz   && \
php migrate db:schema env=ps_dev_geme   && \
php migrate db:schema env=ps_dev_kloo   && \
php migrate db:schema env=ps_dev_test   && \
php migrate db:schema env=ps_dev_demo   && \
php migrate db:schema env=ps_test       && \
php migrate db:schema env=ps_test_pfin  && \
php migrate db:schema env=ps_test_frei  && \
php migrate db:schema env=ps_test_kitz  && \
php migrate db:schema env=ps_test_geme  && \
php migrate db:schema env=ps_test_kloo  && \
php migrate db:schema env=ps_test_test  && \
php migrate db:schema env=ps_test_demo  && \
php migrate db:schema env=ps_pro        && \
php migrate db:schema env=ps_pro_pfin   && \
php migrate db:schema env=ps_pro_frei   && \
php migrate db:schema env=ps_pro_kitz   && \
php migrate db:schema env=ps_pro_geme   && \
php migrate db:schema env=ps_pro_kloo   && \
php migrate db:schema env=ps_pro_test   && \
php migrate db:schema env=ps_pro_demo

# Setup
php migrate db:setup env=ps_local && \
php migrate db:setup env=ps_dev && \
php migrate db:setup env=ps_test && \
php migrate db:setup env=ps_pro && \

php migrate db:setup env=ps_local_pfin && \
php migrate db:setup env=ps_local_frei && \
php migrate db:setup env=ps_local_kitz && \
php migrate db:setup env=ps_local_geme && \
php migrate db:setup env=ps_local_kloo && \
php migrate db:setup env=ps_local_test && \
php migrate db:setup env=ps_local_demo && \
php migrate db:setup env=ps_dev_pfin && \
php migrate db:setup env=ps_dev_frei && \
php migrate db:setup env=ps_dev_kitz && \
php migrate db:setup env=ps_dev_geme && \
php migrate db:setup env=ps_dev_kloo && \
php migrate db:setup env=ps_dev_test && \
php migrate db:setup env=ps_dev_demo && \
php migrate db:setup env=ps_test_pfin && \
php migrate db:setup env=ps_test_frei && \
php migrate db:setup env=ps_test_kitz && \
php migrate db:setup env=ps_test_geme && \
php migrate db:setup env=ps_test_kloo && \
php migrate db:setup env=ps_test_test && \
php migrate db:setup env=ps_test_demo && \
php migrate db:setup env=ps_pro_pfin && \
php migrate db:setup env=ps_pro_frei && \
php migrate db:setup env=ps_pro_kitz && \
php migrate db:setup env=ps_pro_geme && \
php migrate db:setup env=ps_pro_kloo && \
php migrate db:setup env=ps_pro_test && \
php migrate db:setup env=ps_pro_demo

# Sync
rsync -rv --delete migrations/default/ps_local/ migrations/default/ps_dev/ && \
rsync -rv --delete migrations/default/ps_local/ migrations/default/ps_test/ && \
rsync -rv --delete migrations/default/ps_local/ migrations/default/ps_pro/ && \

rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_local_pfin && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_local_frei && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_local_kitz && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_local_geme && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_local_kloo && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_local_demo && \

rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_dev_pfin && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_dev_frei && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_dev_kitz && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_dev_geme && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_dev_kloo && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_dev_demo && \

rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_test_pfin && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_test_frei && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_test_kitz && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_test_geme && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_test_kloo && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_test_demo && \

rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_pro_pfin && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_pro_frei && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_pro_kitz && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_pro_geme && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_pro_kloo && \
rsync -rv --delete migrations/default/ps_local_test/ migrations/default/ps_pro_demo && \

rsync -rv --delete migrations/default/ migrations/dev && \
rsync -rv --delete migrations/default/ migrations/test && \
rsync -rv --delete migrations/default/ migrations/pro

# Create folder
mkdir -p migrations/default/ps_local      && touch migrations/default/ps_local/.gitkeep && \
mkdir -p migrations/default/ps_local_pfin && touch migrations/default/ps_local_pfin/.gitkeep && \
mkdir -p migrations/default/ps_local_frei && touch migrations/default/ps_local_frei/.gitkeep && \
mkdir -p migrations/default/ps_local_kitz && touch migrations/default/ps_local_kitz/.gitkeep && \
mkdir -p migrations/default/ps_local_geme && touch migrations/default/ps_local_geme/.gitkeep && \
mkdir -p migrations/default/ps_local_kloo && touch migrations/default/ps_local_kloo/.gitkeep && \
mkdir -p migrations/default/ps_local_test && touch migrations/default/ps_local_test/.gitkeep && \
mkdir -p migrations/default/ps_local_demo && touch migrations/default/ps_local_demo/.gitkeep && \
mkdir -p migrations/default/ps_dev        && touch migrations/default/ps_dev/.gitkeep && \
mkdir -p migrations/default/ps_dev_pfin   && touch migrations/default/ps_dev_pfin/.gitkeep && \
mkdir -p migrations/default/ps_dev_frei   && touch migrations/default/ps_dev_frei/.gitkeep && \
mkdir -p migrations/default/ps_dev_kitz   && touch migrations/default/ps_dev_kitz/.gitkeep && \
mkdir -p migrations/default/ps_dev_geme   && touch migrations/default/ps_dev_geme/.gitkeep && \
mkdir -p migrations/default/ps_dev_kloo   && touch migrations/default/ps_dev_kloo/.gitkeep && \
mkdir -p migrations/default/ps_dev_test   && touch migrations/default/ps_dev_test/.gitkeep && \
mkdir -p migrations/default/ps_dev_demo   && touch migrations/default/ps_dev_demo/.gitkeep && \
mkdir -p migrations/default/ps_test       && touch migrations/default/ps_test/.gitkeep && \
mkdir -p migrations/default/ps_test_pfin  && touch migrations/default/ps_test_pfin/.gitkeep && \
mkdir -p migrations/default/ps_test_frei  && touch migrations/default/ps_test_frei/.gitkeep && \
mkdir -p migrations/default/ps_test_kitz  && touch migrations/default/ps_test_kitz/.gitkeep && \
mkdir -p migrations/default/ps_test_geme  && touch migrations/default/ps_test_geme/.gitkeep && \
mkdir -p migrations/default/ps_test_kloo  && touch migrations/default/ps_test_kloo/.gitkeep && \
mkdir -p migrations/default/ps_test_test  && touch migrations/default/ps_test_test/.gitkeep && \
mkdir -p migrations/default/ps_test_demo  && touch migrations/default/ps_test_demo/.gitkeep && \
mkdir -p migrations/default/ps_pro        && touch migrations/default/ps_pro/.gitkeep && \
mkdir -p migrations/default/ps_pro_pfin   && touch migrations/default/ps_pro_pfin/.gitkeep && \
mkdir -p migrations/default/ps_pro_frei   && touch migrations/default/ps_pro_frei/.gitkeep && \
mkdir -p migrations/default/ps_pro_kitz   && touch migrations/default/ps_pro_kitz/.gitkeep && \
mkdir -p migrations/default/ps_pro_geme   && touch migrations/default/ps_pro_geme/.gitkeep && \
mkdir -p migrations/default/ps_pro_kloo   && touch migrations/default/ps_pro_kloo/.gitkeep && \
mkdir -p migrations/default/ps_pro_test   && touch migrations/default/ps_pro_test/.gitkeep && \
mkdir -p migrations/default/ps_pro_demo   && touch migrations/default/ps_pro_demo/.gitkeep && \