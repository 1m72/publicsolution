Public Solution
===============

Version
---
1.0

Developer
---
1. tran.duc.chien@kloon.vn
2. phung.manh.huong@kloon.vn
3. le.nhung@kloon.vn
4. nguyen.quynh.chi@kloon.vn

API
---
- chmod : /sync, /uploads
- symbol link: /sync, /uploads

APP
---
- chmod /tmp, /tmp/captcha
- symbol link /media
- smtp account for send mail
- Account Main Version : bis-office.com Dev Version : dev.bis-ofice.com
 - username : andreas   - salt : tQrQAdJj - password : osuOo5fX2hKPheqQ - hash_password : 275001568f5dd11a98abfe322af6c373
 - username : admin     - salt : as#fs^%  - password : Kloon.vn         - hash_password : e4ca6181bd406da9fb530cb83e477a71
 - username : freising  - salt : nmWdbYHE - password : EDZHcxE6923FjvPx - hash_password : 2f5aea6d9ce59d68f861cdb09ec71ae9
 - username : pfinztal  - salt : Q3Qg4VHW - password : hA2wKdLv1UGfmQO9 - hash_password : c9b0bbafbd01a4b78f52b231ecca46fb
 - username : kitzingen - salt : Q3Qg4VHW - password : hA2wKdLv1UGfmQO9 - hash_password : c9b0bbafbd01a4b78f52b231ecca46fb

LIB
---
- memcached, php5-mecached
- beanstalk
- curl, php5-curl
- avconv + lame (avconv OR ffmpeg)
   - Convert audio, video file to webm. (Syntax: avconv -i ca9beddbd5c8212620140218135644_movie0.mp4 -ar 44100 1a.webm)
   - rotate video :
        - install package: libavcodec-extra-53
        - command : avconv -i intro.mp4 -c:v libx264 -preset veryfast -codec:a copy -vf "transpose=1" intro_new.mp4
                    ffmpeg -i 9abb1a8f6fd2897320140414073703_voice0.3gp -ar 32k -f mp3 out.mp3

SQL
---
- Convert id_city : UPDATE `activities` SET id_city = 1; UPDATE `company` SET id_city = 1; UPDATE `devices` SET id_city = 1; UPDATE `fasttask` SET id_city = 1; UPDATE `machine` SET id_city = 1; UPDATE `machine_material_used` SET id_city = 1; UPDATE `material` SET id_city = 1; UPDATE `object` SET id_city = 1; UPDATE `object_place` SET id_city = 1; UPDATE `worker` SET id_city = 1; UPDATE `worker_activity` SET id_city = 1; UPDATE `worker_gps` SET id_city = 1; UPDATE `worker_information` SET id_city = 1;
- Select worker_activity by id_wkgps: SELECT street, min(starttime) AS 'start_time', max(starttime) as 'end_time', (max(starttime) - min(starttime)) as duration, group_street, GROUP_CONCAT(id) as id_range  FROM `worker_activity` WHERE `id_wkgps` = 1 GROUP BY id_wkgps, id_activity, id_task_activity, street, group_street ORDER BY id asc
- Grand permisson : GRANT ALL PRIVILEGES ON `test\_publicsolution\_demo`.* TO 'ps_demo'@'%'; GRANT ALL PRIVILEGES ON `test\_publicsolution\_demo`.* TO 'ps_demo'@'localhost'; GRANT ALL PRIVILEGES ON `test\_publicsolution\_test`.* TO 'ps_test'@'%'; GRANT ALL PRIVILEGES ON `test\_publicsolution\_test`.* TO 'ps_test'@'localhost'; GRANT ALL PRIVILEGES ON `test\_publicsolution\_pfinztal`.* TO 'ps_pfinztal'@'%'; GRANT ALL PRIVILEGES ON `test\_publicsolution\_pfinztal`.* TO 'ps_pfinztal'@'localhost'; GRANT ALL PRIVILEGES ON `test\_publicsolution\_freising`.* TO 'ps_freising'@'%'; GRANT ALL PRIVILEGES ON `test\_publicsolution\_freising`.* TO 'ps_freising'@'localhost'; GRANT ALL PRIVILEGES ON `test\_publicsolution\_template`.* TO 'ps_template'@'%'; GRANT ALL PRIVILEGES ON `test\_publicsolution\_template`.* TO 'ps_template'@'localhost'; GRANT ALL PRIVILEGES ON `test\_publicsolution`.* TO 'publicsolution'@'%'; GRANT ALL PRIVILEGES ON `test\_publicsolution`.* TO 'publicsolution'@'localhost';
- Views : DROP VIEW `v_activity`, `v_addon`, `v_company`, `v_devices`, `v_machine`, `v_machine_join`, `v_material`, `v_modules`, `v_nfc`, `v_objects`, `v_worker`, `v_worker_join`; CREATE VIEW v_activity AS select `activities`.`id` AS `id`,`activities`.`id_ref` AS `id_ref`,`activities`.`id_city` AS `id_city`,`activities`.`name` AS `name`,`activities`.`color` AS `color`,`activities`.`deleted_at` AS `deleted_at`,`activities`.`updated_at` AS `updated_at`,`activities`.`created_at` AS `created_at` from `activities` where isnull(`activities`.`deleted_at`); CREATE VIEW v_addon AS select `addon`.`id` AS `id`,`addon`.`id_ref` AS `id_ref`,`addon`.`identifier` AS `identifier`,`addon`.`name` AS `name`,`addon`.`deleted_at` AS `deleted_at`,`addon`.`updated_at` AS `updated_at`,`addon`.`created_at` AS `created_at` from `addon` where isnull(`addon`.`deleted_at`); CREATE VIEW v_company AS select `company`.`id` AS `id`,`company`.`id_city` AS `id_city`,`company`.`id_user` AS `id_user`,`company`.`name` AS `name`,`company`.`description` AS `description`,`company`.`deleted_at` AS `deleted_at`,`company`.`updated_at` AS `updated_at`,`company`.`created_at` AS `created_at` from `company` where isnull(`company`.`deleted_at`); CREATE VIEW v_devices AS select `devices`.`id` AS `id`,`devices`.`id_device` AS `id_device`,`devices`.`id_city` AS `id_city`,`devices`.`id_unique` AS `id_unique`,`devices`.`actived` AS `actived`,`devices`.`deleted_at` AS `deleted_at`,`devices`.`updated_at` AS `updated_at`,`devices`.`created_at` AS `created_at` from `devices` where isnull(`devices`.`deleted_at`); CREATE VIEW v_machine AS (select `machine`.`id` AS `id`,`machine`.`id_ref` AS `id_ref`,`machine`.`id_city` AS `id_city`,`machine`.`nfc_code` AS `nfc_code`,`machine`.`identifier` AS `identifier`,`machine`.`name` AS `name`,`machine`.`description` AS `description`,`machine`.`deleted_at` AS `deleted_at`,`machine`.`updated_at` AS `updated_at`,`machine`.`created_at` AS `created_at` from `machine` where isnull(`machine`.`deleted_at`)); CREATE VIEW v_machine_join AS select `mc`.`id` AS `id`,`mc`.`id_ref` AS `id_ref`,`mc`.`id_city` AS `id_city`,`nf`.`code` AS `code`,`mc`.`nfc_code` AS `nfc_code`,`mc`.`machine_code` AS `machine_code`,`mc`.`identifier` AS `identifier`,`mc`.`name` AS `name`,`mc`.`id_default` AS `id_default`,`mc`.`description` AS `description`,`mc`.`updated_at` AS `updated_at`,`mc`.`created_at` AS `created_at` from (`machine` `mc` left join `nfc` `nf` on((`nf`.`nfc_code` = `mc`.`nfc_code`))) where (isnull(`mc`.`deleted_at`) and isnull(`nf`.`deleted_at`)); CREATE VIEW v_material AS select `material`.`id` AS `id`,`material`.`id_ref` AS `id_ref`,`material`.`id_city` AS `id_city`,`material`.`name` AS `name`,`material`.`unit` AS `unit`,`material`.`deleted_at` AS `deleted_at`,`material`.`updated_at` AS `updated_at`,`material`.`created_at` AS `created_at` from `material` where isnull(`material`.`deleted_at`); CREATE VIEW v_modules AS SELECT `modules`.`id` AS `id`, `modules`.`id_parent` AS `id_parent`, `modules`.`title` AS `title`, `modules`.`deleted_at` AS `deleted_at`, `modules`.`created_at` AS `created_at`, `modules`.`updated_at` AS `updated_at` FROM `modules` WHERE `modules`.`deleted_at` IS NULL AND id_parent = 0; CREATE VIEW v_nfc AS select `nfc`.`id` AS `id`,`nfc`.`id_company` AS `id_company`,`nfc`.`code` AS `code`,`nfc`.`nfc_code` AS `nfc_code`,`nfc`.`active` AS `active`,`nfc`.`deleted_at` AS `deleted_at`,`nfc`.`updated_at` AS `updated_at`,`nfc`.`created_at` AS `created_at` from `nfc` where isnull(`nfc`.`deleted_at`); CREATE VIEW v_objects AS select `obj`.`id` AS `id`,`obj`.`id_ref` AS `id_ref`,`obj`.`id_city` AS `id_city`,`obj`.`id_group` AS `id_group`,`obj`.`name` AS `obj_name`,`obj_place`.`name` AS `place_name`,`obj`.`position` AS `position`,`obj`.`deleted_at` AS `deleted_at`,`obj`.`created_at` AS `created_at`,`obj`.`updated_at` AS `updated_at`,`obj`.`id_parent` AS `id_parent`,`obj`.`lat` AS `lat`,`obj`.`lon` AS `lon`,`obj`.`flag` AS `flag` from (`object` `obj` left join `object_place` `obj_place` on((`obj`.`id_group` = `obj_place`.`id`))) where (isnull(`obj`.`deleted_at`) and isnull(`obj_place`.`deleted_at`)); CREATE VIEW v_worker AS select `worker`.`id` AS `id`,`worker`.`id_ref` AS `id_ref`,`worker`.`id_city` AS `id_city`,`worker`.`id_company` AS `id_company`,`worker`.`id_teamleader` AS `id_teamleader`,`worker`.`nfc_code` AS `nfc_code`,`worker`.`personal_code` AS `personal_code`,`worker`.`first_name` AS `first_name`,`worker`.`last_name` AS `last_name`,`worker`.`address` AS `address`,`worker`.`phone` AS `phone`,`worker`.`birthday` AS `birthday`,`worker`.`deleted_at` AS `deleted_at`,`worker`.`created_at` AS `created_at`,`worker`.`updated_at` AS `updated_at` from `worker` where isnull(`worker`.`deleted_at`); CREATE VIEW v_worker_join AS select `worker`.`id` AS `id`,`worker`.`id_ref` AS `id_ref`,`worker`.`id_city` AS `id_city`,`worker`.`id_company` AS `id_company`,`worker`.`id_teamleader` AS `id_teamleader`,`worker`.`personal_code` AS `personal_code`,`worker`.`first_name` AS `first_name`,`worker`.`last_name` AS `last_name`,`worker`.`id_default` AS `id_default`,`worker`.`address` AS `address`,`worker`.`phone` AS `phone`,`worker`.`birthday` AS `birthday`,`worker`.`deleted_at` AS `deleted_at`,`worker`.`created_at` AS `created_at`,`worker`.`updated_at` AS `updated_at`,`company`.`name` AS `company_name`,`company`.`description` AS `company_description`,`nfc`.`code` AS `nfc_code` from ((`worker` join `company` on((`company`.`id` = `worker`.`id_company`))) left join `nfc` on((`nfc`.`nfc_code` = `worker`.`nfc_code`))) where (isnull(`worker`.`deleted_at`) and isnull(`company`.`deleted_at`) and isnull(`nfc`.`deleted_at`));

BEANSTALK
---------
- start daemon persistent : /usr/bin/beanstalkd -l 0.0.0.0 -p 11300 -b /var/lib/beanstalkd/ &
- upload_tour_data : {"id_city":3, "type":"activity", "data":"sync/winter_activites/2014/02/12/20140212135815_tour_data.txt"}
- media : {"type":2, "rotate":90, "path":"sync/winter_activites/2014/02/12/20140212135815_tour_data.txt"}
- email_extract : id_email
- email_send : id_email

CRON
----
*/1   *   *   *   *   php /home/ps/public_html/api/index.php web/v1/cron/check_cron_status
*/30  *   *   *   *   php /home/ps/public_html/api/index.php web/v1/nfc/generate

WEB ACCOUNT [ username / password / hash / salt ]
-----------
[X] admin / Kloon.vn / e4ca6181bd406da9fb530cb83e477a71 / as#fs^%
[X] freising / bYHE / EDZHcxE6923FjvPx / 2f5aea6d9ce59d68f861cdb09ec71ae9
[X] pfinztal / 4VHW / hA2wKdLv1UGfmQO9 / c9b0bbafbd01a4b78f52b231ecca46fb
[9] Minsch / psps4711
[9] Kisslegg.Anzeige / 12814


MIGRATIONS
----------
* sync
cp -Rvf migrations/local/publicsolution_test/* migrations/local/publicsolution_pfinztal/  && \
cp -Rvf migrations/local/publicsolution_test/* migrations/local/publicsolution_freising/  && \
cp -Rvf migrations/local/publicsolution_test/* migrations/local/publicsolution_kitzingen/ && \
cp -Rvf migrations/local/publicsolution_test/* migrations/local/gemeinde_kissleg/

cp -Rvf migrations/local/publicsolution_test/* migrations/test/publicsolution_pfinztal/  && \
cp -Rvf migrations/local/publicsolution_test/* migrations/test/publicsolution_freising/  && \
cp -Rvf migrations/local/publicsolution_test/* migrations/test/publicsolution_kitzingen/ && \
cp -Rvf migrations/local/publicsolution_test/* migrations/test/gemeinde_kissleg/

cp -Rvf migrations/test/publicsolution_test/* migrations/pro/publicsolution_pfinztal/  && \
cp -Rvf migrations/test/publicsolution_test/* migrations/pro/publicsolution_freising/  && \
cp -Rvf migrations/test/publicsolution_test/* migrations/pro/publicsolution_kitzingen/ && \
cp -Rvf migrations/test/publicsolution_test/* migrations/pro/gemeinde_kissleg/

* global
php migrate db:migrate env=main

* distributed
php migrate db:migrate env=pfinztal           && \
php migrate db:migrate env=test               && \
php migrate db:migrate env=freising           && \
php migrate db:migrate env=kitzingen          && \
php migrate db:migrate env=gemeinde_kissleg

* Rollback
php migrate db:migrate env=pfinztal VERSION=-1          && \
php migrate db:migrate env=test VERSION=-1              && \
php migrate db:migrate env=freising VERSION=-1          && \
php migrate db:migrate env=kitzingen VERSION=-1         && \
php migrate db:migrate env=gemeinde_kissleg VERSION=-1

* init
php migrate db:setup env=pfinztal           && \
php migrate db:setup env=test               && \
php migrate db:setup env=freising           && \
php migrate db:setup env=kitzingen          && \
php migrate db:setup env=gemeinde_kissleg

MYSQL
-----
ps                / jfUeSdfsMFRFbpRU

ps_pro            / hbPWp7AGcn5sasnv / UMJFMWy8Q8qTXrD6
ps_pro_pfin       / cqLWXsdRrf7UxeEC
ps_pro_frei       / WRnPeYfTAaedMZdE
ps_pro_kitz       / 55uHpbVQn4hCNTHn
ps_pro_geme       / MNCDPdPuWGUv4bMA
ps_pro_kloo       / NWDFafBFp5udjWNa
ps_pro_test       / N28tRUqZk6v8KKFQ
ps_pro_demo       / fp2khsNSnRUhBkw2

ps_test           / hbPWp7AGcn5sasnv / UMJFMWy8Q8qTXrD6
ps_test_pfin      / cqLWXsdRrf7UxeEC
ps_test_frei      / WRnPeYfTAaedMZdE
ps_test_kitz      / 55uHpbVQn4hCNTHn
ps_test_geme      / MNCDPdPuWGUv4bMA
ps_test_kloo      / NWDFafBFp5udjWNa
ps_test_test      / N28tRUqZk6v8KKFQ
ps_test_demo      / fp2khsNSnRUhBkw2

ps_dev            / hbPWp7AGcn5sasnv / UMJFMWy8Q8qTXrD6
ps_dev_pfin       / cqLWXsdRrf7UxeEC
ps_dev_frei       / WRnPeYfTAaedMZdE
ps_dev_kitz       / 55uHpbVQn4hCNTHn
ps_dev_geme       / MNCDPdPuWGUv4bMA
ps_dev_kloo       / NWDFafBFp5udjWNa
ps_dev_test       / N28tRUqZk6v8KKFQ
ps_dev_demo       / fp2khsNSnRUhBkw2

ps_local           / hbPWp7AGcn5sasnv / UMJFMWy8Q8qTXrD6
ps_local_pfin      / cqLWXsdRrf7UxeEC
ps_local_frei      / WRnPeYfTAaedMZdE
ps_local_kitz      / 55uHpbVQn4hCNTHn
ps_local_geme      / MNCDPdPuWGUv4bMA
ps_local_kloo      / NWDFafBFp5udjWNa
ps_local_test      / N28tRUqZk6v8KKFQ
ps_local_demo      / fp2khsNSnRUhBkw2
ps_local_neuf      / wNPHfXdqEBJfeewm