<?php
/**
 * A base controller for CodeIgniter with view autoloading, layout support,
 * model loading, helper loading, asides/partials and per-controller 404
 *
 * @link http://github.com/jamierumbelow/codeigniter-base-controller
 * @copyright Copyright (c) 2012, Jamie Rumbelow <http://jamierumbelow.net>
 */

class WEB_Controller extends MX_Controller
{

    /* --------------------------------------------------------------
     * VARIABLES
     * ------------------------------------------------------------ */

    /**
     * The current request's view. Automatically guessed
     * from the name of the controller and action
     */
    protected $view = FALSE;

    /**
     * An array of variables to be passed through to the
     * view, layout and any asides
     */
    protected $data = array();

    /**
     * The name of the layout to wrap around the view.
     */
    protected $layout;

    /**
     * An arbitrary list of asides/partials to be loaded into
     * the layout. The key is the declared name, the value the file
     */
    protected $asides = array();

    /**
     * A list of models to be autoloaded
     */
    protected $models = array();

    /**
     * A formatting string for the model autoloading feature.
     * The percent symbol (%) will be replaced with the model name.
     */
    protected $model_string = '%_model';

    /**
     * A list of helpers to be autoloaded
     */
    protected $helpers = array();

    /**
     * Global language
     * @var string
     */
    protected $global_language;

    /* Benchmark switch */
    protected $benchmark = FALSE;

    /* Current router */
    protected $_modules;
    protected $_controller;
    protected $_method;
    protected $_rout;

    // Languages
    public $_global_language = '';

    // ACL
    public $acl;

    // Assets
    protected $_assets = array();

    // id city
    protected $_city_id = NULL;
    /* --------------------------------------------------------------
     * GENERIC METHODS
     * ------------------------------------------------------------ */

    /**
     * Initialise the controller, tie into the CodeIgniter superobject
     * and try to autoload the models and helpers
     */
    public function __construct(){
        parent::__construct();

        $id_city = intval($this->input->get('id_city'));
        if ($id_city) {
            $id_city = ($id_city <= 0) ? 1 : $id_city;
            $_SESSION['id_city'] = $id_city;
        } else {
            $id_city = isset($_SESSION['id_city']) ? $_SESSION['id_city'] : 1;
        }
        $this->_city_id = $id_city;

        // Global router information
        global $_modules, $_controller, $_method;
        $_modules    = $this->_modules    = $this->router->fetch_module();
        $_controller = $this->_controller = $this->router->class;
        $_method     = $this->_method     = $this->router->method;
        $_rout       = $this->_rout       = "{$this->_modules}/{$this->_controller}/{$this->_method}";

        // Check login
        if (!session_login() AND $this->router->fetch_method() != 'login') {
            redirect('sys/login');
        }

        // Cities
        $this->data['cities'] = $this->db->get_where(TBL_CITY, array('deleted_at' => NULL))->result();

        // Load models
        $this->_load_models();

        // Load helpers
        $this->_load_helpers();
    }

    /* --------------------------------------------------------------
     * VIEW RENDERING
     * ------------------------------------------------------------ */

    /**
     * Override CodeIgniter's despatch mechanism and route the request
     * through to the appropriate action. Support custom 404 methods and
     * autoload the view into the layout.
     */
    public function _remap($method)
    {
        if (method_exists($this, $method))
        {
            call_user_func_array(array($this, $method), array_slice($this->uri->rsegments, 2));
            $this->_load_view();
        }
        else
        {
            if (method_exists($this, '_404'))
            {
                call_user_func_array(array($this, '_404'), array($method));
            }
            else
            {
                show_404(strtolower(get_class($this)).'/'.$method);
            }
        }
    }

    /**
     * Automatically load the view, allowing the developer to override if
     * he or she wishes, otherwise being conventional.
     */
    protected function _load_view()
    {
        /* Benchmark startup */
        $this->output->enable_profiler($this->benchmark);

        // Assets
        $this->data['global_assets'] = $this->load_assets(NULL);

        // ACL object
        global $acl;
        $this->data['acl'] = $acl;

        // If $this->view == FALSE, we don't want to load anything
        if ($this->view !== FALSE)
        {
            // If $this->view isn't empty, load it. If it isn't, try and guess based on the controller and action name
            $view = (!empty($this->view)) ? $this->view : $this->router->directory . $this->router->class . '/' . $this->router->method;

            // Load the view into $yield
            $data['yield'] = $this->load->view($view, $this->data, TRUE);

            // Do we have any asides? Load them.
            if (!empty($this->asides))
            {
                foreach ($this->asides as $name => $file)
                {
                    $data['yield_'.$name] = $this->load->view($file, $this->data, TRUE);
                }
            }

            // Load in our existing data with the asides and view
            $data = array_merge($this->data, $data);
            $layout = FALSE;

            // If we didn't specify the layout, try to guess it
            if (!isset($this->layout))
            {
                if (file_exists(APPPATH . 'modules/'.$this->_modules.'/views/layout.php'))
                {
                    $layout = $this->_modules . '/layout';
                } else if (file_exists(APPPATH . 'modules/'.$this->_modules.'/views/layouts/index.php')) {
                    $layout = $this->_modules . '/layouts/index';
                } else {
                    $layout = 'layouts/application';
                }

                #$layout = $this->router->fetch_module(). '/' .$layout;
            }

            // If we did, use it
            else if ($this->layout !== FALSE)
            {
                $layout = $this->layout;
            }

            // If $layout is FALSE, we're not interested in loading a layout, so output the view directly
            if ($layout == FALSE)
            {
                $this->output->set_output($this->load_assets(NULL) . $data['yield']);
            }

            // Otherwise? Load away :)
            else
            {
                $this->load->view($layout, $data);
            }
        }
    }

    /* --------------------------------------------------------------
     * MODEL LOADING
     * ------------------------------------------------------------ */

    /**
     * Load models based on the $this->models array
     */
    private function _load_models()
    {
        foreach ($this->models as $model)
        {
            $this->load->model($this->_model_name($model), $model);
        }
    }

    /**
     * Returns the loadable model name based on
     * the model formatting string
     */
    protected function _model_name($model)
    {
        return str_replace('%', $model, $this->model_string);
    }

    /* --------------------------------------------------------------
     * HELPER LOADING
     * ------------------------------------------------------------ */

    /**
     * Load helpers based on the $this->helpers array
     */
    private function _load_helpers()
    {
        foreach ($this->helpers as $helper)
        {
            $this->load->helper($helper);
        }
    }

    protected function load_assets($path, $type = NULL, $location = NULL) {
        if ($path == NULL) {
            $return = '';
            if ( is_array($this->_assets) AND (!empty($this->_assets)) ) {
                foreach ($this->_assets as $key => $value) {
                    $return .= $value . PHP_EOL;
                }
            }
            return $return;
        } else {
            switch ($type) {
                case ASSET_KENDO_JS:
                    $location = ASSET_KENDO_JS;
                    $type     = ASSET_JS;
                    break;

                case ASSET:
                case ASSET_JS:
                case ASSET_BOOTSTRAP_JS:
                    $type = ASSET_JS;
                    break;

                case ASSET_KENDO_CSS:
                    $location = ASSET_KENDO_CSS;
                    $type     = ASSET_JS;
                    break;

                case ASSET_CSS:
                case ASSET_FONT:
                case ASSET_BOOTSTRAP_CSS:
                case ASSET_BOOTSTRAP_FONT:
                    $type = ASSET_CSS;
                    break;

                case ASSET_IMG:
                    break;

                default:
                    // code...
                    break;
            }

            if ($location === NULL) {
                $path = $type . $path;
            } else if ($location === FALSE) {
                $path = $path;
            } else {
                $path = $location . $path;
            }

            if ($type == ASSET_JS) {
                $this->_assets[] = '<script type="text/javascript" src="'.base_url($path).'"></script>';
            } else if ($type == ASSET_CSS) {
                $this->_assets[] = '<link rel="stylesheet" href="'.base_url($path).'">';
            } else {}
            return true;
        }
    }

    protected function _kendoui_process($key = NULL, $id_city = NULL, $id_module = NULL, $url = NULL) {
        // Header response as JSON
        header('Content-Type: application/json');

        // Init API server
        $this->rest->initialize(array('server' => config_item('api_web')));

        $api_param = json_decode(file_get_contents('php://input'), true);

        if (empty($api_param)) {
            $api_param = $_REQUEST;
        }

        if (is_null($key) == FALSE) {
            $api_param['key'] = $key;
        }

        if ( is_null($url) ) {
            $url = config_item('kendoui').'?id_city=' . (isset($id_city) ? $id_city : $this->_city_id);
        }
        $api_result = $this->rest->post($url, $api_param, 'json');
        $api_status = $this->rest->status();
        echo json_encode($api_result);
    }
}
