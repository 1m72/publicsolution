<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Email model
 *
 * @author Chien Tran <tran.duc.chien@kloon.vn>
 */
class Email_model extends MY_Model{
    /**
     * Send email
     * @param  array $email_data
     *     from_email       : Required
     *     from_name        : Optional
     *     to_email         : Required
     *     to_name          : Optional
     *     reply_to         : Optional
     *     cc               : Email with CC (ex1@company.com,ex2@company.com,...)
     *     bcc              : Email with BCC (ex1@company.com,ex2@company.com,...)
     *     subject          : Optional
     *     message          : Optional
     *     template         : Optional
     *     template_pattern : Attach with template param
     *     template_replace : Attach with template param
     *
     * @return boolean Return status of send_mail
     */
    function send_mail($email_data) {
        # Required
        $requires = array('from_email', 'to_email');
        $email_data_key = array_keys($email_data);
        if (array_intersect($email_data_key, $requires) != $requires) {
            return false;
        }

        # CI instance
        $CI =& get_instance();

        # Email info
        $from_email = isset($email_data['from_email']) ? $email_data['from_email'] : '';
        $from_name  = isset($email_data['from_name']) ? $email_data['from_name'] : $from_email;
        $to_email   = isset($email_data['to_email']) ? $email_data['to_email'] : '';
        $to_name    = isset($email_data['to_name']) ? $email_data['to_name'] : $to_email;

        # Reply_to, CC, BC
        $reply_to   = isset($email_data['reply_to']) ? $email_data['reply_to'] : '';
        $cc         = isset($email_data['cc']) ? $email_data['cc'] : '';
        $bcc        = isset($email_data['bcc']) ? $email_data['bcc'] : '';

        # Email contain
        $subject    = isset($email_data['subject']) ? $email_data['subject'] : '';
        $message    = isset($email_data['message']) ? $email_data['message'] : '';

        # Email template
        if (isset($email_data['template'])) {
            $CI->load->config('email_template');
            $template = config_item($email_data['template']);
            if ( $template AND !empty($template) ) {
                $template_pattern = isset($email_data['template_pattern']) AND is_array($email_data['template_pattern']) ? $email_data['template_pattern'] : array();
                $template_replace = isset($email_data['template_replace']) AND is_array($email_data['template_replace']) ? $email_data['template_replace'] : array();
                $message          = preg_replace($template_pattern, $template_replace, $template);
            }
        }

        # Insert to email
        $email = array(
            'from_email' => $from_email,
            'from_name'  => $from_name,
            'to_email'   => $to_email,
            'to_name'    => $to_name,
            'message'    => $message,
            'created_at' => date('Y-m-d H:i:s')
        );
        $db_flag = $CI->db->insert(TBL_EMAIL, $email);

        /*if ($db_flag) {
            $CI->load->library('email');
            $CI->email->from($from_email, $from_name);
            $CI->email->to($to_email, $to_name);
            if (!empty($reply_to)) {
                $CI->email->reply_to($reply_to);
            }
            if (!empty($cc)) {
                $CI->email->cc($cc);
            }
            if (!empty($bcc)) {
                $CI->email->bcc($bcc);
            }
            $CI->email->subject($subject);
            $CI->email->message($message);
            $result = $CI->email->send();
        }*/
    }
}
?>