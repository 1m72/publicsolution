<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE' , 0644);
define('FILE_WRITE_MODE' , 0666);
define('DIR_READ_MODE' , 0755);
define('DIR_WRITE_MODE' , 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',                          'rb');
define('FOPEN_READ_WRITE',                    'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',      'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',                  'ab');
define('FOPEN_READ_WRITE_CREATE',             'a+b');
define('FOPEN_WRITE_CREATE_STRICT',           'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',      'x+b');


# DB Tables
define('TBL_3RD_API_LOGS'           , '3rd_api_logs');
define('TBL_ACTIVITIES'             , 'activities');
define('TBL_ADDON'                  , 'addon');
define('TBL_ADDON_MODULE'           , 'addon_module');
define('TBL_ADDON_MODULE_ACTIVITI'  , 'addon_module_activity');
define('TBL_API_KEYS'               , 'api_keys');
define('TBL_API_LOGS'               , 'api_logs');
define('TBL_CITY'                   , 'city');
define('TBL_CITY_CONFIG'            , 'city_config');
define('TBL_COMPANY'                , 'company');
define('TBL_COST_CENTER'            , 'cost_center');
define('TBL_CRON'                   , 'cron');
define('TBL_CRON_PUS_JOBS'          , 'cron_push_jobs');
define('TBL_DEVICES'                , 'devices');
define('TBL_EMAIL'                  , 'email');
define('TBL_EMAIL_SAVE'             , 'email');
define('TBL_EMPLOYEE_ACTIVITY'      , 'employee_activity');
define('TBL_EMPLOYEE_GROUP'         , 'employee_group');
define('TBL_EMPLOYEE_GROUP_COST'    , 'employee_group_cost');
define('TBL_EXTRACT_TOURS'          , 'extract_tours');
define('TBL_FASTTASK'               , 'fasttask');
define('TBL_FREETEXT'               , 'freetext');
define('TBL_GROUP'                  , 'group');
define('TBL_GROUP_ACTIVITY'         , 'group_activity');
define('TBL_HOLIDAY'                , 'holiday');
define('TBL_LOGS'                   , 'api_logs');
define('TBL_MACHINE'                , 'machine');
define('TBL_MACHINE_ACTIVITY'       , 'machine_activity');
define('TBL_MACHINE_ADD_ON'         , 'addon');
define('TBL_MACHINE_ADDON_MODULE'   , 'machine_addon_module');
define('TBL_MACHINE_MATERIAL'       , 'machine_material');
define('TBL_MACHINE_MATERIAL_USED'  , 'machine_material_used');
define('TBL_MACHINE_MODULE'         , 'machine_module');
define('TBL_MACHINE_MODULE_ACTIVITI', 'machine_module_activity');
define('TBL_MATERIAL'               , 'material');
define('TBL_MIGRATIONS'             , 'migrations');
define('TBL_MODULES'                , 'modules');
define('TBL_NFC'                    , 'nfc');
define('TBL_OBJECT'                 , 'object');
define('TBL_OBJECT_PLACE'           , 'object_place');
define('TBL_POST_SERVICE_LOG'       , 'post_service_logs');
define('TBL_TMP'                    , 'tmp');
define('TBL_USER_GROUP'             , 'user_group');
define('TBL_USERS'                  , 'users');
define('TBL_WORKER'                 , 'worker');
define('TBL_WORKER_ACTIVITY'        , 'worker_activity');
define('TBL_WORKER_ACTIVITY_SAVED'  , 'worker_activity_saved');
define('TBL_WORKER_ACTIVITY_SPLIT'  , 'worker_activity_split');
define('TBL_WORKER_GPS'             , 'worker_gps');
define('TBL_WORKER_GPS_SPLIT'       , 'worker_gps_split');
define('TBL_WORKER_GROUP'           , 'worker_group');
define('TBL_WORKER_INFORMATION'     , 'worker_information');
define('TBL_WORKER_MATERIAL'        , 'machine_material_used');
define('TBL_WORKER_MODULES'         , 'worker_module');
define('TBL_WORKTIME'               , 'worktime');
define('TBL_WORKTIME_DETAIL'        , 'worktime_detail');

# DB Views
define('VIEW_USERS'                 , 'v_users');
define('VIEW_USERS_GROUP'           , 'v_user_group');
define('VIEW_CITY'                  , 'v_city');
define('VIEW_CITY_CONFIG'           , 'v_city_config');
define('VIEW_COMPANY'               , 'v_company');
define('VIEW_DEVICES'               , 'v_devices');
define('VIEW_MACHINE'               , 'v_machine');
define('VIEW_MACHINE_JOIN'          , 'v_machine_join');
define('VIEW_MACHINE_ADDON'         , 'v_addon');
define('VIEW_ADDON'                 , 'v_addon');
define('VIEW_MATERIAL'              , 'v_material');
define('VIEW_NFC'                   , 'v_nfc');
define('VIEW_WORKER'                , 'v_worker');
define('VIEW_OBJECT'                , 'v_objects');
define('VIEW_WORKER_JOIN'           , 'v_worker_join');
define('VIEW_ACTIVITY'              , 'v_activity');
define('VIEW_POSITION'              , 'v_position');
define('VIEW_MODULE'                , 'v_modules');
define('VIEW_MACHINE_MODULE'        , 'v_machine_module');
define('VIEW_ADDON_MODULE'          , 'v_addon_module');
define('VIEW_WORKER_GPS'            , 'v_worker_gps');
define('VIEW_WORKER_MODULE'         , 'v_worker_module');
define('VIEW_GROUP'                 , 'v_group');

# APP Type
define('APP_WINTER'                 , 0);
define('APP_FASTTASK'               , 1);
define('APP_WINTER_TEXT'            , 'winterdienst');
define('APP_FASTTASK_TEXT'          , 'fasttask');

# API Status
define('STATUS_SUCCESS'             , 'ok');
define('STATUS_FAIL'                , 'fail');
define('REST_CODE_OK'               , 200);
define('REST_CODE_PARAM_ERR'        , 412);
define('REST_CODE_SERVER_ERR'       , 501);

# Information type
define('INFO_MESSAGE'               , 1);
define('INFO_VOICE'                 , 2);
define('INFO_IMAGE'                 , 3);
define('INFO_VIDEO'                 , 4);
define('INFO_MESSAGE_TEXTMODULE'    , 5);

# Information status
define('INFO_STATUS_PENDING'       , 'pending');
define('INFO_STATUS_SUCCESS'       , 'success');
define('INFO_STATUS_ERROR'         , 'error');

# NFC status
define('NFC_UNACTIVE'               , 0);
define('NFC_ACTIVE'                 , 1);

# Email status
define('EMAIL_STATUS_WAITING'       , 'waiting');
define('EMAIL_STATUS_PENDING'       , 'pending');
define('EMAIL_STATUS_SUCCESS'       , 'success');
define('EMAIL_STATUS_ERROR'         , 'error');

# Cron
define('CRON_TYPE_ACTIVITY'         , 'activity');
define('CRON_TYPE_INFORMATION'      , 'information');
define('CRON_TYPE_CONVERT_VOICE'    , 'voice');
define('CRON_TYPE_ROTATE'           , 'rotate');
define('CRON_TYPE_OBJECT_LOCATION'  , 'object_locations');
define('CRON_STATUS_PENDING'        , 'pending');
define('CRON_STATUS_SUCCESS'        , 'success');
define('CRON_STATUS_ERROR'          , 'error');

# Mobile
define('GOOGLE_MESSAGE'             , getenv('GCM_MESSAGE_TEST'));
define('GOOGLE_URL'                 , getenv('GCM_URL'));
define('GOOGLE_API_KEY'             , getenv('GCM_KEY_WINTERDIENTS'));

# Queue
define('QUE_EMAIL_EXTRACT'          , getenv('QUE_EMAIL_EXTRACT'));
define('QUE_EMAIL_SEND'             , getenv('QUE_EMAIL_SEND'));
define('QUE_SYNC'                   , getenv('QUE_SYNC'));
define('QUE_MEDIA'                  , getenv('QUE_MEDIA'));
define('QUE_RESOLVE_STREET_NAME'    , getenv('QUE_RESOLVE_STREET_NAME'));
define('QUE_SYNC_2'                 , getenv('QUE_SYNC_2'));
define('QUE_CLEAN_POINT'            , getenv('QUE_CLEAN_POINT'));
define('QUE_SERVICE_POST'           , getenv('QUE_SERVICE_POST'));
define('QUE_SERVER_UPDATE'          , getenv('QUE_SERVER_UPDATE'));
define('QUE_OBJECT_LOCATION'        , getenv('QUE_OBJECT_LOCATION'));


# Memcached
define('MC_CITY_CONFIG'             , getenv('MC_CITY_CONFIG'));
define('MC_CRON_PUSH_JOB'           , getenv('MC_CRON_PUSH_JOB'));
define('MC_GPS_ACTIVITI'            , getenv('MC_GPS_ACTIVITI'));

#Straßenkontrolle
define('MODULE_STREETCHECKING_ID'   , 3);

/* End of file constants.php */
/* Location: ./application/config/constants.php */