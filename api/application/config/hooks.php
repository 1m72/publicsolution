<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$hook['pre_system'][] = array(
    'class'    => 'PHPFatalError',
    'function' => 'setHandler',
    'filename' => 'PHPFatalError.php',
    'filepath' => 'hooks'
);
