<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$active_group = 'default';
$active_record = TRUE;

#--- Main Template
$db['default']['hostname'] = getenv('DB_HOSTNAME');
$db['default']['username'] = getenv('DB_USERNAME');
$db['default']['password'] = getenv('DB_PASSWORD');
$db['default']['database'] = getenv('DB_DATABASE');
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['dbdriver'] = getenv('DB_DRIVER');
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

#--- Database Template
$db['db_template']['hostname'] = getenv('DB_TEMPLATE_HOSTNAME');
$db['db_template']['username'] = getenv('DB_TEMPLATE_USERNAME');
$db['db_template']['password'] = getenv('DB_TEMPLATE_PASSWORD');
$db['db_template']['database'] = getenv('DB_TEMPLATE_DATABASE');
$db['db_template']['dbprefix'] = '';
$db['db_template']['pconnect'] = TRUE;
$db['db_template']['dbdriver'] = 'mysql';
$db['db_template']['db_debug'] = TRUE;
$db['db_template']['cache_on'] = FALSE;
$db['db_template']['cachedir'] = '';
$db['db_template']['char_set'] = 'utf8';
$db['db_template']['dbcollat'] = 'utf8_general_ci';
$db['db_template']['swap_pre'] = '';
$db['db_template']['autoinit'] = FALSE;
$db['db_template']['stricton'] = FALSE;

/* End of file database.php */
/* Location: ./application/config/database.php */