<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config['email_template_forgot_request'] = array(
    'subject' => 'Forgot password - Public Solution',
    'message' => 'Dear [USER] ! <br />This is your link forgot password : [LINK]',
);

$config['email_template_forgot_success'] = array(
    'subject' => 'New password - Public Solution',
    'message' => 'Dear [USER] !<br /> This is your new password : [NEW_PASSWORD]',
);

$config['email_template_sync_notification'] = array(
    'subject' => 'Meldung/Information aus [MODULE_NAME]',
    'message' => '
        <table cellpadding="5" cellspacing="0" border="0" width="100%">
            <tr>
                <td width="130">Datum</td>
                <td>: <strong>[TIME]</strong></td>
            </tr>
            <tr>
                <td width="130">Mitarbeiter</td>
                <td>: <strong>[WORKER]</strong></td>
            </tr>
            <tr>
                <td width="130">Straße</td>
                <td>: <strong>[STREETNAME]</strong></td>
            </tr>
            <tr>
                <td width="130">Text</td>
                <td>: <strong>[INFO_MSG]</strong></td>
            </tr>
            <tr>
                <td width="130">GPS-Koordinate</td>
                <td>: <strong>[GPS_COONDINATE]</strong> [GPS_COONDINATE_LINK]</td>
            </tr>
        </table>
        <div style="margin-top: 10px; padding-top: 10px; border-top: 1px dashed #DDD; font-size: 12px; font-style: italic; color: #9C9C9C;">Dies ist ein E-Mail-Benachrichtigung. Bitte antworten Sie nicht auf diese E-Mail. Danke!</div>
    ',
);