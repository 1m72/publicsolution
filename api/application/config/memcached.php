 <?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
    'default' => array(
        'host'      => '127.0.0.1',
        'port'      => 11211,
        'weight'    => 1
    )
);