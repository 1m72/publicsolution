<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Mobile - Auth
$route['validate_code']               = "mobile/v1/auth/validate_AccessCode";
$route['login']                       = "mobile/v1/auth/login";

// Mobile - Winterdienst
$route['settings']                    = "mobile/v1/city/setting";
$route['initialize_machine']          = "mobile/v2/machine/initialize";
$route['initialize_employee']         = "mobile/v2/employee/initialize_employee";
$route['initialize_activity']         = "mobile/v1/activity/initialize_activity";
$route['initialize_material']         = "mobile/v1/material/initialize_material";
$route['update_winter_service_task']  = "mobile/v2/synchronize/update_winter_service_task";
$route['upload_tour_data']            = "mobile/v2/synchronize/upload_tour_data";
$route['upload_image_record']         = "mobile/v2/synchronize/upload_image_record";
$route['upload_tour_data_complete']   = "mobile/v2/synchronize/upload_tour_data_complete";
$route['synchronize_employee']        = "mobile/v2/employee/synchronize_employee";
$route['synchronize_machine']         = "mobile/v2/machine/synchronize_machine";
$route['synchronize_activity']        = "mobile/v1/activity/synchronize_activity";
$route['synchronize_material']        = "mobile/v1/material/synchronize_material";
$route['register_push_nofitication']  = "mobile/v1/devices/register_push_nofitication";
$route['push_winter_service_data2']   = "mobile/v1/devices/push_winter_service_data";
$route['register_push_nofitication2'] = "mobile/v1/register_push/register_push_nofitication";
$route['push_winter_service_data']    = "mobile/v1/push_winter_service/push_winter_service_data";

// Mobile - Fasttask
$route['create_fast_task_xml']        = "mobile/v1/fasttask_create_fast_task/create_fast_task";
$route['initialize_object']           = "mobile/v1/fasttask_object/object_list";
$route['initialize_place']            = "mobile/v1/fasttask_object/object_group";
$route['object_group']                = "mobile/v1/fasttask_object/object_group"; // initialize_place
$route['object_list']                 = "mobile/v1/fasttask_object/object_list"; // initialize_object
$route['superiors_list']              = "mobile/v1/fasttask/superiors_list";
$route['synchronize_object']          = "mobile/v1/fasttask_object/synchronize_object_location";
$route['synchronize_place']           = "mobile/v1/fasttask_object/synchronize_place";
$route['text_modules']                = "mobile/v1/fasttask/text_modules";
$route['upload_fast_task_binary']     = "mobile/v1/fasttask_create_fast_task/upload_fast_task";
$route['upload_objects_locations']    = "mobile/v1/fasttask/upload_objects_locations";
$route['employee_group']              = "mobile/v1/fasttask/employee_group";
$route['cost_center']                 = "mobile/v1/fasttask/cost_center";

// Disabled
// $route['assignment']                = "mobile/v1/fasttask/assignment"; // Removed
// $route['object_place']              = "mobile/v1/fasttask_object/synchronize_place"; // synchronize_place
// $route['initialize_group_objects']  = "mobile/v1/fasttask_group_objects/initialize_group_objects";
// $route['synchronize_group_objects'] = "mobile/v1/fasttask_group_objects/synchronize_group_objects";

# Web
$route['web/v2/(:any)']               = "web/v2/$1";
$route['web/v1/(:any)']               = "web/v1/$1";
$route['web/(:any)']                  = "web/v1/$1";

# Default
$route['default_controller']          = "dev/home/index";
$route['404_override']                = 'dev/home/error_404';

/* End of file routes.php */
/* Location: ./application/config/routes.php */