<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config['database_dir']  = getenv('DATABASE_DIR');
$config['webapp_link']   = getenv('WEBAPP_LINK');
$config['media_dir']     = getenv('MEDIA_DIR');
$config['images_link']   = getenv('IMAGES_LINK');

# Email
$config['email_name']    = 'BIS-OFFICE Mail System';
$config['email_account'] = array(
    'noreply' => array(
        'username'     => getenv('EMAIL_NOREPLY_USERNAME'),
        'email'        => getenv('EMAIL_NOREPLY_EMAIL'),
        'display_name' => getenv('EMAIL_NOREPLY_DISPLAY_NAME'),
    ),
);

# Database
$config['mysql_root'] = array(
    'hostname' => getenv('MYSQL_ROOT_HOSTNAME'),
    'username' => getenv('MYSQL_ROOT_USERNAME'),
    'password' => getenv('MYSQL_ROOT_PASSWORD'),
    'database' => getenv('MYSQL_ROOT_DATABASE'),
    'port'     => 3306,
    'prefix'   => '',
    'dbdriver' => "mysqli",
    'pconnect' => TRUE,
    'db_debug' => FALSE,
    'cache_on' => TRUE,
    'cachedir' => "",
    'char_set' => "utf8",
    'dbcollat' => "utf8_general_ci",
);

$config['encryption_key']             = getenv('APP_KEY');

$config['api_version']                = '1.0';
$config['upload_dir']                 = '../data/uploads/';
$config['upload_allowed']             = '*'; #'gif|jpg|jpeg|png|mp3|wma|wav|3gp|txt|mp4|json';

$config['sync_dir']                   = getenv('SYNC_DIR');
$config['sync_winter_dir']            = getenv('SYNC_WINTER_DIR');
$config['sync_winter_activities_dir'] = getenv('SYNC_WINTER_ACTIVITIES_DIR');
$config['sync_fasttask_dir']          = getenv('SYNC_FASTTASK_DIR');
$config['sync_object_location_dir']   = getenv('SYNC_OBJECT_LOCATION_DIR');

// Default DB Param
$config['db_prefix']                  = '';
$config['db_port']                    = 3306;
$config['db_dbdriver']                = "mysql";
$config['db_pconnect']                = TRUE;
$config['db_db_debug']                = FALSE;
$config['db_cache_on']                = TRUE;
$config['db_cachedir']                = "";
$config['db_char_set']                = "utf8";
$config['db_dbcollat']                = "utf8_general_ci";

// API - Mobile
$config['mobile_format']              = getenv('REST_FORMAT_MOBILE');

// API - Web
$config['web_format']                 = getenv('REST_FORMAT_WEB');

// Synchronize - ignore city
$config['ignore_resolve_street'] = array(1);

$config['resolve_services'] = array(
    'kloon'         => getenv('OSM_KLOON'),
    'mapquest'      => getenv('OSM_MAPQUEST'),
    'openstreetmap' => getenv('OSM_OPENSTREETMAP'),
    'gisgraphy'     => getenv('OSM_GISGRAPHY'),
);

$config['email_admin'] = array('tran.duc.chien@kloon.vn');