<?php
class MY_Encrypt extends CI_Encrypt {
    /**
     * Encode
     *
     * Encodes the message string using bitwise XOR encoding.
     * The key is combined with a random hash, and then it
     * too gets converted using XOR. The whole thing is then run
     * through mcrypt (if supported) using the randomized key.
     * The end result is a double-encrypted message string
     * that is randomized with each call to this function,
     * even if the supplied message and key are the same.
     *
     * @access    public
     * @param    string    the string to encode
     * @param    string    the key
     * @return    string
     */
    function encode($string, $key = '')
    {
        $key = $this->get_key($key);

        if (($this->_mcrypt_exists === TRUE) && FALSE)
        {

            $enc = $this->mcrypt_encode($string, $key);
        }
        else
        {

            $enc = $this->_xor_encode($string, $key);
        }

        return base64_encode($enc);
    }

    // --------------------------------------------------------------------

    /**
     * Decode
     *
     * Reverses the above process
     *
     * @access    public
     * @param    string
     * @param    string
     * @return    string
     */
    function decode($string, $key = '')
    {
        $key = $this->get_key($key);

        if (preg_match('/[^a-zA-Z0-9\/\+=]/', $string))
        {
            return FALSE;
        }

        $dec = base64_decode($string);

        if (($this->_mcrypt_exists === TRUE) && FALSE)
        {
            if (($dec = $this->mcrypt_decode($dec, $key)) === FALSE)
            {
                return FALSE;
            }
        }
        else
        {
            $dec = $this->_xor_decode($dec, $key);
        }

        return $dec;
    }

}