<?php defined('BASEPATH') OR exit('No direct script access allowed');
abstract class Uploadfile extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
	}
	
	function getExtension($str)
	{
		$i = strrpos($str,".");
		if (!$i) { return ""; }
	
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}
	
	protected $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
		
	protected function upload($type = 'local',$server='',$username='',$password='',$path='',$file)
	{
		switch ($type){
			case 'local':
				$tmp_name = $file["tmp_name"];
				$filename = $file["name"];
				$ext = $this->getExtension($filename);
				if(strlen($filename) > 0)
				{
					if(in_array($ext,$this->valid_formats))
					{
						move_uploaded_file($tmp_name, $path.$filename) or die("Unable to upload");
						echo"File successfully uploaded";
					}
				}
				break;
			case 'ftp':
				$filename = $file['name'];
				$tmp = $file['tmp_name'];
				$ext = $this->getExtension($filename);
				if(strlen($filename) > 0)
				{
					if(in_array($ext,$this->valid_formats))
					{
						// set up basic connection
						$connect = ftp_connect($server) or die("Unable to connect to host");
						// login with username and password
						$login_result = ftp_login($connect, $username, $password) or die("Authorization Failed");
						// upload a file				
						if(!$filename)
							echo"Please select a file";
						else
						{
							ftp_put($connect,$path.$filename,$tmp,FTP_BINARY) or die("Unable to upload");
							echo"File successfully uploaded to FTP";
						}
						// close the connection
						ftp_close($connect);
					}
				}
				break;
			case 's3':
				$filename = $file['name'];
				$tmp = $file['tmp_name'];
				$ext = $this->getExtension($filename);
				if(strlen($filename) > 0)
				{
					if(in_array($ext,$this->valid_formats))
					{
						include('Storage/Drivers/S3.php');
						//Rename image name.
						$actual_image_name = time().".".$ext;
						if($s3->putObjectFile($tmp, $bucket , $actual_image_name, S3::ACL_PUBLIC_READ_WRITE) )
						{
							$msg = "S3 Upload Successful.";
							$s3file='http://'.$bucket.'.s3.amazonaws.com/'.$actual_image_name;
						}
						else
							$msg = "S3 Upload Fail.";
					}
					else
						$msg = "Invalid file, please upload image file.";				
				}
				else
					$msg = "Please select image file.";
				break;
		}
		return true;
	}
}

?>