<?php
    /**
     * SFTP storage driver
     *
     * @author  : chientran <tran.duc.chien@kloon.vn>
     * @created : 29 Oct 2013
     */
    class Storage_sftp {
    	private $_ci;
    	private $_result;
    	private $_message;
    	 
    	private $hostname;
    	private $username;
    	private $password;
    	private $port;
    	private $debug;
    	
    	function __construct($config = array())
    	{
    		$this->_ci = get_instance();
    		$this->_ci->load->library('sftp');
    		
    		foreach ($config as $key => $val)
    		{
    			if (isset($this->$key))
    			{
    				$this->$key = $val;
    			}
    		}
    	}

    	function connect()
    	{
    		$config['hostname'] = $this->hostname;
    		$config['username'] = $this->username;
    		$config['password'] = $this->password;
    		return $this->_ci->sftp->connect($config);
    	}
    	
        function change_dir() {
        }

        function list_dir() {
        }

        function make_dir() {
        }

        function remove_dir() {
        }

        function remove_file() {
        }

        function move() {
        }

        function rename() {
        }

        function upload($field_name = null) {
        	if(isset($field_name) && !empty($field_name))
        	{
        		$filename = $field_name['name'];
        		$filetmp = $field_name['tmp_name'];
        		
        		if($this->connect())
        		{
        			if($this->_ci->sftp->upload($filetmp,$filename))
        				$this->_result = true;
        			else
        				$this->_result = false;
        		}
        		else
        			$this->_result = false;
        	}
        	else
        		$this->_result = false;
        	return $this->_result;
        }
        
        public function get_message() {
        	return $this->_message;
        }
    }
?>