<?php
    /**
     * Amazone S3 storage driver
     *
     * @author  : chientran <tran.duc.chien@kloon.vn>
     * @created : 29 Oct 2013
     */
    class Storage_s3 {

        function change_dir() {
        }

        function list_dir() {
        }

        function make_dir() {
        }

        function remove_dir() {
        }

        function remove_file() {
        }

        function move() {
        }

        function rename() {
        }

        function upload() {
        }
    }
?>