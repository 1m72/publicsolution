<?php
    /**
     * Local storage driver
     *
     * @author  : chientran <tran.duc.chien@kloon.vn>
     * @created : 29 Oct 2013
     */
    class Storage_local extends Storage_abstract {
        private $_ci;
        private $_result;
        private $_message;

        public function __construct() {
            $this->_ci = get_instance();
            $this->_ci->load->library('upload');
        }

        public function connect() {
            return true;
        }

        public function upload($field_name) {
            $config['upload_path']   = $_SERVER['DOCUMENT_ROOT'].'/uploads/';
            $config['allowed_types'] = '*';
            $this->_ci->load->initialize($config);

            if ( ! $this->_ci->upload->do_upload($field_name)) {
                $this->_result = false;
                $this->_message = $this->_ci->upload->display_errors();
                return false;
            } else {
                $this->_result = true;
                $this->_message = $this->_ci->upload->data();
                return true;
            }
        }

        public function get_message() {
            return $this->_message;
        }

        function change_dir() {
        }

        function list_dir() {
        }

        function make_dir() {
        }

        function remove_dir() {
        }

        function remove_file() {
        }

        function move() {
        }

        function rename() {
        }
    }
?>