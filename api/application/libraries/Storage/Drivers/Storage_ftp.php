<?php
    /**
     * FTP storage driver
     *
     * @author  : chientran <tran.duc.chien@kloon.vn>
     * @created : 29 Oct 2013
     */
    class Storage_ftp extends Storage_abstract {
    	private $_ci;
    	private $_result;
    	private $_message;
    	
    	private $hostname;
    	private $username;
    	private $password;
    	private $port;
    	private $debug;
    	/*
    	 * @author  : huongpm <phung.manh.huong@kloon.vn>
    	* @created : 30 Oct 2013
    	*/
    	function __construct($config = array())
    	{
    		$this->_ci = get_instance();
    		$this->_ci->load->library('ftp');    		
    		$this->hostname = $config['hostname'];    		
    		$this->username = $config['username'];    		
    		$this->password = $config['password'];    		
    		$this->port = $config['port'];    		
    		$this->debug = $config['debug'];
    	}
    	/*
    	 * @author  : huongpm <phung.manh.huong@kloon.vn>
    	 * @created : 30 Oct 2013
    	*/
    	function connect()
    	{
    		$config['hostname'] = $this->hostname;
        	$config['username'] = $this->username;
        	$config['password'] = $this->password;
        	$config['debug'] = true;        	
        	return $this->_ci->ftp->connect($config);
    	}

        function change_dir() {
        }

        function list_dir() {
        }

        function make_dir() {
        }

        function remove_dir() {
        }
        /*
         * @author  : huongpm <phung.manh.huong@kloon.vn>
         * @created : 30 Oct 2013
        */
        public function remove_file($file_name) {
        	if(isset($file_name) && !empty($file_name))
        	{
        		if($this->connect())
        		{
	        		if($this->_ci->ftp->delete_file($file_name))
	        			$this->_result = true;
	        		else
	        			$this->_result = false;
	        		
	        		$this->_ci->ftp->close();
        		}
        		else 
        			$this->_result = false;
        	}
        	else 
        		$this->_result = false;
        	return $this->_result;
        }

        function move() {
        }
        /*
         * @author  : huongpm <phung.manh.huong@kloon.vn>
         * @created : 30 Oct 2013
        */
        public function rename($oldname = '',$newname = '') {
        	if(isset($oldname) && !empty($oldname) && isset($newname) && !empty($newname))
        	{
        		if($this->connect())
        		{
	        		if($this->_ci->ftp->rename($oldname,$newname,false))        			
	        			$this->_result = true;
	        		else
	        			$this->_result = false;
	        	
	        		$this->_ci->ftp->close();
        		}
        		else 
        			$this->_result = false;
        	}
        	else
        		$this->_result = false;
        	return $this->_result;
        }
        /*
         * @author  : huongpm <phung.manh.huong@kloon.vn>
         * @created : 30 Oct 2013
        */
        public function upload($field_name = null) {
        	if(isset($field_name) && !empty($field_name))
        	{
	        	$filename = $field_name['name'];
	        	$filetmp = $field_name['tmp_name']; 
	        	
	        	if($this->connect())
        		{	        	
		        	if($this->_ci->ftp->upload($filetmp,$filename, 'ftp_binary'))
		        		$this->_result = true;
		        	else 
		        		$this->_result = false;
		        	
		        	$this->_ci->ftp->close();
        		}
        		else 
        			$this->_result = false;
        	}
        	else 
        		$this->_result = false;
        	return $this->_result;
        }
        
    	public function get_message() {
            return $this->_message;
        }
    }
?>