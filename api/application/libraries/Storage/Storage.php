<?php
/**
 * Base storage
 *
 * @author  : Chien Tran <tran.duc.chien@kloon.vn>
 * @created : 28 Oct 2013
 */

require('Storage_abstract.php');
define('STORAGE_PREFIX', 'storage_');
class Storage extends CI_Controller {
    protected $valid_drivers;
    protected $server_type;
    protected $hostname;
    protected $username;
    protected $password;
    protected $port;
    protected $passive;
    protected $debug;
    protected $_ci;
    protected $_config;

    public function __construct() {
        # Get CI Instance
        $this->_ci =& get_instance();

        # Get storage config
        $this->_ci->load->config('storage', true);
        $this->_config = $this->_ci->config->item('storage');

        # Init valid driver by config
        $this->valid_drivers = $this->_config['storage_drivers'];
    }

    public function init($config = array()) {
        $config = is_array($config) ? $config : (array) $config;

        # Hostname
        if (isset($config['hostname'])) {
            $this->hostname = $config['hostname'];
        }

        # Username
        if (isset($config['username'])) {
            $this->username = $config['username'];
        }

        # Password
        if (isset($config['password'])) {
            $this->password = $config['password'];
        }

        # Server type
        $this->server_type = isset($config['server_type']) ? $config['server_type'] : NULL;

        # Load storage driver
        if ( !is_null($this->server_type) AND in_array($this->server_type, $this->valid_drivers) ) {
            log_message('debug', 'Storage driver '.$this->server_type.' has been loaded');
            $cf = array(
            	'hostname'	=> $this->hostname,
            	'username'	=> $this->username,
            	'password'	=> $this->password,
            	'port' 		=>$this->port,
            	'debug' 	=>$this->debug
            );
            $this->_ci->load->library('Storage/Drivers/storage_'.$this->server_type,$cf);
        } else {
            die('Storage driver is invalid.');
        }

        # Connect
        #$this->_ci->{'storage_'.$this->server_type}->connect();
    }

    public function __call($name, $param) {
        $flag = true;
        switch ($name) {
            case 'change_dir':
            case 'list_dir':
            case 'make_dir':
            case 'remove_dir':
            case 'remove_file':
            	$field_name = $param[0];
            	$result = $this->_ci->{STORAGE_PREFIX.$this->server_type}->remove_file($field_name);
            	print_r($this->_ci->{STORAGE_PREFIX.$this->server_type}->get_message());
            	var_dump($result);
            	break;
            case 'move':
            case 'rename':
            	$oldname = $param[0];
            	$newname = $param[0];
            	$result = $this->_ci->{STORAGE_PREFIX.$this->server_type}->rename($oldname,$newname);
            	print_r($this->_ci->{STORAGE_PREFIX.$this->server_type}->get_message());
            	var_dump($result);
            	break;
            case 'upload':
                $field_name = $param[0];
                $result = $this->_ci->{STORAGE_PREFIX.$this->server_type}->upload($field_name);
                print_r($this->_ci->{STORAGE_PREFIX.$this->server_type}->get_message());
                var_dump($result);
                break;

            default:
                $flag = false;
                break;
        }
    }
}
?>