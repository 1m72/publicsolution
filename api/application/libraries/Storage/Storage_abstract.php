<?php
    /**
     * Storage abstract
     *
     * @author  : chientran <tran.duc.chien@kloon.vn>
     * @created : 29 Oct 2013
     */
    abstract class Storage_abstract {
        abstract public function change_dir();
        abstract public function list_dir();
        abstract public function make_dir();
        abstract public function remove_dir();
        abstract public function remove_file($field_name);
        abstract public function move();
        abstract public function rename($oldname, $newname);
        abstract public function upload($field_name);
    }
?>