<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Worker process media file (convert audio, video, rotate image, ...)
 *
 * @author tran.duc.chien@kloon.vn
 * @date   18 Feb 2014
 */
class Media extends MX_Controller {

    /* Constructor */
    function __construct(){
        parent::__construct();
    }

    /**
     * Queue process media (video, audio, image, ...)
     * Using json_encode to push job.
     * Required :
     *     path : real path to file
     *     type : audio, video, image (required more param "rotate"), ...
     * @return [type] [description]
     */
    function index() {
        sleep(5);

        # Pheanstalk lib
        require_once(APPPATH.'libraries/pheanstalk/pheanstalk_init.php');

        # Pheanstalk init
        $pheanstalk = new Pheanstalk(getenv('QUEUE_PHEANSTALK_HOST'));

        # Exit after x minute : fix memory leak
        $start = time();
        while((time() - $start) < 300) {
            $job        = $pheanstalk->watchOnly(QUE_MEDIA)->ignore('default')->reserve();
            $job_data   = json_decode($job->getData(), true);
            $pheanstalk->delete($job);
            $this->db->reconnect();

            $id   = isset($job_data['id'])   ? $job_data['id']   : false;
            $path = isset($job_data['path']) ? $job_data['path'] : false;
            $type = isset($job_data['type']) ? $job_data['type'] : false;

            # Check params
            if ( (!$path) OR (!$type) ) {
                write_log("Param is invalid : {$id} _ {$type} _ {$path}", 'worker_media');
                return false;
            }

            # Log
            write_log("Start process type {$type} : {$id} {$path}", 'worker_media');

            $result = false;
            switch ($type) {
                case INFO_VIDEO:
                    $result = $this->convert_video($path);
                    break;

                case INFO_VOICE:
                    $result = $this->convert_audio_v2($path);
                    break;

                case INFO_IMAGE:
                    $rotate = isset($job_data['rotate']) ? $job_data['rotate'] : false;
                    $result = $this->rotate_image($path, $rotate);
                    break;

                default:
                    write_log("Convert default case {$type} : {$id} {$path}", 'worker_media');
                    break;
            }
            write_log("End convert", 'worker_media');var_dump($result);

            # Update status
            # $this->db->where('id', $id);
            $this->db->where('type', $type);
            $this->db->where('data', $path);
            $this->db->set('status', INFO_STATUS_SUCCESS);
            $this->db->set('last_run', date('Y-m-d H:i:s'));
            $db_flag = $this->db->update(TBL_CRON);
            write_log("DB update status " . ($db_flag === false ? 'OK' : 'FAIL'), 'worker_media');
        }
    }

    /**
     * Convert audio file
     * @param  boolean $file_path : Path file convert
     * @param  boolean $path_new  : Path new file
     * @return boolean            : Result
     * @created : 28 Feb 2-14
     * @author Chien Tran <tran.duc.chien@kloon.vn>
     */
    function convert_audio($file_path = false, $path_new = false, $rename = false){
        if (!$file_path) {
            write_log("---> Converting file fail. file_path is required", 'worker_media');
            return false;
        }

        write_log("---> Converting file : {$file_path}", 'worker_media');

        if (!file_exists($file_path)) {
            write_log("File not exists : {$file_path}", 'worker_media');
            return false;
        }

        # Lib
        require APPPATH.'/libraries/class.audio_streamer.php';

        # Converting
        try {
            $sound    = new audio_streamer($file_path);
            $new_file = $sound->save();

            # Backup old file
            $bak_file = str_replace('.3gp', '_bak.3gp', $file_path);
            write_log("Backup file : {$file_path} -> {$bak_file}", 'worker_media');
            rename($file_path, $bak_file);

            # Save new file
            write_log("Save new file : {$new_file} -> {$file_path}", 'worker_media');
            rename($new_file, $file_path);

            # Chmod files
            write_log("Chmod files", 'worker_media');
            @chmod($bak_file, 0644);
            @chmod($file_path, 0644);

            return true;
        } catch (Exception $e) {
            write_log(json_encode($e), 'worker_media');
            return false;
        }
    }

    function convert_audio_v2($file_path = false, $path_new = false, $rename = false) {
        if (!$file_path) {
            write_log("---> Converting file fail. file_path is required", 'worker_media');
            return false;
        }

        write_log("---> Converting file : {$file_path}", 'worker_media');

        if (!file_exists($file_path)) {
            write_log("File not exists : {$file_path}", 'worker_media');
            return false;
        }

        # Converting
        try {
            $bak_file = str_replace('.3gp', '_bak.3gp', $file_path);
            $new_file = str_replace('.3gp', '.mp3', $file_path);
            $result   = shell_exec("ffmpeg -y -i {$file_path} -ar 32k -f mp3 {$new_file}");

            # Backup old file
            $bak_file = str_replace('.3gp', '_bak.3gp', $file_path);
            write_log("Backup file : {$file_path} -> {$bak_file}", 'worker_media');
            rename($file_path, $bak_file);

            # Save new file
            write_log("Save new file : {$new_file} -> {$file_path}", 'worker_media');
            write_log($result, 'worker_media');
            rename($new_file, $file_path);

            # Chmod files
            write_log("Chmod files", 'worker_media');
            @chmod($bak_file, 0644);
            @chmod($file_path, 0644);

            return true;
        } catch (Exception $e) {
            write_log(json_encode($e), 'worker_media');
            return false;
        }
    }

    /**
     * Convert video file
     * @param  boolean $file_path : Path file convert
     * @param  boolean $path_new  : Path new file
     * @return boolean            : Result
     * @date   28 Feb 2014
     * @author Chien Tran <tran.duc.chien@kloon.vn>
     */
    function convert_video($file_path = false, $path_new = false, $rename = false){
    }

    /**
     * Rotate image file
     * @param  boolean $file_path : Path file rotate
     * @param  boolean $rotate    : Degree
     * @param  boolean $path_new  : Path new file
     * @param  boolean $rename    : New name
     * @return [type]             : Result
     */
    function rotate_image($file_path = false, $rotate = false, $path_new = false, $rename = false) {
        if (!$file_path) {
            write_log("---> Rotate image fail. file_path is required", 'worker_media');
            return false;
        }

        write_log("---> Rotate {$rotate} image : {$file_path}", 'worker_media');

        if (!file_exists($file_path)) {
            $file_path = get_server_path() . $file_path;
        }

        if (!file_exists($file_path)) {
            write_log("File not exists : {$file_path}", 'worker_media');
            return false;
        }

        if (!$rotate) {
            write_log("Rotate param is required : {$file_path}", 'worker_media');
            return false;
        } else {
            $rotate = floatval($rotate);
        }

        $result = img_rotate($file_path, $rotate);
        write_log($result['message'], 'worker_media');
        if (isset($result['status']) AND ($result['status'] == true)) {
            return true;
        } else {
            return false;
        }
    }

    function test_image() {
        $this->load->helper('pheanstalk');
        $job = array(
            'type'   => INFO_IMAGE,
            'path'   => get_server_path() . 'test/img.jpg',
            'rotate' => 90,
        );

        var_dump($job);
        var_dump(push_job(QUE_MEDIA, json_encode($job)));
    }

    function test_voice() {
        $this->load->helper('pheanstalk');
        $job = array(
            'type' => INFO_VOICE,
            'path' => get_server_path() . 'test/voice2.3gp',
        );

        var_dump($job);
        var_dump(push_job(QUE_MEDIA, json_encode($job)));
    }

    function test_video(){}
}
