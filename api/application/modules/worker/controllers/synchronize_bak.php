<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Synchronize using queue beanstalkd
 * @author tran.duc.chien@kloon.vn
 * @date   10 Feb 2014
 *
 * --- extract_tour_data \ _sync_activity \ _walking_nodes
 * --- get_street_name \ resolve_street_name
 * --- group_street
 * --- clean_unnecessary_streets
 */
class Synchronize extends MX_Controller {

    function __construct(){
        parent::__construct();
    }

    /**
     * Worker extract tour data
     * @return [type]
     * @author Chien Tran <tran.duc.chien@kloon.vn>
     */
    function extract_tour_data(){
        sleep(5);

        # Set limit timeout : 300 seconds = 5 minutes
        ini_set('max_execution_time', 300);

        # Pheanstalk lib
        require_once(APPPATH.'libraries/pheanstalk/pheanstalk_init.php');

        # Pheanstalk init
        $pheanstalk = new Pheanstalk('127.0.0.1');

        # Get jobs
        $pheanstalk_status = $pheanstalk->getConnection()->isServiceListening(); // true or false
        write_log('extract_tour_data: Pheanstalk status : ' . ($pheanstalk_status === true ? 'OK' : 'FAIL') ,'pheanstalk');

        # Exit after x minute : fix memory leak
        $start = time();
        while(time() - $start < 300) {
            $job        = $pheanstalk->watchOnly(QUE_SYNC)->ignore('default')->reserve();
            $job_data   = json_decode($job->getData(), true);
            $pheanstalk->delete($job);
            $this->db->reconnect();

            if(!isset($job_data) || empty($job_data)){
                write_log('Data invalid ', 'worker_extract_tour_data');
                return false;
            }
            $file_path = isset($job_data['data']) ? $job_data['data'] : '';
            if (!empty($file_path) AND !file_exists($file_path)) {
                $file_path = get_server_path() . $file_path;
            }
            $id_cron = isset($job_data['id_cron']) ? $job_data['id_cron'] : '';

            # city id
            $id_city = isset($job_data['id_city']) ? $job_data['id_city'] : '';
            $db_connect = connect_db(false, $id_city);
            if ($db_connect === false) {
                write_log('Connect fail '.json_encode($db_connect), 'worker_extract_tour_data');
                return false;
            }

            # Call sync activity
            $result = $this->_sync_activity($file_path, $db_connect, $id_city);

            # Update cron status
            $update = array();
            if (isset($result['status']) AND $result['status']) {
                $update['status']   = CRON_STATUS_SUCCESS;
                $update['last_run'] = date('Y-m-d H:i:s');

                # Push job to resolve street name
                $this->load->helper('pheanstalk');
                $id_wkgps   = isset($result['id_wkgps']) ? $result['id_wkgps'] : false;
                $job = array(
                    'id_city'   => $id_city,
                    'id_wkgps'  => $id_wkgps,
                    'id_cron'   => $id_cron
                );
                push_job(QUE_RESOLVE_STREET_NAME, json_encode($job));
                write_log('Add queue ' . QUE_RESOLVE_STREET_NAME . ' success data : ' .json_encode($job), 'add_push_job');

                $data_extract_tour = array(
                    'id_city'   => isset($id_city) ? $id_city : FALSE,
                    'id_wk_gps' => isset($id_wkgps) ? $id_wkgps : FALSE,
                    'id_cron'   => isset($id_cron) ? $id_cron : FALSE,
                    'data'      => isset($job_data['data']) ? $job_data['data'] : FALSE
                );
                $flag = $this->db->insert(TBL_EXTRACT_TOURS,$data_extract_tour);
                if($flag != FALSE){
                    write_log('Insert table : ' . TBL_EXTRACT_TOURS . ' success data : ' .json_encode($data_extract_tour), 'add_push_job');
                } else {
                    write_log('Insert table : ' . TBL_EXTRACT_TOURS . ' fail data : ' .json_encode($data_extract_tour), 'add_push_job');
                }
            } else {
                $update['status'] = CRON_STATUS_ERROR;
                if ( isset($result['msg']) ) {
                    $update['note'] = $result['msg'];
                }
            }
            $update['updated_at'] = date('Y-m-d H:i:s');

            # Close city connection
            $db_connect->close();

            $this->db->where('id', $id_cron);
            $db_flag = $this->db->update(TBL_CRON, $update);
            if ($db_flag === false) {
                write_log("Update cron status FAIL _ id: {$id_cron} _ Query: " . $this->db->last_query() . ' _ Data: ' . json_encode($update), 'worker_extract_tour_data');
            }

            # Close DB connection
            $this->db->close();
        }
    }

    /**
     * Read xml content file, insert to worker_activity table
     * @param  boolean $file_path
     * @param  object  $dbconnect
     * @param  integer $id_city
     * @return array
     */
    function _sync_activity($file_path = false, $dbconnect = null, $id_city = 0) {
        $status = true;
        $msg    = '';

        if ($file_path AND !file_exists($file_path)) {
            $file_path = get_server_path() . $file_path;
        }

        # Check file path
        if (!$file_path OR (file_exists($file_path) == false)) {
            write_log('File not exist '.json_encode($file_path), 'worker_sync_activity');
            return array(
                'status' => false,
                'msg'    => 'File not exist'
            );
        }

        # Check database connect
        if (!isset($dbconnect) OR empty($dbconnect)) {
            write_log('Connect not exist '.json_encode($dbconnect), 'worker_sync_activity');
            return array(
                'status' => false,
                'msg'    => 'Connect not exist'
            );
        }

        # Helper
        $this->load->helper('file');

        # Read content from file
        $json_text = read_file($file_path);
        if (!$json_text OR empty($json_text)) {
            write_log('Can not read file '.json_encode($file_path), 'worker_sync_activity');
            return array(
                'status' => false,
                'msg'    => 'Can not read file'
            );
        }

        # Decode data
        $json_data = json_decode($json_text, true);

        if (!is_array($json_data)) {
            write_log('File data is format invalid '.json_encode($json_data), 'worker_sync_activity');
            return array(
                'status' => false,
                'msg'    => 'File data is format invalid'
            );
        }

        # Transaction start
        $dbconnect->trans_start();

        # Params
        $id_task        = isset($json_data['task_id'])     ? $json_data['task_id']     : '';
        $id_employee    = isset($json_data['employee_id']) ? $json_data['employee_id'] : '';
        $id_machine     = isset($json_data['machine_id'])  ? $json_data['machine_id']  : '';
        $module_id      = isset($json_data['module_id'])   ? $json_data['module_id']   : 1;
        $module_id      = (intval($module_id) <= 0) ? 1 : $module_id;
        $end_time       = isset($json_data['end_time'])    ? $json_data['end_time']    : '';
        $start_time     = isset($json_data['start_time'])  ? $json_data['start_time']  : '';
        $id_gps     = FALSE;
        $gps    = $dbconnect->get_where(TBL_WORKER_GPS, array('id_task' => $id_task ))->row_array();
        if (is_array($gps) AND !empty($gps)) {
            $id_gps = isset($gps['id']) ? $gps['id'] : FALSE;
        }

        if (empty($id_task) || !isset($id_task)) {
            write_log('Task id invalid '.json_encode($id_task), 'worker_sync_activity');
            return array(
                'status' => false,
                'msg'    => 'Task id invalid'
            );
        }
        write_log("ID gps " . $id_gps, 'worker_sync_activity');
        # Prepare gps detail record
        if ($id_gps == FALSE) {
            $gps_data = array(
                'id_city'    => $id_city,
                'id_task'    => $id_task,
                'id_object'  => 0,
                'id_worker'  => $id_employee,
                'id_machine' => $id_machine,
                'app'        => $module_id,
                'ontime'     => $start_time,
                'created_at' => date('Y-m-d H:i:s')
            );
            $db_flag  = $dbconnect->insert(TBL_WORKER_GPS, $gps_data);

            if ($db_flag !== false) {
                $id_gps         = $dbconnect->insert_id();
                $gps_data['id'] = $id_gps;
                $gps            = $gps_data;
            } else {
                $return = array(
                    'status' => false,
                    'msg'    => 'Cant not insert new gps _ Query: ' .$dbconnect->last_query(). ' _ Data: ' . json_encode($gps_data)
                );
                write_log(json_encode($return), 'worker_sync_activity');
                return $return;
            }
        }

        # Check id_gps
        if (is_null($id_gps) OR (intval($id_gps) <= 0)) {
            return array(
                'status' => false,
                'msg'    => 'id_gps is invalid_Value: ' . $id_gps
            );
        }

        # All activities
        $activities = array();
        if (!isset($json_data['activities']) OR empty($json_data['activities'])) {
            return array(
                'status' => false,
                'msg'    => 'Activity data is not exist'
            );
        }

        $activities = $this->_walking_nodes($json_data['activities'], 'activityid', 'activitie');

        if (!is_array($activities) OR empty($activities)) {
            return array(
                'status' => false,
                'msg'    => 'Activity data parse is invalid_Data: ' . json_encode($activities)
            );
        }

        # Walking activities elements
        $coordinates = array();
        $coordinate_string = '';
        foreach ($activities as $key_activity => $activity) {
            $activity_id               = isset($activity['activityid'])     ? trim($activity['activityid'])     : '';
            $activity_starttime        = isset($activity['starttime'])      ? trim($activity['starttime'])      : '';
            $activity_endtime          = isset($activity['endtime'])        ? trim($activity['endtime'])        : '';
            $activity_task_activity_id = isset($activity['taskactivityid']) ? trim($activity['taskactivityid']) : '';

            # Locations process
            $locations = array();
            if (isset($activity['locations']) && !empty($activity['locations'])) {
                $locations = $activity['locations'];
                $locations = $this->_walking_nodes($locations, 'lat', 'location');
                if (is_array($locations) AND !empty($locations)) {
                    foreach ($locations as $key_location => $value_location) {
                        $lat  = isset($value_location['lat'])  ? $value_location['lat']  : '';
                        $lon  = isset($value_location['lon'])  ? $value_location['lon']  : '';
                        $time = isset($value_location['time']) ? $value_location['time'] : '';
                        $coordinates[] = array(
                            'id_city'          => $id_city,
                            'id_wkgps'         => $id_gps,
                            'id_activity'      => $activity_id,
                            'id_task'          => $id_task,
                            'id_task_activity' => $activity_task_activity_id,
                            'latitude'         => $lat,
                            'longtitude'       => $lon,
                            'starttime'        => $time,
                            'endtime'          => $time,
                            'created_at'       => date('Y-m-d H:i:s')
                        );
                        $coordinate_string .= "{$lat} {$lon} {$time},";
                    }
                }
            }

            if (is_array($coordinates) AND !empty($coordinates)) {
                $db_flag = $dbconnect->insert_batch(TBL_WORKER_ACTIVITY, $coordinates);
                if ($db_flag === false) {
                    write_log('Insert batch error_Query: '.$dbconnect->last_query().'_Data:'.json_encode($coordinates), 'sync_activity');
                } else {
                    $coordinates = array();
                }
            }
        }

        # Update worker_gps.position
        $coordinate_string = trim($coordinate_string, ',');
        $dbconnect->where('id', $id_gps);
        $dbconnect->set('position', $coordinate_string);
        $dbconnect->set('updated_at', date('Y-m-d H:i:s'));
        $db_flag = $dbconnect->update(TBL_WORKER_GPS);
        if ($db_flag === false) {
            write_log("Update position for worder_gps {$id_gps} fail", 'sync_activity');
        }
        # Transaction end
        $dbconnect->trans_complete();

        return array(
            'status'    => true,
            'id_wkgps'  => $id_gps,
            'msg'       => 'The data has been successfully process'
        );
    }

    /**
     * Return array data from xml parsed data
     * @param  array  $data
     * @param  string $key_checker
     * @param  string $key_value
     * @return [type]
     */
    function _walking_nodes($data = array(), $key_checker = '', $key_value = '') {
        $return = array();
        # Has one
        if (isset($data[$key_value]) && isset($data[$key_value][$key_checker]) || isset($data[$key_checker])) {
            $return[] = $data[$key_value];
        } else {
            # Has many
            if (!empty($key_value) && isset($data[$key_value]) && !empty($data)) {
                $return = $data[$key_value];
            } else {
                if(isset($data) && !empty($data)){
                    foreach ($data as $key => $value) {
                        $return[] = $value;
                    }
                }
            }
        }
        return $return;
    }

    /**
     * Worker resolve street_name
     * @author Chien Tran <trand.duc.chien@kloon.vn>
     * @date_add() 27 Feb 2014
     */
    function get_street_name() {
        sleep(5);

        # Set limit timeout : 900 seconds = 15 minutes
        ini_set('max_execution_time', 900);

        # Pheanstalk lib
        require_once(APPPATH.'libraries/pheanstalk/pheanstalk_init.php');

        # Pheanstalk init
        $pheanstalk = new Pheanstalk('127.0.0.1');

        # Exit after x minute : fix memory leak
        $start = time();
        while(time() - $start < 300) {
            write_log("Start get street name", 'worker_get_street_name');

            # Pheanstalk jobs
            $job        = $pheanstalk->watchOnly(QUE_RESOLVE_STREET_NAME)->ignore('default')->reserve();
            $job_data   = json_decode($job->getData(), true);
            write_log('Job data: ' . json_encode($job_data), 'worker_get_street_name');
            $pheanstalk->delete($job);
            $id_city    = isset($job_data['id_city']) ? $job_data['id_city'] : false;
            $id_wkgps   = isset($job_data['id_wkgps']) ? $job_data['id_wkgps'] : false;
            $id_cron    = isset($job_data['id_cron']) ? $job_data['id_cron'] : false;
            if ($id_city AND $id_wkgps) {
                $db_connect = connect_db(false, $id_city);
                if ($db_connect === false) {
                    write_log("Connect db fail _ City : {$id_city}", 'worker_get_street_name');
                    return false;
                } else {
                    # Get list street and call service resolve street name
                    $db_connect->where('street', '');
                    $db_connect->where('id_wkgps', $id_wkgps);
                    $result = $db_connect->get(TBL_WORKER_ACTIVITY)->result();

                    # Ignore city 1
                    $ignore_city = config_item('ignore_resolve_street');

                    if (is_array($result) AND !empty($result)) { #AND !in_array($id_city, $ignore_city)
                        $total   = count($result);
                        $counter = 1;
                        $batch_data = array();
                        $_index = 0;
                        foreach ($result as $key => $value) {
                            $counter++;
                            $street_name = $this->resolve_street_name($value->latitude, $value->longtitude);
                            $batch_data[] = array(
                                'id'         => $value->id,
                                'street'     => $street_name,
                                'updated_at' => date('Y-m-d H:i:s')
                            );

                            # Logs
                            write_log("{$counter} / {$total} : {$value->latitude} _ {$value->longtitude} _ {$street_name}", 'worker_get_street_name');

                            # update
                            if($_index == 500){
                                if (is_array($batch_data) AND !empty($batch_data)) {
                                    $db_flag = $db_connect->update_batch(TBL_WORKER_ACTIVITY, $batch_data, 'id');
                                    if ($db_flag === false) {
                                        write_log("Batch update 500 record FAIL _ City: {$id_city} _ id_wkgps: {$id_wkgps} _ Data: ".json_encode($batch_data), "worker_get_street_name");
                                        echo "FAIL\n" . PHP_EOL;
                                    } else {
                                        write_log('Batch update 500 record OK', 'worker_get_street_name');
                                        echo "OK\n" . PHP_EOL;
                                    }
                                    $batch_data = array();
                                    $_index = 0;
                                }
                            }
                        }

                        if (is_array($batch_data) AND !empty($batch_data)) {
                            $db_flag = $db_connect->update_batch(TBL_WORKER_ACTIVITY, $batch_data, 'id');
                            if ($db_flag === false) {
                                write_log("Batch update FAIL _ City: {$id_city} _ id_wkgps: {$id_wkgps} _ Data: ".json_encode($batch_data), "worker_get_street_name");
                                echo "FAIL\n" . PHP_EOL;
                            } else {
                                write_log('OK', 'worker_get_street_name');
                                echo "OK\n" . PHP_EOL;
                            }
                        }
                    }

                    # group_street
                    $flag = $this->group_street($id_city, $id_wkgps);

                    # clean_unnecessary_streets
                    # $flag = $this->clean_unnecessary_streets($id_city, $id_wkgps, $db_connect);

                    # clean_unnecessary_streets_v2
                    $flag = $this->clean_unnecessary_streets_v2($id_city, $id_wkgps, $db_connect);

                    # Update worker_gps status
                    $db_connect->where('id', $id_wkgps);
                    $db_flag = $db_connect->update(TBL_WORKER_GPS, array('status' => CRON_STATUS_SUCCESS));
                    if ($db_flag === false) {
                        write_log("Update worker_gps status FAIL _ city: {$id_city} _ id_wkgps: {$id_wkgps}", 'worker_get_street_name');
                    }

                    #update cron status
                    $this->db->where('id', $id_cron);
                    $db_flag = $this->db->update(TBL_CRON, array('status' => CRON_STATUS_SUCCESS));
                    if ($db_flag === false) {
                        write_log("Update cron status FAIL _ city: {$id_city} _ id_cron: {$id_cron} _ Query : " . $this->db->last_query(), 'worker_get_street_name');
                    } else {
                        # delete data
                        #$id_city,$id_wkgps,$id_cron
                        $extract_detail = array();
                        $this->db->where('id_city'      , $id_city);
                        $this->db->where('id_wk_gps'    , $id_wkgps);
                        $this->db->where('id_cron'      , $id_cron);
                        $extract_detail = $this->db->get(TBL_EXTRACT_TOURS)->row_array();
                        $extract_tour_data = FALSE;
                        if(is_array($extract_detail) AND !empty($extract_detail)){
                            $extract_tour_data = isset($extract_detail['data']) ? $extract_detail['data'] : FALSE;
                            # delete
                            $this->db->where('id_city'   , $id_city);
                            $this->db->where('id_wk_gps' , $id_wkgps);
                            $this->db->where('id_cron'   , $id_cron);
                            $this->db->delete('TBL_EXTRACT_TOURS');
                        } else {
                            write_log('Tour empty data : ' . json_encode(array('id_city'=>$id_city,'id_wk_gps'=>$id_wkgps,'id_cron'=>$id_cron)), 'add_push_job');
                        }
                        if($extract_tour_data != FALSE){
                            $this->db->where('id_city' , $id_city);
                            $this->db->where('id_cron' , $id_cron);
                            $this->db->where('data'    , $extract_tour_data);
                            $this->db->delete('TBL_CRON_PUS_JOBS');
                        }
                    }
                }

                # Close db connection
                $db_connect->close();
            }
        }
    }

    /**
     * Update group_street field for grouping
     * @param  [type] $id_city
     * @param  [type] $id_wkgps
     * @return [type]
     */
    function group_street($id_city = null, $id_wkgps = null, $db_connect = null) {
        if (is_null($id_city)) {
            return 'Requried id_city';
        }

        $db_connect = connect_db(false, $id_city);
        if (!$db_connect) {
            print_r(array(
                'status' => false,
                'msg'    => 'Connect fail'
            ));
            die;
        }

        if (is_null($id_wkgps)) {
            $db_connect->select('id_wkgps');
            $db_connect->where('group_street', 0);
            $data = $db_connect->get(TBL_WORKER_ACTIVITY)->row();
            if (isset($data->id_wkgps)) {
                $id_wkgps = $data->id_wkgps;
            } else {
                return 'id_wkgps is required';
            }
        }
        echo "---> id_wkgps : ".$id_wkgps;

        # Activites
        $db_connect->where('deleted_at', NULL, false);
        $db_connect->where('id_wkgps', $id_wkgps);
        $db_connect->order_by('id', 'asc');
        $activities =$db_connect->get(TBL_WORKER_ACTIVITY)->result_array();

        if (is_array($activities) AND !empty($activities)) {
            echo '------------------------------------------' . PHP_EOL;
            $batch_data = array();
            if (is_array($activities) AND !empty($activities)) {
                $counter = 1;
                $last_street = '';
                foreach ($activities as $key => $value) {
                    $current_street = trim($value['street']);
                    if (empty($last_street)) {
                        $last_street = $current_street;
                    }
                    $street_checker = array();
                    if (is_array($value) AND !empty($value)) {
                        if ($last_street != $current_street) {
                            $counter++;
                        }
                        $last_street = $current_street;
                        $batch_data[] = array(
                            'id'           => $value['id'],
                            'group_street' => $counter
                        );
                    }
                }

                echo '---> Batch data :' . PHP_EOL;
                print_r($batch_data);
                if (is_array($batch_data) AND !empty($batch_data)) {
                    echo '---> Update batch worker_activity : ' . PHP_EOL;
                    $db_flag = $db_connect->update_batch(TBL_WORKER_ACTIVITY, $batch_data, 'id');
                    echo $db_connect->last_query();
                    if ($db_flag === false) {
                        $msg = 'FAIL _ Query : '. $this->db->last_query();
                    } else {
                        $msg = 'OK _ Query : '. $this->db->last_query();
                    }
                }
            }
        }

        # Close db connection
        $db_connect->close();
    }

    /**
     * Resolve street name from lat, lon
     * @param  string $latitude
     * @param  string $longtitude
     * @param  string $service
     * @return string
     */
    function resolve_street_name($latitude, $longtitude, $service = 'kloon') {
        $services = array(
            'kloon'         => 'http://nominatim.kloon.net/reverse?format=json&zoom=17&addressdetails=1',
            'mapquest'      => 'http://open.mapquestapi.com/nominatim/v1/reverse.php?format=json&zoom=17',
            'openstreetmap' => 'http://nominatim.openstreetmap.org/reverse?format=json&zoom=17&addressdetails=1',
            'gisgraphy'     => 'http://services.gisgraphy.com/street/streetsearch?format=json&from=1&to=1',
        );

        if (!isset($services[$service])) {
            $service = $services['mapquest'];
        } else {
            $service = $services[$service];
        }

        $street = '';
        if ((isset($latitude) && !empty($latitude)) && (isset($longtitude) && !empty($longtitude))) {
            $service_url    = '';
            $service_type   = $service;
            $count  = 0;
            $status = FALSE;
            while ( ($count < 3) AND ($status != TRUE) ) {
                switch ($count) {
                    case 1:
                        $service_type = 'mapquest';
                        $service_url  = $services['mapquest'] . '&lat=' . $latitude . '&lon=' . $longtitude;
                        break;

                    case 2:
                        $service_type = 'openstreetmap';
                        $service_url  = $services['openstreetmap'] . '&lat=' . $latitude . '&lon=' . $longtitude;
                        break;

                    default:
                        $service_type = 'kloon';
                        $service_url  = $services['kloon'] . '&lat=' . $latitude . '&lon=' . $longtitude;
                        break;
                }
                $street = $this->curl_read_content($service_url, $service_type);

                # Logs
                write_log("{$service_type}: {$latitude} - {$longtitude} {$street}", 'resolve_street_name');

                if( (isset($street) AND !empty($street)) OR ($count >= 3) ){
                    $status = TRUE;
                }
                $count++;

                usleep(100);
            }
        }
        return $street;
    }

    function curl_read_content($link = FALSE, $service = 'kloon', $timeout = 2500){
        $street = '';
        if($link != FALSE){
            $street_data = '';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $link);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            #curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 2000);
            $street_data = curl_exec($ch);
            curl_close($ch);

            $street_array = json_decode($street_data,true);
            if (isset($street_array['address']) && !empty($street_array['address'])) {
                $street_detail = $street_array['address'];
                if(isset($street_detail['road']) && !empty($street_detail['road'])){
                    $street_name = isset($street_detail['road']) ? $street_detail['road'] : '';
                    $street      = $street_name;
                } else {
                    /*if(isset($street_detail['address26']) && !empty($street_detail['address26'])){
                        $street_name = isset($street_detail['address26']) ? $street_detail['address26'] : '';
                        $street      = $street_name;
                    }*/
                }
            }
        }
        return $street;
    }

    function resolve_street_name_bak($latitude, $longtitude, $service = 'kloon') {
        $services = array(
            'kloon'         => 'http://nominatim.kloon.net/reverse?format=json&zoom=17&addressdetails=1',
            'gisgraphy'     => 'http://services.gisgraphy.com/street/streetsearch?format=json&from=1&to=1',
            'openstreetmap' => 'http://nominatim.openstreetmap.org/reverse?format=json&zoom=18&addressdetails=1',
            'mapquest'      => 'http://open.mapquestapi.com/nominatim/v1/reverse.php?format=json',
        );

        if (!isset($services[$service])) {
            $service = $services['openstreetmap'];
        } else {
            $service = $services[$service];
        }

        $street = '';
        if ((isset($latitude) && !empty($latitude)) && (isset($longtitude) && !empty($longtitude))) {
            $street_data = '';
            try {
                $street_data = file_get_contents($service.'&lat='.$latitude.'&lon='.$longtitude);
            } catch (Exception $e) {}

            if(isset($street_data) && !empty($street_data)){
                $street_array = json_decode($street_data,true);
                if (isset($street_array['address']) && !empty($street_array['address'])) {
                    $street_detail = $street_array['address'];
                    if(isset($street_detail['road']) && !empty($street_detail['road'])){
                        $street_name = isset($street_detail['road']) ? $street_detail['road'] : '';
                        $street      = $street_name;
                        write_log("{$latitude} {$longtitude} {$street}", 'resolve_street_name');
                    }
                }
            }
        }
        return $street;
    }

    /**
     * Remove unnecessary street by update street field in worker_activity table
     * @param  [type] $id_city
     * @param  [type] $id_wkgps
     * @param  [type] $db_connect
     * @return [type]
     */
    function clean_unnecessary_streets($id_city = null, $id_wkgps = null, $db_connect = null) {
        if ( is_null($id_city) OR is_null($id_wkgps) ) {
            die('id_city or id_wkgps can not null');
        }

        if (is_null($db_connect)) {
            $db_connect = connect_db(false, $id_city);
        }

        # Logs
        write_log("Start clean_unnecessary_streets _ id_city: {$id_city} _ id_wkgps: {$id_wkgps}", 'clean_unnecessary_streets');

        # Set max lenght group_concat function
        $db_connect->query('SET SESSION group_concat_max_len = 1000000000');

        $db_connect->select('id, id_city, id_wkgps, id_activity, id_task, id_task_activity, street, street_old, group_street, min(`starttime`) as `start_time`, max(`starttime`) as `end_time`, (max(`starttime`) - min(`starttime`) ) as duration, GROUP_CONCAT(id) as id_range ', false);
        $db_connect->where('deleted_at', NULL);
        $db_connect->group_by(array('id_wkgps', 'id_activity', 'id_task_activity', 'street', 'group_street'));
        $db_connect->order_by('id', 'asc');
        $result = $db_connect->get_where(TBL_WORKER_ACTIVITY, array('id_wkgps' => $id_wkgps))->result_array();

        if ( !is_array($result) OR empty($result) ) {
            $msg = 'List activities are empty';

            # Logs
            write_log($msg, 'clean_unnecessary_streets');
            return false;
        }

        # Mark by duration
        foreach ($result as $key => $value) {
            $result[$key]['flag_duration'] = '';
            if ( ($value['duration'] < 10) AND ( isset($result[$key-1]) AND ($result[$key]['id_activity'] == $result[$key-1]['id_activity']) ) ) {
                $result[$key]['flag_duration'] = 'x';
            }
        }

        # Mark by street name sequence
        foreach ($result as $key => $value) {
            if (!isset($result[$key]['flag_1'])) {
                $result[$key]['flag_1'] = '';
            }
            if ($value['flag_duration'] == 'x') {
                $backward1 = isset($result[$key - 1]) ? $result[$key - 1] : false;
                $forward1  = isset($result[$key + 1]) ? $result[$key + 1] : false;
                if ( $backward1 AND $forward1 AND ($backward1['street'] == $forward1['street']) AND 1) {
                    $result[$key]['flag_1']           = 'x';
                    $result[$key]['street']           = $backward1['street'];
                    $result[$key]['street_old']       = $value['street'];
                    $result[$key]['group_street']     = $backward1['group_street'];

                    $result[$key + 1]['flag_1']       = 'x';
                    $result[$key + 1]['street']       = $backward1['street'];
                    $result[$key + 1]['street_old']   = $result[$key + 1]['street'];
                    $result[$key + 1]['group_street'] = $backward1['group_street'];
                }
            }
        }

        # Batch update street, street_old, group_street
        $batch_update = array();
        $counter = 0;
        $total = count($result);
        foreach ($result as $key => $value) {
            $counter++;
            if ($value['flag_1'] == 'x') {
                $list_id = trim($value['id_range']);
                $list_id = explode(',', $value['id_range']);
                foreach ($list_id as $sub_key => $sub_value) {
                    $output  = "---> {$counter} / {$total}\n".PHP_EOL;
                    $batch_update[] = array(
                        'id'           => $sub_value,
                        'street'       => $value['street'],
                        'street_old'   => $value['street_old'],
                        'group_street' => $value['group_street'],
                        'updated_at'   => date('Y-m-d H:i:s')
                    );

                    $output .= "-> id: {$sub_value}\n".PHP_EOL;
                    $output .= "-> street: {$value['street']}\n".PHP_EOL;
                    $output .= "-> street_old: {$value['street_old']}\n".PHP_EOL;
                    $output .= "-> group_street: {$value['group_street']}\n".PHP_EOL;
                    echo "$output\n\n".PHP_EOL;
                }
            }
        }

        if (!empty($batch_update)) {
            $output  = "---> Batch update : \n".PHP_EOL;
            $db_flag = $db_connect->update_batch(TBL_WORKER_ACTIVITY, $batch_update, 'id');
            $db_flag = (($db_flag === false) ? 'FAIL' : 'OK');
            echo $db_flag . "\n" . PHP_EOL;

            write_log("End clean_unnecessary_streets {$db_flag}", 'clean_unnecessary_streets');
        } else {
            $this->load->library('table');
            $this->table->set_heading(
                'id',
                'id_city',
                'id_wkgps',
                'id_activity',
                'id_task',
                'id_task_activity',
                'street',
                'street_old',
                'group_street',
                'start_time',
                'end_time',
                'duration',
                'id_range',
                'flag_duration',
                'flag_1'
            );
            $tmpl = array ( 'table_open'  => '<table border="1" cellpadding="5" cellspacing="0" class="mytable">' );
            $this->table->set_template($tmpl);
            echo $this->table->generate($result);
        }
    }

    function clean_unnecessary_streets_v2($id_city = null, $id_wkgps = null, $db_connect = null){
        if ( is_null($id_city) OR is_null($id_wkgps) ) {
            die('id_city or id_wkgps can not null');
        }

        if (is_null($db_connect)) {
            $db_connect = connect_db(false, $id_city);
        }

        # Logs
        write_log("Start clean_unnecessary_streets2 _ id_city: {$id_city} _ id_wkgps: {$id_wkgps}", 'clean_unnecessary_streets_v2');

        # get activiti group
        $activiti_result = array();

        # Set max lenght group_concat function
        $db_connect->query('SET SESSION group_concat_max_len = 1000000000');

        $db_connect->select("wac.id_task_activity, group_concat(wac.id ORDER BY wac.id) as id_group, group_concat(wac.street_old  ORDER BY wac.id) as street_name_old,wac.id_wkgps,wac.id_activity,min(wac.starttime) as starttime,max(wac.endtime) as endtime, (max(wac.endtime) - min(wac.starttime)) as duration,wac.street,wac.group_street");
        $db_connect->from(TBL_WORKER_ACTIVITY.' as wac');
        $db_connect->where('wac.deleted_at',NULL);
        $db_connect->where('wac.id_wkgps',$id_wkgps);
        $db_connect->order_by('wac.id','asc');
        $db_connect->group_by('wac.id_wkgps,wac.id_activity,wac.id_task_activity,wac.street,wac.group_street');
        $activiti_result = $db_connect->get()->result_array();
        $group_street = NULL;
        $is_first     = FALSE;
        $count        = count($activiti_result);
        $limit_time   = 3;
        for ($i=0; $i < $count; $i++) {
            $duration = strtotime($activiti_result[$i]['endtime']) - strtotime($activiti_result[$i]['starttime']);
            if($duration < $limit_time){#$activiti_result[$i]['duration']
                if($i < ($count - 1)){
                    $check_duration = FALSE;
                    for ($j=($i + 1); $j < $count; $j++) {
                        $_duration = strtotime($activiti_result[$j]['endtime']) - strtotime($activiti_result[$j]['starttime']);
                        if ($_duration > ($limit_time - 1) ) {#$activiti_result[$j]['duration']
                            for ($k = $i; $k < $j; $k++) {
                                $activiti_result[$k]['street_old']       = $activiti_result[$k]['street'];
                                $activiti_result[$k]['id_activity']      = $activiti_result[$j]['id_activity'];
                                $activiti_result[$k]['id_task_activity'] = $activiti_result[$j]['id_task_activity'];
                                $activiti_result[$k]['street']           = $activiti_result[$j]['street'];
                                $activiti_result[$k]['group_street']     = $activiti_result[$j]['group_street'];
                                $activiti_result[$k]['flag']             = 'x';
                            }
                            $check_duration = TRUE;
                            $i = $j;
                            break;
                        }
                    }
                    if($check_duration != TRUE){
                        if($i > 0){
                            for ($k = $i; $k < $count; $k++) {
                                $activiti_result[$k]['street_old']       = $activiti_result[$k]['street'];
                                $activiti_result[$k]['id_activity']      = $activiti_result[$i - 1]['id_activity'];
                                $activiti_result[$k]['id_task_activity'] = $activiti_result[$i - 1]['id_task_activity'];
                                $activiti_result[$k]['street']           = $activiti_result[$i - 1]['street'];
                                $activiti_result[$k]['group_street']     = $activiti_result[$i - 1]['group_street'];
                                $activiti_result[$k]['flag']             = 'x';
                            }
                        } else {
                            $db_connect->where('id_wkgps', $id_wkgps);
                            $flag = $db_connect->update(TBL_WORKER_ACTIVITY, array('deleted_at' => date('Y-m-d H:i:s')));
                        }
                    }
                } else {
                    $activiti_result[$i]['street_old']       = $activiti_result[$i]['street'];
                    $activiti_result[$i]['id_activity']      = $activiti_result[$i - 1]['id_activity'];
                    $activiti_result[$i]['id_task_activity'] = $activiti_result[$i - 1]['id_task_activity'];
                    $activiti_result[$i]['street']           = $activiti_result[$i - 1]['street'];
                    $activiti_result[$i]['group_street']     = $activiti_result[$i - 1]['group_street'];
                    $activiti_result[$i]['flag']             = 'x';
                }
            }
        }

        for($i = 0; $i < count($activiti_result); $i++) {
            if($i > 0){
                if ( (strtolower(trim($activiti_result[$i]['street'])) == strtolower(trim($activiti_result[$i - 1]['street']))) OR (trim($activiti_result[$i]['street']) == '') ){
                    $activiti_result[$i]['group_street'] = $activiti_result[$i - 1]['group_street'];
                    /*if($activiti_result[$i]['id_activity'] = $activiti_result[$i]['id_activity']){
                        $activiti_result[$i]['id_task_activity'] = $activiti_result[$i - 1]['id_task_activity'];
                    }*/
                    $activiti_result[$i]['flag']         = 'x';
                }
            }
        }

        $update_array = array();
        foreach ($activiti_result as $key => $value) {
            $flag = isset($value['flag']) ? TRUE : FALSE;
            if($flag){
                $id_group           = $value['id_group'];
                $id_array           = explode(',', $id_group);
                $street_name_old    = $value['street_name_old'];
                $street_array       = explode(',', $street_name_old);
                foreach ($id_array as $id_key => $id_value) {
                    if (intval($id_value) > 0) {
                        $_street_old    = isset($value['street_old']) ? $value['street_old'] : $value['street'];
                        $update_array[] = array(
                            'id'               => $id_value,
                            'id_activity'      => $value['id_activity'],
                            'id_task_activity' => $value['id_task_activity'],
                            'street'           => $value['street'],
                            'street_old'       => (isset($street_array[$id_key]) ? $street_array[$id_key] : '').'-'.$_street_old,
                            'group_street'     => $value['group_street'],
                            'updated_at'       => date('Y-m-d H:i:s')
                        );
                        write_log("ID: {$id_value} - Street: {$value['street']}", 'clean_unnecessary_streets_v2');
                    } else {
                        write_log("ID: {$id_value} - ID invalid: {$value['street']}", 'clean_unnecessary_streets_v2');
                    }
                }
            }
        }
        if(is_array($update_array) AND !empty($update_array)){
            $flag = $db_connect->update_batch(TBL_WORKER_ACTIVITY,$update_array,'id');
            if ($flag === FALSE) {
                write_log("FAIL", 'clean_unnecessary_streets_v2');
            } else {
                write_log("OK", 'clean_unnecessary_streets_v2');
                $this->load->helper('pheanstalk');
                $job = array(
                    'id_city'  => $id_city,
                    'id_wkgps' => $id_wkgps
                );
                push_job(QUE_CLEAN_POINT, json_encode($job));
                write_log('Add queue ' . QUE_CLEAN_POINT . ' success data : ' .json_encode($job), 'clean_unnecessary_streets_v2');
            }
        }else {
            write_log("OK - Data update invalid", 'clean_unnecessary_streets_v2');
        }
    }

    function clean_point_position(/*$id_city = null, $id_wkgps = null, $db_connect = null*/){
        sleep(5);

        # Pheanstalk lib
        require_once(APPPATH.'libraries/pheanstalk/pheanstalk_init.php');

        # Pheanstalk init
        $pheanstalk = new Pheanstalk('127.0.0.1');

        $start = time();
        while((time() - $start) < 300) {
            $job        = $pheanstalk->watchOnly(QUE_CLEAN_POINT)->ignore('default')->reserve();
            $job_data   = json_decode($job->getData(), true);
            $pheanstalk->delete($job);
            if(!isset($job_data) || empty($job_data)){
                write_log('Data invalid ', 'clean_point_position');
                return false;
            }
            $id_city    = isset($job_data['id_city'])   ? $job_data['id_city']  : FALSE;
            $id_wkgps   = isset($job_data['id_wkgps'])  ? $job_data['id_wkgps'] : FALSE;

            if ( !isset($id_city) OR empty($id_city) ) {
                write_log('id_city invalid ', 'clean_point_position');
                return false;
            }

            if ( !isset($id_wkgps) OR empty($id_wkgps) ) {
                write_log('id_wkgps invalid ', 'clean_point_position');
                return false;
            }

            $db_connect = connect_db(false, $id_city);
            # get position
            $gps_result = array();
            $db_connect->where('id_wkgps', $id_wkgps);
            $gps_result = $db_connect->get(TBL_WORKER_ACTIVITY)->result_array();
            if(!is_array($gps_result) OR empty($gps_result)){
                write_log("Data GPS empty", 'clean_point_position');
                die;
            }
            $line_position = '';
            $old_lat        = false;
            $old_lon        = false;
            $old_street     = false;
            $old_activiti   = false;
            foreach ($gps_result as $key => $value){
                if($key < 1){
                    $old_lat        = $value['latitude'];
                    $old_lon        = $value['longtitude'];
                    $old_street     = $value['street'];
                    $old_activiti   = $value['id_activity'];
                    $line_position = $old_lat . ' ' . $old_lon;
                    continue;
                }
                $lat            = $value['latitude'];
                $lon            = $value['longtitude'];
                $street         = $value['street'];
                $activiti       = $value['id_activity'];

                if(($street == $old_street) AND ($activiti == $old_activiti)){
                    $distance = $this->distance_gps($old_lat,$old_lon,$lat,$lon);
                    if($distance > 10){
                        $old_lat        = $lat;
                        $old_lon        = $lon;
                        $old_street     = $street;
                        $old_activiti   = $activiti;
                        $line_position  = $line_position . ',' . $old_lat . ' ' . $old_lon;
                    }
                } else {
                    $old_lat        = $lat;
                    $old_lon        = $lon;
                    $old_street     = $street;
                    $old_activiti   = $activiti;
                    $line_position  = $line_position . ',' . $old_lat . ' ' . $old_lon;
                }
            }
            if(isset($line_position) AND !empty($line_position)){
                $db_connect->where('id', $id_wkgps);
                $flag = $db_connect->update(TBL_WORKER_GPS,array('position_clean' => $line_position));
                if($flag != FALSE){
                    write_log("Update GPS OK - id : " . $id_wkgps, 'clean_point_position');
                } else {
                    write_log("Update GPS FAIL - id : " . $id_wkgps, 'clean_point_position');
                }
            } else {
                write_log("Data position empty - id : " . $id_wkgps, 'clean_point_position');
            }

            # Close db connection
            $db_connect->close();
        }
    }

    function distance_gps($lat1 = 0, $lon1 = 0, $lat2 = 0, $lon2 = 0,$unit = 'm'){
        $distance = (3956 * acos(cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($lon2) - deg2rad($lon1)) + sin(deg2rad($lat1)) * sin(deg2rad($lat2))));
        switch ($unit)
        {
            case 'mi':  #miles
                return $distance;
                break;
            case 'km':  #kilometers
                return 1.609344 * $distance;
                break;
            case 'm':   #meters
                return 1.609344 * 1000 * $distance;
                break;
            case 'y':   #yards
                return 1760 * $distance;
                break;
            case 'ft':  #feet
                return 1760 * 3 * $distance;
                break;
            default:
                return $distance;
                break;
        }
    }

    function add_que_service($id_city, $id_gps){
        require_once(APPPATH.'libraries/pheanstalk/pheanstalk_init.php');
        $this->load->helper('pheanstalk');
        $job = array(
            'id_city' => $this->_id_city,
            'id_gps'  => $id_gps
        );
        push_job(QUE_SERVICE_POST, json_encode($job));
        echo "OK";
    }

    function post_service(){
        sleep(5);

        // Set limit timeout : 300 seconds = 5 minutes
        ini_set('max_execution_time', 300);
        # Pheanstalk lib
        require_once(APPPATH.'libraries/pheanstalk/pheanstalk_init.php');
        // Pheanstalk init
        $pheanstalk = new Pheanstalk('127.0.0.1');

        // Get jobs
        $pheanstalk_status = $pheanstalk->getConnection()->isServiceListening(); // true or false
        write_log('extract_tour_data: Pheanstalk status : ' . ($pheanstalk_status === true ? 'OK' : 'FAIL') ,'pheanstalk');

        // Exit after x minute : fix memory leak
        $start = time();
        while(time() - $start < 300) {
            $job        = $pheanstalk->watchOnly(QUE_SERVICE_POST)->ignore('default')->reserve();
            $job_data   = json_decode($job->getData(), true);
            $pheanstalk->delete($job);
            $this->db->reconnect();

            if(!isset($job_data) || empty($job_data)){
                write_log('Data invalid ', 'post_service');
                return false;
            }

            $id_city = isset($job_data['id_city']) ? $job_data['id_city'] : '';
            $id_gps  = isset($job_data['id_gps']) ? $job_data['id_gps'] : '';
            write_log('data - id_city : ' . $id_city . ' & id_gps : ' . $id_gps, 'post_service');
            # connect
            $db_connect = connect_db(null, $id_city);

            # get infor worker, machin
            $gps_detail = array();
            $db_connect->where('id',$id_gps);
            $gps_detail = $db_connect->get(TBL_WORKER_GPS)->row_array();
            if(!is_array($gps_detail) OR empty($gps_detail)){
                echo "gps empty";
                return;
            }
            $id_worker = isset($gps_detail['id_worker']) ? $gps_detail['id_worker'] : '';
            $id_module = isset($gps_detail['app']) ? $gps_detail['app'] : '';
            $ontime    = isset($gps_detail['ontime']) ? $gps_detail['ontime'] : '';

            # get name module
            $module_name   = '';
            $module_detail = array();
            $db_connect->where('id',$id_module);
            $module_detail = $db_connect->get(TBL_MODULES)->row_array();
            if(is_array($module_detail) AND !empty($module_detail)){
                $module_name = $module_detail['title'];
            }
            # get name worker
            $worker_name = '';
            $worker_detail = array();
            $db_connect->where('id',$id_worker);
            $worker_detail = $db_connect->get(TBL_WORKER)->row_array();
            if(is_array($worker_detail) AND !empty($worker_detail)){
                $worker_name = $worker_detail['first_name'] . ' ' . $worker_detail['last_name'];
            }
            # get information
            $information_gps = array();
            $information     = array();
            $db_connect->where('id_worker_gps',$id_gps);
            $information = $db_connect->get(TBL_WORKER_INFORMATION)->result_array();
            foreach ($information as $key => $val) {
                $detail_lat  = $val['lat'];
                $detail_lon  = $val['lon'];
                $detail_type = $val['type'];
                $detail_data = $val['data'];
                $detail_time = $val['time'];
                if(!isset($information_gps[$detail_lat . '-' . $detail_lon]['gps']) OR empty($information_gps[$detail_lat . '-' . $detail_lon]['gps'])){
                    $information_gps[$detail_lat . '-' . $detail_lon]['gps'] = $detail_lat . ' ' . $detail_lon;
                }
                if(!isset($information_gps[$detail_lat . '-' . $detail_lon]['street']) OR empty($information_gps[$detail_lat . '-' . $detail_lon]['street'])){
                    $street = $this->resolve_street_name($detail_lat, $detail_lon);
                    $information_gps[$detail_lat . '-' . $detail_lon]['street'] = $street;
                }
                $information_gps[$detail_lat . '-' . $detail_lon]['data'][] = array(
                    'id'   => $val['id'],
                    'time' => $detail_time,
                    'type' => $detail_type,
                    'data' => $detail_data,
                    'path' => 'uploads/' . $id_city . '/' . date('Y/m/d', strtotime($ontime)) . '/' . $id_worker . '/'
                );
            }
            write_log('data information : ' . json_encode($information_gps), 'post_service');
            $url    = 'http://www.auftragsbuch.info/ps_file.php';
            $return = $this->_post($url, $information_gps, $worker_name, $module_name, $ontime);
            write_log('id_city: '. $id_city . ' _ Data: ' . json_encode($return), 'post_service');
        }
    }

    function post_service_2($_id_city = NULL, $_id_gps = NULL){
        $id_city = $_id_city;
        $id_gps  = $_id_gps;
        write_log('data - id_city : ' . $id_city . ' & id_gps : ' . $id_gps, 'post_service');
        # connect
        $db_connect = connect_db(null, $id_city);

        # get infor worker, machin
        $gps_detail = array();
        $db_connect->where('id',$id_gps);
        $gps_detail = $db_connect->get(TBL_WORKER_GPS)->row_array();
        if(!is_array($gps_detail) OR empty($gps_detail)){
            echo "gps empty";
            return;
        }
        $id_worker = isset($gps_detail['id_worker']) ? $gps_detail['id_worker'] : '';
        $id_module = isset($gps_detail['app']) ? $gps_detail['app'] : '';
        $ontime    = isset($gps_detail['ontime']) ? $gps_detail['ontime'] : '';

        # get name module
        $module_name   = '';
        $module_detail = array();
        $db_connect->where('id',$id_module);
        $module_detail = $db_connect->get(TBL_MODULES)->row_array();
        if(is_array($module_detail) AND !empty($module_detail)){
            $module_name = $module_detail['title'];
        }
        # get name worker
        $worker_name = '';
        $worker_detail = array();
        $db_connect->where('id',$id_worker);
        $worker_detail = $db_connect->get(TBL_WORKER)->row_array();
        if(is_array($worker_detail) AND !empty($worker_detail)){
            $worker_name = $worker_detail['first_name'] . ' ' . $worker_detail['last_name'];
        }
        # get information
        $information_gps = array();
        $information     = array();
        $db_connect->where('id_worker_gps',$id_gps);
        $information = $db_connect->get(TBL_WORKER_INFORMATION)->result_array();
        foreach ($information as $key => $val) {
            $detail_lat  = $val['lat'];
            $detail_lon  = $val['lon'];
            $detail_type = $val['type'];
            $detail_data = $val['data'];
            $detail_time = $val['time'];
            if(!isset($information_gps[$detail_lat . '-' . $detail_lon]['gps']) OR empty($information_gps[$detail_lat . '-' . $detail_lon]['gps'])){
                $information_gps[$detail_lat . '-' . $detail_lon]['gps'] = $detail_lat . ' ' . $detail_lon;
            }
            if(!isset($information_gps[$detail_lat . '-' . $detail_lon]['street']) OR empty($information_gps[$detail_lat . '-' . $detail_lon]['street'])){
                $street = $this->resolve_street_name($detail_lat, $detail_lon);
                $information_gps[$detail_lat . '-' . $detail_lon]['street'] = $street;
            }
            $information_gps[$detail_lat . '-' . $detail_lon]['data'][] = array(
                'id'   => $val['id'],
                'time' => $detail_time,
                'type' => $detail_type,
                'data' => $detail_data,
                'path' => config_item('upload_dir') . $id_city . '/' . date('Y/m/d', strtotime($ontime)) . '/' . $id_worker . '/'
            );
        }
        write_log('data information : ' . json_encode($information_gps), 'post_service');
        $url    = 'http://www.auftragsbuch.info/ps_file.php';
        $return = $this->_post($url, $information_gps, $worker_name, $module_name, $ontime);
        write_log('id_city: '. $id_city . ' _ Data: ' . json_encode($return), 'post_service');
    }

    function _post($url , $data, $_worker, $_module, $_ontime){
        $return             = array();
        $eol                = "\r\n";
        $boundary           = 'AaB03x';
        $index = 0;
        foreach ($data as $key => $val) {
            $index++;
            $current_time       = time();
            $_file_name_content = date('YmdHis',strtotime($_ontime)) . $index .'$1$.msg';
            write_log('File .msg : ' . $_file_name_content, 'post_service');
            $_name_file = '';
            $_add_file  = '';
            $count = 1;
            foreach ($val['data'] as $_key => $_val) {
                if($_val['type'] != INFO_MESSAGE){
                    $file_path = $_val['path'] . $_val['data'];
                    if (file_exists($file_path) AND is_readable($file_path)) {
                        $_name_file .= $_val['data'] . ';';

                        # add file
                        $count++;
                        $_add_file         .= '--' . $boundary . $eol;
                        $_file_name_attach = date('YmdHis',strtotime($_ontime)) . $index .'$' . $count . '$.att';
                        $_add_file         .= 'Content-Disposition: form-data; name="somefile"; filename="'. $_file_name_attach .'"' . $eol;
                        write_log('File .att' . $_file_name_attach, 'post_service');
                        if(strpos($file_path, '.jpg') !== false){
                            $_add_file .= 'Content-Transfer-Encoding: binary';
                            $_add_file .= 'Content-Type: image/jpeg' . $eol;
                        } else {
                            if(strpos($file_path, '.3gp') !== false){
                                $_add_file .= 'Content-Transfer-Encoding: binary';
                                $_add_file .= 'Content-Type: video/3gpp' . $eol;
                            } else {
                                $_add_file .= 'Content-Transfer-Encoding: binary';
                                $_add_file .= 'Content-Type: video/mp4' . $eol;
                            }
                        }
                        print_r($file_path).$eol;
                        print_r(@file_get_contents($file_path)) . $eol;
                        echo '=========================';
                        $handle = fopen($file_path, "rb");
                        $contents = fread($handle, filesize($file_path));
                        fclose($handle);
                        $_add_file .= $contents . $eol;
                    }
                } else {
                    $count++;
                    $_add_file .= '--' . $boundary . $eol;
                    $_file_name_attach = date('YmdHis',strtotime($_ontime)) . $index .'$' . $count . '$.att';
                    $_add_file .= 'Content-Disposition: form-data; name="somefile"; filename="'. $_file_name_attach .'"' . $eol;
                    write_log('File .att' . $_file_name_attach, 'post_service');
                    $_add_file .= 'Content-Transfer-Encoding: binary';
                    $_add_file .= 'Content-Type: text/plain' . $eol;
                    $handle = fopen($file_path, "rb");
                    $contents = fread($handle, filesize($file_path));
                    fclose($handle);
                    $_add_file .= $contents;
                }
            }

            # Message body
            $body = '';
            $body = '--' . $boundary . $eol;
            $body .= 'Content-Disposition: form-data; name="datei"; filename="' . $_file_name_content . '"' . $eol; # file content
            $body .= 'Content-Transfer-Encoding: binary' . $eol;
            $body .= 'Content-Type: application/octet-stream' . $eol . $eol;

            $body .= 'PS.Info: ' . $val['street'] . $eol; # street name
            $body .= 'PSType=PS.Info' . $eol;
            $body .= 'Formular=AUFTRAG.HTM' . $eol . $eol;
            $body .= 'ps_auftraggeber=' . $_module . $eol;  # module name
            $body .= 'ps_sachbearbeiter=' . $_worker . $eol;    # worker name
            $body .= 'ps_email=' . $eol;
            $body .= 'ps_gps=' . $val['gps'];   # gps
            $body .= 'ps_giaufext=' . $val['street'] . $eol;
            $body .= 'ps_gimemo=' . $eol;
            $body .= 'ps_belegex=2010-0001' . $eol; # id ????
            $body .= 'ps_datum=' . date('Y.m.d',strtotime($_ontime)) . $eol;
            $body .= 'ps_anlagen=' . $_name_file . $eol; # ten file
            $body .= '--' . $boundary . $eol;
            $body .= 'Content-Disposition: form-data; name="target"' . $eol . $eol;

            $body .= 'ab/9999999/an/' . $eol;
            $body .= '--' . $boundary . $eol;
            $body .= 'Content-Disposition: form-data; name="Kunde"' . $eol . $eol;

            $body .= '9999999' . $eol;
            $body .= !empty($_add_file) ? $_add_file : '';
            $body .= '--' . $boundary . '--' . $eol . $eol;
            $this->_save_to_files(date('YmdHis',strtotime($_ontime)) . $index . '.txt',$body);

            $params = array('http' => array(
                  'method' => 'POST',
                  'header' => 'Content-Type: multipart/form-data; boundary=AaB03x' . $eol,
                  'content' => $body
                )
            );
            $ctx      = stream_context_create($params);
            $response = @file_get_contents($url, FILE_TEXT, $ctx);
            $return[] = array(
                'requested_at' => $current_time,
                'response'     => $response
            );

            // $ch = curl_init();
            // if (!is_resource($ch))  {
            //     write_log('curl_init error', 'service');
            //     return false;
            // }
            // curl_setopt( $ch , CURLOPT_SSL_VERIFYPEER , 0 );
            // curl_setopt( $ch , CURLOPT_FOLLOWLOCATION , 0 );
            // curl_setopt( $ch , CURLOPT_URL , $url );
            // curl_setopt( $ch , CURLOPT_POST , 1 );
            // curl_setopt( $ch , CURLOPT_POSTFIELDS , '@' . $file );
            // curl_setopt( $ch , CURLOPT_RETURNTRANSFER , 1 );
            // curl_setopt( $ch , CURLOPT_VERBOSE , 0 );
            // $response = curl_exec($ch);
            // curl_close($ch);
            // return $response;
        }
        return $return;
    }

    function _save_to_files($file_name,$data) {
        $path = config_item('upload_dir') . 'service/' . date('Y/m/d') . '/';
        if (!is_dir($path)) {
            @umask(0000);
            @mkdir ($path, 0755, TRUE);
        }
        $file_path = $path.$file_name;
        $this->load->helper('file');
        if (!write_file($file_path, $data)) {
            write_log('data post service');
            return false;
        }
        return true;
    }
}
