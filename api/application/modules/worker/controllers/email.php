<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Worker send mail
 *
 * @author tran.duc.chien@kloon.vn
 * @date   10 Feb 2014
 */
class Email extends MX_Controller {

    function __construct(){
        parent::__construct();
    }

    /**
     * Extract email
     * @return [type] [description]
     * @author Chien Tran <tran.duc.chien@kloon.vn>
     *
     * Beanstalk data structure : <id_email>
     */
    function extract() {
        sleep(5);

        # Pheanstalk library
        require_once(APPPATH.'libraries/pheanstalk/pheanstalk_init.php');

        # Beanstalk Init
        $pheanstalk = new Pheanstalk(getenv('QUEUE_PHEANSTALK_HOST'));

        $start = time();
        while((time() - $start) < 300) {
            $job          = $pheanstalk->watchOnly(QUE_EMAIL_EXTRACT)->ignore('default')->reserve();
            $job_data_raw = $job->getData();
            $job_data     = $job_data_raw;
            $pheanstalk->delete($job);
            $this->db->reconnect();
            write_log("Get and delete job : {$job_data}", 'worker_email_extract');

            if ($job_data) {
                write_log("Email extract {$job_data}", 'worker_email_extract');

                # id_email
                $id_email = $job_data;

                # Email template
                $this->load->config('email_template');
                $email_template = config_item('email_template_sync_notification');

                $this->db->where('status', EMAIL_STATUS_WAITING);
                $this->db->where('id', $id_email);
                $this->db->limit(1);
                $data_extract = $this->db->get(TBL_EMAIL)->result_array();

                # ectract email
                if (isset($data_extract) && !empty($data_extract)) {
                    # Email Account
                    $email_account_config = config_item('email_account');
                    $email_account        = $email_account_config['noreply'];

                    $array_extract        = array();
                    foreach ($data_extract as $extract) {
                        $data_detail = json_decode($extract['data'], true);
                        if ($data_detail) {
                            $id_city   = isset($data_detail['id_city']) ? $data_detail['id_city'] : false;
                            $id_module = isset($data_detail['module_id']) ? $data_detail['module_id'] : false;
                            $street    = isset($data_detail['street']) ? $data_detail['street'] : false;
                            $id_email  = $extract['id'];

                            # Logs
                            write_log("---> Extracting email {$id_email} _ city: {$id_city}", 'worker_email_extract');

                            # db connect
                            $db_connect = connect_db(false, $id_city);
                            if ($db_connect === false) {
                                write_log("Connect to database FAIL _ city: {$id_city}", 'worker_email_extract');
                            } else {
                                # get user
                                $_user_array = $this->db->get_where(TBL_USERS, array('id_city' => $id_city))->result_array();

                                # mail address
                                $_to_emmail = array();
                                $_cc        = array();
                                $_bcc       = array();

                                if (is_array($_user_array) && !empty($_user_array)) {
                                    foreach ($_user_array as $value) {
                                        $email       = isset($value['email']) ? $value['email'] : FALSE;
                                        if ($email) {
                                            $_to_emmail[] = $email;
                                        }

                                        $extra_email = isset($value['extra_email']) ? $value['extra_email'] : FALSE;
                                        if ($extra_email) {
                                            $extra_email = explode(';', $extra_email);
                                            $_cc         = array_merge($_cc, $extra_email);
                                        }
                                    }
                                }

                                # get module
                                $module_detail = array();
                                $module_detail = $db_connect->get_where(TBL_MODULES, array('id' => $id_module))->row_array();
                                $module_email  = '';
                                if(is_array($module_detail) AND !empty($module_detail)){
                                    $module_email = isset($module_detail['email']) ? $module_detail['email'] : '';
                                }

                                # email
                                $_to_emmail = implode(';', array_unique($_to_emmail)) . ';' . $module_email;
                                $_cc        = implode(';', array_unique($_cc));

                                $taskid = $data_detail['taskid'];

                                # Employee
                                $employeeid       = $data_detail['employeeid'];
                                $_employee_name   = '';
                                $_employee_detail = $db_connect->get_where(TBL_WORKER, array('id' => $employeeid))->row_array();
                                if (is_array($_employee_detail) && !empty($_employee_detail)) {
                                    $_employee_name = isset($_employee_detail['first_name']) ? $_employee_detail['first_name'] : '';
                                    $_employee_name .= isset($_employee_detail['last_name']) ? (' ' . $_employee_detail['last_name']) : '';
                                }

                                # Machine
                                $machineid       = $data_detail['machineid'];
                                $_machine_name   = '';
                                $_machine_detail = $db_connect->get_where(TBL_MACHINE, array('id' => $machineid))->row_array();
                                if (is_array($_machine_detail) && !empty($_machine_detail)) {
                                    $_machine_name = isset($_machine_detail['machine_code']) ? $_machine_detail['machine_code'] : '';
                                    $_machine_name .= isset($_machine_detail['name']) ? (' '.$_machine_detail['name']) : '';
                                }
                                $type       = $data_detail['type'];
                                $data       = $data_detail['data'];
                                $created_at = $data_detail['created_at'];
                                $lat        = isset($data_detail['lat']) ? $data_detail['lat'] : '';
                                $lon        = isset($data_detail['lon']) ? $data_detail['lon'] : '';
                                $url_infor  = config_item('webapp_link') . "sys/viewinfor/index/".urlencode($lat)."/".urlencode($lon);

                                # Email from
                                $from_email = $email_account['email'];
                                $from_name  = $email_account['display_name'];

                                # Email To
                                $to_email   = trim($_to_emmail,';');
                                $to_name    = $_employee_name;

                                # Email message
                                $subject = isset($email_template['subject']) ? $email_template['subject'] : '';
                                $subject = str_replace('[MODULE_NAME]', $module_detail['title'], $subject);
                                $message = isset($email_template['message']) ? $email_template['message'] : '';
                                $message = str_replace('[TIME]', $created_at, $message);
                                $message = str_replace('[WORKER]', $_employee_name, $message);
                                $message = str_replace('[STREETNAME]', $street, $message);
                                $message = str_replace('[MACHINE]', $_machine_name, $message);
                                $message = str_replace('[GPS_COONDINATE]', "{$lat} - {$lon}", $message);
                                $message = str_replace('[GPS_COONDINATE_LINK]', $url_infor, $message);

                                if ($type != INFO_MESSAGE){
                                    $attach = $data;
                                } else {
                                    $message = str_replace('[INFO_MSG]', $data, $message);
                                }
                                $message = str_replace('[INFO_MSG]', '', $message);

                                $tmp = array(
                                    'id'         => $id_email,
                                    'status'     => EMAIL_STATUS_PENDING,
                                    'from_email' => $email_account['email'],
                                    'from_name'  => $email_account['display_name'],
                                    'subject'    => $subject,
                                    'message'    => $message,
                                );

                                if (isset($to_email) AND (!empty($to_email)))  {
                                    $tmp['to_email'] = $to_email;
                                } else {
                                    write_log('Email extract error_ID: '.$id_email, 'email');
                                    $tmp['status'] = EMAIL_STATUS_ERROR;
                                }

                                if (isset($to_name) AND (!empty($to_name)))  {
                                    $tmp['to_name'] = $to_name;
                                }

                                if (isset($_cc) AND (!empty($_cc)))  {
                                    $tmp['cc'] = trim($_cc,';');
                                }

                                if (isset($_bcc) AND (!empty($_bcc)))  {
                                    $tmp['bcc'] = $_bcc;
                                }

                                if (isset($attach) AND (!empty($attach)))  {
                                    $tmp['attach'] = $attach;
                                }

                                $array_extract[] = $tmp;

                                # Logs
                                write_log(json_encode($tmp), 'worker_email_extract');
                            }
                        } else {
                            $array_extract[] = array(
                                'id'         => $id_email,
                                'from_email' => $email_account['email'],
                                'from_name'  => $email_account['display_name'],
                                'status'     => EMAIL_STATUS_PENDING
                            );
                        }
                    }

                    # update extract
                    if (isset($array_extract) && !empty($array_extract)) {
                        $this->load->helper('pheanstalk');
                        foreach ($array_extract as $key => $value) {
                            $this->db->where('id' , $value['id']);
                            $email_flag = $this->db->update(TBL_EMAIL, $value);
                            write_log('Update batch email data _ Query: '. $this->db->last_query(), 'worker_email_extract');
                            if($email_flag != FALSE){
                                write_log('OK data ' . $value['id'], 'worker_email_extract');
                                push_job(QUE_EMAIL_SEND , $value['id']);
                                write_log('push_job OK ' . $value['id'], 'worker_email_extract');
                            } else {
                                write_log('FAIL data ' . $value['id'], 'worker_email_extract');
                            }
                        }
                        /* $email_flag = $this->db->update_batch(TBL_EMAIL, $array_extract, 'id');
                        var_dump($email_flag);
                        # Logs
                        write_log('Update batch email data _ Query: '. $this->db->last_query(), 'worker_email_extract');
                        if ($email_flag !== FALSE) {
                            write_log('FAIL', 'worker_email_extract');
                            $return = array(
                                'version' => config_item('api_version'),
                                'status'  => STATUS_SUCCESS,
                                'msg'     => 'Email has been extracted'
                            );
                            return $return;
                        } else {
                            write_log('OK', 'worker_email_extract');
                            $return = array(
                                'version' => config_item('api_version'),
                                'status'  => STATUS_FAIL,
                                'msg'     => 'Has an error when extract email'
                            );
                            return $return;
                        }*/
                    }
                } else {
                    write_log('Email extract is empty', 'worker_email_extract');
                    $return = array(
                        'version' => config_item('api_version'),
                        'status'  => STATUS_FAIL,
                        'msg'     => 'Email extract is empty'
                    );
                    return $return;
                }
            } else {
                # Logs
                write_log("---> Email job data empty _ Data: {$job_data_raw}", 'worker_email_extract');
            }
        }
    }

    /**
     * Send email
     * @return [type] [description]
     * @author Chien Tran <tran.duc.chien@kloon.vn>
     *
     * Jobs structure : <id_email>
     */
    function send(){
        sleep(5);

        # Pheanstalk library
        require_once(APPPATH.'libraries/pheanstalk/pheanstalk_init.php');

        # Beanstalk Init
        $pheanstalk = new Pheanstalk(getenv('QUEUE_PHEANSTALK_HOST'));

        # Exit after x minute : fix memory leak
        $start = time();
        while((time() - $start) < 300) {
            $job          = $pheanstalk->watchOnly(QUE_EMAIL_SEND)->ignore('default')->reserve();
            $job_data_raw = $job->getData();
            // $job_data  = json_decode($job_data_raw, true);
            $job_data     = $job_data_raw;
            $pheanstalk->delete($job);
            $this->db->reconnect();

            if (!empty($job_data)) {
                # Logs
                write_log("---> Sending email {$job_data} : ", 'worker_email_send');

                # Load email library and config
                $email_config = array();
                $smtp         = new stdClass();
                if (isset($job_data['smtp_id']) AND FAIL) {
                    $smtp                        = isset($smtp_array[$job_data['smtp_id']]) ? $smtp_array[$job_data['smtp_id']] : false;
                    $email_config['protocol']    = isset($smtp->protocol)  ? $smtp->protocol  : false;
                    $email_config['smtp_host']   = isset($smtp->host)      ? $smtp->host      : false;
                    $email_config['smtp_port']   = isset($smtp->port)      ? $smtp->port      : false;
                    $email_config['smtp_crypto'] = isset($smtp->smtp_auth) ? $smtp->smtp_auth : false;
                    $email_config['smtp_user']   = isset($smtp->username)  ? $smtp->username  : false;
                    $email_config['smtp_pass']   = isset($smtp->password)  ? $smtp->password  : false;
                } else {
                    $email_config['protocol']  = 'sendmail';
                    $email_config['mailpath']  = '/usr/sbin/sendmail';
                }
                $email_config['wordwrap']  = true;
                $email_config['useragent'] = 'BIS-OFFICE System';
                $email_config['charset']   = 'utf-8';
                $email_config['mailtype']  = 'html';
                $email_config['crlf']      = '\r\r\n';
                $email_config['newline']   = '\r\r\n';

                # Email library
                $this->load->library('email', $email_config);
                $this->email->set_newline("\r\n");

                echo "-----------------------------------------------------------\n";
                print_r($email_config);
                echo '---';
                print_r($job_data);
                echo "\n";

                # Reconnect mysql
                $this->db->reconnect();
                $this->db->initialize();

                # Default email information
                $default_from_name  = '';
                $default_from_email = '';

                # Email information
                $job_data   = $this->db->get_where(TBL_EMAIL, array('id' => $job_data))->row_array();
                $from_email = isset($job_data['from_email']) ? $job_data['from_email'] : $default_from_email;
                $from_name  = isset($job_data['from_name'])  ? $job_data['from_name']  : $default_from_email;
                $to_email   = isset($job_data['to_email'])   ? $job_data['to_email']   : '';
                $to_name    = isset($job_data['to_name'])    ? $job_data['to_name']    : '';
                $cc         = isset($job_data['cc'])         ? $job_data['cc']         : '';
                $bcc        = isset($job_data['bcc'])        ? $job_data['bcc']        : '';

                // Email admins
                $email_admin = config_item('email_admin');
                if ($email_admin AND !empty($email_admin)) {
                    $bcc .= ",{$email_admin}";
                }

                $this->email->from($from_email, $from_name);
                $this->email->to($to_email);
                if ($cc AND !empty($cc)) {
                    $this->email->cc($cc);
                }
                if ($bcc AND !empty($bcc)) {
                    $this->email->bcc($bcc);
                }
                $this->email->subject($job_data['subject']);
                $this->email->message($job_data['message']);

                // Reply address
                if (isset($job_data['reply_email']) AND !empty($job_data['reply_email'])){
                    $this->email->reply_to($job_data['reply_email'], isset($job_data['from_name']) ? $job_data['from_name'] : $job_data['reply_email']);
                } else {
                    $this->email->reply_to($default_from_email, $default_from_email);
                }

                // Email attach
                $attachs = (isset($job_data['attach']) AND (!empty($job_data['attach'])) ) ? explode(';', $job_data['attach']) : false;
                if (is_array($attachs) AND (!empty($attachs))) {
                    foreach ($attachs as $key => $value) {
                        $path = get_server_path() . '/' . $value;
                        if (file_exists($path)) {
                            $this->email->attach($path);
                        }
                    }
                }

                # Send email
                $result = $this->email->send();

                if ($result){
                    write_log('OK', 'worker_email_send');

                    # Update status success
                    $this->db->where('id', $job_data['id']);
                    $db_flag = $this->db->update(
                        TBL_EMAIL,
                        array(
                            'status'      => EMAIL_STATUS_SUCCESS,
                            'from_email'  => $from_email,
                            'from_name'   => $from_name,
                            'reply_email' => $default_from_email,
                            'send_at'     => date('Y-m-d H:i:s')
                        )
                    );

                    if ($db_flag === false) {
                        write_log("Update email  {$id_email} FAIL _ Query : " . $this->db->last_query(), 'worker_email_send');
                    }
                } else {
                    write_log('ERROR', 'worker_email_send');

                    # Update status error and log error
                    $this->db->where('id', $job_data['id']);
                    $this->db->set('re_send', 're_send+1', FALSE);
                    $this->db->set('status', EMAIL_STATUS_ERROR);
                    $this->db->set('from_email', $default_from_email);
                    $this->db->set('reply_email', empty($job_data['reply_email']) ? $default_from_email : $job_data['reply_email']);
                    write_log('Send mail FAIL _ Email ID : '.$job_data['id'].' _ Message:'.json_encode($this->email->print_debugger()).' _ email_config: '.json_encode($email_config).' _ Job data : '.json_encode($job_data), 'worker_email_send');
                    $db_flag = $this->db->update(TBL_EMAIL);
                    if ($db_flag === false) {
                        write_log("Update email  {$id_email} FAIL _ Query : " . $this->db->last_query(), 'worker_email_send');
                    }
                }

                # Debug
                echo $this->email->print_debugger();
            } else {
                # Logs
                write_log("---> Email job data empty _ Data: " . $job_data_raw, 'worker_email_send');
            }
        }
    }
}
