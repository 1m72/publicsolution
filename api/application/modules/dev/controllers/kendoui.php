<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

/**
 * Kendo UI Test controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 12 Nov 2013
 */
# Get
define('GET_CITY'               , 0);
define('GET_COMPANY'            , 1);
define('GET_DEVICE'             , 2);
define('GET_MACHINE'            , 3);
define('GET_OBJECT'             , 4);
define('GET_USERS'              , 5);
define('GET_WORKER'             , 6);
define('GET_MACHINE_ADDON'      , 7);
define('GET_ACTIVITY'      		, 8);

class Kendoui extends REST_Controller {
    function __construct() {
        parent::__construct();
    }

    public function index_get() {
        $tenantId =  $this->_tenantId;
        if(isset($tenantId) && !empty($tenantId)){
            $this->_process();
        }else{
            echo null;
        }
    }

    public function index_post() {
        $tenantId =  $this->_tenantId;
        if(isset($tenantId) && !empty($tenantId)){
            $this->_process();
        }else{
            echo null;
        }
    }

    public function index_put() {
        $tenantId =  $this->_tenantId;
        if(isset($tenantId) && !empty($tenantId)){
            $this->_process();
        }else{
            echo null;
        }
    }

    public function _process() {
        require_once APPPATH . 'libraries/kendoui/DataSourceResult.php';

        # Convert request array to object
        $key     = $this->post('key');
        $request = json_decode(json_encode($this->post()));

        # Connection
        if (in_array($key, array(GET_USERS, GET_CITY))) {
            $result = $this->_init_DataSourceResult($this->_db_global);
        } else {
            $result = $this->_init_DataSourceResult();
        }

        # Results
        switch ($key)
        {
            case GET_CITY:
                $data = $result->read(
                        VIEW_CITY,
                        array(
                                VIEW_CITY.'.id' => array('type' => 'number'),
                                VIEW_CITY.'.name',
                                VIEW_CITY.'.passcode',
                                VIEW_CITY.'.database as city_database',
                                VIEW_CITY.'.storage as city_storage',
                                VIEW_CITY.'.deleted_at',
                                VIEW_CITY.'.created_at',
                                VIEW_CITY.'.updated_at',
                        ),
                        $request
                );
                break;

            case GET_COMPANY:
				$data = $result->read(
                        VIEW_COMPANY,
                        array(
                                VIEW_COMPANY.'.id' => array('type' => 'number'),
                                VIEW_COMPANY.'.id_city' => array('type' => 'number'),
                                VIEW_COMPANY.'.id_user' => array('type' => 'number'),
                                VIEW_COMPANY.'.name',
                                VIEW_COMPANY.'.description',
                                VIEW_COMPANY.'.deleted_at',
                                VIEW_COMPANY.'.created_at',
                                VIEW_COMPANY.'.updated_at',
                        ),
                        $request
                );
                break;
            case GET_DEVICE:
                $data = $result->read(
                        VIEW_DEVICES,
                        array(
                                VIEW_DEVICES.'.id' => array('type' => 'number'),
                                VIEW_DEVICES.'.id_device',
                                VIEW_DEVICES.'.id_city' => array('type' => 'number'),
                                VIEW_DEVICES.'.id_unique',
                                VIEW_DEVICES.'.actived' => array('type' => 'number'),
                                VIEW_DEVICES.'.deleted_at',
                                VIEW_DEVICES.'.created_at',
                                VIEW_DEVICES.'.updated_at',
                        ),
                        $request
                );
                break;

            case GET_MACHINE:
                $data = $result->read(
                        VIEW_MACHINE,
                        array(
                                VIEW_MACHINE.'.id' => array('type' => 'number'),
                                VIEW_MACHINE.'.id_ref',
                                VIEW_MACHINE.'.id_city' => array('type' => 'number'),
                                VIEW_MACHINE.'.nfc_code',
                                VIEW_MACHINE.'.name',
                                VIEW_MACHINE.'.description',
                                VIEW_MACHINE.'.deleted_at',
                                VIEW_MACHINE.'.created_at',
                                VIEW_MACHINE.'.updated_at',
                        ),
                        $request
                );
                break;

            case GET_OBJECT:
                $data = $result->read(
                        VIEW_OBJECT,
                        array(
                            'id'    => array('type' => 'number'),
                            'id_ref',
                            'id_city' => array('type' => 'number'),
                            'id_group' => array('type' => 'number'),
                            'obj_name',
                            'place_name',
                            'position',
                            'deleted_at',
                            'created_at',
                            'updated_at'
                        ),
                        $request
                );
                break;

            /**
             * @author  : Chien Tran <tran.duc.chien@kloon.vn>
             * @created : 19 Nov 2013
             */
            case GET_USERS:
                $data = $result->read(
                    VIEW_USERS,
                    array(
                        'id'            => array('type' => 'number'),
                        'id_city'       => array('type' => 'number'),
                        'id_company'    => array('type' => 'number'),
                        'id_user_group' => array('type' => 'number'), //edit by Le Thi Nhung (2013-11-28)
                        'group_name',
                        'username',
                        'email',
                        'salt',
                        'deleted_at',
                        'updated_at',
                        'created_at'
                    ),
                    $request
                );
                break;

            case GET_MACHINE_ADDON:
                $data = $result->read(
                    VIEW_MACHINE_ADDON,
                    array(
                        'id'		=> array('type' => 'number'),
                        'id_ref',
                        'name',
                        'deleted_at',
                        'updated_at',
                        'created_at'
                    ),
                    $request
                );
                break;

            case GET_ACTIVITY:
               	$data = $result->read(
                VIEW_ACTIVITY,
                array(
                    'id'=> array('type' => 'number'),
                    'id_ref',
                    'id_city'=> array('type' => 'number'),
                    'name',
                    'color',
                    'deleted_at',
                    'updated_at',
                    'created_at'
                	),
                	$request
            	);
                	break;

            case GET_WORKER:
                $data = $result->read(
                    VIEW_WORKER_JOIN,
                    array(
                        'id' => array('type' => 'number'),
                        'id_ref',
                        'id_city' => array('type' => 'number'),
                        'id_company' => array('type' => 'number'),
                        'id_teamleader' => array('type' => 'number'),
                        'nfc_code',
                        'personal_code',
                        'first_name',
                        'last_name',
                        'address',
                        'phone',
                        'birthday',
                        'deleted_at',
                        'created_at',
                        'updated_at',
                        'name',
                        'description'
                    ),
                    $request
                );
                break;

            default:
                $data = array();
                break;
        }
        echo json_encode($data);
    }

    function _init_DataSourceResult($db_connection = NULL) {
        if (is_null($db_connection)) {
            $db_connection = $this->db;
        }

        # Database config
        $db_host = $db_connection->hostname;
        $db_host_tmp = explode(':', $db_host);
        $db_host = isset($db_host_tmp[0]) ? $db_host_tmp[0] : 'localhost';
        $db_port = $db_connection->port;
        $db_user = $db_connection->username;
        $db_pass = $db_connection->password;
        $db_name = $db_connection->dbprefix.$db_connection->database;
        $result = new DataSourceResult("mysql:host={$db_host};dbname={$db_name}", $db_user, $db_pass);
        return $result;
    }
}