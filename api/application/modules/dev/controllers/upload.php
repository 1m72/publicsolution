<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Cache Test controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 30 Oct 2013
 */
class Upload extends CI_Controller {

    function upload_image_record(){
        $this->load->helper('url');
        $this->load->helper('form');
        echo form_open_multipart('upload_image_record?id_city=1');
        echo '<div>'.form_input('task_id', 1).'</div>';
        echo '<div>'.form_input('employee_id', 2).'</div>';
        echo '<div>'.form_input('machine_id', 3).'</div>';
        echo '<div>'.form_upload('picture1').'</div>';
        echo '<div>'.form_upload('picture2').'</div>';
        echo '<div>'.form_upload('picture3').'</div>';
        echo '<div>'.form_upload('picture4').'</div>';
        echo '<div>'.form_upload('voice1').'</div>';
        echo '<div>'.form_upload('voice2').'</div>';
        echo '<div>'.form_upload('video1').'</div>';
        echo '<div>'.form_upload('video2').'</div>';
        echo form_submit('btn_save', 'Upload');
        echo form_close();
    }

    function _upload_image_record() {
        # Save file
        $config['upload_path']     = 'uploads/';
        $config['encrypt_name']    = true;
        $config['allowed_types'] = 'gif|jpg|png|mp3|wma|wav|txt|html';
        /*$config['max_size']        = '100';
        $config['max_width']       = '1024';
        $config['max_height']      = '768';*/

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('picture')) {
            echo 'Error';
            print_r($this->upload->display_errors('<p>', '</p>'));
        } else {
            echo 'Saved files';
            print_r($this->upload->data());
        }

        # Log request
        $db = $this->db->insert(
            'z_up',
            array (
                'data' => json_encode(array(
                        'get'     => $_GET,
                        'post'    => $_POST,
                        'files'   => $_FILES,
                        'request' => $_REQUEST
                )),
                'created' => date('Y-m-d H:i:s')
            )
        );

        echo $db ? 'request has been logs' : 'has an error when log request';

        echo '<pre>';
        print_r($_REQUEST);
        echo '</pre>';
    }

    function upload_fast_task_binary(){
        $this->load->helper('url');
        $this->load->helper('form');
        echo form_open_multipart('upload_fast_task_binary?id_city=2');
        echo '<div>'.form_input('task_id', 1).'</div>';
        echo '<div>'.form_input('employee_id', 2).'</div>';
        echo '<div>'.form_input('machine_id', 3).'</div>';
        echo '<div>'.form_upload('picture1').'</div>';
        echo '<div>'.form_upload('picture2').'</div>';
        echo '<div>'.form_upload('picture3').'</div>';
        echo '<div>'.form_upload('picture4').'</div>';
        echo '<div>'.form_upload('voice1').'</div>';
        echo '<div>'.form_upload('voice2').'</div>';
        echo '<div>'.form_upload('video1').'</div>';
        echo '<div>'.form_upload('video2').'</div>';
        echo form_submit('btn_save', 'Upload');
        echo form_close();
    }
}
?>