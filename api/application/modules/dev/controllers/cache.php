<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Cache Test controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 30 Oct 2013
 */
class Cache extends CI_Controller {
    function object() {
        $data = $this->db->get_where('z_test', array('id' => 1));
        var_dump($data);
        return;
        $data = unserialize($data);
        print_r($data);
    }

    function memcached_save() {
        # Cache driver
        $this->load->driver('cache');
        if (!$this->cache->memcached->is_supported()) {
            echo 'Unsupport memcached';
        }
        var_dump($this->cache->memcached->save('chien', 'abc'));
    }

    function memcached_get() {
        # Cache driver
        $this->load->driver('cache');
        if (!$this->cache->memcached->is_supported()) {
            echo 'Unsupport memcached';
        }
        var_dump($this->cache->memcached->get('chien'));
    }
}