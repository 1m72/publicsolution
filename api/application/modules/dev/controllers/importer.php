<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Import Data controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 30 Oct 2013
 */
define('TBL_IMPORT', '_old_data');
define('TBL_OBJECT_FAIL', 'object_fail');
class Importer extends CI_Controller {
    private $_id_city = 1;

    function __construct() {
        parent::__construct();
        $this->output->enable_profiler(TRUE);
    }

    function extract() {
        $this->db->where('flag', FALSE);
        $this->db->from(TBL_IMPORT);
        $result = $this->db->get()->result_array();

        $task         = '';
        $object_group = '';
        $batch_data   = array();
        foreach ($result as $key => $value) {
            if (!empty($value['task'])) {
                $task = $value['task'];
            } else {
                $value['task'] = $task;
                $value['flag'] = TRUE;
            }

            if (!empty($value['object_group'])) {
                $object_group = $value['object_group'];
            } else {
                $value['object_group'] = $object_group;
                $value['flag'] = TRUE;
            }

            $batch_data[] = $value;
        }

        $this->db->update_batch(TBL_IMPORT, $batch_data, 'id');
    }

    function update_group() {
        $this->db->select('task, object_group');
        $result = $this->db->get(TBL_IMPORT)->result_array();
        $data = array();
        foreach ($result as $key => $value) {
            if (!empty($value['object_group'])) {
                $data[$value['task']][] = $value['object_group'];
                $data[$value['task']] = array_unique($data[$value['task']]);
            }
        }

        $this->db->trans_start();
        foreach ($data as $key => $value) {
            # Group parents
            $this->db->insert(
                TBL_OBJECT_GROUP,
                array(
                    'id_city'    => $this->_id_city,
                    'id_parent'  => 0,
                    'name'       => $key,
                    'created_at' => date('Y-m-d H:i:s')
                )
            );
            $insert_id = $this->db->insert_id();

            # Group childs
            if (is_array($value) AND (!empty($value))) {
                $patch = array();
                foreach ($value as $sub_key => $sub_value) {
                    $patch[] = array(
                        'id_city'    => $this->_id_city,
                        'id_parent'  => $insert_id,
                        'name'       => $sub_value,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                }
                $this->db->insert_batch(TBL_OBJECT_GROUP, $patch);
            }
        }
        $this->db->trans_complete();
    }

    function update_object() {
        $groups = array();
        $result = $this->db->get(TBL_OBJECT_GROUP)->result_array();
        foreach ($result as $key => $value) {
            $groups[$value['name']] = $value['id'];
        }

        $batch_data = array();
        $batch_fail = array();
        $result     = $this->db->get(TBL_IMPORT)->result_array();
        foreach ($result as $key => $value) {
            if (isset($groups[$value['object_group']]) AND (!empty($value['object']))) {
                $batch_data[] = array(
                    'id_city'    => $this->_id_city,
                    'id_group'   => $groups[$value['object_group']],
                    'name'       => $value['object'],
                    'created_at' => date('Y-m-d H:i:s')
                );
            } else {
                $batch_fail[] = array(
                    'id_city'    => $this->_id_city,
                    'id_group'   => 0,
                    'name'       => $value['object'],
                    'created_at' => date('Y-m-d H:i:s')
                );
            }
        }

        $this->db->insert_batch(TBL_OBJECT, $batch_data);
        $this->db->insert_batch(TBL_OBJECT_FAIL, $batch_fail);
    }
}