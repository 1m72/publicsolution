<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Test controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 25 Oct 2013
 */
class Test extends CI_Controller {

    function test_array() {
        $array = array(
            array(
                'data' => 'hello'
            ),
            array(
                'data' => 'goodbye'
            ),
        );

        var_dump(in_array(array('data' => 'hello'), $array));
    }

    function clean() {
        $db_config = array(
            'hostname' => 'localhost',
            'username' => 'root',
            'password' => 'vertrigo',
            'database' => 'publicsolution_pfinztal',
            'dbprefix' => '',
            'pconnect' => TRUE,
            'dbdriver' => 'mysql',
            'db_debug' => TRUE,
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'autoinit' => TRUE,
            'stricton' => FALSE,
        );

        $db_connect = $this->load->database($db_config, TRUE);
        $result = $db_connect->get(TBL_WORKER_ACTIVITY, array('id_wkgps' => 540))->result_array();

        $this->db->where('id_wkgps', 549);
        $result = $query = $db_connect->get('worker_activity')->result();
        $batch = array();
        foreach ($result as $key => $value) {
            $tmp = $value->street;
            $tmp = str_replace("\n", "", $tmp);
            $tmp = str_replace("\r", "", $tmp);
            $batch[] = array(
                'id'     => $value->id,
                'street' => $tmp,
            );
        }
        print_r($batch);
        $db_connect->update_batch(TBL_WORKER_ACTIVITY, $batch, 'id');
        $db_connect->last_query();
    }

    function storage() {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->load->library('storage');
            $storage_config = array(
                'server_type' => 'local',
                'hostname'    => '',
                'username'    => '',
                'password'    => ''
            );
            $this->storage->init($storage_config);
            $this->storage->upload($_FILES['file_test']);
            echo PHP_EOL.'<hr />End';
        } else {
            $this->load->helper('form');
            echo form_open_multipart();
            echo form_upload('file_test');
            echo form_submit('submit', 'Upload');
            echo form_close();
        }
    }

    function storage_ftp_upload() {
    	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    		$this->load->library('storage');
    		$storage_config = array(
    				'server_type' => 'ftp',
    				'hostname'    => '127.0.0.1',
    				'username'    => 'huong1',
    				'password'    => '123qaz'
    		);
    		$this->storage->init($storage_config);
    		$this->storage->upload($_FILES['file_test']);
    		echo PHP_EOL.'<hr />End';
    	} else {
    		$this->load->helper('form');
    		echo form_open_multipart();
    		echo form_upload('file_test');
    		echo form_submit('submit', 'Upload');
    		echo form_close();
    	}
    }

    function storage_ftp_remove() {
    		$this->load->library('storage');
    		$storage_config = array(
    				'server_type' => 'ftp',
    				'hostname'    => '127.0.0.1',
    				'username'    => 'huong1',
    				'password'    => '123qaz'
    		);
    		$this->storage->init($storage_config);
    		$this->storage->remove_file('huong.png');
    }

    function storage_ftp_rename() {
    	$this->load->library('storage');
    	$storage_config = array(
    			'server_type' => 'ftp',
    			'hostname'    => '127.0.0.1',
    			'username'    => 'huong1',
    			'password'    => '123qaz'
    	);
    	$this->storage->init($storage_config);
    	$this->storage->rename('huong.png','huong1.png');
    }

    function index(){
        $this->load->config('rest');
    }

	public function _post_sync() {
        /*$filename = "e:\\www\\publicsolution\\photo.jpg";
        $handle   = fopen($filename, "rb");
        $contents = fread($handle, filesize($filename));
        fclose($handle);*/

        $filename = "e:\\www\\publicsolution\\photo.jpg";
        $this->load->helper('file');
        $content = read_file($filename);

        # Demo data
        $content = '/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAUDBBAQDw4QEBAPDw0PDxAPEA8QEA8QDxANDhAODxEPEw4NDxAQDQ4ODw8PDxUMDhERFBMTDw0WGBYSGBASFBIBBQUFCAcIDwkJDx0UEhUfFxUXFxQfFBQYHBwXFRUUHBQXFRQUFBQUFxQXFBQUFBQUFBQUFBQWFBQUFBQUFBUUFP/AABEIAGAAYAMBIgACEQEDEQH/xAAdAAAABwEBAQAAAAAAAAAAAAACAwQFBgcIAAEJ/8QAQBAAAQIDBAUGDQMDBQAAAAAAAQIDAAQRBRIhMQYiQVFxB2GBkrHRCBMUIzJCUlNicpGhwUPh8DNjgiRUc7LS/8QAGwEAAQUBAQAAAAAAAAAAAAAAAgEDBAUGAAf/xAArEQABBAAFAwIGAwAAAAAAAAABAAIDEQQSITFBBRNRIrEGYXGB0fAVI5H/2gAMAwEAAhEDEQA/AKZtK3Hr6qOuZn11b+MJk269713rq74NnWhfXh6x7YIKE7oyudVluQlW48B/Vd66u+AItx/3rvXV3x1wRylgYUghIl1XJt5/3rvXV3wa3aswf1Xeurvgpp0boUMub8IQylcGm90EWk/713rq74Gm0n/eu9dXfHrqt0FpWYHuuRZR5Tgief8Aeu9dXfC2bXMJS2pS3QHE30a6sUhRTXPeDHug1kLmn2mEZrVifZbGKlnmSmvTSLV8JWzUtCRuAJQltbIHwoulPTt41iRHh5Hwvm4Ffv2TgDaVUsWi771zrq74MYtV7a45T51d8NrDm2FBmIhF5XZR5RM01VxQGZUR01h6luTuYWakJbGeucafKmpiPWoaKXj6x7YcNGdMnmcErvI9hdSmnNtTxBibhDAH/wBwNfJNkqQJ5Ljtd6qCe0jKAP8AJcTW68Kj2kav1B/ETvQ7TJiY1TRp0+qoi6o/CvafhNDxiWO2bs6aZZ/iNXD07AzMzRix9T+UOqoG1uTucZR4wsqcZ96z5xIpneCdZB+ZIiKBz+bY17oPaamHDiQheqrcT6qujL6xJNJNEpCcr4+XaUs/qIHi3OuihPTWIU/w+TZiP2KIrDpeMeh0xo7Sfwa0GplJkjb4t8V6A6gf9kxG9B+RGZE4hM23clm/OLWCFIdCTghKk+0fSrQhNYp3dMna8MLd+eELmkqbeDXoR4hjyhwUmJlIKQc25eurhnVw6x5rsJfC2ZHk0oRhSYWN2JRU/eLxlmMqUpnzU2DgObdFMeGA6PFSTe0rcc6EpCe0xocdEzD4Ixj5e6dA9KzY2DBjOIgBNK7o6ScqN0Y4tsJtBtNOsr5j2wkVC2aVrqr7R7TBBA2QtFG5qJAi2uQLSSbdmmZPB5pVa363mW0iqlheJokYXVVqSBhWKnuV20jXfglaB+TSiptwefmwLlc0SwNUjmLh1zzXYsOl9zvDIa8pAyypZa2iqU5EEc4pt/EQ+YnlNOFJrgcOByP0iwtIJ3OIbb8hfAUPST9Sn9o3zMzW2uIBSuyrcOEOYtBaljOgwG0Hfwr+IjmjtnkqpkBj+0SOZAbxpVXEgfaHA4HdDlKksiRSqiAAKmuA5zwjJvhC6XCZnnAn+kwkMt9GKlf5KP0Ai5tLNIVBtxxRohtClXRgMBXpqaZxkx6eKipahVSyVE86jWMt8QPytazzr/iPNojVqBgbaRshMlxPCFDLophGUSAhJ7QeopXzHthG5N4x5aVb66+0e0wQsw7k8pRfKnPI9owJ2bQhw3JZvzswrcykjVG9TqqIAG8nZGu7b5SEJAQ00bqQEipCQEgUFEiuAGyKL5FLIDUmF/qTBKyfhSSEJ+lVcTElclSd5xricuAHZxjZ9I6aGRB7tzqhc+hQTzOaYuKJ1UDmxJiVaCtOuVWugTkAB94iGjVi3ljcDicjSuPRlFz2JLBKQBhFzM4xt1KGNuY2mUSaGyUDMCvQe7dshgt6Yzh60lV59WzBI6aRHLdSRmOnZ+0LG0loJ5TjqVQ8vduXJYNg4vLA/wAE6yvqaCKJcdyixfCAZeLyFFCvEJQEoXmkrJqoGnonIUOdIrRto7oxXXHF2JN8aBABpaWBNcIGmX2QmbSRjChL+2KUtKWgkdrK1zT2j2wW7t4QpnwL6hT1j2wEsBR3Q4Smw8WtMaAI/wBHKj+winVywh+8l3CpNObLf0ZQw8lSr0lLbgi7X5FEftDxpTb6JZhbzmQwCK4qc9VAptOZOwVj0qCVjcO152oey7LZTXpLykMSDrTblbzibxuitxJOBUM6KNacCYsvQ7Txl9IU24laeY9ozHTGF9L7TcfdW8s1ccNTuA2JA2JSMAISSU+61VTK1tOAVCkmmWOIyI5iIzf87nkOZvpO3kKQxpsNC39az15y8MQQNu7AwublQoAKoa838+8ZY5N+XRTakJm0lbZACnEDWFfWubeAjUOiFuszDaVsOodSR6igSOZQ9JJG0ECLqLHxSsHbO3HKQA5kVNaOtrSUKQlTawbyVCoUMsQe3AxRXKTyEqTedkarRmZcmqh/xqPpj+2rHcTGmA2frzbPzxgtxFM6CnQKb8cMoj4uCPEtqQfQ8hHlXzznGlXikggg0UCCCFDYQcQRuMCTLYcI1lyzaO2VMAqemWJaaAweQtBWfnbST40DgFbjGVZ0JStSQ4HEpJAcSFBKwMlALAUAdxEZDF4QwGrBCAit032u9rqoKm8cuJhGH8DvhXNyi/GKNCNY9sFvS5KssYj+m6QiJt7q/vB5nx5CbxADTrlVKySggOVJ2ClYqzlY02M29q1Eu3UNJ373CPaX9hSGdm2Hky70snVbeWlaztIQKXB8KsCd9BDQiVUcKYRbYjqRkw7IAdANUtAHRBv1oSMIWltNxdM7iuyEokzkIPlZFVHPkPaBFPIABun4H1KEjcaGGzCPZKfUhQUhS21e0hSkH6pIMHrZVlhWObksRXZuhwSgcqOZaCelaYTv+8mqbvHOf+obJ60H3cXH3liuSnFq7VQU80aQUUKArTCC7zjyjzBFqaoa5QNDtabYNS4DgR0RyWU7AYDP5SB1lf/Z';

        $xml_data = '
            <?xml version="1.0" encoding="UTF-8"?>
            <results>
                <row>
                    <employeeid>1</employeeid>
                    <machineid>1</machineid>
                    <activities>
                        <activity>
                            <activityid>1</activityid>
                            <locations>
                                <location>
                                    <lat>12.345</lat>
                                    <lon>15.123</lon>
                                    <time>2013-10-20 13:12:00</time>
                                </location>
                            </locations>
                            <starttime>2013-10-20 13:12:00</starttime>
                            <endtime>2013-10-20 18:12:00</endtime>
                        </activity>
                    </activities>
                    <materials>
                        <material>
                            <materialid>1</materialid>
                            <materialconsumption>sfsdf</materialconsumption>
                        </material>
                    </materials>
                    <information>
                        <messages>
                            <message>test massage</message>
                        </messages>
                        <images>
                            <image>'.base64_encode($content).'</image>
                        </images>
                    </information>
                </row>
            </results>';

        #echo urlencode($content);die;
        #echo $xml_data;die;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.publicsolution.dev/v1/synchronize/update_winter_service_task");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, "xml=".$xml_data );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        var_dump($output);
        curl_close($ch);
	}

    function test2(){
        $data = 'iVBORw0KGgoAAAANSUhEUgAAABwAAAASCAMAAAB/2U7WAAAABl'
               . 'BMVEUAAAD///+l2Z/dAAAASUlEQVR4XqWQUQoAIAxC2/0vXZDr'
               . 'EX4IJTRkb7lobNUStXsB0jIXIAMSsQnWlsV+wULF4Avk9fLq2r'
               . '8a5HSE35Q3eO2XP1A1wQkZSgETvDtKdQAAAABJRU5ErkJggg==';
        $data = base64_decode($data);

        $im = imagecreatefromstring($data);
        if ($im !== false) {
            header('Content-Type: image/png');
            imagepng($im);
            imagedestroy($im);
        }
        else {
            echo 'An error occurred.';
        }
    }

    function _save_img()
    {
    	$content = '/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAUDBBAQDw4QEBAPDw0PDxAPEA8QEA8QDxANDhAODxEPEw4NDxAQDQ4ODw8PDxUMDhERFBMTDw0WGBYSGBASFBIBBQUFCAcIDwkJDx0UEhUfFxUXFxQfFBQYHBwXFRUUHBQXFRQUFBQUFxQXFBQUFBQUFBQUFBQWFBQUFBQUFBUUFP/AABEIAGAAYAMBIgACEQEDEQH/xAAdAAAABwEBAQAAAAAAAAAAAAACAwQFBgcIAAEJ/8QAQBAAAQIDBAUGDQMDBQAAAAAAAQIDAAQRBRIhMQYiQVFxB2GBkrHRCBMUIzJCUlNicpGhwUPh8DNjgiRUc7LS/8QAGwEAAQUBAQAAAAAAAAAAAAAAAgEDBAUGAAf/xAArEQABBAAFAwIGAwAAAAAAAAABAAIDEQQSITFBBRNRIrEGYXGB0fAVI5H/2gAMAwEAAhEDEQA/AKZtK3Hr6qOuZn11b+MJk269713rq74NnWhfXh6x7YIKE7oyudVluQlW48B/Vd66u+AItx/3rvXV3x1wRylgYUghIl1XJt5/3rvXV3wa3aswf1Xeurvgpp0boUMub8IQylcGm90EWk/713rq74Gm0n/eu9dXfHrqt0FpWYHuuRZR5Tgief8Aeu9dXfC2bXMJS2pS3QHE30a6sUhRTXPeDHug1kLmn2mEZrVifZbGKlnmSmvTSLV8JWzUtCRuAJQltbIHwoulPTt41iRHh5Hwvm4Ffv2TgDaVUsWi771zrq74MYtV7a45T51d8NrDm2FBmIhF5XZR5RM01VxQGZUR01h6luTuYWakJbGeucafKmpiPWoaKXj6x7YcNGdMnmcErvI9hdSmnNtTxBibhDAH/wBwNfJNkqQJ5Ljtd6qCe0jKAP8AJcTW68Kj2kav1B/ETvQ7TJiY1TRp0+qoi6o/CvafhNDxiWO2bs6aZZ/iNXD07AzMzRix9T+UOqoG1uTucZR4wsqcZ96z5xIpneCdZB+ZIiKBz+bY17oPaamHDiQheqrcT6qujL6xJNJNEpCcr4+XaUs/qIHi3OuihPTWIU/w+TZiP2KIrDpeMeh0xo7Sfwa0GplJkjb4t8V6A6gf9kxG9B+RGZE4hM23clm/OLWCFIdCTghKk+0fSrQhNYp3dMna8MLd+eELmkqbeDXoR4hjyhwUmJlIKQc25eurhnVw6x5rsJfC2ZHk0oRhSYWN2JRU/eLxlmMqUpnzU2DgObdFMeGA6PFSTe0rcc6EpCe0xocdEzD4Ixj5e6dA9KzY2DBjOIgBNK7o6ScqN0Y4tsJtBtNOsr5j2wkVC2aVrqr7R7TBBA2QtFG5qJAi2uQLSSbdmmZPB5pVa363mW0iqlheJokYXVVqSBhWKnuV20jXfglaB+TSiptwefmwLlc0SwNUjmLh1zzXYsOl9zvDIa8pAyypZa2iqU5EEc4pt/EQ+YnlNOFJrgcOByP0iwtIJ3OIbb8hfAUPST9Sn9o3zMzW2uIBSuyrcOEOYtBaljOgwG0Hfwr+IjmjtnkqpkBj+0SOZAbxpVXEgfaHA4HdDlKksiRSqiAAKmuA5zwjJvhC6XCZnnAn+kwkMt9GKlf5KP0Ai5tLNIVBtxxRohtClXRgMBXpqaZxkx6eKipahVSyVE86jWMt8QPytazzr/iPNojVqBgbaRshMlxPCFDLophGUSAhJ7QeopXzHthG5N4x5aVb66+0e0wQsw7k8pRfKnPI9owJ2bQhw3JZvzswrcykjVG9TqqIAG8nZGu7b5SEJAQ00bqQEipCQEgUFEiuAGyKL5FLIDUmF/qTBKyfhSSEJ+lVcTElclSd5xricuAHZxjZ9I6aGRB7tzqhc+hQTzOaYuKJ1UDmxJiVaCtOuVWugTkAB94iGjVi3ljcDicjSuPRlFz2JLBKQBhFzM4xt1KGNuY2mUSaGyUDMCvQe7dshgt6Yzh60lV59WzBI6aRHLdSRmOnZ+0LG0loJ5TjqVQ8vduXJYNg4vLA/wAE6yvqaCKJcdyixfCAZeLyFFCvEJQEoXmkrJqoGnonIUOdIrRto7oxXXHF2JN8aBABpaWBNcIGmX2QmbSRjChL+2KUtKWgkdrK1zT2j2wW7t4QpnwL6hT1j2wEsBR3Q4Smw8WtMaAI/wBHKj+winVywh+8l3CpNObLf0ZQw8lSr0lLbgi7X5FEftDxpTb6JZhbzmQwCK4qc9VAptOZOwVj0qCVjcO152oey7LZTXpLykMSDrTblbzibxuitxJOBUM6KNacCYsvQ7Txl9IU24laeY9ozHTGF9L7TcfdW8s1ccNTuA2JA2JSMAISSU+61VTK1tOAVCkmmWOIyI5iIzf87nkOZvpO3kKQxpsNC39az15y8MQQNu7AwublQoAKoa838+8ZY5N+XRTakJm0lbZACnEDWFfWubeAjUOiFuszDaVsOodSR6igSOZQ9JJG0ECLqLHxSsHbO3HKQA5kVNaOtrSUKQlTawbyVCoUMsQe3AxRXKTyEqTedkarRmZcmqh/xqPpj+2rHcTGmA2frzbPzxgtxFM6CnQKb8cMoj4uCPEtqQfQ8hHlXzznGlXikggg0UCCCFDYQcQRuMCTLYcI1lyzaO2VMAqemWJaaAweQtBWfnbST40DgFbjGVZ0JStSQ4HEpJAcSFBKwMlALAUAdxEZDF4QwGrBCAit032u9rqoKm8cuJhGH8DvhXNyi/GKNCNY9sFvS5KssYj+m6QiJt7q/vB5nx5CbxADTrlVKySggOVJ2ClYqzlY02M29q1Eu3UNJ373CPaX9hSGdm2Hky70snVbeWlaztIQKXB8KsCd9BDQiVUcKYRbYjqRkw7IAdANUtAHRBv1oSMIWltNxdM7iuyEokzkIPlZFVHPkPaBFPIABun4H1KEjcaGGzCPZKfUhQUhS21e0hSkH6pIMHrZVlhWObksRXZuhwSgcqOZaCelaYTv+8mqbvHOf+obJ60H3cXH3liuSnFq7VQU80aQUUKArTCC7zjyjzBFqaoa5QNDtabYNS4DgR0RyWU7AYDP5SB1lf/Z';

    	$content = base64_decode($content);

    	$im = imagecreatefromstring($content);
    	if ($im !== false) {
    		$fileName = "F:\\test.png";
    		//setting alpha blending on
    		imagealphablending($im, false);
    		//save alphablending setting (important)
    		imagesavealpha($im, true);
    		//Generate image and print it
    		$resp = imagepng($im, $fileName);
    	}
    	else {
    		echo 'An error occurred.';
    	}
    }

    function trans() {
        $this->output->enable_profiler(true);
        $this->db->trans_start();
        $this->db->insert(
            'test',
            array(
                'created_at' => date('Y-m-d H:i:s')
            )
        );

        $this->db->insert(
            'test',
            array(
                'data'       => $this->db->insert_id(),
                'created_at' => date('Y-m-d H:i:s')
            )
        );
        $this->db->trans_complete();
    }

    function get() {
        if ( function_exists('getallheaders') AND is_callable('getallheaders') ) {
            print_r(getallheaders());
        }
    }

    function get_mime(){
        $this->load->helper('file');
        $file = 'audiorecordtest.3gp';
        var_dump(get_mime_by_extension($file));
        echo 'dd';
    }

    function db() {
        $this->db->where('deleted_at IS NOT NULL');
        $this->db->get(TBL_MACHINE)->result();
        echo $this->db->last_query();
    }

    /**
     * Test merge activity vs information
     * @return [type] [description]
     */
    function activity_information() {
        $id = 445;
        $connect_info = $this->db->select('database')->get_where(TBL_CITY_CONFIG, array('id' => 3))->row();
        $connect_info = isset($connect_info->database) ? json_decode($connect_info->database, true) : array();
        $db_connect = NULL;
        if ( is_array($connect_info) AND !empty($connect_info) ) {
            $db_config = array(
                'hostname' => isset($connect_info['hostname']) ? $connect_info['hostname'] : '',
                'username' => isset($connect_info['username']) ? $connect_info['username'] : '',
                'password' => isset($connect_info['password']) ? $connect_info['password'] : '',
                'database' => isset($connect_info['database']) ? $connect_info['database'] : '',
                'dbprefix' => isset($connect_info['dbprefix']) ? $connect_info['dbprefix'] : config_item('db_prefix'),
                'port'     => isset($connect_info['port'])     ? $connect_info['port']     : config_item('db_port'),
                'dbdriver' => config_item('db_dbdriver'),
                'pconnect' => config_item('db_pconnect'),
                'db_debug' => config_item('db_db_debug'),
                'cache_on' => config_item('db_cache_on'),
                'cachedir' => config_item('db_cachedir'),
                'char_set' => config_item('db_char_set'),
                'dbcollat' => config_item('db_dbcollat'),
            );

            # Get DB instance
            $db_connect = $this->load->database($db_config, true);

            $db_connect->where('id_wkgps', $id);
            $activities = $db_connect->get_where(TBL_WORKER_ACTIVITY)->result();

            $infor      = $db_connect->get_where(TBL_WORKER_INFORMATION, array('id_worker_gps' => $id))->result();
            // echo $db_connect->last_query();
            // _debug($activities);
            // _debug($infor);
            // die;

            $data = array();
            foreach ($activities as $key => $value) {
                $time = strtotime($value->endtime);
                if ($time) {
                    $data[$time] = array(
                        'type'  => 'activity',
                        'value' =>  $value
                    );
                }
            }
            foreach ($infor as $key => $value) {
                $time = strtotime($value->time);
                if ($time) {
                    $data[$time] = array(
                        'type'  => 'infor',
                        'value' =>  $value
                    );
                }
            }
            _debug($data);
        }
    }
}