<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Email Test controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 20 Mar 2014
 */
class Email extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    function index() {
        # Load email library and config
        $email_config              = array();
        $email_config['protocol']  = 'sendmail';
        $email_config['mailpath']  = '/usr/sbin/sendmail';
        $email_config['wordwrap']  = true;
        $email_config['useragent'] = 'BIS-OFFICE System';
        $email_config['charset']   = 'utf-8';
        $email_config['mailtype']  = 'html';
        $email_config['crlf']      = '\r\r\n';
        $email_config['newline']   = '\r\r\n';
        $this->load->library('email', $email_config);
        $this->email->set_newline("\r\n");
        $this->email->from('noreply@bis-office.com', 'Bis-Office');
        $this->email->to('br.tdchien@gmail.com');
        $this->email->cc('tran.duc.chien@kloon.vn');
        $this->email->bcc('br.tdchien@gmail.com');

        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');
        $this->email->send();

        echo $this->email->print_debugger();
    }
}