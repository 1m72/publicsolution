<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * author  	: huongpm(phung.manh.huong@kloon.vn)
 * @created : 30 Oct 2013
 * */
class Storageftp extends CI_Controller{
	function __construct()
	{
		parent:: __construct();
	}
	
	function ftp_upload()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$this->load->library('storage');
			$storage_config = array(
					'server_type' => 'ftp',
					'hostname'    => '127.0.0.1',
					'username'    => 'huong1',
					'password'    => '123qaz'
			);
			$this->storage->init($storage_config);
			$this->storage->upload($_FILES['file_test']);
			echo PHP_EOL.'<hr />End';
		} else {
			$this->load->helper('form');
			echo form_open_multipart();
			echo form_upload('file_test');
			echo form_submit('submit', 'Upload');
			echo form_close();
		}
	}
	
	function ftp_remove()
	{
		$this->load->library('storage');
		$storage_config = array(
				'server_type' => 'ftp',
				'hostname'    => '127.0.0.1',
				'username'    => 'huong1',
				'password'    => '123qaz'
		);
		$this->storage->init($storage_config);
		$this->storage->remove_file('sftp.php');
	}
	
	function ftp_rename()
	{
		$this->load->library('storage');
		$storage_config = array(
				'server_type' => 'ftp',
				'hostname'    => '127.0.0.1',
				'username'    => 'huong1',
				'password'    => '123qaz'
		);
		$this->storage->init($storage_config);
		$this->storage->rename('huong.png','huong1.png');
	}
	
	function sftp_upload()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$this->load->library('storage');
			$storage_config = array(
					'server_type' => 'sftp',
					'hostname'    => 'publicsolution@c',
					'username'    => '',
					'password'    => '123456'
			);
			$this->storage->init($storage_config);
			$msg = $this->storage->upload($_FILES['file_test']);
			echo PHP_EOL.'<hr />End';
		} else {
			$this->load->helper('form');
			echo form_open_multipart();
			echo form_upload('file_test');
			echo form_submit('submit', 'Upload');
			echo form_close();
		}
	}
	
	function test()
	{
		$this->load->library('session');
		# config
		$db_ = array(
				'hostname' => '192.168.18.189',
				'username' => 'publicsolution',
				'password' => '123456',
				'database' => 'publicsolution',
				'dbprefix' => '',
				'pconnect' => TRUE,
				#'port'    => 3306,
				'dbdriver' => 'mysql',
				'db_debug' => TRUE,
				'cache_on' => FALSE,
				'cachedir' => '',
				'char_set' => 'utf8',
				'dbcollat' => 'utf8_general_ci',
				'swap_pre' => '',
				'autoinit' => TRUE,
				'stricton' => FALSE
		);
		# add database config => session
		$this->session->set_userdata('db_config', $db_);
		//$this->session->set_userdata('db_config', null);
		# get infomation database connect
		$db_config = $this->session->userdata("db_config");
		# load new database
		if(isset($db_config) && !empty($db_config))
			$this->_db = $this->load->database($db_config,true);
		else
			$this->_db = $this->db;
		# query
		$data = $this->_db->get(TBL_ACTIVITIES)->result_array();
		# echo
		$data2 = $this->db->get(TBL_ACTIVITIES)->result_array();
		var_dump($data);
		echo '<br><br>';
		var_dump($data2);
	}
}
?>