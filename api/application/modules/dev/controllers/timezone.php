<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Timezone controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 21 feb 2013
 */
class Timezone extends MX_Controller {

    function convert_main_db() {
        $str = '0000-00-00 00:00:00';
        $str = '1990-00-00 00:00:00';

        $list_tables = array(
            'city'        => array('created_at', 'updated_at', 'deleted_at'),
            'city_config' => array('created_at', 'updated_at', 'deleted_at'),
            'cron'        => array('created_at', 'updated_at', 'last_run'),
            'email'       => array('created_at', 'send_at'),
            'nfc'         => array('created_at'),
            'user_group'  => array('created_at', 'updated_at', 'deleted_at'),
            'users'       => array('created_at', 'updated_at', 'deleted_at'),
        );

        # Set default timezone
        $msg = 'Current timezone: ' . date_default_timezone_get() . ' : ' . date('Y-m-d H:i:s') . '<hr />';
        write_log($msg, 'convert_datetime');

        date_default_timezone_set('Europe/Berlin');
        $msg = 'Switch to timezone: ' . date_default_timezone_get() . ' : ' . date('Y-m-d H:i:s') . '<hr />';
        write_log($msg, 'convert_datetime');

        # DB Forge
        $this->load->dbforge();

        # Check field converted
        foreach ($list_tables as $table => $fields) {
            if (!$this->db->field_exists('datetime_converted', $table)) {
                $converted_field = array(
                    'datetime_converted' => array(
                        'type'       => 'TINYINT',
                        'constraint' => '1',
                        'default'    => '0'
                    ),
                );
                $db_flag = $this->dbforge->add_column($table, $converted_field);
                write_log('Add field '.$table.'.datetime_converted ' . (($db_flag === false) ? 'OK' : 'FAIL'), 'convert_datetime');
            } else {
                # $this->db->update($table, array('datetime_converted' => false));
            }
        }

        # Helper
        $this->load->helper('date');
        foreach ($list_tables as $table => $fields) {
            $limit   = 500;
            $flag    = true;
            while($flag) {
                $result = $this->db->limit($limit)->get_where($table, array('datetime_converted' => false))->result();
                if (count($result) == 0) {
                    $flag = false;
                } else {
                    $data = array();

                    // Alter datetime field
                    foreach ($fields as $field) {
                        $sql = 'ALTER TABLE `'.$table.'` CHANGE `'.$field.'` `'.$field.'` DATETIME NULL DEFAULT NULL;';
                        $db_flag = $this->db->query($sql);
                        $msg = "{$sql}" . ($db_flag === false ? 'FAIL' : 'OK');
                        write_log($msg, 'convert_datetime');
                    }

                    foreach ($result as $key => $value) {
                        if (!empty($fields)) {
                            $item = array();
                            $item['id']        = $value->id;
                            $item['datetime_converted'] = true;
                            foreach ($fields as $field) {
                                $tmp = strtotime($value->{$field});
                                if ( is_null($value->{$field}) OR $tmp) {
                                    $item[$field] = date('Y-m-d H:i:s', local_to_gmt($tmp) );
                                }
                            }
                            $data[] = $item;

                            write_log("{$table} [{$value->id}]", 'convert_datetime');
                        }
                    }
                    $db_flag = $this->db->update_batch($table, $data, 'id');
                    $msg = "Update batch [{$table}] - ".count($result) ." records : ". (($db_flag === false) ? 'FAIL' : 'OK' );
                    write_log($msg, 'convert_datetime');
                }
            }
        }
    }

    function convert_child_db() {
        $list_city = $this->db->get_where(TBL_CITY, array('deleted_at' => null))->result();
        // print_r($list_city);die;
        // Backup connection
        $this->_db_global = $this->db;

        if (is_array($list_city) AND !empty($list_city)) {
            foreach ($list_city as $key_city => $value_city) {
                write_log('Start convert datetime city '. $value_city->id, 'convert_datetime');

                // Connection database
                // $this->db = connect_db(null, $value_city->id);
                // $result = $this->db->get(TBL_WORKER_GPS, 10)->result();
                // print_r($result);
                // die;
                // print_r($this->db); die;

                $list_tables = array(
                    'activities'              => array('deleted_at', 'created_at', 'updated_at'),
                    'addon'                   => array('deleted_at', 'created_at', 'updated_at'),
                    'addon_module'            => array('deleted_at', 'created_at'),
                    'addon_module_activity'   => array('deleted_at', 'created_at'),
                    'company'                 => array('deleted_at', 'created_at', 'updated_at'),
                    'devices'                 => array('deleted_at', 'created_at', 'updated_at'),
                    'employee_activity'       => array('deleted_at', 'created_at'),
                    'fasttask'                => array('deleted_at', 'created_at', 'updated_at'),
                    'machine'                 => array('deleted_at', 'created_at', 'updated_at'),
                    'machine_activity'        => array('deleted_at', 'created_at'),
                    'machine_addon_module'    => array('deleted_at', 'created_at'),
                    'machine_material'        => array('deleted_at', 'created_at'),
                    'machine_material_used'   => array('created_at'),
                    'machine_module'          => array('deleted_at', 'created_at'),
                    'machine_module_activity' => array('deleted_at', 'created_at'),
                    'material'                => array('deleted_at', 'created_at', 'updated_at'),
                    'modules'                 => array('deleted_at', 'created_at', 'updated_at'),
                    'nfc'                     => array('deleted_at', 'created_at', 'updated_at'),
                    'object'                  => array('deleted_at', 'created_at', 'updated_at'),
                    'object_place'            => array('deleted_at', 'created_at', 'updated_at'),
                    'worker'                  => array('deleted_at', 'created_at', 'updated_at'),
                    'worker_activity'         => array('deleted_at', 'created_at', 'updated_at', 'starttime', 'endtime'),
                    'worker_activity_saved'   => array('deleted_at', 'created_at'),
                    'worker_gps'              => array('deleted_at', 'created_at', 'updated_at', 'upload_flag', 'ontime'),
                    'worker_gps_split'        => array(),
                    'worker_information'      => array('deleted_at', 'created_at', 'updated_at', 'time'),
                    'worker_module'           => array('deleted_at', 'created_at'),
                );

                # Set default timezone
                $msg = 'Current timezone: ' . date_default_timezone_get() . ' : ' . date('Y-m-d H:i:s') . '<hr />';
                write_log($msg, 'convert_datetime');

                date_default_timezone_set('Europe/Berlin');
                $msg = 'Switch to timezone: ' . date_default_timezone_get() . ' : ' . date('Y-m-d H:i:s') . '<hr />';
                write_log($msg, 'convert_datetime');

                # DB Forge
                $this->load->dbforge();
                $this->dbforge->set_database($this->db);

                # Check field converted
                foreach ($list_tables as $table => $fields) {
                    if (!$this->db->field_exists('datetime_converted', $table)) {
                        $converted_field = array(
                            'datetime_converted' => array(
                                'type'       => 'TINYINT',
                                'constraint' => '1',
                                'default'    => '0'
                            ),
                        );
                        $db_flag = $this->dbforge->add_column($table, $converted_field);
                        write_log('Add field '.$table.'.datetime_converted ' . (($db_flag === false) ? 'OK' : 'FAIL'), 'convert_datetime');
                    } else {
                        # $this->db->update($table, array('datetime_converted' => false));
                    }
                }

                # Helper
                $this->load->helper('date');
                foreach ($list_tables as $table => $fields) {
                    $limit   = 500;
                    $flag    = true;
                    while($flag) {
                        $result = $this->db->limit($limit)->get_where($table, array('datetime_converted' => false))->result();
                        if (count($result) == 0) {
                            $flag = false;
                        } else {
                            $data = array();

                            // Alter datetime field
                            foreach ($fields as $field) {
                                $sql = 'ALTER TABLE `'.$table.'` CHANGE `'.$field.'` `'.$field.'` DATETIME NULL DEFAULT NULL;';
                                $db_flag = $this->db->query($sql);
                                $msg = "{$sql}" . ($db_flag === false ? 'FAIL' : 'OK');
                                write_log($msg, 'convert_datetime');
                            }

                            foreach ($result as $key => $value) {
                                if (!empty($fields)) {
                                    $item = array();
                                    $item['id']        = $value->id;
                                    $item['datetime_converted'] = true;
                                    foreach ($fields as $field) {
                                        $tmp = strtotime($value->{$field});
                                        if ( is_null($value->{$field}) OR $tmp) {
                                            $item[$field] = date('Y-m-d H:i:s', local_to_gmt($tmp) );
                                        }
                                    }
                                    $data[] = $item;

                                    write_log("{$table} [{$value->id}]", 'convert_datetime');
                                }
                            }
                            $db_flag = $this->db->update_batch($table, $data, 'id');
                            $msg = "Update batch [{$table}] - ".count($result) ." records : ". (($db_flag === false) ? 'FAIL' : 'OK' );
                            write_log($msg, 'convert_datetime');
                        }
                    }
                }
            }
        }
    }
}
