<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Test controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 25 Oct 2013
 */
class Synchronize extends CI_Controller {
    private $_wkgps = 725;

    function post_xml() {
        $data = '<?xml version="1.0" encoding="UTF-8"?> <results> <row> <taskid>1</taskid> <employeeid>3</employeeid> <machineid>1</machineid> <activities> <activity> <taskactivityid>1a</taskactivityid> <activityid>1</activityid> <locations> <location> <lat>12.345</lat> <lon>15.123</lon> <time>2013-10-20 13:12:00</time> </location> </locations> <starttime>2013-10-20 13:12:00</starttime> <endtime>2013-10-20 18:12:00</endtime> </activity> </activities> <materials> <material> <materialid>1</materialid> <materialconsumption>Salt</materialconsumption> </material> <material> <materialid>2</materialid> <materialconsumption>Stone</materialconsumption> </material> </materials> <information> <messages> <message>test massage 1</message> <message>test massage 2</message> </messages> </information> </row> </results>';
        # Rest client
        $this->load->library('rest');
        $this->rest->initialize(array('server' => base_url()));
        $api_param = array(
            'xml' => $data
        );
        $api_result = $this->rest->post('mobile/synchronize/update_winter_service_task', $api_param, 'json');
        $api_status = $this->rest->status();
        $this->rest->debug();
    }

    function clean_street($id_city = null, $id_wkgps = null) {
        if ( is_null($id_city) OR is_null($id_wkgps) ) {
            die('id_city or id_wkgps can not null');
        }

        $db = connect_db(false, $id_city);

        # Set max lenght group_concat function
        $db->query('SET SESSION group_concat_max_len = 1000000000');

        $db->select('
            id,
            id_city,
            id_wkgps,
            id_activity,
            id_task,
            id_task_activity,
            street,
            street_old,
            group_street,
            min(`starttime`) as `start_time`,
            max(`starttime`) as `end_time`,
            (max(`starttime`) - min(`starttime`) ) as duration,
            GROUP_CONCAT(id) as id_range
        ', FALSE);
        $db->where('deleted_at', NULL);
        $db->group_by(array('id_wkgps', 'id_activity', 'id_task_activity', 'street', 'group_street'));
        $db->order_by('id', 'asc');
        $result = $db->get_where('test', array('id_wkgps' => $id_wkgps))->result_array();

        if ( !is_array($result) OR empty($result) ) {
            die('List activities are empty');
            return false;
        }

        # Mark by duration
        foreach ($result as $key => $value) {
            $result[$key]['flag_duration'] = '';
            if ( ($value['duration'] < 10) AND ( isset($result[$key-1]) AND ($result[$key]['id_activity'] == $result[$key-1]['id_activity']) ) ) {
                $result[$key]['flag_duration'] = 'x';
            }
        }

        # Mark by street name sequence
        foreach ($result as $key => $value) {
            if (!isset($result[$key]['flag_1'])) {
                $result[$key]['flag_1'] = '';
            }
            if ($value['flag_duration'] == 'x') {
                $backward1 = isset($result[$key - 1]) ? $result[$key - 1] : false;
                $forward1  = isset($result[$key + 1]) ? $result[$key + 1] : false;
                if ( $backward1 AND $forward1 AND ($backward1['street'] == $forward1['street']) AND 1) {
                    $result[$key]['flag_1']           = 'x';
                    $result[$key]['street']           = $backward1['street'];
                    $result[$key]['street_old']       = $value['street'];
                    $result[$key]['group_street']     = $backward1['group_street'];

                    $result[$key + 1]['flag_1']       = 'x';
                    $result[$key + 1]['street']       = $backward1['street'];
                    $result[$key + 1]['street_old']   = $result[$key + 1]['street'];
                    $result[$key + 1]['group_street'] = $backward1['group_street'];

                }
            }
        }

        $batch_update = array();
        foreach ($result as $key => $value) {
            if ($value['flag_1'] == 'x') {
                $list_id = trim($value['id_range']);
                $list_id = explode(',', $value['id_range']);
                foreach ($list_id as $sub_key => $sub_value) {
                    $batch_update[] = array(
                        'id'           => $sub_value,
                        'street'       => $value['street'],
                        'street_old'   => $value['street_old'],
                        'group_street' => $value['group_street'],
                    );
                }
            }
        }

        if (!empty($batch_update)) {
            $db_flag = $db->update_batch('test', $batch_update, 'id');
            echo $db->last_query();
            die;
        }

        $this->load->library('table');
        $this->table->set_heading(
            'id',
            'id_city',
            'id_wkgps',
            'id_activity',
            'id_task',
            'id_task_activity',
            'street',
            'street_old',
            'group_street',
            'start_time',
            'end_time',
            'duration',
            'id_range',
            'flag_duration',
            'flag_1'
        );
        $tmpl = array ( 'table_open'  => '<table border="1" cellpadding="5" cellspacing="0" class="mytable">' );
        $this->table->set_template($tmpl);
        echo $this->table->generate($result);
    }

    function tmp_update_street_name($id_city = null, $id_wkgps = null) {
        if (is_null($id_city) OR is_null($id_wkgps)) {
            die('Param is required');
        }

        $db_connect = connect_db(false, $id_city);
        if ($db_connect === false) {
            $msg = 'Connect to mysql fail';
            write_log($msg, 'tmp_update_street_name');
            die($msg);
        }
        $result     = $db_connect->limit(1000)->get_where(TBL_WORKER_ACTIVITY, array('id_wkgps' => $id_wkgps))->result();
        $batch_data = array();
        if (is_array($result) AND !empty($result)) {
            $total = count($result);
            $counter = 0;
            foreach ($result as $key => $value) {
                $counter++;
                $latitude    = $value->latitude;
                $longtitude  = $value->longtitude;
                $street_name = $this->resolve_street_name($latitude, $longtitude);
                $batch_data[] = array(
                    'id'         => $value->id,
                    'street'     => $street_name,
                    'street_old' => '',
                    'updated_at' => date('Y-m-d H:i:s')
                );

                echo "---> {$counter} / {$total} : {$latitude} {$longtitude} {$street_name}\n" . PHP_EOL;
            }
        }

        if (is_array($batch_data) AND !empty($batch_data)) {
            $db_flag = $db_connect->update_batch(TBL_WORKER_ACTIVITY, $batch_data, 'id');
            $db_flag = $db_flag === false ? 'FAIL' : 'OK';
            echo "$db_flag\n" . PHP_EOL;
        }
    }

    function resolve_street_name($latitude, $longtitude, $service = 'kloon') {
        $services = array(
            'kloon'         => 'http://nominatim.kloon.net/reverse?format=json&zoom=17&addressdetails=1',
            'gisgraphy'     => 'http://services.gisgraphy.com/street/streetsearch?format=json&from=1&to=1',
            'openstreetmap' => 'http://nominatim.openstreetmap.org/reverse?format=json&zoom=18&addressdetails=1',
            'mapquest'      => 'http://open.mapquestapi.com/nominatim/v1/reverse.php?format=json',
        );

        if (!isset($services[$service])) {
            $service = $services['openstreetmap'];
        } else {
            $service = $services[$service];
        }

        $street = '';
        if ((isset($latitude) && !empty($latitude)) && (isset($longtitude) && !empty($longtitude))) {
            $street_data = '';
            try {
                $street_data = file_get_contents($service.'&lat='.$latitude.'&lon='.$longtitude);
            } catch (Exception $e) {}

            if(isset($street_data) && !empty($street_data)){
                $street_array = json_decode($street_data,true);
                if (isset($street_array['address']) && !empty($street_array['address'])) {
                    $street_detail = $street_array['address'];
                    if(isset($street_detail['road']) && !empty($street_detail['road'])){
                        $street_name   = isset($street_detail['road']) ? $street_detail['road'] : '';
                        $street = $street_name;
                    }
                }
            }
        }
        return $street;
    }

    function clean_point_position(/*$id_city = null, $id_wkgps = null, $db_connect = null*/){
        /*$pheanstalk = new Pheanstalk('127.0.0.1');
        $start = time();
        $job        = $pheanstalk->watchOnly(QUE_CLEAN_POINT)->ignore('default')->reserve();
        $job_data   = json_decode($job->getData(), true);
        $pheanstalk->delete($job);*/
        $job_data = array(
            'id_city' => 1,
            'id_wkgps' => $this->_wkgps
        );

        if(!isset($job_data) || empty($job_data)){
            write_log('Data invalid ', 'clean_point_position');
            return false;
        }
        $id_city    = isset($job_data['id_city'])   ? $job_data['id_city']  : FALSE;
        $id_wkgps   = isset($job_data['id_wkgps'])  ? $job_data['id_wkgps'] : FALSE;

        if ( !isset($id_city) OR empty($id_city) ) {
            write_log('id_city invalid ', 'clean_point_position');
            return false;
        }

        if ( !isset($id_wkgps) OR empty($id_wkgps) ) {
            write_log('id_wkgps invalid ', 'clean_point_position');
            return false;
        }

        // $db_connect = connect_db(false, $id_city);
        $db_connect = $this->db;

        # get position
        $gps_result = array();
        $db_connect->where('id_wkgps', $id_wkgps);
        $gps_result = $db_connect->get(TBL_WORKER_ACTIVITY)->result_array();
        if(!is_array($gps_result) OR empty($gps_result)){
            write_log("Data GPS empty", 'clean_point_position');
            die;
        }
        $line_position = '';
        $old_lat        = false;
        $old_lon        = false;
        $old_street     = false;
        $old_activiti   = false;
        foreach ($gps_result as $key => $value){
            if($key < 1){
                $old_lat        = $value['latitude'];
                $old_lon        = $value['longtitude'];
                $old_street     = $value['street'];
                $old_activiti   = $value['id_activity'];
                $line_position = $old_lat . ' ' . $old_lon;
                continue;
            }
            $lat            = $value['latitude'];
            $lon            = $value['longtitude'];
            $street         = $value['street'];
            $activiti       = $value['id_activity'];

            if(($street == $old_street) AND ($activiti == $old_activiti)){
                $distance = $this->distance_gps($old_lat,$old_lon,$lat,$lon);
                if($distance > 10){
                    $old_lat        = $lat;
                    $old_lon        = $lon;
                    $old_street     = $street;
                    $old_activiti   = $activiti;
                    $line_position  = $line_position . ',' . $old_lat . ' ' . $old_lon;
                }
            } else {
                $old_lat        = $lat;
                $old_lon        = $lon;
                $old_street     = $street;
                $old_activiti   = $activiti;
                $line_position  = $line_position . ',' . $old_lat . ' ' . $old_lon;
            }
        }
        if(isset($line_position) AND !empty($line_position)){
            $db_connect->where('id', $id_wkgps);
            $flag = $db_connect->update(TBL_WORKER_GPS,array('position_clean' => $line_position));
            if($flag != FALSE){
                write_log("Update GPS OK - id : " . $id_wkgps, 'clean_point_position');
            } else {
                write_log("Update GPS FAIL - id : " . $id_wkgps, 'clean_point_position');
            }
        } else {
            write_log("Data position empty - id : " . $id_wkgps, 'clean_point_position');
        }

        # Close db connection
        $db_connect->close();
    }

    function clean_unnecessary_streets($id_city = null, $id_wkgps = null, $db_connect = null) {
        /*if ( is_null($id_city) OR is_null($id_wkgps) ) {
            die('id_city or id_wkgps can not null');
        }

        if (is_null($db_connect)) {
            $db_connect = connect_db(false, $id_city);
        }*/

        $id_wkgps = $this->_wkgps;
        $id_city = 1;
        $db_connect = $this->db;

        # Logs
        write_log("Start clean_unnecessary_streets _ id_city: {$id_city} _ id_wkgps: {$id_wkgps}", 'clean_unnecessary_streets');

        # Set max lenght group_concat function
        $db_connect->query('SET SESSION group_concat_max_len = 1000000000');

        $db_connect->select('id, id_city, id_wkgps, id_activity, id_task, id_task_activity, street, street_old, group_street, min(`starttime`) as `start_time`, max(`starttime`) as `end_time`, (max(`starttime`) - min(`starttime`) ) as duration, GROUP_CONCAT(id) as id_range ', false);
        $db_connect->where('deleted_at', NULL);
        $db_connect->group_by(array('id_wkgps', 'id_activity', 'id_task_activity', 'street', 'group_street'));
        $db_connect->order_by('id', 'asc');
        $result = $db_connect->get_where('test', array('id_wkgps' => $id_wkgps))->result_array();
        echo $db_connect->last_query();
        _debug($result);
        die;

        if ( !is_array($result) OR empty($result) ) {
            $msg = 'List activities are empty';

            # Logs
            write_log($msg, 'clean_unnecessary_streets');
            return false;
        }

        # Mark by duration
        foreach ($result as $key => $value) {
            $result[$key]['flag_duration'] = '';
            if ( ($value['duration'] < 10) AND ( isset($result[$key-1]) AND ($result[$key]['id_activity'] == $result[$key-1]['id_activity']) ) ) {
                $result[$key]['flag_duration'] = 'x';
            }
        }

        # Mark by street name sequence
        foreach ($result as $key => $value) {
            if (!isset($result[$key]['flag_1'])) {
                $result[$key]['flag_1'] = '';
            }
            if ($value['flag_duration'] == 'x') {
                $backward1 = isset($result[$key - 1]) ? $result[$key - 1] : false;
                $forward1  = isset($result[$key + 1]) ? $result[$key + 1] : false;
                if ( $backward1 AND $forward1 AND ($backward1['street'] == $forward1['street']) AND 1) {
                    $result[$key]['flag_1']           = 'x';
                    $result[$key]['street']           = $backward1['street'];
                    $result[$key]['street_old']       = $value['street'];
                    $result[$key]['group_street']     = $backward1['group_street'];

                    $result[$key + 1]['flag_1']       = 'x';
                    $result[$key + 1]['street']       = $backward1['street'];
                    $result[$key + 1]['street_old']   = $result[$key + 1]['street'];
                    $result[$key + 1]['group_street'] = $backward1['group_street'];
                }
            }
        }

        # Batch update street, street_old, group_street
        $batch_update = array();
        $counter = 0;
        $total = count($result);
        foreach ($result as $key => $value) {
            $counter++;
            if ($value['flag_1'] == 'x') {
                $list_id = trim($value['id_range']);
                $list_id = explode(',', $value['id_range']);
                foreach ($list_id as $sub_key => $sub_value) {
                    $output  = "---> {$counter} / {$total}\n".PHP_EOL;
                    $batch_update[] = array(
                        'id'           => $sub_value,
                        'street'       => $value['street'],
                        'street_old'   => $value['street_old'],
                        'group_street' => $value['group_street'],
                        'updated_at'   => date('Y-m-d H:i:s')
                    );

                    $output .= "-> id: {$sub_value}\n".PHP_EOL;
                    $output .= "-> street: {$value['street']}\n".PHP_EOL;
                    $output .= "-> street_old: {$value['street_old']}\n".PHP_EOL;
                    $output .= "-> group_street: {$value['group_street']}\n".PHP_EOL;
                    echo "$output\n\n".PHP_EOL;
                }
            }
        }

        if (!empty($batch_update)) {
            $output  = "---> Batch update : \n".PHP_EOL;
            $db_flag = $db_connect->update_batch('test', $batch_update, 'id');
            $db_flag = (($db_flag === false) ? 'FAIL' : 'OK');
            echo $db_flag . "\n" . PHP_EOL;

            write_log("End clean_unnecessary_streets {$db_flag}", 'clean_unnecessary_streets');
        } else {
            $this->load->library('table');
            $this->table->set_heading(
                'id',
                'id_city',
                'id_wkgps',
                'id_activity',
                'id_task',
                'id_task_activity',
                'street',
                'street_old',
                'group_street',
                'start_time',
                'end_time',
                'duration',
                'id_range',
                'flag_duration',
                'flag_1'
            );
            $tmpl = array ( 'table_open'  => '<table border="1" cellpadding="5" cellspacing="0" class="mytable">' );
            $this->table->set_template($tmpl);
            echo $this->table->generate($result);
        }
    }

    function clean_unnecessary_streets_v2() {
        /*if ( is_null($id_city) OR is_null($id_wkgps) ) {
            die('id_city or id_wkgps can not null');
        }

        if (is_null($db_connect)) {
            $db_connect = connect_db(false, $id_city);
        }*/

        $id_city = 1;
        $id_wkgps = $this->_wkgps;
        $db_connect = $this->db;

        # Logs
        write_log("Start clean_unnecessary_streets2 _ id_city: {$id_city} _ id_wkgps: {$id_wkgps}", 'clean_unnecessary_streets_v2');

        # get activiti group
        $activiti_result = array();

        # Set max lenght group_concat function
        $db_connect->query('SET SESSION group_concat_max_len = 1000000000');

        $db_connect->select("group_concat(wac.id) as id_group, group_concat(wac.street_old) as street_name_old,wac.id_wkgps,wac.id_activity,min(wac.starttime) as starttime,max(wac.endtime) as endtime, (max(wac.endtime) - min(wac.starttime)) as duration,wac.street,wac.group_street");
        $db_connect->from(TBL_WORKER_ACTIVITY.' as wac');
        $db_connect->where('wac.deleted_at',NULL);
        $db_connect->where('wac.id_wkgps',$id_wkgps);
        $db_connect->order_by('wac.id','asc');
        $db_connect->group_by('wac.id_wkgps,wac.id_activity,wac.id_task_activity,wac.street,wac.group_street');
        $activiti_result = $db_connect->get()->result_array();
        $group_street = NULL;
        $is_first     = FALSE;
        $count        = count($activiti_result);
        $limit_time   = 3;
        for ($i=0; $i < $count; $i++) {
            if($activiti_result[$i]['duration'] < $limit_time){
                if($i < ($count - 1)){
                    $check_duration = FALSE;
                    for ($j=($i + 1); $j < $count; $j++) {
                        if ($activiti_result[$j]['duration'] > ($limit_time - 1) ) {
                            for ($k = $i; $k < $j; $k++) {
                                $activiti_result[$k]['street_old']   = $activiti_result[$k]['street'];
                                $activiti_result[$k]['street']       = $activiti_result[$j]['street'];
                                $activiti_result[$k]['group_street'] = $activiti_result[$j]['group_street'];
                                $activiti_result[$k]['flag']         = 'x';
                            }
                            $check_duration = TRUE;
                            $i = $j;
                            break;
                        }
                    }
                    if($check_duration != TRUE){
                        if($i > 0){
                            for ($k = $i; $k < $count; $k++) {
                                $activiti_result[$k]['street_old']   = $activiti_result[$k]['street'];
                                $activiti_result[$k]['street']       = $activiti_result[$i - 1]['street'];
                                $activiti_result[$k]['group_street'] = $activiti_result[$i - 1]['group_street'];
                                $activiti_result[$k]['flag']         = 'x';
                            }
                        } else {
                            $db_connect->where('id_wkgps', $id_wkgps);
                            $flag = $db_connect->update(TBL_WORKER_ACTIVITY, array('deleted_at' => date('Y-m-d H:i:s')));
                        }
                    }
                } else {
                    $activiti_result[$i]['street_old']   = $activiti_result[$i]['street'];
                    $activiti_result[$i]['street']       = $activiti_result[$i - 1]['street'];
                    $activiti_result[$i]['group_street'] = $activiti_result[$i - 1]['group_street'];
                    $activiti_result[$i]['flag']         = 'x';
                }
            }
        }

        for($i = 0; $i < count($activiti_result); $i++) {
            if($i > 0){
                if ( (strtolower(trim($activiti_result[$i]['street'])) == strtolower(trim($activiti_result[$i - 1]['street']))) OR (trim($activiti_result[$i]['street']) == '') ){
                    $activiti_result[$i]['group_street'] = $activiti_result[$i - 1]['group_street'];
                    $activiti_result[$i]['flag']         = 'x';
                }
            }
        }

        $update_array = array();
        foreach ($activiti_result as $key => $value) {
            $flag = isset($value['flag']) ? TRUE : FALSE;
            if($flag){
                $id_group           = $value['id_group'];
                $id_array           = explode(',', $id_group);
                $street_name_old    = $value['street_name_old'];
                $street_array       = explode(',', $street_name_old);
                foreach ($id_array as $id_key => $id_value) {
                    if (intval($id_value) > 0) {
                        $_street_old    = isset($value['street_old']) ? $value['street_old'] : $value['street'];
                        $update_array[] = array(
                            'id'            => $id_value,
                            'street'        => $value['street'],
                            'street_old'    => (isset($street_array[$id_key]) ? $street_array[$id_key] : '').'-'.$_street_old,
                            'group_street'  => $value['group_street'],
                            'updated_at'    => date('Y-m-d H:i:s')
                        );
                        write_log("ID: {$id_value} - Street: {$value['street']}", 'clean_unnecessary_streets_v2');
                    } else {
                        write_log("ID: {$id_value} - ID invalid: {$value['street']}", 'clean_unnecessary_streets_v2');
                    }
                }
            }
        }
        if(is_array($update_array) AND !empty($update_array)){
            $flag = $db_connect->update_batch(TBL_WORKER_ACTIVITY,$update_array,'id');
            if ($flag === FALSE) {
                write_log("FAIL", 'clean_unnecessary_streets_v2');
            } else {
                write_log("OK", 'clean_unnecessary_streets_v2');
                // $this->load->helper('pheanstalk');
                // $job = array(
                //     'id_city'  => $id_city,
                //     'id_wkgps' => $id_wkgps
                // );
                // push_job(QUE_CLEAN_POINT, json_encode($job));
                // write_log('Add queue ' . QUE_CLEAN_POINT . ' success data : ' .json_encode($job), 'clean_unnecessary_streets_v2');
            }
        }else {
            write_log("OK - Data update invalid", 'clean_unnecessary_streets_v2');
        }
    }

    function group_street($id_city = null, $id_wkgps = null, $db_connect = null) {
        /*if (is_null($id_city)) {
            return 'Requried id_city';
        }

        $db_connect = connect_db(false, $id_city);
        if (!$db_connect) {
            print_r(array(
                'status' => false,
                'msg' => 'Connect fail'
            ));
            die;
        }*/

        $id_wkgps = $this->_wkgps;
        $db_connect = $this->db;
        // _debug($db_connect);
        // die;

        /*if (is_null($id_wkgps)) {
            $db_connect->select('id_wkgps');
            $db_connect->where('group_street', 0);
            $data = $db_connect->get('test')->row();
            if (isset($data->id_wkgps)) {
                $id_wkgps = $data->id_wkgps;
            } else {
                return 'id_wkgps is required';
            }
        }
        echo "---> id_wkgps : ".$id_wkgps;*/

        # Activites
        $db_connect->where('deleted_at', NULL, false);
        $db_connect->where('id_wkgps', $id_wkgps);
        $db_connect->order_by('id', 'asc');
        $activities =$db_connect->get(TBL_WORKER_ACTIVITY)->result_array();
        // _debug($activities);
        // die;

        if (is_array($activities) AND !empty($activities)) {
            echo '------------------------------------------' . PHP_EOL;
            $batch_data = array();
            if (is_array($activities) AND !empty($activities)) {
                $counter = 1;
                $last_street = '';
                foreach ($activities as $key => $value) {
                    $current_street = trim($value['street']);
                    if (empty($last_street)) {
                        $last_street = $current_street;
                    }
                    $street_checker = array();
                    if (is_array($value) AND !empty($value)) {
                        if ($last_street != $current_street) {
                            $counter++;
                        }
                        $last_street = $current_street;
                        $batch_data[] = array(
                            'id'           => $value['id'],
                            'group_street' => $counter
                        );
                    }
                }

                echo '---> Batch data :' . PHP_EOL;
                print_r($batch_data);
                if (is_array($batch_data) AND !empty($batch_data)) {
                    echo '---> Update batch worker_activity : ' . PHP_EOL;
                    $db_flag = $db_connect->update_batch(TBL_WORKER_ACTIVITY, $batch_data, 'id');
                    echo $db_connect->last_query();
                    if ($db_flag === false) {
                        $msg = 'FAIL _ Query : '. $this->db->last_query();
                    } else {
                        $msg = 'OK _ Query : '. $this->db->last_query();
                    }
                }
            }
        }

        # Close db connection
        $db_connect->close();
    }
}
