<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Geo code controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 25 Oct 2013
 */
class Geocode extends CI_Controller {
    function index() {
        $result = $this->db->limit(5)->get_where(TBL_OBJECT, array('flag' => FALSE))->result_array();
        $batch_data = array();
        foreach ($result as $key => $value) {
            $batch_data[] = array(
                'id'       => $value['id'],
                'position' => json_encode($this->call_api($value['name'])),
                'flag'     => TRUE
            );
        }

        #$this->output->enable_profiler(TRUE);
        $this->db->update_batch(TBL_OBJECT, $batch_data, 'id');
    }

    function call_api($street = '') {
        $api_keys = 'Fmjtd%7Cluubn162l1%2C8g%3Do5-90ald6';
        $street   = urlencode($street);
        $link     = "http://www.mapquestapi.com/geocoding/v1/address?key={$api_keys}&street={$street}&adminArea1=DE&format=json";
        $ch       = curl_init();
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output);
    }
}