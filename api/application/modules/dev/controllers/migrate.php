<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Migrate Test controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 30 Oct 2013
 */
class Migrate extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    // a_auftrag, a_position, gistab, ps_gps, ps_gpsadresse

    // --- a_auftrag
    // giaugr=the task group number
    // giaufisn=the task number
    // gimatch=Regular task / object label
    // SELECT * FROM `a_auftrag` WHERE `giposnr` = '0000' LIMIT 0, 1000
    // SELECT * FROM `ps_gpsadresse` WHERE `unterauftrag` = '0000' ORDER BY `stammauftrag` LIMIT 0, 1000

    function index() {
        $config['hostname'] = "localhost";
        $config['username'] = "root";
        $config['password'] = "1";
        $config['database'] = "ps_auftragsbuch";
        $config['dbdriver'] = "mysql";
        $config['dbprefix'] = "";
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = "";
        $config['char_set'] = "utf8";
        $config['dbcollat'] = "utf8_general_ci";
        $db1 = $this->load->database($config, true);

        $config['database'] = 'publicsolution_test'
        $db2 = $this->load->database($config, true);

        $result = $db1->get_where('a_auftrag', array('giposnr' => '0000'))->result_array();
        $batch_data = array();
        if (is_array($result) AND !empty($result)) {
            foreach ($result as $key => $value) {
                $batch_data[] = array(
                    'id_ref'     => '',
                    'id_city'    => '',
                    'id_group'   => '',
                    'id_parent'  => '',
                    'name'       => $value['gimatch'],
                    'lat'        => '',
                    'lon'        => '',
                    'position'   => '',
                    'deleted_at' => '',
                    'updated_at' => '',
                    'created_at' => '',
                    'flag'       => '',
                );
            }
        }
    }
}