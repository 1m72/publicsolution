<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

class Home extends REST_Controller {
    function index_get() {
        $return = array(
            'status' => STATUS_SUCCESS,
            'msg'    => 'Hello World'
        );
        return $this->response($return);
    }

    function error_404_get() {
        $return = array(
            'status' => STATUS_FAIL,
            'msg'    => 'Not found'
        );
        return $this->response($return, REST_CODE_PARAM_ERR);
    }
}