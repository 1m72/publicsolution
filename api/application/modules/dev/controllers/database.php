<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * DB Test controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 30 Oct 2013
 */
class Database extends CI_Controller {
    public function connect() {
        # List configs
        $city_configs = $this->db->get(TBL_CITY_CONFIG)->result();

        # Throught list configs
        $db = new stdClass();
        foreach ($city_configs as $key => $value) {
            $connect_info = isset($value->database) ? json_decode($value->database, true) : array();
            if ( is_array($connect_info) AND !empty($connect_info) ) {
                # DB config
                $db_config = array(
                    'hostname' => isset($connect_info['hostname']) ? $connect_info['hostname'] : '',
                    'username' => isset($connect_info['username']) ? $connect_info['username'] : '',
                    'password' => isset($connect_info['password']) ? $connect_info['password'] : '',
                    'database' => isset($connect_info['database']) ? $connect_info['database'] : '',
                    'dbprefix' => isset($connect_info['dbprefix']) ? $connect_info['dbprefix'] : config_item('db_prefix'),
                    'port'     => isset($connect_info['port'])     ? $connect_info['port']     : config_item('db_port'),
                    'dbdriver' => config_item('db_dbdriver'),
                    'pconnect' => config_item('db_pconnect'),
                    'db_debug' => config_item('db_db_debug'),
                    'cache_on' => config_item('db_cache_on'),
                    'cachedir' => config_item('db_cachedir'),
                    'char_set' => config_item('db_char_set'),
                    'dbcollat' => config_item('db_dbcollat'),
                );

                # Get DB instance
                $db->{$value->id_city} = $this->load->database($db_config, true);
            }
        }

        # Test connect
        $result_1 = $db->{1}->limit(5)->get_where(TBL_WORKER)->result();
        _debug($result_1);

        $result_2 = $db->{2}->limit(3)->get_where(TBL_WORKER)->result();
        _debug($result_2);

        $this->cache($db);
    }

    function cache($obj) {
        $data = serialize($obj);
        $this->db->insert('z_test', array('data' => $data));
    }

    function clone_db() {
        $db_config = array(
            'hostname' => 'localhost',
            'username' => 'root',
            'password' => 'vertrigo',
            'database' => 'publicsolution_cus2',
            'dbprefix' => config_item('db_prefix'),
            'port'     => config_item('db_port'),
            'dbdriver' => config_item('db_dbdriver'),
            'pconnect' => config_item('db_pconnect'),
            'db_debug' => config_item('db_db_debug'),
            'cache_on' => config_item('db_cache_on'),
            'cachedir' => config_item('db_cachedir'),
            'char_set' => config_item('db_char_set'),
            'dbcollat' => config_item('db_dbcollat'),
        );
        $db_connect = $this->load->database($db_config, TRUE);

        $activity_create = "CREATE TABLE IF NOT EXISTS `activities` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `id_ref` varchar(255) NOT NULL COMMENT 'Link to Old DB',
            `id_city` int(11) NOT NULL,
            `name` varchar(255) NOT NULL,
            `color` varchar(20) NOT NULL,
            `deleted_at` datetime DEFAULT NULL,
            `updated_at` datetime NOT NULL,
            `created_at` datetime NOT NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;";

        $db_query = $db_connect->query($activity_create);
        $this->output->enable_profiler(TRUE);
    }
}