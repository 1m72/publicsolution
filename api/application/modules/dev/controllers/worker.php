<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Worker Test controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 30 Oct 2013
 */
class Worker extends CI_Controller {

    function resolve_street() {
        $this->load->helper('street');
        $lat    = '48.0810335';
        $lon    = '11.8714348';
        $street = resolve_street_name($lat, $lon);
        var_dump($street);
    }
}
