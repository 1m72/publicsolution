<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Gcm controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 25 Oct 2013
 */
require APPPATH.'/controllers/REST_Controller.php';
class Gcm extends REST_Controller {
	function index_get() {
		$this->load->helper('pheanstalk');
		$result = push_job(QUE_SERVER_UPDATE,'{"id_city": "1", "app": "2", "msg_key":"server_update", "msg_body":"object", "log_id":"1"}');
		var_dump($result);
		die;
		// $this->gcm_server_update();
	}

	function gcm_server_update(){
			$return     = array();
			# constants
			$message    = 'test message';
			$key        = 'AIzaSyAnPeTnUsodCvrStKbQaAMsPOtR9JQONEg';
			$url        = 'https://android.googleapis.com/gcm/send';

			if(isset($device_list) AND !empty($device_list) AND is_array($device_list) OR 1){
				$registrationid = array(
					"abc123",
					"APA91bGkXAKg4l0SOJkeoUza1G05smDQx2AYVR5OzswB2HKjm7LE_-gWyagERrqGxZmR1xlapjFNt24o3yZ9GxK_Vc4iqaWvKM7i8VMnTojA_B5i5OWjT5JNiInkc3eYEU8JAV7BmZCz0c3uluv2clVNMTBl1MZvQw",
					"APA91bFpWmJJJCE_cWN2qSDasZh8H5YdABKhAz62M0KLwdgr4BSa1ZdSKk0YoMC-1iw0w88eoxvOjB-PU6Rd7zxzrlexZImHuvbs4L7WMtSuaLD15bNvRrJ88YmP-lfgdUUN8Goz_aBWsnryJXy1ucROnhaJ5wkMihxnrMyzVgwYQGmZx1SC3bM",
					"APA91bF0jstWiEJlkSgKx7r8iQv-lCoHeza12Pm242gNmDkkXuubCPdCj_6cI7wGO6O3usVVF0JRyTnejbz3E4RCJMyeBSf-GOmvTvlRRt7KRbRHmQeOvJ1yyJ8uU8sS6Ph3vkYg77jNNxkx3j3ZAGy7XcOIO0h_j77mOX2Y8DFtVZPzsvbhbCA",
					"APA91bFK_oiTVu3xjwOvmr4jcQ9AFpTJPbw9G3wZOMR1X34F5y",
					"APA91bFK_oiTVu3xjwOvmr4jcQ9AFpTJPbw9G3wZOMR1X34F5y",
					"APA91bHIKAd7moVUXHa3upH7buw6Mtm0NSsegbdxw6ytud8vWnwYOAyBXJ-M6lhYnu7EQzzvEGUPaOatCeFstDw4HGDJzXIgy1xxjRiZMxlPZuDSiv8GZJgfnYEt1UZzAR_vNz0Cd7upJIM5RXDEnhdOEiRlzYFSKA",
					"APA91bFK_oiTVu3xjwOvmr4jcQ9AFpTJPbw9G3wZOMR1X34F5y",
					"APA91bFK_oiTVu3xjwOvmr4jcQ9AFpTJPbw9G3wZOMR1X34F5y",
					"APA91bFwdzeOzjEG1pKr9Vp6xKsm4Chaclo6spjJm_wcKFIf_3OfHEQXrZ29HOpFZnzPE9kNpRLTDEI7w3bkpp4tR9IgW7yHpaC8uovj8kQGVgTWupGXRVVOkfrkkNP-4pEiQwFgJPTdUC2RtsMKu02v7KQAKSPgdg",
					"APA91bHEzHSZLySzE1oWJ8AFM72k8lNVQMqPgX4QinJZ6ylJ1m4Vh55tm8_00yqRZdxNXgqdNTSvGuicayYEe_AziPQTuHSuvtPKXirEqg6rCwFQdDQgTvkzFjm0Ocujtul6I3XUyKg3pbOVG0XiyYx_0yWsHQirUA",
					"APA91bFETKZDZtNpG7Jew7ZlcedQ7t4ocqX_jxvya307HWj9bV0VTIACz_4kx9F-2filPu9HhT_ph02f2Ij0ShtpFzgoG5MUV-Ltq-gv3c2Ty0ddVhhg5M6GFpCqS390Abk_3KFgVaOriHELHDfjosR7rL0WbQdMeg",
					"APA91bFzpVxFsU_V9w6MIvCDynLUy1kMdgXM0O4LVnedbVCdjEyvg69yCGqt5549vFkBuLeLJ1D2_PD73KNlexkbILLtm0XRcfldLSM-jm5aFIaUtqyB2onEqdG90LIHRhkyC-8Qy-yJ6K-XEPbyl4MwWWkY97UuXA",
					"APA91bEsJ9KDsGfZCihYdNATczfAiZGoO8GX0g1LjvHYVxfeAlk1kW_mARTtW07Haywt4oc-RHS6640hqNGeTFMURiOZ-PzOeIHDiEt9TK5Vsjajs65anMg56OkTVR68PXkvRfYrWAixeuIf0jilRtEFpwrlX13X5uYh-Shrs41LT6RgEwhAuxY",
					"APA91bFlWYZvM815s6UNQwxv4RQRMh6khdRBd4hD_KFydrUHAA5504xVQZXG4SwgPS1VVfdHAoWwo75whkKKlKNJt5-JLcxH6sr9hI5xJuDG9RkkPQgWQkAdp1nNgLDP3nM4op6jMwiNCF68bAx1mbzFMN1IbDIWtg",
					"APA91bEt2E6Hk8eKdkqMduGOrvse6ObbTrH-HTzPpZFo61OLExA1M9UUolftOvljHFzRHFZawlbbm26WqbOVvQ_Js9JoyUxmPOCXFQ5dhP-s023TP_Wj253WPh-nT4CmtqDTwLio3Sn3WhmzzJrr1p3jX2UEPEXkXA",
					"APA91bFR3J6ZlxYGhjmMIlpCle9jVuzfSdxecglZJ2twWf7-_J5ejuyzDd46F6ANeZwLpp7SwNACoh05gruYJEOZMlHZivhDg0fs1sU5p_hIfC_QCW1fyDRW-H_REKsZR5fhGeegMncDHwyDLdcAGCFd0yi4EfbOMoLd8WvwkC5wZAmZ9cToFZE",
					"APA91bGFjAow-nhjxy36tgNE_hLnr72vmxf0723ZZZSq8KiTrr_wR71ScIapZuWtZbYkhmBsZXIekL6d50-lzk50wF0yiIMwT6PMgGZFMvdOrA6Bvf8D6Pzl7zrXBBL8WOZg-GmYF2V3vnzIglXPWZmoHpeKsqzpBA",
					"APA91bEVTbSpyNZ-j69YvH4l3ApTyBmFq7coNdShnr93zBYnLjauANFXVuFVMHymJHYV9E4yGV1IAAgUx_n3-TFHJvubBSM_RQhQVMytkNvlAcrtOlOIXf8U2Yx1EXmvYeZm3ecD-M71pIqX3m85LmqkmSNoJbmmbQ",
					"APA91bHXf9YwYX2adDTnNVRtXdPJ69Exfoj2XanipTfp9JcumPMQZOt-TFppsvKKVEkDhSmmtwPFVLcLC9ujJrdby5jonZ2hGdVU649D_iaKaEtaoQFGbxoOrRR5_b3M1q2ek8jI91CIOScgu-emS-NkPuMiGZKD8w",
					"APA91bFk3wwbkAJfWF6PZ6vWbVoIW4VezYx58FUHyRxzYdrAg4oximQWJvIo5rJm4_J0ruxi5VLoGUhNhKx7torFq9Q6UUpRvtnouc7RMM1r3OUJkmNHqAm8fTWlb6PKZ2Hmw7Dbd9HHp8am4PY-amqNoYVk21cj7bjFrGU9fJDQytQaWUDKKWw",
					"APA91bHKCsCmQWdicmIypqpsV4kuwR_Tb2DY684yorNzeiJTsRmAco6ELQDznOv-1zW9afPEDe2rLUd1L04bUAhQF1pTmf9_u1FysSnBFBAq_Ru_j_mh4ucDAkJdbIjcYzhHwesvLoEdHVG9v9qzn1MfhK-yAN_4lcUcqK6tqPSyLcN3BO-qpbY",
					"APA91bHLNe5_gpK_cwpynB5DzLItu2TXmcbgFhE3g2BjIlAzA5N4w0qK5BbE4MfTMt-ZSKQuZX863qZwunZ264zPEkZqWt0c-QpvQ606X1npPdT4mnh-Qk-acPdzNOFbhPUf9sxJrc4OqskzAcWK5UeNDzLOgBrs-ZD0OSrNS31dKVvIL-JvkMc",
					"APA91bHhT2-r9P29ehyNEk_TVVcCTKU8kmybwZUlj_0RY4MAAZOs5nPucNKESvf9npqgmUpVWT2DyW4BxxamhDxUzBkaYEEvPyM2vK1ra7G6zpAxzKovkkXB9beRLOM2kyYK9FIcLj2Q7QwmLPMVbrGbbOg-z-5Uq74t1_71-Gq3olJk-yxooH4"
				);

				if(isset($registrationid) AND !empty($registrationid) AND is_array($registrationid)){
					$fields = array(
						'registration_ids'  => $registrationid, //array($registrationid),
						'data'              => array("server_update" => $message)
					);

					$headers = array(
						'Content-Type:application/json',
						'Authorization:key='.$key
					);

					// Open connection
					$ch = curl_init();
					// Set the url, number of POST vars, POST data
					curl_setopt( $ch, CURLOPT_URL, $url );
					curl_setopt( $ch, CURLOPT_POST, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields));

					// Execute post
					$result = curl_exec($ch);
					$errno  = curl_errno($ch);

					// var_dump($registrationid);
					// var_dump($errno);
					// var_dump($result);

					// Close connection
					curl_close($ch);
					$return = $registrationid;
				}
			}
			# return
			return $return;
	}

	function fasttask_push_notification_get() {
		$this->load->helper('pheanstalk');
		$result = push_job(QUE_SERVER_UPDATE,'{"id_city": "1", "app": "2", "msg_key":"server_update", "msg_body":"object", "log_id":"1"}');
		return $this->response(array('status' => $result));
	}
}
