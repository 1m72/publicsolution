<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author tran.duc.chien@kloon.vn
 * @created 21/10/2013
 */
class Machine_model extends MY_Model {
    function __construct() {
        $this->_table      = TBL_MACHINE;
        //$this->soft_delete = true;
    }

    function get_machines(){
        return $this->as_array()->get_all();
    }

    function get_machine_activities($id_machine = 0){
        # Get machines available
        $machine_available_result = $this->select('id')->as_array()->get_all();
        $machine_available = array_map('array_pop', $machine_available_result);

        $return = array();
        if ( is_array($machine_available) AND !empty($machine_available) ) {
            $this->db->from(TBL_MACHINE_ACTIVITY);
            $this->db->where_in('id_machine', $machine_available);
            if (is_numeric($id_machine) && $id_machine > 0) {
                $this->db->where('id_machine', $id_machine);
            }
            $this->db->order_by('id_machine', 'asc');
            $return = $this->db->get()->result_array();
        }

        return $return;
    }

    function get_machine_materials($id_machine = 0){
        # Get machines available
        $machine_available_result = $this->select('id')->as_array()->get_all();
        $machine_available = array_map('array_pop', $machine_available_result);

        $return = array();
        if ( is_array($machine_available) AND !empty($machine_available) ) {
            $this->db->from(TBL_MACHINE_MATERIAL);
            if (is_array($machine_available) AND !empty($machine_available)) {
                $this->db->where_in('id_machine', $machine_available);
            }
            if (is_numeric($id_machine) && $id_machine > 0) {
                $this->db->where('id_machine', $id_machine);
            }
            $this->db->order_by('id_machine', 'asc');
            $return = $this->db->get()->result_array();
        }

        return $return;
    }

	/*
	* @function : synchronize_machine_get
	* @author   : Le Thi Nhung(le.nhung@kloon.vn)
	* @since    : 2013-10-25
	*/
	function synchronize_machine_get($lastupdate){
		$this->db->select("m.id,m.name,m.id_default,m.nfc_code,if(deleted_at !='','D',if(m.updated_at is null,'N',if(m.updated_at>m.created_at,'U','N'))) as flag",FALSE);
		$this->db->where('m.updated_at >',$lastupdate);
		$this->db->or_where('m.created_at >',$lastupdate);
		$result = $this->db->get(TBL_MACHINE .' m');
		return $result->result_array();
	}
}