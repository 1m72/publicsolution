<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class SynchronizeTest_Model extends CI_Model
{
	function insert($table,$data)
	{
		$this->db->insert($table,$data);
		return $this->db->insert_id(); 
	}
	
	function insert_batch($table,$data)
	{
		$this->db->insert_batch($table, $data); 
	}
	
	function update($table,$where=array(),$data)
	{
		foreach ($where as $key=>$value)
		{
			$this->db->where($key,$value);
		}
		$this->db->update($table,$data);
	}
}
?>