<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author tran.duc.chien@kloon.vn
 * @created 21/10/2013
 */
class Material_model extends MY_Model {
    function getActivities(){
    }

    /**
     * [material_get description]
     * @param  [type] $tenantid [description]
     * @return [type]           [description]
     * @author HuongPM <phung.manh.huong@kloon.vn>
     */
    function material_get($tenantid)
    {
        $this->db->select('m.id,m.name,m.unit');
        $this->db->from(TBL_MATERIAL . ' m');
        $this->db->where('m.id_city',$tenantid);
        $this->db->where('m.deleted_at',NULL);
        //$this->db->where('m.deleted_at',NULL);
        /*$this->db->join(TBL_MACHINE_MATERIAL . ' mm','m.id = mm.id_material');
        $this->db->join(TBL_MACHINE . ' mc','mc.id = mm.id_machine');
        $this->db->join(TBL_NFC . ' n','n.nfc_code = mc.nfc_code');
        $this->db->where('n.id_company',$tenantid);
        $this->db->where('n.deleted',0);
        $this->db->where('m.deleted',0);
        $this->db->where('mc.deleted',0);
        $this->db->group_by(array('m.id','m.name','m.unit'));*/
        return $this->db->get()->result_array();
    }
	
	/*
	* @function: synchronize_material_get
	* @author  :Le Thi Nhung(le.nhung@kloon.vn)
	* @since   :2013-10-25
	*/
	function synchronize_material_get($lastupdate){
		$this->db->select("id as materialid,name as materialname, unit ,if(deleted_at!='','D',if(updated_at is null,'N',if(updated_at>created_at,'U','N'))) as flag",false);
		$this->db->where("updated_at >",$lastupdate);
		$this->db->or_where("created_at >",$lastupdate);
		$result = $this->db->get(TBL_MATERIAL);
		return $result->result_array();
		
	}
	/*
	* @function: insertMaterial
	* @author  :Le Thi Nhung(le.nhung@kloon.vn)
	*/
	public function insertMaterial($data){
		$this->db->insert(TBL_MATERIAL,$data);
		return $this->db->insert_id(); //return materialid after insert
	}
}