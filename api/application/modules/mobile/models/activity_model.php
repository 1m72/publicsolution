<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author tran.duc.chien@kloon.vn
 * @created 21/10/2013
 */
class Activity_model extends MY_Model {
    function get_activities(){
        return $this->db->get(TBL_ACTIVITIES)->result();
    }

    function activity_get($tenantid)
    {
        $this->db->select('a.id, a.name, a.color');
        $this->db->from(TBL_ACTIVITIES . ' a');
        $this->db->where('a.id_city', $tenantid);
        $this->db->where('a.deleted_at', null);
        $this->db->order_by('a.name','asc');
        return $this->db->get()->result_array();
    }

	/*
	* @fucntion: synchonize_activity_get
	* @author  : Le Thi Nhung (le.nhung@kloon.vn)
	* @since   : 2013-10-24
	*/

	function synchronize_activity_get($lastupdate){
		$this->db->select("id as activityid,name as activityname , color as activitycolor, if(deleted_at !='','D',if(updated_at is null,'N',if(updated_at>created_at,'U','N'))) as flag",false);
		$this->db->where("updated_at >",$lastupdate);
		$this->db->or_where("created_at >",$lastupdate);
        $this->db->order_by('name','asc');
		$result = $this->db->get(TBL_ACTIVITIES);
		return $result->result_array();
	}
}