<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* End of file module_model.php */
/* Location: ./application/modules/mobile/models/module_model.php */
class Module_model extends MY_Model {
    public $_table      = TBL_MODULES;
    public $soft_delete = true;
    function __construct() {
        parent::__construct();
    }
}