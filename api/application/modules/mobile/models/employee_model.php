<?php
class Employee_Model extends CI_Model
{
	function Employee_Model()
	{
		parent::__construct();
	}

	/**
	 * [employee_get description]
	 * @param  [type] $tenantid [description]
	 * @return [type]           [description]
	 * @author HuongPM <phung.manh.huong@kloon.vn>
	 */

	function  employee_get($tenantid)
	{
		if(isset($tenantid) && !empty($tenantid))
		{
			$this->db->select('w.*');
			$this->db->from(TBL_WORKER . ' w');
			$this->db->where('w.deleted_at', NULL, FALSE); # Add check deleted_at NULL (Chien Tran, 20 Jan 2013)
			$this->db->where('w.id_city', $tenantid);
			//$this->db->where('w.deleted_at',NULL);
			/*$this->db->join(TBL_COMPANY . ' c','c.id = w.id_company');
			$this->db->join(TBL_CITY . ' ct','ct.id = c.id_city');
			$this->db->where('ct.id',$tenantid);
			$this->db->where('ct.deleted',0);
			$this->db->where('c.deleted',0);
			$this->db->where('w.deleted',0);*/
			return $this->db->get()->result_array();
		}
		else
			return NULL;
	}

	/*
	* @fucntion: synchronize_employee_get
	* @author  : Le Thi Nhung (le.nhung@kloon.vn)
	* since    : 2013-10-25
	*/
	function synchronize_employee_get($lastupdate, $tenantId){
		$sql = $this->db->select("id as employeeid,first_name as firstname, last_name as lastname, address, phone,id_default,nfc_code, if(deleted_at !='','D',if(updated_at is null,'N',if(updated_at>created_at,'U','N'))) as flag",FALSE);
		$where = "id_city='".$tenantId."' AND (updated_at >'".$lastupdate."' OR created_at >'".$lastupdate."')";
		$this->db->where($where, NULL, FALSE);
		$result = $this->db->get(TBL_WORKER);
		return $result->result_array();
	}
}
?>