<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * City models
 * @author tran.duc.chien@kloon.vn
 * @created 20/1/2014
 */
class City_model extends MY_Model {
    function __construct() {
        parent::__construct();
        $this->_table = TBL_CITY;
        $this->db     = $this->_db_global;
    }
}