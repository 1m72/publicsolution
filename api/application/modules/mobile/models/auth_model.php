<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Validate passcode + device_id and return tenanID (id_city)
 * @author tran.duc.chien@kloon.vn
 * @created 21/10/2013
 */
class Auth_model extends MY_Model {
    function validate_AccessCode($access_code = '', $device_id = '', $app = APP_WINTER){
        $this->_db_global->select('id as tenantId,id_config,selection as allow_selection, material, name as city_name');
        $this->_db_global->where('passcode', $access_code);
        $result = $this->_db_global->get(TBL_CITY)->row();
        if (!empty($result) ) {
            // Custom DB config
            $this->_db_global->where('id', $result->id_config);
            $city_config = $this->_db_global->get(TBL_CITY_CONFIG)->row();

            // Connect to customer db for check devices information exist
            if (!empty($city_config) AND (isset($city_config->database))) {
                // Customer db config
                $db_config = json_decode($city_config->database);
                $db_config = array(
                    'hostname' => isset($db_config->hostname) ? $db_config->hostname : config_item('hostname'),
                    'username' => isset($db_config->username) ? $db_config->username : config_item('username'),
                    'password' => isset($db_config->password) ? $db_config->password : config_item('password'),
                    'database' => isset($db_config->database) ? $db_config->database : config_item('database'),
                    'port'     => isset($db_config->port) ? $db_config->port : config_item('db_port'),
                    'dbprefix' => config_item('db_prefix'),
                    'dbdriver' => config_item('db_dbdriver'),
                    'pconnect' => config_item('db_pconnect'),
                    'db_debug' => config_item('db_db_debug'),
                    'cache_on' => config_item('db_cache_on'),
                    'cachedir' => config_item('db_cachedir'),
                    'char_set' => config_item('db_char_set'),
                    'dbcollat' => config_item('db_dbcollat'),
                );

                // Connect to customer DB
                $db_connect = $this->load->database($db_config, true);
                $db_connect->where('id_device', $device_id);
                $db_connect->where('id_city', $result->tenantId);
                $device_id_exist = $db_connect->get(TBL_DEVICES)->row();

                // If device isn't exist then create a record for this devices
                if (empty($device_id_exist)) {
                    $db_connect->insert(
                        TBL_DEVICES,
                        array(
                            'id_city'    => $result->tenantId,
                            'id_device'  => $device_id,
                            'app'        => $app,
                            'updated_at' => date('Y-m-d H:i:s'),
                            'created_at' => date('Y-m-d H:i:s')
                        )
                    );
                }
            }
        }
        return $result;
    }
}