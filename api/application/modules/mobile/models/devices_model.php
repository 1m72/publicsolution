<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Devices_model extends MY_Model {
	/*
	* @function: insertDevices
	* @author  :Le Thi Nhung(le.nhung@kloon.vn)
	* @Since   : 2013-10-31
	*/
	public function insertDevices($data){
		$this->db->insert(TBL_DEVICES,$data);
	}
	
	/*
	* @function:device_byid
	* @author  :Le Thi Nhung(le.nhung90@gmail.com)
	* @since   : 2013-11-05
	*/
	function device_byid($device_id){
		$this->db->where("id_device",$device_id);
		$this->db->where("deleted_at",NULL);
		$result = $this->db->get(TBL_DEVICES);
		return $result->result_array();
	}
	
		/*
	* @function : updateDevice
	* @author   : Le Thi Nhung(le.nhung@kloon.vn)
	* @since :2013-11-05
	*/
	function update_device($id_device,$data){
		$this->db->where("id_device",$id_device);
		$this->db->update(TBL_DEVICES,$data);
	}
}