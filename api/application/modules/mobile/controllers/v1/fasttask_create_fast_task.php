<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-11-27
 * */
class Fasttask_create_fast_task extends REST_Controller{
    private $_id_city                   = 0;
    private $_id_config                 = 1;
    private $_service_provider          = array();
    private $_service_provider_selected = '';
    private $_service_provider_fails    = array();

    function __construct(){
        parent:: __construct();

        if (isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->_id_city = intval($this->_tenantId);
        } else {
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL,
                    'msg'       => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        // Service provider
        $this->_service_provider = array(
            'bis'           => 'http://nominatim.bis-office.com/reverse.php?format=json&zoom=17&addressdetails=1',
            'kloon'         => 'http://nominatim.kloon.net/reverse.php?format=json&zoom=17&addressdetails=1',
            'mapquest'      => 'http://open.mapquestapi.com/nominatim/v1/reverse.php?format=json&zoom=17',
            'openstreetmap' => 'http://nominatim.openstreetmap.org/reverse?format=json&zoom=17&addressdetails=1',
            'gisgraphy'     => 'http://services.gisgraphy.com/street/streetsearch?format=json&from=1&to=1',
        );

        foreach ($this->_service_provider as $key => $value) {
            $this->_service_provider_fails[$key] = 0;
        }
    }

    function create_fast_task_post(){
        $id_city  = $this->_id_city;
        $xml_data = urldecode($_POST['xml']);
        $data     = $this->format->factory($xml_data, 'xml')->to_array();
        if (isset($data) && !empty($data))
        {
            // begin
            if (isset($data) && !empty($data)){
                $app_fasttask    = $this->db->get_where(TBL_MODULES, array('type' => 'fasttask'))->row_array();
                $app_fasttask_id = isset($app_fasttask['id']) ? $app_fasttask['id'] : 0;
                foreach ($data as $row){
                    // transaction start
                    $this->db->trans_start();
                    $created_task_id   = isset($row['created_task_id']) ? $row['created_task_id'] : '';
                    $employee_id       = isset($row['superior_id']) ? $row['superior_id'] : '';
                    $object_id         = isset($row['object_id']) ? $row['object_id'] : '';
                    $employee_group_id = isset($row['employee_group_id']) ? $row['employee_group_id'] : '';
                    $cost_center_id    = isset($row['cost_center_id']) ? $row['cost_center_id'] : '';
                    $free_text         = isset($row['free_text']) ? $row['free_text'] : '';
                    $free_text         = is_array($free_text) ? implode(',', $free_text) : $free_text;
                    $free_text         = is_string($free_text) ? $free_text : '';
                    $created_time      = isset($row['created_time']) ? $row['created_time'] : date('Y-m-d H:i:s');
                    $id_fasttask       = 0;
                    $information       = array();

                    // Init fasttask record
                    $fasttask = array(
                        'id_city'           => $id_city,
                        'id_task'           => $created_task_id,
                        'id_object'         => $object_id,
                        'superior_id'       => $employee_id,
                        'employee_group_id' => $employee_group_id,
                        'cost_center_id'    => $cost_center_id,
                        'free_text'         => $free_text,
                        'created_at'        => $created_time,
                    );
                    $db_flag = $this->db->insert(TBL_FASTTASK, $fasttask);

                    if ($db_flag){
                        $id_fasttask = $this->db->insert_id();

                        // messages
                        if (isset($row['messages']) && !empty($row['messages'])){
                            $messages = $this->_walking_nodes($row['messages'], 'text', 'message');
                            foreach ($messages as $it_msg) {
                                $text_data = isset($it_msg['text']) ? $it_msg['text'] : '';
                                $text_type = INFO_MESSAGE;
                                if (isset($it_msg['text_modules'])) {
                                    $text_data = $it_msg['text_modules'];
                                    $text_type = INFO_MESSAGE_TEXTMODULE;
                                }
                                $ar_msg = array(
                                    'id_city'       => $id_city,
                                    'id_config'     => $this->_id_config,
                                    'id_worker_gps' => $id_fasttask,
                                    'id_task'       => $created_task_id,
                                    'app'           => $app_fasttask_id,
                                    'type'          => $text_type,
                                    'data'          => $text_data,
                                    'lat'           => isset($it_msg['lat']) ? $it_msg['lat'] : '',
                                    'lon'           => isset($it_msg['lon']) ? $it_msg['lon'] : '',
                                    'time'          => isset($it_msg['time']) ? $it_msg['time'] : '',
                                    'created_at'    => date('Y-m-d H:i:s')
                                );
                                $ar_msg['street'] = $this->resolve_street_name($ar_msg['lat'], $ar_msg['lon']);
                                $information[]    = $ar_msg;
                            }
                        }

                        // images
                        if (isset($row['images']) && !empty($row['images'])){
                            $images = $this->_walking_nodes($row['images'], 'image_name', 'image');
                            foreach ($images as $it_img){
                                $ar_img = array(
                                        'id_city'           => $id_city,
                                        'id_config'         => $this->_id_config,
                                        'id_worker_gps'     => $id_fasttask,
                                        'id_task'           => $created_task_id,
                                        'app'               => $app_fasttask_id,
                                        'type'              => INFO_IMAGE,
                                        'data'              => isset($it_img['image_name']) ? ($created_task_id . '_' . $it_img['image_name'] . '.jpg') : '',
                                        'lat'               => isset($it_img['lat']) ? $it_img['lat'] : '',
                                        'lon'               => isset($it_img['lon']) ? $it_img['lon'] : '',
                                        'street'            => '',
                                        'time'              => isset($it_img['time']) ? $it_img['time'] : '',
                                        'created_at'        => date('Y-m-d H:i:s')
                                );
                                $ar_img['street'] = $this->resolve_street_name($ar_img['lat'], $ar_img['lon']);
                                $information[]    = $ar_img;
                            }
                        }

                        // voices
                        if (isset($row['voices']) && !empty($row['voices'])){
                            $voices = $this->_walking_nodes($row['voices'], 'voice_name', 'voice');
                            foreach ($voices as $it_voice){
                                $ar_voice = array(
                                        'id_city'           => $id_city,
                                        'id_config'         => $this->_id_config,
                                        'id_worker_gps'     => $id_fasttask,
                                        'id_task'           => $created_task_id,
                                        'app'               => $app_fasttask_id,
                                        'type'              => INFO_VOICE,
                                        'data'              => isset($it_voice['voice_name']) ? ($created_task_id . '_' . $it_voice['voice_name'] . '.3gp') : '',
                                        'lat'               => isset($it_voice['lat']) ? $it_voice['lat'] : '',
                                        'lon'               => isset($it_voice['lon']) ? $it_voice['lon'] : '',
                                        'time'              => isset($it_voice['time']) ? $it_voice['time'] : '',
                                        'created_at'        => date('Y-m-d H:i:s')
                                );
                                $ar_voice['street'] = $this->resolve_street_name($ar_voice['lat'], $ar_voice['lon']);
                                $information[]      = $ar_voice;
                            }
                        }

                        // movies
                        if (isset($row['movies']) && !empty($row['movies'])){
                            $movies = $this->_walking_nodes($row['movies'], 'movie_name', 'movie');
                            foreach ($movies as $it_movie){
                                $ar_movie = array(
                                        'id_city'           => $id_city,
                                        'id_config'         => $this->_id_config,
                                        'id_worker_gps'     => $id_fasttask,
                                        'id_task'           => $created_task_id,
                                        'app'               => $app_fasttask_id,
                                        'type'              => INFO_VIDEO,
                                        'data'              => isset($it_movie['movie_name']) ? ($created_task_id . '_' . $it_movie['movie_name'] . '.mp4') : '',
                                        'lat'               => isset($it_movie['lat']) ? $it_movie['lat'] : '',
                                        'lon'               => isset($it_movie['lon']) ? $it_movie['lon'] : '',
                                        'time'              => isset($it_movie['time']) ? $it_movie['time'] : '',
                                        'created_at'        => date('Y-m-d H:i:s')
                                );
                                $ar_movie['street'] = $this->resolve_street_name($ar_movie['lat'], $ar_movie['lon']);
                                $information[]      = $ar_movie;
                            }
                        }
                    }

                    // insert information
                    if (($id_fasttask > 0) && (isset($information) && !empty($information))){
                        $this->db->insert_batch(TBL_WORKER_INFORMATION,$information);
                    }

                    // transaction end
                    $this->db->trans_complete();

                    // Log errors if transaction errors
                    if ($this->db->trans_status() === FALSE) {
                        write_log('Transaction error_'.json_encode($data), 'update_winter_service_task');
                        return $this->response(
                            array(
                                'status' => STATUS_FAIL,
                                'msg'    => 'Has an error when sync data to DB. This error has been collected.'
                            )
                        );
                    } else {
                        return $this->response(
                            array(
                                'status' => STATUS_SUCCESS,
                                'msg'    => 'Data has been synchronized'
                            )
                        );
                    }
                }// end foreach
            }// end if
        } else {
            return $this->response(
                array (
                    'status' => STATUS_FAIL,
                    'msg'    => 'param null'
                )
            );
        }
    }

    function upload_fast_task_post() {
        $resources = isset($_FILES) ? $_FILES : '';
        $records   = array();
        $counter   = 0;
        $info      = array();

        $created_task_id = $this->input->post('created_task_id');
        $employee_id     = $this->input->post('employee_id');
        $object_id       = $this->input->post('object_id');

        $upload_path = config_item('upload_dir');
        $status      = true;
        if ( isset($resources) AND is_array($resources) AND (!empty($resources)) ) {
            foreach ($resources as $key => $value) {
                $upload_path_tmp = $upload_path . $this->_id_city . '/' . date('Y/m/d') . '/' . $employee_id . '/';
                $result = $this->_upload_process($key, $upload_path_tmp);
                if (file_exists($result['full_path'])) {
                    $counter++;
                    $flag = rename($result['full_path'], $result['file_path'].$created_task_id.'_'.$result['file_name']);
                    if (!$flag) {
                        write_log('Can\'t rename file fast_upload_Task: '.$created_task_id.'_Result: '.json_encode($result), 'upload_fast_task');
                    }
                } else {
                    write_log("File upload not exit_Task: '.$created_task_id.'_Result: ".json_encode($result), 'upload_fast_task');
                }
            }
        } else {
            $status = false;
        }
        return $this->response(
            array(
                'status' => $status,
                'msg'    => "Received: {$counter}",
                'info'   => $info
            )
        );
    }

    function _upload_process($field_name, $upload_path) {
        if (!is_dir($upload_path)) {
            umask(0000);
            @mkdir($upload_path, 0755, true);
        }

        // Config for upload
        $config['upload_path']   = $upload_path;
        $config['allowed_types'] = config_item('upload_allowed');
        // $config['encrypt_name']  = true;
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($field_name)) {
            // Log errors
            write_log('Upload file error_'.json_encode($this->upload->display_errors()), 'upload_file_error');

            return FALSE;
        } else {
            $data = $this->upload->data();
            return $data;
        }
    }

    /**
     * Walking all elements of array (parsed by Format library)
     * @param  array  $data        Data array
     * @param  string $key_checker Array key for check has one or many alement
     * @param  string $key_value   Array key for element containt data
     * @return array               Array valid
     * @author chientran <tran.duc.chien@kloon.vn>
     * @created at 23 Oct 2013
     */
    function _walking_nodes($data = array(), $key_checker = '', $key_value = '') {
        $return = array();
        // Has one
        if (isset($data[$key_value]) && isset($data[$key_value][$key_checker]) || isset($data[$key_checker])) {
            $return[] = $data[$key_value];
        } else {
            // Has many
            if (!empty($key_value) && isset($data[$key_value]) && !empty($data)) {
                $return = $data[$key_value];
            } else {
                foreach ($data as $key => $value) {
                    $return[] = $value;
                }
            }
        }
        return $return;
    }

    // resolve_street_name
    function resolve_street_name($latitude, $longtitude, $service = 'kloon') {
        $services = $this->_service_provider;

        if (!isset($services[$service])) {
            $service = $services['mapquest'];
        } else {
            $service = $services[$service];
        }

        $street = '';
        if ((isset($latitude) && !empty($latitude)) && (isset($longtitude) && !empty($longtitude))) {
            $service_url    = '';
            $service_type   = $service;
            $count  = 0;
            $status = FALSE;
            while ( ($count < count($this->_service_provider)) AND ($status != TRUE) ) {
                switch ($count) {
                    case 1:
                        $service_type = 'mapquest';
                        $service_url  = $services['mapquest'] . '&lat=' . $latitude . '&lon=' . $longtitude;
                        break;

                    case 2:
                        $service_type = 'bis';
                        $service_url  = $services['kloon'] . '&lat=' . $latitude . '&lon=' . $longtitude;
                        break;

                    case 3:
                        $service_type = 'openstreetmap';
                        $service_url  = $services['openstreetmap'] . '&lat=' . $latitude . '&lon=' . $longtitude;
                        break;

                    case 4:
                        $service_type = 'gisgraphy';
                        $service_url  = $services['gisgraphy'] . '&latitude=' . $latitude . '&longitude=' . $longtitude;
                        break;

                    default:
                        $service_type = 'kloon';
                        $service_url  = $services['kloon'] . '&lat=' . $latitude . '&lon=' . $longtitude;
                        break;
                }
                $this->_service_provider_selected = $service_type;
                $street = $this->curl_read_content($service_url, $service_type);

                // Logs
                write_log("{$service_type}: {$latitude} - {$longtitude} {$street}", 'street_resolv');

                // Logs service fails
                if (empty($street) AND isset($this->_service_provider_fails[$service_type])) {
                    $this->_service_provider_fails[$service_type] = intval($this->_service_provider_fails[$service_type]) + 1;
                }

                if ( (isset($street) AND !empty($street)) OR ($count >= count($this->_service_provider)) ){
                    $status = TRUE;
                }
                $count++;

                usleep(100);
            }
        }
        return $street;
    }

    // curl_read_content
    function curl_read_content($link = FALSE, $service = 'kloon', $timeout = 2500){
        $street = '';
        if ($link != FALSE){
            $street_data = '';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $link);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            #curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 2000);
            $street_data = curl_exec($ch);
            curl_close($ch);

            $street_array = json_decode($street_data,true);
            if (isset($street_array['address']) && !empty($street_array['address'])) {
                $street_detail = $street_array['address'];
                if (isset($street_detail['road']) && !empty($street_detail['road'])){
                    $street_name = isset($street_detail['road']) ? $street_detail['road'] : '';
                    $street      = $street_name;
                } else {
                    if (isset($street_detail['address26']) && !empty($street_detail['address26'])){
                        $street_name = isset($street_detail['address26']) ? $street_detail['address26'] : '';
                        $street      = $street_name;
                    }
                }
            }
        }
        return $street;
    }
}
