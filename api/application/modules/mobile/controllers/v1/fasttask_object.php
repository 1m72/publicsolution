<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-11-27
 * */
class Fasttask_object extends REST_Controller{
    private $_id_city = 0;

    function __construct(){
        parent:: __construct();

        if (isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->_id_city = intval($this->_tenantId);
        } else {
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL,
                    'msg'       => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    // initialize_place
    function object_group_get() {
        $lastupdate = get_lastupdate(TBL_OBJECT_PLACE, 'updated_at');
        $obj_place  = $this->db->get_where(TBL_OBJECT_PLACE, array('id_parent' => 0, 'deleted_at' => null))->result_array();
        $result = array(
            'version'    => config_item('api_version'),
            'status'     => STATUS_SUCCESS,
            'lastupdate' => isset($lastupdate) ? $lastupdate : '',
            'total'      => count($obj_place)
        );
        if (isset($obj_place) && !empty($obj_place))
        {
            foreach ($obj_place as $key => $value)
            {
                $item = array(
                    'object_group_id'   => $value['id'],
                    'object_group_name' => $value['name']
                );
                $result['data'][] = $item;
            }
        }
        return $this->response($result);
    }

    function synchronize_place_get() {
        $_lastupdate = $this->get('lastupdate');
        if (isset($_lastupdate) && !empty($_lastupdate)){
            $start = intval($this->get('start'));
            $range = (intval($this->get('range')) === 0)?1000:intval($this->get('range'));

            $lastupdate = date('Y-m-d H:i:s', strtotime($_lastupdate));
            # Result
            $sql = "SELECT * FROM ".TBL_OBJECT_PLACE." WHERE id_parent = 0 AND (created_at > '". $lastupdate ."' OR updated_at > '".$lastupdate."')";
            $obj_place = $this->db->query($sql)->result_array();

            $result = array(
                    'version'    => config_item('api_version'),
                    'status'     => STATUS_SUCCESS,
                    'lastupdate' => $lastupdate,
                    'total'      => count($obj_place)
            );
            if (isset($obj_place) && !empty($obj_place))
            {
                foreach ($obj_place as $key => $value)
                {
                    $flag = isset($value['deleted_at']) ? 'D' : (($value['updated_at'] > $value['created_at']) ? 'U' : 'N');
                    $item = array(
                            'place_id'  => $value['id'],
                            'place_name'            => $value['name'],
                            'flag'              => $flag
                    );
                    $result[] = $item;
                }
            }
        } else {
            $result = array(
                    'version'    => config_item('api_version'),
                    'status'     => STATUS_FAIL
            );
        }
        return $this->response($result);
    }

    function initialize_object_get() {
        $start = intval($this->get('start'));
        $range = (intval($this->get('range')) === 0)?300:intval($this->get('range'));

        $lastupdate = get_lastupdate(TBL_OBJECT, 'updated_at');
        $count = $this->db->where('deleted_at', null)->count_all_results(TBL_OBJECT);

        $this->db->limit($range,$start);
        $object = $this->db->where('deleted_at', null)->get(TBL_OBJECT)->result_array();
        $result = array(
            'version'    => config_item('api_version'),
            'status'     => STATUS_SUCCESS,
            'lastupdate' => isset($lastupdate) ? $lastupdate : '',
            'count'      => count($object),
            'total'      => $count
        );
        if (isset($object) && !empty($object))
        {
            foreach ($object as $key => $value)
            {
                $item = array(
                    'object_id'   => $value['id'],
                    'object_name' => $value['name'],
                    'lat'         => isset($value['lat']) ? $value['lat'] : '',
                    'lon'         => isset($value['lon']) ? $value['lon'] : '',
                    'place_id'    => $value['id_group']
                );
                if (!isset($value['lat']) || empty($value['lat']))
                    unset($item['lat']);
                if (!isset($value['lon']) || empty($value['lon']))
                    unset($item['lon']);
                $result[] = $item;
            }
        }

        return $this->response($result);
    }

    // initialize_object_location
    function object_list_get() {
        $start = intval($this->get('start'));
        $range = (intval($this->get('range')) <= 0) ? 20 : intval($this->get('range'));

        $lastupdate = get_lastupdate(TBL_OBJECT, 'updated_at');
        $count = $this->db->where('deleted_at', null)->count_all_results(TBL_OBJECT);
        $range = ($range >= $count) ? $count : $range;

        $this->db->limit($range,$start);
        $object = $this->db->where('deleted_at', null)->get(TBL_OBJECT)->result_array();
        $result = array(
            'version'    => config_item('api_version'),
            'status'     => STATUS_SUCCESS,
            'lastupdate' => isset($lastupdate) ? $lastupdate : '',
            'count'      => count($object),
            'total'      => $count,
            'data'       => array(),
        );

        if (isset($object) && !empty($object))
        {
            foreach ($object as $key => $value)
            {
                $item = array(
                    'object_id'       => $value['id'],
                    'object_name'     => $value['name'],
                    'locations'       => array(),
                    'object_group_id' => $value['id_group']
                );
                $lat_lon = $value['position'];
                if (isset($lat_lon) && !empty($lat_lon)){
                    if ($this->lastIndexOf($lat_lon, 'LINESTRING') != -1){
                        $lat_lon     = str_replace('LINESTRING(','',$lat_lon);
                        $lat_lon     = str_replace(')','',$lat_lon);
                        $array_point = explode(',', $lat_lon);
                        foreach ($array_point as $point_key=>$point_value){
                            $point_detail = explode(' ', $point_value);
                            $item['locations'][] = array(
                                'lat' => doubleval(isset($point_detail[0]) ? $point_detail[0] : 0),
                                'lon' => doubleval(isset($point_detail[1]) ? $point_detail[1] : 0),
                            );
                        }
                    } else {
                        $lat_lon      = str_replace('POINT(','',$lat_lon);
                        $lat_lon      = str_replace(')','',$lat_lon);
                        $point_detail = explode(' ', $lat_lon);
                        $item['locations'][] = array(
                            'lat' => doubleval(isset($point_detail[0]) ? $point_detail[0] : 0),
                            'lon' => doubleval(isset($point_detail[1]) ? $point_detail[1] : 0),
                        );
                    }
                } else {
                    unset($item['locations']);
                }
                $result['data'][] = $item;
            }
        }

        return $this->response($result);
    }

    function synchronize_object_get() {
        $start = intval($this->get('start'));
        $range = (intval($this->get('range')) === 0)?300:intval($this->get('range'));

        $_lastupdate = $this->get('lastupdate');
        if (isset($_lastupdate) && !empty($_lastupdate)){
            $lastupdate = date('Y-m-d H:i:s', strtotime($_lastupdate));

            $this->db->where('updated_at >',$lastupdate);
            $this->db->or_where('created_at >',$lastupdate);
            $count = $this->db->count_all_results(TBL_OBJECT);
            $this->db->limit($range,$start);
            $object = $this->db->get(TBL_OBJECT)->result_array();

            $result = array(
                    'version'       => config_item('api_version'),
                    'status'        => STATUS_SUCCESS,
                    'lastupdate'    => $lastupdate,
                    'count'         => count($object),
                    'total'         => $count
            );
            if (isset($object) && !empty($object))
            {
                foreach ($object as $key => $value)
                {
                    $flag = isset($value['deleted_at']) ? 'D' : (($value['updated_at'] > $value['created_at']) ? 'U' : 'N');
                    $item = array(
                            'object_id'     => $value['id'],
                            'object_name'   => $value['name'],
                            'lat'           => isset($value['lat']) ? $value['lat'] : '',
                            'lon'           => isset($value['lon']) ? $value['lon'] : '',
                            'place_id'      => $value['id_group'],
                            'flag'          => $flag
                    );
                    if (!isset($value['lat']) || empty($value['lat']))
                        unset($item['lat']);
                    if (!isset($value['lon']) || empty($value['lon']))
                        unset($item['lon']);
                    $result[] = $item;
                }
            }
        } else {
            $result = array(
                    'version'    => config_item('api_version'),
                    'status'     => STATUS_FAIL
            );
        }
        return $this->response($result);
    }

    // location
    function synchronize_object_location_get() {
        $start = intval($this->get('start'));
        $range = (intval($this->get('range')) === 0)?300:intval($this->get('range'));

        $_lastupdate = $this->get('lastupdate');
        if (isset($_lastupdate) && !empty($_lastupdate)){
            $lastupdate = date('Y-m-d H:i:s', strtotime($_lastupdate));

            $this->db->where('updated_at >',$lastupdate);
            $this->db->or_where('created_at >',$lastupdate);
            $count = $this->db->count_all_results(TBL_OBJECT);

            $this->db->where('updated_at >',$lastupdate);
            $this->db->or_where('created_at >',$lastupdate);
            $this->db->limit($range,$start);
            $object = $this->db->get(TBL_OBJECT)->result_array();

            $result = array(
                    'version'       => config_item('api_version'),
                    'status'        => STATUS_SUCCESS,
                    'lastupdate'    => $lastupdate,
                    'count'         => count($object),
                    'total'         => $count
            );
            if (isset($object) && !empty($object))
            {
                foreach ($object as $key => $value)
                {
                    $flag = isset($value['deleted_at']) ? 'D' : (($value['updated_at'] > $value['created_at']) ? 'U' : 'N');
                    $item = array(
                            'object_id'     => $value['id'],
                            'object_name'   => $value['name'],
                            'locations'         => array(),
                            'place_id'      => $value['id_group'],
                            'flag'          => $flag
                    );
                    $lat_lon = $value['position'];
                    if (isset($lat_lon) && !empty($lat_lon)){
                        if ($this->lastIndexOf($lat_lon, 'LINESTRING') != -1){
                            $lat_lon = str_replace('LINESTRING(','',$lat_lon);
                            $lat_lon = str_replace(')','',$lat_lon);
                            $array_point = explode(',', $lat_lon);
                            foreach ($array_point as $point_key=>$point_value){
                                $point_detail = explode(' ', $point_value);
                                $item['locations']['location,'.$point_key]['lat'] = $point_detail[0];
                                $item['locations']['location,'.$point_key]['lon'] = $point_detail[1];
                            }
                        } else {
                            $lat_lon = str_replace('POINT(','',$lat_lon);
                            $lat_lon = str_replace(')','',$lat_lon);
                            $point_detail = explode(' ', $lat_lon);

                            $item['locations']['location']['lat'] = $point_detail[0];
                            $item['locations']['location']['lon'] = $point_detail[1];
                        }
                    }else
                        unset($item['locations']);
                    /*if (!isset($value['lat']) || empty($value['lat']))
                        unset($item['lat']);
                    if (!isset($value['lon']) || empty($value['lon']))
                        unset($item['lon']);*/
                    $result[] = $item;
                }
            }
        } else {
            $result = array(
                    'version'    => config_item('api_version'),
                    'status'     => STATUS_FAIL
            );
        }
        return $this->response($result);
    }

    function lastIndexOf($string,$item){
        $index=strpos(strrev($string),strrev($item));
        if ($index){
            $index=strlen($string)-strlen($item)-$index;
            return $index;
        }
        else
            return -1;
    }
}
