<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

class Devices extends REST_Controller{
	function __construct() {
		parent::__construct();
	}

	/*
	* @description: register push notification for device
	* @function: register_push_nofitication_post (route key in route config)
	* @author : Le Thi Nhung (le.nhung@kloon.vn)
	* @create : 2013-11-05
	*
	* @route: mobile/device/register_push_nofitication
	* @param:
	*/
	function register_push_nofitication_post  () {
		$this->load->model('devices_model', 'device');
		$app             = trim($this->post('app'));
		$app             = in_array($app, array(APP_WINTER_TEXT, APP_FASTTASK_TEXT)) ? $app : APP_WINTER_TEXT;
		$registration_id = $this->post('registration_id');
		$device_id       = $this->post('device_id');
		$tenantId        = $this->_tenantId;

		// Check device exist
		$this->db->where('id_device', $device_id);
		$this->db->where('app', $app);
		$this->db->where('deleted_at', null);
		$checkdevice = $this->db->get(TBL_DEVICES)->row_array();
		$return = array(
			'version'  => config_item('api_version'),
			'status'   => STATUS_SUCCESS
		);
		if (!empty($checkdevice)) {
			$data = array(
				'id_unique'    => $registration_id,
				'notification' => true,
				'updated_at'   => date('Y-m-d H:i:s'),
			);
			$this->device->update_device($device_id,$data);
		} else {
			$data = array(
				'id_city'      => $tenantId,
				'id_device'    => $device_id,
				'id_unique'    => $registration_id,
				'app'          => $app,
				'notification' => true,
				'actived'      => true,
				'created_at'   => date('Y-m-d H:i:s')
			);
			$this->device->insert($data);
		}
		return  $this->response($return);
	}

	/*
	* @description: push notification to device
	* @function: send_notification
	* @author : Le Thi Nhung (le.nhung@kloon.vn)
	* @create : 2013-10-31
	*
	* @route:
	* @param: ($registration_id:is returned from Google Cloud Message ,$message:message to device)
	*/
	public function send_notification($registration_id, $message) {
		// Set POST variables
		$url = 'https://android.googleapis.com/gcm/send';

		$fields = array(
			'registration_ids' => $registration_id,
			'data' => $message,
		);

		$headers = array(
			'Authorization: key=' . GOOGLE_API_KEY,
			'Content-Type: application/json'
		);

		// Open connection
		$ch = curl_init();

		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// Disabling SSL Certificate support temporarly
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

		// Execute post
		$result = curl_exec($ch);
		return $result;
	}

	/*
	* @description: push notification to device
	* @function: push_winter_service_data_post
	* @author : Le Thi Nhung (le.nhung@kloon.vn)
	* @create : 2013-10-31
	*
	* @route: mobile/device/push_winter_service_data
	* @param:
	*/
	function push_winter_service_data_post() {
		//$registration_id = "APA91bGj9J5zZPqvzWj0o3j6rQmECW6gghSlrk2fMbFXKd9HgC0SfiR_dyY81kIuCcQo4yhXWJjdBE2gi_z6K_Krh-ldwrknw1Ne1hdQJI5byrTXwXcM25mkDh0NNVbUy95SX_8Lj89BSb-RL6mHfgFSBR91-SBGfmUVUok-PJa6yRuYNnIuXa0";
		$this->load->model('devices_model', 'device');
		$device_id = $this->post('device_id');
		$device    = $this->device->device_byid($device_id);
		if (!empty($device)) {
			$registration_id = $device['0']['id_device'];
			// Message to be sent
			$registration_id = array($registration_id);
			$message = "Disable device";
			$message = array( "disable_device" => $message);

			$result = $this->send_notification($registration_id,$message);
			if ($result === FALSE) {
				$arrayresult  = array('version'=>config_item('api_version'),'status'=>STATUS_FAIL);
			}
			else{
				$arrayresult  = array('version'=>config_item('api_version'),'status'=>STATUS_SUCCESS);

			}

		}
		else{
			$arrayresult  = array('version'=>config_item('api_version'),'status'=>STATUS_FAIL);
		}
		return $this->response($arrayresult);
	}
}
?>