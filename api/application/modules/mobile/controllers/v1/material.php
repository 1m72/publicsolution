<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
class Material extends REST_Controller
{
    function __construct()
    {
        parent::__construct();

        if(!isset($this->_tenantId) || empty($this->_tenantId)){
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL,
                    'msg'       => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /**
     * [initialize_Material_get description]
     * @param  [type]           [description]
     * @return [type]           [description]
     * @author HuongPM <phung.manh.huong@kloon.vn>
     */

    function initialize_material_get()
    {
        $this->load->model('material_model','mate');
        $tenantId = $this->_tenantId;

        if(isset($tenantId) && !empty($tenantId))
        {
            $material = $this->mate->material_get($tenantId);
            $lastupdate = get_lastupdate(TBL_MATERIAL, 'updated_at');
            $arraymaterial = array(
                'version'    => config_item('api_version'),
                'status'     => STATUS_SUCCESS,
                'lastupdate' => $lastupdate,
                'total'=>sizeof($material)
            );
            if(isset($material) && !empty($material))
            {
                $i = 4;
                foreach ($material as $item_key=>$item_value)
                {
                    $item = array(
                            'materialid'=>  $item_value['id'],
                            'materialname'  =>  $item_value['name'],
                            'unit'  =>  $item_value['unit']
                    );
                    $arraymaterial[$i] =$item;
                    $i++;
                }
            }
        }
        else
            $arraymaterial = array('version'=>config_item('api_version'),'status'=>STATUS_FAIL);
        return $this->response($arraymaterial);
    }

    /*
    * @fucntion :synchronize_material_get
    * @author   :Le Thi Nhung(le.nhung@kloon.vn)
    * @since   :2013-10-25
    */
    function synchronize_material_get(){
        $this->load->model('material_model');

        $getlastupdate = $this->get('lastupdate');
        $lastupdate = date('Y-m-d H:i:s', strtotime($getlastupdate));
        if(isset($getlastupdate) && !empty($getlastupdate) && ($getlastupdate==$lastupdate)){

            $arraymaterial  = $this->material_model->synchronize_material_get($lastupdate);
            $total = count($arraymaterial);

            $return = array(
                "version"    => config_item('api_version'),
                "status"     => STATUS_SUCCESS,
                "lastupdate" => $lastupdate,
                "total"      => $total,
            );
            foreach($arraymaterial as $key=>$value){
                    array_push($return,$value);
            }

        }
        else{
            $return = array('version'=>config_item('api_version'),'status'=>STATUS_FAIL);
        }

        return $this->response($return);
    }

}
?>