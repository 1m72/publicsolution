<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-10-31
 */
class Push_winter_service extends REST_Controller{
    private $_id_city = 0;

    function __construct(){
        parent:: __construct();

        if(isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->_id_city = intval($this->_tenantId);
        }else{
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /*
     * @description
     * @function : push_winter_service_data_post
     * @author : huongpm<phung.manh.huong@kloon.vn>
     * @create : 2013-10-31
     * @route
     * */
    function push_winter_service_data_post()
    {
        $id_device = $_POST['device_id'];
        $tenantId = $this->_tenantId;

        //$url = 'https://android.googleapis.com/gcm/send';
        //$apiKey = 'AIzaSyAnPeTnUsodCvrStKbQaAMsPOtR9JQONEg';
        $message = GOOGLE_MESSAGE;

        $data = $this->db->get_where(TBL_DEVICES,array('id_device'=>$id_device,'id_city'=>$tenantId))->result_array();

        if(isset($data) && !empty($data))
        {
            $registrationid = $data[0]['id_unique'];
            /*'APA91bGj9J5zZPqvzWj0o3j6rQmECW6gghSlrk2fMbFXKd9HgC0SfiR_dyY81kIuCcQo4yhXWJjdBE2gi_z6K_Krh-ldwrknw1Ne1hdQJI5byrTXwXcM25mkDh0NNVbUy95SX_8Lj89BSb-RL6mHfgFSBR91-SBGfmUVUok-PJa6yRuYNnIuXa0';*/
            if(isset($registrationid) && !empty($registrationid))
            {
                $fields = array(
                        'registration_ids'  => array($registrationid),
                        'data'              => array("disable_device" => $message)
                );

                $headers = array(
                        'Content-Type:application/json',
                        'Authorization:key='.GOOGLE_API_KEY
                );

                // Open connection
                $ch = curl_init();
                // Set the url, number of POST vars, POST data
                curl_setopt( $ch, CURLOPT_URL, GOOGLE_URL );
                curl_setopt( $ch, CURLOPT_POST, true );
                curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields));
                // Execute post
                $result = curl_exec($ch);
                write_log(json_encode($result),'push_server_disable_device');
                // Close connection
                curl_close($ch);
                return $this->response(array('version'=>config_item('api_version'),'status'=>STATUS_SUCCESS));
                //echo $result;
            }
            else
                return $this->response(array('version'=>config_item('api_version'),'status'=>STATUS_FAIL));
        }
        else
            return $this->response(array('version'=>config_item('api_version'),'status'=>STATUS_FAIL));
    }
}
