<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

/**
 * @author tran.duc.chien@kloon.vn
 * @created 21/10/2013
 */
class Auth extends REST_Controller {
    function __construct() {
        parent::__construct();
    }

    function test_get() {
        $this->load->model('module_model', 'module');
        _debug($this->module->select('id AS module_id, title as module_name')->get_all());
    }

    /**
     * This function is called at the first launch to send the access code that worker enters to server to validate when the application runs the first time.
     * If access code is valid, return success and tenant identification for subsequence accesses, if not return failed (text xml).
     * @return array
     */
    function validate_AccessCode_post() {
        # Library
        $this->load->library('encrypt');

        # Load model
        $this->load->model('auth_model', 'auth');

        # Post data
        $app         = trim($this->post('app'));
        $app         = in_array($app, array(APP_WINTER_TEXT, APP_FASTTASK_TEXT)) ? $app : APP_WINTER_TEXT;
        $access_code = trim($this->post('access_code'));
        $device_id   = trim($this->post('device_id'));

        # Validate access_code
        $checker = $this->auth->validate_AccessCode($access_code, $device_id, $app);
        if (!empty($checker)) {
            $return = array(
                'version'                          => config_item('api_version'),
                'status'                           => STATUS_SUCCESS,
                'tenantId'                         => isset($checker->tenantId) ? $this->encrypt->encode(trim($checker->tenantId).'|'.time()) : '',
                'allow_selection_employee_machine' => (isset($checker->allow_selection) && (intval($checker->allow_selection) > 0)) ? 'Y' : 'N',
                'has_material'                     => (isset($checker->material) && (intval($checker->material) > 0)) ? 'Y' : 'N',
                'city'                             => isset($checker->city_name) ? mb_convert_case('Stadt ' . $checker->city_name, MB_CASE_TITLE) : '',
            );

            # Modules
            if (isset($checker->tenantId)) {
                $id_city = $checker->tenantId;
                $this->db = connect_db(false, $id_city);
                $this->load->model('module_model', 'module');
                $modules = $this->module->select('id AS module_id, title as module_name')->get_many_by(array('id_parent' => 0));
                if (is_array($modules) AND !empty($modules)) {
                    foreach ($modules as $key => $value) {
                        $return['module'][] = $value;
                    }
                }
            }
        } else {
            $return = array(
                'version'  => config_item('api_version'),
                'status'   => STATUS_FAIL
            );
        }

        # Response to client
        return $this->response($return);
    }

    function login_post(){
        if(isset($this->_tenantId) && !empty($this->_tenantId)){
            $password = $this->post('password');
            $id_city = intval($this->_tenantId);
            $id_group = 2;

            $this->_db_global->where('deleted_at',NULL);
            $this->_db_global->where('id_city',$id_city);
            $this->_db_global->where('id_user_group',$id_group);
            $result_user = $this->_db_global->get(TBL_USERS)->result_array();
            if(isset($result_user) && !empty($result_user)){
                $status = STATUS_FAIL;
                foreach ($result_user as $key => $value){
                    $salt = isset($value['salt']) ? $value['salt'] : '';
                    $hash_password = md5(md5($password) . $salt);
                    if((isset($value['password']) ? $value['password'] : '') == $hash_password){
                        $status = STATUS_SUCCESS;
                        break;
                    }
                }
                $return = array(
                        'version'   => config_item('api_version'),
                        'status'    => $status
                );
                return  $this->response($return, REST_CODE_OK);
            } else {
                $return = array(
                        'version'   => config_item('api_version'),
                        'status'    => STATUS_FAIL
                );
                return  $this->response($return, REST_CODE_PARAM_ERR);
            }
        } else {
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
            );
            return  $this->response($return, REST_CODE_PARAM_ERR);
        }
    }
}
