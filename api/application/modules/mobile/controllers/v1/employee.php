<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

class Employee extends REST_Controller{
	function __construct() {
		parent::__construct();

		if(isset($this->_tenantId) && !empty($this->_tenantId))
			$this->load->model('employee_model','employee');
		else{
			$return = array(
					'version'   => config_item('api_version'),
					'status'    => STATUS_FAIL,
					'msg'       => 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	/**
	 * [Initialize_Employee_get description]
	 * @param  [type]           [description]
	 * @return [type]           [description]
	 * @author HuongPM <phung.manh.huong@kloon.vn>
	 */
	function initialize_employee_get()
	{
		$tenantId =  $this->_tenantId;

		$arrayemployee = array();
		$arrayitem = array();

		if(isset($tenantId) && !empty($tenantId))
		{
			$employee = $this->employee->employee_get($tenantId);
			$lastupdate = get_lastupdate(TBL_WORKER, 'updated_at');
			$arrayemployee = array(
					'version'    => config_item('api_version'),
					'status'     => STATUS_SUCCESS,
					'lastupdate' => $lastupdate,
					'total'      => count($employee)
			);

			if(isset($employee) && !empty($employee))
			{
				$i = 4;
				foreach ($employee as $item_key => $item_value)
				{
					$item = array(
							'employeeid'=>  $item_value['id'],
							'firstname' =>  $item_value['first_name'],
							'lastname'  =>  $item_value['last_name'],
							'address'   =>  $item_value['address'],
							'phone'     =>  $item_value['phone'],
							'activities'=> array(),
							'nfc_code' => $item_value['nfc_code']
					);
					if(isset($item_value['address']) || empty($item_value['address']))
						unset($item['address']);
					if(isset($item_value['phone']) || empty($item_value['phone']))
						unset($item['phone']);
					# Activiti
					$this->db->select('wk_ac.*');
					$this->db->from(TBL_EMPLOYEE_ACTIVITY.' as wk_ac');
					$this->db->join(TBL_ACTIVITIES.' as ac', 'ac.id = wk_ac.id_activity');
					$this->db->where('ac.deleted_at',null);
					$this->db->where('wk_ac.deleted_at',null);
					$this->db->where('wk_ac.id_employee',$item_value['id']);
					$this->db->order_by('wk_ac.id','asc');
					$activity = $this->db->get()->result_array();

					if(isset($activity) && !empty($activity))
					{
						foreach ($activity as $activity_key => $activity_value){
							$item['activities']['activityid,'.$activity_key] = $activity_value['id_activity'];
						}
						if(isset($item_value['id_default']) && !empty($item_value['id_default']))
							$item['activities']['activitydefault'] = $item_value['id_default'];
						else
							$item['activities']['activitydefault'] = 0;
					}
					else
						unset($item['activities']);
					$arrayemployee[$i] =$item;
					$i++;
				}
			}
			//$arrayemployee['rows'][0] = array('a'=>1,'b'=>2);//$arrayitem;
		}
		else
			$arrayemployee = array('version'=>config_item('api_version'),'status'=>STATUS_FAIL);

		return $this->response($arrayemployee);
	}


	/*
	* @function: synchronize_employee_get
	* @author  : Le Thi Nhung (le.nhung@kloon.vn)
	* @since   : 2013-10-25
	*/
	function synchronize_employee_get(){
		$getlastupdate = $this->get('lastupdate');
		$lastupdate = date('Y-m-d H:i:s', strtotime($getlastupdate));

		$tenantId =  $this->_tenantId;

		if((isset($getlastupdate) && !empty($getlastupdate)) && ($getlastupdate==$lastupdate) &&  (isset($tenantId) && !empty($tenantId))){

			$arrayemployee  = $this->employee->synchronize_employee_get($lastupdate, $tenantId);
			$total = count($arrayemployee);

			$return = array(
				"version"    => config_item('api_version'),
				"status"     => STATUS_SUCCESS,
				"lastupdate" => $lastupdate,
				"total"      => $total,
			);

			foreach($arrayemployee as $key=>$value){
				$item = array(
						'employeeid' => $value['employeeid'],
						'firstname' => $value['firstname'],
						'lastname' => $value['lastname'],
						'address' => $value['address'],
						'phone' => $value['phone'],
						'activities'=> array(),
						'nfc_code' => $value['nfc_code'],
						'flag' => $value['flag']
				);
				$this->db->select('wk_ac.*');
				$this->db->from(TBL_EMPLOYEE_ACTIVITY.' as wk_ac');
				$this->db->join(TBL_ACTIVITIES.' as ac', 'ac.id = wk_ac.id_activity');
				$this->db->where('ac.deleted_at',null);
				$this->db->where('wk_ac.deleted_at',null);
				$this->db->where('wk_ac.id_employee',$value['employeeid']);
				$this->db->order_by('wk_ac.id','asc');
				$activity = $this->db->get()->result_array();

				if(isset($activity) && !empty($activity))
				{
					foreach ($activity as $activity_key => $activity_value){
						$item['activities']['activityid,'.$activity_key] = $activity_value['id_activity'];
					}
					if(isset($value['id_default']) && !empty($value['id_default']))
						$item['activities']['activitydefault'] = $value['id_default'];
					else
						$item['activities']['activitydefault'] = 0;
				}
				else
					unset($item['activities']);
				$return[] =$item;
			}
		}
		else{
			$return = array('version'=>config_item('api_version'),'status'=>STATUS_FAIL);
		}

		return $this->response($return);
	}
}