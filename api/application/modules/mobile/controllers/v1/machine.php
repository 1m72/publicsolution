<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

/**
 * @author tran.duc.chien@kloon.vn
 * @created 21/10/2013
 */
class Machine extends REST_Controller {
    private $check_material = FALSE;
    function __construct() {
        parent::__construct();

        if(isset($this->_tenantId) || !empty($this->_tenantId)){
            $city_data = $this->_db_global->get_where(TBL_CITY,array('id' => intval($this->_tenantId)))->row_array();
            if(isset($city_data) && !empty($city_data))
            {
                if(isset($city_data['material']) && $city_data['material'] > 0){
                    $this->check_material = TRUE;
                }
            }
        }else{
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL,
                    'msg'       => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /**
     * This function is called at the first launch to download the list of machines that public solution company owns
     * @return array
     */
    function initialize_get() {
        # Load model
        $this->load->model('machine_model', 'machine');
        $this->load->model('material_model', 'material');
        $this->load->model('activity_model', 'activity');

        # Get id_city (TernanceID)
        $id_city = $this->_tenantId;
        # Machines data
        $machines           = $this->machine->get_machines();
        if (is_array($machines) && !empty($machines)) {
            # Return
            $lastupdate = get_lastupdate(TBL_MACHINE, 'updated_at');
            $return = array(
                'version'    => config_item('api_version'),
                'status'     => STATUS_SUCCESS,
                'lastupdate' => $lastupdate,
                'total'      => count($machines),
            );

            foreach ($machines as $key => $value) {
                $item = array(
                    'machineid'     => $value['id'],
                    'machinename'   => $value['name'],
                    'activities'    => array(),
                    'nfc_code'      => $value['nfc_code'],
                    'materials'     => array()
                );

                # Activities data
                $this->db->select('mc_ac.*');
                $this->db->from(TBL_MACHINE_ACTIVITY.' as mc_ac');
                $this->db->join(TBL_ACTIVITIES.' as ac', 'ac.id = mc_ac.id_activity');
                $this->db->where('ac.deleted_at',null);
                $this->db->where('mc_ac.deleted_at',null);
                $this->db->where('mc_ac.id_machine',$value['id']);
                $this->db->order_by('mc_ac.id','asc');
                $machine_activities = $this->db->get()->result_array();

                #$machine_activities = $this->machine->get_machine_activities();
                $activity = array();
                foreach ($machine_activities as $activity_key => $activity_value) {
                    if ($activity_value['id_machine'] == $value['id']) {
                        $item['activities']['activityid,'.$activity_key] = $activity_value['id_activity'];
                        $activity[] = $activity_value['id_activity'];
                    }
                }

                # Materials data
                $machine_materials  = $this->machine->get_machine_materials();
                foreach ($machine_materials as $material_key => $material_value) {
                    if ($material_value['id_machine'] == $value['id']) {
                        $item['materials']['materialid,'.$material_key] = $material_value['id_material'];
                    }
                }

                # Activities - Remove duplication
                if ( isset($item['activities']) AND is_array($item['activities']) AND !empty($item['activities']) ) {
                    $item['activities'] = array_unique($item['activities']);
                }

                # Materials - Remove duplication
                if ( isset($item['materials']) AND is_array($item['activities']) AND !empty($item['materials']) ) {
                    $item['materials'] = array_unique($item['materials']);
                }
                # NFC
                # Activities - Remove empty
                if (isset($item['activities']) AND empty($item['activities']) ) {
                    unset($item['activities']);
                }

                # Materials - Remove empty
                if (isset($item['materials']) AND empty($item['materials']) ) {
                    unset($item['materials']);
                }
                if(isset($activity) && !empty($activity))
                {
                    if(isset($value['id_default']) && !empty($value['id_default']))
                        $item['activitydefault'] = $value['id_default'];
                    else
                        $item['activitydefault'] = 0;
                }
                # check material
                if(!$this->check_material){
                    unset($item['materials']);
                }
                # Push item to array
                array_push($return, $item);
            }
        } else {
            $return = array(
                'version'    => config_item('api_version'),
                'status'     => STATUS_FAIL,
                'lastupdate' => date('Y-m-d H:i:s'),
            );
        }

        # Response to client
        return $this->response($return);
    }


    /*
    * @function: synchronize_machine_get
    * @author  : Le Thi Nhung (le.nhung@kloon.vn)
    * since    : 2013-10-25
    */
    function synchronize_machine_get(){
        $this->load->model('machine_model');

        $getlastupdate = $this->get('lastupdate');
        $lastupdate = date('Y-m-d H:i:s', strtotime($getlastupdate));
        if(isset($getlastupdate) && !empty($getlastupdate) && ($getlastupdate==$lastupdate)){
            $arraymachine  = $this->machine_model->synchronize_machine_get($lastupdate);

            $total = count($arraymachine);

                $return = array(
                    "version"    => config_item('api_version'),
                    "status"     => STATUS_SUCCESS,
                    "lastupdate" => $lastupdate,
                    "total"      => $total,
                );

                foreach ($arraymachine as $key => $value) {
                    $item = array(
                        'machineid'     => $value['id'],
                        'machinename'   => $value['name'],
                        'activities'    => array(),
                        'materials'     => array(),
                        'nfc_code'      => $value['nfc_code'],
                        'flag'          => $value['flag'],
                    );

                    // activities
                    $this->db->select('mc_ac.*');
                    $this->db->from(TBL_MACHINE_ACTIVITY.' as mc_ac');
                    $this->db->join(TBL_ACTIVITIES.' as ac', 'ac.id = mc_ac.id_activity');
                    $this->db->where('ac.deleted_at',null);
                    $this->db->where('mc_ac.deleted_at',null);
                    $this->db->where('mc_ac.id_machine',$value['id']);
                    $this->db->order_by('mc_ac.id','asc');
                    $machine_activities = $this->db->get()->result_array();
                    //$machine_activities = $this->machine_model->get_machine_activities();
                    $activity = array();
                    foreach ($machine_activities as $activity_key => $activity_value) {
                        if ($activity_value['id_machine'] == $value['id']) {
                            $item['activities']['activityid,'.$activity_key] = $activity_value['id_activity'];
                            $activity[] = $activity_value['id_activity'];
                        }
                    }

                    // materials
                    $machine_materials  = $this->machine_model->get_machine_materials();
                    foreach ($machine_materials as $material_key => $material_value) {
                        if ($material_value['id_machine'] == $value['id']) {
                            $item['materials']['materialid,'.$material_key] = $material_value['id_material'];
                        }
                    }
                    // activities - Remove empty and Remove duplication
                    if(empty($item["activities"])){
                        unset($item["activities"]);
                    }
                    else {
                        $item["activities"] = array_unique($item["activities"]);
                    }
                    // materials - Remove empty and Remove duplication
                    if(empty($item["materials"])){
                        unset($item["materials"]);
                    }
                    else{
                        $item["materials"]  = array_unique($item["materials"]);
                    }
                    if(isset($activity) && !empty($activity))
                    {
                        if(isset($value['id_default']) && !empty($value['id_default']))
                            $item['activitydefault'] = $value['id_default'];
                        else
                            $item['activitydefault'] = 0;
                    }
                    # check material
                    if(!$this->check_material){
                        unset($item['materials']);
                    }
                    //Push item to array
                    array_push($return, $item);

                }
        }
        else{
            $return = array('version'=>config_item('api_version'),'status'=>STATUS_FAIL);
        }

        return $this->response($return);
    }
}