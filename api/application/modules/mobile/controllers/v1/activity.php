<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
class Activity extends REST_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->helper('color');
    }

    /**
     * [initialize_Activity_get description]
     * @param  [type]           [description]
     * @return [type]           [description]
     * @author HuongPM <phung.manh.huong@kloon.vn>
     */

    function initialize_activity_get()
    {
        $this->load->model('activity_model','acti');
        $tenantId = $this->_tenantId;

        if(isset($tenantId) && !empty($tenantId))
        {
            $activity = $this->acti->activity_get($tenantId);
            $lastupdate = get_lastupdate(TBL_ACTIVITIES, 'updated_at');
            $arrayactivity = array(
                'version'    => config_item('api_version'),
                'status'     => STATUS_SUCCESS,
                'lastupdate' => $lastupdate,
                'total'=>sizeof($activity)
            );

            if(isset($activity) && !empty($activity))
            {
                $i = 4;
                foreach ($activity as $item_key=>$item_value)
                {
                    $item = array(
                        'activityid'    => $item_value['id'],
                        'activityname'  => $item_value['name']
                    );
                    if ($item_value['color']) {
                        $item['activitycolor'] = $this->_convert_color($item_value['color']);
                    }
                    $arrayactivity[$i] =$item;
                    $i++;
                }
            }
        }
        else
            $arrayactivity = array('version'=>config_item('api_version'),'status'=>STATUS_FAIL);
        return $this->response($arrayactivity);
    }

    /*
    * @function: synchronize_activity_get
    * @author  :   Le Thi Nhung (le.nhung@kloon.vn)
    * @since   :2013-10-24
    */
    function synchronize_activity_get(){
        $this->load->model('activity_model');
        $getlastupdate = $this->get('lastupdate');
        $lastupdate = date('Y-m-d H:i:s', strtotime($getlastupdate));

        if(isset($getlastupdate) && !empty($getlastupdate) && ($getlastupdate==$lastupdate)){

            $arrayactivity  = $this->activity_model->synchronize_activity_get($lastupdate);
            $total = count($arrayactivity);

            $return = array(
                "version"    => config_item('api_version'),
                "status"     => STATUS_SUCCESS,
                "lastupdate" => $lastupdate,
                "total"      => $total,
            );

            foreach($arrayactivity as $key=>$value){
                if (!$value['activitycolor']) {
                    unset($value['activitycolor']);
                } else {
                    $value['activitycolor'] = $this->_convert_color($value['activitycolor']);
                }
                array_push($return,$value);
            }

        }
        else{
            $return = array('version'=>config_item('api_version'),'status'=>STATUS_FAIL);
        }

        return $this->response($return);
    }

    function _convert_color($color) {
        $color = colorname2hex($color);
        return $color;
        $color = hex2rgb($color);
        return array(
            'red'   => $color[0],
            'green' => $color[1],
            'blue'  => $color[2],
        );
    }
}