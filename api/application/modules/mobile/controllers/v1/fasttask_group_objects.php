<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-11-27
 * */
class Fasttask_group_objects extends REST_Controller{
    private $_id_city = 0;
    function __construct(){
        parent:: __construct();

        if (isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->_id_city = intval($this->_tenantId);
        } else {
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL,
                    'msg'       => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function initialize_task_get()
    {
        $lastupdate = get_lastupdate(TBL_OBJECT_GROUP, 'updated_at');
        $obj_generaltask = $this->db->get_where(TBL_OBJECT_GROUP, array('id_parent' => 0, 'deleted_at' => null))->result_array();
        $result = array(
                'version'    => config_item('api_version'),
                'status'     => STATUS_SUCCESS,
                'lastupdate' => isset($lastupdate) ? $lastupdate : '',
                'total'      => count($obj_generaltask)
        );
        if (isset($obj_generaltask) && !empty($obj_generaltask))
        {
            foreach ($obj_generaltask as $key=>$value)
            {
                $item = array(
                        'general_task_id'   => $value['id'],
                        'task_name'         => $value['name']
                );
                $result[] = $item;
            }
        }
        return $this->response($result);
    }

    function synchronize_task_get()
    {
        $_lastupdate = $this->get('lastupdate');
        if (isset($_lastupdate) && !empty($_lastupdate)){
            $start = intval($this->get('start'));
            $range = (intval($this->get('range')) === 0)?1000:intval($this->get('range'));

            $lastupdate = date('Y-m-d H:i:s', strtotime($_lastupdate));

            # Total
            $sql = "SELECT count(id) as `total` FROM ".TBL_OBJECT_GROUP." WHERE id_parent = 0 AND (created_at > '". $lastupdate ."' OR updated_at > '".$lastupdate."')";
            $obj_generaltask_total = $this->db->query($sql)->row_array();

            # Result
            $sql = "SELECT * FROM ".TBL_OBJECT_GROUP." WHERE id_parent = 0 AND (created_at > '". $lastupdate ."' OR updated_at > '".$lastupdate."') LIMIT {$start}, {$range}";
            $obj_generaltask = $this->db->query($sql)->result_array();

            $result = array(
                'version'    => config_item('api_version'),
                'status'     => STATUS_SUCCESS,
                'lastupdate' => $lastupdate,
                'total'      => $obj_generaltask_total['total']
            );
            if (isset($obj_generaltask) && !empty($obj_generaltask))
            {
                foreach ($obj_generaltask as $key=>$value)
                {
                    $flag = isset($value['deleted_at']) ? 'D' : (($value['updated_at'] > $value['created_at']) ? 'U' : 'N');
                    $item = array(
                        'general_task_id' => $value['id'],
                        'task_name'       => $value['name'],
                        'flag'            => $flag
                    );
                    $result[] = $item;
                }
            }
        } else {
            $result = array(
                    'version'    => config_item('api_version'),
                    'status'     => STATUS_FAIL
            );
        }
        return $this->response($result);
    }

    function initialize_group_objects_get()
    {
        $lastupdate = get_lastupdate(TBL_OBJECT_GROUP, 'updated_at');
        $obj_group = $this->db->get_where(TBL_OBJECT_GROUP,array('id_parent >'=>0))->result_array();
        $result = array(
                'version'    => config_item('api_version'),
                'status'     => STATUS_SUCCESS,
                'lastupdate' => isset($lastupdate) ? $lastupdate : '',
                'total'      => count($obj_group)
        );
        if (isset($obj_group) && !empty($obj_group))
        {
            foreach ($obj_group as $key=>$value)
            {
                $item = array(
                        'group_objects_id'      => $value['id'],
                        'group_objects_name'    => $value['name'],
                        'general_task_id'       => $value['id_parent']
                );
                $result[] = $item;
            }
        }

        return $this->response($result);
    }

    function synchronize_group_objects_get()
    {
        $_lastupdate = $this->get('lastupdate');
        if (isset($_lastupdate) && !empty($_lastupdate)){
            $lastupdate = date('Y-m-d H:i:s', strtotime($_lastupdate));

            $sql = "SELECT * FROM ".TBL_OBJECT_GROUP." WHERE id_parent > 0 AND (created_at > '". $lastupdate ."' OR updated_at > '".$lastupdate."')";
            $obj_group = $this->db->query($sql)->result_array();

            $result = array(
                    'version'    => config_item('api_version'),
                    'status'     => STATUS_SUCCESS,
                    'lastupdate' => $lastupdate,
                    'total'      => count($obj_group)
            );
            if (isset($obj_group) && !empty($obj_group))
            {
                foreach ($obj_group as $key=>$value)
                {
                    $flag = isset($value['deleted_at']) ? 'D' : (($value['updated_at'] > $value['created_at']) ? 'U' : 'N');
                    $item = array(
                            'group_objects_id'      => $value['id'],
                            'group_objects_name'    => $value['name'],
                            'general_task_id'       => $value['id_parent'],
                            'flag'              => $flag
                    );
                    $result[] = $item;
                }
            }
        } else {
            $result = array(
                    'version'    => config_item('api_version'),
                    'status'     => STATUS_FAIL
            );
        }
        return $this->response($result);
    }
}
