<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

/**
 * Controller for sync data from mobile apps
 * @author tran.duc.chien@kloon.vn
 * @created 21/10/2013
 */
class Synchronize extends REST_Controller {
	private $_id_city                    = -1;
	private $_path_service               = 'http://services.gisgraphy.com/street/streetsearch?format=json';
	private $_upload_path                = '';
	private $_sync_winter_dir            = '';
	private $_sync_winter_activities_dir = '';
	private $services = array(
		'kloon'         => 'http://nominatim.kloon.net/reverse?format=json&zoom=17&addressdetails=1',
		'mapquest'      => 'http://open.mapquestapi.com/nominatim/v1/reverse.php?format=json&zoom=17',
		'openstreetmap' => 'http://nominatim.openstreetmap.org/reverse?format=json&zoom=17&addressdetails=1',
		'gisgraphy'     => 'http://services.gisgraphy.com/street/streetsearch?format=json&from=1&to=1',
	);

	function __construct() {
		parent::__construct();

		if(isset($this->_tenantId) && !empty($this->_tenantId)){
			$this->_id_city                     = intval($this->_tenantId);
			$this->_upload_path                 = config_item('upload_dir');
			$this->_sync_winter_dir             = config_item('sync_winter_dir');
			$this->_sync_winter_activities_dir  = config_item('sync_winter_activities_dir');
		}else{
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	/**
	 * Synchronize data from mobile apps
	 * @return array Status for API
	 * @author HuongPM <phung.manh.huong@kloon.vn>
	 * @created  21 Nov 2013
	 */
	function update_winter_service_task_post() {
		$xml_data = urldecode($_POST['xml']);

		# Logs
		$this->_save_to_files($xml_data);

		$data = $this->format->factory($xml_data, 'xml')->to_array();
		foreach ($data as $item)
		{
			# Transaction start
			$this->db->trans_start();

			$id_gps           = 0;
			$id_task_activity = 0;
			$position         = '';
			$taskid           = (isset($item['taskid']) && !empty($item['taskid'])) ? $item['taskid'] : '';
			$employeeid       = (isset($item['employeeid']) && !empty($item['employeeid'])) ? $item['employeeid'] : '';
			$machineid        = (isset($item['machineid']) && !empty($item['machineid'])) ? $item['machineid'] : '';
			$start_time       = (isset($item['starttime']) && !empty($item['starttime'])) ? $item['starttime'] : '';
			$end_time         = (isset($item['endtime']) && !empty($item['endtime'])) ? $item['endtime'] : '';

			$this->db->where('id_task',$taskid);
			$gps_detail = $this->db->get(TBL_WORKER_GPS)->row_array();
			if(isset($gps_detail) && !empty($gps_detail)){
				$id_gps = $gps_detail['id'];
			} else{
				$data_gps = array(
					'id_city'    => $this->_id_city,
					'id_task'    => $taskid,
					'id_object'  => 0,
					'id_worker'  => $employeeid,
					'id_machine' => $machineid,
					'ontime'     => $start_time,
					'created_at' => date('Y-m-d H:i:s')
				);
				if($this->db->insert(TBL_WORKER_GPS,$data_gps)){
					$id_gps = $this->db->insert_id();
				}
			}

			# materials
			$array_materials = array();
			if(isset($item['materials']) && !empty($item['materials'])) {
				$materials = $item['materials'];
				$materials = $this->_walking_nodes($item['materials'], 'materialid', 'material');
			}
			if(isset($materials) && !empty($materials)) {
				foreach ($materials as $it_material) {
					array_push(
						$array_materials,
						array(
							'id_city'       => $this->_id_city,
							'id_material'   => isset($it_material['materialid']) ? $it_material['materialid'] : '',
							'id_worker_gps' => $id_gps,
							'amount'        => isset($it_material['amount']) ? $it_material['amount'] : '',
							'created_at'    => date('Y-mn-d H:i:s')
						)
					);
				}
			}

			#--- Prepare for email notification
			$array_data_mail   = array();
			$array_information = array();

			# messages
			$messages = array();
			if(isset($item['messages']) && !empty($item['messages'])) {
				$messages = $item['messages'];
				$messages = $this->_walking_nodes($item['messages'], 'text', 'message');
			}
			if(isset($messages) && !empty($messages)) {
				foreach ($messages as $it_message) {
					array_push(
						$array_information,
						array(
							'id_city'       => $this->_id_city,
							'id_config'     => 1,
							'id_worker_gps' => $id_gps,
							'type'          => INFO_MESSAGE,
							'data'          => isset($it_message['text']) ? $it_message['text'] : '',
							'lat'           => isset($it_message['lat']) ? $it_message['lat'] : '',
							'lon'           => isset($it_message['lon']) ? $it_message['lon'] : '',
							'time'          => isset($it_message['time']) ? $it_message['time'] : '',
							'rotate'        => '',
							'created_at'    => date('Y-m-d H:i:s')
						)
					);

					# mail
					$data_email = json_encode(array(
						'id_city'       => $this->_id_city,
						'taskid'        => $taskid,
						'employeeid'    => $employeeid,
						'machineid'     => $machineid,
						'type'          => INFO_MESSAGE,
						'lat'           => isset($it_message['lat']) ? $it_message['lat'] : '',
						'lon'           => isset($it_message['lon']) ? $it_message['lon'] : '',
						'data'          => isset($it_message['text']) ? $it_message['text'] : '',
						#'created_at'    => isset($it_message['time']) ? $it_message['time'] : ''
						'created_at'    => date('Y-m-d H:i:s')
					));
					$array_data_mail[] = array(
							'data' => $data_email,
							'created_at'    => date('Y-m-d H:i:s')
					);
				}
			}

			# images
			$images = array();
			if(isset($item['images']) && !empty($item['images'])) {
				$images = $item['images'];
				$images = $this->_walking_nodes($item['images'], 'imagename', 'image');
			}
			$image_rotate_array = array();
			if(isset($images) && !empty($images)) {
				foreach ($images as $it_image) {
					array_push(
						$array_information,
						array(
							'id_city'           => $this->_id_city,
							'id_config'         => 1,
							'id_worker_gps'     => $id_gps,
							'type'              => INFO_IMAGE,
							'data'              => isset($it_image['imagename']) ? $taskid.'_'.$it_image['imagename'].'.jpg' : '',
							'lat'               => isset($it_image['lat']) ? $it_image['lat'] : '',
							'lon'               => isset($it_image['lon']) ? $it_image['lon'] : '',
							'time'              => isset($it_image['time']) ? $it_image['time'] : '',
							'rotate'            => isset($it_image['orientation_rotate']) ? $it_image['orientation_rotate'] : '',
							'created_at'        => date('Y-m-d H:i:s')
						)
					);
					#
					if(isset($it_image['orientation_rotate']) && !empty($it_image['orientation_rotate'])){
						$full_path = $this->_upload_path.$this->_tenantId.'/'.date('Y/m/d',strtotime($start_time)).'/'.$employeeid.'/'.(isset($it_image['imagename']) ? $taskid.'_'.$it_image['imagename'].'.jpg' : '');
						$image_rotate_array[] = array(
								'id_city'   => $this->_id_city,
								'type'      => 'rotate',
								'data'      => json_encode(array(
									'full_path' => $full_path,
									'rotate'    => $it_image['orientation_rotate']
								)),
								'status'     => CRON_STATUS_PENDING,
								'created_at' => date('Y-m-d H:i:s')
						);
					}
					# mail
					$tmp = $this->_upload_path.$this->_tenantId.'/'.date('Y/m/d',strtotime($start_time)).'/'.$employeeid.'/'.(isset($it_image['imagename']) ? $taskid.'_'.$it_image['imagename'].'.jpg' : '');
					$data_email = json_encode(array(
						'id_city'       => $this->_id_city,
						'taskid'        => $taskid,
						'employeeid'    => $employeeid,
						'machineid'     => $machineid,
						'type'          => INFO_IMAGE,
						'lat'               => isset($it_image['lat']) ? $it_image['lat'] : '',
						'lon'               => isset($it_image['lon']) ? $it_image['lon'] : '',
						'data'          => $tmp,
						#'created_at'    => isset($it_image['time']) ? $it_image['time'] : ''
						'created_at'    => date('Y-m-d H:i:s')
					));
					$array_data_mail[] = array(
						'attach' => $tmp,
						'data'   => $data_email,
						'created_at'    => date('Y-m-d H:i:s')
					);
				}
			}

			# voices
			$voices = array();
			$voice_convert_array = array();
			if(isset($item['voices']) && !empty($item['voices'])){
				$voices = $item['voices'];
				$voices = $this->_walking_nodes($item['voices'], 'voicename', 'voice');
			}
			if(isset($voices) && !empty($voices)) {
				foreach ($voices as $it_voice) {
					array_push(
						$array_information,
						array(
							'id_city'           => $this->_id_city,
							'id_config'         => 1,
							'id_worker_gps'     => $id_gps,
							'type'              => INFO_VOICE,
							'data'              => isset($it_voice['voicename']) ? $taskid.'_'.$it_voice['voicename'].'.3gp' : '',
							'lat'               => isset($it_voice['lat']) ? $it_voice['lat'] : '',
							'lon'               => isset($it_voice['lon']) ? $it_voice['lon'] : '',
							'time'              => isset($it_voice['time']) ? $it_voice['time'] : '',
							'rotate'            => '',
							'created_at'        => date('Y-m-d H:i:s')
						)
					);
					$full_path = $this->_upload_path.$this->_tenantId.'/'.date('Y/m/d',strtotime($start_time)).'/'.$employeeid.'/'.(isset($it_voice['voicename']) ? $taskid.'_'.$it_voice['voicename'].'.3gp' : '');
					$voice_convert_array[] = array(
							'id_city'   => $this->_id_city,
							'type'      => 'voice',
							'data'      => $full_path,
							'status'    => CRON_STATUS_PENDING,
							'created_at'    => date('Y-m-d H:i:s')
					);
					# mail
					$tmp = $this->_upload_path.$this->_tenantId.'/'.date('Y/m/d',strtotime($start_time)).'/'.$employeeid.'/'.(isset($it_voice['voicename']) ? $taskid.'_'.$it_voice['voicename'].'.3gp' : '');
					$data_email = json_encode(array(
						'id_city'       => $this->_id_city,
						'taskid'        => $taskid,
						'employeeid'    => $employeeid,
						'machineid'     => $machineid,
						'type'          => INFO_VOICE,
						'lat'           => isset($it_voice['lat']) ? $it_voice['lat'] : '',
						'lon'           => isset($it_voice['lon']) ? $it_voice['lon'] : '',
						'data'          => $tmp,
						#'created_at'    => isset($it_voice['time']) ? $it_voice['time'] : ''
						'created_at'    => date('Y-m-d H:i:s')
					));
					$array_data_mail[] = array(
						'attach'        => $tmp,
						'data'          => $data_email,
						'created_at'    => date('Y-m-d H:i:s')
					);
				}
			}

			# movies
			$movies = array();
			if(isset($item['movies']) && !empty($item['movies'])) {
				$movies = $item['movies'];
				$movies = $this->_walking_nodes($item['movies'], 'moviename', 'movie');
			}
			if(isset($movies) && !empty($movies)) {
				foreach ($movies as $it_movie) {
					array_push(
						$array_information,
						array(
							'id_city'           => $this->_id_city,
							'id_config'         => 1,
							'id_worker_gps'     => $id_gps,
							'type'              => INFO_VIDEO,
							'data'              => isset($it_movie['moviename']) ? $taskid.'_'.$it_movie['moviename'].'.mp4' : '',
							'lat'               => isset($it_movie['lat']) ? $it_movie['lat'] : '',
							'lon'               => isset($it_movie['lon']) ? $it_movie['lon'] : '',
							'time'              => isset($it_movie['time']) ? $it_movie['time'] : '',
							'rotate'            => '',
							'created_at'        => date('Y-m-d H:i:s')
						)
					);
					# mail
					$data_email = json_encode(array(
						'id_city'     => $this->_id_city,
						'taskid'      => $taskid,
						'employeeid'  => $employeeid,
						'machineid'   => $machineid,
						'type'        => INFO_VIDEO,
						'lat'         => isset($it_movie['lat']) ? $it_movie['lat'] : '',
						'lon'         => isset($it_movie['lon']) ? $it_movie['lon'] : '',
						'data'        => $this->_upload_path.$this->_tenantId.'/'.date('Y/m/d',strtotime($start_time)).'/'.$employeeid.'/'.(isset($it_movie['moviename']) ? $taskid.'_'.$it_movie['moviename'].'.mp4' : ''),
						'created_at'  => date('Y-m-d H:i:s')
					));
					$array_data_mail[] = array(
						'data'       => $data_email,
						'created_at' => date('Y-m-d H:i:s')
					);
				}
			}

			# insert information
			if(isset($array_information) && !empty($array_information)) {
				if($this->db->insert_batch(TBL_WORKER_INFORMATION,$array_information) === FALSE){
					write_log('insert information_'.$taskid.'_'.json_encode($array_information), 'update_winter_service');
				}
			}

			# Image rotate
			if(isset($image_rotate_array) && !empty($image_rotate_array)){
				# Insert to cron table
				if($this->_db_global->insert_batch(TBL_CRON,$image_rotate_array) === FALSE){
					write_log('Insert cron rotate '.$taskid.'_'.json_encode($image_rotate_array), 'update_winter_service');
				}

				# Push to beanstalk
				if (is_array($image_rotate_array) AND !empty($image_rotate_array)) {
					foreach ($image_rotate_array as $key => $value) {
						$data = json_decode($value->data);
						$job = array(
							'id'     => false,
							'type'   => INFO_IMAGE,
							'path'   => isset($data->full_path) ? $data->full_path : false,
							'rotate' => isset($data->rotate) ? $data->rotate : false,
						);
						push_job(QUE_MEDIA, json_encode($job));
					}
				}
			}

			# Voice convert
			if(isset($voice_convert_array) && !empty($voice_convert_array)){
				# Insert to cron table
				if($this->_db_global->insert_batch(TBL_CRON,$voice_convert_array) === FALSE){
					write_log('Insert cron convert '.$taskid.'_'.json_encode($voice_convert_array), 'update_winter_service');
				}

				# Push to beanstalk
				if (is_array($image_rotate_array) AND !empty($image_rotate_array)) {
					foreach ($image_rotate_array as $key => $value) {
						$data = json_decode($value->data);
						$job = array(
							'id'   => false,
							'type' => INFO_VOICE,
							'path' => isset($data->full_path) ? $data->full_path : false,
						);
						push_job(QUE_MEDIA, json_encode($job));
					}
				}
			}

			# insert materials
			if(isset($array_materials) && !empty($array_materials)){
				if($this->db->insert_batch(TBL_MACHINE_MATERIAL_USED,$array_materials) === FALSE)
				{
					write_log('insert material_'.$taskid.'_'.json_encode($array_materials), 'update_winter_service');
				}
			}

			# insert data mail
			if(isset($array_data_mail) && !empty($array_data_mail)){
				# Push to queue
				if (is_array($array_data_mail) AND !empty($array_data_mail)) {
					foreach ($array_data_mail as $key => $value) {
						# DB
						$db_flag = $this->_db_global->insert(TBL_EMAIL, $value);
						if ($db_flag === false) {
							write_log("Insert email data synchronize FAIL _ id_task : {$taskid} _ Data: ".json_encode($value)." _ Query : " . $this->_db_global->last_query(), 'update_winter_service');
						} else {
							write_log("Insert email data synchronize OK _ id_task : {$taskid}", 'update_winter_service');

							# Beanstalk
							$job = $this->_db_global->insert_id();
							$job_flag = push_job(QUE_EMAIL_EXTRACT, $job);
							write_log("Push job to que " . QUE_EMAIL_EXTRACT. ' : ' . ($job_flag === false ? 'FAIL' : 'OK'), 'update_winter_service');
						}

					}
				}
			}

			# transaction end
			$this->db->trans_complete();

			# Log errors if transaction errors
			if ($this->db->trans_status() === FALSE) {
				write_log('Transaction error_'.json_encode($data), 'update_winter_service_task');
				return $this->response(
						array(
							'status' => STATUS_FAIL,
							'msg'    => 'Has an error when sync data to DB. This error has been collected.'
						)
				);
			} else {
				return $this->response(
						array(
								'status' => STATUS_SUCCESS,
								'msg'    => 'Data has been synchronized'
						)
				);
			}
		}
	}

	function upload_tour_data_post(){
		$this->load->helper('pheanstalk');

		$resources = isset($_FILES) ? $_FILES : '';
		$upload_path = $this->_sync_winter_activities_dir;
		$status      = FALSE;
		$msg         = '';
		if ( isset($resources) AND is_array($resources) AND (!empty($resources)) ) {
			$file_infor_array = array();
			foreach ($resources as $key => $value) {
				$upload_path_tmp = $upload_path. date('Y/m/d');
				$result = $this->_upload_process($key, $upload_path_tmp);
				if(file_exists($result['full_path'])) {
					$file_name          = date('YmdHis').'_'.$result['file_name'];
					$file_path          = $result['file_path'].$file_name;
					$flag               = rename($result['full_path'], $file_path);
					$file_path_relative = trim($upload_path_tmp, '/') . '/' . $file_name;
					$file_path_relative = str_replace("//", "/", $file_path_relative);
					if ($flag) {
						$item = array(
							'id_city'       => $this->_id_city,
							'type'          => 'activity',
							'data'          => $file_path_relative,
							'status'        => CRON_STATUS_PENDING,
							'created_at'    => date('Y-m-d H:i:s')
						);
						#push_job('upload_tour_data', json_encode($item));
						$file_infor_array[] = $item;
						$status = TRUE;
						$msg    = 'Data has been received';
					} else{
						write_log('Can\'t rename file upload_Task: '.date('YmdHis').'_Result: '.json_encode($result), '_upload_tour_data');
					}
				} else {
					write_log('File upload not exit_Task: '.date('YmdHis').'_Result: '.json_encode($result), '_upload_tour_data');
				}
			}
			if(isset($file_infor_array) && !empty($file_infor_array)){
				foreach ($file_infor_array as $key => $value) {
					$data_cron_insert = array(
						'id_city'       => $this->_id_city,
						'type'          => 'activity',
						'data'          => $value['data'],
						'status'        => CRON_STATUS_PENDING,
						'created_at'    => date('Y-m-d H:i:s')
					);
					$flag = $this->_db_global->insert(TBL_CRON,$data_cron_insert);
					if($flag != FALSE){
						write_log('insert ' . TBL_CRON . ' OK data :'.json_encode($data_cron_insert), 'add_push_job');
						$id_cron = $this->_db_global->insert_id();
						push_job(QUE_SYNC, json_encode($data_cron_insert));
						write_log('Add queue ' . QUE_SYNC . ' FAIL data :'.json_encode($data_cron_insert), 'add_push_job');
						$cron_push_job_data = array(
							'id_city'   => $this->_id_city,
							'id_cron'   => $id_cron,
							'data'      => $value['data']
						);
						$flag = $this->_db_global->insert(TBL_CRON_PUS_JOBS,$cron_push_job_data);
						if($flag != FALSE){
							write_log('insert ' . TBL_CRON_PUS_JOBS . ' OK data :'.json_encode($cron_push_job_data), 'add_push_job');
						} else {
							write_log('insert ' . TBL_CRON_PUS_JOBS . ' FAIL data :'.json_encode($cron_push_job_data), 'add_push_job');
						}
					} else {
						write_log('insert ' . TBL_CRON . ' FAIL data :'.json_encode($data_cron_insert), 'add_push_job');
					}
				}
			}
		} else {
			$status = false;
		}

		# return
		return $this->response(
			array(
				'status'    => $status,
				'msg'       => $msg
			)
		);
	}

	/**
	 * Process sync activities
	 * @author Chien Tran <tran.duc.chien@kloon.vn>
	 * @param  boolean $file_path [description]
	 * @return [type]             [description]
	 */
	function _sync_activity($file_path = FALSE){
		#$file_path = get_server_path() . 'uploads/activities/2014/01/21/20140121074624_tour_data.txt';
		#$file_path = get_server_path() . 'uploads/activities/2014/01/21/20140121075127_tour_data.txt';

		$status = TRUE;
		$msg    = '';

		# Check file path
		if (!$file_path OR (file_exists($file_path) == FALSE) ) {
			return array(
				'status' => FALSE,
				'msg' => 'File not exist '.$file_path
			);
		}

		# Helper
		$this->load->helper('file');

		# Read content from file
		$json_text = read_file($file_path);
		if (!$json_text OR empty($json_text)) {
			return array(
				'status' => FALSE,
				'msg' => 'Can not read file '.$file_path
			);
		}

		# Decode data
		$json_data = json_decode($json_text, TRUE);
		if (!is_array($json_data)) {
			return array(
				'status' => FALSE,
				'msg' => 'File data is format invalid'
			);
		}

		# Transaction start
		$this->db->trans_start();

		# Params
		$id_task     = isset($json_data['task_id'])     ? $json_data['task_id']     : '';
		$id_employee = isset($json_data['employee_id']) ? $json_data['employee_id'] : '';
		$id_machine  = isset($json_data['machine_id'])  ? $json_data['machine_id']  : '';
		$end_time    = isset($json_data['end_time'])    ? $json_data['end_time']    : '';
		$start_time  = isset($json_data['start_time'])  ? $json_data['start_time']  : '';

		# Prepare gps detail record
		$gps    = $this->db->get_where(TBL_WORKER_GPS, array('id_task' => $id_task))->row_array();
		$id_gps = NULL;
		if (is_array($gps) AND !empty($gps)) {
			$id_gps = isset($gps['id']) ? $gps['id'] : NULL;
		} else {
			$gps_data = array(
				'id_city'    => $this->_id_city,
				'id_task'    => $id_task,
				'id_object'  => 0,
				'id_worker'  => $id_employee,
				'id_machine' => $id_machine,
				'ontime'     => $start_time,
				'created_at' => date('Y-m-d H:i:s')
			);
			$db_flag = $this->db->insert(TBL_WORKER_GPS, $gps_data);
			if($db_flag !== false){
				$id_gps         = $this->db->insert_id();
				$gps_data['id'] = $id_gps;
				$gps            = $gps_data;
			} else {
				return array(
					'status' => FALSE,
					'msg'    => 'Cant not insert new gps _ Query: ' .$this->db->last_query(). ' _ Data: '.json_encode($gps_data)
				);
			}
		}

		# Check id_gps
		if (is_null($id_gps) OR (intval($id_gps) <= 0)) {
			return array(
				'status' => FALSE,
				'msg'    => 'id_gps is invalid_Value: '.$id_gps
			);
		}

		$activities = array();
		if(!isset($json_data['activities']) OR empty($json_data['activities'])){
			return array(
				'status' => FALSE,
				'msg'    => 'Activity data is not exist'
			);
		}

		$activities = $this->_walking_nodes($json_data['activities'], 'activityid', 'activitie');
		if (!is_array($activities) OR empty($activities)) {
			return array(
				'status' => FALSE,
				'msg'    => 'Activity data parse is invalid_Data: '.json_encode($activities)
			);
		}

		# Position string will be append to current in worker_gps.position
		$position_new = '';

		# Walking activities elements
		foreach ($activities as $key_activity => $activity){
			$activity_id               = isset($activity['activityid'])     ? trim($activity['activityid'])     : '';
			$activity_starttime        = isset($activity['starttime'])      ? trim($activity['starttime'])      : '';
			$activity_endtime          = isset($activity['endtime'])        ? trim($activity['endtime'])        : '';
			$activity_task_activity_id = isset($activity['taskactivityid']) ? trim($activity['taskactivityid']) : '';

			# Last data
			$last_street   = '';
			$last_position = array();

			# Check activity exist
			$this->db->where('id_task', $id_task);
			$this->db->where('id_task_activity', $activity_task_activity_id);
			$activity_exist = $this->db->get(TBL_WORKER_ACTIVITY)->row_array();
			if (is_array($activity_exist) AND !empty($activity_exist)) {
				$last_position   = isset($activity_exist['end_lat_lon']) ? json_decode($activity_exist['end_lat_lon'], TRUE) : '';
				if (!is_array($last_position)) {
					$last_position = array();
				}
				$last_street     = isset($activity_exist['street']) ? $activity_exist['street'] : '';
			} else {}

			#--- Array data for batch process
			# Activities will be update to worker_activitiess
			$activities_update = array();

			# Activities will be add news to worker_activitiess
			$activities_new    = array();

			# Data location end will be update to worker_gps by id_task_activity
			$location_ends     = array();

			# Locations process
			$locations = array();
			if(isset($activity['locations']) && !empty($activity['locations'])){
				$locations = $activity['locations'];
				$locations = $this->_walking_nodes($locations, 'lat', 'location');
				if (is_array($locations) AND !empty($locations)) {
					$locations = array_merge($last_position, $locations);
					$locations = array_values($locations);
					$streets   = $this->_check_street($locations, $activity_starttime, $activity_endtime);

					# Streets walking
					if (is_array($streets) AND !empty($streets)) {
						foreach ($streets as $key_street => $street) {
							$current_street = isset($street['street']) ? $street['street'] : '';
							# If first element on same street with last record then update time, task_activity_id
							# Else create new record for worker_activities
							if ( ($key_street == 0) AND ($last_street != '') AND ($last_street == $current_street) ) {
								$activities_update[] = array(
									'id_task_activity' => $activity_task_activity_id,
									'endtime'          => isset($street['end']) ? $street['end'] : '',
									'updated_at'       => date('Y-m-d H:i:s')
								);
							} else {
								$activities_new[] = array(
									'id_city'          => $this->_id_city,
									'id_wkgps'         => $id_gps,
									'id_task'          => $id_task,
									'id_task_activity' => $activity_task_activity_id,
									'street'           => isset($street['street']) ? $street['street'] : '',
									'latitude'         => isset($street['lat']) ? $street['lat'] : '',
									'longtitude'       => isset($street['lon']) ? $street['lon'] : '',
									'id_activity'      => $activity_id,
									'starttime'        => isset($street['start']) ? $street['start'] : '',
									'endtime'          => isset($street['end']) ? $street['end'] : '',
									'created_at'       => date('Y-m-d H:i:s')
								);
							}
						}
					}

					# Position walking - Update end positionto field worker_gps.end_lat_lon for next checking
					$locations       = array_values($locations);
					$location_end    = end($locations);
					if (is_array($location_end) AND !empty($location_end)) {
						$location_end    = array(
							'id_task_activity' => $activity_task_activity_id,
							'end_lat_lon'      => json_encode($location_end),
							'updated_at'       => date('Y-m-d H:i:s')
						);
						$location_ends[] = $location_end;
					}

					# Position new
					foreach ($locations as $key_location => $value_location) {
						$tmp_lat  = isset($value_location['lat'])  ? $value_location['lat']  : '';
						$tmp_lon  = isset($value_location['lon'])  ? $value_location['lon']  : '';
						$tmp_time = isset($value_location['time']) ? $value_location['time'] : '';

						if ( (empty($tmp_lat) != FALSE) AND (empty($tmp_lon) != FALSE) AND (empty($tmp_time) != FALSE) ) {
							$position_new .= trim("{$tmp_lat} {$tmp_lon} {$tmp_time},");
						}
					}
				}
			}

			#--- Batch process
			# Insert into worker_activities
			if ( is_array($activities_new) AND !empty($activities_new) ) {
				$db_flag = $this->db->insert_batch(TBL_WORKER_ACTIVITY, $activities_new);
				if ($db_flag === FALSE) {
					write_log('Insert worker activities fail_'.$id_task.'_Query: '.$this->db->last_query().'_Data: '.json_encode($activities_new), 'sync_activities');
				}
			}

			# Update end_time to worker_activities
			if ( is_array($activities_update) AND !empty($activities_update) ) {
				$db_flag = $this->db->update_batch(TBL_WORKER_ACTIVITY, $activities_update);
				if ($db_flag === FALSE) {
					write_log('Update worker activities fail_'.$id_task.'_Query: '.$this->db->last_query().'_Data: '.json_encode($activities_update), 'sync_activities');
				}
			}

			# Update end_lat_long to worker_gps
			if ( is_array($location_ends) AND !empty($location_ends) ) {
				$db_flag = $this->db->update_batch(TBL_WORKER_ACTIVITY, $location_ends, 'id_task_activity');
				if ($db_flag === FALSE) {
					write_log('update end lat lon activity_'.$id_task.'_'.json_encode($location_ends).'_Query: '.$this->db->last_query(), 'sync_activities');
				}
			}
		}

		# Append position_new to worker_gps.position
		$position_new = trim($position_new);
		if (!empty($position_new)) {
			$this->db->set('position', "CONCAT(position, '{$position_new}')", FALSE);
			$this->db->set('updated_at', date('Y-m-d H:i:s'));
			$this->db->where('id', $id_gps);
			$db_flag = $this->db->update(TBL_WORKER_GPS);
			if(!$db_flag){
				write_log('Update position activity fail_'.$id_task.'_Data: '.json_encode($position), 'sync_activities');
				return array(
					'status' => FALSE,
					'msg'    => 'Has an error when saving position !'
				);
			}
		}

		# Transaction end
		$this->db->trans_complete();

		return array(
			'status' => TRUE,
			'msg'    => 'The data has been successfully process'
		);
	}

	# Insert Activiti and position
	function _add_activiti($file_path = '',$id_task = 0,$id_employee = 0,$id_machine = 0,$start_time = NULL, $end_time = NULL){
		$return = array(
				'status'    => TRUE,
				'msg'       => ''
		);
		if(isset($file_path) && !empty($file_path)){
			$json_text = file_get_contents($file_path);
			if(isset($json_text) && !empty($json_text)){
				$data = json_decode($json_text,TRUE);
				if(isset($data) && !empty($data)){
					$id_gps = 0;
					$this->db->where('id_task',$id_task);
					$gps_detail = $this->db->get(TBL_WORKER_GPS)->row_array();
					if(isset($gps_detail) && !empty($gps_detail)){
						$id_gps = $gps_detail['id'];
					}else{
						$data_gps = array('id_city'=>$this->_id_city,
								'id_task'   => $id_task,
								'id_object' => 0,
								'id_worker' => $id_employee,
								'id_machine'=> $id_machine,
								'ontime'    => $start_time,
								'created_at'=> date('Y-m-d H:i:s')

						);
						if($this->db->insert(TBL_WORKER_GPS,$data_gps)){
							$id_gps = $this->db->insert_id();
						}
					}
					if(intval($id_gps) > 0){
						#position
						$position = isset($gps_detail['position']) ? $gps_detail['position'] : '';
						# check split
						$split = $this->db->get_where(TBL_WORKER_GPS_SPLIT,array('id_task'=>$id_task))->row_array();
						$check_split = FALSE;
						$id_task_activity = '';
						if(isset($split) && !empty($split)){
							$check_split = TRUE;
							$id_task_activity = $split['id_task_activity'];
						}
						foreach ($data as $row){
							$start_time         = $row['starttime'];
							$end_time           = $row['endtime'];
							$task_activity_id   = $row['taskactivityid'];
							$activity_id        = $row['activityid'];
							# Get Location
							$locations = array();
							if(isset($row['locations']) && !empty($row['locations'])){
								$locations = $row['locations'];
								$locations = $this->_walking_nodes($locations, 'lat', 'location');
							}
							#
							$array_activities = array();
							if(isset($locations) && !empty($locations)){
								# Activity
								$street_ac = $this->_check_street($locations);
								//var_dump($street_ac);die;
								if(isset($street_ac) && !empty($street_ac)){
									foreach ($street_ac as $ac_item){
										$array_activities[] = array(
												'id_city'           =>$this->_id_city,
												'id_wkgps'          => $id_gps,
												'id_task'           => $id_task,
												'id_task_activity'  => $task_activity_id,
												'street'            => isset($ac_item['street']) ? $ac_item['street'] : '',
												'latitude'          => isset($ac_item['lat']) ? $ac_item['lat'] : '',
												'longtitude'        => isset($ac_item['lon']) ? $ac_item['lon'] : '',
												'id_activity'       => $activity_id,
												'starttime'         => isset($ac_item['start']) ? $ac_item['start'] : '',
												'endtime'           => isset($ac_item['end']) ? $ac_item['end'] : '',
												'created_at'        => date('Y-m-d H:i:s')
										);
									}
								} else{
									$array_activities[] = array(
											'id_city'           =>$this->_id_city,
											'id_wkgps'          => $id_gps,
											'id_task'           => $id_task,
											'id_task_activity'  => $task_activity_id,
											'street'            => '',
											'latitude'          => $locations[0]['lat'],
											'longtitude'        => $locations[0]['lon'],
											'id_activity'       => $activity_id,
											'starttime'         => $start_time,
											'endtime'           => $end_time,
											'created_at'        => date('Y-m-d H:i:s')
									);
								}
								# Poisition
								foreach ($locations as $locaitem)
								{
									$lat = isset($locaitem['lat']) ? trim($locaitem['lat']) : '';
									$lon = isset($locaitem['lon']) ? trim($locaitem['lon']) : '';
									$time = isset($locaitem['time']) ? trim($locaitem['time']) : '';
									if(strlen($position)>0)
										$position = $position.','.$lat.' '.$lon.' '.$time;
									else
										$position = $position.$lat.' '.$lon.' '.$time;
								}
							}# end if Location
							# Insert Activity
							if(isset($array_activities) && !empty($array_activities)){
								if($this->db->insert_batch(TBL_WORKER_ACTIVITY,$array_activities) == FALSE){
									write_log('insert worker activity_'.$id_task.'_'.json_encode($array_activities), 'update_winter_service');
								}
							}
						}# end for row
						# Update Position
						$this->db->where('id',$id_gps);
						$this->db->update(TBL_WORKER_GPS,array('position' => $position));
						# Update id_activity infor
						update_infor($id_gps);
					} else{ # end if gps_detail
						$return = array(
								'status'    => FALSE,
								'msg'       => 'no gps detail'
						);
					}
				} else{ # end if data json
					$return = array(
							'status'    => FALSE,
							'msg'       => 'no data json'
					);
				}
			}
		}
		return $return;
	}

	/**
	 * save file upload from api request
	 * @return array : status for api
	 */
	function upload_image_record_post() {
		$resources = isset($_FILES) ? $_FILES : '';
		$records   = array();
		$counter   = 0;
		$info      = array();

		# Post data
		$id_task     = $this->input->post('task_id');
		$id_employee = $this->input->post('employee_id');
		$id_machine  = $this->input->post('machine_id');

		# Upload patch
		$upload_path = config_item('upload_dir');

		$status      = true;
		if ( isset($resources) AND is_array($resources) AND (!empty($resources)) ) {
			foreach ($resources as $key => $value) {
				$upload_path_tmp = $upload_path . $this->_id_city . '/' . date('Y/m/d') . '/' . $id_employee . '/';
				$result = $this->_upload_process($key, $upload_path_tmp);
				if(file_exists($result['full_path'])) {
					$counter++;
					$flag = rename($result['full_path'], $result['file_path'].$id_task.'_'.$result['file_name']);
					if (!$flag) {
						write_log('Can\'t rename file upload_Task: '.$id_task.'_Result: '.json_encode($result), 'upload_image_record');
					}
				} else {
					write_log("File upload not exit_Task: '.$id_task.'_Result: ".json_encode($result), 'upload_image_record');
				}
			}
		} else {
			$status = false;
		}

		return $this->response(
			array(
				'status' => $status ? STATUS_SUCCESS : STATUS_FAIL,
				'msg'    => "Received: {$counter}",
				'info'   => $info
			)
		);
	}

	function _upload_process($field_name, $upload_path) {
		if (!is_dir($upload_path)) {
			umask(0000);
			@mkdir($upload_path, 0755, TRUE);
		}

		# Config for upload
		$config['upload_path']   = $upload_path;
		$config['allowed_types'] = config_item('upload_allowed');
		//$config['encrypt_name']  = true;
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload($field_name)) {
			# Log errors
			write_log('Upload file error_'.json_encode($this->upload->display_errors()), 'upload_file_error');

			return FALSE;
		} else {
			$data = $this->upload->data();
			@chmod($data['full_path'], 0644);
			return $data;
		}
	}

	/**
	 * Store a image from binary string
	 * @param  [type] $binary_data [description]
	 * @return [type]              [description]
	 * @author HuongPM <phung.manh.huong@kloon.vn>
	 */
	function _binary2Img($binary_data) {
		$im  = imagecreatefromstring($img);
		$imgname = 'img-'.date('YmdHis').'-'.rand(1,100).'png';
		if ($im !== false) {
			try
			{
				$fileUrl = base_url().$imgname;

				//setting alpha blending on
				imagealphablending($im, false);

				//save alphablending setting (important)
				imagesavealpha($im, true);

				//Generate image and print it
				$resp = imagepng($im, $fileUrl);
			}
			catch (Exception $e)
			{
				$imgname = '';
			}
		}
	}

	/**
	 * Walking all elements of array (parsed by Format library)
	 * @param  array  $data        Data array
	 * @param  string $key_checker Array key for check has one or many alement
	 * @param  string $key_value   Array key for element containt data
	 * @return array               Array valid
	 * @author chientran <tran.duc.chien@kloon.vn>
	 * @created at 23 Oct 2013
	 */
	function _walking_nodes($data = array(), $key_checker = '', $key_value = '') {
		#print_r($data);die;
		$return = array();
		# Has one
		if (isset($data[$key_value]) && isset($data[$key_value][$key_checker]) || isset($data[$key_checker])) {
			$return[] = $data[$key_value];
		} else {
			# Has many
			if (!empty($key_value) && isset($data[$key_value]) && !empty($data)) {
				$return = $data[$key_value];
			} else {
				if(isset($data) && !empty($data)){
					foreach ($data as $key => $value) {
						$return[] = $value;
					}
				}
			}
		}
		return $return;
	}

	/**
	 * Save data to file.
	 *    Path:  id_city / <year> / <month> / <day> / <hour>-<minites>-<second>_<unique_id>
	 * @param  string  $data : Data for write file
	 * @return boolean
	 */
	function _save_to_files($data) {
		$path = $this->_sync_winter_dir.$this->_id_city.'/'.date('Y/m/d') . '/';
		if (!is_dir($path)) {
			@umask(0000);
			@mkdir ($path, 0755, TRUE);
		}
		$file_name = date('H-i-s_') . uniqid().'.txt';
		$file_path = $path.$file_name;
		$this->load->helper('file');
		if (!write_file($file_path, $data)) {
			write_log('Backup data sync error');
			return false;
		}
		return true;
	}

	/*
	 * Geo street name from lat long
	 **/
	function _extract_street($_lat,$_lon){
		$return = '';
		if((isset($_lat) && !empty($_lat)) && (isset($_lon) && !empty($_lon))){
			$_street_data = '';
			try{
				$_street_data = file_get_contents($this->_path_service.'&lat='.$_lat.'&lng='.$_lon.'&from=1&to=1');
			}
			catch(Exception $e){}
			$_street_array = json_decode($_street_data);
			if(isset($_street_array->result) && !empty($_street_array->result)){
				$_street_detail = $_street_array->result[0];
				$_street_name = isset($_street_detail->name) ? $_street_detail->name : '';
				$return = $_street_name;
			} else{
				write_log("error get street name NULL : ".json_encode(array('lat'=>$_lat,'lon'=>$_lon)), 'sync_get_street');
			}
		} else{
			write_log("error location NULL : ".json_encode(array('lat'=>$_lat,'lon'=>$_lon)), 'sync_get_street');
		}
		return $return;
	}

	/*
	 * */
	function _check_street($location = NULL,$start = '', $end = ''){
		$return = array();
		$time   = NULL;
		$street = NULL;
		$lat    = NULL;
		$lon    = NULL;
		$count  = 0;
		if(isset($location) && !empty($location)){
			foreach ($location as $item){
				$_lat  = isset($item['lat']) ? trim($item['lat']) : '';
				$_lon  = isset($item['lon']) ? trim($item['lon']) : '';
				$_time = isset($item['time']) ? trim($item['time']) : '';

				# begin lat
				if(!isset($lat) && empty($lat)) {
					$lat = $_lat;
				}

				# begin lon
				if(!isset($lon) && empty($lon)){
					$lon = $_lon;
				}

				if(!isset($start) && empty($start)){
					$start = $_time;
				}

				if(isset($lat) && !empty($lat) && isset($lon) && !empty($lon)){
					$_street = $this->_extract_street($_lat, $_lon);
				}
				if(isset($_street) && !empty($_street))
				{
					if(($street != $_street)){
						$return[$count] = array(
							'street'    => $_street,
							'lat'       => $lat,
							'lon'       => $lon,
							'start'     => $start,
							'end'       => ''
						);
						if($count > 0){
							$return[$count - 1]['end'] = $_time;
						}
						$lat = $_lat;
						$lon = $_lon;
						# start street
						$start = $_time;
						$count++;
					}
				} else{
					write_log("Error Street Name NULL : ".json_encode(array('lat'=>$_lat,'lon'=>$_lon,'time'=>$time)), 'sync_get_street');
				}
				$street = $_street;
			}
			# Add endtime last
			if(isset($return) && !empty($return)){
				$return[$count - 1]['end'] = $end;
			}
		}else{
			write_log("Error Location NULL : ".json_encode(array('lat'=>$_lat,'lon'=>$_lon,'time'=>$time)), 'sync_get_street');
		}
		return $return;
	}

	/**
	 * Resolve street name from lat, lon
	 * @param  string $latitude
	 * @param  string $longtitude
	 * @param  string $service
	 * @return string
	 */
	function resolve_street_name($latitude, $longtitude, $service = 'kloon') {
		$services = array(
			'kloon'         => 'http://nominatim.kloon.net/reverse?format=json&zoom=17&addressdetails=1',
			'mapquest'      => 'http://open.mapquestapi.com/nominatim/v1/reverse.php?format=json&zoom=17',
			'openstreetmap' => 'http://nominatim.openstreetmap.org/reverse?format=json&zoom=17&addressdetails=1',
			'gisgraphy'     => 'http://services.gisgraphy.com/street/streetsearch?format=json&from=1&to=1',
		);

		if (!isset($services[$service])) {
			$service = $services['mapquest'];
		} else {
			$service = $services[$service];
		}

		$street = '';
		if ((isset($latitude) && !empty($latitude)) && (isset($longtitude) && !empty($longtitude))) {
			$service_url    = '';
			$service_type   = $service;
			$count  = 0;
			$status = FALSE;
			while ( ($count < 3) AND ($status != TRUE) ) {
				switch ($count) {
					case 1:
						$service_type = 'mapquest';
						$service_url  = $services['mapquest'] . '&lat=' . $latitude . '&lon=' . $longtitude;
						break;

					case 2:
						$service_type = 'openstreetmap';
						$service_url  = $services['openstreetmap'] . '&lat=' . $latitude . '&lon=' . $longtitude;
						break;

					default:
						$service_type = 'kloon';
						$service_url  = $services['kloon'] . '&lat=' . $latitude . '&lon=' . $longtitude;
						break;
				}
				$street = $this->curl_read_content($service_url, $service_type);

				# Logs
				write_log("Synchronize controller: {$service_type}: {$latitude} - {$longtitude} {$street}", 'resolve_street_name');

				if( (isset($street) AND !empty($street)) OR ($count >= 3) ){
					$status = TRUE;
				}
				$count++;

				usleep(100);
			}
		}
		return $street;
	}

	function curl_read_content($link = FALSE, $service = 'kloon', $timeout = 2500){
		$street = '';
		if($link != FALSE){
			$street_data = '';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $link);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			#curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
			curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 2000);
			$street_data = curl_exec($ch);
			curl_close($ch);

			$street_array = json_decode($street_data,true);
			if (isset($street_array['address']) && !empty($street_array['address'])) {
				$street_detail = $street_array['address'];
				if(isset($street_detail['road']) && !empty($street_detail['road'])){
					$street_name = isset($street_detail['road']) ? $street_detail['road'] : '';
					$street      = $street_name;
				} else {
					/*if(isset($street_detail['address26']) && !empty($street_detail['address26'])){
						$street_name = isset($street_detail['address26']) ? $street_detail['address26'] : '';
						$street      = $street_name;
					}*/
				}
			}
		}
		return $street;
	}
}
