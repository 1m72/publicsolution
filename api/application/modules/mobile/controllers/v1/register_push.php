<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-11-05
 */
class Register_push extends REST_Controller
{
	function __construct(){
		parent::__construct();

		if(!isset($this->_tenantId) || empty($this->_tenantId)){
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	/*
	 * @description
	 * @function : register_push_notifiation_post
	 * @author : huongpm<phung.manh.huong@kloon.vn>
	 * @create : 2013-11-05
	 * @route
	 * */
	function register_push_nofitication_post()
	{
		$registration_id = $this->post('registration_id');
		$device_id = $this->post('device_id');
		$tenantId = $this->_tenantId;

		$return = array(
			'version'  => config_item('api_version'),
			'status'   => STATUS_SUCCESS
		);

		$data = $this->db->get_where(TBL_DEVICES,array('id_device'=>$device_id))->result_array();
		if(isset($data) && !empty($data))
		{
			if(isset($data[0]['deleted_at']) && !empty($data[0]['deleted_at']))
			{
				$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_FAIL
				);
			}
			else
			{
				$this->db->where('id_device',$device_id);
				$this->db->update(TBL_DEVICES,
						array(
							'id_unique'=>$registration_id,
							'updated_at'=>date('Y-m-d H:i:s')
						)
				);
			}
		}
		else
		{
			$this->db->insert(TBL_DEVICES,array(
					'id_city' 		=> $tenantId,
					'id_unique' 	=> $registration_id,
					'id_device' 	=> $device_id,
					'created_at' 	=> date('Y-m-d H:i:s')
			));
		}
		return  $this->response($return);
	}
	/*
	 * @description
	 * @function : push_notifiation_get
	 * @author : huongpm<phung.manh.huong@kloon.vn>
	 * @create : 2013-11-05
	 * @route
	 * */
	function push_notifiation_get()
	{
		$result = $this->db->get(TBL_DEVICES)->result_array();

		return $this->response($result);
	}
}
?>