<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

class City extends REST_Controller{
	function __construct() {
		parent::__construct();

		if(isset($this->_tenantId) && !empty($this->_tenantId))
			$this->load->model('city_model','city');
		else{
			$return = array(
					'version'   => config_item('api_version'),
					'status'    => STATUS_FAIL,
					'msg'       => 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function setting_get() {
		$city_setting = $this->city->get($this->_tenantId);
		$return = array(
			'version' => config_item('api_version'),
		);
		if (is_array($city_setting) AND !empty($city_setting)) {
			$return['status']                           = STATUS_SUCCESS;
			$return['allow_selection_employee_machine'] = (isset($city_setting['selection']) AND (intval($city_setting['selection']) == 1)) ? 'Y' : 'N';
			$return['has_material']                     = (isset($city_setting['material']) AND (intval($city_setting['material']) == 1)) ? 'Y' : 'N';
			$return['city']                             = isset($city_setting['name']) ? mb_convert_case('Stadt ' . $city_setting['name'], MB_CASE_TITLE) : '';

			# Modules
			$this->load->model('module_model', 'module');
			$modules           = $this->module->select('id AS module_id, title as module_name')->get_many_by(array('id_parent' => 0));
			foreach ($modules as $key => $value) {
				$return['module'][] = $value;
			}
		} else {
			$return['status'] = STATUS_FAIL;
			$return['msg'] = 'Not found city config';
		}
		return $this->response($return);
	}
}