<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author Chien Tran <tran.duc.chien@kloon.vn>
 * @create 2015-05-21
 **/

class Fasttask extends REST_Controller{
    function __construct() {
        parent::__construct();
    }

    public function text_modules_get() {
        $result = $this->db->select('id as text_module_id, parent_id, title as description')->where('deleted_at', null)->get(TBL_FREETEXT)->result_array();
        return $this->response($result, 200, 'json');
    }

    public function upload_objects_locations_post() {
        $path = config_item('sync_object_location_dir') . $this->_tenantId . '/' . date('Y/m/d') . '/';
        if (!is_dir($path)) {
            $old = umask(0);
            @mkdir($path, 0755, true);
            umask($old);
        }

        $files = $_FILES;
        if (!is_array($files) OR empty($files)) {
            return $this->response(
                array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'File not found'
                ),
                REST_CODE_PARAM_ERR
            );
        }

        // Pheanstalk libs
        $this->load->helper('pheanstalk');

        $queue_list = array();
        foreach ($files as $file) {
            $file_tmp  = $file['tmp_name'];
            $file_ext  = end(explode('.', $file['name']));
            $file_save = $path . date('YmdHis') . '_' . uniqid() . '.' . $file_ext;
            $result    = move_uploaded_file($file_tmp, $file_save);
            if ($result) {
                // Push to queue
                push_job(QUE_OBJECT_LOCATION, json_encode(array('data' => $file_save, 'id_city' => $this->_tenantId)));

                $queue_list[] = array(
                    'id_city'       => $this->_tenantId,
                    'id_task'       => '',
                    'id_worker_gps' => '',
                    'type'          => CRON_TYPE_OBJECT_LOCATION,
                    'data'          => $file_save,
                    'status'        => CRON_STATUS_PENDING,
                    'last_run'      => null,
                    'updated_at'    => null,
                    'created_at'    => date('Y-m-d H:i:s'),
                );
            }
        }

        if (count($queue_list) > 0) {
            $result = $this->_db_global->insert_batch(TBL_CRON, $queue_list);
            if ($result !== false) {
                return $this->response(
                    array(
                        'version' => config_item('api_version'),
                        'status'  => STATUS_SUCCESS
                    )
                );
            } else {
                return $this->response(
                    array(
                        'version' => config_item('api_version'),
                        'status'  => STATUS_FAIL,
                        'msg'     => 'Has an error when saving data'
                    ),
                    REST_CODE_PARAM_ERR
                );
            }
        } else {
            return $this->response(
                array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'No task is created for this request'
                ),
                REST_CODE_PARAM_ERR
            );
        }
    }

    public function assignment_post() {
        $password = $this->post('password');

        // Check city exist
        $city = $this->_db_global->where('id', $this->_tenantId)->get(TBL_CITY)->row();
        if (empty($city)) {
            $result = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'User not found'
            );
            return $this->response($result, REST_CODE_PARAM_ERR);
        }

        // Check passcode
        if ($city->passcode === $password) {
            $result = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
            );
            return $this->response($result);
        } else {
            $result = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL
            );
            return $this->response($result, REST_CODE_PARAM_ERR);
        }
    }

    function superiors_list_get() {
        $module = $this->db->select('id')->get_where(TBL_MODULES, array('type' => APP_FASTTASK_TEXT))->row();
        $module_id = isset($module->id) ? $module->id : false;
        $data = array();
        if ($module_id) {
            $this->db->select('w.id AS superior_id, w.first_name AS first_name, w.last_name AS last_name', FALSE);
            $this->db->from(TBL_WORKER . ' AS w');
            $this->db->join(TBL_WORKER_MODULES . ' AS m', 'w.id = m.id_worker', 'left');
            $this->db->where('w.deleted_at', null);
            $this->db->where('m.deleted_at', null);
            $this->db->where('m.id_module', $module_id);
            $data = $this->db->get()->result_array();
        }
        $this->set_format('json');
        return $this->response($data, 200, 'json');
    }

    function employee_group_get() {
        $this->db->select('id AS employee_group_id, title AS employee_group_name');
        $this->db->where('deleted_at', null);
        $data = $this->db->get(TBL_EMPLOYEE_GROUP)->result_array();
        return $this->response($data);
    }

    function cost_center_get() {
        $this->db->select('c.id AS cost_center_id, c.title AS cost_center_name, e.id_employee_group AS employee_group_id');
        $this->db->from(TBL_COST_CENTER . ' AS c');
        $this->db->join(TBL_EMPLOYEE_GROUP_COST . ' AS e', 'c.id = e.id_cost_center');
        $this->db->where('c.deleted_at', null);
        $this->db->where('e.deleted_at', null);
        $data = $this->db->get()->result_array();
        return $this->response($data);
    }
}
