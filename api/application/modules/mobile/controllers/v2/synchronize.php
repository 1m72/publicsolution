<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

class Synchronize extends REST_Controller {
    private $_id_city                       = -1;
    private $_upload_path                   = '';
    private $_sync_winter_dir               = '';
    private $_sync_winter_activities_dir    = '';

    function __construct() {
        parent::__construct();

        if (isset($this->_tenantId) && !empty($this->_tenantId)) {
            $this->_id_city                     = intval($this->_tenantId);
            $this->_upload_path                 = config_item('upload_dir');
            $this->_sync_winter_dir             = config_item('sync_winter_dir');
            $this->_sync_winter_activities_dir  = config_item('sync_winter_activities_dir');
        } else {
            $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function update_winter_service_task_post() {
        if (!isset($_POST['xml'])) {
            $return = array(
                'status' => STATUS_FAIL,
                'msg'    => 'xml field is required'
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }

        $xml_data = urldecode($_POST['xml']);

        // Logs
        $this->_save_to_files($xml_data, 'xml');

        $raw_data = $this->format->factory($xml_data, 'xml')->to_array();
        if(!is_array($raw_data) OR empty($raw_data)){
            write_log('xml convert array empty', 'update_winter_service_task');
            $return = array(
                'status' => STATUS_FAIL,
                'msg'    => 'xml convert array empty.'
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
        $data = $this->_walking_nodes($raw_data, 'taskid', 'row');

        if (is_array($data) AND !empty($data)) {
            // Helpers
            $this->load->helper('street');

            // Transaction start
            $this->db->trans_start();
            foreach ($data as $item) {
                $id_gps           = 0;
                $id_task_activity = 0;
                $position         = '';
                $taskid           = (isset($item['taskid']) && !empty($item['taskid'])) ? $item['taskid'] : '';
                $employeeid       = (isset($item['employeeid']) && !empty($item['employeeid'])) ? $item['employeeid'] : '';
                $machineid        = (isset($item['machineid']) && !empty($item['machineid'])) ? $item['machineid'] : '';
                $module_id        = (isset($item['module_id']) && !empty($item['module_id'])) ? $item['module_id'] : '';
                $start_time       = (isset($item['starttime']) && !empty($item['starttime'])) ? $item['starttime'] : '';
                $end_time         = (isset($item['endtime']) && !empty($item['endtime'])) ? $item['endtime'] : '';

                $this->db->where('id_task',$taskid);
                $gps_detail = $this->db->get(TBL_WORKER_GPS)->row_array();
                if(isset($gps_detail) && !empty($gps_detail)){
                    $id_gps = $gps_detail['id'];
                } else{
                    $data_gps = array(
                        'id_city'    => $this->_id_city,
                        'id_task'    => $taskid,
                        'id_object'  => 0,
                        'id_worker'  => $employeeid,
                        'id_machine' => $machineid,
                        'app'        => $module_id,
                        'ontime'     => $start_time,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    if($this->db->insert(TBL_WORKER_GPS,$data_gps)){
                        $id_gps = $this->db->insert_id();
                    }
                }

                // materials
                $array_materials = array();
                if(isset($item['materials']) && !empty($item['materials'])) {
                    $materials = $item['materials'];
                    $materials = $this->_walking_nodes($item['materials'], 'materialid', 'material');
                    if(isset($materials) && !empty($materials)) {
                        foreach ($materials as $it_material) {
                            array_push(
                                $array_materials,
                                array(
                                    'id_city'       => $this->_id_city,
                                    'id_material'   => isset($it_material['materialid']) ? $it_material['materialid'] : '',
                                    'id_worker_gps' => $id_gps,
                                    'amount'        => isset($it_material['amount']) ? $it_material['amount'] : '',
                                    'created_at'    => date('Y-mn-d H:i:s')
                                )
                            );
                        }
                    }
                }

                // Init array store information and email data
                $array_data_mail    = array();
                $array_information  = array();

                // Storage distribute
                $id_city_config = 1;

                // Messages
                $messages = array();
                if(isset($item['messages']) && !empty($item['messages'])) {
                    $messages = $item['messages'];
                    $messages = $this->_walking_nodes($item['messages'], 'text', 'message');
                    if (isset($messages) && !empty($messages)) {
                        foreach ($messages as $information_item) {
                            $information_item_text       = isset($information_item['text']) ? $information_item['text'] : '';
                            $information_item_lat        = isset($information_item['lat']) ? $information_item['lat'] : '';
                            $information_item_lon        = isset($information_item['lon']) ? $information_item['lon'] : '';
                            $information_item_time       = isset($information_item['time']) ? $information_item['time'] : '';
                            $information_item_rotate     = isset($information_item['orientation_rotate']) ? $information_item['orientation_rotate'] : '';
                            $information_item_voice_name = isset($information_item['voicename']) ? ($taskid.'_'.$information_item['voicename'].'.3gp') : '';
                            $information_item_image_name = isset($information_item['imagename']) ? ($taskid.'_'.$information_item['imagename'].'.jpg') : '';
                            $information_item_video_name = isset($information_item['moviename']) ? ($taskid.'_'.$information_item['moviename'].'.mp4') : '';
                            $information_item_street     = resolve_street_name($information_item_lat, $information_item_lon);
                            $information_record = array(
                                'id_city'       => $this->_id_city,
                                'id_config'     => $id_city_config,
                                'id_worker_gps' => $id_gps,
                                'id_task'       => $taskid,
                                'app'           => $module_id,
                                'type'          => INFO_MESSAGE,
                                'data'          => $information_item_text,
                                'lat'           => $information_item_lat,
                                'lon'           => $information_item_lon,
                                'time'          => $information_item_time,
                                'street'        => $information_item_street,
                                'rotate'        => 0,
                                'created_at'    => date('Y-m-d H:i:s')
                            );

                            // Insert information
                            $array_information[] = $information_record;
                            $db_flag             = $this->db->insert(TBL_WORKER_INFORMATION, $information_record);
                            $id_information      = ($db_flag !== false) ? $this->db->insert_id() : false;

                            // Data for email
                            $array_data_mail[] = array(
                                'created_at'    => date('Y-m-d H:i:s'),
                                'data'          => json_encode(
                                    array(
                                        'id_city'        => $this->_id_city,
                                        'id_information' => $id_information,
                                        'id_gps'         => $id_gps,
                                        'taskid'         => $taskid,
                                        'module_id'      => $module_id,
                                        'employeeid'     => $employeeid,
                                        'machineid'      => $machineid,
                                        'type'           => INFO_MESSAGE,
                                        'lat'            => $information_item_lat,
                                        'lon'            => $information_item_lon,
                                        'data'           => $information_item_text,
                                        'street'         => $information_item_street,
                                        'ontime'         => $information_item_time,
                                        'created_at'     => date('Y-m-d H:i:s')
                                    )
                                ),
                            );
                        }
                    }
                }

                // images
                $images             = array();
                $image_rotate_array = array();
                if(isset($item['images']) && !empty($item['images'])) {
                    $images = $item['images'];
                    $images = $this->_walking_nodes($item['images'], 'imagename', 'image');
                    if (isset($images) && !empty($images)) {
                        foreach ($images as $information_item) {
                            $information_item_text       = isset($information_item['text']) ? $information_item['text'] : '';
                            $information_item_lat        = isset($information_item['lat']) ? $information_item['lat'] : '';
                            $information_item_lon        = isset($information_item['lon']) ? $information_item['lon'] : '';
                            $information_item_time       = isset($information_item['time']) ? $information_item['time'] : '';
                            $information_item_rotate     = isset($information_item['orientation_rotate']) ? $information_item['orientation_rotate'] : '';
                            $information_item_voice_name = isset($information_item['voicename']) ? ($taskid.'_'.$information_item['voicename'].'.3gp') : '';
                            $information_item_image_name = isset($information_item['imagename']) ? ($taskid.'_'.$information_item['imagename'].'.jpg') : '';
                            $information_item_video_name = isset($information_item['moviename']) ? ($taskid.'_'.$information_item['moviename'].'.mp4') : '';
                            $information_item_street     = resolve_street_name($information_item_lat, $information_item_lon);

                            $information_record = array(
                                'id_city'       => $this->_id_city,
                                'id_config'     => $id_city_config,
                                'id_worker_gps' => $id_gps,
                                'id_task'       => $taskid,
                                'app'           => $module_id,
                                'type'          => INFO_IMAGE,
                                'data'          => $information_item_image_name,
                                'lat'           => $information_item_lat,
                                'lon'           => $information_item_lon,
                                'time'          => $information_item_time,
                                'rotate'        => $information_item_rotate,
                                'street'        => $information_item_street,
                                'created_at'    => date('Y-m-d H:i:s')
                            );

                            // Insert information
                            $array_information[] = $information_record;
                            $db_flag             = $this->db->insert(TBL_WORKER_INFORMATION, $information_record);
                            $id_information      = ($db_flag !== false) ? $this->db->insert_id() : false;

                            $image_path  = $this->_upload_path;
                            $image_path .= $this->_tenantId . '/';
                            $image_path .= date('Y/m/d',strtotime($start_time)) . '/';
                            $image_path .= $employeeid . '/';
                            $image_path .= $information_item_image_name;
                            if(isset($it_image['orientation_rotate']) && !empty($it_image['orientation_rotate'])){
                                $image_rotate_array[] = array(
                                    'id_city'        => $this->_id_city,
                                    'type'           => 'rotate',
                                    'data'           => json_encode(array(
                                        'full_path' => $image_path,
                                        'rotate'    => $information_item_rotate
                                    )),
                                    'status'     => CRON_STATUS_PENDING,
                                    'created_at' => date('Y-m-d H:i:s')
                                );
                            }

                            // Data for email
                            $array_data_mail[] = array(
                                'attach'     => $image_path,
                                'created_at' => date('Y-m-d H:i:s'),
                                'data'       => json_encode(
                                    array(
                                        'id_city'        => $this->_id_city,
                                        'id_information' => $id_information,
                                        'id_gps'         => $id_gps,
                                        'taskid'         => $taskid,
                                        'module_id'      => $module_id,
                                        'employeeid'     => $employeeid,
                                        'machineid'      => $machineid,
                                        'type'           => INFO_IMAGE,
                                        'lat'            => $information_item_lat,
                                        'lon'            => $information_item_lon,
                                        'data'           => $image_path,
                                        'street'         => $information_item_street,
                                        'ontime'         => $information_item_time,
                                        'created_at'     => date('Y-m-d H:i:s')
                                    )
                                )
                            );
                        }
                    }
                }

                // voices
                $voices = array();
                $voice_convert_array = array();
                if(isset($item['voices']) && !empty($item['voices'])){
                    $voices = $item['voices'];
                    $voices = $this->_walking_nodes($item['voices'], 'voicename', 'voice');
                    if(isset($voices) && !empty($voices)) {
                        foreach ($voices as $information_item) {
                            $information_item_text       = isset($information_item['text']) ? $information_item['text'] : '';
                            $information_item_lat        = isset($information_item['lat']) ? $information_item['lat'] : '';
                            $information_item_lon        = isset($information_item['lon']) ? $information_item['lon'] : '';
                            $information_item_time       = isset($information_item['time']) ? $information_item['time'] : '';
                            $information_item_rotate     = isset($information_item['orientation_rotate']) ? $information_item['orientation_rotate'] : '';
                            $information_item_voice_name = isset($information_item['voicename']) ? ($taskid.'_'.$information_item['voicename'].'.3gp') : '';
                            $information_item_image_name = isset($information_item['imagename']) ? ($taskid.'_'.$information_item['imagename'].'.jpg') : '';
                            $information_item_video_name = isset($information_item['moviename']) ? ($taskid.'_'.$information_item['moviename'].'.mp4') : '';
                            $information_item_street     = resolve_street_name($information_item_lat, $information_item_lon);

                            $information_record = array(
                                'id_city'       => $this->_id_city,
                                'id_config'     => $id_city_config,
                                'id_worker_gps' => $id_gps,
                                'id_task'       => $taskid,
                                'app'           => $module_id,
                                'type'          => INFO_VOICE,
                                'data'          => $information_item_voice_name,
                                'lat'           => $information_item_lat,
                                'lon'           => $information_item_lon,
                                'time'          => $information_item_time,
                                'street'        => $information_item_street,
                                'rotate'        => 0,
                                'created_at'    => date('Y-m-d H:i:s')
                            );

                            // Insert information
                            $array_information[] = $information_record;
                            $db_flag             = $this->db->insert(TBL_WORKER_INFORMATION, $information_record);
                            $id_information      = ($db_flag !== false) ? $this->db->insert_id() : false;

                            $voice_path  = $this->_upload_path;
                            $voice_path .= $this->_tenantId . '/';
                            $voice_path .= date('Y/m/d',strtotime($start_time)) . '/';
                            $voice_path .= $employeeid . '/';
                            $voice_path .= $information_item_voice_name;
                            $voice_convert_array[] = array(
                                'id_city'    => $this->_id_city,
                                'type'       => 'voice',
                                'data'       => $voice_path,
                                'status'     => CRON_STATUS_PENDING,
                                'created_at' => date('Y-m-d H:i:s')
                            );

                            // Data for email
                            $array_data_mail[] = array(
                                'attach'     => $voice_path,
                                'created_at' => date('Y-m-d H:i:s'),
                                'data'       => json_encode(
                                    array(
                                        'id_city'        => $this->_id_city,
                                        'id_information' => $id_information,
                                        'id_gps'         => $id_gps,
                                        'taskid'         => $taskid,
                                        'module_id'      => $module_id,
                                        'employeeid'     => $employeeid,
                                        'machineid'      => $machineid,
                                        'type'           => INFO_VOICE,
                                        'lat'            => $information_item_lat,
                                        'lon'            => $information_item_lon,
                                        'data'           => $voice_path,
                                        'street'         => $information_item_street,
                                        'ontime'         => $information_item_time,
                                        'created_at'     => date('Y-m-d H:i:s')
                                    )
                                )
                            );
                        }
                    }
                }

                // movies
                $movies = array();
                if(isset($item['movies']) && !empty($item['movies'])) {
                    $movies = $item['movies'];
                    $movies = $this->_walking_nodes($item['movies'], 'moviename', 'movie');
                    if (isset($movies) && !empty($movies)) {
                        foreach ($movies as $information_item) {
                            $information_item_text       = isset($information_item['text']) ? $information_item['text'] : '';
                            $information_item_lat        = isset($information_item['lat']) ? $information_item['lat'] : '';
                            $information_item_lon        = isset($information_item['lon']) ? $information_item['lon'] : '';
                            $information_item_time       = isset($information_item['time']) ? $information_item['time'] : '';
                            $information_item_rotate     = isset($information_item['orientation_rotate']) ? $information_item['orientation_rotate'] : '';
                            $information_item_voice_name = isset($information_item['voicename']) ? ($taskid.'_'.$information_item['voicename'].'.3gp') : '';
                            $information_item_image_name = isset($information_item['imagename']) ? ($taskid.'_'.$information_item['imagename'].'.jpg') : '';
                            $information_item_video_name = isset($information_item['moviename']) ? ($taskid.'_'.$information_item['moviename'].'.mp4') : '';
                            $information_item_street     = resolve_street_name($information_item_lat, $information_item_lon);

                            $information_record = array(
                                'id_city'       => $this->_id_city,
                                'id_config'     => $id_city_config,
                                'id_worker_gps' => $id_gps,
                                'id_task'       => $taskid,
                                'app'           => $module_id,
                                'type'          => INFO_VIDEO,
                                'data'          => $information_item_video_name,
                                'lat'           => $information_item_lat,
                                'lon'           => $information_item_lon,
                                'time'          => $information_item_time,
                                'street'        => $information_item_street,
                                'rotate'        => 0,
                                'created_at'    => date('Y-m-d H:i:s')
                            );

                            // Insert information
                            $array_information[] = $information_record;
                            $db_flag             = $this->db->insert(TBL_WORKER_INFORMATION, $information_record);
                            $id_information      = ($db_flag !== false) ? $this->db->insert_id() : false;

                            $video_path  = $this->_upload_path;
                            $video_path .= $this->_tenantId . '/';
                            $video_path .= date('Y/m/d',strtotime($start_time)) . '/';
                            $video_path .= $employeeid . '/';
                            $video_path .= $information_item_video_name;

                            // Data for email
                            $array_data_mail[] = array(
                                'created_at' => date('Y-m-d H:i:s'),
                                'data'       => json_encode(
                                    array(
                                        'id_city'        => $this->_id_city,
                                        'id_information' => $id_information,
                                        'id_gps'         => $id_gps,
                                        'taskid'         => $taskid,
                                        'module_id'      => $module_id,
                                        'employeeid'     => $employeeid,
                                        'machineid'      => $machineid,
                                        'type'           => INFO_VIDEO,
                                        'lat'            => $information_item_lat,
                                        'lon'            => $information_item_lon,
                                        'data'           => $video_path,
                                        'street'         => $information_item_street,
                                        'ontime'         => $information_item_time,
                                        'created_at'     => date('Y-m-d H:i:s')
                                    )
                                ),
                            );
                        }
                    }
                }

                // insert information
                // if(isset($array_information) && !empty($array_information)) {
                //     $db_flag = $this->db->insert_batch(TBL_WORKER_INFORMATION, $array_information);
                //     if($db_flag === FALSE) {
                //         write_log('insert information_'.$taskid.'_'.json_encode($array_information), 'update_winter_service');
                //     }
                // }

                // insert materials
                if(isset($array_materials) && !empty($array_materials)){
                    if($this->db->insert_batch(TBL_MACHINE_MATERIAL_USED,$array_materials) === FALSE) {
                        write_log('insert material_'.$taskid.'_'.json_encode($array_materials), 'update_winter_service');
                    }
                }

                // insert data mail
                if(isset($array_data_mail) && !empty($array_data_mail)) {
                    // Push to queue
                    if (is_array($array_data_mail) AND !empty($array_data_mail)) {
                        foreach ($array_data_mail as $key => $value) {
                            // DB
                            $db_flag = $this->_db_global->insert(TBL_EMAIL, $value);
                            if ($db_flag === false) {
                                write_log("Insert email data synchronize FAIL _ id_task : {$taskid} _ Data: ".json_encode($value)." _ Query : " . $this->_db_global->last_query(), 'update_winter_service');
                            } else {
                                write_log("Insert email data synchronize OK _ id_task : {$taskid}", 'update_winter_service');

                                // id_email
                                $id_email = $this->_db_global->insert_id();

                                // Store temperation
                                $data = array(
                                    'unique_id'  => $this->_id_city . '-' . $taskid,
                                    'value'      => $id_email,
                                    'created_at' => date('Y-m-d H:i:s')
                                );
                                $db_flag = $this->_db_global->insert(TBL_TMP, $data);
                                write_log('Store temperation email data _ ' . (($db_flag === false) ? 'FAIL' : 'OK') . ' Data: ' . json_encode($data), 'update_winter_service');
                            }
                        }
                    }
                }
            }

            // transaction end
            $this->db->trans_complete();

            // Log errors if transaction errors
            if ($this->db->trans_status() === FALSE) {
                write_log('Transaction error_'.json_encode($data), 'update_winter_service_task');
                $return = array(
                    'status' => STATUS_FAIL,
                    'msg'    => 'Has an error when sync data to DB. This error has been collected.'
                );
                return $this->response($return, REST_CODE_PARAM_ERR);
            } else {
                $return = array(
                    'status' => STATUS_SUCCESS,
                    'msg'    => 'Data has been synchronized Count : ' . count($data)
                );
                return $this->response($return, REST_CODE_OK);
            }
        } else {
            write_log('Parse data error _ Data: ' . $raw_data, 'update_winter_service_task');
            $return = array(
                'status' => STATUS_FAIL,
                'msg'    => 'Data parse error'
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    function upload_success_information_post(){
        $this->upload_tour_data_complete_post();
    }

    function upload_tour_data_post(){
        $this->load->helper('pheanstalk');

        $resources = isset($_FILES) ? $_FILES : '';
        $upload_path = $this->_sync_winter_activities_dir;
        $status      = FALSE;
        $msg         = '';
        if ( isset($resources) AND is_array($resources) AND (!empty($resources)) ) {
            $file_infor_array = array();
            foreach ($resources as $key => $value) {
                $upload_path_tmp = $upload_path. date('Y/m/d');
                $result = $this->_upload_process($key, $upload_path_tmp);
                if(file_exists($result['full_path'])) {
                    $file_name          = date('YmdHis').'_'.$result['file_name'];
                    $file_path          = $result['file_path'].$file_name;
                    $flag               = rename($result['full_path'], $file_path);
                    $file_path_relative = trim($upload_path_tmp, '/') . '/' . $file_name;
                    $file_path_relative = str_replace("//", "/", $file_path_relative);
                    if ($flag) {
                        $item = array(
                            'id_city'    => $this->_id_city,
                            'type'       => 'activity',
                            'data'       => $file_path_relative,
                            'status'     => CRON_STATUS_PENDING,
                            'created_at' => date('Y-m-d H:i:s')
                        );
                        #push_job('upload_tour_data', json_encode($item));
                        $file_infor_array[] = $item;
                        $status = TRUE;
                        $msg    = 'Data has been received';
                    } else{
                        write_log('Can\'t rename file upload_Task: '.date('YmdHis').'_Result: '.json_encode($result), '_upload_tour_data');
                    }
                } else {
                    write_log('File upload not exit_Task: '.date('YmdHis').'_Result: '.json_encode($result), '_upload_tour_data');
                }
            }
            if(isset($file_infor_array) && !empty($file_infor_array)){
                foreach ($file_infor_array as $key => $value) {
                    $data_cron_insert = array(
                        'id_city'    => $this->_id_city,
                        'type'       => 'activity',
                        'data'       => $value['data'],
                        'status'     => CRON_STATUS_PENDING,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $flag = $this->_db_global->insert(TBL_CRON, $data_cron_insert);
                    if($flag != FALSE){
                        write_log('insert ' . TBL_CRON . ' OK data :'.json_encode($data_cron_insert), 'add_push_job');
                        $id_cron = $this->_db_global->insert_id();
                        $cron_push_job = array(
                            'id_cron' => $id_cron,
                            'id_city' => $this->_id_city,
                            'data'    => $value['data']
                        );
                        push_job(QUE_SYNC, json_encode($cron_push_job));
                        write_log('Add queue ' . QUE_SYNC . ' OK data :'.json_encode($cron_push_job), 'add_push_job');
                        $flag = $this->_db_global->insert(TBL_CRON_PUS_JOBS,$cron_push_job);
                        if($flag != FALSE){
                            write_log('insert ' . TBL_CRON_PUS_JOBS . ' OK data :'.json_encode($cron_push_job), 'add_push_job');
                        } else {
                            write_log('insert ' . TBL_CRON_PUS_JOBS . ' FAIL data :'.json_encode($cron_push_job), 'add_push_job');
                        }
                    } else {
                        write_log('insert ' . TBL_CRON . ' FAIL data :'.json_encode($data_cron_insert), 'add_push_job');
                    }
                }
            }
        } else {
            $status = false;
        }

        // return
        $return = array(
            'status'    => $status,
            'msg'       => $msg
        );
        if($status != FALSE){
            return $this->response($return, REST_CODE_OK);
        } else {
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    function upload_tour_data_complete_post(){
        // Pheanstald lib
        $this->load->helper('pheanstalk');

        // service post
        $taskid = $this->post('taskid');
        $module = $this->post('module_id');

        // check complete
        $this->db->where("id_task", $taskid);
        $this->db->where("upload_flag", null);

        $gps_upload_flag_detail = $this->db->get(TBL_WORKER_GPS)->result_array();
        if(isset($gps_upload_flag_detail) AND empty($gps_upload_flag_detail) AND count($gps_upload_flag_detail) < 1){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
                'msg'     => 'function has been called'
            );
            write_log('Function has been called : Id_city : ' . $this->_id_city . ' Taskid : ' . $taskid, 'upload_tour_data_complete');
            return $this->response($return, REST_CODE_OK);
        }

        if(isset($taskid) AND !empty($taskid)){
            $id_gps = 0;
            $gps_detail = array();
            $this->db->where('id_task', $taskid);
            $this->db->where('app', $module);
            $gps_detail = $this->db->get(TBL_WORKER_GPS)->row_array();
            if(is_array($gps_detail) AND !empty($gps_detail)){
                $id_gps = isset($gps_detail['id']) ? intval($gps_detail['id']) : 0;
            } else {
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'Data worker gps empty'
                );
                write_log('Data worker gps empty Id_city : ' . $this->_id_city . ' Taskid : ' . $taskid, 'upload_tour_data_complete');
                return $this->response($return, REST_CODE_PARAM_ERR);
            }

            if(isset($id_gps) AND intval($id_gps) > 0){
                $job = array(
                    'id_city' => $this->_id_city,
                    'id_gps'  => $id_gps
                );
                push_job(QUE_SERVICE_POST, json_encode($job));
                write_log('push_job Id_city : ' . $this->_id_city . ' Taskid : ' . $taskid . ' Data : ' . json_encode($job), 'upload_tour_data_complete');

                // Update worker_gps.upload_flag
                $db_flag = $this->db->where('id', $id_gps)->update(TBL_WORKER_GPS, array('upload_flag' => date('Y-m-d H:s:i')));
                if ($db_flag === false) {
                    write_log('Update upload_flag fail _ Query: ' . $this->db->last_query(), 'upload_tour_data_complete');
                }

                // get gps detail
                $gps = array();
                $this->db->where('id', $id_gps);
                $gps = $this->db->get(TBL_WORKER_GPS)->row_array();
                if(!is_array($gps) OR empty($gps)){
                    write_log('Data gps empty Id_city : ' . $this->_id_city . ' Taskid : ' . $taskid, 'upload_tour_data_complete');
                    $return = array(
                        'version' => config_item('api_version'),
                        'status'  => STATUS_FAIL,
                        'msg'     => 'Data gps empty'
                    );
                    return $this->response($return, REST_CODE_PARAM_ERR);
                }
                $ontime = isset($gps['ontime']) ? $gps['ontime'] : '';
                $employeeid = isset($gps['id_worker']) ? $gps['id_worker'] : '';

                // Push job media
                $information = array();
                $this->db->where('id_worker_gps', $id_gps);
                $this->db->where('type !=', INFO_MESSAGE);
                $information = $this->db->get(TBL_WORKER_INFORMATION)->result_array();
                if(is_array($information) AND !empty($information)){
                    foreach ($information as $key => $val) {
                        $data   = isset($val['data']) ? $val['data'] : '';
                        $path   = $this->_upload_path . $this->_tenantId . '/' . date('Y/m/d',strtotime($ontime)) . '/' . $employeeid . '/' . $data;
                        $rotate = isset($val['rotate']) ? intval($val['rotate']) : 0;

                        write_log("[DEBUG] Information: " . json_encode($val), 'upload_tour_data_complete');
                        switch ($val['type']) {
                            case INFO_IMAGE:
                                $job = array(
                                    'id'     => isset($val['id']) ? $val['id'] : false,
                                    'type'   => INFO_IMAGE,
                                    'path'   => $path,
                                    'rotate' => $rotate
                                );
                                $job_flag = push_job(QUE_MEDIA, json_encode($job));
                                if ($job_flag === false) {
                                    write_log('Push job information FAIL _ Job: '. json_encode($job), 'upload_tour_data_complete');
                                } else {
                                    write_log('Push job information OK _ Job: '. json_encode($job), 'upload_tour_data_complete');
                                }
                                break;
                            case INFO_VOICE:
                                $job = array(
                                    'id'   => isset($val['id']) ? $val['id'] : false,
                                    'type' => INFO_VOICE,
                                    'path' => $path
                                );
                                $job_flag = push_job(QUE_MEDIA, json_encode($job));
                                if ($job_flag === false) {
                                    write_log('Push job information FAIL _ Job: '. json_encode($job), 'upload_tour_data_complete');
                                } else {
                                    write_log('Push job information OK _ Job: '. json_encode($job), 'upload_tour_data_complete');
                                }
                                break;
                            case INFO_VIDEO:
                                break;
                            default:
                                break;
                        }
                    }
                }
                $return = array(
                    'version' => config_item('api_version'),
                    'status' => STATUS_SUCCESS
                );

                // Sent mail notification
                $tmp_unique_id = $this->_id_city . '-' . $taskid;
                $tmp = $this->_db_global->order_by('id', 'desc')->get_where(TBL_TMP, array('unique_id' => $tmp_unique_id))->result();
                if (is_array($tmp) AND !empty($tmp)) {
                    foreach ($tmp as $key_tmp => $value_tmp) {
                        $job_flag = push_job(QUE_EMAIL_EXTRACT, $value_tmp->value);
                        write_log("Push job to que " . QUE_EMAIL_EXTRACT. ' : ' . (($job_flag === false) ? 'FAIL' : 'OK'), 'upload_tour_data_complete');
                    }

                    // Remove tmp data
                    $tmp = $this->_db_global->where('unique_id', $tmp_unique_id)->delete(TBL_TMP);
                    write_log("Remove tmp data _ unique_id " . $tmp_unique_id . ' : ' . (($tmp === false) ? 'FAIL' : 'OK'), 'upload_tour_data_complete');
                }

                return $this->response($return, REST_CODE_OK);
            } else {
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'Data id_gps gps empty'
                );
                write_log('Data id_gps gps empty Id_city : ' . $this->_id_city . ' Taskid : ' . $taskid, 'upload_tour_data_complete');
                return $this->response($return, REST_CODE_PARAM_ERR);
            }
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL,
                'msg'     => 'Taskid empty'
            );
            write_log('Taskid empty Id_city : ' . $this->_id_city , 'upload_tour_data_complete');
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    function upload_image_record_post() {
        $resources = isset($_FILES) ? $_FILES : '';
        $records   = array();
        $counter   = 0;
        $info      = array();

        // Post data
        $id_task     = $this->input->post('task_id');
        $id_employee = $this->input->post('employee_id');
        $id_machine  = $this->input->post('machine_id');
        $short       = $this->input->post('short');

        // Upload patch
        $upload_path = config_item('upload_dir');

        // Worker GPS
        $worker_gps = $this->db->order_by('id', 'desc')->get_where(TBL_WORKER_GPS, array('id_task' => $id_task))->row();

        $status     = true;
        if ( isset($resources) AND is_array($resources) AND (!empty($resources)) ) {
            foreach ($resources as $key => $value) {
                $gps_time        = isset($worker_gps->ontime) ? $worker_gps->ontime : time();
                $id_employee     = isset($worker_gps->id_worker) ? $worker_gps->id_worker : $id_employee;
                $upload_path_tmp = $upload_path . $this->_id_city . '/' . date('Y/m/d', strtotime($gps_time)) . '/' . $id_employee . '/';
                $result          = $this->_upload_process($key, $upload_path_tmp);
                if(file_exists($result['full_path'])) {
                    $counter++;
                    $info[] = $result['file_name'];
                    $flag = rename($result['full_path'], $result['file_path'].$id_task.'_'.$result['file_name']);
                    if (!$flag) {
                        write_log('Can\'t rename file upload_Task: '.$id_task.'_Result: '.json_encode($result), 'upload_image_record');
                    }
                } else {
                    write_log("File upload not exit_Task: '.$id_task.'_Result: ".json_encode($result), 'upload_image_record');
                }
            }
        } else {
            $status = false;
        }
        $status = $status ? STATUS_SUCCESS : STATUS_FAIL;

        if(($short != false) AND isset($short) AND !empty($short)){
            if(isset($info) AND is_array($info) AND !empty($info)){
                unset($info[count($info) - 1]);
            }
        }

        $file_name = isset($info) ? implode(',', $info) : '';

        $return = array(
            'status'    => $status,
            'msg'       => "Received: {$counter}",
            'file_name' => $file_name
        );
        if($status != STATUS_FAIL){
            return $this->response($return, REST_CODE_OK);
        } else {
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    function _walking_nodes($data = array(), $key_checker = '', $key_value = '') {
        #print_r($data);die;
        $return = array();
        // Has one
        if (isset($data[$key_value]) && isset($data[$key_value][$key_checker]) || isset($data[$key_checker])) {
            $return[] = $data[$key_value];
        } else {
            // Has many
            if (!empty($key_value) && isset($data[$key_value]) && !empty($data)) {
                $return = $data[$key_value];
            } else {
                if(isset($data) && !empty($data)){
                    foreach ($data as $key => $value) {
                        $return[] = $value;
                    }
                }
            }
        }
        return $return;
    }

    function _upload_process($field_name, $upload_path) {
        if (!is_dir($upload_path)) {
            umask(0000);
            @mkdir($upload_path, 0755, TRUE);
        }

        // Config for upload
        $config['upload_path']   = $upload_path;
        $config['allowed_types'] = config_item('upload_allowed');
        //$config['encrypt_name']  = true;
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($field_name)) {
            // Log errors
            write_log('Upload file error_'.json_encode($this->upload->display_errors()), 'upload_file_error');

            return FALSE;
        } else {
            $data = $this->upload->data();
            @chmod($data['full_path'], 0644);
            return $data;
        }
    }

    function _save_to_files($data, $file_ext = 'txt') {
        $path = $this->_sync_winter_dir.$this->_id_city.'/'.date('Y/m/d') . '/';
        if (!is_dir($path)) {
            @umask(0000);
            @mkdir ($path, 0755, TRUE);
        }
        $file_name = date('H-i-s_') . uniqid().'.' . $file_ext;
        $file_path = $path.$file_name;
        $this->load->helper('file');
        if (!write_file($file_path, $data)) {
            write_log('Backup data sync error');
            return false;
        }
        return true;
    }

    function test_push_extract_post(){
        return $this->response(urlencode(stripslashes('http://dev.bis-office.com/sys/viewinfor/index/21.0339336/105.8287421')), REST_CODE_OK);ide;
        // Pheanstald lib
        $this->load->helper('pheanstalk');
        $job = json_encode(array(
            'id_city'     => 1,
            'taskid'      => 'aaaaaaaaaaaaaaaaaa',
            'employeeid'  => 1,
            'machineid'   => 1,
            'type'        => INFO_VIDEO,
            'lat'         => '105',
            'lon'         => '21',
            'data'        => 'dadas',
            'created_at'  => date('Y-m-d H:i:s')
            ));
        push_job(QUE_EMAIL_EXTRACT, $job);
        $return = array(
            'status'    => TRUE,
            'msg'       => 'test'
        );
        return $this->response($return, REST_CODE_OK);
    }
}
