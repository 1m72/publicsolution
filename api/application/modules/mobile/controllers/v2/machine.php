<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @create 2014-03-25
* @author huongpm <phung.manh.huong@kloon.vn>
* */
class Machine extends REST_Controller{
	function __construct() {
		parent::__construct();
		if(isset($this->_tenantId) && !empty($this->_tenantId))
			$this->load->helper('color');
		else{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function initialize_get() {
		# get module id
		$id_module = $this->get('moduleid');
		$return = array();
		if(isset($id_module) AND !empty($id_module)){
			$return = $this->machine_get($id_module);
		} else {
			$return = $this->machine_get();
		}
		if(isset($return) AND ( isset($return['status']) AND $return['status'] ==  STATUS_SUCCESS)){
			return $this->response($return,REST_CODE_OK);
		} else {
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function synchronize_machine_get(){
		$getlastupdate = $this->get('lastupdate');
		$lastupdate = date('Y-m-d H:i:s', strtotime($getlastupdate));
		$return = $this->machine_get(FALSE ,$lastupdate);
		if(isset($return) AND ( isset($return['status']) AND $return['status'] ==  STATUS_SUCCESS)){
			return $this->response($return,REST_CODE_OK);
		} else {
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function machine_get($id_module = FALSE ,$lastupdate = FALSE){
		# get module
		$modules = array();
		$this->db->where('deleted_at'	, NULL);
		$this->db->where('id_parent'	, 0);
		if($id_module != FALSE){
			$this->db->where('id'		, $id_module);
		}
		$this->db->order_by('id'	, 0);
		$modules = $this->db->get(TBL_MODULES)->result_array();

		if(!isset($modules) OR empty($modules)){
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'Module empty'
			);
			return $return;
		}
		# get machine_module
		$machine_modules = array();
		$this->db->where('deleted_at'	, NULL);
		$this->db->order_by('id'	, 'asc');
		if($id_module != FALSE){
			$this->db->where('id_module', $id_module);
		}
		$machine_modules = $this->db->get(TBL_MACHINE_MODULE)->result_array();
		if(!isset($machine_modules) OR empty($machine_modules)){
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'Machin_module empty'
			);
			return $return;
		}
		# get machine_module_activity
		$machine_module_activitis = array();
		$this->db->where('deleted_at',NULL);
		$this->db->order_by('id', 'asc');
		$machine_module_activitis = $this->db->get(TBL_MACHINE_MODULE_ACTIVITI)->result_array();
		# get activiti
		$activitis = array();
		$this->db->where('deleted_at'	, NULL);
		$activitis = $this->db->get(TBL_ACTIVITIES)->result_array();

		if(!isset($activitis) OR empty($activitis)){
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'Activiti empty'
			);
			return $return;
		}
		# get material
		$materials = array();
		$this->db->where('deleted_at'	, NULL);
		$materials = $this->db->get(TBL_MATERIAL)->result_array();
		# get machine_material
		$machine_materials = array();
		$this->db->where('deleted_at', NULL);
		$machine_materials = $this->db->get(TBL_MACHINE_MATERIAL)->result_array();
		# get machine
		$machines = array();
		if($lastupdate != FALSE){
			$this->db->where("deleted_at >= '{$lastupdate}' OR updated_at >= '{$lastupdate}' OR created_at >= '{$lastupdate}'");
		} else {
			$this->db->where('deleted_at' , NULL);
		}
		$machines = $this->db->get(TBL_MACHINE)->result_array();
		if(!isset($machines) OR empty($machines)){
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'Machine empty'
			);
			return $return;
		}
		# display
		$_lastupdate = get_lastupdate(TBL_MACHINE, 'updated_at');
		$return = array(
			'version'    => config_item('api_version'),
			'status'     => STATUS_SUCCESS,
			'lastupdate' => $_lastupdate,
		);
		$total = 0;
		foreach ($machines as $machine_key => $machine_value){
			$row_item = array();
			$machin_id = $machine_value['id'];
			$row_item['machineid']		= $machin_id;
			$row_item['machinename']	= $machine_value['name'];
			$row_item['nfc_code']		= $machine_value['nfc_code'];
			# materials
			$row_material = array();
			foreach ($machine_materials as $key => $value){
				if( (isset($value['id_machine']) AND $value['id_machine'] == $machin_id) AND (isset($value['id_material']) AND !in_array($value['id_material'], $row_material)) ){
					$row_material[] = $value['id_material'];
					unset($machine_materials[$key]);
				}
			}
			foreach ($materials as $key => $value){
				if(isset($value['id']) AND in_array($value['id'], $row_material)){
					$row_item['materials']['materialid,'.$key] = $value['id'];
				}
			}
			# modules
			$row_module = array();
			$row_machine_module = array();
			$row_module_activiti_default = array();
			foreach ($machine_modules as $key => $value){
				if((isset($value['id_machine']) AND $value['id_machine'] == $machin_id) AND (isset($value['id_module']) AND !in_array($value['id_module'], $row_module))){
					$row_machine_module[] 			= $value['id'];
					$row_module[] 					= $value['id_module'];
					$row_module_activiti_default[] 	= $value['id_default'];
					unset($machine_modules[$key]);
				}
				#$row_item['modules']['module,'.$module_key]['module_id,'.$module_key] = $module_value['id'];
				#activities - activityid - activitydefault
			}
			foreach ($row_module as $module_key => $module_val){
				$row_item['modules']['module,'.$module_key]['module_id,'.$module_key] = $module_val;
				$row_item['modules']['module,'.$module_key]['activitydefault,'.$module_key] = $row_module_activiti_default[$module_key];
				foreach ($machine_module_activitis as $key => $value){
					if(isset($value['id_machine_module']) AND $value['id_machine_module'] == $row_machine_module[$module_key]){
						$row_item['modules']['module,'.$module_key]['activities,'.$module_key]['activityid,'.$key] = $value['id_activity'];
					}
				}
			}
			if($lastupdate != FALSE){
				$flag = 'N';
				if(isset($machine_value['deleted_at']) AND !empty($machine_value['deleted_at'])){
					$flag = 'D';
				} elseif (isset($machine_value['updated_at']) AND !empty($machine_value['updated_at'])){
					$flag = 'U';
				} else {
					$flag = 'N';
				}
				$row_item['flag'] = $flag;
			}
			if(isset($row_item['modules']) AND !empty($row_item['modules'])){
				$return[] = $row_item;
				$total++;
			}
		}
		$return['total'] = $total;
		return $return;
	}
}
