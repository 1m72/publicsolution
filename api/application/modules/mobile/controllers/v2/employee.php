<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @create 2014-03-25
 * @author huongpm <phung.manh.huong@kloon.vn>
 * */
class Employee extends REST_Controller{
	private $_id_city = -1;
	function __construct() {
		parent::__construct();
		if(isset($this->_tenantId) && !empty($this->_tenantId))
			$this->_id_city = $this->_tenantId;
		else{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function initialize_employee_get(){
		# get module id
		$id_module = $this->get('moduleid');
		$return = array();
		if(isset($id_module) AND !empty($id_module)){
			$return = $this->employee_get($id_module);
		} else {
			$return = $this->employee_get();
		}
		if(isset($return) AND ( isset($return['status']) AND $return['status'] ==  STATUS_SUCCESS)){
			return $this->response($return,REST_CODE_OK);
		} else {
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function synchronize_employee_get(){
		$getlastupdate = $this->get('lastupdate');
		$lastupdate = date('Y-m-d H:i:s', strtotime($getlastupdate));

		$return = $this->employee_get(FALSE, $lastupdate);
		if(isset($return) AND ( isset($return['status']) AND $return['status'] ==  STATUS_SUCCESS)){
			return $this->response($return,REST_CODE_OK);
		} else {
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function employee_get($id_module = FALSE, $lastupdate = FALSE){
		# get module
		$modules = array();
		$this->db->where('deleted_at'	, NULL);
		$this->db->where('id_parent'	, 0);
		if($id_module != FALSE){
			$this->db->where('id'		, $id_module);
		}
		$this->db->order_by('id'	, 'asc');
		$modules = $this->db->get(TBL_MODULES)->result_array();

		if(!isset($modules) OR empty($modules)){
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'Module empty'
			);
			return $return;
			//return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		# get employee_module
		$employee_modules = array();
		$this->db->where('deleted_at' , NULL);
		if($id_module != FALSE){
			$this->db->where('id_module'	, $id_module);
		}
		$employee_modules = $this->db->get(TBL_WORKER_MODULES)->result_array();
		# get activiti
		$activitis = array();
		$this->db->where('deleted_at'	, NULL);
				$activitis = $this->db->get(TBL_ACTIVITIES)->result_array();

		if(!isset($activitis) OR empty($activitis)){
			$return = array(
				'version'  	=> config_item('api_version'),
				'status'   	=> STATUS_FAIL,
				'msg'		=> 'Activiti empty'
			);
			return $return;
			#return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		# get employee_activity
		$employee_activitis = array();
		$this->db->where('deleted_at'	, NULL);
		$employee_activitis = $this->db->get(TBL_WORKER_ACTIVITY_SAVED)->result_array();
		# get employee
		$employees = array();
		if($lastupdate != FALSE){
			$this->db->where("deleted_at >= '{$lastupdate}' OR updated_at >= '{$lastupdate}' OR created_at >= '{$lastupdate}'");
		} else {
			$this->db->where('deleted_at' , NULL);
		}
		$employees = $this->db->get(TBL_WORKER)->result_array();

		if(!isset($employees) OR empty($employees)){
			$return = array(
				'version'  	=> config_item('api_version'),
				'status'   	=> STATUS_FAIL,
				'msg'		=> 'Employee empty'
			);
			return $return;
			#return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		# display
		$_lastupdate = get_lastupdate(TBL_WORKER, 'updated_at');
		//$total		= count($employees);
		$return = array(
			'version'    => config_item('api_version'),
			'status'     => STATUS_SUCCESS,
			'lastupdate' => $_lastupdate,
		);
		$total = 0;
		foreach ($employees as $employee_key => $employee_value){
			$row_item = array();
			# employee
			$employeeid 					= $employee_value['id'];
			$row_item['employeeid'] 		= $employeeid;
			$row_item['firstname'] 			= isset($employee_value['first_name']) 	? $employee_value['first_name'] : '';
			$row_item['lastname'] 			= isset($employee_value['last_name']) 	? $employee_value['last_name'] 	: '';
			$row_item['address'] 			= isset($employee_value['address']) 	? $employee_value['address'] 	: '';
			$row_item['phone'] 				= isset($employee_value['phone']) 		? $employee_value['phone'] 		: '';
			$row_item['activitydefault']	= isset($employee_value['id_default']) 	? $employee_value['id_default'] : '';
			$row_item['nfc_code'] 			= isset($employee_value['nfc_code']) 	? $employee_value['nfc_code'] 	: '';
			# activities
			$row_activiti = array();
			foreach ($employee_activitis as $key => $value){
				if( (isset($value['id_employee']) AND ($value['id_employee'] == $employeeid)) AND (isset($value['id_activity']) AND (!in_array($value['id_activity'], $row_activiti))) ){
					$row_activiti[] = $value['id_activity'];
					unset($employee_activitis[$key]);
				}
			}
			foreach ($activitis as $key => $value){
				if(isset($value['id']) AND in_array($value['id'], $row_activiti)){
					$row_item['activities']['activityid,'.$key] = $value['id'];
				}
			}
			# modules
			$row_module = array();
			foreach ($employee_modules as $key => $value){
				if( (isset($value['id_worker']) AND ($value['id_worker'] == $employeeid)) AND (isset($value['id_module']) AND !in_array($value['id_module'], $row_module)) ){
					$row_module[] = $value['id_module'];
					unset($employee_modules[$key]);
				}
			}
			foreach ($modules as $key => $value){
				if(isset($value['id']) AND in_array($value['id'], $row_module)){
					$row_item['modules']['module_id,'.$key] = $value['id'];
				}
			}
			if($lastupdate != FALSE){
				$flag = 'N';
				if(isset($employee_value['deleted_at']) AND !empty($employee_value['deleted_at'])){
					$flag = 'D';
				} elseif (isset($employee_value['updated_at']) AND !empty($employee_value['updated_at'])){
					$flag = 'U';
				} else {
					$flag = 'N';
				}
				$row_item['flag'] = $flag;
			}
			if(isset($row_item['modules']) AND !empty($row_item['modules'])){
				$return[] = $row_item;
				$total++;
			}
		}
		# response
		$return['total'] = $total;
		return $return;
		#return $this->response($return,REST_CODE_OK);
	}
}
