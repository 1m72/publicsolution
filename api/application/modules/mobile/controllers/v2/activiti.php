<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
class Activiti extends REST_Controller{
	private $id_module = 1;
	function __construct() {
		parent::__construct();
		if(isset($this->_tenantId) && !empty($this->_tenantId))
			$this->load->helper('color');
		else{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function initialize_activity_get()
	{

	}
}
?>