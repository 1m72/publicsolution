<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fasttask extends CI_Controller {
    public function object() {
        $config['hostname'] = "localhost";
        $config['username'] = "root";
        $config['password'] = "1";
        $config['database'] = "ps_auftragsbuch";
        $config['dbdriver'] = "mysql";
        $config['dbprefix'] = "";
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = "";
        $config['char_set'] = "utf8";
        $config['dbcollat'] = "utf8_general_ci";
        $source = $this->load->database($config, true);

        $config['database'] = "ps_local_neuf";
        $target = $this->load->database($config, true);

        $task_id = array();
        // $tmp = sprintf('%06d', 123);
        // echo $tmp;
        // die;

        // Extract - task
        $source->where('giposnr', '0000');
        $result = $source->get('a_auftrag')->result_array();
        $batch_data = array();
        echo '// Begin task';
        foreach ($result as $key => $value) {
            echo '-> ' . $value['sin'] . PHP_EOL;
            $task_id[] = sprintf('%06d', $value['']);
            $batch_data[] = array(
                'giposnr'  => $value['giposnr'],
                'gimatch'  => $value['gimatch'],
                'giaufisn' => $value['giaufisn'],
                'giaugr'   => $value['giaugr'],
                'sin'      => $value['sin'],
            );
        }
        if (!empty($batch_data)) {
            $source->truncate('tmp_task');
            $source->insert_batch('tmp_task', $batch_data);
        }
        echo '// End task' . PHP_EOL . PHP_EOL;

        // Extract - gps
        $source->where('giposnr', '0000');
        $result = $source->get('ps_gpsadresse')->result_array();
        $batch_data = array();
        echo '// Begin gps';
        foreach ($result as $key => $value) {
            echo '-> ' . $value['sin'] . PHP_EOL;
            $batch_data[] = array(
                'giposnr'  => $value['giposnr'],
                'gimatch'  => $value['gimatch'],
                'giaufisn' => $value['giaufisn'],
                'giaugr'   => $value['giaugr'],
                'sin'      => $value['sin'],
            );
        }
        if (!empty($batch_data)) {
            $source->truncate('tmp_gps');
            $source->insert_batch('tmp_gps', $batch_data);
        }
        echo '// End gps' . PHP_EOL . PHP_EOL;

        // SELECT * FROM a_auftrag GROUP BY giaufisn, giposnr
    }
}