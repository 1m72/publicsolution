<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    /**
     * Get session login
     * @param string $return_boolean
     * @return boolean|Ambigous <NULL, unknown>
     * @created 4 Nov 2013
     */
    if (!function_exists('session_login')) {
        function session_login($return_boolean = true){
            $session_key = config_item('session_key');
            if ($return_boolean){
                return isset($_SESSION[$session_key]->id) ? true : false;
            } else {
                return isset($_SESSION[$session_key]) ? $_SESSION[$session_key] : NULL;
            }
        }
    }

    /**
     * Create / Destroy session login
     * @param NULL|string $user_object
     * @return boolean
     * @author Chien Tran <tran.duc.chien@kloon.vn>
     * @created 4 Nov 2013
     */
    if (!function_exists('session_holder')) {
        function session_holder($user_object = NULL){
            # CI instance
            $CI = get_instance();

            $session_key = config_item('session_key');

            # Global server name
            $server_name = explode('.', $_SERVER["SERVER_NAME"]);
            if (is_array($server_name) && (count($server_name) >= 2)){
                $server_name = '.'.$server_name[count($server_name) - 2].'.'.$server_name[count($server_name) - 1];
            } else {
                $server_name = '/';
            }

            if (!is_null($user_object)){
                # Global cookie
                #$CI->input->set_cookie(array('name' => COOKIE_CROSS, 'value' => $user_object->first_name . '|' .$user_object->last_name . '|' . $user_object->display_name, 'expire' => '2629743', 'domain' => $server_name));

                $_SESSION[$session_key] = $user_object;
                return true;
            } else {
                /* Remove cookie */
                #$CI->input->set_cookie(array('name' => COOKIE_AUTH, 'value' => NULL, 'expire' => ''));

                 /* Remove global cookie */
                #$CI->input->set_cookie(array('name' => COOKIE_CROSS, 'value' => NULL, 'domain' => $server_name, 'expire' => ''));

                /* Destroy session */
                $_SESSION[$session_key] = NULL;
                unset($_SESSION[$session_key]);
                return false;
            }
        }
    }
