<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/WEB_Controller.php';
class Sys extends WEB_Controller {
    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->view = 'index';
    }

    function login() {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            if ( ($username == 'kloon') AND ($password == 'Kloon.vn') ) {
                $session_data = json_decode(json_encode(
                    array(
                        'id'       => 1,
                        'username' => $username,
                    )
                ));
                session_holder($session_data);
                redirect('sys');
            } else {
                $this->data['err'] = 'Username or password is invalid';
            }
        }
        $this->layout = false;
        $this->view   = 'login';
    }

    function logout() {
        session_holder(false);
        redirect('sys/login');
    }
}
