<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/WEB_Controller.php';
class Gps extends WEB_Controller {
    private $_id_city     = 1;
    private $_limit       = 10;
    private $_offset      = 0;
    private $_edit_flag   = false;

    function __construct() {
        parent::__construct();
    }

    function index() {
        if ($this->input->is_ajax_request()) {
            $this->_kendoui_process(12, $this->_city_id);
            return;
        }

        $column_properties = array(
            'no_width'      => 50,
            'id_gps_width'  => 60,
            'id_cron_width' => 70,
            'ontime_width'  => 160,
            'created_width' => 160,
            'status_width'  => 90,
            'total_width'   => 80,
            'option_width'  => 100,
        );
        $this->data['column_properties'] = json_encode($column_properties);

        $this->data['limit'] = $this->_limit;
        $this->view          = 'gps/index';
    }
}
