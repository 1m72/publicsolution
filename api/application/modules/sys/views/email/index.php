<script type="text/javascript">
    $(function() {
        var column_properties = $.parseJSON('<?php echo $column_properties; ?>');
        var current_url = '<?php echo current_url(); ?>';
        var id_city     = '?id_city=<?php echo intval($this->input->get('id_city')); ?>';
        var grid_config = {
            'target': '#grid_view',
            'url': current_url + id_city,
            'limit': <?php echo isset($limit) ? intval($limit) : 0; ?>,
            'columns':[
                { field: "number", title: 'No', filterable: false, sortable: false,width: column_properties.no_width, template: function(data) {return grid_number = grid_number + 1;} },
                { field: "to_email", title: 'to email',},
                { field: "status", title: 'status', width: column_properties.status_width,},
                { field: "created_at", title: 'created', type: 'date', format: "{0:MM/dd/yyyy H:m:s}", width: column_properties.created_width,},
                { field: "send_at", title: 'send_at', type: 'date', format: "{0:MM/dd/yyyy H:m:s}", width: column_properties.created_width,},

                { field: "option", title: 'option', attributes:{style: "text-align:center"},filterable: false, sortable: false, width: column_properties.option_width, template: function(data) {
                        if(data.status != 'success'){
                            html = "<a href='javascript:void(0);' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span>&nbsp;Send mail</a>&nbsp;";
                            return html;
                        } else { return '';}
                    }
                }
            ]
        };
        create_grid(grid_config);
    });
</script>
<div id="grid_view"></div>
<script id="toolbar_template" type="text/x-kendo-template">
    <div>
        <span class="pull-left">
            <h4>
                &nbsp;<span class="glyphicon glyphicon-user"></span>
                &nbsp;<strong>LIST GPS</strong>
            </h4>
        </div>
    </div>
</script>