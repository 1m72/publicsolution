<!DOCTYPE html>
<html>
    <head>
        <title>PS Manager</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootswatch/3.1.1/united/bootstrap.min.css">

        <link href="http://cdn.kendostatic.com/2014.1.416/styles/kendo.common.min.css" rel="stylesheet" />
        <link href="http://cdn.kendostatic.com/2014.1.416/styles/kendo.default.min.css" rel="stylesheet" />

        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script src="http://cdn.kendostatic.com/2014.1.416/js/kendo.web.min.js"></script>
        <script src="<?php echo config_item('webapp_link') . 'assets/js/grid_view.js'; ?>"></script>
        <style type="text/css">
        * {
          -webkit-border-radius: 0 !important;
             -moz-border-radius: 0 !important;
                  border-radius: 0 !important;
        }
        </style>
        <script type="text/javascript">
            var global_grid_height = '650px';
        </script>
    </head>
    <body>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url('sys'); ?>">BIS-Office</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">City <b class="caret"></b></a>
                            <ul class="dropdown-menu" role="menu">
                                <?php if(is_array($cities) AND !empty($cities)) :?>
                                <?php foreach ($cities as $key => $value) : ?>
                                <li><a href="<?php echo current_url() . '?id_city=' . $value->id; ?>"><?php echo $value->name; ?></a></li>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                <li class="divider"></li>
                                <li><a href="<?php echo base_url('sys/city') . '?id_city=' . $value->id; ?>">City configs</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url('sys/cron'); ?>">Cron</a></li>
                        <li><a href="<?php echo base_url('sys/email'); ?>">Email</a></li>
                        <li><a href="<?php echo base_url('sys/gps'); ?>">GPS</a></li>
                        <li><a href="<?php echo base_url('sys/nfc'); ?>">NFC</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo base_url('sys/logout'); ?>">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="row-fluid">
            <div class="col-sm-12 col-md-12">
                <?php echo isset($yield) ? $yield : ''; ?>
            </div>
        </div>
  </body>
</html>