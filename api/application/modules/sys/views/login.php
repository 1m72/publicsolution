<!DOCTYPE html>
<html>
    <head>
        <title>PS Manager</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootswatch/3.1.1/united/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ; ?>assets/css/login.css">
        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <form class="form-signin" role="form" method="post">
                <h2 class="form-signin-heading">Please sign in</h2>
                <?php if (isset($err)) : ?>
                <div class="alert alert-warning"><?php echo $err; ?></div>
                <?php endif; ?>
                <input type="text" name="username" class="form-control" placeholder="Username" required="" autofocus="">
                <input type="password" name="password" class="form-control" placeholder="Password" required="">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
        </div>
    </body>
</html>