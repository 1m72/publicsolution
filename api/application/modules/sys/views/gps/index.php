<script type="text/javascript">
    $(function() {
        var column_properties = $.parseJSON('<?php echo $column_properties; ?>');
        var current_url = '<?php echo current_url(); ?>';
        var id_city     = '?id_city=<?php echo intval($this->input->get('id_city')); ?>';
        var grid_config = {
            'target': '#grid_gps',
            'url': current_url + id_city,
            'limit': <?php echo isset($limit) ? intval($limit) : 0; ?>,
            'columns':[
                { field: "number", title: 'No', filterable: false, sortable: false,width: column_properties.no_width, template: function(data) {return grid_number = grid_number + 1;} },
                { field: "id", title: 'id_gps', width: column_properties.id_gps_width,},
                { field: "id_cron", title: 'id_cron', width: column_properties.id_cron_width,},
                { field: "id_task", title: 'id_task',},
                { field: "ontime", title: 'ontime', type: 'date', format: "{0:MM/dd/yyyy H:m:s}", width: column_properties.ontime_width,},
                { field: "created_at", title: 'created', type: 'date', format: "{0:MM/dd/yyyy H:m:s}", width: column_properties.created_width,},
                { field: "status", title: 'status', width: column_properties.status_width,},
                { field: "total_activity", title: 'total', width: column_properties.total_width,},
                { field: "option", title: 'option', attributes:{style: "text-align:center"},filterable: false, sortable: false, width: column_properties.option_width, template: function(data) {
                        if(data.status != 'success'){
                            html = "<a href='javascript:PushJob(" + data.id + ");' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span>&nbsp;Push job</a>&nbsp;";
                            return html;
                        } else { return '';}
                    }
                }
            ]
        };
        create_grid(grid_config);
    });
</script>
<div id="grid_gps"></div>
<script id="toolbar_template" type="text/x-kendo-template">
    <div>
        <span class="pull-left">
            <h4>
                &nbsp;<span class="glyphicon glyphicon-user"></span>
                &nbsp;<strong>LIST GPS</strong>
            </h4>
        </div>
    </div>
</script>