<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

class General extends REST_Controller{
    private $_id_city       = 0;
    private $_database_path = '';
    private $_database_file = 'template.sql';

    function __construct(){
        parent:: __construct();

        if(isset($this->_tenantId) AND !empty($this->_tenantId)){
            $this->_id_city = intval($this->_tenantId);

            if (in_array($this->db->database, array('publicsolution', 'publicsolution_pfinztal', 'publicsolution_freising', 'publicsolution_template'))) {
                $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL,
                    'msg'       => 'Database is invalid !'
                );
                return  $this->response($return, REST_CODE_PARAM_ERR);
            }

            $this->_database_path = config_item('database_dir');
        } else {
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_FAIL,
                'msg'       => 'tenantId not found !'
            );
            return  $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    /**
     * Init new database from table
     * @return RESTfull result
     */
    function init_database_post(){
        $uniqid = uniqid();

        $path = $this->_database_path.$this->_database_file;
        if(isset($path) AND !empty($path) AND is_readable($path)){
            $data = explode(";", file_get_contents($path));
            if(isset($data) AND !empty($data)){
                $all_queries = array();

                # Start transaction
                $this->db->trans_start();

                $last_query = array();
                foreach ($data as $item){
                    if (stripos($item,"VIEW") === FALSE) {
                        try {
                            $this->db->query($item);
                            $all_queries[] = $this->db->last_query();
                        } catch (Exception $ex){}
                    } else {
                        $last_query[] = $item;
                    }
                }

                # Create view
                foreach ($last_query as $last_item){
                    try {
                        $this->db->query($last_item);
                        $all_queries[] = $this->db->last_query();
                    } catch (Exception $ex){}
                }
                $this->db->trans_complete();

                # Transaction status
                if ($this->db->trans_status() === FALSE) {
                    write_log('City: ' . $this->_id_city . '_Transaction error ['.$uniqid.']_Path: '.$path, 'db_template');
                    $return = array(
                        'version'   => config_item('api_version'),
                        'status'    => STATUS_SUCCESS
                    );
                    return  $this->response($return, REST_CODE_OK);
                } else {
                    write_log('City: ' . $this->_id_city . '_Database has been initilize successfully ['.$uniqid.']_Path: '.$path, 'db_template');
                    $return = array(
                        'version'   => config_item('api_version'),
                        'status'    => STATUS_SUCCESS
                    );
                    return  $this->response($return, REST_CODE_OK);
                }

                # Log all queries
                write_log(implode($all_queries, '\n'), 'db_template_'.$uniqid);
            } else {
                write_log('City: ' . $this->_id_city . '_DB template invalid_Path: '.$path, 'db_template');
                $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
                );
                return  $this->response($return, REST_CODE_PARAM_ERR);
            }
        } else {
            write_log('City: ' . $this->_id_city . '_Unable read DB template file_Path: '.$path, 'db_template');
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_FAIL
            );
            return  $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    # Upgrade database template
    function upgrade_db_template_post(){
        try {
            $db_template = $this->load->database('db_template', TRUE);
            $db_config = array(
                'dbdriver' => $db_template->dbdriver,
                'hostname' => $db_template->hostname,
                'username' => $db_template->username,
                'password' => $db_template->password,
                'database' => $db_template->database,
            );
            $db_connected = check_db_connection($db_config);
            if ($db_connected === FALSE) {
                write_log('City: ' . $this->_id_city . '_Unable connect to database template_Info: '.json_encode($db_config), 'db_template');
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'Has an error when connect to database server. Check log for detail'
                );
                return  $this->response($return, REST_CODE_SERVER_ERR);
            } else {
                $this->db = $this->load->database('db_template', TRUE);
                $db_connected = $this->db->initialize();
                $this->load->dbutil();
                $prefs = array(
                    //'tables'   => array('table1'), // Array of tables to backup.
                    'ignore'     => array(),           // List of tables to omit from the backup
                    'format'     => 'txt',             // gzip, zip, txt
                    'filename'   => 'mybackup.sql',    // File name - NEEDED ONLY WITH ZIP FILES
                    'add_drop'   => TRUE,              // Whether to add DROP TABLE statements to backup file
                    'add_insert' => FALSE,             // Whether to add INSERT data to backup file
                    'newline'    => "\n"               // Newline character used in backup file
                );

                $backup  = $this->dbutil->backup($prefs);
                $backup  = preg_replace('/DEFINER=`(.*)`\sSQL/i', 'SQL', $backup);
                $db_name = $this->_database_file.'.tmp';
                $path    = $this->_database_path;
                $save    = $path.$db_name;

                # Check writeable
                if (is_writable($path) === FALSE) {
                    write_log('City: ' . $this->_id_city . '_Unable write to database template path_Path: '.$path, 'db_template');

                    # API return
                    $return = array(
                        'version' => config_item('api_version'),
                        'status'  => STATUS_FAIL,
                        'msg'     => 'Has an error when upgrade template. Please check log for detail'
                    );
                    return $this->response($return, REST_CODE_OK);
                } else {
                    # save
                    $this->load->helper('file');
                    write_file($save, $backup);

                    # Rename
                    rename($path.$this->_database_file, $path.date('YmdHis').'-'.$this->_database_file);
                    rename($path.$db_name, $path.$this->_database_file);

                    # Download
                    //$this->load->helper('download');
                    //force_download($db_name, $backup);

                    # Write log
                    write_log('Template database has been upgrade_City: '.$this->_id_city, 'db_template');

                    # API return
                    $return = array(
                        'version' => config_item('api_version'),
                        'status'  => STATUS_SUCCESS
                    );
                    return $this->response($return, REST_CODE_OK);
                }
            }
        } catch (Exception $e){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => $e
            );
            return  $this->response($return, REST_CODE_SERVER_ERR);
        }
    }
}