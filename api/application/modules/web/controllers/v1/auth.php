<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

/**
 * Authentication controller
 *
 * @author Chien Tran <tran.duc.chien@kloon.vn>
 * @created 1 Nov 2013
 */
class Auth extends REST_Controller{
    function __construct(){
        parent:: __construct();
        $this->load->model('auth_model','auth');
        $this->load->model('user_model','user');
    }

    /**
     * Login to web app with username + password
     * @return array Response parsed
     */
    function login_post() {
        # Username
        $username = $this->post('username');
        if (is_bool($username)) {
            $return = array(
                'version'  => config_item('api_version'),
                'status' => STATUS_FAIL,
                'msg'    => 'username is required'
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        } else {
            $username = trim($username);
        }

        # Password
        $password = $this->post('password');
        if (is_bool($username)) {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'password is required'
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }

        $login_checker = $this->auth->login($username, $password);
        if ($login_checker) {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
                'info'    => $login_checker
            );

            # Update last_login
            $this->_db_global->where('id', $login_checker['id']);
            $this->_db_global->update(TBL_USERS, array('last_login' => date('Y-m-h H:i:s')));

            return $this->response($return, REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => $this->lang->line('auth_err_login_fail')
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    function forgot_request_post() {
        # Email
        $email = $this->post('email');
        if (is_bool($email)) {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Email is required'
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        } else {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $email = trim($email);
            } else {
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'Email is invalid'
                );
                return $this->response($return, REST_CODE_PARAM_ERR);
            }
        }

        # Check exist
        $checker = $this->_db_global->where('email', $email)->count_all_results(TBL_USERS);
        if ($checker > 0) {
            $link_forgot_param = array(
                'email' => $email,
                'hash'  => $this->encrypt->encode($email.'|'.time())
            );
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS
            );
            return $this->response($return, REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Email is not exist'
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    function forgot_validate_post() {
        # Email
        $email = $this->post('email');
        if (is_bool($email)) {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Email is required'
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        } else {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $email = trim($email);
            } else {
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'Email is invalid'
                );
                return $this->response($return, REST_CODE_PARAM_ERR);
            }
        }

        # Hash
        $hash = $this->post('hash');
        if (is_bool($hash)) {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Hash is required'
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        } else {
            $hash = explode('|', trim($hash));
        }

        if ($email AND $hash AND (!empty($hash_password)) AND ($email === $hash[0]) ) {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
            );
            return $this->response($return, REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    function forgot_success_put() {
    }

    function generate_password_get() {
        # Helpers
        $this->load->helper('string');

        # Generate password
        $salt          = random_string('alnum', 8);
        $password = $this->input->get('password');
        if (!$password) {
            $password  = random_string('alnum', 16);
        }
        $hash_password = $this->auth->hash_password($password, $salt);

        $return = array(
            'version'       => config_item('api_version'),
            'status'        => STATUS_SUCCESS,
            'salt'          => $salt,
            'password'      => $password,
            'hash_password' => $hash_password
        );
        return $this->response($return, REST_CODE_OK);
    }

    function pwd_get($pwd) {
        echo $this->auth->hash_password($pwd, 'as#fs^%');
    }
}
