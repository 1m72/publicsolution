<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author HuongPM<phung.manh.huong@kloon.vn>
 * @create 2013-12-11
 * Updated by Chien Tran at 2015-06-04
 * */
class Activity extends REST_Controller{
	private $_id_city = 0;

	function __construct(){
		parent:: __construct();

		if(isset($this->_tenantId) && !empty($this->_tenantId))
			$this->_id_city = intval($this->_tenantId);
		else{
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	# get activity
	function index_get(){
		$id_array_activity = array();
		$id_activity = $this->get('id');
		if(isset($id_activity) && !empty($id_activity)){
			$id_array_activity = convert_param_array($id_activity);
		}

		$id_array_machine = array();
		$id_machine = $this->get('id_machine');
		if(isset($id_machine) && !empty($id_machine)){
			$id_array_machine = convert_param_array($id_machine);
		}

		$id_array_worker = array();
		$id_worker = $this->get('id_worker');
		if(isset($id_worker) && !empty($id_worker)){
			$id_array_worker = convert_param_array($id_worker);
		}
		# order
		$order = array();
		$order_name = $this->get('order_name');
		$order_type = $this->get('order_type');
		if(isset($order_name) && !empty($order_name))
			$order = array($order_name => (isset($order_type) ? $order_type : 'asc'));
		if(count($order) < 1)
			$order = array('name'=>'asc');
		# Activity all
		$this->db->where('deleted_at',null);
		$this->db->where('id_city',$this->_id_city);
		if(isset($id_array_activity) && !empty($id_array_activity))
			$this->db->where_in('id',$id_array_activity);
		if(isset($order) && !empty($order)){
			foreach ($order as $key=>$value){
				$this->db->order_by($key,$value);
			}
		}
		$activities = $this->db->get(TBL_ACTIVITIES)->result_array();
		#
		if(isset($activities) && !empty($activities))
		{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_SUCCESS,
					'data'		=> $activities,
			);
			return $this->response($return,REST_CODE_OK);
		}else{
			$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function index_post(){
		$this->load->helper('pheanstalk');
		$job = array(
			"id_city"       => $this->_id_city,
			"controllers"   => "activity",
			"type"          => "post"
		);
		push_job(QUE_SERVER_UPDATE, json_encode($job));


		$id_ref 	= $this->post('id_ref');
		$id_city 	= $this->_id_city;
		$name		= $this->post('name');
		$color		= $this->post('color');
		$data = array(
			'id_ref'     => $id_ref,
			'id_city'    => $id_city,
			'name'       => $name,
			'color'      => $color,
			'created_at' => date('Y-m-d H:i:s')
		);

		if($this->db->insert(TBL_ACTIVITIES,$data)){
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_SUCCESS
			);
			return $this->response($return,REST_CODE_OK);
		}else{
			$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function index_put(){
		$this->load->helper('pheanstalk');
		$job = array(
			"id_city"       => $this->_id_city,
			"controllers"   => "activity",
			"type"          => "put"
		);
		push_job(QUE_SERVER_UPDATE, json_encode($job));


		$id 		= $this->put('id');
		$id_ref 	= $this->put('id_ref');
		$id_city 	= $this->_id_city;
		$name		= $this->put('name');
		$color		= $this->put('color');
		$data = array(
				'id_ref' 	=> $id_ref,
				'id_city'	=> $id_city,
				'name'		=> $name,
				'color'		=> $color,
				'updated_at'=> date('Y-m-d H:i:s')
		);
		$this->db->where('id',$id);
		if($this->db->update(TBL_ACTIVITIES,$data)){
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_SUCCESS
			);
			return $this->response($return,REST_CODE_OK);
		}else{
			$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function index_delete(){
		$this->load->helper('pheanstalk');
		$job = array(
			"id_city"       => $this->_id_city,
			"controllers"   => "activity",
			"type"          => "delete"
		);
		push_job(QUE_SERVER_UPDATE, json_encode($job));


		$id = $this->delete('id');
		$this->db->where('id',$id);
		if($this->db->update(TBL_ACTIVITIES,array('deleted_at'=>date('Y-m-d H:i:s')))){
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_SUCCESS
			);
			return $this->response($return,REST_CODE_OK);
		}else{
			$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}
}
