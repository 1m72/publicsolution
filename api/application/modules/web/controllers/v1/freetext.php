<?php
require APPPATH . '/controllers/REST_Controller.php';

class Freetext extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('pheanstalk');
    }

    function index_get() {
        $id        = $this->get('id');
        $parent_id = $this->get('parent_id');
        $ids       = array();

        empty($id) || ($ids = convert_param_array($id));
        empty($ids) || ($this->db->where_in('id', $ids));
        empty($parent_id) || $this->db->where('parent_id', $parent_id);

        $order      = array();
        $order_name = $this->get('order_name');
        $order_type = $this->get('order_type');

        $level = $this->get('level');
        empty($level) || $this->db->where('level', $level);

        empty($order_name) || ($order = array(
            $order_name => (isset($order_type) ? $order_type : 'asc')
        ));
        count($order) < 1 && ($order = array(
            'name' => 'asc'
        ));

        $this->db->where('deleted_at', null);
        $freetexts = $this->db->get(TBL_FREETEXT)->result_array();

        if (!empty($freetexts)) {
            $return = array(
                'version' => config_item('api_version'),
                'status' => STATUS_SUCCESS,
                'data' => $freetexts
            );
            return $this->response($return, REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    function index_post() {
        $parent_id = intval($this->post('parent_id'));
        $level     = intval($this->post('level'));
        $title     = $this->post('title');

        if(empty($title)){
            $return = array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }

        $data = array(
            'parent_id' => $parent_id,
            'level' => $level,
            'title' => $title,
            'created_at' => date('Y-m-d H:i:s')
        );

        $status_code = null;
        if ($this->db->insert(TBL_FREETEXT, $data)) {
            $return = array(
                'version' => config_item('api_version'),
                'status' => STATUS_SUCCESS
            );
            $status_code = REST_CODE_OK;
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            );
            $status_code = REST_CODE_PARAM_ERR;
        }

        // Push notification
        $job = array(
            'id_city'  => get_tenant_id(),
            'app'      => APP_FASTTASK_TEXT,
            'msg_key'  => 'server_update',
            'msg_body' => 'text_modules',
        );
        push_job(QUE_SERVER_UPDATE, json_encode($job));

        // Return to client
        return $this->response($return, $status_code);
    }

    function index_put() {
        $id        = intval($this->put('id'));
        $parent_id = $this->put('parent_id');
        $level     = $this->put('level');
        $title     = $this->put('title');

        if(empty($title)){
            $return = array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }

        $data = array(
            'updated_at' => date('Y-m-d H:i:s')
        );

        $parent_id && ($data['parent_id'] = intval($parent_id));
        $level && ($data['level'] = intval($level));
        $title && ($data['title'] = $title);

        $this->db->where('id', $id);
        $status_code = null;
        if ($this->db->update(TBL_FREETEXT, $data)) {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS
            );
            $status_code = REST_CODE_OK;
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL
            );
            $status_code = REST_CODE_PARAM_ERR;
        }

        // Push notification
        $job = array(
            'id_city'  => get_tenant_id(),
            'app'      => APP_FASTTASK_TEXT,
            'msg_key'  => 'server_update',
            'msg_body' => 'text_modules',
        );
        push_job(QUE_SERVER_UPDATE, json_encode($job));

        // Return to client
        return $this->response($return, $status_code);
    }

    function index_delete() {
        $id = intval($this->delete('id'));
        $db_flag = $this->db->where('id', $id)->update(TBL_FREETEXT, array(
            'deleted_at' => date('Y-m-d H:i:s')
        ));

        if ($db_flag) {
            $this->db->where('parent_id', $id);
            $this->db->update(TBL_FREETEXT, array(
                'deleted_at' => date('Y-m-d H:i:s')
            ));

            $return = array(
                'version' => config_item('api_version'),
                'status' => STATUS_SUCCESS
            );

            // Push notification
            $job = array(
                'id_city'  => get_tenant_id(),
                'app'      => APP_FASTTASK_TEXT,
                'msg_key'  => 'server_update',
                'msg_body' => 'text_modules',
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            return $this->response($return, REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }
}
