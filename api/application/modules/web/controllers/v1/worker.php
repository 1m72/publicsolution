<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-11-05
 * */
class Worker extends REST_Controller{
    private $_id_city = 0;
    function __construct(){
        parent:: __construct();
        if(isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->_id_city = intval($this->_tenantId);
            $this->load->model('worker_model','wk');
            $this->load->model('worker_activity_model','wk_acti');
            $this->load->model('worker_information_model','wk_info');
            $this->load->model('gps_model','wk_gps');
        }
        else{
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL,
                    'msg'       => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /*
     * @description
     * @function : _get
     * @author : huongpm<phung.manh.huong@kloon.vn>
     * @create : 2013-11-05
     * @update : 2013-11-11
     * @route
     * */
    function index_get()
    {
        $id_module = $this->get('id_module');
        // if(!$id_module){
        //     $return = array(
        //         'version'   => config_item('api_version'),
        //         'status'    => STATUS_FAIL,
        //         'msg'       => 'Id module fail'
        //     );
        //     return $this->response($return,REST_CODE_PARAM_ERR);
        // }
        $id_worker = $this->get('id_worker');
        $limit = (intval($this->get('limit')) === 0)?100:intval($this->get('limit'));
        $offset = $this->get('offset');
        if(!isset($offset) || empty($offset)){
            $offset = 0;
        }
        # get worker module
        $this->db->where('deleted_at', NULL);
        if (!is_bool($id_module)) {
            $this->db->where('id_module', $id_module);
        }
        $worker_model = $this->db->get(TBL_WORKER_MODULES)->result_array();
        if(!isset($worker_model) OR empty($worker_model)){
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_FAIL,
                'msg'       => 'Worker module empty'
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
        $id_worker_select = array();
        foreach ($worker_model as $key => $val) {
            $id_worker_select[] = $val['id_worker'];
        }
        # get worker
        $this->db->where('deleted_at', NULL);
        $this->db->where_in('id', $id_worker_select);
        $this->db->order_by('id','asc');
        $this->db->limit($limit,$offset);
        $worker = $this->db->get(TBL_WORKER)->result_array();
        if(isset($worker) AND !empty($worker)){
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_SUCCESS,
                'data'      => $worker
            );
            return $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_FAIL,
                'msg'       => 'Worker empty'
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }
    #
    function index_post()
    {
        $id_ref        = $this->post('id_ref');
        $id_city       = $this->post('id_city');
        $id_company    = $this->post('id_company');
        $id_teamleader = $this->post('id_teamleader');
        $nfc_code      = $this->post('nfc_code');
        $personal_code = $this->post('personal_code');
        $first_name    = $this->post('first_name');
        $last_name     = $this->post('last_name');
        $address       = $this->post('address');
        $phone         = $this->post('phone');
        $birthday      = $this->post('birthday');
        $id_activity   = $this->post('id_activity');
        $id_default    = $this->post('id_default');

        $data = array(
            'id_ref'        => $id_ref,
            'id_city'       => $id_city,
            'id_company'    => $id_company,
            'id_teamleader' => $id_teamleader,
            'nfc_code'      => $nfc_code,
            'personal_code' => $personal_code,
            'first_name'    => $first_name,
            'last_name'     => $last_name,
            'id_default'    => $id_default,
            'address'       => $address,
            'phone'         => $phone,
            'birthday'      => $birthday,
            'created_at'    => date('Y-m-d H:i:s')
        );
        if($this->db->insert(TBL_WORKER,$data)){
            $this->load->helper('pheanstalk');
            $job = array(
                "id_city" => $this->_id_city,
                "worker"  => "worker",
                "type"    => "post"
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            $id_worker = $this->db->insert_id();
            # nfc update
            if(isset($nfc_code) && !empty($nfc_code)){
                $this->db->where('nfc_code',$nfc_code);
                $this->db->update(TBL_NFC,array('active'=>NFC_ACTIVE));
            }
            # activity
            if(isset($id_activity) && !empty($id_activity)){
                $array_activity = explode(',', $id_activity);
                $activity_data = array();
                foreach ($array_activity as $ac_item)
                {
                    $item = array(
                            'id_employee'   => $id_worker,
                            'id_activity'   => $ac_item,
                            'created_at'    => date('Y-m-d H:i:s')
                    );
                    array_push($activity_data, $item);
                }
                if($this->db->insert_batch(TBL_EMPLOYEE_ACTIVITY,$activity_data)){
                    $return = array(
                            'version'   => config_item('api_version'),
                            'status'    => STATUS_SUCCESS
                    );
                    return $this->response($return,REST_CODE_OK);
                }else{
                    $return = array(
                            'version'   => config_item('api_version'),
                            'status'    => STATUS_FAIL
                    );
                    return $this->response($return,REST_CODE_PARAM_ERR);
                }
            }else{
                $return = array(
                        'version'   => config_item('api_version'),
                        'status'    => STATUS_FAIL
                );
                return $this->response($return,REST_CODE_PARAM_ERR);
            }
        }else{
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function index_put()
    {
        $id            = $this->put('id');
        $id_ref        = $this->put('id_ref');
        $id_city       = $this->put('id_city');
        $id_company    = $this->put('id_company');
        $id_teamleader = $this->put('id_teamleader');
        $nfc_code      = $this->put('nfc_code');
        $personal_code = $this->put('personal_code');
        $first_name    = $this->put('first_name');
        $last_name     = $this->put('last_name');
        $address       = $this->put('address');
        $phone         = $this->put('phone');
        $birthday      = $this->put('birthday');
        $id_activity   = $this->put('id_activity');
        $id_default    = $this->put('id_default');

        $data = array(
            'id_ref'        => $id_ref,
            'id_city'       => $id_city,
            'id_company'    => $id_company,
            'id_teamleader' => $id_teamleader,
            'nfc_code'      => $nfc_code,
            'personal_code' => $personal_code,
            'first_name'    => $first_name,
            'last_name'     => $last_name,
            'id_default'    => $id_default,
            'address'       => $address,
            'phone'         => $phone,
            'birthday'      => $birthday,
            'updated_at'    => date('Y-m-d H:i:s')
        );
        # get nfc old
        $this->db->where('id',$id);
        $wk_detail = $this->db->get(TBL_WORKER)->row_array();
        $_nfc_code = isset($wk_detail['nfc_code']) ? $wk_detail['nfc_code'] : '';

        $this->db->where('id',$id);
        if($this->db->update(TBL_WORKER, $data)){
            $this->load->helper('pheanstalk');
            $job = array(
                "id_city" => $this->_id_city,
                "worker"  => "worker",
                "type"    => "put"
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            $this->db->where('id_employee',$id);
            $this->db->update(TBL_EMPLOYEE_ACTIVITY,array('deleted_at'=>date('Y-m-d H:i:s')));
            # nfc update
            if(isset($_nfc_code) && !empty($_nfc_code)){
                $this->db->where('nfc_code',$_nfc_code);
                $this->db->update(TBL_NFC,array('active' => NFC_UNACTIVE,'updated_at' => date('Y-m-d H:i:s')));
            }
            if(isset($nfc_code) && !empty($nfc_code)){
                $this->db->where('nfc_code',$nfc_code);
                $this->db->update(TBL_NFC,array('active' => NFC_ACTIVE,'updated_at' => date('Y-m-d H:i:s')));
            }
            # activity
            if(isset($id_activity) && !empty($id_activity)){
                $array_activity = explode(',', $id_activity);
                $activity_data = array();
                foreach ($array_activity as $ac_item)
                {
                    $item = array(
                            'id_employee'   => $id,
                            'id_activity'   => $ac_item,
                            'created_at'    => date('Y-m-d H:i:s')
                    );
                    array_push($activity_data, $item);
                }
                if($this->db->insert_batch(TBL_EMPLOYEE_ACTIVITY,$activity_data)){
                    $return = array(
                            'version'   => config_item('api_version'),
                            'status'    => STATUS_SUCCESS
                    );
                    return $this->response($return,REST_CODE_OK);
                }else{
                    $return = array(
                            'version'  => config_item('api_version'),
                            'status'   => STATUS_FAIL
                    );
                    return $this->response($return,REST_CODE_PARAM_ERR);
                }
            }else{
                $return = array(
                        'version'  => config_item('api_version'),
                        'status'   => STATUS_FAIL
                );
                return $this->response($return,REST_CODE_PARAM_ERR);
            }
        }else{
            $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /*
     * @description
     * @function : nfc_worker_post
     * @author : huongpm<phung.manh.huong@kloon.vn>
     * @create : 2013-11-05
     * @route
     * */
    function nfc_worker_post()
    {
        $worker_id = $this->post('id_worker');
        $nfc_code = $this->post('nfc_code');

        if(!$this->wk->link_nfc_worker($worker_id,$nfc_code))
        {
            $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_SUCCESS
            );
            return $this->response($return,REST_CODE_OK);
        }
        else
        {
            $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /*
     * @description
     * @function : worker_teamlead_post
     * @author : huongpm<phung.manh.huong@kloon.vn>
     * @create : 2013-11-05
     * @route
     * */
    function worker_teamlead_post()
    {
        $worker_id = $this->post('id_worker');
        $teamlead_id = $this->post('id_teamlead');

        if($this->wk->link_worker_teamlead($worker_id,$teamlead_id))
        {
            $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_SUCCESS
            );
            return $this->response($return,REST_CODE_OK);
        }
        else
        {
            $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /*
     * @description
     * @function : index_delete
     * @author : Le Thi Nhung(le.nhung@kloon.vn)
     * @create : 2013-12-07
     * @route
     * */
    function index_delete()
    {
        $idworker = intval($this->delete('id_worker'));
        if($idworker){
            $this->db->where('id', $idworker);
            $flag = $this->db->update(TBL_WORKER, array('deleted_at' => date('Y-m-d H:i:s')));
            if($flag == true){
                $this->load->helper('pheanstalk');
                $job = array(
                    "id_city" => $this->_id_city,
                    "worker"  => "machine",
                    "type"    => "delete"
                );
                push_job(QUE_SERVER_UPDATE, json_encode($job));

                $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_SUCCESS
                );
                return $this->response($return,REST_CODE_OK);
            } else {
                $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_FAIL
                );
                return $this->response($return,REST_CODE_PARAM_ERR);
            }
        } else {
            $return = array(
                'version'  => config_item('api_version'),
                'status'   => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }
}
