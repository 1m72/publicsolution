<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author 	huongpm<phung.manh.huong@kloon.vn>
 * @create	2013-11-04
 * */
class Company extends REST_Controller
{
	function __construct()
	{
		parent::__construct();

		if(isset($this->_tenantId) && !empty($this->_tenantId))
			$this->load->model('company_model','com');
		else{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}
	/*
	 * @description
	 * @function	index_post
	 * @author 		huongpm<phung.manh.huong@kloon.vn>
	 * @create		2013-11-05
	 * @route
	* */
	function index_post()
	{
		$idcity = intval($this->post('id_city'));
		$name = $this->post('name');
		$des = $this->post('description');

		if($this->db->insert(TBL_COMPANY,array(
				'id_city' 		=> $idcity,
				'name' 			=> $name,
				'description' 	=> $des,
				'created_at' 	=> date('Y-m-d H:i:s')
		))){
			$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_SUCCESS
			);
			return  $this->response($return,REST_CODE_OK);
		}else{
			$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_FAIL
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}
	/*
	 * @description
	 * @function : index_get
	 * @author : huongpm<phung.manh.huong@kloon.vn>
	 * @create : 2013-11-05
	 * @route
	 * */
	function index_get()
	{
		$id_array_city = array();
		$id_city = $this->get('id_city');
		if(isset($id_city) && !empty($id_city)){
			$id_array_city = convert_param_array($id_city);
		}

		$id_array_company = array();
		$id_company = $this->get('id_company');
		if(isset($id_company) && !empty($id_company)){
			$id_array_company = convert_param_array($id_company);
		}

		$limit = (intval($this->get('limit')) === 0)?100:intval($this->get('limit'));
		$offset = $this->get('offset');
		if(!isset($offset) || empty($offset))
			$offset = 0;
		#order
		$order = array();
		$order_name = $this->get('order_name');
		$order_type = $this->get('order_type');
		if(isset($order_name) && !empty($order_name))
			$order = array($order_name => (isset($order_type) ? $order_type : 'asc'));
		if(count($order) < 1)
			$order = array('name'=>'asc');
		#get
		$company = $this->com->getcompany($id_array_company,$id_array_city,$limit,$offset,$order);
		if(isset($company) && !empty($company))
		{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_SUCCESS,
					'data'		=> $company
			);
			return $this->response($return,REST_CODE_OK);
		}
		else{
			$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	/*
	 * @description
	 * @function : index_put
	 * @author : huongpm<phung.manh.huong@kloon.vn>
	 * @create : 2013-11-05
	 * @route
	 * */
	function index_put()
	{
		$id = intval($this->put('id_company'));
		$idcity = $this->put('id_city');
		$name = $this->put('name');
		$des = $this->put('description');

		if($this->com->update(TBL_COMPANY,array('id'=>$id),array(
				'id_city' 		=> $idcity,
				'name' 			=> $name,
				'description' 	=> $des,
				'updated_at' 	=> date('Y-m-d H:i:s')
		)))
		{
			$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_SUCCESS
			);
			return $this->response($return,REST_CODE_OK);
		}
		else
		{
			$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}
	/*
	 * @description
	 * @function : index_delete
	 * @author : huongpm<phung.manh.huong@kloon.vn>
	 * @create : 2013-11-05
	 * @route
	 * */
	function index_delete()
	{
		$id = intval($this->delete('id_company'));
		if($this->com->update(TBL_COMPANY,array('id'=>$id),array('deleted_at' 	=> date('Y-m-d H:i:s'))))
		{
			$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_SUCCESS
			);
			return $this->response($return,REST_CODE_OK);
		}
		else
		{
			$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function companybycity_get(){
		$a = array(
			'id'  =>1,
			'name'=>'Company 1',
		);
		$b = array(
			'id'  =>2,
			'name'=>'Company 2',
		);
		$l = array($a,$b);
		echo json_encode($l);

	}
}
?>