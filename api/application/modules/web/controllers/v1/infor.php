<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-11-07
 * */
class Infor extends REST_Controller{
	private $_media_path = '';
	function __construct(){
		parent:: __construct();

		if(isset($this->_tenantId) && !empty($this->_tenantId)){
			$this->load->model('infor_model','info');
			$this->_media_path = config_item('media_dir');
		}else{
			$return = array(
				'version'  	=> config_item('api_version'),
				'status'   	=> STATUS_FAIL,
				'msg'		=> 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function detail_get(){
		$id 	= $this->get('id');
		$id_gps = $this->get('id_gps');
		if(($id == false) OR ($id_gps == false)){
			$return = array(
				'version'  	=> config_item('api_version'),
				'status'   	=> STATUS_FAIL,
				'msg'		=> 'id not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		if(isset($id) AND !is_array($id) AND !empty($id)){
			$id = explode(',', $id);
		}

		if(!is_array($id) OR empty($id)){
			$return = array(
				'version'  	=> config_item('api_version'),
				'status'   	=> STATUS_FAIL,
				'msg'		=> 'array id found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		# GPS

		$this->db->where('id', $id_gps);
		$gps_detail = $this->db->get(TBL_WORKER_GPS)->row_array();

		if(!isset($gps_detail) OR !is_array($gps_detail) OR empty($gps_detail)){
			$return = array(
				'version'  	=> config_item('api_version'),
				'status'   	=> STATUS_FAIL,
				'msg'		=> 'data gps empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		$time = $gps_detail['ontime'];
		$id_wk = $gps_detail['id_worker'];
		$id_city = $gps_detail['id_city'];

		# Information

		$this->db->where_in('id', $id);
		$information = $this->db->get(TBL_WORKER_INFORMATION)->result_array();

		if(!isset($information) OR !is_array($information) OR empty($information)){
			$return = array(
				'version'  	=> config_item('api_version'),
				'status'   	=> STATUS_FAIL,
				'msg'		=> 'information detail id :' . $id . ' not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		foreach ($information as $key => $value) {
			if(isset($value['type']) AND ($value['type'] != INFO_MESSAGE)){
				$path_media      = $this->_media_path . $id_city . '/' . date('Y/m/d',strtotime($time)) . '/' . $id_wk . '/' . $value['data'];
				$path_thumbs     = config_item('images_link') . 'w450/' . $id_city .'/'.date('Y/m/d',strtotime($time)).'/'.$id_wk.'/'.$value['data'];
				$information[$key]['data']   = $path_media;
				$information[$key]['thumbs'] = $path_thumbs;
			}
		}

		$return = array(
			'version' => config_item('api_version'),
			'status'  => STATUS_SUCCESS,
			'data'	  => $information
		);
		return $this->response($return,REST_CODE_OK);
	}

	function get_location_by_activity_get(){
		$param_id_gps 		= $this->get('id_gps');
		$param_start_time 	= $this->get('start_time');
		$param_end_time		= $this->get('end_time');
		#
		$this->db->where('id_wkgps',$param_id_gps);
		$this->db->where('starttime >=',$param_start_time);
		$this->db->where('starttime <=',$param_end_time);
		$locations = array();
		$result_location = $this->db->get(TBL_WORKER_ACTIVITY)->result_array();

		if(isset($result_location) && !empty($result_location)){
			foreach ($result_location as $item){
				$locations[] = array(
						'lat'	=> $item['latitude'],
						'lon'	=> $item['longtitude'],
						'time'	=> $item['starttime']
				);
			}
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_SUCCESS,
					'location'	=> $locations
			);
			return $this->response($return,REST_CODE_OK);
		} else {
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function get_information_by_activiti_get(){
		$param_id_gps 	= $this->get('id_gps');
		$param_start_time = $this->get('start_time');
		$param_end_time	= $this->get('end_time');
		#
		$param_lat		= $this->get('lat');
		$param_lon		= $this->get('lon');
		#
		# infor
		$wk_obj = $this->info->get_obj_worker($param_id_gps);
		$id_wk = 0;
		$work = '';
		$obj = '';
		if(isset($wk_obj) && !empty($wk_obj))
		{
			$work 	= $wk_obj[0]['first_name'].' '.$wk_obj[0]['last_name'];
			$id_wk 	= $wk_obj[0]['id_wk'];
		}
		#
		$result_infor = array();
		$this->db->where('id_worker_gps',$param_id_gps);
		if(isset($param_lat) && !empty($param_lat) && isset($param_lon) && !empty($param_lon)){
			$this->db->where('lat',$param_lon);
			$this->db->where('lon',$param_lat);
		} else {
			$this->db->where('time >=',date('Y-m-d H:i:s', strtotime($param_start_time)));
			$this->db->where('time <=',date('Y-m-d H:i:s', strtotime($param_end_time)));
		}
		$result_infor = $this->db->get(TBL_WORKER_INFORMATION)->result_array();

		if(isset($result_infor) && !empty($result_infor)){
			$message = 0;
			$video = 0;
			$image = 0;
			$voice = 0;
			$infor = array();
			foreach ($result_infor as $key => $value) {
				$_data_msg       = $value['data'];
				$path_media      = $this->_media_path . $value['id_city'].'/'.date('Y/m/d',strtotime($value['time'])).'/'.$id_wk.'/'.$value['data'];
				$path_thumbs     = config_item('images_link') . 'w450/' . $value['id_city'].'/'.date('Y/m/d',strtotime($value['time'])).'/'.$id_wk.'/'.$value['data'];
				$value['data']   = $path_media;
				$value['thumbs'] = $path_thumbs;
				unset($value['starttime']);
				unset($value['endtime']);
				switch ($value['type'])
				{
					case INFO_IMAGE:
						$image++;
						break;
					case INFO_VIDEO:
						$video++;
						break;
					case INFO_VOICE:
						$video++;
						break;
					case INFO_MESSAGE:
						$message++;
						$value['data'] = $_data_msg;
						break;
					default:
						break;
				}
				$infor[] = $value;
			}
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_SUCCESS,
					'worker'	=> $work,
					'object'	=> $obj,
					'count'		=> count($infor),
					'message'	=> $message,
					'video'		=> $video,
					'image'		=> $image,
					'voice'		=> $voice,
					'starttime'	=> $param_start_time,
					'endtime'	=> $param_end_time,
					'data' 		=> $infor
			);
			return $this->response($return,REST_CODE_OK);
		} else{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function infor_by_gps_activity_get(){
		$id_wk_gps 		= $this->get('id_worker_gps');
		$id_wk_activiti = $this->get('id_worker_activiti');
		$latitude		= $this->get('latitude');
		$longtitude		= $this->get('longtitude');

		$position 			= '';
		$activiti_position 	= '';
		$location_array		= array();
		$start_location 	= '';
		$start_time			= '';
		$end_location 		= '';
		$end_time			= '';
		if(isset($id_wk_gps) && !empty($id_wk_gps)){
			# location
			$this->db->select('position');
			$this->db->from(TBL_WORKER_GPS);
			$this->db->where('id',$id_wk_gps);
			$gps_detail = $this->db->get()->row_array();
			if(isset($gps_detail) && !empty($gps_detail)){
				$position = isset($gps_detail['position']) ? $gps_detail['position'] : '';
			}
			# get location
			if(isset($id_wk_activiti) && !empty($id_wk_activiti)){
				$this->db->where('id',$id_wk_activiti);
				$activiti_detail = $this->db->get(TBL_WORKER_ACTIVITY)->row_array();

				if(isset($activiti_detail) && !empty($activiti_detail)){
					$start_time		= isset($activiti_detail['starttime']) ? $activiti_detail['starttime'] : '';
					$start_location = (isset($activiti_detail['latitude']) ? $activiti_detail['latitude'] : '').' '.(isset($activiti_detail['longtitude']) ? $activiti_detail['longtitude'] : '').' '.$start_time.',';
					$_end_location 	= isset($activiti_detail['end_lat_lon']) ? $activiti_detail['end_lat_lon'] : '';
					if(isset($_end_location) && !empty($_end_location)){
						$end_location_array = json_decode($_end_location,true);
						if(isset($end_location_array) && !empty($end_location_array)){
							$end_time = isset($end_location_array['time']) ? $end_location_array['time'] : '';
							$end_location = (isset($end_location_array['lat']) ? $end_location_array['lat'] : '').' '.(isset($end_location_array['lon']) ? $end_location_array['lon'] : '').' '.$end_time.',';
						}
					}
				}
			}

			$fist = 0;
			$last = strlen($position);

			if(isset($start_location) && !empty($start_location)){
				$fist = $this->_lastIndexOf($position,$start_location);
			}

			if(isset($end_location) && !empty($end_location)){
				$last = $this->_lastIndexOf($position,$end_location);
			}

			$activiti_position = $start_location.substr($position,$fist,$last).$end_location;
			if(isset($activiti_position) && !empty($activiti_position)){
				$activiti_position = trim($activiti_position,',');
				$_location_array = explode(',',$activiti_position);
				if(is_array($_location_array) && isset($_location_array)){
					foreach ($_location_array as $item_loca){
						$_item_array = explode(' ',$item_loca);
						$location_array[] = array(
								'lat'	=> isset($_item_array[0]) ? $_item_array[0] : '',
								'lon'	=> isset($_item_array[1]) ? $_item_array[1] : '',
								'time'	=> isset($_item_array[2]) ? $_item_array[2] : ''
						);
					}
				}

			}
			# infor
			$wk_obj = $this->info->get_obj_worker($id_wk_gps);
			$work = '';
			$obj = '';
			if(isset($wk_obj) && !empty($wk_obj))
			{
				$work = $wk_obj[0]['first_name'].' '.$wk_obj[0]['last_name'];
				$obj = $wk_obj[0]['obj_name'];
			}

			$result = $this->info->getinfo($id_wk_gps, $id_wk_activiti, $latitude, $longtitude);
			if(isset($result) && !empty($result)){
				$message = 0;
				$video = 0;
				$image = 0;
				$voice = 0;
				$infor = array();
				$starttime = NULL;
				$endtime = NULL;
				foreach ($result as $key => $value) {
					if(!isset($starttime) && empty($starttime))
					{
						$starttime = $value['starttime'];
					}
					if(isset($value['endtime']) && !empty($value['endtime'])){
						$endtime = $value['endtime'];
					}
					$_data_msg = $value['data'];
					$path_media = $this->_media_path.$value['id_city'].'/'.date('Y/m/d',strtotime($value['ontime'])).'/'.$value['id_worker'].'/'.$value['data'];
					//if (isset($path_media) AND (file_exists($path_media) == TRUE)){
						$value['data'] = $path_media;
						unset($value['starttime']);
						unset($value['endtime']);
						switch ($value['type'])
						{
							case INFO_IMAGE:
								$image++;
								break;
							case INFO_VIDEO:
								$video++;
								break;
							case INFO_VOICE:
								$video++;
								break;
							case INFO_MESSAGE:
								$message++;
								$value['data'] = $_data_msg;
								break;
							default:
								break;
						}
						$infor[] = $value;
					//}
				}
				$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_SUCCESS,
					'worker'	=> $work,
					'object'	=> $obj,
					'count'		=> count($infor),
					'message'	=> $message,
					'video'		=> $video,
					'image'		=> $image,
					'voice'		=> $voice,
					'starttime'	=> $starttime,
					'endtime'	=> $endtime,
					'data' 		=> $infor,
					'location'	=> $location_array
				);
				return $this->response($return,REST_CODE_OK);
			} else{
				$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL
				);
				return $this->response($return,REST_CODE_PARAM_ERR);
			}
		} else {
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	/*
	 * @description
	 * @function	index_get
	 * @author 		huongpm<phung.manh.huong@kloon.vn>
	 * @create		2013-11-06
	 * @route
	 * */
	function index_get()
	{
		$id_worker_gps = $this->get('id_worker_gps');
		if(isset($id_worker_gps) && !empty($id_worker_gps))
		{
			$object = NULL;

			$result = $this->info->getinfo($id_worker_gps);

			$count = count($result);
			$message = 0;
			$video = 0;
			$image = 0;
			$voice = 0;
			$infor = array();
			$starttime = NULL;
			$endtime = NULL;
			foreach ($result as $item)
			{
				if(!isset($starttime) && empty($starttime))
				{
					$starttime = $item['starttime'];
				}
				if(isset($item['endtime']) && !empty($item['endtime'])){
					$endtime = $item['endtime'];
				}
				$compamy_id = $this->info->get_company_by_city($item['id_city']);
				$url_infor = $this->_media_path.$item['id_city'].'/'.date('Y/m/d',strtotime($item['ontime'])).'/'.$item['id_worker'].'/'.$item['data'];
				$item['data'] = $url_infor;
				unset($item['starttime']);
				unset($item['endtime']);
				$infor[] = $item;
				switch ($item['type'])
				{
					case INFO_IMAGE:
						$image++;
						break;
					case INFO_VIDEO:
						$video++;
						break;
					case INFO_VOICE:
						$video++;
						break;
					case INFO_MESSAGE:
						$message++;
						break;
					default:
						break;
				}
			}
			//city/company/Y/M/D/worker/
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_SUCCESS,
					'count'		=> $count,
					'message'	=> $message,
					'video'		=> $video,
					'image'		=> $image,
					'voice'		=> $voice,
					'starttime'	=> $starttime,
					'endtime'	=> $endtime,
					'data' 		=> $infor
			);

			return $this->response($return,REST_CODE_OK);
		}
		else
		{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL
			);

			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function activiti_by_latlon_get(){

		$return = array(
			'version'  	=> config_item('api_version'),
			'status'   	=> STATUS_FAIL
		);
		$rest_code_parem_err = REST_CODE_PARAM_ERR;
		$lat 	= $this->get('latitude');
		$lon	= $this->get('longtitude');
		$id_gps = $this->get('id_gps');
		#
		if( (isset($lat) AND !empty($lat)) AND (isset($lon) AND !empty($lon)) ){
			$_url_path_service  = 'http://nominatim.kloon.net/reverse?format=json&zoom=17&addressdetails=1&lat='.$lat.'&lon='.$lon;
			$_url_path_service = str_replace("'", "", $_url_path_service);

			$street_data = file_get_contents($_url_path_service);

			$street_array = json_decode($street_data,true);
			if(is_array($street_array) AND !empty($street_array)){
				if (isset($street_array['address']) && !empty($street_array['address'])) {
	                $street_detail = $street_array['address'];
	                if(isset($street_detail['road']) && !empty($street_detail['road'])){
	                    $street_name   = isset($street_detail['road']) ? $street_detail['road'] : '';
	                    if( isset($street_name) AND !empty($street_name) ){
	                    	$return = array(
								'version'  		=> config_item('api_version'),
								'status'   		=> STATUS_SUCCESS,
								'street_name'	=> $street_name
							);
	                    }#
	                }#
	            }#
			}#
		}#

		if( isset($return['street_name']) AND !empty($return['street_name']) )
			$rest_code_parem_err = REST_CODE_OK;
		return $this->response($return,$rest_code_parem_err);
	}

	function _lastIndexOf($string,$item){
		$index=strpos(strrev($string),strrev($item));
		if ($index){
			$index=strlen($string)-strlen($item)-$index;
			return $index;
		} else {
			return -1;
		}
	}
}