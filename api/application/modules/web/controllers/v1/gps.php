<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-11-05
 * */
class Gps extends REST_Controller{
    private $_path_service = 'http://services.gisgraphy.com/street/streetsearch?format=json';

    function __construct(){
        parent:: __construct();

        if(isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->load->model('gps_model','gpsm');
        }
        else{
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL,
                    'msg'       => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function update_position_post(){
        $id_gps = $this->post('id_gps');
        $this->db->where('id_wkgps',$id_gps);
        $result = $this->db->get(TBL_WORKER_ACTIVITY)->result_array();
        if(isset($result) && !empty($result)){
            $position = '';
            foreach ($result as $item){
                $lat    = isset($item['latitude']) ? trim($item['latitude']) : '';
                $lon    = isset($item['longtitude']) ? trim($item['longtitude']) : '';
                $time   = isset($item['starttime']) ? trim($item['starttime']) : '';
                if(isset($position) && !empty($position)) {
                    $position .= ','.$lat.' '.$lon.' '.$time;
                }
                else {
                    $position = $lat.' '.$lon.' '.$time;
                }
            }
            $this->db->where('id', $id_gps);
            if($this->db->update(TBL_WORKER_GPS,array('position'=>$position,'updated_at'=>date('Y-m-d H:i:s')))){
                $return = array(
                        'version'   => config_item('api_version'),
                        'status'    => STATUS_SUCCESS
                );
                return $this->response($return,REST_CODE_PARAM_ERR);
            } else {
                $return = array(
                        'version'   => config_item('api_version'),
                        'status'    => STATUS_FAIL
                );
                return $this->response($return,REST_CODE_PARAM_ERR);
            }
        } else {
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function update_time_post(){
        $id_gps = $this->post('id_gps');
        $update = array();

        $this->db->where('id_wkgps',$id_gps);
        $this->db->order_by('id','asc');
        $result = $this->db->get(TBL_WORKER_ACTIVITY)->result_array();
        $start_time = NULL;
        if(isset($result) && !empty($result)){
            $start_time = $result[0]['starttime'];
        }
        #
        if($start_time){
            $count = 0;
            foreach ($result as $item){
                $update[] = array(
                        'id'        => $item['id'],
                        'starttime' => date('Y-m-d H:i:s',strtotime($start_time) + $count),
                        'endtime'   => date('Y-m-d H:i:s',strtotime($start_time) + $count)
                );
                $count++;
            }
        }
    }

    function activity_by_wkgps_get(){
        $id_gps = $this->get('id_gps');
        if(isset($id_gps) && !empty($id_gps) && ($id_gps > 0)){
            $activity = $this->gpsm->get_activity_by_worker($id_gps);
            if(isset($activity) && !empty($activity)){
                $return = array(
                        'version'   => config_item('api_version'),
                        'status'    => STATUS_SUCCESS,
                        'data'      => $activity
                );
                return $this->response($return,REST_CODE_PARAM_ERR);
            } else {
                $return = array(
                        'version'   => config_item('api_version'),
                        'status'    => STATUS_FAIL
                );
                return $this->response($return,REST_CODE_PARAM_ERR);
            }
        } else {
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /**
     * @description
     * @function    index_get
     * @author      huongpm<phung.manh.huong@kloon.vn>
     * @create      2013-11-05
     * @route
     */
    function gps_by_id_get()
    {
        $id_gps = trim($this->get('id_gps'));
        if(isset($id_gps) && !empty($id_gps)){
            $gps_detail = $this->gpsm->get_gps_by_id($id_gps);
            if(isset($gps_detail) && !empty($gps_detail)){
                $return = array(
                        'version'   => config_item('api_version'),
                        'status'    => STATUS_SUCCESS,
                        'data'      => $gps_detail
                );
                return $this->response($return,REST_CODE_PARAM_ERR);
            } else {
                $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
                );
                return $this->response($return,REST_CODE_PARAM_ERR);
            }
        } else {
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function index_gps_get(){
        $data_return = array();

        # color activity
        $color = 'black';

        # id city
        $id_city_array = array();
        $id_city = trim($this->get('id_city'));
        if(isset($id_city) AND !empty($id_city)){
            $id_city_array = convert_param_array($id_city);
        }

        # id machine
        $id_machine_array = array();
        $id_machine = trim($this->get('machine'));
        if(isset($id_machine) AND !empty($id_machine)){
            $id_machine_array = convert_param_array($id_machine);
        }

        # id worker
        $id_worker_array = array();
        $id_worker = trim($this->get('worker'));
        if(isset($id_worker) AND !empty($id_worker)){
            $id_worker_array = convert_param_array($id_worker);
        }

        # street name
        $street_name = trim($this->get('street'));

        # day
        $day = $this->get('day');

        # limit
        $limit = (intval($this->get('limit')) === 0)?100:intval($this->get('limit'));
        $offset = $this->get('offset');
        if(!isset($offset) || empty($offset)) {
            $offset = 0;
        }

        if( (isset($id_city_array) && !empty($id_city_array)) || (isset($id_machine_array) && !empty($id_machine_array)) || (isset($id_worker_array) && !empty($id_worker_array)) || (isset($day) && !empty($day)) ){
            # Get data activiti
            $activiti_data = array();
            $this->db->where('deleted_at',NULL);
            $activiti_data = $this->db->get(TBL_ACTIVITIES)->result_array();
            # Get data machine
            $machine_data = array();
            $this->db->where('deleted_at',NULL);
            $machine_data = $this->db->get(TBL_MACHINE)->result_array();
            # Get data worker
            $worker_data = array();
            $this->db->where('deleted_at',NULL);
            $worker_data = $this->db->get(TBL_WORKER)->result_array();
            #return $this->response($activiti_data);
            if( (!is_array($activiti_data) OR empty($activiti_data)) OR (!is_array($machine_data) OR empty($machine_data)) OR (!is_array($worker_data) OR empty($worker_data)) ){
                $return = array(
                        'version'   => config_item('api_version'),
                        'status'    => STATUS_FAIL
                );
                return $this->response($return,REST_CODE_PARAM_ERR);
            }
            # record gps
            $gps_result = $this->gpsm->record_gps_get_where($id_city_array, $id_machine_array, $id_worker_array, $street_name, $day, $limit, $offset);
            if( !isset($gps_result) OR empty($gps_result) ){
                $return = array(
                        'version'   => config_item('api_version'),
                        'status'    => STATUS_FAIL
                );
                return $this->response($return,REST_CODE_PARAM_ERR);
            }

            # array id gps
            $id_wk_gps_array    = $this->_array_colum($gps_result,'id');
            # get activity by gps
            $activiti_result    = $this->gpsm->activiti_get_where($id_wk_gps_array);
            # get infor by gps
            $infor_result       = $this->gpsm->infor_get_where($id_wk_gps_array);
            foreach ($gps_result as $gps_key => $gps_value) {
                # GPS - id , ontime,position, machin, worker
                $gps_id             = $gps_value['id'];
                $gps_ontime         = $gps_value['ontime'];
                $gps_id_machine     = $gps_value['id_machine'];
                $gps_mc_name        = '';
                foreach ($machine_data as $key => $value) {
                    if( isset($value['id']) AND ($value['id'] == $gps_id_machine) ){
                        $gps_mc_name = isset($value['name']) ? $value['name'] : '';
                        break;
                    }
                }
                $gps_id_wk          = $gps_value['id_worker'];
                $gps_wk_first_name  = '';
                $gps_wk_last_name   = '';
                foreach ($worker_data as $key => $value) {
                    if( isset($value['id']) AND ($value['id'] == $gps_id_wk) ){
                        $gps_wk_first_name  = isset($value['first_name']) ? $value['first_name'] : '';
                        $gps_wk_last_name   = isset($value['last_name']) ? $value['last_name'] : '';
                        break;
                    }
                }
                $gps_position       = '';
                if(isset($gps_value['position_clean']) AND !empty($gps_value['position_clean'])){
                    $gps_position       = $gps_value['position_clean'];
                } else {
                    $gps_position       = $gps_value['position'];
                }

                $gps_position       = trim($gps_position);
                $gps_position       = trim($gps_position, ',');
                $gps_position       = trim(trim($gps_position, 'LINESTRING('));
                $gps_position       = trim(trim($gps_position, ')'));
                $gps_status         = $gps_value['status'];

                # Information - gps_id, lat, lon
                $information_array = array();
                foreach ($infor_result as $infor_key => $infor_value) {
                    $infor_id_gps   = $infor_value['id_gps'];
                    if($infor_id_gps == $gps_id){
                        $infor_lat  = $infor_value['lat'];
                        $infor_lon  = $infor_value['lon'];
                        $infor_type = $infor_value['type'];
                        $infor_data = $infor_value['data'];
                        $infor_time = $infor_value['time'];
                        $infor_message  = 0;
                        $infor_image    = 0;
                        $infor_voice    = 0;
                        $infor_video    = 0;
                        $infor_data     = array();
                        switch ($infor_type) {
                            case INFO_MESSAGE:
                                $infor_message = 1;
                                break;
                            case INFO_IMAGE:
                                $infor_image = 1;
                                break;
                            case INFO_VOICE:
                                $infor_voice = 1;
                                break;
                            case INFO_VIDEO:
                                $infor_video = 1;
                                break;
                            default:
                                break;
                        }
                        if( isset($information_array[$infor_lat.$infor_lon]) AND !empty($information_array[$infor_lat.$infor_lon]) ){

                            $infor_message  = $information_array[$infor_lat.$infor_lon]['message']  + $infor_message;
                            $infor_image    = $information_array[$infor_lat.$infor_lon]['image']    + $infor_image;
                            $infor_voice    = $information_array[$infor_lat.$infor_lon]['voice']    + $infor_voice;
                            $infor_video    = $information_array[$infor_lat.$infor_lon]['video']    + $infor_video;
                            $infor_data     = $information_array[$infor_lat.$infor_lon]['data'];
                        }
                        $infor_data[]   = array(
                            'time'  => $infor_time,
                            'data'  => $infor_data
                        );
                        $information_array[$infor_lat.$infor_lon] = array(
                            'gps_id'    => $infor_id_gps,
                            'lat'       => $infor_lat,
                            'lon'       => $infor_lon,
                            'message'   => $infor_message,
                            'image'     => $infor_image,
                            'voice'     => $infor_voice,
                            'video'     => $infor_video,
                            'data'      => $infor_data
                        );

                    }# end gps_id  infor_id_gps
                }

                # Activiti
                $activiti_end_time = FALSE;
                $activiti_array = array();
                $activiti_duration_total = 0;
                foreach ($activiti_result as $activiti_key => $activiti_value) {
                    # ACTIVITI - start time, end time, position, activiti, street
                    $activiti_id_wkgps  = $activiti_value['id_wkgps'];
                    if($activiti_id_wkgps == $gps_id){
                        $activiti_position  = '';
                        $latitude           = $activiti_value['latitude'];
                        $latitude_explode   = explode(',',$latitude);
                        $longtitude         = $activiti_value['longtitude'];
                        $longtitude_explode = explode(',',$longtitude);
                        if( count($latitude_explode) == count($longtitude_explode) ){
                            for ($i=0; $i < count($latitude_explode); $i++) {
                                if($i < 1)
                                    $activiti_position = $latitude_explode[$i] . ' ' . $longtitude_explode[$i];
                                else
                                    $activiti_position = $activiti_position . ',' . $latitude_explode[$i] . ' ' . $longtitude_explode[$i];
                            }
                        }

                        $activiti_date               = date('Y-m-d',strtotime($activiti_value['starttime']));
                        $activiti_start              = date('H:i:s',strtotime($activiti_value['starttime']));
                        $activiti_end                = date('H:i:s',strtotime($activiti_value['endtime']));
                        # Check start end time
                        if($activiti_end_time != FALSE){
                            if( (strtotime($activiti_start) > strtotime($activiti_end_time)) AND ((strtotime($activiti_start) - strtotime($activiti_end_time)) > 3) AND ((strtotime($activiti_start) - strtotime($activiti_end_time)) < 16) ){
                                $activiti_start = date('H:i:s',strtotime($activiti_end_time) + 3);
                            }
                        }
                        $activiti_end_time = $activiti_end;

                        $activiti_duration           = intval($activiti_value['duration']);
                        $activiti_duration_total     += $activiti_duration;
                        $activiti_duration_total_tmp = sprintf("%02d%s%02d%s%02d",floor($activiti_duration_total/3600), ':', ($activiti_duration_total/60)%60, ':', $activiti_duration_total%60);
                        $activiti_duration           = sprintf("%02d%s%02d%s%02d",floor($activiti_duration/3600), ':', ($activiti_duration/60)%60, ':', $activiti_duration%60);
                        $activiti_street             = isset($activiti_value['street']) ? $activiti_value['street'] : '';
                        $activiti_id                 = $activiti_value['id_activity'];
                        $activiti_name               = '';
                        $activiti_color              = '';
                        foreach ($activiti_data as $key => $value) {
                            if( isset($value['id']) AND ($value['id'] == $activiti_id) ){
                                $activiti_name  = isset($value['name']) ? $value['name'] : '';
                                $activiti_color = isset($value['color']) ? $value['color'] : '#000000';
                                break;
                            }
                        }

                        # Infor mation activiti
                        $activiti_message   = 0;
                        $activiti_image     = 0;
                        $activiti_voice     = 0;
                        $activiti_video     = 0;
                        foreach ($infor_result as $infor_key => $infor_value) {
                            $infor_time = $infor_value['time'];
                            if( (strtotime($infor_time) > strtotime($activiti_value['starttime'])) AND (strtotime($infor_time) <= strtotime($activiti_value['endtime'])) ){
                                $infor_type = $infor_value['type'];
                                switch ($infor_type) {
                                    case INFO_MESSAGE:
                                        $activiti_message++;
                                        break;
                                    case INFO_IMAGE:
                                        $activiti_image++;
                                        break;
                                    case INFO_VOICE:
                                        $activiti_voice++;
                                        break;
                                    case INFO_VIDEO:
                                        $activiti_video++;
                                        break;
                                    default:
                                        break;
                                }
                                unset($infor_result[$infor_key]);
                            }
                        }
                        $activiti_array[] = array(
                            'id_wkgps'       => $gps_id,
                            'latitude'       => isset($latitude_explode[0]) ? $latitude_explode[0] : '',
                            'longtitude'     => isset($longtitude_explode[0]) ? $longtitude_explode[0] : '',
                            'starttime'      => $activiti_value['starttime'],
                            'endtime'        => $activiti_value['endtime'],
                            'date'           => isset($activiti_date) ? $activiti_date : '',
                            'start'          => isset($activiti_start) ? $activiti_start : '',
                            'end'            => isset($activiti_end) ? $activiti_end : '',
                            'duration'       => isset($activiti_duration) ? $activiti_duration : '',
                            'duration_total' => isset($activiti_duration_total_tmp) ? $activiti_duration_total_tmp : '',
                            'street'         => isset($activiti_street) ? $activiti_street : '',
                            'color'          => isset($activiti_color) ? $activiti_color : '',
                            'acname'         => isset($activiti_name) ? $activiti_name : '',
                            'position'       => isset($activiti_position) ? $activiti_position : '',
                            'message'        => $activiti_message,
                            'image'          => $activiti_image,
                            'voice'          => $activiti_voice,
                            'video'          => $activiti_video,
                        );
                        unset($activiti_result[$activiti_key]);
                    }# end if gpg_id = activiti_id_gps
                }
                # position activiti
                $_check = '';
                $count_activiti = 0;
                foreach ($activiti_array as $position_key => $position_value) {
                    $color = (isset($position_value['color']) && !empty($position_value['color'])) ? $position_value['color'] : 'black';
                    $check = $position_value['latitude'].' '.$position_value['longtitude'];
                    if($count_activiti < 1){
                        $gps_position = 'color '.$color.','.$gps_position;
                    } else {
                        if($_check != $check){
                            $_check = $check;
                            $first = $this->gpsm->lastIndexOf($gps_position,$check);
                            $gps_position = substr($gps_position, 0,$first).'color '.$color.','.substr($gps_position,$first);
                        }
                    }
                    $count_activiti++;
                }
                # Add result
                $data_return[] = array(
                    'id'            => $gps_id,
                    'ontime'        => $gps_ontime,
                    'status'        => $gps_status,
                    'mcname'        => $gps_mc_name,
                    'id_worker'     => $gps_id_wk,
                    'first_name'    => $gps_wk_first_name,
                    'last_name'     => $gps_wk_last_name,
                    'position'      => 'LINESTRING('.$gps_position.')',
                    'activity'      => $activiti_array,
                    'information'   => $information_array
                );
            }# end for gps
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS,
                    'data'      => $data_return
            );

            return $this->response($return,REST_CODE_OK);
        } else{# en if where
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    # function index_get()
    function index_gps2_get()
    {
        # worker_gps,worker_activity,worker,city,company
        # id_city
        $idctarray = array();
        $city = trim($this->get('id_city'));
        if(isset($city) && !empty($city)){
            $idctarray = convert_param_array($city);
        }

        # id_machine
        $idmcarray = array();
        $machine = trim($this->get('machine'));
        if(isset($machine) && !empty($machine)){
            $idmcarray = convert_param_array($machine);
        }

        # id_worker
        $idwkarray = array();
        $worker = trim($this->get('worker'));
        if(isset($worker) && !empty($worker)){
            $idwkarray = convert_param_array($worker);
        }

        # street
        $street = trim($this->get('street'));
        $day = $this->get('day');

        $limit = (intval($this->get('limit')) === 0)?100:intval($this->get('limit'));
        $offset = $this->get('offset');
        if(!isset($offset) || empty($offset))
            $offset = 0;

        if((isset($idmcarray) && !empty($idmcarray)) || (isset($idwkarray) && !empty($idwkarray)) || (isset($idctarray) && !empty($idctarray)) || (isset($day) && !empty($day)))
        {
            $data = array();

            //linestring
            $gps = $this->gpsm->get_gps($idmcarray,$idwkarray,$day,$idctarray,$street,$limit,$offset);

            //activity
            $color = 'black';
            foreach ($gps as $itgps)
            {
                $idwgps = $itgps['id'];
                # Position
                $line = $itgps['position'];
                $line = trim($line);
                $line = trim($line, ',');
                $line = trim(trim($line, 'LINESTRING('));
                $line = trim(trim($line, ')'));

                # Get Activity
                $activity = $this->gpsm->get_activity_by_worker($idwgps);

                # Activity - grid
                $ac_array = array();
                if(isset($activity) && !empty($activity)){
                    $total_duration = 0;
                    foreach ($activity as $item)
                    {
                        $message    = 0;
                        $image      = 0;
                        $voice      = 0;
                        $video      = 0;
                        $id_gps     = $item['id_wkgps'];
                        $start_time = $item['starttime'];
                        $end_time   = $item['endtime'];
                        # get infomation
                        $this->db->where('id_worker_gps',$id_gps);
                        $this->db->where('time >=',date('Y-m-d H:i:s', strtotime($start_time)));
                        $this->db->where('time <=',date('Y-m-d H:i:s', strtotime($end_time)));
                        $_infor_activity = $this->db->get(TBL_WORKER_INFORMATION)->result_array();
                        if(isset($_infor_activity) && !empty($_infor_activity)){
                            foreach ($_infor_activity as $in_ac_item){
                                switch ($in_ac_item['type']){
                                    case INFO_MESSAGE:
                                        $message++;
                                        break;
                                    case INFO_IMAGE:
                                        $image++;
                                        break;
                                    case INFO_VOICE:
                                        $voice++;
                                        break;
                                    case INFO_VIDEO:
                                        $video++;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        # Add array activity
                        $item['date']       = date("Y-m-d", strtotime($item['starttime']));
                        $item['starttime']  = $item['starttime'];   //date("H:i:s", strtotime($item['starttime']));
                        $item['endtime']    = $item['endtime']; //date("H:i:s", strtotime($item['endtime']));
                        $item['start']      = date("H:i:s", strtotime($item['starttime']));
                        $item['end']        = date("H:i:s", strtotime($item['endtime']));
                        $_duration = (strtotime($item['endtime']) - strtotime($item['starttime']));
                        $total_duration += $_duration;
                        if($_duration > 0)
                        {
                            $item['duration']       = sprintf("%02d%s%02d%s%02d",floor($_duration/3600), ':', ($_duration/60)%60, ':', $_duration%60);
                            $item['duration_total'] = sprintf("%02d%s%02d%s%02d",floor($total_duration/3600), ':', ($total_duration/60)%60, ':', $total_duration%60);
                            $item['street']         = (isset($item['street']) && !empty($item['street'])) ? $item['street'] : '';
                            $item['message']        = $message;
                            $item['image']          = $image;
                            $item['voice']          = $voice;
                            $item['video']          = $video;
                            # position by activiti
                            $position_activiti = '';
                            $this->db->where('id_wkgps',$id_gps);
                            $this->db->where('starttime >=',date('Y-m-d H:i:s', strtotime($start_time)));
                            $this->db->where('endtime <=',date('Y-m-d H:i:s', strtotime($end_time)));
                            $position_activiti_result = $this->db->get(TBL_WORKER_ACTIVITY)->result_array();
                            if(isset($position_activiti_result) && !empty($position_activiti_result)){
                                foreach ($position_activiti_result as $key => $value) {
                                    if(isset($position_activiti) && !empty($position_activiti)){
                                        $position_activiti .= ','.$value['latitude'].' '.$value['longtitude'].' '.$value['starttime'];
                                    } else {
                                        $position_activiti = $value['latitude'].' '.$value['longtitude'].' '.$value['starttime'];
                                    }
                                }
                            }
                            $item['position'] = $position_activiti;
                            $ac_array[] = $item;
                        }
                    }
                }

                # activiti position
                $_check = '';
                $activity_position = $this->gpsm->get_activity_by_worker($idwgps);
                if(isset($activity_position) && !empty($activity_position)){
                    $count_activiti = 0;
                    foreach ($activity_position as $item_position){
                        if((isset($item_position['latitude']) && !empty($item_position['latitude'])) && (isset($item_position['longtitude']) && !empty($item_position['longtitude']))){
                            $color = (isset($item_position['color']) && !empty($item_position['color'])) ? $item_position['color'] : 'black';
                            $check = $item_position['latitude'].' '.$item_position['longtitude'];
                            if($count_activiti < 1){
                                $line = 'color '.$color.','.$line;
                            } else {
                                if($_check != $check){
                                    $_check = $check;
                                    $first = $this->gpsm->lastIndexOf($line,$check);
                                    $line = substr($line, 0,$first).'color '.$color.','.substr($line,$first);
                                }
                            }
                        }
                        $count_activiti ++;
                    }
                }

                # Get Information
                $infor = $this->gpsm->information_get($idwgps);
                $lat = NULL;
                $lon = NULL;
                $infor_group = array();
                $data_infor = array();
                foreach ($infor as $in_item){
                    $_lat = $in_item['lat'];
                    $_lon = $in_item['lat'];
                    if(($_lat != $lat) || ($_lon != $lon)){
                        $lat = $_lat;
                        $lon = $_lon;
                    }
                    $data_infor[$lat.'_infor_'.$lon][] = $in_item;
                }
                if(isset($data_infor) && !empty($data_infor))
                {
                    foreach ($data_infor as $root_item){
                        $count        = 0;
                        $item_id      = 0;
                        $item_lat     = NULL;
                        $item_lon     = NULL;
                        $item_wk_name = '';
                        $item_mc_name = '';
                        $infor_item   = array();
                        $item_msg     = 0;
                        $item_img     = 0;
                        $item_voice   = 0;
                        $item_video   = 0;
                        foreach ($root_item as $child_item){
                            switch ($child_item['type']){
                                case INFO_MESSAGE:
                                    $item_msg++;
                                    break;
                                case INFO_IMAGE:
                                    $item_img++;
                                    break;
                                case INFO_VOICE:
                                    $item_voice++;
                                    break;
                                case INFO_VIDEO:
                                    $item_video++;
                                    break;
                                default:
                                    break;
                            }
                            if($count < 1){
                                $item_id       = $child_item['id'];
                                $item_lat      = $child_item['lat'];
                                $item_lon      = $child_item['lon'];
                                $item_wk_name  = $child_item['first_name'].' '.$child_item['last_name'];
                                $item_mc_name  = $child_item['mc_name'];
                                $item_gps_id   = $child_item['id_gps'];
                                $item_wk_ac_id = $child_item['id_wk_ac'];
                            }
                            $infor_item[] = array(
                                    'time' => $child_item['time'],
                                    'data' => $child_item['data']
                            );
                        }
                        $infor_group[] = array(
                            'id'      => $item_id,
                            'wk_name' => $item_wk_name,
                            'mc_name' => $item_mc_name,
                            'lat'     => $item_lat,
                            'lon'     => $item_lon,
                            'gps_id'  => $item_gps_id,
                            'ac_id'   => $item_wk_ac_id,
                            'message' => $item_msg,
                            'image'   => $item_img,
                            'voice'   => $item_voice,
                            'video'   => $item_video,
                            'data'    => $infor_item
                        );
                    }
                }
                # Data
                $itgps['position'] = 'LINESTRING('.$line.')';
                $itgps['activity'] = $ac_array;
                $itgps['information'] = $infor_group;

                $data[] = $itgps;
            }
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS,
                    'data'      => $data
            );

            return $this->response($return,REST_CODE_OK);
        }
        else
        {
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function activity_get() {
        $idwkgps = $this->get('idwkgps');
        if(isset($idwkgps) && !empty($idwkgps))
            $data = $this->gpsm->activity_get($idwkgps);
        else
            $data = $this->gpsm->activity_get(0);

        return $this->response($data);
    }

    function information_gps_get() {
        $id_gps      = intval($this->get('id_gps'));
        $id_activity = intval($this->get('id_activity'));
        $this->db->where('id_activity', $id_activity);
        $this->db->where('id_wkgps', $id_gps);
        $data = $this->db->get(TBL_WORKER_ACTIVITY)->result();
        $return = array(
            'version' => config_item('api_version'),
            'status'  => STATUS_SUCCESS,
            'data'    => $data
        );
        return $this->response($return, REST_CODE_OK);
    }

    function _array_colum($data = array(),$field = ''){
        $return = array();
        if(isset($field) && !empty($field)){
            if(is_array($data) AND !empty($data)){
                if(isset($data[0][$field]) && !empty($data[0][$field])){
                    foreach ($data as $key => $value) {
                        $return[] = $value[$field];
                    }
                }
            }
        }
        return $return;
    }

    function _array_where($data = array(), $where = array()){
        $return = array();
        if(is_array($data) AND !empty($data)){
            if(is_array($where) AND !empty($where)){
                foreach ($data as $key => $value) {

                }
            } else {
                $return = $data;
            }
        }
        return $return;
    }
}