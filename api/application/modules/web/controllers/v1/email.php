<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

/**
 * Authentication controller
 *
 * @author Chien Tran <tran.duc.chien@kloon.vn>
 * @created 1 Nov 2013
 */
class Email extends REST_Controller{
    function __construct(){
        parent:: __construct();

        # Config
        $this->load->config('email_template');

        # Model
        $this->load->model('email_model','email');

    }

    function send_post() {
        /*$from_email = $this->post('from_email');
        if (is_bool($from_email)) {
            $return = array(
                'version'  => config_item('api_version'),
                'status' => STATUS_FAIL,
                'msg'    => 'from_email is required'
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        } else {
            $from_email = trim($from_email);
        }*/

        $to_email = $this->post('to_email');
        if (is_bool($to_email)) {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'to_email is required'
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        } else {
            if (filter_var($to_email, FILTER_VALIDATE_EMAIL) == FALSE) {
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'to_email is invalid'
                );
                return $this->response($return, REST_CODE_PARAM_ERR);
            } else {
                $to_email = trim($to_email);
            }
        }

        $subject = $this->post('subject');
        $message = $this->post('message');

        # Template
        $template = $this->post('template');
        if (!is_bool($template)) {
            $template = config_item($template);
            if ($template) {
                $subject = isset($template['subject']) ? $template['subject'] : '';
                $message = isset($template['message']) ? $template['message'] : '';
            }
        }

        # Template pattern
        $template_pattern = $this->post('template_pattern');
        if (!is_bool($template_pattern)) {
            $template_pattern = json_decode(trim($template_pattern), TRUE);
        }

        # Template replace
        $template_replace = $this->post('template_replace');
        if (!is_bool($template_replace)) {
            $template_replace = json_decode(trim($template_replace), TRUE);
        }

        if (is_array($template_pattern) AND is_array($template_replace) AND !empty($template_replace) AND !empty($template_pattern) ) {
            $subject = str_replace($template_pattern, $template_replace, $subject);
            $message = str_replace($template_pattern, $template_replace, $message);
        }

        # Insert To Database
        $email_account = config_item('email_account');
        $email_account = $email_account['noreply'];
        $data = array(
            'from_email' => $email_account['email'],
            'from_name'  => $email_account['display_name'],
            'to_email'   => $to_email,
            'subject'    => $subject,
            'message'    => $message,
            'status'     => EMAIL_STATUS_PENDING,
            'created_at' => date('Y-m-d H:i:s')
        );
        $db_flag = $this->db->insert(TBL_EMAIL, $data);
        if ($db_flag) {
            // mail($to_email, $subject, $message);
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
            );
            return $this->response($return, REST_CODE_OK);
        } else {
            write_log('mail/send Insert to DB_Query'.$this->db->last_query(), 'db');
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Has an error when update database'
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }
}
?>