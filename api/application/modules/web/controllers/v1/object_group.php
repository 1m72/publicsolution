<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require APPPATH . '/controllers/REST_Controller.php';
/*
 * @author Le Thi Nhung(le.nhung@kloon.vn)
 * @create 2013-11-26
 * */
class Object_group extends REST_Controller {

    protected $id_ref = 0;

    public function __construct() {
        parent::__construct();
        $this->load->helper('pheanstalk');
    }

    public function index_get() {
        $tenantId = $this->_tenantId;
        if (isset($tenantId) && !empty($tenantId)) {
            $id  = $this->get('id');
            $ids = array();
            $id && ($ids = convert_param_array($id));
            $ids && ($this->db->where_in('id', $ids));

            $data = $this->db->where('deleted_at', null)->get(TBL_OBJECT_PLACE)->result_array();
            return $this->response(array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
                'data'    => $data
            ), REST_CODE_OK);
        } else {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }
    }

    public function index_post() {
        $id_city   = get_tenant_id();
        $id_parent = intval($this->post('id_parent'));
        $name      = trim($this->post('name'));
        $level     = intval($this->post('level'));

        $data = array(
            'id_ref' => $this->id_ref,
            'id_city' => $id_city,
            'id_parent' => $id_parent,
            'name' => $name,
            'level' => $level,
            'created_at' => date('Y-m-d H:i:s')
        );

        if ($this->db->insert(TBL_OBJECT_PLACE, $data)) {
            // Push notification
            push_job(QUE_SERVER_UPDATE, json_encode(array(
                'id_city' => $id_city,
                'app' => APP_FASTTASK_TEXT,
                'msg_key' => 'server_update',
                'msg_body' => 'object_group'
            )));

            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_SUCCESS
            ), REST_CODE_OK);
        } else {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }
    }

    function index_put() {
        $id        = intval($this->put('id'));
        $id_city   = get_tenant_id();
        $id_parent = $this->put('id_parent');
        $name      = $this->put('name');
        $level     = $this->put('level');

        $data = array(
            'updated_at' => date('Y-m-d H:i:s'),
            'id_ref' => $this->id_ref,
            'id_city' => $id_city
        );

        $id_parent && ($data['id_parent'] = intval($id_parent));
        $name && ($data['name'] = trim($name));
        $level && ($data['level'] = intval($level));

        $this->db->where('id', $id);
        if ($this->db->update(TBL_OBJECT_PLACE, $data)) {
            push_job(QUE_SERVER_UPDATE, json_encode(array(
                'id_city' => $id_city,
                'app' => APP_FASTTASK_TEXT,
                'msg_key' => 'server_update',
                'msg_body' => 'object_group'
            )));
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_SUCCESS
            ), REST_CODE_OK);
        } else {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }
    }

    public function index_delete() {
        $id = intval($this->delete('id'));
        $flag_update = $this->delete_recursive($id);
        if($flag_update){
            // Push notification
            push_job(QUE_SERVER_UPDATE, json_encode(array(
                'id_city'  => get_tenant_id(),
                'app'      => APP_FASTTASK_TEXT,
                'msg_key'  => 'server_update',
                'msg_body' => 'object_group'
            )));

            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_SUCCESS
            ), REST_CODE_OK);

        }else{
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }
    }
    function delete_recursive($id_object_place){

        $update_flag = $this->db->where('id',$id_object_place)
                            ->update(TBL_OBJECT_PLACE,array('deleted_at'=>date('Y-m-d H:i:s')));

        if($update_flag){
            $data     = $this->db->where('id_parent', $id_object_place)
                                 ->where('deleted_at', null)
                                 ->get(TBL_OBJECT_PLACE)->result_array();
            if(count($data)>0){
                foreach ($data as $key => $value) {
                    $id_child = $value['id'];
                    $this->delete_recursive($id_child);
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
