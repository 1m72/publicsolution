<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
class Module extends REST_Controller{
    private $id_module  = 1;
    private $_id_city   = 1;
    function __construct(){
        parent:: __construct();

        if (isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->_id_city = intval($this->_tenantId);
        } else {
            $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function index_get(){
        $id = $this->get('id');
        if ($id !== false) {
            $id = trim($id);
            $id = explode(',', $id);
        }

        $this->db->where('deleted_at', NULL);
        $this->db->where('id_parent', 0);
        if(is_array($id) AND !empty($id)){
            $this->db->where_in('id', $id);
        }

        $data_module = $this->db->get(TBL_MODULES)->result_array();
        if(is_array($data_module) AND !empty($data_module)){
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_SUCCESS,
                'data'      => $data_module
            );
            return  $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Data empty !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function index_post(){
        $this->load->helper('pheanstalk');
        $job = array(
            "id_city"       => $this->_id_city,
            "controllers"   => "module",
            "type"          => "post"
        );
        push_job(QUE_SERVER_UPDATE, json_encode($job));


        $id_parent  = $this->post('id_parent');
        $title      = $this->post('title');
        $email      = $this->post('email');
        $data_insert = array(
            'id_parent'     => $id_parent,
            'title'         => $title,
            'email'         => $email,
            'created_at'    => date('Y-m-d H:i:s')
            );
        $flag = $this->db->insert(TBL_MODULES, $data_insert);
        if($flag != FALSE){
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS
            );
            return  $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function index_put(){
        $this->load->helper('pheanstalk');
        $job = array(
            "id_city"       => $this->_id_city,
            "controllers"   => "module",
            "type"          => "put"
        );
        push_job(QUE_SERVER_UPDATE, json_encode($job));


        $id         = $this->put('id');
        $id_parent  = $this->put('id_parent');
        $title      = $this->put('title');
        $email      = $this->put('email');

        if($id_parent){
            $data_update['id_parent'] = $id_parent;
        }
        if($title){
            $data_update['title'] = $title;
        }
        if($email){
            $data_update['email'] = $email;
        }
        $data_update['updated_at']  = date('Y-m-d H:i:s');

        $this->db->where('id', $id);
        $flag = $this->db->update(TBL_MODULES, $data_update);
        if($flag != FALSE){
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS
            );
            return  $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function index_delete(){
        $this->load->helper('pheanstalk');
        $job = array(
            "id_city"       => $this->_id_city,
            "controllers"   => "module",
            "type"          => "delete"
        );
        push_job(QUE_SERVER_UPDATE, json_encode($job));


        $id     = $this->delete('id');
        $this->db->where('id' , $id);
        $flag = $this->db->update(TBL_MODULES, array('deleted_at' => date('Y-m-d H:i:s')));
        if($flag != FALSE){
            # delete worker_module
            $this->db->where('id_module',$id);
            $this->db->update(TBL_WORKER_MODULES,array('deleted_at' => date('Y-m-d H:i:s')));
            # delete machine_module
            $this->db->where('id_module',$id);
            $this->db->update(TBL_MACHINE_MODULE,array('deleted_at' => date('Y-m-d H:i:s')));
            # delete machine_module_activity
            $machine_module_id      = array();
            $machine_module_result  = array();
            $this->db->where('id_module'    , $id);
            $machine_module_result = $this->db->get(TBL_MACHINE_MODULE)->result_array();
            if(is_array($machine_module_result) AND !empty($machine_module_result)){
                foreach ($machine_module_result as $key => $value) {
                    $machine_module_id[] = isset($value['id']) ? $value['id'] : FALSE;
                }
            }
            $this->db->where_in('id_machine_module',$machine_module_id);
            $this->db->update(TBL_MACHINE_MODULE_ACTIVITI,array('deleted_at' => date('Y-m-d H:i:s')));
            # delete addon_module
            $this->db->where('id_module',$id);
            $this->db->update(TBL_ADDON_MODULE,array('deleted_at' => date('Y-m-d H:i:s')));
            # delete addon_module_activity
            $addon_module_id        = array();
            $addon_module_result    = array();
            $this->db->where('id_module'    , $id);
            $addon_module_result = $this->db->get(TBL_ADDON_MODULE)->result_array();
            if(is_array($addon_module_result) AND !empty($addon_module_result)){
                foreach ($addon_module_result as $key => $value) {
                    $addon_module_id[] = isset($value['id']) ? $value['id'] : FALSE;
                }
            }
            $this->db->where_in('id_addon_module',$addon_module_id);
            $this->db->update(TBL_ADDON_MODULE_ACTIVITI,array('deleted_at' => date('Y-m-d H:i:s')));

            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS
            );
            return  $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }
}
