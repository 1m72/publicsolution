<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

/**
 * Kendo UI Test controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 12 Nov 2013
 */
# Get
define('GET_CITY'               , 0);
define('GET_COMPANY'            , 1);
define('GET_DEVICE'             , 2);
define('GET_MACHINE'            , 3);
define('GET_OBJECT'             , 4);
define('GET_USERS'              , 5);
define('GET_WORKER'             , 6);
define('GET_ADDON'              , 7);
define('GET_ACTIVITY'           , 8);
define('GET_NFC'                , 9);
define('GET_USERS_GROUP'        , 10);
define('GET_MODULE'             , 11);

define('GET_WORKER_GPS'         , 12);
define('API_GET_EMAIL'          , 13);
define('GET_GROUP'              , 14);

define('GET_MATERIAL'          , 15);
define('GET_FREETEXT'          , 16);
define('GET_COST_CENTER'       , 17);
define('GET_EMPLOYEE_GROUP'    , 18);
//VIEW_MATERIAL

class Kendoui extends REST_Controller {
    function __construct() {
        parent::__construct();
    }

    public function index_get() {
        $tenantId =  $this->_tenantId;
        if(isset($tenantId) && !empty($tenantId)){
            $this->_process();
        }else{
            echo null;
        }
    }

    public function index_post() {
        $tenantId =  $this->_tenantId;
        if(isset($tenantId) && !empty($tenantId)){
            $this->_process();
        }else{
            echo null;
        }
    }

    public function index_put() {
        $tenantId =  $this->_tenantId;
        if(isset($tenantId) && !empty($tenantId)){
            $this->_process();
        }else{
            echo null;
        }
    }

    public function _process() {
        $key     = $this->post('key');
        $request = json_decode(json_encode($this->post()));

        $data = $this->executeData($key, $request);

        echo json_encode($data);
    }

    function executeData($key = NULL, $request = NULL){;
        $total = 0;
        $data = array();

        if (in_array($key, array(GET_USERS, GET_CITY, GET_USERS_GROUP, API_GET_EMAIL))) {
            $db_connect = $this->_db_global;
        } else {
            $db_connect = $this->db;
        }

        $limit      = 50;
        $offset     = 0;
        $module     = false;
        $module_not = false;

        if(isset($request) AND !empty($request) AND !is_null($request)){
            $request    = json_decode(json_encode($request), true);

            $page       = isset($request['page']) ? $request['page'] : 1;
            $pageSize   = isset($request['pageSize']) ? $request['pageSize'] : 10;

            $limit      = isset($request['take']) ? $request['take'] : $pageSize;
            $offset     = isset($request['skip']) ? $request['skip'] : ($page - 1) * $pageSize;

            $filter     = isset($request['filter']) ? $request['filter'] : array();

            $module     = isset($request['module']) ? $request['module'] : false;
            $module_not = isset($request['module_not']) ? $request['module_not'] : false;
        }

        $where = array();
        $not_where = array();
        $where_in_id = array();
        # execute
        switch ($key) {
            case GET_CITY:
                $db_connect->start_cache();
                # select
                $db_connect->from(VIEW_CITY);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;

            case GET_COMPANY:
                $db_connect->start_cache();
                # select
                $db_connect->from(VIEW_COMPANY);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;

            case GET_DEVICE:
                $db_connect->start_cache();
                # select
                $db_connect->from(VIEW_DEVICES);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;

            case GET_MACHINE:
                $db_connect->where('deleted_at', NULL);
                if($module){
                    $db_connect->where('id_module', $module);
                }
                $machine_module = $db_connect->get(TBL_MACHINE_MODULE)->result_array();
                if(isset($machine_module) AND !empty($machine_module)){
                    foreach ($machine_module as $key => $value) {
                        $where_in_id[] = $value['id_machine'];
                    }
                }

                $db_connect->start_cache();

                $db_connect->from(TBL_MACHINE);
                $db_connect->where('deleted_at', NULL);

                if($module){
                    if(!empty($where_in_id)){
                        $db_connect->where_in('id', $where_in_id);
                    } else {
                        $db_connect->where('id', 0);
                    }
                }

                $db_connect->stop_cache();

                $total = $db_connect->count_all_results();
                $data  = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;

            case GET_OBJECT:
                $db_connect->start_cache();
                # select
                $db_connect->from(VIEW_OBJECT);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;

            case GET_USERS:
                $db_connect->start_cache();
                # select
                $db_connect->from(VIEW_USERS);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;

            case GET_ADDON:
                $db_connect->where('deleted_at', NULL);
                if($module){
                    $db_connect->where('id_module', $module);
                }

                $addon_module = $db_connect->get(TBL_ADDON_MODULE)->result_array();

                if(isset($addon_module) AND !empty($addon_module)){
                    foreach ($addon_module as $key => $value) {
                        $where_in_id[] = $value['id_addon'];
                    }
                }

                $db_connect->start_cache();

                $db_connect->from(TBL_ADDON);
                $db_connect->where('deleted_at', NULL);

                if($module){
                    if(!empty($where_in_id)){
                        $db_connect->where_in('id', $where_in_id);
                    } else {
                        $db_connect->where('id', 0);
                    }
                }

                $db_connect->stop_cache();

                $total = $db_connect->count_all_results();
                $data  = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;

            case GET_MATERIAL:
                $db_connect->start_cache();
                # select
                $db_connect->from(VIEW_MATERIAL);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();

                # Get count
                $total = $db_connect->count_all_results();

                # Get data limit
                $data = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;

            case GET_ACTIVITY:
                $db_connect->start_cache();
                # select
                $db_connect->from(VIEW_ACTIVITY);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;

            case GET_WORKER:
                $db_connect->where('deleted_at', NULL);
                if($module){
                    $db_connect->where('id_module', $module);
                }

                $worker_module = $db_connect->get(TBL_WORKER_MODULES)->result_array();

                if(isset($worker_module) AND !empty($worker_module)){
                    foreach ($worker_module as $key => $value) {
                        $where_in_id[] = $value['id_worker'];
                    }
                }

                $db_connect->start_cache();

                $db_connect->from(TBL_WORKER);
                $db_connect->where('deleted_at', NULL);

                if($module){
                    if(!empty($where_in_id)){
                        $db_connect->where_in('id', $where_in_id);
                    } else {
                        $db_connect->where('id', 0);
                    }
                }

                $db_connect->stop_cache();

                $total = $db_connect->count_all_results();
                $data  = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();

                $company = array();
                $this->db->where('deleted_at', NULL);
                $result_company = $this->db->get(TBL_COMPANY)->result_array();
                if($result_company AND !empty($result_company)){
                    foreach ($result_company as $key => $value) {
                        $company[$value['id']] = $value['name'];
                    }
                }

                foreach ($data as $key => $value) {
                    $data[$key]['company_name'] = isset($company[$value['id_company']]) ? $company[$value['id_company']] : '';
                }
                break;

            case GET_NFC:
                $db_connect->start_cache();
                # select
                $db_connect->from(VIEW_NFC);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;

            case GET_USERS_GROUP :
                $db_connect->start_cache();
                # select
                $db_connect->from(VIEW_USERS_GROUP);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;

            case GET_MODULE:
                $db_connect->start_cache();
                # select
                $db_connect->from(VIEW_MODULE);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;

            case GET_WORKER_GPS:
                $db_connect->start_cache();
                # select
                $db_connect->from(VIEW_WORKER_GPS);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;

            case API_GET_EMAIL:
                $db_connect->start_cache();
                # select
                $db_connect->from(TBL_EMAIL);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;

            case GET_GROUP:
                $db_connect->start_cache();
                # select
                $db_connect->from(VIEW_GROUP);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;
            default:
                $data = array();
                break;
            case GET_FREETEXT:
                $db_connect->start_cache();
                # select
                $db_connect->from(TBL_FREETEXT);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();

                $db_connect->flush_cache();
                break;
            case GET_COST_CENTER:
                $db_connect->start_cache();
                # select
                $db_connect->from(TBL_COST_CENTER);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();
                $db_connect->flush_cache();
                break;
            case GET_EMPLOYEE_GROUP:
                $db_connect->start_cache();
                # select
                $db_connect->from(TBL_EMPLOYEE_GROUP);
                $db_connect->where('deleted_at', NULL);

                $db_connect->stop_cache();
                # Get count
                $total = $db_connect->count_all_results();
                # Get data limit
                $data       = $db_connect->limit($limit, $offset)->get()->result_array();
                $db_connect->flush_cache();
                break;
            default:
                $data = array();
                break;
        }

        if(isset($data) AND !empty($data)){
            foreach ($data as $key => $value) {
                if(isset($data[$key]['created_at']) AND !empty($data[$key]['created_at'])){
                    $data[$key]['created_at'] = date('Y-m-d', strtotime($data[$key]['created_at']));
                }

                if(isset($data[$key]['updated_at']) AND !empty($data[$key]['updated_at'])){
                    $data[$key]['updated_at'] = date('Y-m-d', strtotime($data[$key]['updated_at']));
                }

                if(isset($data[$key]['deleted_at']) AND !empty($data[$key]['deleted_at'])){
                    $data[$key]['deleted_at'] = date('Y-m-d', strtotime($data[$key]['deleted_at']));
                }
            }
        }
        # return
        $return = array(
            'total'   => $total,
            'data'    => $data,
        );
        return $return;
    }
}