<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author  huongpm<phung.manh.huong@kloon.vn>
 * @create  2013-12-17
 * @modified 04 Mar 2014 : Chien Tran <tran.duc.chien@kloon.vn>
 **/
class Machine_add_on extends REST_Controller{
    private $_id_city = 0;
    function __construct(){
        parent:: __construct();
        if (isset($this->_tenantId) && !empty($this->_tenantId))
            $this->_id_city = intval($this->_tenantId);
        else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'tenantId not found !'
            );
            return  $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    function index_get(){
        $id_array_addon = array();
        $id_add_on = $this->get('id');
        if (isset($id_add_on) && !empty($id_add_on)){
            $id_array_addon = convert_param_array($id_add_on);
        }
        $this->db->where('deleted_at',null);
        if (isset($id_array_addon) && !empty($id_array_addon)) {
            $this->db->where_in('id',$id_array_addon);
        }
        $addon = $this->db->get(TBL_MACHINE_ADD_ON)->result_array();
        if (isset($addon) && !empty($addon))
        {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
                'data'    => $addon
            );
            return $this->response($return, REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    function index_post(){
        $name       = $this->post('name');
        $identifier = $this->post('identifier');
        $data       = array(
            'name'       => $name,
            'identifier' => $identifier,
            'created_at' => date('Y-m-d H:i:s')
        );
        if ($this->db->insert(TBL_MACHINE_ADD_ON,$data)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS
            );
            return $this->response($return, REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    function index_put(){
        $id         = $this->put('id');
        $name       = $this->put('name');
        $identifier = $this->put('identifier');
        $update     = array(
            'name'       => $name,
            'identifier' => $identifier,
            'updated_at' => date('Y-m-d H:i:s')
        );
        $this->db->where('id',$id);
        if ($this->db->update(TBL_MACHINE_ADD_ON,$update)){
            $this->load->helper('pheanstalk');
            $job = array(
                "id_city"       => $this->_id_city,
                "controllers"   => "machine_addon",
                "type"          => "delete"
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_SUCCESS
            );
            return $this->response($return, REST_CODE_OK);
        } else {
            $return = array(
                'version'  => config_item('api_version'),
                'status'   => STATUS_FAIL
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    function index_delete(){
        $id = $this->delete('id');
        $update = array('deleted_at'=>date('Y-m-d H:i:s'));
        $this->db->where('id',$id);
        if ($this->db->update(TBL_MACHINE_ADD_ON,$update)){
            $this->load->helper('pheanstalk');
            $job = array(
                "id_city"       => $this->_id_city,
                "controllers"   => "machine_addon",
                "type"          => "delete"
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS
            );
            return $this->response($return, REST_CODE_OK);
        } else {
            $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_FAIL
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }
}