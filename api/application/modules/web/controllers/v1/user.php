<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author : Le Thi Nhung(le.nhung@kloon.vn)
 * @creat  :  2013-11-05
 */
class User extends REST_Controller
{
    function __construct()
    {
        parent::__construct();

        if(isset($this->_tenantId) && !empty($this->_tenantId))
            $this->load->model('user_model','user');
        else{
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL,
                    'msg'       => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        $this->db = $this->_db_global;
    }
    /*
    * @description: get user by id
    * @function   : index_get (route key in route config)
    * @author     : Le Thi Nhung (le.nhung@kloon.vn)
    * @create     : 2013-11-05
    * @update     : 2013-11-28
    *
    * @route      : web/user/index
    * @param      :
    */
    function index_get()
    {   # id user
        $iduserarray = array();
        $iduser = $this->get('id');
        $iduserarray = convert_param_array($iduser);

        # id city
        $idctarray = array();
        $idcity = $this->get('id_city');
        $idctarray = convert_param_array($idcity);

        # user_by_city
        $user_by_cityarray = array();
        $user_by_city = $this->get('user_by_city');
        $user_by_cityarray = convert_param_array($user_by_city);

        #id company
        $idcomarray = array();
        $idcompany = $this->get('id_company');
        $idcomarray = convert_param_array($idcompany);

        #id user group
        $idgrouparray = array();
        $idusergroup = $this->get('id_user_group');
        $idgrouparray = convert_param_array($idusergroup);
        #
        $limit = (intval($this->get('limit')) === 0)?100:intval($this->get('limit'));
        $offset = $this->get('offset');
        if(!isset($offset) || empty($offset))
            $offset = 0;
        $emailarray = array();
        $user = $this->user->user_get($iduserarray,$emailarray,$user_by_cityarray,$idcomarray,$idusergroup,$limit,$offset);
        if(isset($user) && !empty($user))
        {
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS,
                    'data'      => $user
            );
            return $this->response($return,REST_CODE_OK);
        }
        else
        {
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /*
    * @description: error for user
    * @function   : user_error (route key in route config)
    * @author     : Le Thi Nhung (le.nhung@kloon.vn)
    * @create     : 2013-11-07
    *
    * @route      : web/user/user_error
    * @param      :
    */
    function user_error($code,$message){
        if ($this->form_validation->run() == FALSE){
            $return = array(
                'version'  => config_item('api_version'),
                'status'   => STATUS_FAIL,
                'error'    => $code,
                'message'  => $message,
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    /*
    * @description: create new user
    * @function   : index_post (route key in route config)
    * @author     : Le Thi Nhung (le.nhung@kloon.vn)
    * @create     : 2013-11-05
    *
    * @route      : web/user/index
    * @param      :
    */
    function index_post()
    {
        $this->load->model('auth_model','auth');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->helper('string');

        $salt       = random_string('alnum', 8);
        $username   = $this->post('username');
        $username   = strtolower($username);
        $password   = $this->post('password');
        $passconf   = $this->post('passconf');
        $email      = $this->post('email');
        $id_city    = $this->post('id_city');
        $id_company = $this->post('id_company');
        $id_user_group = $this->post('id_user_group');

        //-------------------------------id_city required---------------------------
        $this->form_validation->set_rules('id_city', 'City', 'trim|required|integer');
        $this->user_error(400,"The City Id field is required.");
        //-------------------------------id_company required---------------------------
        $this->form_validation->set_rules('id_company', 'Company', 'trim|required|integer');

        $this->user_error(401,"The Company Id field is required.");
        //-------------------------------id_user_group required---------------------------
        $this->form_validation->set_rules('id_user_group', 'User group', 'trim|required|integer');
        $this->user_error(402,"The User group Id field is required.");
        //-------------------------------username required---------------------------
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->user_error(100,"The Username field is required.");

        //----------------------------username min_length------------------------------------
        $this->form_validation->set_rules('username', 'Username', 'min_length[5]');
        $this->user_error(101,"The Username field must be at least 5 characters in length.");

        //-------------------------username is_unique--------------------------------
        $this->form_validation->set_rules('username', 'Username', 'is_unique['.TBL_USERS.'.username]');
        $this->user_error(102,"The Username field must contain a unique value.");

        //--------------------------email required---------------------------------
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->user_error(200,"The Email field is required.");

        //-----------------------------email valid_email-----------------------------
        $this->form_validation->set_rules('email', 'Email', 'valid_email');
        $this->user_error(201,"The Email field must contain a valid email address.");

        //-----------------------------email is_unique-------------------------------
        $this->form_validation->set_rules('email', 'Email', 'is_unique['.TBL_USERS.'.email]');
        $this->user_error(202,"The Email field must contain a unique value.");

        //-------------------------password required ------------------------------
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->user_error(300,"The Password field is required");
        $password   = $this->auth->hash_password($password,$salt);
        //----------------------------passconf required---------------------------
        $this->form_validation->set_rules('passconf', 'Passconf', 'trim|required');
        $this->user_error(301,"The Passconf field is required");
        $passconf   = $this->auth->hash_password($passconf,$salt);
        //----------------------------passconf matches---------------------------
        $this->form_validation->set_rules('password', 'Password', 'matches[passconf]');
        $this->user_error(302,"The Password field does not match the Password Confirm field.");

        //------------------------------
        $data = array(
            "id_city"       => $id_city,
            "id_company"    => $id_company,
            "id_user_group" => $id_user_group,
            "username"      => $username,
            "password"      => $password,
            "email"         => $email,
            "salt"          => $salt,
            "created_at"    => date('Y-m-d H:i:s'),
        );

        $this->user->create_user_post($data);
        $return = array(
            'version'  => config_item('api_version'),
            'status'   => STATUS_SUCCESS
        );

        return  $this->response($return);
    }

/*
    * @description: update user
    * @function   : index_put (route key in route config)
    * @author     : Le Thi Nhung (le.nhung@kloon.vn)
    * @create     : 2013-11-07
    *
    * @route      : web/user/index
    * @param      :
    */
    function index_put()
    {
        $this->load->model('auth_model','auth');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->helper('string');

        $id          = intval($this->put('id'));
        $oldemail    = trim($this->put('oldemail'));

        $username   = $this->put('username');
        $username   = strtolower($username);
        $password   = $this->put('password');
        $passconf   = $this->put('passconf');
        $salt       = $this->put('salt');

        #in case change email notification
        $notification = $this->put('notification');
        $extra_email  = $this->put('extra_email');
        $timezone     = $this->put('timezone');

        if(empty($salt)){
            $salt       = random_string('alnum', 8);
        }

        $email                  = trim($this->put('email'));
        $id_city                = $this->put('id_city');
        $id_company             = $this->put('id_company');
        $id_user_group          = $this->put('id_user_group');

        $_POST['id_city']       = $id_city;
        $_POST['id_company']    = $id_company;
        $_POST['id_user_group'] = $id_user_group;
        $_POST['password']      = $password;
        $_POST['username']      = $username;
        $_POST['password']      = $password;
        $_POST['passconf']      = $passconf;
        $_POST['salt']          = $salt;
        $_POST['email']         = $email;

        # update email to city
        if(isset($extra_email) AND !empty($extra_email)){
            $this->_db_global->where('id', $this->_tenantId);
            $flag = $this->_db_global->update(TBL_CITY, array('email_notification' => $extra_email, 'updated_at' => date('Y-m-d H:i:s')));
            write_log('Update email OK id : ' . $id_city . ' Query : ' . $this->_db_global->last_query(), 'update_email_city');
            if($flag == FALSE){
                write_log('Update email fail id : ' . $id_city . ' Query : ' . $this->_db_global->last_query(), 'update_email_city');
            }
        }

        $data = array(
            "updated_at"    => date('Y-m-d H:i:s'),
        );

        if ($notification !== FALSE && $extra_email !== FALSE) {
            $data+= array(
                'notification' => $notification,
                'extra_email'  => $extra_email,
                'timezone'     => $timezone
            );
        }
        if(!empty($id_city)){
            $this->form_validation->set_rules('id_city', 'City', 'trim|integer');
            $this->user_error(400,"The City Id field is required.");
            $data+=array("id_city" => $id_city,);
        }
        if(!empty($id_company)){
            $this->form_validation->set_rules('id_company', 'Company', 'trim|integer');
            $this->user_error(401,"The Company Id field is required.");
            $data+=array("id_company" => $id_company,);
        }
        if(!empty($id_user_group)){
            $this->form_validation->set_rules('id_user_group', 'User Group', 'trim|integer');
            $this->user_error(402,"The User group Id field is required.");
            $data+=array("id_user_group" => $id_user_group,);
        }
        if(!empty($password)||!empty($passconf)){

            $this->form_validation->set_rules('salt', 'Salt', 'trim|required');
            $this->user_error(303,"The Salt field is required.");

            //-------------------------password required ------------------------------
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->user_error(300,"The Password field is required");
            $password   = $this->auth->hash_password($password,$salt);

            //----------------------------passconf required---------------------------
            $this->form_validation->set_rules('passconf', 'Passconf', 'trim|required');
            $this->user_error(301,"The Passconf field is required");
            $passconf   = $this->auth->hash_password($passconf,$salt);

            //----------------------------passconf matches---------------------------
            $this->form_validation->set_rules('password', 'Password', 'matches[passconf]');
            $this->user_error(302,"The Password field does not match the Password Confirm field.");

            //------------------------------
            $datapassword = array(
                "password"      => $password,
                "salt"          => $salt,
            );
            $data +=$datapassword;
        }

        $checkid    = $this->user->count_by(array('id' => $id));
        $checkemail = $this->user->count_by(array("email"=>$oldemail));
        //update user know id
        if($checkid>0)
        {
            $userbyid = $this->user->user_get(array("0"=>$id));
            if(!empty($username) && $username!=$userbyid['0']['username']){
                //-------------------------------username required---------------------------
                $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
                $this->user_error(100,"The Username field is required.");

                //----------------------------username min_length------------------------------------
                $this->form_validation->set_rules('username', 'Username', 'min_length[5]');
                $this->user_error(101,"The Username field must be at least 5 characters in length.");

                //-------------------------username is_unique--------------------------------
                $this->form_validation->set_rules('username', 'Username', 'is_unique['.TBL_USERS.'.username]');
                $this->user_error(102,"The Username field must contain a unique value.");

                $data+= array("username"=> $username,);

            }

            if(!empty($email) && $email!=$userbyid['0']['email']){
                //--------------------------email required---------------------------------
                $this->form_validation->set_rules('email', 'Email', 'required');
                $this->user_error(200,"The Email field is required.");

                //-----------------------------email valid_email-----------------------------
                $this->form_validation->set_rules('email', 'Email', 'valid_email');
                $this->user_error(201,"The Email field must contain a valid email address.");

                //-----------------------------email is_unique-------------------------------
                $this->form_validation->set_rules('email', 'Email', 'is_unique['.TBL_USERS.'.email]');
                $this->user_error(202,"The Email field must contain a unique value.");
                $data+= array("email"=> $email,);
            }
            if(count($data)>1){
                $this->user->update(TBL_USERS,array("id"=>$id),$data);
                $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_SUCCESS
                );
            }
            else{
                $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_FAIL
                );
            }
        }
        else if($checkemail>0){
            $userbyemail = $this->user->user_get(array(),array("0"=>$oldemail));

            if(!empty($username) && $username!=$userbyemail['0']['username']){
                //-------------------------------username required---------------------------
                $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
                $this->user_error(100,"The Username field is required.");

                //----------------------------username min_length------------------------------------
                $this->form_validation->set_rules('username', 'Username', 'min_length[5]');
                $this->user_error(101,"The Username field must be at least 5 characters in length.");

                //-------------------------username is_unique--------------------------------
                $this->form_validation->set_rules('username', 'Username', 'is_unique['.TBL_USERS.'.username]');
                $this->user_error(102,"The Username field must contain a unique value.");

                $data+= array("username"      => $username,);

            }

            if(!empty($email) && $email!=$userbyemail['0']['email']){
                //--------------------------email required---------------------------------
                $this->form_validation->set_rules('email', 'Email', 'required');
                $this->user_error(200,"The Email field is required.");

                //-----------------------------email valid_email-----------------------------
                $this->form_validation->set_rules('email', 'Email', 'valid_email');
                $this->user_error(201,"The Email field must contain a valid email address.");

                //-----------------------------email is_unique-------------------------------
                $this->form_validation->set_rules('email', 'Email', 'is_unique['.TBL_USERS.'.email]');
                $this->user_error(202,"The Email field must contain a unique value.");
                $data+= array("email"      => $email,);
            }
            if(count($data)>1){
                $this->user->update(TBL_USERS,array("email"=>$oldemail),$data);
                $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_SUCCESS
                );
            }
            else{
                $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_FAIL
                );
            }
        }
        else{
            $return = array(
                'version'  => config_item('api_version'),
                'status'   => STATUS_FAIL
            );
        }
        return  $this->response($return);
    }

    /*
    * @description: lock user by id
    * @function   : index_delete (route key in route config)
    * @author     : Le Thi Nhung (le.nhung@kloon.vn)
    * @create     : 2013-11-07
    *
    * @route      : web/user/index
    * @param      :
    */

    function index_delete(){

        $id = intval($this->delete('id'));
        if($this->user->update(TBL_USERS,array('id'=>$id),array('deleted_at'    => date('Y-m-d H:i:s'))))
        {
            $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_SUCCESS
            );
            return $this->response($return,REST_CODE_OK);
        }
        else
        {
            $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }
    /*
    * @description: check unique of field in table(ex:usernam,email)
    * @function   : unique_check_get (route key in route config)
    * @author     : Le Thi Nhung (le.nhung@kloon.vn)
    * @create     : 2014-01-15
    *
    * @route      : web/user/unique_check
    * @param      :
    */

    function unique_check_get(){
        $fieldname      = $this->get('fieldname');
        $check          = $this->get('check');
        $result = true;
        $count = $this->user->count_by(array($fieldname=>$check));
        if($count>=1){
            $result = false;
        }
        return $this->response(array("result"=>$result));

    }
    function check_password_get(){
        $password   = $this->get('password');
        $salt       = $this->get('salt');
        $this->load->model('auth_model','auth');
        $password   = $this->auth->hash_password($password,$salt);
        return $this->response(array("password"=>$password));
    }
}
?>