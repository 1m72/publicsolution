<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
class Street extends REST_Controller{
	private $_id_city = 0;

	function __construct(){
		parent:: __construct();

		if(isset($this->_tenantId) && !empty($this->_tenantId))
			$this->_id_city = intval($this->_tenantId);
		else{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function index_get()
	{
		$this->db->select('street');
		$this->db->where('deleted_at',NULL);
		$this->db->order_by('street','asc');
		$this->db->group_by('street');
		$result_street = $this->db->get(TBL_WORKER_ACTIVITY)->result_array();
		if(isset($result_street) && !empty($result_street)){
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_SUCCESS,
					'data'		=> $result_street,
			);
			return $this->response($return,REST_CODE_OK);
		}else{
			$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}
}
?>