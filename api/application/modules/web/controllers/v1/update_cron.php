<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

class Update_cron extends REST_Controller {
	private $_id_city = -1;
	private $_path_service = 'http://services.gisgraphy.com/street/streetsearch?format=json';

	function __construct(){
		parent:: __construct();

		if(isset($this->_tenantId) && !empty($this->_tenantId)){
			$this->_id_city = intval($this->_tenantId);
		}else{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	/*
	 *
	 * */
	function image_rotate_post(){
		$return = TRUE;
		$this->_db_global->where('status !=','success');
		$this->_db_global->where('type','sync');
		$this->_db_global->limit(10,0);
		$result = $this->_db_global->get(TBL_CRON)->result_array();
		if(isset($result) && !empty($result)){
			$update_array = array();
			foreach ($result as $row){
				if(isset($row['data']) && !empty($row['data'])){
					$data = json_decode($row['data'],TRUE);
					$file_path 	= $data['full_path'];
					$rotate		= $data['rotate'];
					//var_dump($file_path);die;
					if (file_exists($file_path)) {
						$result_rotate = img_rotate($file_path,$rotate);
						if(!$result_rotate['status']){
							write_log('rotate error : '.json_encode($result_rotate), '_update_image_rotate');
							$update_array[] = array(
									'id'		=> $data['id'],
									'status'	=> 'error',
									'last_run'	=> date('Y-m-d H:i:s')
							);
						}else{
							$update_array[] = array(
									'id'		=> $row['id'],
									'status'	=> 'success',
									'last_run'	=> date('Y-m-d H:i:s')
							);
						}
					} else {
						write_log('not exists file : '.json_encode($file_path), '_update_image_rotate');
					}
				}
			}
			if(isset($update_array) && !empty($update_array)){
				if($this->_db_global->update_batch(TBL_CRON, $update_array, 'id') === FALSE){
					$return = FALSE;
					write_log('update status file error : '.json_encode($update_array), '_update_image_rotate');
				}
			}
		}
		return $this->response(array('status'=> $return));
	}

	/*
	 *
	 * */
	function activiti_infor_post(){
		$this->db->where('id_wk_activity',0);
		$this->db->limit(10,0);
		$data_infor = $this->db->get(TBL_WORKER_INFORMATION)->result_array();
		if(isset($data_infor) && !empty($data_infor)){
			$data_update = array();
			foreach ($data_infor as $infor_item){
				$_id_infor = $infor_item['id'];
				$id_worker_gps = $infor_item['id_worker_gps'];

				$this->db->where('id_wkgps',$id_worker_gps);
				$data_activiti = $this->db->get(TBL_WORKER_ACTIVITY)->result_array();
				if(isset($data_activiti) && !empty($data_activiti)){
					if(count($data_activiti) == 1){
						$_id_ac = $data_activiti[0]['id'];
						$data_update[] = array(
								'id'				=> $_id_infor,
								'id_wk_activity'	=> $_id_ac
						);
					} else{
						for ($i = 0; $i < count($data_activiti); $i ++){
							if($i < (count($data_activiti) - 1))
							{

							} else{
								$_id_ac = $data_activiti[count($data_activiti) - 1]['id'];
								$data_update[] = array(
										'id'				=> $_id_infor,
										'id_wk_activity'	=> $_id_ac
								);
							}
						}
					}
				}
			}
			# Update
			if(isset($data_update) && !empty($data_update)){
				if($this->db->update_batch(TBL_WORKER_INFORMATION, $data_update, 'id')){
					$return = array(
						'version'  	=> config_item('api_version'),
						'status'   	=> STATUS_SUCCESS
					);
					return $this->response($return,REST_CODE_OK);
				} else {
					$return = array(
					'version'  	=> config_item('api_version'),
							'status'   	=> STATUS_FAIL,
							'msg'		=> '2'
					);
					return $this->response($return,REST_CODE_PARAM_ERR);
				}
			} else{
				$return = array(
					'version' 	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> '3'
				);
				return $this->response($return,REST_CODE_PARAM_ERR);
			}
		}
	}

	/*
	 *
	 * */
	function email_post(){
		$this->load->config('email_template');
		$email_template = config_item('email_template_extract_mail');
		# get data
		$this->_db_global->where('to_email',NULL);
		$this->_db_global->or_where('to_email','');
		$this->_db_global->order_by('id');
		$this->_db_global->limit(10,0);
		$data_extract = $this->_db_global->get(TBL_EMAIL)->result_array();
		# ectract email
		if(isset($data_extract) && !empty($data_extract)){
			$array_extract = array();
			foreach ($data_extract as $extract){
				$id_mail = $extract['id'];
				$data_detail = json_decode($extract['data'],TRUE);
				#
				$id_city	= $data_detail['id_city'];
				# get user
				$_user_data = file_get_contents(base_url().'web/user/index?id_city='.$id_city);
				$_user_array = json_decode($_user_data,TRUE);
				if(isset($_user_array['data']) && !empty($_user_array['data']))
					$_user_array = $_user_array['data'];
				else
					$_user_array = array();
				# add mail
				$_to_emmail	= '';
				$_cc		= '';
				$_bcc		= '';
				#
				if(isset($_user_array) && !empty($_user_array)){
					$_count_to = 0;
					foreach ($_user_array as $value){
						$email = '';
						$extra_email = '';
						if(isset($value['email']) && !empty($value['email'])){
							$email = $value['email'];
						}
						if(isset($value['extra_email']) && !empty($value['extra_email'])){
							$extra_email = $value['extra_email'];
						}
						if((isset($email) && !empty($email)) || (isset($extra_email) && !empty($extra_email))){
							if($_count_to < 1){
								$_to_emmail = isset($email) ? ($email.(isset($extra_email) ? ';'.$extra_email : '')) : (isset($extra_email) ? $extra_email : '');
							} else {
								if(isset($_bcc) && !empty($_bcc)){
									$_cc .= ';'.isset($email) ? ($email.(isset($extra_email) ? ';'.$extra_email : '')) : (isset($extra_email) ? $extra_email : '');
								} else {
									$_cc .= isset($email) ? ($email.(isset($extra_email) ? ';'.$extra_email : '')) : (isset($extra_email) ? $extra_email : '');
								}
							}
							$_count_to++;
						}
					}
				}
				$taskid		= $data_detail['taskid'];
				# employee
				$employeeid	= $data_detail['employeeid'];
				# ten nhan vien
				$_employee_name = '';
				$_employee_data = file_get_contents(base_url().'web/worker/index?id_city='.$id_city.'&id_worker='.$employeeid);
				$_employee_array = json_decode($_employee_data,TRUE);
				if(isset($_employee_array['data']) && !empty($_employee_array['data'])){
					$_employee_detail = $_employee_array['data'][0];
					$_employee_name = $_employee_detail['first_name'].' '.$_employee_detail['last_name'];
				}
				# machine
				$machineid	= $data_detail['machineid'];
    			# ten may
				$_machine_name = '';
				$_machine_data = file_get_contents(base_url().'web/machine/index?id_city='.$id_city.'&id_machine='.$machineid);
				$_machine_array = json_decode($_machine_data,TRUE);
				if(isset($_machine_array['data']) && !empty($_machine_array['data'])){
					$_machine_detail = $_machine_array['data'][0];
					$_machine_name = $_machine_detail['machine_code'].' '.$_machine_detail['name'];
				}
				#
				$type		= $data_detail['type'];
				$data		= $data_detail['data'];
				$created_at	= $data_detail['created_at'];
				# data insert
    			$from_email = '';
				$from_name	= '';
				$to_email	= $_to_emmail;
				$to_name	= $_employee_name;
				$subject	= isset($email_template['subject']) ? $email_template['subject'] : '';
				$message	= isset($email_template['message']) ? $email_template['message'] : '';
				$message 	= str_replace('[WORKER]', $_employee_name, $message);
				$message 	= str_replace('[MACHINE]', $_machine_name, $message);
				$message 	= str_replace('[TIME]', $created_at, $message);
				if($type != INFO_MESSAGE)
					$attach		= $data;
				else
					$message .= ' content : '.$data;
				#
				$array_extract[] = array(
					'id'			=> $id_mail,
					'from_email' 	=> '',
					'from_name'		=> '',
					'to_email'		=> $to_email,
					'to_name'		=> $to_name,
					'cc'			=> $_cc,
					'bcc'			=> $_bcc,
					'subject'		=> $subject,
					'message'		=> $message,
					'attach'		=> isset($attach) ? $attach : ''
				);
			}
			# update extract
			if(isset($array_extract) && !empty($array_extract)){
				if($this->_db_global->update_batch(TBL_EMAIL,$array_extract,'id') !== FALSE){
					$return = array(
							'version'	=> config_item('api_version'),
							'status'  	=> STATUS_SUCCESS,
							'msg'		=> ''
					);
					return $this->response($return, REST_CODE_OK);
				} else {
					$return = array(
							'version'	=> config_item('api_version'),
							'status'  	=> STATUS_FAIL,
							'msg'		=> ''
					);
					return $this->response($return, REST_CODE_OK);
				}
			}
		}else{
			$return = array(
				'version'	=> config_item('api_version'),
				'status'  	=> STATUS_FAIL,
				'msg'		=> ''
    		);
			return $this->response($return, REST_CODE_OK);
		}
	}
	/*
	 *
	 * */
	function activity_post(){
		$status = TRUE;
		$msg    = '';
		# get file
		$this->db->where('status','pendding');
		$this->db->order_by('id','asc');
		$this->db->limit(1,0);
		$file_data = $this->db->get(TBL_WORKER_ACTIVITY_SPLIT)->row_array();
		if(!$file_data || empty($file_data)){
			return array(
					'status'	=> FALSE,
					'msg'		=> 'File data invalid'
			);
		}
		$file_path	= isset($file_data['file_path']) ? $file_data['file_path'] : '';
		$id 		= isset($file_data['id']) ? $file_data['id'] : '';
		$update = array();
		# Call sync
		$result = $this->_sync_activity($file_path);
		if($result['status']){
			$update['status']	= 'success';
		} else {
			$update['status']	= 'error';
		}
		$this->db->where('id',$id);
		$this->db->update(TBL_WORKER_ACTIVITY_SPLIT,$update);
		return $this->response($result);
	}
	/*
	 *
	 * */
	function _sync_activity($file_path = FALSE){
		$status = TRUE;
		$msg    = '';

		# Check file path
		if (!$file_path OR (file_exists($file_path) == FALSE) ) {
			return array(
				'status' => FALSE,
				'msg' => 'File not exist '.$file_path
			);
		}
		#var_dump($file_path);die;
		# Helper
		$this->load->helper('file');

		# Read content from file
		$json_text = read_file($file_path);
		if (!$json_text OR empty($json_text)) {
			return array(
				'status' => FALSE,
				'msg' => 'Can not read file '.$file_path
			);
		}

		# Decode data
		$json_data = json_decode($json_text, TRUE);
		if (!is_array($json_data)) {
			return array(
				'status' => FALSE,
				'msg' => 'File data is format invalid'
			);
		}
		#var_dump($json_data);die;
		# Transaction start
		$this->db->trans_start();

		# Params
		$id_task     = isset($json_data['task_id'])     ? $json_data['task_id']     : '';
		$id_employee = isset($json_data['employee_id']) ? $json_data['employee_id'] : '';
		$id_machine  = isset($json_data['machine_id'])  ? $json_data['machine_id']  : '';
		$end_time    = isset($json_data['end_time'])    ? $json_data['end_time']    : '';
        $start_time  = isset($json_data['start_time'])  ? $json_data['start_time']  : '';

        # Prepare gps detail record
        $gps    = $this->db->get_where(TBL_WORKER_GPS, array('id_task' => $id_task))->row_array();
        #var_dump($gps);die;
        $id_gps = NULL;
        if (is_array($gps) AND !empty($gps)) {
			$id_gps = isset($gps['id']) ? $gps['id'] : NULL;
		} else {
			$gps_data = array(
				'id_city'    => $this->_id_city,
				'id_task'    => $id_task,
				'id_object'  => 0,
				'id_worker'  => $id_employee,
				'id_machine' => $id_machine,
				'ontime'     => $start_time,
				'created_at' => date('Y-m-d H:i:s')
			);
			$db_flag = $this->db->insert(TBL_WORKER_GPS, $gps_data);
			if($db_flag !== false){
				$id_gps         = $this->db->insert_id();
				$gps_data['id'] = $id_gps;
				$gps            = $gps_data;
			} else {
				return array(
					'status' => FALSE,
					'msg'    => 'Can tnot insert new gps _ Query: ' .$this->db->last_query(). ' _ Data: '.json_encode($gps_data)
					);
			}
		}

		# Check id_gps
		if (is_null($id_gps) OR (intval($id_gps) <= 0)) {
			return array(
				'status' => FALSE,
				'msg'    => 'id_gps is invalid_Value: '.$id_gps
			);
		}

		$activities = array();
		if(!isset($json_data['activities']) OR empty($json_data['activities'])){
			return array(
				'status' => FALSE,
				'msg'    => 'Activity data is not exist'
           	);
        }

		$activities = $this->_walking_nodes($json_data['activities'], 'activityid', 'activitie');
		if (!is_array($activities) OR empty($activities)) {
			return array(
				'status' => FALSE,
				'msg'    => 'Activity data parse is invalid_Data: '.json_encode($activities)
			);
		}

		# Position string will be append to current in worker_gps.position
		$position_new = '';

        # Walking activities elements
	    foreach ($activities as $key_activity => $activity){
    		$activity_id               = isset($activity['activityid'])     ? trim($activity['activityid'])     : '';
    		$activity_starttime        = isset($activity['starttime'])      ? trim($activity['starttime'])      : '';
    		$activity_endtime          = isset($activity['endtime'])        ? trim($activity['endtime'])        : '';
	        $activity_task_activity_id = isset($activity['taskactivityid']) ? trim($activity['taskactivityid']) : '';

	        # Last data
	        $last_street   = '';
	        $last_position = array();

	        # Check activity exist
	        $this->db->where('id_task', $id_task);
	        $this->db->where('id_task_activity', $activity_task_activity_id);
	        $activity_exist = $this->db->get(TBL_WORKER_ACTIVITY)->row_array();
	        if (is_array($activity_exist) AND !empty($activity_exist)) {
	        	$last_position   = isset($activity_exist['end_lat_lon']) ? json_decode($activity_exist['end_lat_lon'], TRUE) : '';
	            if (!is_array($last_position)) {
	            	$last_position = array();
	        	}
	        	$last_street     = isset($activity_exist['street']) ? $activity_exist['street'] : '';
	        } else {}

	        #--- Array data for batch process
	        # Activities will be update to worker_activitiess
	        $activities_update = array();

	        # Activities will be add news to worker_activitiess
	        $activities_new    = array();

	        # Data location end will be update to worker_gps by id_task_activity
	        $location_ends     = array();

	        # Locations process
	        $locations = array();
	        if(isset($activity['locations']) && !empty($activity['locations'])){
	        	$locations = $activity['locations'];
	        	$locations = $this->_walking_nodes($locations, 'lat', 'location');
	        	if (is_array($locations) AND !empty($locations)) {
	        		$locations = array_merge($last_position, $locations);
	        		$locations = array_values($locations);
	        		$streets   = $this->_check_street($locations, $activity_starttime, $activity_endtime);

	        		# Streets walking
	        		if (is_array($streets) AND !empty($streets)) {
	        			foreach ($streets as $key_street => $street) {
	        				$current_street = isset($street['street']) ? $street['street'] : '';
	        				# If first element on same street with last record then update time, task_activity_id
	        				# Else create new record for worker_activities
	        				if ( ($key_street == 0) AND ($last_street != '') AND ($last_street == $current_street) ) {
	        					$activities_update[] = array(
	        						'id_task_activity' => $activity_task_activity_id,
	        						'endtime'          => isset($street['end']) ? $street['end'] : '',
	        						'updated_at'       => date('Y-m-d H:i:s')
	        					);
	        				} else {
	        					$activities_new[] = array(
	        						'id_city'          => $this->_id_city,
	        						'id_wkgps'         => $id_gps,
	        						'id_task'          => $id_task,
	        						'id_task_activity' => $activity_task_activity_id,
	        						'street'           => isset($street['street']) ? $street['street'] : '',
	        						'latitude'         => isset($street['lat']) ? $street['lat'] : '',
	        						'longtitude'       => isset($street['lon']) ? $street['lon'] : '',
	        						'id_activity'      => $activity_id,
	        						'starttime'        => isset($street['start']) ? $street['start'] : '',
	        						'endtime'          => isset($street['end']) ? $street['end'] : '',
	        						'created_at'       => date('Y-m-d H:i:s')
	        					);
	        				}
	        			}
	        		}

	        		# Position walking - Update end positionto field worker_gps.end_lat_lon for next checking
	        		$locations       = array_values($locations);
	        		$location_end    = end($locations);
	        		if (is_array($location_end) AND !empty($location_end)) {
	        			$location_end    = array(
	        				'id_task_activity' => $activity_task_activity_id,
	        				'end_lat_lon'      => json_encode($location_end),
	        				'updated_at'       => date('Y-m-d H:i:s')
	        			);
	        			$location_ends[] = $location_end;
	        		}

	        		# Position new
	        		foreach ($locations as $key_location => $value_location) {
	        			$tmp_lat  = isset($value_location['lat'])  ? $value_location['lat']  : '';
	        			$tmp_lon  = isset($value_location['lon'])  ? $value_location['lon']  : '';
	        			$tmp_time = isset($value_location['time']) ? $value_location['time'] : '';

	        			if ( !empty($tmp_lat) AND !empty($tmp_lon) AND !empty($tmp_time) ) {
	        				$position_new .= trim("{$tmp_lat} {$tmp_lon} {$tmp_time},");
	        			}
	        		}
	        	}
	        }
	        #--- Batch process
	        # Insert into worker_activities
	        if ( is_array($activities_new) AND !empty($activities_new) ) {
	        	$db_flag = $this->db->insert_batch(TBL_WORKER_ACTIVITY, $activities_new);
	        	if ($db_flag === FALSE) {
	        		write_log('Insert worker activities fail_'.$id_task.'_Query: '.$this->db->last_query().'_Data: '.json_encode($activities_new), 'sync_activities');
	        	}
	        }

	        # Update end_time to worker_activities
	        if ( is_array($activities_update) AND !empty($activities_update) ) {
	        	$db_flag = $this->db->update_batch(TBL_WORKER_ACTIVITY, $activities_update);
	        	if ($db_flag === FALSE) {
	        		write_log('Update worker activities fail_'.$id_task.'_Query: '.$this->db->last_query().'_Data: '.json_encode($activities_update), 'sync_activities');
	        	}
	        }

	        # Update end_lat_long to worker_gps
	        if ( is_array($location_ends) AND !empty($location_ends) ) {
	        	$db_flag = $this->db->update_batch(TBL_WORKER_ACTIVITY, $location_ends, 'id_task_activity');
	        	if ($db_flag === FALSE) {
	        		write_log('update end lat lon activity_'.$id_task.'_'.json_encode($location_ends).'_Query: '.$this->db->last_query(), 'sync_activities');
	        	}
	        }
	    }

	    # Append position_new to worker_gps.position
	    $position_new = trim($position_new);
	    if (!empty($position_new)) {
	        $this->db->set('position', "CONCAT(position, '{$position_new}')", FALSE);
            $this->db->set('updated_at', date('Y-m-d H:i:s'));
	        $this->db->where('id', $id_gps);
	        $db_flag = $this->db->update(TBL_WORKER_GPS);
	        if(!$db_flag){
	        	write_log('Update position activity fail_'.$id_task.'_Data: '.json_encode($position_new), 'sync_activities');
	        	return array(
	        		'status' => FALSE,
	        		'msg'    => 'Has an error when saving position !'
	        	);
	        }
        }

        # Transaction end
        $this->db->trans_complete();

	    return array(
	        'status' => TRUE,
	        'msg'    => 'The data has been successfully process'
		);
	}
	/*
	 *
	 * */
	function _walking_nodes($data = array(), $key_checker = '', $key_value = '') {
		#print_r($data);die;
		$return = array();
		# Has one
		if (isset($data[$key_value]) && isset($data[$key_value][$key_checker]) || isset($data[$key_checker])) {
			$return[] = $data[$key_value];
		} else {
			# Has many
			if (!empty($key_value) && isset($data[$key_value]) && !empty($data)) {
				$return = $data[$key_value];
			} else {
				if(isset($data) && !empty($data)){
					foreach ($data as $key => $value) {
						$return[] = $value;
					}
				}
			}
		}
		return $return;
	}
	/*
	 *
	 * */
	function _extract_street($_lat,$_lon){
		//$this->db->where('street',NULL);
		//$this->db->order_by('id');
		//$this->db->limit(1,0);
		//$data_extract = $this->db->get(TBL_WORKER_ACTIVITY)->result_array();
		$return = '';
		if((isset($_lat) && !empty($_lat)) && (isset($_lon) && !empty($_lon))){
			$_street_data = '';
			try{
				$_street_data = file_get_contents($this->_path_service.'&lat='.$_lat.'&lng='.$_lon.'&from=1&to=1');
			}
			catch(Exception $e){}
			$_street_array = json_decode($_street_data);
			if(isset($_street_array->result) && !empty($_street_array->result)){
				$_street_detail = $_street_array->result[0];
				$_street_name = isset($_street_detail->name) ? $_street_detail->name : '';
				$return = $_street_name;
			} else{
				write_log("error get street name NULL : ".json_encode(array('lat'=>$_lat,'lon'=>$_lon)), 'symc_get_street');
			}
		} else{
			write_log("error location NULL : ".json_encode(array('lat'=>$_lat,'lon'=>$_lon)), 'symc_get_street');
		}
		return $return;
	}
	/*
	 * */
	function _check_street($location = NULL,$start_time = '', $end_time = ''){
		$return = array();
		$time = NULL;
		$street = NULL;
		$lat = NULL;
		$lon = NULL;
		$start = $start_time;
		$end = $end_time;
		$count = 0;
		if(isset($location) && !empty($location)){
			#var_dump($location);die;
			foreach ($location as $item){
				$_lat = isset($item['lat']) ? trim($item['lat']) : '';
				$_lon = isset($item['lon']) ? trim($item['lon']) : '';
				$_time = isset($item['time']) ? trim($item['time']) : '';
				# begin lat
				if(!isset($lat) && empty($lat)) {
					$lat = $_lat;
				}
				# begin lon
				if(!isset($lon) && empty($lon)){
				$lon = $_lon;
				}

				if(!isset($start) && empty($start)){
				$start = $_time;
				}
					#
					if(isset($lat) && !empty($lat) && isset($lon) && !empty($lon)){
					$_street = $this->_extract_street($_lat, $_lon);
					}
					if(isset($_street) && !empty($_street))
					{
					if(($street != $_street)){
					$return[$count] = array(
					'street' 	=> $_street,
					'lat'		=> $lat,
					'lon'		=> $lon,
					'start'		=> $start,
					'end'		=> ''
							);
							if($count > 0){
							$return[$count - 1]['end'] = $_time;
					}
						$lat = $_lat;
					$lon = $_lon;
						# start street
						$start = $_time;
						$count++;
					}
					} else{
					write_log("Error Street Name NULL : ".json_encode(array('lat'=>$_lat,'lon'=>$_lon,'time'=>$time)), 'symc_get_street');
				}
				$street = $_street;
					}
					# Add endtime last
							if(isset($return) && !empty($return)){
							$return[$count - 1]['end'] = $end;
				}
				}else{
				write_log("Error Location NULL : ".json_encode(array('lat'=>$_lat,'lon'=>$_lon,'time'=>$time)), 'symc_get_street');
		}
						return $return;
	}
	/*
	 *
	 * */
}
?>