<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

/**
 * Kendo UI Test controller
 *
 * @author  : chientran <tran.duc.chien@kloon.vn>
 * @created : 12 Nov 2013
 */
# Get
define('GET_CITY'               , 0);
define('GET_COMPANY'            , 1);
define('GET_DEVICE'             , 2);
define('GET_MACHINE'            , 3);
define('GET_OBJECT'             , 4);
define('GET_USERS'              , 5);
define('GET_WORKER'             , 6);
define('GET_MACHINE_ADDON'      , 7);
define('GET_ADDON'              , 7);
define('GET_ACTIVITY'           , 8);
define('GET_NFC'                , 9);
define('GET_USERS_GROUP'        , 10);
define('GET_MODULE'             , 11);
define('GET_WORKER_GPS'         , 12);


class Kendoui extends REST_Controller {
    function __construct() {
        parent::__construct();
    }

    public function index_get() {
        $tenantId =  $this->_tenantId;
        if(isset($tenantId) && !empty($tenantId)){
            $this->_process();
        }else{
            echo null;
        }
    }

    public function index_post() {
        $tenantId =  $this->_tenantId;
        if(isset($tenantId) && !empty($tenantId)){
            $this->_process();
        }else{
            echo null;
        }
    }

    public function index_put() {
        $tenantId =  $this->_tenantId;
        if(isset($tenantId) && !empty($tenantId)){
            $this->_process();
        }else{
            echo null;
        }
    }

    public function _process() {
        require_once APPPATH . 'libraries/kendoui/DataSourceResult.php';

        # Convert request array to object
        $key     = $this->post('key');
        $request = json_decode(json_encode($this->post()));

        # Connection
        if (in_array($key, array(GET_USERS, GET_CITY,GET_USERS_GROUP))) {
            $result = $this->_init_DataSourceResult($this->_db_global);
        } else {
            $result = $this->_init_DataSourceResult();
        }

        # Results
        switch ($key)
        {
            case GET_CITY:
                $data = $result->read(
                        VIEW_CITY,
                        array(
                                VIEW_CITY.'.id' => array('type' => 'number'),
                                VIEW_CITY.'.name',
                                VIEW_CITY.'.passcode',
                                VIEW_CITY.'.database as city_database',
                                VIEW_CITY.'.storage as city_storage',
                                VIEW_CITY.'.deleted_at',
                                VIEW_CITY.'.created_at',
                                VIEW_CITY.'.updated_at',
                        ),
                        $request
                );
                break;

            case GET_COMPANY:
                $data = $result->read(
                        VIEW_COMPANY,
                        array(
                                VIEW_COMPANY.'.id' => array('type' => 'number'),
                                VIEW_COMPANY.'.id_city' => array('type' => 'number'),
                                VIEW_COMPANY.'.id_user' => array('type' => 'number'),
                                VIEW_COMPANY.'.name',
                                VIEW_COMPANY.'.description',
                                VIEW_COMPANY.'.deleted_at',
                                VIEW_COMPANY.'.created_at',
                                VIEW_COMPANY.'.updated_at',
                        ),
                        $request
                );
                break;

            case GET_DEVICE:
                $data = $result->read(
                        VIEW_DEVICES,
                        array(
                                VIEW_DEVICES.'.id' => array('type' => 'number'),
                                VIEW_DEVICES.'.id_device',
                                VIEW_DEVICES.'.id_city' => array('type' => 'number'),
                                VIEW_DEVICES.'.id_unique',
                                VIEW_DEVICES.'.actived' => array('type' => 'number'),
                                VIEW_DEVICES.'.deleted_at',
                                VIEW_DEVICES.'.created_at',
                                VIEW_DEVICES.'.updated_at',
                        ),
                        $request
                );
                break;

            case GET_MACHINE:
                $properties = array(
                    'id',
                    'id_ref',
                    'id_city',
                    'code',
                    'nfc_code',
                    'identifier',
                    'name',
                    'description',
                    'created_at',
                    'updated_at',
                );
                $data  = $this->_executeData(VIEW_MACHINE_MODULE, $properties, $request);
                break;

            case GET_OBJECT:
                $data = $result->read(
                        VIEW_OBJECT,
                        array(
                            'id'    => array('type' => 'number'),
                            'id_ref',
                            'id_city' => array('type' => 'number'),
                            'id_group' => array('type' => 'number'),
                            'obj_name',
                            'place_name',
                            'position',
                            'deleted_at',
                            'created_at',
                            'updated_at'
                        ),
                        $request
                );
                break;

            /**
             * @author  : Chien Tran <tran.duc.chien@kloon.vn>
             * @created : 19 Nov 2013
             */
            case GET_USERS:
                $data = $result->read(
                    VIEW_USERS,
                    array(
                        'id'            => array('type' => 'number'),
                        'id_city'       => array('type' => 'number'),
                        'id_company'    => array('type' => 'number'),
                        'id_user_group' => array('type' => 'number'), //edit by Le Thi Nhung (2013-11-28)
                        'group_name',
                        'username',
                        'email',
                        'salt',
                        'deleted_at',
                        'updated_at',
                        'created_at'
                    ),
                    $request
                );
                break;

            case GET_ADDON:
                $properties = array(
                    'id',
                    'id_ref',
                    'identifier',
                    'name',
                    'updated_at',
                    'created_at'
                );
                $data  = $this->_executeData(VIEW_ADDON_MODULE, $properties, $request);
                break;

            case GET_ACTIVITY:
                $data = $result->read(
                VIEW_ACTIVITY,
                array(
                    'id'=> array('type' => 'number'),
                    'id_ref',
                    'id_city'=> array('type' => 'number'),
                    'name',
                    'color',
                    'deleted_at',
                    'updated_at',
                    'created_at'
                    ),
                    $request
                );
                    break;

            case GET_WORKER:
                $properties = array(
                    'id',
                    'id_ref',
                    'id_city',
                    'id_company',
                    'id_teamleader',
                    'nfc_code',
                    'personal_code',
                    'first_name',
                    'last_name',
                    'address',
                    'phone',
                    'birthday',
                    'created_at',
                    'updated_at',
                    'company_name',
                    'company_description'
                );
                $data  = $this->_executeData(VIEW_WORKER_MODULE, $properties, $request);
                break;
            case GET_NFC:
                $data = $result->read(
                    VIEW_NFC,
                    array(
                        'id' => array('type' => 'number'),
                        'id_company' => array('type' => 'number'),
                        'code',
                        'nfc_code',
                        'active',
                        'deleted_at',
                        'updated_at',
                        'created_at'
                    ),
                    $request
                );
                break;
            case GET_USERS_GROUP :
                $data = $result->read(
                        VIEW_USERS_GROUP,
                        array(
                                'id' => array('type' => 'number'),
                                'name',
                                'updated_at',
                                'created_at',
                        ),
                        $request
                );
                break;
            case GET_MODULE:
                $data = $result->read(
                        VIEW_MODULE,
                        array(
                                'id' => array('type' => 'number'),
                                'id_parent' => array('type' => 'number'),
                                'title',
                                'updated_at',
                                'created_at',
                        ),
                        $request
                );
                break;
            case GET_WORKER_GPS:
                $data = $result->read(
                        VIEW_WORKER_GPS,
                        array(
                            'id',
                            'id_city',
                            'id_worker',
                            'id_task',
                            'id_object',
                            'id_machine',
                            'app',
                            'position',
                            'position_clean',
                            'ontime',
                            'status',
                            'total_activity',
                            'id_cron',
                            'created_at',
                            'updated_at'
                        ),
                        $request
                );
                break;
            default:
                $data = array();
                break;
        }
        echo json_encode($data);
    }

    function _executeData($table = NULL, $properties = array(), $request = NULL){
        $total = 0;
        $data = array();
        if(isset($table) AND !empty($table)){
            # properties
            $select = implode(',', $properties);
            # request
            $limit  = 50;
            $offset = 0;
            $module = false;
            $module_not = false;
            $where = array();
            $not_where = array();
            if(isset($request) AND !empty($request) AND !is_null($request)){
                $request    = json_decode(json_encode($request), true);
                $page       = isset($request['page']) ? $request['page'] : 1;
                $pageSize   = isset($request['pageSize']) ? $request['pageSize'] : 10;
                $limit      = isset($request['take']) ? $request['take'] : $pageSize;
                $offset     = isset($request['skip']) ? $request['skip'] : ($page - 1) * $pageSize;
                $filter     = isset($request['filter']) ? $request['filter'] : array();
                $module     = isset($request['module']) ? $request['module'] : false;
                $module_not = isset($request['module_not']) ? $request['module_not'] : false;
            }

            if ($module) {
                $where[] = $module;
            }
            if($module_not){
                switch ($table) {
                    case VIEW_MACHINE_MODULE:
                        $machine_module = array();
                        $this->db->where('deleted_at', NULL);
                        $this->db->where('id_module', $module_not);
                        $machine_module = $this->db->get(TBL_MACHINE_MODULE)->result_array();
                        if(is_array($machine_module) AND !empty($machine_module)){
                            foreach ($machine_module as $key => $val) {
                                $not_where[] = $val['id_machine'];
                            }
                        }
                        break;
                    case VIEW_ADDON_MODULE:
                        $addon_module = array();
                        $this->db->where('deleted_at', NULL);
                        $this->db->where('id_module', $module_not);
                        $addon_module = $this->db->get(TBL_ADDON_MODULE)->result_array();
                        if(is_array($addon_module) AND !empty($addon_module)){
                            foreach ($addon_module as $key => $val) {
                                $not_where[] = $val['id_addon'];
                            }
                        }
                        break;
                    case VIEW_WORKER_MODULE:
                        $worker_module = array();
                        $this->db->where('deleted_at', NULL);
                        $this->db->where('id_module', $module_not);
                        $worker_module = $this->db->get(TBL_WORKER_MODULES)->result_array();
                        if(is_array($worker_module) AND !empty($worker_module)){
                            foreach ($worker_module as $key => $val) {
                                $not_where[] = $val['id_worker'];
                            }
                        }
                        break;
                    default:
                        break;
                }
                #$this->db->where('id_module !=', $module_not);
                #$this->db->or_where('id_module', NULL);
            }
            /*if(empty($where) AND empty($not_where)){
                return array();
            }*/
            $this->db->start_cache();
            $this->db->select($select);
            $this->db->from($table);
            if(is_array($where) AND !empty($where)){
                $this->db->where_in('id_module', $where);
            }
            if(is_array($not_where) AND !empty($not_where)){
                $this->db->where_not_in('id' , $not_where);
                $this->db->group_by('id');
            }

            $this->db->stop_cache();

            # total
            $total = $this->db->count_all_results();

            # data
            $data       = $this->db->limit($limit, $offset)->get()->result_array();
            $this->db->flush_cache();
        }

        # return
        $return = array(
            'total'   => $total,
            'data'    => $data,
            // 'request' => $request,
            // 'query'   => $this->db->last_query(),
        );
        return $return;
    }

    function _init_DataSourceResult($db_connection = NULL) {
        if (is_null($db_connection)) {
            $db_connection = $this->db;
        }

        # Database config
        $db_host = $db_connection->hostname;
        $db_host_tmp = explode(':', $db_host);
        $db_host = isset($db_host_tmp[0]) ? $db_host_tmp[0] : 'localhost';
        $db_port = $db_connection->port;
        $db_user = $db_connection->username;
        $db_pass = $db_connection->password;
        $db_name = $db_connection->dbprefix.$db_connection->database;
        $result = new DataSourceResult("mysql:host={$db_host};dbname={$db_name}", $db_user, $db_pass);
        return $result;
    }
}
