<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

class Cost_center extends REST_Controller{
    private $_id_city = 0;

    function __construct(){
        parent:: __construct();

        if(isset($this->_tenantId) && !empty($this->_tenantId))
            $this->_id_city = intval($this->_tenantId);
        else{
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        $this->load->helper('pheanstalk');
    }

    public function index_get(){
        $id        = $this->get('id');
        $ids       = array();
        $id && ($ids = convert_param_array($id)); // id = 0 -> ???
        $ids && ($this->db->where_in('id', $ids));

        $title = $this->get('title');
        $title && $this->db->where('title', $title);

        $assigned_at = !!$this->get('assigned_at');
        $assigned_at && $this->db->where('assigned_at IS NOT NULL');

        $this->db->where('deleted_at', null);
        $cost_center = $this->db->get(TBL_COST_CENTER);

        if (!empty($cost_center)) {
            $cost_center = $cost_center->result_array();
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_SUCCESS,
                'data' => $cost_center
            ), REST_CODE_OK);
        } else {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }
    }

    public function index_post() {
        $title     = $this->post('title');

        if(empty($title)){
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }

        $data = array(
            'title'      => $title,
            'created_at' => date('Y-m-d H:i:s')
        );

        if ($this->db->insert(TBL_COST_CENTER, $data)) {
            // Push notification
            $job = array(
                'id_city'  => get_tenant_id(),
                'app'      => APP_FASTTASK_TEXT,
                'msg_key'  => 'server_update',
                'msg_body' => 'cost_center',
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            return $this->response(array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS
            ), REST_CODE_OK);
        } else {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }
    }

    public function index_put() {
        $id        = intval($this->put('id'));
        $title     = $this->put('title');

        if(!$title || !$id){
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }

        $data = array(
            'updated_at' => date('Y-m-d H:i:s')
        );

        $title && ($data['title'] = $title);

        $db_flag = $this->db->where('id', $id)->update(TBL_COST_CENTER, $data);
        if ($db_flag !== false) {
            // Push notification
            $job = array(
                'id_city'  => get_tenant_id(),
                'app'      => APP_FASTTASK_TEXT,
                'msg_key'  => 'server_update',
                'msg_body' => 'cost_center',
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_SUCCESS
            ), REST_CODE_OK);
        } else {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }
    }

    public function index_delete() {
        $id = intval($this->delete('id'));

        if(empty($id)){
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }

        $db_flag = $this->db->where('id', $id)->update(TBL_COST_CENTER, array('deleted_at' => date('Y-m-d H:i:s') ));
        if ($db_flag !== false) {
            // Delete from employee_group_cost
            $this->db->where('id_cost_center', $id)->delete(TBL_EMPLOYEE_GROUP_COST);

            // Push notification
            $job = array(
                'id_city'  => get_tenant_id(),
                'app'      => APP_FASTTASK_TEXT,
                'msg_key'  => 'server_update',
                'msg_body' => 'cost_center',
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_SUCCESS
            ), REST_CODE_OK);
        } else {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }
    }

    public function assigned_get() {
        $id_employee_group = $this->get('id_employee_group');
        $cost_center = array();
        if ($id_employee_group === false) {
            $this->db->select('c.*');
            $this->db->from(TBL_COST_CENTER . ' as c');
            $this->db->join(TBL_EMPLOYEE_GROUP_COST . ' AS e', 'c.id = e.id_cost_center', 'left');
            $this->db->where('c.deleted_at', null);
            $cost_center = $this->db->get();
        } else {
            $id_employee_group = intval($id_employee_group);
            $sql = "SELECT `c`.*
                    FROM `cost_center` AS `c`
                    LEFT JOIN `employee_group_cost` AS `e` ON `c`.`id` = `e`.`id_cost_center`
                    WHERE (`c`.`deleted_at` IS NULL) AND ((`c`.`assigned_at` IS NULL) OR (`e`.`id_employee_group` = {$id_employee_group}) )";
            $cost_center = $this->db->query($sql);
        }
        $cost_center = $cost_center->result_array();

        if (!empty($cost_center)) {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
                'data'    => $cost_center
            ), REST_CODE_OK);
        } else {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }
    }
}
