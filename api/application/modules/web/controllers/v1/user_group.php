<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author Le Thi Nhung(le.nhung@kloon.vn)
 * @create 2013-11-26
 * */
class User_group extends REST_Controller{
	function __construct(){
		parent:: __construct();
		if(isset($this->_tenantId) && !empty($this->_tenantId)){
			$this->_id_city = intval($this->_tenantId);
			$this->load->model('user_group_model','u_group');
		}else{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}
	function indexall_get()
	{
		// $data = $this->db->get_where(TBL_USER_GROUP,array('deleted_at'=>NULL))->result_array();

		# Added by ChienTran - 11 Dec 2013
		$data = $this->_db_global->get_where(TBL_USER_GROUP,array('deleted_at'=>NULL))->result_array();
		$return = array(
				'version'  	=> config_item('api_version'),
				'status'   	=> STATUS_SUCCESS,
				'data'		=> $data
		);
		return $this->response($return,REST_CODE_OK);
	}
	/*
	* @description: get user group by id
	* @function   : index_get (route key in route config)
	* @author     : Le Thi Nhung (le.nhung@kloon.vn)
	* @create     : 2013-12-31
	* @route      : web/user_group/index
	* @param      :
	*/
	function index_get()
	{
		#id user group
		$idgrouparray = array();
		$idusergroup = $this->get('id_group');
		if(isset($idusergroup) && !empty($idusergroup))
		{
			$idgrouparray = explode(',',$idusergroup);
		}

		$limit = (intval($this->get('limit')) === 0)?100:intval($this->get('limit'));
		$offset = $this->get('offset');
		if(!isset($offset) || empty($offset))
			$offset = 0;

		$user_group = $this->u_group->user_group_get($idgrouparray,$limit,$offset);
		if(isset($user_group) && !empty($user_group))
		{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_SUCCESS,
					'data'		=> $user_group
			);
			return $this->response($return,REST_CODE_OK);
		}
		else
		{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}
	/*
	* @description: create new user group
	* @function   : index_post (route key in route config)
	* @author     : Le Thi Nhung (le.nhung@kloon.vn)
	* @create     : 2013-12-31
	*
	* @route      : web/user_group/index
	* @param      :
	*/
	function index_post()
	{
        $name = $this->input->post('name');

		$data = array(
			"name"          => $name,
			"created_at"    => date('Y-m-d H:i:s'),
		);

		$this->u_group->create_user_group_post($data);
		$return = array(
			'version'  => config_item('api_version'),
			'status'   => STATUS_SUCCESS
		);

		return  $this->response($return);
	}
/*
	* @description: update  user group
	* @function   : index_put (route key in route config)
	* @author     : Le Thi Nhung (le.nhung@kloon.vn)
	* @create     : 2013-12-31
	*
	* @route      : web/user_group/index
	* @param      :
	*/
	function index_put()
	{
		$id         = intval($this->put('id_group'));
		$name       = $this->put('name');
		$data = array(
			"name"          => $name,
			"updated_at"    => date('Y-m-d H:i:s'),
		);
		$this->u_group->update(TBL_USER_GROUP,array("id"=>$id),$data);
		$return = array(
			'version'  => config_item('api_version'),
			'status'   => STATUS_SUCCESS
		);
		return  $this->response($return);
	}

	/*
	* @description: lock user group by id
	* @function   : index_delete (route key in route config)
	* @author     : Le Thi Nhung (le.nhung@kloon.vn)
	* @create     : 2013-12-31
	*
	* @route      : web/user_group/index
	* @param      :
	*/

	function index_delete(){

		$id = intval($this->delete('id'));
		if($this->u_group->update(TBL_USER_GROUP,array('id'=>$id),array('deleted_at' 	=> date('Y-m-d H:i:s'))))
		{
			$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_SUCCESS
			);
			return $this->response($return,REST_CODE_OK);
		}
		else
		{
			$return = array(
					'version'  => config_item('api_version'),
					'status'   => STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}
}
?>