<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-11-21
 * */
class Devices extends REST_Controller{
	function __construct(){
		parent:: __construct();

		if(isset($this->_tenantId) && !empty($this->_tenantId))
			$this->load->model('devices_model','dev');
		else{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function index_get()
	{
		$id_city_array = array();
		$id_city = $this->get('id_city');
		if(isset($id_city) && !empty($id_city)){
			$id_city_array = convert_param_array($id_city);
		}
		$limit = (intval($this->get('limit')) === 0)?100:intval($this->get('limit'));
		$offset = $this->get('offset');
		if(!isset($offset) || empty($offset))
			$offset = 0;
		$data = $this->dev->get($id_city_array,$limit,$offset);
		$return = array(
				'version'  	=> config_item('api_version'),
				'status'   	=> STATUS_SUCCESS,
				'data'		=> $data
		);
		return $this->response($return,REST_CODE_OK);
	}

	function index_post()
	{

	}

	function index_put()
	{

	}

	function index_delete()
	{

	}
}
?>