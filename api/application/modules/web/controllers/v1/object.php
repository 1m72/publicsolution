<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 3013-11-22
 * */
class Object extends REST_Controller{
    function __construct(){
        parent:: __construct();

        if (isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->load->model('object_model','obj');
        } else {
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_FAIL,
                'msg'       => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        $this->load->helper('pheanstalk');
    }

    public function index_get()
    {
        $id_city_array = array();
        $id_city    = $this->get('id_city');
        if (isset($id_city) && !empty($id_city)){
            $id_city_array = convert_param_array($id_city);
        }

        $id_parent_array = array();
        $id_parent  = $this->get('id_parent');
        if (isset($id_parent) && !empty($id_parent)){
            $id_parent_array = convert_param_array($id_parent);
        }

        $id_object_array = array();
        $id_object  = $this->get('id_object');
        if (isset($id_object) && !empty($id_object)){
            $id_object_array = convert_param_array($id_object);
        }

        $id_un_object_array = array();
        $id_un_object = $this->get('id_un_object');
        if (isset($id_un_object) && !empty($id_un_object)){
            $id_un_object_array = convert_param_array($id_un_object);
        }
        $data = $this->obj->get($id_city_array,$id_parent_array,$id_object_array,$id_un_object_array);
        if (isset($data) && !empty($data))
        {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
                'data'    => $data
            );
            return $this->response($return,REST_CODE_OK);
        }
        else
        {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    public function index_post()
    {
        $id_ref     = $this->post('id_ref');
        $id_city    = $this->post('id_city');
        $id_parent  = $this->post('id_parent');
        $id_group   = $this->post('id_group');  #edit by Le Thi Nhung(2013-11-29)
        $position   = $this->post('position');
        $name       = $this->post('name');
        $data       = array(
            'id_ref'        => (isset($id_ref) && !empty($id_ref)) ? $id_ref : '',
            'id_city'       => (isset($id_city) && !empty($id_city)) ? $id_city : '',
            'id_parent'     => (isset($id_parent) && !empty($id_parent)) ? $id_parent : '',
            'id_group'      => (isset($id_group) && !empty($id_group)) ? $id_group : '', #edit by Le Thi Nhung(2013-11-29)
            'position'      => (isset($position) && !empty($position)) ? $position : '',
            'name'          => (isset($name) && !empty($name)) ? $name : '',
            'created_at'    => date('Y-m-d H:i:s')
        );

        $return_code = null;

        if ($this->db->insert(TBL_OBJECT,$data)) {
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_SUCCESS
            );
            $return_code = REST_CODE_OK;
        } else {
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
            );
            $return_code = REST_CODE_PARAM_ERR;
        }

        // Push notification
        $job = array(
            'id_city'  => get_tenant_id(),
            'app'      => APP_FASTTASK_TEXT,
            'msg_key'  => 'server_update',
            'msg_body' => 'object',
        );
        push_job(QUE_SERVER_UPDATE, json_encode($job));

        return $this->response($return, $return_code);
    }

    public function index_put()
    {
        $id         = $this->put('id');
        $id_ref     = $this->put('id_ref');
        $id_city    = $this->put('id_city');
        $id_parent  = $this->put('id_parent');
        $id_group   = $this->put('id_group');#edit by Le Thi Nhung(2013-11-29)
        $position   = $this->put('position');
        $name       = $this->put('name');
        $data       = array(
            'id_ref'     => (isset($id_ref) && !empty($id_ref)) ? $id_ref : '',
            'id_city'    => (isset($id_city) && !empty($id_city)) ? $id_city : '',
            'id_parent'  => (isset($id_parent) && !empty($id_parent)) ? $id_parent : '',
            'id_group'   => (isset($id_group) && !empty($id_group)) ? $id_group : '',#edit by Le Thi Nhung(2013-11-29)
            'position'   => (isset($position) && !empty($position)) ? $position : '',
            'name'       => (isset($name) && !empty($name)) ? $name : '',
            'updated_at' => date('Y-m-d H:i:s')
        );

        $return_code = null;

        $this->db->where('id',$id);
        if ($this->db->update(TBL_OBJECT,$data))
        {
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_SUCCESS
            );
            $return_code = REST_CODE_OK;
        } else {
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
            );
            $return_code = REST_CODE_PARAM_ERR;
        }

        // Push notification
        $job = array(
            'id_city'  => get_tenant_id(),
            'app'      => APP_FASTTASK_TEXT,
            'msg_key'  => 'server_update',
            'msg_body' => 'object',
        );
        push_job(QUE_SERVER_UPDATE, json_encode($job));

        return $this->response($return, $return_code);
    }

    public function index_delete()
    {
        $id = $this->delete('id');
        $this->db->where('id',$id);
        if ($this->db->update(TBL_OBJECT,array('deleted_at'=>date('Y-m-d H:i:s'))))
        {
            // Push notification
            $job = array(
                'id_city'  => get_tenant_id(),
                'app'      => APP_FASTTASK_TEXT,
                'msg_key'  => 'server_update',
                'msg_body' => 'object',
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_SUCCESS
            );
            return $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }
}
