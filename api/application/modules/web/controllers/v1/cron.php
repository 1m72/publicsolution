<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_Controller {
    private $_path_service      = 'http://services.gisgraphy.com/street/streetsearch?format=json';
    private $_path_service2     = 'http://nominatim.openstreetmap.org/reverse?format=json';
    private $_url_path_service1 = 'http://services.gisgraphy.com/street/streetsearch?format=json&from=1&to=1';
    private $_url_path_service2 = 'http://nominatim.openstreetmap.org/reverse?format=json&zoom=18&addressdetails=1';
    private $_url_path_service3 = 'http://open.mapquestapi.com/nominatim/v1/reverse.php?format=json';
    private $_url_path_service  = 'http://nominatim.kloon.net/reverse?format=json&zoom=18&addressdetails=1';


    function __construct() {
        parent::__construct();
    }

    function record(){
        require APPPATH.'/libraries/class.audio_streamer.php';
        $this->db->where('status', CRON_STATUS_PENDING);
        $this->db->where('type', CRON_TYPE_CONVERT_VOICE);
        $this->db->limit(5);
        $result = $this->db->get(TBL_CRON)->result_array();
        if(isset($result) && !empty($result)){
            foreach ($result as $row){
                $file_path  = $row['data'];
                $id         = $row['id'];
                $id_city    = $row['id_city'];
                echo "---> Converting file : {$file_path}\r\n";
                echo "-> ID: {$id}\r\n";
                echo "-> City: {$id_city}\r\n";

                if (!file_exists($file_path)) {
                    $file_path = get_server_path() . $file_path;
                }

                $update_data = array(
                    'last_run' => date('Y-m-d H:i:s')
                );
                if (file_exists($file_path)) {
                    $update_data['status'] = CRON_STATUS_SUCCESS;
                    echo "-> Result: OK\r\n";

                    $sound = new audio_streamer($file_path);
                    $new_file = $sound->save();
                    echo "-> Result converted file: {$new_file}\r\n";

                    # Rename
                    $bak_file = str_replace('.3gp', '_bak.3gp', $file_path);
                    rename($file_path, $bak_file);
                    rename($new_file, $file_path);

                    # Chmod files
                    @chmod($bak_file, 0644);
                    @chmod($file_path, 0644);

                    echo "-> Rename converted file: \r\n";
                    echo "- Backup file: {$bak_file}\r\n";
                    echo "- New file: {$file_path}\r\n";
                } else {
                    $update_data['status'] = CRON_STATUS_ERROR;
                    write_log('File not exists : '.json_encode($file_path), 'update_voice');
                    echo "-> Result: FAIL\r\n";
                }
                $this->db->where('id', $id);
                $db_flag = $this->db->update(TBL_CRON, $update_data);
                echo "-> DB update status : ". ($db_flag ? 'OK' : 'FAIL') . " _ Query: " .$this->db->last_query() ."\r\n";
                echo "=======================================================================\r\n";
            }
        }

        print_r($update_array);
    }

    function rotate() {
        $return = TRUE;
        $this->db->where('status', CRON_STATUS_PENDING);
        $this->db->where('type', CRON_TYPE_ROTATE);
        $this->db->limit(10);
        $result = $this->db->get(TBL_CRON)->result_array();
        if (isset($result) && !empty($result)) {
            $update_array = array();
            foreach ($result as $row) {
                if (isset($row['data']) && !empty($row['data'])) {
                    $data      = json_decode($row['data'], TRUE);
                    $file_path = $data['full_path'];
                    $rotate    = $data['rotate'];

                    if (!file_exists($file_path)) {
                        $file_path = get_server_path() . $file_path;
                    }

                    if (file_exists($file_path)) {
                        $result_rotate = img_rotate($file_path, $rotate);
                        if (!$result_rotate['status']) {
                            $msg = 'rotate error : ' . json_encode($result_rotate);
                            write_log($msg, 'update_image_rotate');
                            echo "$msg\r\n";
                            $update_array[] = array(
                                'id' => $data['id'],
                                'status' => CRON_STATUS_ERROR,
                                'last_run' => date('Y-m-d H:i:s')
                            );
                        } else {
                            echo "---> File rotate {$rotate}: {$file_path}";
                            print_r($data);
                            $update_array[] = array(
                                'id'       => $row['id'],
                                'status'   => CRON_STATUS_SUCCESS,
                                'last_run' => date('Y-m-d H:i:s')
                            );
                        }
                        echo "-------------------------------------------\r\n";
                    } else {
                        $msg = 'File not exists : ' . json_encode($file_path);
                        write_log($msg, 'update_image_rotate');
                        echo $msg;
                    }
                }
            }
            if (isset($update_array) && !empty($update_array)) {
                if ($this->db->update_batch(TBL_CRON, $update_array, 'id') === FALSE) {
                    $return = FALSE;
                    $msg = 'update status file error : ' . json_encode($update_array);
                    write_log($msg, 'update_image_rotate');
                    echo $msg;
                }
            }
        }
        var_dump($return);
    }

    function email_extract()
    {
        $this->load->config('email_template');
        $email_template = config_item('email_template_extract_mail');

        $this->db->where('status', EMAIL_STATUS_WAITING);
        $this->db->limit(10);
        $data_extract = $this->db->get(TBL_EMAIL)->result_array();

        # ectract email
        if (isset($data_extract) && !empty($data_extract)) {
            # Email Account
            $email_account_config = config_item('email_account');
            $email_account        = $email_account_config['noreply'];

            $array_extract        = array();
            foreach ($data_extract as $extract) {
                $id_mail     = $extract['id'];
                $data_detail = json_decode($extract['data'], TRUE);
                $id_city     = $data_detail['id_city'];
                if ($data_detail) {
                    # get user
                    $_user_array = $this->db->get_where(TBL_USERS, array('id_city' => $id_city))->result_array();

                    # add mail
                    $_to_emmail = array();
                    $_cc        = array();
                    $_bcc       = array();

                    if (is_array($_user_array) && !empty($_user_array)) {
                        foreach ($_user_array as $value) {
                            $email       = isset($value['email']) ? $value['email'] : FALSE;
                            if ($email) {
                                $_to_emmail[] = $email;
                            }

                            $extra_email = isset($value['extra_email']) ? $value['extra_email'] : FALSE;
                            if ($extra_email) {
                                $extra_email = explode(';', $extra_email);
                                $_cc         = array_merge($_cc, $extra_email);
                            }
                        }
                    }
                    $_to_emmail = implode(';', array_unique($_to_emmail));
                    $_cc        = implode(';', array_unique($_cc));

                    $taskid = $data_detail['taskid'];

                    # Employee
                    $employeeid      = $data_detail['employeeid'];
                    $_employee_name  = '';
                    $_employee_data  = file_get_contents(base_url() . 'web/worker/index?id_city=' . $id_city . '&id_worker=' . $employeeid);
                    $_employee_array = json_decode($_employee_data, TRUE);
                    if (isset($_employee_array['data']) && !empty($_employee_array['data'])) {
                        $_employee_detail = $_employee_array['data'][0];
                        $_employee_name   = $_employee_detail['first_name'] . ' ' . $_employee_detail['last_name'];
                    }

                    # Machine
                    $machineid      = $data_detail['machineid'];
                    $_machine_name  = '';
                    $_machine_data  = file_get_contents(base_url() . 'web/machine/index?id_city=' . $id_city . '&id_machine=' . $machineid);
                    $_machine_array = json_decode($_machine_data, TRUE);
                    if (isset($_machine_array['data']) && !empty($_machine_array['data'])) {
                        $_machine_detail = $_machine_array['data'][0];
                        $_machine_name   = $_machine_detail['machine_code'] . ' ' . $_machine_detail['name'];
                    }
                    $lat        = isset($data_detail['lat']) ? $data_detail['lat'] : '';
                    $lon        = isset($data_detail['lon']) ? $data_detail['lon'] : '';
                    $type       = $data_detail['type'];
                    $data       = $data_detail['data'];
                    $created_at = $data_detail['created_at'];

                    # data insert
                    $from_email = $email_account['email'];
                    $from_name  = $email_account['display_name'];
                    $url_infor = create_url('sys/viewinfor/index') . '/' . $lat . '/' .$lon;

                    # Email To
                    $to_email   = trim($_to_emmail,';');
                    $to_name    = $_employee_name;

                    # Email message
                    $subject    = isset($email_template['subject']) ? $email_template['subject'] : '';
                    $message    = isset($email_template['message']) ? $email_template['message'] : '';
                    $message    = str_replace('[WORKER]', $_employee_name, $message);
                    $message    = str_replace('[MACHINE]', $_machine_name, $message);
                    $message    = str_replace('[TIME]', $created_at, $message);
                    $message    = str_replace('[DATA]', $url_infor, $message);
                    if ($type != INFO_MESSAGE){
                        $attach = $data;
                    } else {
                        $message .= ' content : ' . $data;
                    }

                    $tmp = array(
                        'id'         => $id_mail,
                        'status'     => EMAIL_STATUS_PENDING,
                        'from_email' => $email_account['email'],
                        'from_name'  => $email_account['display_name'],
                        'subject'    => $subject,
                        'message'    => $message,
                    );

                    if (isset($to_email) AND (!empty($to_email)))  {
                        $tmp['to_email'] = $to_email;
                    } else {
                        write_log('Email extract error_ID: '.$id_mail, 'email');
                        $tmp['status'] = EMAIL_STATUS_ERROR;
                    }

                    if (isset($to_name) AND (!empty($to_name)))  {
                        $tmp['to_name'] = $to_name;
                    }

                    if (isset($_cc) AND (!empty($_cc)))  {
                        $tmp['cc'] = trim($_cc,';');
                    }

                    if (isset($_bcc) AND (!empty($_bcc)))  {
                        $tmp['bcc'] = $_bcc;
                    }

                    if (isset($attach) AND (!empty($attach)))  {
                        $tmp['attach'] = $attach;
                    }

                    $array_extract[] = $tmp;
                } else {
                    $array_extract[] = array(
                        'id'         => $id_mail,
                        'from_email' => $email_account['email'],
                        'from_name'  => $email_account['display_name'],
                        'status'     => EMAIL_STATUS_PENDING
                    );
                }
            }

            # update extract
            if (isset($array_extract) && !empty($array_extract)) {
                $email_flag = $this->db->update_batch(TBL_EMAIL, $array_extract, 'id');
                echo $this->db->last_query();
                if ($email_flag !== FALSE) {
                    $return = array(
                        'version' => config_item('api_version'),
                        'status'  => STATUS_SUCCESS,
                        'msg'     => 'Email has been extracted'
                    );
                    return $return;
                } else {
                    $return = array(
                        'version' => config_item('api_version'),
                        'status'  => STATUS_FAIL,
                        'msg'     => 'Has an error when extract email'
                    );
                    return $return;
                }
            }
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Email extract is empty'
            );
            return $return;
        }
    }

    function email_send() {
        # Email config
        $config['useragent'] = 'BIS-OFFICE System';
        $config['protocol']  = 'sendmail';
        $config['mailpath']  = '/usr/sbin/sendmail';
        $config['charset']   = 'utf-8';
        $config['crlf']      = '\r\r\n';
        $config['newline']   = '\r\r\n';
        $config['wordwrap']  = TRUE;
        $config['mailtype']  = 'html';
        $this->load->library('email', $config);

        # Email information
        $this->db->where('status', EMAIL_STATUS_PENDING);
        $this->db->limit(3);
        $emails = $this->db->get(TBL_EMAIL)->result_array();
        $batch_update = array();
        if (is_array($emails) AND !empty($emails)) {
            foreach ($emails as $key => $value) {
                $noreply_email = config_item('email_account');
                $noreply_email = $noreply_email['noreply'];
                $from_email = isset($value['from_email']) ? $value['from_email'] : $noreply_email['email'];
                $from_name  = isset($value['from_name'])  ? $value['from_name']  : $noreply_email['display_name'];
                $to_email   = isset($value['to_email'])   ? $value['to_email']   : FALSE;
                $to_name    = isset($value['to_name'])    ? $value['to_name']    : $to_email;
                $cc         = (isset($value['cc']) AND (!empty($value['cc'])) ) ? $value['cc'] : FALSE;
                $bcc        = (isset($value['bcc']) AND (!empty($value['bcc'])) ) ? $value['bcc'] : FALSE;
                $subject    = isset($value['subject']) ? $value['subject'] : '';
                $message    = isset($value['message']) ? $value['message'] : '';
                $message    = preg_replace('/\[(.*)+\]/i', '', $message);
                $attach     = (isset($value['attach']) AND (!empty($value['attach'])) ) ? $value['attach'] : FALSE;

                if ($to_email !== FALSE) {
                    $this->email->from($from_email, $from_name);
                    $this->email->to($to_email, $to_name);
                    $this->email->subject($subject);
                    $this->email->message($message);

                    if ($cc) {
                        $this->email->cc($cc);
                    }
                    if ($bcc) {
                        $this->email->bcc($bcc);
                    }
                    if ($attach) {
                        if (!file_exists($attach)) {
                            $attach = get_server_path() . $attach;
                        }
                        $this->email->attach($attach);
                    }

                    # Sendding
                    $email_flag = $this->email->send();
                    if ($email_flag) {
                        $batch_update[] = array(
                            'id'      => $value['id'],
                            'send_at' => date('Y-m-d H:i:s'),
                            'status'  => EMAIL_STATUS_SUCCESS,
                        );
                    } else {
                        $batch_update[] = array(
                            'id'      => $value['id'],
                            'status'  => EMAIL_STATUS_ERROR,
                        );
                        write_log('Email send error_ID: '.$id, 'email');
                    }

                    # Print logs
                    echo $this->email->print_debugger();
                } else {
                    $msg = 'Email address is required_email.id='.$value['id'];
                    write_log($msg, 'email');
                    echo $msg.'\r\n';
                }

                echo '\n----------------------------------------------\r\n';
            }

            if (is_array($batch_update) AND (!empty($batch_update))) {
                $this->db->update_batch(TBL_EMAIL, $batch_update, 'id');
            }
        }
    }

    function information($id_city = 0)
    {
        # connect
        $this->db->where('id_city', $id_city);
        $config = $this->db->get(TBL_CITY_CONFIG)->row_array();
        if (!$config || empty($config)) {
            print_r(array(
                'status' => FALSE,
                'msg' => 'Data connect invalid'
            ));
            die;
        }
        $data_connect   = $config['database'];
        $db_info        = json_decode($data_connect);
        $db_config      = array(
            'hostname' => isset($db_info->hostname) ? $db_info->hostname : config_item('hostname'),
            'username' => isset($db_info->username) ? $db_info->username : config_item('username'),
            'password' => isset($db_info->password) ? $db_info->password : config_item('password'),
            'database' => isset($db_info->database) ? $db_info->database : config_item('database'),
            'port'     => isset($db_info->port) ? $db_info->port : config_item('db_port'),
            'dbprefix' => config_item('db_prefix'),
            'dbdriver' => config_item('db_dbdriver'),
            'pconnect' => config_item('db_pconnect'),
            'db_debug' => config_item('db_db_debug'),
            'cache_on' => config_item('db_cache_on'),
            'cachedir' => config_item('db_cachedir'),
            'char_set' => config_item('db_char_set'),
            'dbcollat' => config_item('db_dbcollat')
        );
        $dbcity         = $this->load->database($db_config, true);
        $dbcity_connect = $dbcity->initialize();
        if (!$dbcity_connect) {
            print_r(array(
                'status' => FALSE,
                'msg' => 'Connect fail'
            ));
            die;
        }

        $dbcity->where('id_wk_activity', 0);
        $dbcity->limit(10);
        $dbcity->order_by('id', 'desc');
        $data_infor = $dbcity->get(TBL_WORKER_INFORMATION)->result_array();
        print_r($data_infor);
        if (isset($data_infor) && !empty($data_infor)) {
            echo '----------------------------------'.PHP_EOL;
            $data_update = array();
            foreach ($data_infor as $infor_item) {
                $_id_infor      = $infor_item['id'];
                $id_worker_gps  = $infor_item['id_worker_gps'];
                $_time          = $infor_item['time'];
                echo "--> ID infor: $_id_infor".PHP_EOL;
                echo "Time: $_time".PHP_EOL;
                echo "id_worker_gps: $id_worker_gps".PHP_EOL;

                $tmp = array();

                $dbcity->where('id_wkgps', $id_worker_gps);
                $data_activiti = $dbcity->get(TBL_WORKER_ACTIVITY)->result_array();
                if (isset($data_activiti) && !empty($data_activiti)) {
                    foreach ($data_activiti as $ac_item){

                        var_dump($ac_item['starttime']);
                        var_dump($ac_item['endtime']);

                        if(isset($ac_item['starttime']) && !empty($ac_item['starttime']) && isset($ac_item['endtime']) && !empty($ac_item['endtime'])){
                            if((strtotime($_time) >= strtotime($ac_item['starttime'])) && (strtotime($_time) < strtotime($ac_item['endtime']))){
                                $data_update[] = array(
                                    'id'             => $_id_infor,
                                    'id_wk_activity' => isset($ac_item['id']) ? $ac_item['id'] : 0,
                                    'updated_at'     => date('Y-m-d H:i:s')
                                );
                            }
                        }
                        /*
                        if (empty($tmp)) {
                            $tmp = array(
                                'id'             => $_id_infor,
                                'id_wk_activity' => -1,
                                'updated_at'     => date('Y-m-d H:i:s')
                            );
                        }
                        $data_update[] = $tmp;
                        */
                    }
                }
                /*
                if (empty($tmp)) {
                    $tmp = array(
                        'id'             => $_id_infor,
                        'id_wk_activity' => -1,
                        'updated_at'     => date('Y-m-d H:i:s')
                    );
                    $data_update[] = $tmp;
                }
                */
                echo PHP_EOL.PHP_EOL;
            }
            echo '----------------------------------'.PHP_EOL;

            # Update
            var_dump($data_update);
            if (isset($data_update) && !empty($data_update)) {
                $db_flag = $dbcity->update_batch(TBL_WORKER_INFORMATION, $data_update, 'id');
                if ($db_flag !== FALSE) {
                    print_r(array(
                        'version' => config_item('api_version'),
                        'status' => STATUS_SUCCESS
                    ));
                } else {
                    print_r(array(
                        'version' => config_item('api_version'),
                        'status'  => STATUS_FAIL,
                        'msg'     => 'Update batch worker_information error_Query: '.$dbcity->last_query()
                    ));
                }
            } else {
                print_r(array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'Data update is empty'
                ));
            }
        }
    }

    #--- ACTIVITY -------------------------------------------------------------------------------
    function activity() {
        $status = TRUE;
        $msg    = '';
        # get file
        $this->db->where('status', CRON_STATUS_PENDING);
        $this->db->where('type', CRON_TYPE_ACTIVITY);
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        $file_data = $this->db->get(TBL_CRON)->row_array();
        $file_data = str_replace('\\\\', '\\', $file_data);

        if (!$file_data || empty($file_data)) {
            print_r(array(
                'status' => FALSE,
                'msg' => 'File data invalid'
            ));
            die;
        }
        $file_path = isset($file_data['data']) ? $file_data['data'] : '';
        if (!empty($file_path) AND !file_exists($file_path)) {
            $file_path = get_server_path() . $file_path;
        }
        $id        = isset($file_data['id']) ? $file_data['id'] : '';
        $id_city   = isset($file_data['id_city']) ? $file_data['id_city'] : '';

        # connect
        $this->db->where('id_city', $id_city);
        $config = $this->db->get(TBL_CITY_CONFIG)->row_array();
        if (!$config || empty($config)) {
            print_r(array(
                'status' => FALSE,
                'msg'    => 'Data connect invalid'
            ));
            die;
        }
        $data_connect   = $config['database'];
        $db_info        = json_decode($data_connect);
        $db_config      = array(
            'hostname' => isset($db_info->hostname) ? $db_info->hostname : config_item('hostname'),
            'username' => isset($db_info->username) ? $db_info->username : config_item('username'),
            'password' => isset($db_info->password) ? $db_info->password : config_item('password'),
            'database' => isset($db_info->database) ? $db_info->database : config_item('database'),
            'port'     => isset($db_info->port) ? $db_info->port : config_item('db_port'),
            'dbprefix' => config_item('db_prefix'),
            'dbdriver' => config_item('db_dbdriver'),
            'pconnect' => config_item('db_pconnect'),
            'db_debug' => config_item('db_db_debug'),
            'cache_on' => config_item('db_cache_on'),
            'cachedir' => config_item('db_cachedir'),
            'char_set' => config_item('db_char_set'),
            'dbcollat' => config_item('db_dbcollat')
        );
        $dbcity         = $this->load->database($db_config, true);
        $dbcity_connect = $dbcity->initialize();
        if (!$dbcity_connect) {
            print_r(array(
                'status' => FALSE,
                'msg' => 'Connect fail'
            ));
            die;
        }
        $update = array();
        # Call sync
        $result = $this->_sync_activity($file_path, $dbcity, $id_city);
        if ($result['status']) {
            $update['status']   = CRON_STATUS_SUCCESS;
            $update['last_run'] = date('Y-m-d H;I;s');
        } else {
            $update['status'] = CRON_STATUS_ERROR;
            $update['note']   = $result['msg'];
        }
        $update['updated_at'] = date('Y-m-d H:i:s');
        $this->db->where('id', $id);
        $this->db->update(TBL_CRON, $update);
        echo $this->db->last_query();
        print_r($result);
        echo '-------------------------------------------\r\n';
    }

    function _sync_activity($file_path = FALSE, $dbconnect = NULL, $id_city = 0) {
        $status = TRUE;
        $msg    = '';

        if ($file_path AND !file_exists($file_path)) {
            $file_path = get_server_path() . $file_path;
        }

        # Check file path
        if (!$file_path OR (file_exists($file_path) == FALSE)) {
            print_r(array(
                'status' => FALSE,
                'msg'    => 'File not exist ' . $file_path
            ));
            die;
        }

        # Check database connect
        if (!isset($dbconnect) OR empty($dbconnect)) {
            print_r(array(
                'status' => FALSE,
                'msg' => 'Connect not exist '
            ));
            die;
        }

        # Helper
        $this->load->helper('file');

        # Read content from file
        $json_text = read_file($file_path);
        echo '--------------------------------------------' . PHP_EOL;
        echo $json_text;
        echo '--------------------------------------------' . PHP_EOL;
        echo '-> Content file read: '.$json_text.'\r\n';
        if (!$json_text OR empty($json_text)) {
            pritn_r(array(
                'status' => FALSE,
                'msg'    => 'Can not read file ' . $file_path
            ));
            die;
        }

        # Decode data
        echo $json_text;
        $json_data = json_decode($json_text, TRUE);

        echo '-> Data parse : '.PHP_EOL;
        print_r($json_data);
        echo PHP_EOL;

        if (!is_array($json_data)) {
            return array(
                'status' => FALSE,
                'msg'    => 'File data is format invalid'
            );
        }

        # Transaction start
        $dbconnect->trans_start();

        # Params
        $id_task     = isset($json_data['task_id'])     ? $json_data['task_id']     : '';
        $id_employee = isset($json_data['employee_id']) ? $json_data['employee_id'] : '';
        $id_machine  = isset($json_data['machine_id'])  ? $json_data['machine_id']  : '';
        $end_time    = isset($json_data['end_time'])    ? $json_data['end_time']    : '';
        $start_time  = isset($json_data['start_time'])  ? $json_data['start_time']  : '';

        if (empty($id_task) || !isset($id_task)) {
            return array(
                'status' => FALSE,
                'msg'    => 'Task id invalid'
            );
            die;
        }

        # Prepare gps detail record
        $gps    = $dbconnect->get_where(TBL_WORKER_GPS, array('id_task' => $id_task ))->row_array();
        $id_gps = NULL;
        if (is_array($gps) AND !empty($gps)) {
            $id_gps = isset($gps['id']) ? $gps['id'] : NULL;
        } else {
            $gps_data = array(
                'id_city'    => $id_city,
                'id_task'    => $id_task,
                'id_object'  => 0,
                'id_worker'  => $id_employee,
                'id_machine' => $id_machine,
                'ontime'     => $start_time,
                'created_at' => date('Y-m-d H:i:s')
            );
            $db_flag  = $dbconnect->insert(TBL_WORKER_GPS, $gps_data);
            echo '-> SQL query: '. $dbconnect->last_query() .'\r\n' ;
            if ($db_flag) {
                $id_gps         = $dbconnect->insert_id();
                $gps_data['id'] = $id_gps;
                $gps            = $gps_data;
            } else {
                return array(
                    'status' => FALSE,
                    'msg' => 'Cant not insert new gps_Data: ' . json_encode($gps_data)
                );
            }
        }

        # Check id_gps
        if (is_null($id_gps) OR (intval($id_gps) <= 0)) {
            return array(
                'status' => FALSE,
                'msg'    => 'id_gps is invalid_Value: ' . $id_gps
            );
        }

        # All activities
        $activities = array();
        if (!isset($json_data['activities']) OR empty($json_data['activities'])) {
            return array(
                'status' => FALSE,
                'msg'    => 'Activity data is not exist'
            );
        }

        $activities = $this->_walking_nodes($json_data['activities'], 'activityid', 'activitie');
        print_r($activities);
        if (!is_array($activities) OR empty($activities)) {
            return array(
                'status' => FALSE,
                'msg'    => 'Activity data parse is invalid_Data: ' . json_encode($activities)
            );
        }

        # Walking activities elements
        $coordinates = array();
        $coordinate_string = '';
        foreach ($activities as $key_activity => $activity) {
            $activity_id               = isset($activity['activityid'])     ? trim($activity['activityid'])     : '';
            $activity_starttime        = isset($activity['starttime'])      ? trim($activity['starttime'])      : '';
            $activity_endtime          = isset($activity['endtime'])        ? trim($activity['endtime'])        : '';
            $activity_task_activity_id = isset($activity['taskactivityid']) ? trim($activity['taskactivityid']) : '';

            # Locations process
            $locations = array();
            if (isset($activity['locations']) && !empty($activity['locations'])) {
                $locations = $activity['locations'];
                $locations = $this->_walking_nodes($locations, 'lat', 'location');
                print_r($locations);
                if (is_array($locations) AND !empty($locations)) {
                    foreach ($locations as $key_location => $value_location) {
                        $lat  = isset($value_location['lat'])  ? $value_location['lat']  : '';
                        $lon  = isset($value_location['lon'])  ? $value_location['lon']  : '';
                        $time = isset($value_location['time'])  ? $value_location['time']  : '';
                        $coordinates[] = array(
                            'id_city'          => $id_city,
                            'id_wkgps'         => $id_gps,
                            'id_activity'      => $activity_id,
                            'id_task'          => $id_task,
                            'id_task_activity' => $activity_task_activity_id,
                            'latitude'         => $lat,
                            'longtitude'       => $lon,
                            'starttime'        => $time,
                            'endtime'          => $time,
                            'created_at'       => date('Y-m-d H:i:s')
                        );

                        $coordinate_string .= "{$lat} {$lon} {$time},";
                    }
                }
            }

            if (is_array($coordinates) AND !empty($coordinates)) {
                $db_flag = $dbconnect->insert_batch(TBL_WORKER_ACTIVITY, $coordinates);
                if ($db_flag === FALSE) {
                    write_log('Insert batch error_Query: '.$dbconnect->last_query().'_Data:'.json_encode($coordinates), 'sync_activity');
                } else {
                    $coordinates = array();
                    echo PHP_EOL . '---> Insert batch activities : ' . PHP_EOL . $dbconnect->last_query() . PHP_EOL;
                }
            }
        }

        # Update worker_gps.position
        $coordinate_string = trim($coordinate_string, ',');
        $dbconnect->where('id', $id_gps);
        $dbconnect->update(TBL_WORKER_GPS, array('position' => $coordinate_string));

        # Transaction end
        $dbconnect->trans_complete();

        return array(
            'status' => TRUE,
            'msg'    => 'The data has been successfully process'
        );
    }

    #---------------------------------------------------------------------------------------------

    function geo_street_name($id_city = NULL, $web_service = NULL) {
        if (is_null($id_city)) {
            return 'Requried id_city';
        }

        $this->db->where('id_city', $id_city);
        $config = $this->db->get(TBL_CITY_CONFIG)->row_array();
        var_dump($config);
        if (!$config || empty($config)) {
            print_r(array(
                'status' => FALSE,
                'msg' => 'Data connect invalid'
            ));
            die;
        }
        $data_connect   = $config['database'];
        $db_info        = json_decode($data_connect);
        $db_config      = array(
            'hostname' => isset($db_info->hostname) ? $db_info->hostname : config_item('hostname'),
            'username' => isset($db_info->username) ? $db_info->username : config_item('username'),
            'password' => isset($db_info->password) ? $db_info->password : config_item('password'),
            'database' => isset($db_info->database) ? $db_info->database : config_item('database'),
            'port'     => isset($db_info->port) ? $db_info->port : config_item('db_port'),
            'dbprefix' => config_item('db_prefix'),
            'dbdriver' => config_item('db_dbdriver'),
            'pconnect' => config_item('db_pconnect'),
            'db_debug' => config_item('db_db_debug'),
            'cache_on' => config_item('db_cache_on'),
            'cachedir' => config_item('db_cachedir'),
            'char_set' => config_item('db_char_set'),
            'dbcollat' => config_item('db_dbcollat')
        );
        $dbcity         = $this->load->database($db_config, true);
        $dbcity_connect = $dbcity->initialize();
        if (!$dbcity_connect) {
            print_r(array(
                'status' => FALSE,
                'msg' => 'Connect fail'
            ));
            die;
        }

        # Get id worker_gps incomplete
        $dbcity->where('status', CRON_STATUS_PENDING);
        $dbcity->where('deleted_at', NULL);
        $id_gps_incomplete = $dbcity->get(TBL_WORKER_GPS)->row();
        if (is_object($id_gps_incomplete) AND !empty($id_gps_incomplete)) {
            $id_gps_incomplete = $id_gps_incomplete->id;
        } else {
            return 'ig_worker_gps is invalid';
        }
        echo '---> id_worker_gps : '. $id_gps_incomplete . PHP_EOL;

        # Get all worker_gps by id_worker_gps
        $dbcity->where('deleted_at', NULL);
        $dbcity->where('id_wkgps', $id_gps_incomplete);
        $dbcity->where('street', '');
        $dbcity->order_by('id_task', 'asc');
        $worker_activities = $dbcity->get(TBL_WORKER_ACTIVITY)->result_array();
        $flag      = TRUE;
        if (is_array($worker_activities) AND !empty($worker_activities)) {
            if (!empty($worker_activities)) {
                echo PHP_EOL . '---------------------------' . PHP_EOL;
                $task_flag = FALSE;
                $data      = array();

                # Activities process
                $counter = 0;
                $total = count($worker_activities);
                foreach ($worker_activities as $key => $value) {
                    echo "--- {$counter} / {$total}" . PHP_EOL;
                    $counter++;
                    if ($task_flag === FALSE) {
                        $task_flag = $id_task;
                    }

                    $id_task     = isset($value['id_task'])    ? $value['id_task']    : '';
                    $latitude    = isset($value['latitude'])   ? $value['latitude']   : '';
                    $longtitude  = isset($value['longtitude']) ? $value['longtitude'] : '';

                    $street_name = $this->extract_street($latitude,$longtitude);

                    echo "---> Geo street name : {$latitude} - {$longtitude} - {$street_name}" . PHP_EOL;
                    $data[] = array(
                        'id'        => $value['id'],
                        'street'    => $street_name
                    );
                    # Update batch if record reach by id_task
                    if ($id_task != $task_flag) {
                        $db_flag = $dbcity->update_batch(TBL_WORKER_ACTIVITY, $data, 'id');
                        if ($db_flag === FALSE) {
                            $flag = FALSE;
                            $msg  = '- Update batch result: Update batch worker_activity fail_Query: '. $dbcity->last_query();
                            write_log($msg, 'cron_geo_street_name');
                        } else {
                            $data = array();
                        }
                        echo $msg . PHP_EOL;
                    }
                }
                echo PHP_EOL . '---------------------------' . PHP_EOL;
            }
            # Group street
            echo '---> Grouping street : '.PHP_EOL;
            $this->group_street($id_city, $id_gps_incomplete);
            # Check location
            echo '---> Check location : '.PHP_EOL;
            $this->check_location_activity($id_city, $id_gps_incomplete);
        }
        # up date gps
        if ($flag) {
            echo '---> Update status worker_gps : ';
            $dbcity->where('id', $id_gps_incomplete);
            $db_flag = $dbcity->update(TBL_WORKER_GPS, array('status' => CRON_STATUS_SUCCESS));
            if ($db_flag) {
                echo 'OK' . PHP_EOL;
            } else {
                write_log('Update worker_gps status fail_ id_worker_gps '.$id_gps_incomplete.'_Query : '.$dbcity->last_query(), 'cron_geo_street_name');
                echo 'FAIL' . PHP_EOL;
            }
        }
    }

    function check_location_activity($id_city = NULL, $id_wkgps = NULL){
        if (is_null($id_city)) {
            return 'Requried id_city';
        }
        # database connect
        $db->city = connect_db(array(),$id_city);
        $dbcity_connect = $db->city->initialize();
        if (!$dbcity_connect) {
            print_r(array(
                'status' => FALSE,
                'msg' => 'Connect fail'
            ));
            die;
        }
        # get all location activity
        if (is_null($id_wkgps)) {
            return 'Requried id_wkgps';
        }

        $db->city->where('id_wkgps',$id_wkgps);
        $db->city->order_by('starttime','asc');
        $db->city->order_by('id','asc');
        $data_activitis = $db->city->get(TBL_WORKER_ACTIVITY)->result_array();
        if(!isset($data_activitis) OR empty($data_activitis)){
            return 'Data location activiti invalid';
        }
        # group activitis
        $data_activitis_group = $this->get_activity_by_idgps($db->city,$id_wkgps);
        if(!isset($data_activitis_group) OR empty($data_activitis_group)){
            return 'Data activiti group invalid';
        }
        #
        $time_check = 15;
        $data               = array();
        $street_old         = '';
        $id_activity_old    = 0;
        $group_street_old   = 0;
        $count              = count($data_activitis_group);
        $count_index        = 0;
        foreach ($data_activitis_group as $key => $value) {
            $id_wkgps           = $value['id_wkgps'];
            $id_activity        = $value['id_activity'];
            $id_task_activity   = $value['id_task_activity'];
            $starttime          = $value['starttime'];
            $endtime            = $value['endtime'];
            $latitude           = $value['latitude'];
            $longtitude         = $value['longtitude'];
            $street             = $value['street'];
            $group_street       = $value['group_street'];

            $_duration = (strtotime($endtime) - strtotime($starttime));
            echo 'Duration ------>'.$_duration.PHP_EOL;
            # get location by activitis
            $location_by_time = array();
            foreach ($data_activitis as $_key => $_value) {
                $_id_wkgps          = $_value['id_wkgps'];
                $_id_activity       = $_value['id_activity'];
                $_id_task_activity  = $_value['id_task_activity'];
                $_starttime         = $_value['starttime'];
                $_endtime           = $_value['endtime'];
                $_latitude          = $_value['latitude'];
                $_longtitude        = $_value['longtitude'];
                $_group_street      = $_value['group_street'];
                $_street            = $_value['street'];
                //($id_activity == $_id_activity) AND ($group_street == $_group_street) AND ($id_task_activity == $_id_task_activity) AND ($street == $_street)
                if( (strtotime($_starttime) >= strtotime($starttime)) AND (strtotime($_endtime) <= strtotime($endtime)) ){
                    $location_by_time[] = array(
                        'lat'   => $_latitude,
                        'lon'   => $_longtitude
                    );
                }
            }
            $_distance = 0;
            if(isset($location_by_time) AND !empty($location_by_time)){
                $count_location = count($location_by_time);
                if($count_location > 1)
                    $_distance = $this->distance_gps($location_by_time[0]['lat'],$location_by_time[0]['lon'],$location_by_time[$count_location - 1]['lat'],$location_by_time[$count_location - 1]['lon']);
            }
            echo 'Distance ------>'.$_distance.PHP_EOL;
            # speed
            $_speed = round($_distance/$_duration);
            echo 'Speed --------->'.$_speed.PHP_EOL;
            #
            $time_check = round(50/$_speed);
            echo 'Time ---------->'.$time_check.PHP_EOL;
            if(isset($street) && !empty($street)){
                echo '-------------'.$street.PHP_EOL;
                $_street_name = $street;
                $_group_street = $group_street;
                if($time_check < 1){
                    $_street_name = $street_old;
                    $_group_street = $group_street_old;
                } else {
                    if(isset($street_old) && !empty($street_old)){
                        if($street_old != $street){
                            $street_new         = '';
                            if($count_index < ($count - 1)){
                                $street_new         = $result_activities[$count_index + 1]['street'];
                            }
                            if(isset($street_new) && !empty($street_new)){
                                if( $street_old == $street_new ){
                                    if($_duration < $time_check){
                                        $_street_name = $street_old;

                                        if($id_activity_old == $id_activity){
                                            $_group_street = $group_street_old;
                                        }
                                    }
                                } else {
                                    if($_duration < $time_check){
                                        if($id_activity_old == $id_activity){
                                            $_street_name = $street_old;
                                            $_group_street = $group_street_old;
                                        }
                                    }
                                }
                            } else {
                                if($id_activity_old == $id_activity){
                                    $_group_street = $group_street_old;
                                }
                            }
                        } else {
                            if($id_activity_old == $id_activity){
                                $_group_street = $group_street_old;
                            }
                        }

                        $street_old = $_street_name;
                        $id_activity_old = $id_activity;
                        $group_street_old = $_group_street;
                    }
                }
                $data[] = array(
                    'starttime'     => $starttime,
                    'endtime'       => $endtime,
                    'id_activity'   => $id_activity,
                    'street'        => $_street_name,
                    'street_old'    => $street,
                    'group_street'  => $_group_street
                );
                #
                if(!isset($street_old) || empty($street_old))
                    $street_old = $street;
                if($id_activity_old < 1)
                    $id_activity_old = $id_activity;
                if($group_street_old < 1)
                    $group_street_old = $group_street;
            }
            $count_index++;
        }

        if(isset($data) && !empty($data)){
            $data_update = array();
            foreach ($data as $key => $value) {
                $start_time    = $value['starttime'];
                $end_time      = $value['endtime'];
                echo 'Street --->'.$value['street'].PHP_EOL;
                foreach ($data_activitis as $item) {
                    $_start_time    = $item['starttime'];
                    $_end_time      = $item['endtime'];
                    $_id_activity   = $item['id_activity'];
                    if( (strtotime($_start_time) >= strtotime($start_time)) AND (strtotime($_end_time) <= strtotime($end_time)) AND ($value['id_activity'] == $_id_activity) ){
                        $data_update[] = array(
                            'id'            => $item['id'],
                            'starttime'     => $value['starttime'],
                            'endtime'       => $value['endtime'],
                            'id_activity'   => $value['id_activity'],
                            'street'        => $value['street'],
                            'street_old'    => $value['street_old'],
                            'group_street'  => $value['group_street']
                        );
                    }
                }
            }
            #
            if(isset($data_update) && !empty($data_update)){
                echo '---> Update batch worker_activity : ' . PHP_EOL;
                $db_flag = $db->city->update_batch(TBL_WORKER_ACTIVITY, $data_update, 'id');
                if ($db_flag === FALSE) {
                    echo 'UPDATE_FAIL'.PHP_EOL;
                } else {
                    echo 'UPDATE_OK'.PHP_EOL;
                }
            } else{
                echo 'Data update invalid'.PHP_EOL;
            }
        } else {echo 'Data invalid'.PHP_EOL;}
    }

    function check_location_activity_back($id_city = NULL, $id_wkgps = NULL){
        if (is_null($id_city)) {
            return 'Requried id_city';
        }
        #
        $dbcity = connect_db(array(),$id_city);
        $dbcity_connect = $dbcity->initialize();
        if (!$dbcity_connect) {
            print_r(array(
                'status' => FALSE,
                'msg' => 'Connect fail'
            ));
            die;
        }

        $result_activities = $this->get_activity_by_idgps($dbcity,$id_wkgps);
        if(isset($result_activities) && !empty($result_activities)){

            $data               = array();
            $street_old         = '';
            $id_activity_old    = 0;
            $group_street_old   = 0;
            $count              = count($result_activities);
            $count_index        = 0;
            foreach ($result_activities as $key => $value) {
                $id_wkgps           = $value['id_wkgps'];
                $id_activity        = $value['id_activity'];
                $id_task_activity   = $value['id_task_activity'];
                $starttime          = $value['starttime'];
                $endtime            = $value['endtime'];
                $latitude           = $value['latitude'];
                $longtitude         = $value['longtitude'];
                $street             = $value['street'];
                $group_street       = $value['group_street'];
                $_duration = (strtotime($endtime) - strtotime($starttime));
                echo '---------'.$_duration.PHP_EOL;
                #
                if(isset($street) && !empty($street)){
                    echo '-------------'.$street.PHP_EOL;
                    $_street_name = $street;
                    $_group_street = $group_street;
                    if(isset($street_old) && !empty($street_old)){
                        if($street_old != $street){
                            $street_new         = '';
                            if($count_index < ($count - 1)){
                                $street_new         = $result_activities[$count_index + 1]['street'];
                            }
                            if(isset($street_new) && !empty($street_new)){
                                if( $street_old == $street_new ){
                                    if($_duration < 15){
                                        $_street_name = $street_old;

                                        if($id_activity_old == $id_activity){
                                            $_group_street = $group_street_old;
                                        }
                                    }
                                } else {
                                    if($_duration < 15){
                                        if($id_activity_old == $id_activity){
                                            $_street_name = $street_old;
                                            $_group_street = $group_street_old;
                                        }
                                    }
                                }
                            } else {
                                if($id_activity_old == $id_activity){
                                    $_group_street = $group_street_old;
                                }
                            }
                        } else {
                            if($id_activity_old == $id_activity){
                                $_group_street = $group_street_old;
                            }
                        }

                        $street_old = $_street_name;
                        $id_activity_old = $id_activity;
                        $group_street_old = $_group_street;
                    }
                    $data[] = array(
                        'starttime'     => $starttime,
                        'endtime'       => $endtime,
                        'id_activity'   => $id_activity,
                        'street'        => $_street_name,
                        'street_old'    => $street,
                        'group_street'  => $_group_street
                    );
                    #
                    if(!isset($street_old) || empty($street_old))
                        $street_old = $street;
                    if($id_activity_old < 1)
                        $id_activity_old = $id_activity;
                    if($group_street_old < 1)
                        $group_street_old = $group_street;
                }
                $count_index++;
            }
            if(isset($data) && !empty($data)){
                $data_update = array();
                foreach ($data as $key => $value) {
                    $dbcity->where('starttime >=',date('Y-m-d H:i:s',strtotime($value['starttime'])));
                    $dbcity->where('endtime <=',date('Y-m-d H:i:s',strtotime($value['endtime'])));
                    $dbcity->where('id_activity ',$value['id_activity']);
                    $result_update = $dbcity->get(TBL_WORKER_ACTIVITY)->result_array();
                    if(isset($result_update) && !empty($result_update)){
                        foreach ($result_update as $item) {
                            $data_update[] = array(
                                'id'            => $item['id'],
                                'starttime'     => $value['starttime'],
                                'endtime'       => $value['endtime'],
                                'id_activity'   => $value['id_activity'],
                                'street'        => $value['street'],
                                'street_old'    => $value['street_old'],
                                'group_street'  => $value['group_street']
                            );
                        }
                    }
                }

                if(isset($data_update) && !empty($data_update)){
                    echo '---> Update batch worker_activity : ' . PHP_EOL;
                    $db_flag = $dbcity->update_batch(TBL_WORKER_ACTIVITY, $data_update, 'id');
                    if ($db_flag === FALSE) {
                        echo 'UPDATE_FAIL'.PHP_EOL;
                    } else {
                        echo 'UPDATE_OK'.PHP_EOL;
                    }
                }
            } else {echo 'Data invalid'.PHP_EOL;}
        }
    }

    function clean_unnecessary_streets2($id_city = null, $id_wkgps = null, $db_connect = null){
        if ( is_null($id_city) OR is_null($id_wkgps) ) {
            die('id_city or id_wkgps can not null');
        }

        if (is_null($db_connect)) {
            $db_connect = connect_db(false, $id_city);
        }

        # Logs
        write_log("Start clean_unnecessary_streets _ id_city: {$id_city} _ id_wkgps: {$id_wkgps}", 'clean_unnecessary_streets_2');

        # get activity in tour
        # Get data activiti
        $activiti_data = array();
        $db_connect->where('deleted_at',NULL);
        $activiti_data = $db_connect->get(TBL_ACTIVITIES)->result_array();
        # get activiti group
        $activiti_result = array();
        $db_connect->query('SET SESSION group_concat_max_len = 1000000000');
        $db_connect->select("group_concat(wac.id) as id_group, group_concat(wac.street_old) as street_name_old,wac.id_wkgps,wac.id_activity,min(wac.starttime) as starttime,max(wac.endtime) as endtime, (max(wac.endtime) - min(wac.starttime)) as duration,wac.street,wac.group_street");
        $db_connect->from(TBL_WORKER_ACTIVITY.' as wac');
        $db_connect->where('wac.deleted_at',NULL);
        $db_connect->where('wac.id_wkgps',$id_wkgps);
        $db_connect->order_by('wac.id','asc');
        $db_connect->group_by('wac.id_wkgps,wac.id_activity,wac.id_task_activity,wac.street,wac.group_street');
        $activiti_result = $db_connect->get()->result_array();

        $group_street = NULL;
        $is_first = FALSE;
        foreach ($activiti_result as $key => $value) {
            if($value['duration'] < 3){
                if( ($value['duration'] > 0) AND ($is_first == FALSE) ){
                    $is_first = TRUE;
                }

                $activiti_result[$key]['street_old']        = $activiti_result[$key]['street'];
                if( ($key > 0) AND ($is_first == TRUE) ){
                    if(!isset($group_street) OR empty($group_street)){
                        $group_street = $value['group_street'];
                    }
                    $activiti_result[$key]['street']            = $activiti_result[$key - 1]['street'];;
                    $activiti_result[$key]['group_street']      = $group_street;
                } else {
                    if(!isset($group_street) OR empty($group_street)){
                        $group_street = $activiti_result[$key + 1]['group_street'];
                    }
                    $activiti_result[$key]['street']            = $activiti_result[$key + 1]['street'];;
                    $activiti_result[$key]['group_street']      = $group_street;
                }
                $activiti_result[$key]['flag']              = 'x';
            } else {
                $group_street = $value['group_street'];
            }
        }

        foreach ($activiti_result as $key => $value) {
            if($key > 0){
                if($value['street'] == $activiti_result[$key - 1]['street']){
                    $activiti_result[$key]['group_street'] = $activiti_result[$key - 1]['group_street'];
                    $activiti_result[$key]['flag']              = 'x';
                }
            }
        }

        $update_array = array();
        foreach ($activiti_result as $key => $value) {
            $flag = isset($value['flag']) ? TRUE : FALSE;
            if($flag){

                $id_group           = $value['id_group'];
                $id_array           = explode(',', $id_group);
                $street_name_old    = $value['street_name_old'];
                $street_array       = explode(',', $street_name_old);
                foreach ($id_array as $id_key => $id_value) {
                    $_street_old    = isset($value['street_old']) ? $value['street_old'] : '';
                    $update_array[] = array(
                        'id'            => $id_value,
                        'street'        => $value['street'],
                        'street_old'    => (isset($street_array[$id_key]) ? $street_array[$id_key] : '').'-'.$_street_old,
                        'group_street'  => $value['group_street'],
                        'updated_at'    => date('Y-m-d H:i:s')
                    );
                }
            }
        }
        #print_r($update_array); die;
        if(is_array($update_array) AND !empty($update_array)){
            $flag = $db_connect->update_batch(TBL_WORKER_ACTIVITY,$update_array,'id');
            if ($flag === FALSE) {
                echo 'FAIL _ Query : '. $db_connect->last_query();
            } else {
                echo 'OK _ Query : '. $db_connect->last_query();
            }
        }else {
            echo 'Data update invalid .';
        }
    }

    function check_gps_status(){
        write_log('Running ... ', 'check_gps_status');
        $this->load->helper('pheanstalk');
        $gps_pending = array();
    }

    function check_cron_status(){
        write_log('Running ... ', 'add_push_job');
        $this->load->helper('pheanstalk');

        $cron_push_jobs = array();
        $this->db->select('data');
        $cron_push_jobs = $this->db->get(TBL_CRON_PUS_JOBS)->result_array();

        $cron_result = array();
        $this->db->where('status'   , 'pending');
        $this->db->where('type'     , 'activity');
        $this->db->order_by('id','asc');
        $this->db->limit(20);
        $cron_result = $this->db->get(TBL_CRON)->result_array();
        if(is_array($cron_result) AND !empty($cron_result)){
            foreach ($cron_result as $key => $value) {
                if(is_array($cron_push_jobs) AND !empty($cron_push_jobs)){
                    if(in_array(array('data' => $value['data']), $cron_push_jobs)){
                        write_log('Queue ' . QUE_SYNC . ' exist data :'.json_encode(array('data' => $value['data'])), 'add_push_job');
                        if( (strtotime(date('Y-m-d H:i:s')) - strtotime($value['created_at'])) > 100 ){
                            $data_push_job = array(
                                'id_cron'   => $value['id'],
                                'id_city'   => $value['id_city'],
                                'data'      => $value['data']
                            );
                            push_job(QUE_SYNC, json_encode($data_push_job));
                            write_log('Add queue ' . QUE_SYNC . ' data :'.json_encode($data_push_job), 'add_push_job');
                        }
                    } else {
                        $data_push_job = array(
                            'id_cron'   => $value['id'],
                            'id_city'   => $value['id_city'],
                            'data'      => $value['data'],
                        );
                        push_job(QUE_SYNC, json_encode($data_push_job));
                        write_log('Add queue ' . QUE_SYNC . ' data :'.json_encode($data_push_job), 'add_push_job');
                        # insert
                        $cron_push_job_data = array(
                            'id_city'   => $value['id_city'],
                            'id_cron'   => $value['id'],
                            'data'      => $value['data']
                        );
                        $flag = $this->db->insert(TBL_CRON_PUS_JOBS,$cron_push_job_data);
                        if($flag != FALSE){
                            write_log('insert ' . TBL_CRON_PUS_JOBS . ' OK data :'.json_encode($cron_push_job_data), 'add_push_job');
                        } else {
                            write_log('insert ' . TBL_CRON_PUS_JOBS . ' FAIL data :'.json_encode($cron_push_job_data), 'add_push_job');
                        }
                    }
                }
            }
        }
    }

    function clean_point_position($id_city = null, $id_wkgps = null, $db_connect = null){
    	if ( is_null($id_city) OR is_null($id_wkgps) ) {
    		die('id_city or id_wkgps can not null');
    	}

    	if (is_null($db_connect)) {
    		$db_connect = connect_db(false, $id_city);
    	}
    	# get position
    	$gps_result = array();
    	$db_connect->where('id_wkgps', $id_wkgps);
    	$gps_result = $db_connect->get(TBL_WORKER_ACTIVITY)->result_array();
    	if(!is_array($gps_result) OR empty($gps_result)){
    		write_log("Data GPS empty", 'clean_point_position');
    		die;
    	}
    	$line_position = '';
    	$old_lat		= false;
    	$old_lon		= false;
    	$old_street		= false;
    	$old_activiti	= false;
    	foreach ($gps_result as $key => $value){
    		if($key < 1){
    			$old_lat		= $value['latitude'];
    			$old_lon		= $value['longtitude'];
    			$old_street		= $value['street'];
    			$old_activiti	= $value['id_activity'];
    			$line_position = $old_lat . ' ' . $old_lon;
    			continue;
    		}
    		$lat 			= $value['latitude'];
    		$lon 			= $value['longtitude'];
    		$street			= $value['street'];
    		$activiti		= $value['id_activity'];

    		if(($street == $old_street) AND ($activiti == $old_activiti)){
    			$distance = $this->distance_gps($old_lat,$old_lon,$lat,$lon);
    			if($distance > 10){
    				$old_lat		= $lat;
    				$old_lon		= $lon;
    				$old_street		= $street;
    				$old_activiti	= $activiti;
    				$line_position 	= $line_position . ',' . $old_lat . ' ' . $old_lon;
    			}
    		} else {
    			$old_lat		= $lat;
    			$old_lon		= $lon;
    			$old_street		= $street;
    			$old_activiti	= $activiti;
    			$line_position 	= $line_position . ',' . $old_lat . ' ' . $old_lon;
    		}
    	}
    	if(isset($line_position) AND !empty($line_position)){
    		$db_connect->where('id', $id_wkgps);
    		$flag = $db_connect->update(TBL_WORKER_GPS,array('position_clean' => $line_position));
    		if($flag != FALSE){
    			write_log("Update GPS OK - id : " . $id_wkgps, 'clean_point_position');
    		} else {
    			write_log("Update GPS FAIL - id : " . $id_wkgps, 'clean_point_position');
    		}
    	} else {
    		write_log("Data position empty - id : " . $id_wkgps, 'clean_point_position');
    	}
    }

    function group_street($id_city = NULL, $id_wkgps = NULL) {
        if (is_null($id_city)) {
            return 'Requried id_city';
        }

        $this->db->where('id_city', $id_city);
        $config = $this->db->get(TBL_CITY_CONFIG)->row_array();
        var_dump($config);
        if (!$config || empty($config)) {
            print_r(array(
                'status' => FALSE,
                'msg' => 'Data connect invalid'
            ));
            die;
        }
        $data_connect   = $config['database'];
        $db_info        = json_decode($data_connect);
        $db_config      = array(
            'hostname' => isset($db_info->hostname) ? $db_info->hostname : config_item('hostname'),
            'username' => isset($db_info->username) ? $db_info->username : config_item('username'),
            'password' => isset($db_info->password) ? $db_info->password : config_item('password'),
            'database' => isset($db_info->database) ? $db_info->database : config_item('database'),
            'port'     => isset($db_info->port) ? $db_info->port : config_item('db_port'),
            'dbprefix' => config_item('db_prefix'),
            'dbdriver' => config_item('db_dbdriver'),
            'pconnect' => config_item('db_pconnect'),
            'db_debug' => config_item('db_db_debug'),
            'cache_on' => config_item('db_cache_on'),
            'cachedir' => config_item('db_cachedir'),
            'char_set' => config_item('db_char_set'),
            'dbcollat' => config_item('db_dbcollat')
        );
        $dbcity         = $this->load->database($db_config, true);
        $dbcity_connect = $dbcity->initialize();
        if (!$dbcity_connect) {
            print_r(array(
                'status' => FALSE,
                'msg' => 'Connect fail'
            ));
            die;
        }

        if (is_null($id_wkgps)) {
            $dbcity->select('id_wkgps');
            $dbcity->where('_group_street', 0);
            $data = $dbcity->get(TBL_WORKER_ACTIVITY)->row();
            if (isset($data->id_wkgps)) {
                $id_wkgps = $data->id_wkgps;
            } else {
                return 'id_wkgps is required';
            }
        }
        echo "---> id_wkgps : ".$id_wkgps;

        # Activites
        $dbcity->where('deleted_at', NULL, FALSE);
        $dbcity->where('id_wkgps', $id_wkgps);
        $dbcity->order_by('id', 'asc');
        $activities =$dbcity->get(TBL_WORKER_ACTIVITY)->result_array();
        print_r($activities);

        if (is_array($activities) AND !empty($activities)) {
            /*$data    = array();
            foreach ($activities as $key => $value) {
                $value['street'] = trim($value['street']);
                $data[$value['id_activity']][] = $value;
            }
            print_r($data);*/
            echo '------------------------------------------' . PHP_EOL;

            $batch_data = array();
            if (is_array($activities) AND !empty($activities)) {
                $counter = 1;
                $last_street = '';
                foreach ($activities as $key => $value) {
                    $street_checker = array();
                    if (is_array($value) AND !empty($value)) {
                        if ($value['street'] != $last_street) {
                            $counter++;
                        }
                        $last_street = $value['street'];
                        $batch_data[] = array(
                            'id'           => $value['id'],
                            'group_street' => $counter
                        );
                    }
                }

                echo '---> Batch data :' . PHP_EOL;
                print_r($batch_data);
                if (is_array($batch_data) AND !empty($batch_data)) {
                    echo '---> Update batch worker_activity : ' . PHP_EOL;
                    $db_flag = $dbcity->update_batch(TBL_WORKER_ACTIVITY, $batch_data, 'id');
                    echo $dbcity->last_query();
                    if ($db_flag === FALSE) {
                        $msg = 'FAIL _ Query : '. $this->db->last_query();
                    } else {
                        $msg = 'OK _ Query : '. $this->db->last_query();
                    }
                }
            }
        }
    }

    function _walking_nodes($data = array(), $key_checker = '', $key_value = '')
    {
        return $data;
        #print_r($data);die;
        $return = array();
        # Has one
        if (isset($data[$key_value]) && isset($data[$key_value][$key_checker]) || isset($data[$key_checker])) {
            $return[] = $data[$key_value];
        } else {
            # Has many
            if (!empty($key_value) && isset($data[$key_value]) && !empty($data)) {
                $return = $data[$key_value];
            } else {
                if (isset($data) && !empty($data)) {
                    foreach ($data as $key => $value) {
                        $return[] = $value;
                    }
                }
            }
        }
        return $return;
    }

    function extract_street($_lat,$_lon)
    {
        $street = '';
        if ((isset($_lat) && !empty($_lat)) && (isset($_lon) && !empty($_lon))) {
            $street_data = '';
            try {
                $street_data = file_get_contents($this->_url_path_service.'&lat='.$_lat.'&lon='.$_lon);
            }
            catch (Exception $e) {
            }
            #
            if(isset($street_data) && !empty($street_data)){
                $street_array = json_decode($street_data,true);
                if (isset($street_array['address']) && !empty($street_array['address'])) {
                    $street_detail = $street_array['address'];
                    if(isset($street_detail['road']) && !empty($street_detail['road'])){
                        $street_name   = isset($street_detail['road']) ? $street_detail['road'] : '';
                        $street = $street_name;
                    }
                }
            }
        }
        return $street;
    }

    function _extract_street($_lat, $_lon)
    {
        $return = '';
        if ((isset($_lat) && !empty($_lat)) && (isset($_lon) && !empty($_lon))) {
            $_street_data = '';
            try {
                $_street_data = file_get_contents($this->_path_service . '&lat=' . $_lat . '&lng=' . $_lon . '&from=1&to=1');
            }
            catch (Exception $e) {
            }
            $_street_array = json_decode($_street_data);
            if (isset($_street_array->result) && !empty($_street_array->result)) {
                $_street_detail = $_street_array->result[0];
                $_street_name   = isset($_street_detail->name) ? $_street_detail->name : '';
                $return         = $_street_name;
            } else {
                write_log("error get street name NULL : " . json_encode(array(
                    'lat' => $_lat,
                    'lon' => $_lon
                )), 'sync_get_street');
            }
        } else {
            write_log("error location NULL : " . json_encode(array(
                'lat' => $_lat,
                'lon' => $_lon
            )), 'sync_get_street');
        }
        return $return;
    }

    function _extract_street2($_lat, $_lon)
    {
        $return = '';
        if ((isset($_lat) && !empty($_lat)) && (isset($_lon) && !empty($_lon))) {
            $_street_data = '';
            try {
                $tmp = $this->_path_service2 . '&lat='.$_lat.'&lon='.$_lon.'&zoom=18&addressdetails=1';
                echo $tmp . PHP_EOL;
                $_street_data = file_get_contents($tmp);
            }
            catch (Exception $e) {
            }
            $_street_array = json_decode($_street_data);
            if (isset($_street_array->address) && !empty($_street_array->address)) {
                $_street_detail = $_street_array->address;
                $_street_name   = isset($_street_detail->road) ? $_street_detail->road : '';
                $return         = $_street_name;
            } else {
                write_log("error get street name NULL : " . json_encode(array(
                'lat' => $_lat,
                'lon' => $_lon
                )), 'sync_get_street');
            }
        } else {
            write_log("error location NULL : " . json_encode(array(
            'lat' => $_lat,
            'lon' => $_lon
            )), 'sync_get_street');
        }
        return $return;
    }

    function _extract_street3($_lat, $_lon)
    {
        $url    = 'http://open.mapquestapi.com/nominatim/v1/reverse.php?format=json';
        $return = '';
        if ((isset($_lat) && !empty($_lat)) && (isset($_lon) && !empty($_lon))) {
            $_street_data = '';
            try {
                $tmp = $url . '&lat='.$_lat.'&lon='.$_lon;
                echo $tmp . PHP_EOL;
                $_street_data = file_get_contents($tmp);
            } catch (Exception $e) {
            }

            $_street_array = json_decode($_street_data);
            if (isset($_street_array->address) && !empty($_street_array->address)) {
                $_street_detail = $_street_array->address;
                $_street_name   = isset($_street_detail->road) ? $_street_detail->road : '';
                $return         = $_street_name;
            } else {
                write_log("error get street name NULL : " . json_encode(array(
                'lat' => $_lat,
                'lon' => $_lon
                )), 'sync_get_street');
            }
        } else {
            write_log("error location NULL : " . json_encode(array(
            'lat' => $_lat,
            'lon' => $_lon
            )), 'sync_get_street');
        }
        return $return;
    }

    function _check_street($location = NULL, $start_time = '', $end_time = '')
    {
        $return = array();
        $time   = NULL;
        $street = NULL;
        $lat    = NULL;
        $lon    = NULL;
        $start  = $start_time;
        $end    = $end_time;
        $count  = 0;
        if (isset($location) && !empty($location)) {
            foreach ($location as $item) {
                $_lat  = isset($item['lat']) ? $item['lat'] : '';
                $_lon  = isset($item['lon']) ? $item['lon'] : '';
                $_time = isset($item['time']) ? $item['time'] : '';

                # begin lat
                if (!isset($lat) && empty($lat)) {
                    $lat = $_lat;
                }

                # begin lon
                if (!isset($lon) && empty($lon)) {
                    $lon = $_lon;
                }

                if (!isset($start) && empty($start)) {
                    $start = $_time;
                }

                if (isset($lat) && !empty($lat) && isset($lon) && !empty($lon)) {
                    echo '---> Geo street'.PHP_EOL;
                    $counter = 0;
                    while($counter < 3) {
                        $_street = $this->_extract_street2($_lat, $_lon);
                        echo "{$_lat} {$_lon} -> " . $_street . PHP_EOL;
                        if (trim($_street) == '') {
                            $counter++;
                            sleep(3);
                            echo 'FAIL '.$counter.PHP_EOL;
                        } else {
                            break;
                        }
                    }
                }

                if (!isset($_street) || empty($_street)) {
                    $_street = '';
                }

                if (($street != $_street) && (isset($_street) && !empty($_street))) {
                    $return[$count] = array(
                        'street' => $_street,
                        'lat'    => $lat,
                        'lon'    => $lon,
                        'start'  => $start,
                        'end'    => ''
                    );
                    if ($count > 0) {
                        $return[$count - 1]['end'] = $_time;
                    }
                    $lat   = $_lat;
                    $lon   = $_lon;
                    # start street
                    $start = $_time;
                    $count++;
                }

                /*} else {
                    write_log("Error Street Name NULL : " . json_encode(array(
                        'lat' => $_lat,
                        'lon' => $_lon,
                        'time' => $time
                    )), 'sync_get_street');
                }
                */
                $street = $_street;
            }
            # Add endtime last
            if (isset($return) && !empty($return)) {
                $return[$count - 1]['end'] = $end;
            }
        } else {
            write_log("Error Location NULL : " . json_encode(array(
                'lat' => $_lat,
                'lon' => $_lon,
                'time' => $time
            )), 'sync_get_street');
        }
        return $return;
    }

    function _lastIndexOf($string,$item){
        $index=strpos(strrev($string),strrev($item));
        if ($index){
            $index=strlen($string)-strlen($item)-$index;
            return $index;
        } else {
            return -1;
        }
    }

    function update_city_id($id_city){
        if (is_null($id_city)) {
            return 'Requried id_city';
        }

        $this->db->where('id_city', $id_city);
        $config = $this->db->get(TBL_CITY_CONFIG)->row_array();

        if (!$config || empty($config)) {
            print_r(array(
            'status' => FALSE,
            'msg' => 'Data connect invalid'
                    ));
                    die;
        }
        $data_connect   = $config['database'];
        $db_info        = json_decode($data_connect);
        $db_config      = array(
                'hostname' => isset($db_info->hostname) ? $db_info->hostname : config_item('hostname'),
                'username' => isset($db_info->username) ? $db_info->username : config_item('username'),
                'password' => isset($db_info->password) ? $db_info->password : config_item('password'),
                'database' => isset($db_info->database) ? $db_info->database : config_item('database'),
                'port'     => isset($db_info->port) ? $db_info->port : config_item('db_port'),
                'dbprefix' => config_item('db_prefix'),
                'dbdriver' => config_item('db_dbdriver'),
                'pconnect' => config_item('db_pconnect'),
                'db_debug' => config_item('db_db_debug'),
                'cache_on' => config_item('db_cache_on'),
                'cachedir' => config_item('db_cachedir'),
                'char_set' => config_item('db_char_set'),
                'dbcollat' => config_item('db_dbcollat')
        );
        $dbcity         = $this->load->database($db_config, true);
        $dbcity_connect = $dbcity->initialize();
        if (!$dbcity_connect) {
            print_r(array(
            'status' => FALSE,
            'msg' => 'Connect fail'
                    ));
                    die;
        }
        # update
        #activities
        $dbcity->query('UPDATE '.TBL_ACTIVITIES.' SET id_city = {$id_city}');
        echo '---> Update status '.TBL_ACTIVITIES.' : ';
        echo 'OK' . PHP_EOL;
        #company
        $dbcity->query('UPDATE '.TBL_COMPANY.' SET id_city = {$id_city}');
        echo '---> Update status '.TBL_COMPANY.' : ';
        echo 'OK' . PHP_EOL;
        #devices
        $dbcity->query('UPDATE '.TBL_DEVICES.' SET id_city = {$id_city}');
        echo '---> Update status '.TBL_DEVICES.' : ';
        echo 'OK' . PHP_EOL;
        #fasttask
        $dbcity->query('UPDATE '.TBL_FASTTASK.' SET id_city = {$id_city}');
        echo '---> Update status '.TBL_FASTTASK.' : ';
        echo 'OK' . PHP_EOL;
        #machine
        $dbcity->query('UPDATE '.TBL_MACHINE.' SET id_city = {$id_city}');
        echo '---> Update status '.TBL_MACHINE.' : ';
        echo 'OK' . PHP_EOL;
        #machine_material_used
        $dbcity->query('UPDATE '.TBL_MACHINE_MATERIAL_USED.' SET id_city = {$id_city}');
        echo '---> Update status '.TBL_MACHINE_MATERIAL_USED.' : ';
        echo 'OK' . PHP_EOL;
        #material
        $dbcity->query('UPDATE '.TBL_MATERIAL.' SET id_city = {$id_city}');
        echo '---> Update status '.TBL_MATERIAL.' : ';
        echo 'OK' . PHP_EOL;
        #object
        $dbcity->query('UPDATE '.TBL_OBJECT.' SET id_city = {$id_city}');
        echo '---> Update status '.TBL_OBJECT.' : ';
        echo 'OK' . PHP_EOL;
        #object_place
        $dbcity->query('UPDATE '.TBL_OBJECT_PLACE.' SET id_city = {$id_city}');
        echo '---> Update status '.TBL_OBJECT_PLACE.' : ';
        echo 'OK' . PHP_EOL;
        #worker
        $dbcity->query('UPDATE '.TBL_WORKER.' SET id_city = {$id_city}');
        echo '---> Update status '.TBL_WORKER.' : ';
        echo 'OK' . PHP_EOL;
        # Many
        $count_activiti = 1;
        while ($count_activiti > 0) {
            $dbcity->query('UPDATE '.TBL_WORKER_ACTIVITY.' SET id_city = {$id_city} WHERE id_city != {$id_city} order by id asc LIMIT 5000');

            $check = array();
            $dbcity->where('id_city !=',$id_city);
            $dbcity->order_by('id','desc');
            $dbcity->limit(1);
            $check = $dbcity->get(TBL_WORKER_ACTIVITY)->result_array();
            if(!isset($check) || empty($check))
                $count_activiti = 0;

            sleep(5);
        }
        echo '---> Update status '.TBL_WORKER_ACTIVITY.' : ';
        echo 'OK' . PHP_EOL;
        $count_gps = 1;
        while ($count_gps > 0) {
            $dbcity->query('UPDATE '.TBL_WORKER_GPS.' SET id_city = {$id_city} WHERE id_city != {$id_city} order by id asc LIMIT 5000');

            $check = array();
            $dbcity->where('id_city !=',$id_city);
            $dbcity->order_by('id','desc');
            $dbcity->limit(1);
            $check = $dbcity->get(TBL_WORKER_GPS)->result_array();
            if(!isset($check) || empty($check))
                $count_gps = 0;

            sleep(5);
        }
        echo '---> Update status '.TBL_WORKER_GPS.' : ';
        echo 'OK' . PHP_EOL;
        $count_infor = 1;
        while ($count_infor > 0) {
            $dbcity->query('UPDATE '.TBL_WORKER_INFORMATION.' SET id_city = {$id_city} WHERE id_city != {$id_city} order by id asc LIMIT 5000');

            $check = array();
            $dbcity->where('id_city !=',$id_city);
            $dbcity->order_by('id','desc');
            $dbcity->limit(1);
            $check = $dbcity->get(TBL_WORKER_INFORMATION)->result_array();
            if(!isset($check) || empty($check))
                $count_infor = 0;

            sleep(5);
        }
        echo '---> Update status '.TBL_WORKER_INFORMATION.' : ';
        echo 'OK' . PHP_EOL;
    }

    function update_street_old_tour($id_city = NULL) {
        if (is_null($id_city)) {
            die('Requried id_city');
        }

        $this->db->where('id_city', $id_city);
        $config = $this->db->get(TBL_CITY_CONFIG)->row_array();
        var_dump($config);
        if (!$config || empty($config)) {
            print_r(array(
                'status' => FALSE,
                'msg' => 'Data connect invalid'
            ));
            die;
        }
        $data_connect   = $config['database'];
        $db_info        = json_decode($data_connect);
        $db_config      = array(
            'hostname' => isset($db_info->hostname) ? $db_info->hostname : config_item('hostname'),
            'username' => isset($db_info->username) ? $db_info->username : config_item('username'),
            'password' => isset($db_info->password) ? $db_info->password : config_item('password'),
            'database' => isset($db_info->database) ? $db_info->database : config_item('database'),
            'port'     => isset($db_info->port) ? $db_info->port : config_item('db_port'),
            'dbprefix' => config_item('db_prefix'),
            'dbdriver' => config_item('db_dbdriver'),
            'pconnect' => config_item('db_pconnect'),
            'db_debug' => config_item('db_db_debug'),
            'cache_on' => config_item('db_cache_on'),
            'cachedir' => config_item('db_cachedir'),
            'char_set' => config_item('db_char_set'),
            'dbcollat' => config_item('db_dbcollat')
        );
        $dbcity         = $this->load->database($db_config, true);
        $dbcity_connect = $dbcity->initialize();
        if (!$dbcity_connect) {
            print_r(array(
                'status' => FALSE,
                'msg' => 'Connect fail'
            ));
            die;
        }

        $dbcity->where('street', '');
        $dbcity->limit(200);
        $list_activities = $dbcity->get(TBL_WORKER_ACTIVITY)->result_array();
        if (is_array($list_activities) AND !empty($list_activities)) {
            write_log('Start update street name for older tour', 'street_older_tour');
            $data = array();
            $id_array = array();
            foreach ($list_activities as $key => $value) {
                $lat = isset($value['latitude']) ? $value['latitude'] : '';
                $lon = isset($value['longtitude']) ? $value['longtitude'] : '';
                $street = trim($this->extract_street($lat, $lon));
                echo "{$value['id']} : {$lat} {$lon} ---> {$street} \n" . PHP_EOL;
                $id_array[] = $value['id'];
                $data[] = array(
                    'id'     => $value['id'],
                    'street' => empty($street) ? '<Unavailable>' : $street
                );
            }

            if (is_array($data) AND !empty($data)) {
                echo '--> Update street to db : ';
                $db_flag = $dbcity->update_batch(TBL_WORKER_ACTIVITY, $data, 'id');
                if ($db_flag === false) {
                    echo 'FAIL';
                    write_log('Update fail_Query: '.$dbcity->last_query(), 'street_older_tour');
                } else {
                    echo 'OK_'.implode('|', $id_array);
                    write_log('Update successful_ID: '.implode(',', $id_array), 'street_older_tour');
                }
                echo "\n" . PHP_EOL;
            }
            write_log('End update street name for older tour', 'street_older_tour');
        }
    }

    function get_activity_by_idgps($db_connect = NULL,$id_wkgps = 0, $type= NULL){
        $db_connect->select('id,id_wkgps,id_activity,id_task_activity,min(starttime) as starttime,max(endtime) as endtime,latitude,longtitude,street,group_street');
        $db_connect->from(TBL_WORKER_ACTIVITY);
        $db_connect->where('deleted_at',NULL);
        $db_connect->where('id_wkgps',$id_wkgps);
        $db_connect->order_by('id','asc');
        $db_connect->group_by('id_wkgps,id_activity,id_task_activity,street,group_street');

        return $db_connect->get()->result_array();
    }

    function distance_gps($lat1 = 0, $lon1 = 0, $lat2 = 0, $lon2 = 0,$unit = 'm')
    {
        $distance = (3956 * acos(cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($lon2) - deg2rad($lon1)) + sin(deg2rad($lat1)) * sin(deg2rad($lat2))));
        switch ($unit)
        {
            case 'mi':  #miles
                return $distance;
                break;
            case 'km':  #kilometers
                return 1.609344 * $distance;
                break;
            case 'm':   #meters
                return 1.609344 * 1000 * $distance;
                break;
            case 'y':   #yards
                return 1760 * $distance;
                break;
            case 'ft':  #feet
                return 1760 * 3 * $distance;
                break;
            default:
                return $distance;
                break;
        }
    }
}