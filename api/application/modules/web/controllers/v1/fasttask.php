<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

/**
 * Fasttask controller
 *
 * @author Chien Tran <tran.duc.chien@kloon.vn>
 * @created 6 Apr 2015
 */
class Fasttask extends REST_Controller{
    private $_media_path = '';
    function __construct(){
        parent:: __construct();

        if (isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->load->model('infor_model','info');
            $this->_media_path = config_item('media_dir');
        } else{
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_FAIL,
                'msg'       => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    public function index_get() {
        $module  = $this->get('module_id');

        $day = $this->get('day');
        if (!is_bool($day)) {
            $this->db->where('created_at >=', date('Y-m-d H:i:s', strtotime($day.' 00:00:00')));
            $this->db->where('created_at <=', date('Y-m-d H:i:s', strtotime($day.' 23:59:59')));
        }

        $worker  = $this->get('worker');
        if (!is_bool($worker)) {
            $this->db->where('id_employee', $worker);
        }
        $this->db->where('deleted_at', null);
        $this->db->order_by('created_at', 'desc');
        $fasttask = $this->db->get(TBL_FASTTASK)->result_array();

        // informations
        $fasttask_id_array = array_column($fasttask, 'id');
        $informations = array();
        if (!empty($fasttask_id_array)) {
            $this->db->select('id_worker_gps, type, COUNT(id) as total');
            $this->db->from(TBL_WORKER_INFORMATION);
            $this->db->where_in('id_worker_gps', $fasttask_id_array);
            $this->db->order_by('id_worker_gps', 'asc');
            $this->db->group_by('id_worker_gps, type');
            $information_result = $this->db->get()->result_array();
            if (!empty($information_result)) {
                foreach ($information_result as $key => $value) {
                    $informations[get_value('id_worker_gps', $value) . '_' . get_value('type', $value)] = get_value('total', $value);
                }
            }
        }

        $workers = array();
        $worker_id_array = array_column($fasttask, 'id_employee');
        if (!empty($worker_id_array)) {
            $worker_result = $this->db->where_in('id', $worker_id_array)->get(TBL_WORKER)->result_array();
            if (!empty($worker_result)) {
                foreach ($worker_result as $key => $value) {
                    $workers[get_value('id', $value)] = $value;
                }
            }
        }

        foreach ($fasttask as $key => $value) {
            $id_employee = get_value('id_employee', $value);
            $tmp         = '';
            if (isset($workers[$id_employee])) {
                $tmp = trim($workers[$id_employee]['first_name'] . ' ' . $workers[$id_employee]['last_name']);
            }
            $fasttask[$key]['employee_name'] = $tmp;
            if (get_value('updated_at', $value) == '0000-00-00 00:00:00') {
                $fasttask[$key]['updated_at'] = null;
            }

            // Information
            $tmp = get_value('id', $value) . '_' . INFO_IMAGE;
            $fasttask[$key]['information']['image'] = isset($informations[$tmp]) ? $informations[$tmp] : 0;
            $tmp = get_value('id', $value) . '_' . INFO_VIDEO;
            $fasttask[$key]['information']['video'] = isset($informations[$tmp]) ? $informations[$tmp] : 0;
            $tmp = get_value('id', $value) . '_' . INFO_VOICE;
            $fasttask[$key]['information']['voice'] = isset($informations[$tmp]) ? $informations[$tmp] : 0;
            $tmp = get_value('id', $value) . '_' . INFO_MESSAGE;
            $fasttask[$key]['information']['message'] = isset($informations[$tmp]) ? $informations[$tmp] : 0;
        }

        $return = array(
            'version'      => config_item('api_version'),
            'status'       => STATUS_SUCCESS,
            // 'informations' => $informations,
            'data'         => $fasttask,
        );
        return  $this->response($return, REST_CODE_OK);
    }

    public function information_get() {
        $id_fasttask  = $this->input->get('id_fasttask');

        $fasttask = $this->db->get_where(TBL_FASTTASK, array('id' => $id_fasttask))->row_array();
        if (empty($fasttask)) {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'data'    => 'Fasttask not found',
            );
            return  $this->response($return, REST_CODE_OK);
        }

        $id_wk = get_value('id_employee', $fasttask);

        $this->db->where('id_worker_gps', $id_fasttask);
        // $this->db->limit(20);
        $informations = $this->db->get(TBL_WORKER_INFORMATION)->result_array();

        foreach ($informations as $key => $value) {
            $path_media  = $this->_media_path . $value['id_city'].'/'.date('Y/m/d',strtotime($value['time'])).'/'.$id_wk.'/'.$value['data'];
            $path_thumbs = config_item('images_link') . 'w450/' . $value['id_city'].'/'.date('Y/m/d',strtotime($value['time'])).'/'.$id_wk.'/'.$value['data'];

            switch ($value['type'])
            {
                case INFO_IMAGE:
                    $value['data']   = $path_media;
                    $value['thumbs'] = $path_thumbs;
                    break;

                case INFO_VIDEO:
                case INFO_VOICE:
                    $value['data']   = $path_media;
                    break;

                case INFO_MESSAGE:
                    break;

                default:
                    break;
            }
            $informations[$key] = $value;
        }

        $return = array(
            'version' => config_item('api_version'),
            'status'  => STATUS_SUCCESS,
            'data'    => $informations,
        );
        return  $this->response($return, REST_CODE_OK);
    }
}
