<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/controllers/REST_Controller.php';

class Employee_Group extends REST_Controller {
    private $_id_city = 0;

    function __construct() {
        parent::__construct();

        if (isset($this->_tenantId) && !empty($this->_tenantId))
            $this->_id_city = intval($this->_tenantId);
        else {
            $return = array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL,
                'msg' => 'tenantId not found !'
            );
            return $this->response($return, REST_CODE_PARAM_ERR);
        }
        $this->load->helper('pheanstalk');
    }

    public function index_get() {
        $id  = $this->get('id');
        $ids = array();

        $this->db->query("SET SESSION group_concat_max_len = 1000000;");

        $this->db->select(TBL_EMPLOYEE_GROUP . '.*' . ', GROUP_CONCAT(' . TBL_EMPLOYEE_GROUP_COST . '.id_cost_center) as id_cost_center');
        $this->db->join(TBL_EMPLOYEE_GROUP_COST, TBL_EMPLOYEE_GROUP . '.id = ' . TBL_EMPLOYEE_GROUP_COST . '.id_employee_group', 'left');
        empty($id) || ($ids = convert_param_array($id));
        empty($ids) || ($this->db->where_in(TBL_EMPLOYEE_GROUP . '.id', $ids));

        $title = $this->get('title');
        empty($title) || $this->db->where(TBL_EMPLOYEE_GROUP . '.title', $title);

        $this->db->where(TBL_EMPLOYEE_GROUP . '.deleted_at', null);
        $this->db->where(TBL_EMPLOYEE_GROUP_COST . '.deleted_at', null);
        $this->db->group_by(TBL_EMPLOYEE_GROUP_COST . '.id_employee_group');
        $employee_group = $this->db->get(TBL_EMPLOYEE_GROUP);

        if ($employee_group) {
            $employee_group = $employee_group->result_array();
            foreach ($employee_group as $key => $value) {
                $employee_group[$key]['id_cost_center'] && $employee_group[$key]['id_cost_center'] = explode(',', $employee_group[$key]['id_cost_center']);
            }

            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_SUCCESS,
                'data' => $employee_group
            ), REST_CODE_OK);
        } else {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }
    }

    public function index_post() {
        $id_cost_center = $this->post('id_cost_center');
        $title          = $this->post('title');

        if (empty($title)) {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }

        $data = array(
            'title' => $title,
            'created_at' => date('Y-m-d H:i:s')
        );

        if ($this->db->insert(TBL_EMPLOYEE_GROUP, $data)) {
            $id_employee_group = $this->db->insert_id();
            $id_cost_center ? ($id_cost_center = explode(',', $id_cost_center)) : ($id_cost_center = array());
            $id_cost_center = array_filter($id_cost_center);

            if ($id_cost_center) {
                $insert_batch = array();
                foreach ($id_cost_center as $p) {
                    $insert_batch[] = array(
                        'id_cost_center'    => intval($p),
                        'id_employee_group' => $id_employee_group,
                        'created_at'        => date('Y-m-d H:i:s')
                    );
                }
                $this->db->insert_batch(TBL_EMPLOYEE_GROUP_COST, $insert_batch);

                // Update assigned_at
                $this->db->where_in('id', $id_cost_center)->update(TBL_COST_CENTER, array('assigned' => date('Y-m-d H:i:s')));
            }

            // Push notification
            $job = array(
                'id_city'  => get_tenant_id(),
                'app'      => APP_FASTTASK_TEXT,
                'msg_key'  => 'server_update',
                'msg_body' => 'employee_group',
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_SUCCESS
            ), REST_CODE_OK);
        } else {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }
    }

    public function index_put() {
        $id    = intval($this->put('id'));
        $title = $this->put('title');

        if (!$title || !$id) {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }

        $data = array( 'updated_at' => date('Y-m-d H:i:s') );
        $title && ($data['title'] = $title);

        $this->db->where('id', $id);
        if ($this->db->update(TBL_EMPLOYEE_GROUP, $data)) {
            $id_cost_center = $this->put('id_cost_center');

            if ($id_cost_center !== false) {
                // cost_center list
                $employee_group_cost  = $this->db->select('id, id_cost_center')->get_where(TBL_EMPLOYEE_GROUP_COST, array('id_employee_group' => $id))->result_array();
                $id_cost_center_array = array_column($employee_group_cost, 'id_cost_center');

                // Update assigned cost_center
                if (is_array($id_cost_center_array) AND !empty($id_cost_center_array)) {
                    $this->db->where_in('id', $id_cost_center_array);
                    $this->db->update(TBL_COST_CENTER, array('assigned_at' => null));
                }

                // Remove from employee_group_cost
                $this->db->where('id_employee_group', $id)->delete(TBL_EMPLOYEE_GROUP_COST);

                $id_cost_center ? ($id_cost_center = explode(',', $id_cost_center)) : ($id_cost_center = array());
                $id_cost_center = array_filter($id_cost_center);

                // Mask cost_center assigned
                if (is_array($id_cost_center) AND !empty($id_cost_center)) {
                    $this->db->where_in('id', $id_cost_center);
                    $this->db->update(TBL_COST_CENTER, array('assigned_at' => date('Y-m-d H:i:s')));
                }

                // Insert to employee_group_cost
                $insert_batch = array();
                foreach ($id_cost_center as $p) {
                    $insert_batch[] = array(
                        'id_employee_group' => $id,
                        'id_cost_center'    => intval($p),
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s')
                    );
                }
                $this->db->insert_batch(TBL_EMPLOYEE_GROUP_COST, $insert_batch);
            }

            // Push notification
            $job = array(
                'id_city'  => get_tenant_id(),
                'app'      => APP_FASTTASK_TEXT,
                'msg_key'  => 'server_update',
                'msg_body' => 'employee_group',
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_SUCCESS
            ), REST_CODE_OK);
        } else {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL,
            ), REST_CODE_PARAM_ERR);
        }
    }

    public function index_delete() {
        $id = intval($this->delete('id'));

        if (empty($id)) {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }

        $db_flag = $this->db->where('id', $id)->update(TBL_EMPLOYEE_GROUP, array('deleted_at' => date('Y-m-d H:i:s') ));
        if ($db_flag) {
            // Delete from employee_group_cost
            $this->db->where('id_employee_group', $id)->delete(TBL_EMPLOYEE_GROUP_COST);

            // Push notification
            $job = array(
                'id_city'  => get_tenant_id(),
                'app'      => APP_FASTTASK_TEXT,
                'msg_key'  => 'server_update',
                'msg_body' => 'employee_group',
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_SUCCESS
            ), REST_CODE_OK);
        } else {
            return $this->response(array(
                'version' => config_item('api_version'),
                'status' => STATUS_FAIL
            ), REST_CODE_PARAM_ERR);
        }
    }
}
