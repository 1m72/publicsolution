<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-11-06
 * */
class Machine extends REST_Controller{
    private $_id_city = 0;
    function __construct(){
        parent:: __construct();

        if (isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->_id_city = intval($this->_tenantId);
            $this->load->model('machine_model','mc');
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /*
     * @description
     * @function : index_get
     * @author : huongpm<phung.manh.huong@kloon.vn>
     * @create : 2013-11-05
     * @route
     * */
    function index_get()
    {
        $id_module = $this->get('id_module');
        $id_machine = $this->get('id_machine');
        if(!$id_module){
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_FAIL,
                'msg'       => 'Id module fail'
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
        $limit = (intval($this->get('limit')) === 0)?100:intval($this->get('limit'));
        $offset = $this->get('offset');
        if (!isset($offset) || empty($offset))
            $offset = 0;
        # get machine module
        $this->db->where('deleted_at', NULL);
        $this->db->where('id_module', $id_module);
        $machine_model = $this->db->get(TBL_MACHINE_MODULE)->result_array();
        if(!isset($machine_model) OR empty($machine_model)){
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_FAIL,
                'msg'       => 'Worker module empty'
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
        $id_machine_select = array();
        foreach ($machine_model as $key => $val) {
            $id_machine_select[] = $val['id_machine'];
        }
        # get machine
        $this->db->where('deleted_at', NULL);
        $this->db->where_in('id', $id_machine_select);
        $this->db->order_by('id', 'asc');
        $this->db->limit($limit, $offset);
        $machine = $this->db->get(TBL_MACHINE)->result_array();
        if(isset($machine) AND !empty($machine)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
                'data'    => $machine
            );
            return $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_FAIL,
                'msg'       => 'Worker empty'
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function m_index_get()
    {
        $id_array_machine = array();
        $id_machine = $this->get('id_machine');
        if(isset($id_machine) && !empty($id_machine)){
            $id_array_machine = convert_param_array($id_machine);
        }

        $limit = (intval($this->get('limit')) === 0)?100:intval($this->get('limit'));
        $offset = $this->get('offset');
        if(!isset($offset) || empty($offset))
            $offset = 0;
        #
        if(isset($id_array_machine) && !empty($id_array_machine))
        {
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS,
                    'data'      => array()
            );
            #machine
            $this->db->where('deleted_at',null);
            $this->db->where('id_city',$this->_id_city);
            $this->db->where_in('id',$id_array_machine);
            $machine = $this->db->get(TBL_MACHINE)->result_array();
            if(isset($machine) && !empty($machine))
            {
                foreach ($machine as $it_machine)
                {
                    $item = array(
                            'machine'  =>array(),
                            'activity' =>array(),
                            'addon'    =>array()
                    );
                    $item['machine'] = $it_machine;
                    #
                    $id_machine_detail = intval($it_machine['id']);

                    $this->db->where('deleted_at',null);
                    $this->db->where('id_machine',$id_machine_detail);
                    $this->db->order_by('id','asc');
                    $activity_by_machine = $this->db->get(TBL_MACHINE_ACTIVITY)->result_array();

                    if(isset($activity_by_machine) && !empty($activity_by_machine))
                    {
                        foreach ($activity_by_machine as $ac_key=>$ac_value)
                        {
                            $item['activity'][] = $ac_value['id_activity'];
                            $item['addon'][]    = $ac_value['id_add_on'];
                        }
                    }
                    array_push($return['data'], $item);
                }
            }
            return $this->response($return,REST_CODE_OK);
        }else{
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }
    /*
     * @description
     * @function : _post
     * @author : huongpm<phung.manh.huong@kloon.vn>
     * @create : 2013-11-15
     * @route
     * */
    function index_post()
    {
        $identifier  = $this->post('identifier');
        $id_ref      = $this->post('id_ref');
        $id_city     = $this->post('id_city');
        $nfc_code    = $this->post('nfc_code');
        $name        = $this->post('name');
        $description = $this->post('description');
        $id_activity = $this->post('id_activity');
        $id_default  = $this->post('id_default');

        $data = array(
            'identifier'  => $identifier,
            'id_ref'      => $id_ref,
            'id_city'     => $id_city,
            'nfc_code'    => $nfc_code,
            'name'        => $name,
            'id_default'  => $id_default,
            'description' => $description,
            'created_at'  => date('Y-m-d H:i:s')
        );
        # insert machine
        if ($this->db->insert(TBL_MACHINE,$data)){
            $id_machine = $this->db->insert_id();

            # update nfc_code
            if (isset($nfc_code) && !empty($nfc_code)){
                $this->db->where('nfc_code',$nfc_code);
                $this->db->update(TBL_NFC,array('active'=>NFC_ACTIVE));
            }

            # activity
            if (isset($id_activity) && !empty($id_activity))
            {
                $array_activity = explode(',', $id_activity);
                $activity_data = array();
                foreach ($array_activity as $ac_item)
                {
                    $item = array(
                        'id_machine'  => $id_machine,
                        'id_activity' => $ac_item,
                        'id_add_on'   => 1,
                        'created_at'  => date('Y-m-d H:i:s')
                    );
                    array_push($activity_data, $item);
                }

                # insert activity
                if ($this->db->insert_batch(TBL_MACHINE_ACTIVITY,$activity_data)){
                    $return = array(
                            'version'   => config_item('api_version'),
                            'status'    => STATUS_SUCCESS
                    );
                    return $this->response($return,REST_CODE_OK);
                } else {
                    $return = array(
                            'version'   => config_item('api_version'),
                            'status'    => STATUS_FAIL
                    );
                    return $this->response($return,REST_CODE_PARAM_ERR);
                }
            } else {
                $return = array(
                        'version'   => config_item('api_version'),
                        'status'    => STATUS_FAIL
                );
                return $this->response($return,REST_CODE_PARAM_ERR);
            }
        } else {
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }
    /*
     * @description
     * @function : _put
     * @author : huongpm<phung.manh.huong@kloon.vn>
     * @create : 2013-11-15
     * @route
     * */
    function index_put()
    {
        $id          = $this->put('id');
        $identifier  = $this->put('identifier');
        $id_ref      = $this->put('id_ref');
        $id_city     = $this->put('id_city');
        $nfc_code    = $this->put('nfc_code');
        $name        = $this->put('name');
        $description = $this->put('description');
        $id_activity = $this->put('id_activity');
        $id_default  = $this->put('id_default');

        $data = array(
            'identifier'  => $identifier,
            'id_ref'      => $id_ref,
            'id_city'     => $id_city,
            'nfc_code'    => $nfc_code,
            'name'        => $name,
            'id_default'  => $id_default,
            'description' => $description,
            'updated_at'  => date('Y-m-d H:i:s')
        );

        #get nfc old
        $this->db->where('id',$id);
        $machine_detail = $this->db->get(TBL_MACHINE)->row_array();
        $_nfc_code = isset($machine_detail['nfc_code']) ? $machine_detail['nfc_code'] : '';

        # update machine
        $this->db->where('id',$id);
        if ($this->db->update(TBL_MACHINE,$data)){
            $this->load->helper('pheanstalk');
            $job = array(
                "id_city"       => $this->_id_city,
                "controllers"   => "machine",
                "type"          => "pull"
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            $this->db->where('id_machine',$id);
            $this->db->update(TBL_MACHINE_ACTIVITY,array('deleted_at'=>date('Y-m-d H:i:s')));

            # update nfc
            if (isset($_nfc_code) && !empty($_nfc_code)){
                $this->db->where('nfc_code',$_nfc_code);
                $this->db->update(TBL_NFC,array('active' => NFC_UNACTIVE,'updated_at' => date('Y-m-d H:i:s')));
            }
            if (isset($nfc_code) && !empty($nfc_code)){
                $this->db->where('nfc_code',$nfc_code);
                $this->db->update(TBL_NFC,array('active' => NFC_ACTIVE,'updated_at' => date('Y-m-d H:i:s')));
            }

            # activity
            if (isset($id_activity) && !empty($id_activity)){
                $array_activity = explode(',', $id_activity);
                $activity_data = array();
                foreach ($array_activity as $ac_item)
                {
                    $item = array(
                        'id_machine'  => $id,
                        'id_activity' => $ac_item,
                        'id_add_on'   => 1,
                        'created_at'  => date('Y-m-d H:i:s')
                    );
                    array_push($activity_data, $item);
                }
                if ($this->db->insert_batch(TBL_MACHINE_ACTIVITY,$activity_data)){
                    $return = array(
                        'version'   => config_item('api_version'),
                        'status'    => STATUS_SUCCESS
                    );
                    return $this->response($return,REST_CODE_OK);
                } else {
                    $return = array(
                        'version'  => config_item('api_version'),
                        'status'   => STATUS_FAIL
                    );
                    return $this->response($return,REST_CODE_PARAM_ERR);
                }
            } else {
                $return = array(
                        'version'  => config_item('api_version'),
                        'status'   => STATUS_FAIL
                );
                return $this->response($return,REST_CODE_PARAM_ERR);
            }
        }
        else{
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }
    /*
     * @description
     * @function : _delete
     * @author : huongpm<phung.manh.huong@kloon.vn>
     * @create : 2013-11-15
     * @route
     * */
    function index_delete()
    {
        $id = $this->delete('id');
        $this->db->where('id',$id);
        if ($this->db->update(TBL_MACHINE,array('deleted_at'=>date('Y-m-d H:i:s'))))
        {
            $this->load->helper('pheanstalk');
            $job = array(
                "id_city"       => $this->_id_city,
                "controllers"   => "machine",
                "type"          => "delete"
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            # apdate active nfc
            $this->db->where('id',$id);
            $machine_detail = $this->db->get(TBL_MACHINE)->row_array();
            $_nfc_code = isset($machine_detail['nfc_code']) ? $machine_detail['nfc_code'] : '';
            if (isset($_nfc_code) && !empty($_nfc_code)){
                $this->db->where('nfc_code',$_nfc_code);
                $this->db->update(TBL_NFC,array('active'=>NFC_UNACTIVE,'deleted_at'=>date('Y-m-d H:i:s')));
            }
            #return
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS
            );
            return $this->response($return,REST_CODE_OK);
        }
        else{
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /*
     * @description
     * @function : nfc_machine_post
     * @author : huongpm<phung.manh.huong@kloon.vn>
     * @create : 2013-11-05
     * @route
     * */
    function nfc_machine_post()
    {
        $machineid = $this->post('machine_id');
        $nfc_code  = $this->post('nfc_code');
        if ($this->mc->link_nfc_machine($machineid,$nfc_code))
        {
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_SUCCESS
            );
            return $this->response($return,REST_CODE_OK);
        }
        else
        {
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }
}