<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author : Le Thi Nhung(le.nhung@kloon.vn)
 * @creat  :  2014-08-27
 */
class Material extends REST_Controller{
    private $id_module  = 1;
    private $_id_city   = 1;
    function __construct(){
        parent:: __construct();
        if (isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->_id_city = intval($this->_tenantId);
        } else {
            $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }
/*
 * @author : Le Thi Nhung(le.nhung@kloon.vn)
 * @creat  :  2014-08-27
 */
    function index_get(){
        $id = $this->get('id');
        if ($id !== false) {
            $id = trim($id);
            $id = explode(',', $id);
        }

        $this->db->where('deleted_at', NULL);
        if(is_array($id) AND !empty($id)){
            $this->db->where_in('id', $id);
        }

        $data_material = $this->db->get(TBL_MATERIAL)->result_array();
        if(is_array($data_material) AND !empty($data_material)){
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_SUCCESS,
                'data'      => $data_material
            );
            return  $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Data empty !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }
/*
 * @author : Le Thi Nhung(le.nhung@kloon.vn)
 * @creat  :  2014-08-27
 */
    function index_post(){
        $this->load->helper('pheanstalk');
        $job = array(
            "id_city"       => $this->_id_city,
            "controllers"   => "material",
            "type"          => "post"
        );
        push_job(QUE_SERVER_UPDATE, json_encode($job));


        $name      = $this->post('name');
        $unit      = $this->post('unit');
        $data_insert = array(
            'name'         => $name,
            'unit'         => $unit,
            'created_at'    => date('Y-m-d H:i:s')
            );
        $flag = $this->db->insert(TBL_MATERIAL, $data_insert);
        if($flag != FALSE){
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS
            );
            return  $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }
/*
 * @author : Le Thi Nhung(le.nhung@kloon.vn)
 * @creat  :  2014-08-27
 */
    function index_put(){
        $this->load->helper('pheanstalk');
        $job = array(
            "id_city"       => $this->_id_city,
            "controllers"   => "material",
            "type"          => "put"
        );
        push_job(QUE_SERVER_UPDATE, json_encode($job));


        $id        = $this->put('id');
        $name      = $this->put('name');
        $unit      = $this->put('unit');

        if($name){
            $data_update['name'] = $name;
        }
        if($unit){
            $data_update['unit'] = $unit;
        }
        $data_update['updated_at']  = date('Y-m-d H:i:s');

        $this->db->where('id', $id);
        $flag = $this->db->update(TBL_MATERIAL, $data_update);
        if($flag != FALSE){
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS
            );
            return  $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }
/*
 * @author : Le Thi Nhung(le.nhung@kloon.vn)
 * @creat  :  2014-08-27
 */
    function index_delete(){
        $this->load->helper('pheanstalk');
        $job = array(
            "id_city"       => $this->_id_city,
            "controllers"   => "material",
            "type"          => "delete"
        );
        push_job(QUE_SERVER_UPDATE, json_encode($job));


        $id     = $this->delete('id');
        $this->db->where('id' , $id);
        $flag = $this->db->update(TBL_MATERIAL, array('deleted_at' => date('Y-m-d H:i:s')));
        if($flag != FALSE){
            # delete machine_material
            $this->db->where('id_material',$id);
            $this->db->update(TBL_MACHINE_MATERIAL,array('deleted_at' => date('Y-m-d H:i:s')));

            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS
            );
            return  $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }
}
