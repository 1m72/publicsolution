<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-11-04
 * */
class City extends REST_Controller
{
    private $_id_city = 0;
    function __construct(){
        parent:: __construct();

        if(isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->_id_city = intval($this->_tenantId);
            $this->load->model('city_model','city');
            $this->load->driver('cache');
        }else{
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL,
                    'msg'       => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /*
     * @description
     * @function : index_get
     * @author : huongpm<phung.manh.huong@kloon.vn>
     * @create : 2013-11-04
     * @update : 2013-11-11
     * @route
     * */
    function indexall_get()
    {
        $tenantId =  $this->_tenantId;
        if(isset($tenantId) && !empty($tenantId))
        {
            $data = $this->_db_global->get_where(TBL_CITY,array('deleted_at'=>NULL))->result_array();
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS,
                    'data'      => $data
            );
            return $this->response($return,REST_CODE_OK);
        }else{
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL,
                    'msg'       => ''
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function index_get()
    {
        $idarray = array();
        $id_city = trim($this->get('id_city'));
        if(isset($id_city) && !empty($id_city)){
            $idarray = convert_param_array($id_city);
        }

        $limit = (intval($this->get('limit')) === 0)?100:intval($this->get('limit'));
        $offset = $this->get('offset');
        if(!isset($offset) || empty($offset))
            $offset = 0;
        if(isset($idarray) && !empty($idarray))
        {
            $data = array();
            $city = $this->city->getcity($idarray,$limit,$offset);
            //return $this->response($city); die;
            foreach ($city as $item){
                if(isset($item['id_config']) && !empty($item['id_config'])){
                    $id_congig = isset($item['id_config'])?intval($item['id_config']) : 0;

                    $config = $this->_db_global->get_where(TBL_CITY_CONFIG,array('id'=>$id_congig))->row_array();
                    $dataitem =  $item;
                    #database
                    $db_config = json_decode($config["database"]);
                    $dataitem['cf_database']    = isset($config['database']) ? $config['database'] : '';
                    $dataitem['db_hostname']    = isset($db_config->hostname) ? $db_config->hostname : '';
                    $dataitem['db_database']    = isset($db_config->database) ? $db_config->database : '';
                    $dataitem['db_username']    = isset($db_config->username) ? $db_config->username : '';
                    $dataitem['db_password']    = isset($db_config->password) ? $db_config->password : '';
                    $dataitem['db_port']        = isset($db_config->port) ? $db_config->port : '';
                    #storage
                    $st_config = json_decode($config["storage"]);
                    $dataitem['cf_storage']     = isset($config['storage']) ? $config['storage'] : '';
                    $dataitem['st_type']        = isset($st_config->type) ? $st_config->type : '';
                    $dataitem['st_ip']          = isset($st_config->ip) ? $st_config->ip : '';
                    $dataitem['st_username']    = isset($st_config->username) ? $st_config->username : '';
                    $dataitem['st_password']    = isset($st_config->password) ? $st_config->password : '';
                    $dataitem['st_port']        = isset($st_config->port) ? $st_config->port : '';
                    $dataitem['st_home_dir']    = isset($st_config->home_dir) ? $st_config->home_dir : '';
                    $data[] = $dataitem;
                }
            }
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS,
                    'data'      => $data
            );
            return $this->response($return,REST_CODE_OK);
        }
        else
        {
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /*
     * @description
     * @function : index_post
     * @author : huongpm<phung.manh.huong@kloon.vn>
     * @create : 2013-11-04
     * @route
     * */
    function index_post()
    {
        $return = array(
                'version'  => config_item('api_version'),
                'status'   => STATUS_SUCCESS
        );
        try {
            $cityname = $this->post('cityname');
            $passcode = $this->post('passcode');

            $is_database = $this->post('database');
            $is_storage = $this->post('storage');
            # database
            $db_hostname = $this->post('database_hostname');
            $db_database = $this->post('database_database');
            $db_username = $this->post('database_username');
            $db_password = $this->post('database_password');
            $db_port = $this->post('database_port');
            $_database = json_encode(array(
                    'hostname'  => isset($db_hostname) ? $db_hostname : '',
                    'database'  => isset($db_database) ? $db_database : '',
                    'username'  => isset($db_username) ? $db_username : '',
                    'password'  => isset($db_password) ? $db_password : '',
                    'port'      => isset($db_port) ? $db_port : ''
            ));
            # storage
            $st_type = $this->post('storage_type');
            $st_ip = $this->post('storage_ip');
            $st_username = $this->post('storage_username');
            $st_password = $this->post('storage_password');
            $st_port = $this->post('storage_port');
            $st_home_dir = $this->post('storage_home_dir');
            $_storage = json_encode(array(
                    'type'      => isset($st_type) ? $st_type : '',
                    'ip'        => isset($st_ip) ? $st_ip : '',
                    'username'  => isset($st_username) ? $st_username : '',
                    'password'  => isset($st_password) ? $st_password : '',
                    'port'      => isset($st_port) ? $st_port : '',
                    'home_dir'  => isset($st_home_dir) ? $st_home_dir : ''
            ));
            # insert city
            $this->_db_global->insert(TBL_CITY,array(
                    'name'      => $cityname,
                    'passcode'  => $passcode,
                    'database'  => $is_database,
                    'storage'   => $is_storage,
                    'created_at'=> date('Y-m-d H:i:s')
            ));
            $id_city = $this->_db_global->insert_id();
            #insert
            if((isset($is_database) && !empty($is_database)) || (isset($is_storage) && !empty($is_storage)))
            {
                $dataput = array(
                        'id_city'   => $id_city,
                        'database'  => isset($_database) ? $_database : '',
                        'storage'   => isset($_storage) ? $_storage : '',
                        'created_at'    =>date('Y-m-d H:i:s')
                );
                $this->_db_global->insert(TBL_CITY_CONFIG,$dataput);
                # get id_config
                $id_config = $this->_db_global->insert_id();
                # Add memcache
                if(isset($is_database) && !empty($is_database) && $id_config > 0){
                    $array_config_connect = array();
                    $memcached_config_connect = $this->cache->memcached->get(MC_CITY_CONFIG);
                    if(isset($memcached_config_connect) && !empty($memcached_config_connect)){
                        $array_config_connect   = json_decode($memcached_config_connect,TRUE);

                        $array_config_connect['mc_'.$id_config] = array(
                            'id_city'   => $id_city,
                            'database'  => isset($_database) ? $_database : ''
                        );
                    } else{
                        # get city_config
                        $this->_db_global->where('deleted_at',NULL);
                        $this->_db_global->where('id !=',$id_config);
                        $result_config_connect = $this->_db_global->get(TBL_CITY_CONFIG)->result_array();

                        if(isset($result_config_connect) && !empty($result_config_connect)){
                            foreach ($result_config_connect as $key => $value) {
                                $json_config_connect = isset($value['database']) ? $value['database'] : '';

                                $array_config_connect['mc_'.$value['id']] = array(
                                    'id_city'   => isset($value['id_city']) ? $value['id_city'] : '',
                                    'database'  => $json_config_connect
                                );
                            }
                        }

                        $array_config_connect['mc_'.$id_config] = array(
                            'id_city'   => $id_city,
                            'database'  => isset($_database) ? $_database : ''
                        );
                    }
                    # add memcache
                    if(isset($array_config_connect) && !empty($array_config_connect)){
                        $this->cache->memcached->save(MC_CITY_CONFIG, json_encode($array_config_connect), 1000);
                    }
                }
                # update id_config in city
                $this->_db_global->where('id',$id_city);
                $this->_db_global->update(TBL_CITY,array('id_config'=>$id_config));
            }
        }
        catch (Exception $e)
        {
            $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_FAIL
            );
        }
        return  $this->response($return);
    }

    /*
     * @description
     * @function : index_put
     * @author : huongpm<phung.manh.huong@kloon.vn>
     * @create : 2013-11-04
     * @route
     * */
    function index_put()
    {
        $return = array(
            'version'  => config_item('api_version'),
            'status'   => STATUS_SUCCESS
        );
        try {
            $data_city    = array();
            $data_config  = array();
            $id_city      = $this->_id_city;
            $is_material  = $this->put('material');
            $is_selection = $this->put('selection');
            $city_name    = $this->put('city_name');
            $long         = $this->put('lon');
            $lat          = $this->put('lat');

            if(isset($long) && !empty($long) && isset($lat) && !empty($lat)){
                $data_city['lon']    = $long;
                $data_city['lat']    = $lat;
            }

            if ($is_material !== FALSE) {
                $data_city['material']   = $is_material;
            }

            if ($is_selection !== FALSE) {
                $data_city['selection']  = $is_selection;
            }

            if ($is_material !== FALSE || $is_selection !== FALSE) {
                $data_city['updated_at'] = date('Y-m-d H:i:s');
                $this->_db_global->where('id',$id_city);
            }

            if ($city_name !== FALSE) {
                $data_city['name'] = $city_name;
            }

            $city_detail = $this->_db_global->get_where(TBL_CITY,array('deleted_at'=>NULL,'id'=>$id_city))->row_array();
            if(isset($city_detail['id_config']) && !empty($city_detail['id_config'])){
                $id_config = intval($city_detail['id_config']);

                $cityname    = $this->put('cityname');
                $passcode    = $this->put('passcode');
                $is_database = $this->put('database');
                $is_storage  = $this->put('storage');

                if((isset($cityname) && !empty($cityname)) && (isset($passcode) && !empty($passcode))){
                    $data_city['name'] = $cityname;
                    $data_city['passcode']  = $passcode;
                }

                # database
                $db_hostname = $this->put('database_hostname');
                $db_database = $this->put('database_database');
                $db_username = $this->put('database_username');
                $db_password = $this->put('database_password');
                $db_port = $this->put('database_port');
                $_database = json_encode(
                    array(
                        'hostname'  => isset($db_hostname) ? $db_hostname : '',
                        'database'  => isset($db_database) ? $db_database : '',
                        'username'  => isset($db_username) ? $db_username : '',
                        'password'  => isset($db_password) ? $db_password : '',
                        'port'      => isset($db_port) ? $db_port : ''
                    )
                );
                if(isset($db_hostname) && !empty($db_hostname)){
                    $data_city['database']   = isset($is_database) ? $is_database : '';
                    $data_config['database'] = $_database;
                }

                # storage
                $st_type     = $this->put('storage_type');
                $st_ip       = $this->put('storage_ip');
                $st_username = $this->put('storage_username');
                $st_password = $this->put('storage_password');
                $st_port     = $this->put('storage_port');
                $st_home_dir = $this->put('storage_home_dir');
                $_storage = json_encode(
                    array(
                        'type'      => isset($st_type) ? $st_type : '',
                        'ip'        => isset($st_ip) ? $st_ip : '',
                        'username'  => isset($st_username) ? $st_username : '',
                        'password'  => isset($st_password) ? $st_password : '',
                        'port'      => isset($st_port) ? $st_port : '',
                        'home_dir'  => isset($st_home_dir) ? $st_home_dir : ''
                    )
                );
                if(isset($st_ip) && !empty($st_ip)){
                    $data_city['storage'] = isset($is_storage) ? $is_storage : '';
                    $data_config['storage'] = $_storage;
                }

                # update city
                if(isset($data_city) && !empty($data_city)){
                    $data_city['updated_at'] = date('Y-m-d H:i:s');
                    $this->_db_global->where('id',$id_city);
                    $this->_db_global->update(TBL_CITY,$data_city);

                    $this->load->helper('pheanstalk');
                    $job = array(
                        "id_city" => $this->_id_city,
                        "method"  => "city",
                        "type"    => "put"
                    );
                    push_job(QUE_SERVER_UPDATE, json_encode($job));
                }

                # config
                if(isset($data_config) && !empty($data_config)){
                    $data_config['updated_at'] = date('Y-m-d H:i:s');
                    $this->_db_global->where('id',$id_config);
                    if($this->_db_global->update(TBL_CITY_CONFIG,$data_config)){
                        # memcache
                        $array_config_connect = array();
                        $memcached_config_connect = $this->cache->memcached->get(MC_CITY_CONFIG);
                        if(isset($memcached_config_connect) && !empty($memcached_config_connect)){
                            $array_config_connect   = json_decode($memcached_config_connect,TRUE);
                            $array_config_connect['mc_'.$id_config] = array(
                                'id_city'   => $id_city,
                                'database'  => isset($_database) ? $_database : ''
                            );
                        } else {
                            # get city_config
                            $this->_db_global->where('deleted_at',NULL);
                            $result_config_connect = $this->_db_global->get(TBL_CITY_CONFIG)->result_array();
                            if(isset($result_config_connect) && !empty($result_config_connect)){
                                foreach ($result_config_connect as $key => $value) {
                                    $json_config_connect = isset($value['database']) ? $value['database'] : '';
                                    $array_config_connect['mc_'.$value['id']] = array(
                                        'id_city'   => isset($value['id_city']) ? $value['id_city'] : '',
                                        'database'  => $json_config_connect
                                    );
                                }
                            }
                        }

                        # add memcache
                        if(isset($array_config_connect) && !empty($array_config_connect)){
                            $this->cache->memcached->save(MC_CITY_CONFIG, json_encode($array_config_connect), 1000);
                        }
                    }
                }
            } else{
                $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_FAIL
                );
            }
        } catch (Exception $e) {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'source'  => 'exception'
            );
            write_log(json_encode($e), 'exception');
        }
        return  $this->response($return);
    }

    /*
     * @description
     * @function : index_delete
     * @author : huongpm<phung.manh.huong@kloon.vn>
     * @create : 2013-11-04
     * @route
     * */
    function index_delete()
    {
        $idcity = intval($this->delete('id_city'));
        if($this->city->update(TBL_CITY,array('id'=>$idcity),array('deleted_at'=>date('Y-m-d H:i:s'))))
        {
            $id_config = 0;
            $this->_db_global->where('id',$idcity);
            $city_detail = $this->_db_global->get(TBL_CITY)->row_array();
            $id_config   = isset($city_detail['id_config']) ? $city_detail['id_config'] : '';

            if($this->city->update(TBL_CITY_CONFIG,array('id'=>$id_config),array('deleted_at'=>date('Y-m-d H:i:s'))))
            {
                # memcached
                $array_config_connect = array();
                $memcached_config_connect = $this->cache->memcached->get(MC_CITY_CONFIG);
                if(isset($memcached_config_connect) && !empty($memcached_config_connect)){
                    unset($array_config_connect['mc_'.$id_config]);
                } else {
                    # get city_config
                    $this->_db_global->where('deleted_at',NULL);
                    $result_config_connect = $this->_db_global->get(TBL_CITY_CONFIG)->result_array();
                    if(isset($result_config_connect) && !empty($result_config_connect)){
                        foreach ($result_config_connect as $key => $value) {
                            $json_config_connect = isset($value['database']) ? $value['database'] : '';
                            $array_config_connect['mc_'.$value['id']] = array(
                                'id_city'   => isset($value['id_city']) ? $value['id_city'] : '',
                                'database'  => $json_config_connect
                            );
                        }
                    }
                }
                # add memcache
                if(isset($array_config_connect) && !empty($array_config_connect)){
                    $this->cache->memcached->save(MC_CITY_CONFIG, json_encode($array_config_connect), 1000);
                }

                $return = array(
                        'version'  => config_item('api_version'),
                        'status'   => STATUS_SUCCESS
                );
                return  $this->response($return,REST_CODE_OK);
            }
            else
            {
                $return = array(
                        'version'  => config_item('api_version'),
                        'status'   => STATUS_FAIL
                );
                return  $this->response($return,REST_CODE_PARAM_ERR);
            }
        }
        else
        {
            $return = array(
                    'version'  => config_item('api_version'),
                    'status'   => STATUS_FAIL
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /*
     * @description
     * @function : connect_get
     * @author : Le Thi Nhung(le.nhung@kloon.vn)
     * @create : 2013-12-20
     * @route
     * */
    function connect_post() {
        # List config
        $connect_info = array(
            'hostname'=>$this->post('hostname'),
            'username'=>$this->post('username'),
            'password'=>$this->post('password'),
            'database'=>$this->post('database'),
            'port'    =>$this->post('port'),
        );
        # DB config
        $db_config = array(
            'hostname' => isset($connect_info['hostname']) ? $connect_info['hostname'] : '',
            'username' => isset($connect_info['username']) ? $connect_info['username'] : '',
            'password' => isset($connect_info['password']) ? $connect_info['password'] : '',
            'database' => isset($connect_info['database']) ? $connect_info['database'] : '',
            'dbprefix' => isset($connect_info['dbprefix']) ? $connect_info['dbprefix'] : config_item('db_prefix'),
            'port'     => isset($connect_info['port'])     ? $connect_info['port']     : config_item('db_port'),
            'dbdriver' => config_item('db_dbdriver'),
            'pconnect' => config_item('db_pconnect'),
            'db_debug' => config_item('db_db_debug'),
            'cache_on' => config_item('db_cache_on'),
            'cachedir' => config_item('db_cachedir'),
            'char_set' => config_item('db_char_set'),
            'dbcollat' => config_item('db_dbcollat'),
        );

        $db_obj=$this->load->database($db_config, TRUE);
        if($db_obj->conn_id) {
           $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_SUCCESS
            );
            return $this->response($return,REST_CODE_OK);
        }else{
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function service_put(){
        $url      = $this->put('url');
        $dir_path = $this->put('dir_path');
        if(!$url OR !$dir_path){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'url or dir_path empty'
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        }
        # update
        $update = array(
            'url'      => $url,
            'dir_path' => $dir_path
        );

        $this->_db_global->where('id', $this->_id_city);
        $flag = $this->_db_global->update(TBL_CITY, array('post_service' => json_encode($update)));
        if($flag != TRUE){
            $this->load->helper('pheanstalk');
            $job = array(
                "id_city" => $this->_id_city,
                "method"  => "service",
                "type"    => "put"
            );
            push_job(QUE_SERVER_UPDATE, json_encode($job));

            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'update error'
            );
            return $this->response($return,REST_CODE_PARAM_ERR);
        } else {
            $return = array(
                'version'   => config_item('api_version'),
                'status'    => STATUS_SUCCESS
            );
            return $this->response($return,REST_CODE_OK);
        }
    }
}
