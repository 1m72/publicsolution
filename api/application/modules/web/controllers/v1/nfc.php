<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-11-15
 * */
class Nfc extends REST_Controller{
	private $_id_city = 0;
	function __construct(){
		parent:: __construct();

		if(isset($this->_tenantId) && !empty($this->_tenantId)){
			$this->load->model('nfc_model','nfc');
			$this->_id_city = intval($this->_tenantId);
		}else{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function index_get()
	{
		# nfc detail
		$id_array_nfc = array();
		$id_nfc = $this->get('id');
		if(isset($id_nfc) AND !empty($id_nfc)){
			$id_array_nfc = explode(',', $id_nfc);
		}

		$this->db->where('deleted_at', NULL);
		if(is_array($id_array_nfc) AND !empty($id_array_nfc)){
			$this->db->where_in('id', $id_array_nfc);
		}

		// status
		$active = $this->get('active');
		if ($active !== false) {
			$this->db->where('active', $active);
		}

		$nfc = $this->db->get(TBL_NFC)->result_array();

		if(isset($nfc) AND !empty($nfc) AND is_array($nfc)){
			$return = array(
				'version'  	=> config_item('api_version'),
				'status'   	=> STATUS_SUCCESS,
				'data'		=> (count($nfc) == 1) ? $nfc[0] : $nfc
			);
			return $this->response($return,REST_CODE_OK);
		} else {
			$return = array(
				'version'  	=> config_item('api_version'),
				'status'   	=> STATUS_FAIL,
				'msg'		=> 'Data nfc empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function nfc_add_post(){
		$total = $this->post('total');
		$id_city = $this->_id_city;
		$nfc_add = array();
		$this->_db_global->where('is_used', 0);
		$this->_db_global->order_by('id', 'asc');
		$this->_db_global->limit($total);
		$nfc_add = $this->_db_global->get(TBL_NFC)->result_array();
		if(!is_array($nfc_add) OR empty($nfc_add)){
			$return = array(
				'version'  	=> config_item('api_version'),
				'status'   	=> STATUS_FAIL,
				'msg'		=> 'Data nfc empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		#
		$update = array();
		$insert = array();
		foreach ($nfc_add as $key => $value) {
			$id_nfc = isset($value['id']) ? $value['id'] : '';
			$insert[] = array(
				'code'		=> 'NFC ' . $id_nfc,
				'nfc_code'	=> isset($value['nfc_code']) ? $value['nfc_code'] : '',
				'created_at'=> date('Y-m-d H:i:s')
			);
			$update[] = array(
				'id'	  => isset($value['id']) ? $value['id'] : '',
				'id_city' => $this->_id_city,
				'is_used' => 1
			);
		}
		$flag = $this->db->insert_batch(TBL_NFC, $insert);
		if($flag === true){
			$flag = $this->_db_global->update_batch(TBL_NFC, $update, 'id');
			if($flag !== false){
				$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_SUCCESS
				);
				return $this->response($return,REST_CODE_OK);
			} else {
				$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL,
					'msg'		=> 'Update nfc fail !'
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}
		} else {
			$return = array(
				'version'  	=> config_item('api_version'),
				'status'   	=> STATUS_FAIL,
				'msg'		=> 'Insert new nfc fail !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function index_put()
	{
		$id = $this->put('id');
		$companyid = $this->put('id_company');
		$code = $this->put('code');

		$this->db->where('id',$id);
		if($this->db->update(
				TBL_NFC,
				array(
						'id_company'	=> $companyid,
						'code'			=>$code,
						'updated_at'	=> date('Y-m-d H:i:s')
				)
		)){
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_SUCCESS
			);
			return $this->response($return,REST_CODE_OK);
		}
		else
		{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function index_delete()
	{
		$id = $this->put('id');
		$this->db->where('id',$id);
		if($this->db->update(TBL_NFC,array('deleted_at'=> date('Y-m-d H:i:s'))))
		{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_SUCCESS
			);
			return $this->response($return,REST_CODE_OK);
		}
		else
		{
			$return = array(
					'version'  	=> config_item('api_version'),
					'status'   	=> STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function test2_get(){
		$data = explode(";", file_get_contents("C:Users\\Admin\\Downloads\\sql-template.sql"));
		foreach ($data as $item)
		{
			$this->db->query($item);
		}
		return $this->response("ok");
	}
}
?>