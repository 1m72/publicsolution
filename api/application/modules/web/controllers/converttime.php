<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
/*
 * @author : Le Thi Nhung(le.nhung@kloon.vn)
 * @creat  :  2014-03-06
 */
class Converttime extends REST_Controller
{
    function __construct()
    {
        parent::__construct();

        if(isset($this->_tenantId) && !empty($this->_tenantId))
            $this->load->model('converttime_model','converttime');
        else{
            $return = array(
                    'version'   => config_item('api_version'),
                    'status'    => STATUS_FAIL,
                    'msg'       => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        $this->db = $this->_db_global;
    }
    /*
    * @description: convert all field datetime to gmt
    * @function   : index_put (route key in route config)
    * @author     : Le Thi Nhung (le.nhung@kloon.vn)
    * @create     : 2014-03-06
    * @route      : web/converttime/index
    * @param      :
    */
    function index_put()
    {
        $hour          = $this->put('hour');
        $this->converttime->converttime_update($hour);
        $return = array(
            'version'  => config_item('api_version'),
            'status'   => STATUS_SUCCESS
        );
        return  $this->response($return);
    }
    function index1_put1()
    {
        $table         = $this->put('table');
        $field_select  = $this->put('field_select');
        $this->converttime->converttime_update($table,$field_select);
        $return = array(
            'version'  => config_item('api_version'),
            'status'   => STATUS_SUCCESS
        );
        return  $this->response($return);
    }
}
?>