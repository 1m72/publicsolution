<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    # geo treet name activity by id_gps
    function geo_street_name_by_gps($id_city = NULL,$id_gps = NULL){
        write_log('Geo street name Start', '_geo_street_name_by_gps');
        if(!isset($id_city) OR empty($id_city)){
            write_log('Id city empty', '_geo_street_name_by_gps');
            return;
        }
        if(!isset($id_gps) OR empty($id_gps)){
            write_log('Id gps empty', '_geo_street_name_by_gps');
            return;
        }
        write_log('Geo street name by id_city : ' . $id_city . ' - id_gps : ' . $id_gps, '_geo_street_name_by_gps');
        # connect
        $db_connect = connect_db(false, $id_city);
        if ($db_connect === false) {
            write_log('Connect fail '.json_encode($db_connect), '_geo_street_name_by_gps');
            return false;
        }
        $activities = array();
        $db_connect->where('deleted_at' , NULL);
        $db_connect->where('street'     , '');
        $db_connect->where('id_wkgps'   , $id_gps);
        $activities = $db_connect->get(TBL_WORKER_ACTIVITY)->result_array();
        if(!is_array($activities) OR empty($activities)){
            write_log('Data table ' . TBL_WORKER_ACTIVITY . ' empty', '_geo_street_name_by_gps');
            return false;
        }
        $model_street = array();
        foreach ($activities as $key => $val) {
            $lat = $val['latitude'];
            $lon = $val['longtitude'];
            $street = $this->get_street_name($lat,$lon);
            $model_street[] = array(
                'id'        => $val['id'],
                'street'    => $street
            );
        }
        if(is_array($model_street) AND !empty($model_street)){
            $flag = $db_connect->update_batch(TBL_WORKER_ACTIVITY,$model_street,'id');
            if($flag !== FALSE){
                write_log('Geo street name OK - Data : ' . json_encode($model_street), '_geo_street_name_by_gps');
            } else {
                write_log('Geo street name FAIL - Data : ' . json_encode($model_street), '_geo_street_name_by_gps');
            }
        } else {
            write_log('Data update empty', '_geo_street_name_by_gps');
        }
    }

    function get_street_name($lat = NULL, $lon = NULL, $service = 'kloon'){
        $services = array(
            'kloon'         => 'http://nominatim.kloon.net/reverse?format=json&zoom=17&addressdetails=1',
            'mapquest'      => 'http://open.mapquestapi.com/nominatim/v1/reverse.php?format=json&zoom=17',
            'openstreetmap' => 'http://nominatim.openstreetmap.org/reverse?format=json&zoom=17&addressdetails=1',
            'gisgraphy'     => 'http://services.gisgraphy.com/street/streetsearch?format=json&from=1&to=1',
        );

        if (!isset($services[$service])) {
            $service = $services['mapquest'];
        } else {
            $service = $services[$service];
        }

        $street = '';
        if ((isset($latitude) && !empty($latitude)) && (isset($longtitude) && !empty($longtitude))) {
            $service_url    = '';
            $service_type   = $service;
            $count  = 0;
            $status = FALSE;
            while ( ($count < 3) AND ($status != TRUE) ) {
                switch ($count) {
                    case 1:
                        $service_type   = 'mapquest';
                        $service_url = $services['mapquest'] . '&lat=' . $latitude . '&lon=' . $longtitude;
                        break;

                    case 2:
                        $service_type   = 'openstreetmap';
                        $service_url = $services['openstreetmap'] . '&lat=' . $latitude . '&lon=' . $longtitude;
                        break;

                    default:
                        $service_type   = 'kloon';
                        $service_url = $services['kloon'] . '&lat=' . $latitude . '&lon=' . $longtitude;
                        break;
                }
                $street = $this->curl_read_content($service_url, $service_type);

                # Logs
                write_log("{$latitude} {$longtitude} {$street}", 'resolve_street_name');

                if( (isset($street) AND !empty($street)) OR ($count >= 3) ){
                    $status = TRUE;
                }
                $count++;

                usleep(100);
            }
        }
        return $street;
    }

    function curl_read_content($link = FALSE, $service = 'kloon', $timeout = 2500){
        $street = '';
        if($link != FALSE){
            $street_data = '';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $link);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            #curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 2000);
            $street_data = curl_exec($ch);
            curl_close($ch);

            $street_array = json_decode($street_data,true);
            if (isset($street_array['address']) && !empty($street_array['address'])) {
                $street_detail = $street_array['address'];
                if(isset($street_detail['road']) && !empty($street_detail['road'])){
                    $street_name = isset($street_detail['road']) ? $street_detail['road'] : '';
                    $street      = $street_name;
                }
            }
        }
        return $street;
    }

    function test(){
        echo (strtotime('2014-03-18 14:29:52') - strtotime('2014-03-18 13:54:00'));
    }
}