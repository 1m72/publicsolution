<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
class Worktime extends REST_Controller{
    private $id_module  = 1;
    private $_id_city   = 1;
    function __construct(){
        parent:: __construct();
        if (isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->_id_city = intval($this->_tenantId);
        } else {
            $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function index_get(){
        $id = $this->get('id');
        $group_id = $this->get('group_id');
        $worker_id = $this->get('worker_id');
        $type = $this->get('type');

        if($type == false){
            $type = 0;
        }

        $starttime = $this->get('starttime');
        $endtime = $this->get('endtime');

        $where_in_group    = array();
        $where_in_worker   = array();
        $where_in_worktime = array();

        if($id == false){
            if(!$starttime OR !$endtime){
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'Starttime or endtime fail!'
                );
                return  $this->response($return,REST_CODE_PARAM_ERR);
            }

            $is_all = TRUE;

            if($group_id != false AND !is_array($group_id) AND !empty($group_id)){
                $group_id = explode(',', $group_id);
                $is_all = FALSE;
            }

            if(is_array($group_id) AND !empty($group_id)){
                foreach ($group_id as $key => $value) {
                    $where_in_group[] = $value;
                }
            }

            if($worker_id != false AND !is_array($worker_id) AND !empty($worker_id)){
                $worker_id = explode(',', $worker_id);
                $is_all = FALSE;
            }

            if(is_array($worker_id) AND !empty($worker_id)){
                foreach ($worker_id as $key => $value) {
                    $where_in_worker[] = $value;
                }
            }

            $this->db->where('deleted_at', NULL);
            $this->db->where('type', $type);
            $this->db->where('starttime >=', date('Y-m-d H:i:s', strtotime($starttime)));
            $this->db->where('endtime <=', date('Y-m-d H:i:s', strtotime($endtime)));
            
            $where = '';
            if(is_array($where_in_group) AND !empty($where_in_group)){
                if(!isset($where) OR empty($where)){
                    $where = 'id_group in ('. implode(',', $where_in_group) . ')';
                } else {
                    $where .= ' OR id_group in ('. implode(',', $where_in_group) . ')';
                }

            }

            if(is_array($where_in_worker) AND !empty($where_in_worker)){
                if(!isset($where) OR empty($where)){
                    $where = 'id_worker in ('. implode(',', $where_in_worker) . ')';
                } else {
                    $where .= ' OR id_worker in ('. implode(',', $where_in_worker) . ')';
                }

            }
            
             // var_dump($group_id);
             // echo $where; die;

            if(isset($where) AND !empty($where)){
                $this->db->where('(' . $where . ')');
            }

            $worktime_detail = $this->db->get(TBL_WORKTIME_DETAIL)->result_array();

            if($is_all == FALSE AND (!isset($worktime_detail) OR empty($worktime_detail))){
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'Param group_id and worker_id fail!'
                );
                return  $this->response($return,REST_CODE_PARAM_ERR);
            }

            if(isset($worktime_detail) AND !empty($worktime_detail)){
                foreach ($worktime_detail as $key => $value) {
                    if(isset($value['id_worktime']) AND !in_array($value['id_worktime'], $where_in_worktime)){
                        $where_in_worktime[] = $value['id_worktime'];
                    }
                }
            }
            //print_r($where_in_worktime); die;
            //echo $this->db->last_query(); die;
        }

        #
        if($id != false){
            $this->db->where('id', $id);
        } else {
            if(is_array($where_in_worktime) AND !empty($where_in_worktime)){
                $this->db->where_in('id', $where_in_worktime);
            } else {
                $this->db->where('deleted_at', NULL);
                $this->db->where('type', $type);
                $this->db->where('starttime >=', date('Y-m-d H:i:s', strtotime($starttime)));
                $this->db->where('endtime <=', date('Y-m-d H:i:s', strtotime($endtime)));
            }
        }
        $worktime = $this->db->get(TBL_WORKTIME)->result_array();

        if( !isset($worktime) AND !is_array($worktime) OR empty($worktime)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Worktime empty'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        $id_worktime = array();

        foreach ($worktime as $key => $value) {
            $id_worktime[] = $value['id'];
        }

        # get detail

        $this->db->where('deleted_at', NULL);
        $this->db->where_in('id_worktime', $id_worktime);
        $worktime_detail = $this->db->get(TBL_WORKTIME_DETAIL)->result_array();

        if(!is_array($worktime_detail) OR empty($worktime_detail)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Worktime detail empty'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        # get worker

        $worker = array();
        $worker_result = array();
        $this->db->where('deleted_at', NULL);
        $worker_result = $this->db->get(TBL_WORKER)->result_array();
        if(is_array($worker_result) AND !empty($worker_result)){
            foreach ($worker_result as $key => $value) {
                $worker[$value['id']] = array(
                    'id'         => $value['id'],
                    'first_name' => $value['first_name'],
                    'last_name'  => $value['last_name']
                );
            }
        }

        # get group

        $group = array();
        $group_result = array();
        $this->db->where('deleted_at', NULL);
        $group_result = $this->db->get(TBL_GROUP)->result_array();
        if(is_array($group_result) AND !empty($group_result)){
            foreach ($group_result as $key => $value) {
                $group[$value['id']] = array(
                    'id'   => $value['id'],
                    'name' => $value['name']
                );
            }
        }

        # return
        
        $data = array();

        foreach ($worktime as $key => $value) {
            $_id_worktime = $value['id'];
            $id_worker = array();
            $id_group  = array();

            // print_r($worktime_detail);
            // die('1111111111');

            foreach ($worktime_detail as $_key => $_value) {
                if($_value['id_worktime'] == $_id_worktime AND isset($_value['id_group']) AND (intval($_value['id_group']) > 0) ){
                    if( isset($group[$_value['id_group']]) AND !empty($group[$_value['id_group']]) AND !in_array($group[$_value['id_group']], $id_group) ){
                        $id_group[] = isset($group[$_value['id_group']]) ? $group[$_value['id_group']] : false;
                    }
                    unset($worktime_detail[$_key]);
                } else{
                    if($_value['id_worktime'] == $_id_worktime AND isset($_value['id_worker']) AND intval($_value['id_worker']) > 0){
                        if(isset($worker[$_value['id_worker']]) AND !empty($worker[$_value['id_worker']])){
                            $id_worker[] = isset($worker[$_value['id_worker']]) ? $worker[$_value['id_worker']] : false;
                        }
                        unset($worktime_detail[$_key]);
                    }
                }
            }

            $edit = TRUE;
            if((strtotime($value['starttime']) - 5*24*60*60) < strtotime(date('Y-m-d H:i:s'))){
                $edit = false;
            }

            if((isset($id_group) AND !empty($id_group)) OR (isset($id_worker) AND !empty($id_worker))){
                $data[] = array(
                    'id'        => $_id_worktime,
                    'title'     => isset($value['title']) ? $value['title'] :'',
                    'des'       => isset($value['des']) ? $value['des'] :'',
                    'starttime' => isset($value['starttime']) ? $value['starttime'] :'',
                    'endtime'   => isset($value['endtime']) ? $value['endtime'] :'',
                    'type'      => isset($value['type']) ? $value['type'] :'',
                    'id_group'  => $id_group,
                    'id_worker' => $id_worker,
                    'edit'      => $edit
                );
            } else {
                unset($worktime[$key]);
            }
        }

        if(isset($data) AND !empty($data)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
                'data'    => $data
            );
            return  $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Data worktime empty'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    /*
    $datetime = new DateTime(date('Y-m-d H:i:s'));
    echo $datetime->format('Y-m-d H:i:s') . "\n";

    $fi_time = new DateTimeZone('UTC+2');
    $la_time = new DateTimeZone('UTC');

    



    $datetime->setTimezone($la_time);
    echo $datetime->format('Y-m-d H:i:s');
    die;

    $id = $this->get('id');
    $timezone = $this->get('timezone');

    if($timezone == false){
        $timezone = 'UTC';
    }
    */

    function index_v2_get(){
        $id = $this->get('id');
        $group_id = $this->get('group_id');
        $worker_id = $this->get('worker_id');
        $type = $this->get('type');
        $starttime = $this->get('starttime');
        $endtime = $this->get('endtime');
        if($id == false){
            if(!$starttime OR !$endtime){
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'Starttime or endtime fail!'
                );
                return  $this->response($return,REST_CODE_PARAM_ERR);
            }
        }

        $where_in_group    = array();
        $where_in_worker   = array();
        $where_in_worktime = array();
        //
        if($id == false){
            if(!is_array($group_id) AND !empty($group_id)){
                $group_id = explode(',', $group_id);
            }

            if(is_array($group_id) AND !empty($group_id)){
                foreach ($group_id as $key => $value) {
                    $where_in_group[] = $value;
                }
            }

            if(!is_array($worker_id) AND !empty($worker_id)){
                $worker_id = explode(',', $worker_id);
            }

            if(is_array($worker_id) AND !empty($worker_id)){
                foreach ($worker_id as $key => $value) {
                    $where_in_worker[] = $value;
                }
            }
        }

        if( (is_array($where_in_group) AND !empty($where_in_group)) OR (is_array($where_in_worker) AND !empty($where_in_worker)) ){
            # get id worktime
            $this->db->where('deleted_at', NULL);
        }

        # get worktime
        $this->db->where('deleted_at', NULL);
        if($type){
            $this->db->where('type', $type);
        } else {
            $this->db->where('type', 0);
        }
        if(!$id){
            $this->db->where('starttime >=', date('Y-m-d H:i:s', strtotime($starttime)));
            $this->db->where('endtime <=', date('Y-m-d H:i:s', strtotime($endtime)));
        } else {
            $this->db->where('id', $id);
        }
        $worktime = $this->db->get(TBL_WORKTIME)->result_array();
        echo $this->db->last_query();die;
        if(!is_array($worktime) OR empty($worktime)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Worktime empty'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
        $id_worktime = array();
        foreach ($worktime as $key => $value) {
            $id_worktime[] = $value['id'];
        }
        # get detail
        $this->db->where('deleted_at', NULL);
        $this->db->where_in('id_worktime', $id_worktime);
        $worktime_detail = $this->db->get(TBL_WORKTIME_DETAIL)->result_array();
        if(!is_array($worktime_detail) OR empty($worktime_detail)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Worktime detail empty'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
        # get worker
        $worker = array();
        $worker_result = array();
        $this->db->where('deleted_at', NULL);
        $worker_result = $this->db->get(TBL_WORKER)->result_array();
        if(is_array($worker_result) AND !empty($worker_result)){
            foreach ($worker_result as $key => $value) {
                $worker[$value['id']] = array(
                    'id'         => $value['id'],
                    'first_name' => $value['first_name'],
                    'last_name'  => $value['last_name']
                );
            }
        }
        # get group
        $group = array();
        $group_result = array();
        $this->db->where('deleted_at', NULL);
        $group_result = $this->db->get(TBL_GROUP)->result_array();
        if(is_array($group_result) AND !empty($group_result)){
            foreach ($group_result as $key => $value) {
                $group[$value['id']] = array(
                    'id'   => $value['id'],
                    'name' => $value['name']
                );
            }
        }
        # return
        $data = array();
        foreach ($worktime as $key => $value) {
            $_id_worktime = $value['id'];
            $id_worker = array();
            $id_group  = array();
            foreach ($worktime_detail as $_key => $_value) {
                if($_value['id_worktime'] == $_id_worktime AND isset($_value['id_group']) AND (intval($_value['id_group']) > 0) ){
                    if( isset($group[$_value['id_group']]) AND !empty($group[$_value['id_group']]) AND !in_array($group[$_value['id_group']], $id_group) ){
                        $id_group[] = $group[$_value['id_group']];
                    }
                    unset($worktime_detail[$_key]);
                } else{
                    if($_value['id_worktime'] == $_id_worktime AND isset($_value['id_worker']) AND intval($_value['id_worker']) > 0){
                        if(isset($worker[$_value['id_worker']]) AND !empty($worker[$_value['id_worker']])){
                            $id_worker[] = $worker[$_value['id_worker']];
                        }
                        unset($worktime_detail[$_key]);
                    }
                }
            }

            $edit = true;
            if(strtotime($value['starttime']) < (strtotime(date('Y-m-d H:i:s')) + 5*(24*60*60)) ){
                $edit = false;
            } 
            // else {
            //     $check_day = (strtotime($value['starttime']) - strtotime(date('Y-m-d H:i:s'))) / (24 * 60 * 60);
            //     if($check_day <= 5){
            //         $edit = false;
            //     }
            // }

            if((isset($id_group) AND !empty($id_group)) OR (isset($id_worker) AND !empty($id_worker))){
                $data[] = array(
                    'id'        => $_id_worktime,
                    'title'     => isset($value['title']) ? $value['title'] :'',
                    'des'       => isset($value['des']) ? $value['des'] :'',
                    'starttime' => isset($value['starttime']) ? $value['starttime'] :'',
                    'endtime'   => isset($value['endtime']) ? $value['endtime'] :'',
                    'type'      => isset($value['type']) ? $value['type'] :'',
                    'id_group'  => $id_group,
                    'id_worker' => $id_worker,
                    'edit'      => $edit
                );
            } else {
                unset($worktime[$key]);
            }
        }

        if(isset($data) AND !empty($data)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
                'data'    => $data
            );
            return  $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Data worktime empty'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function index_post(){
        $title        = $this->post('title');
        $des          = $this->post('des');
        $group_id     = $this->post('group_id');
        $worker_id    = $this->post('worker_id');
        $starttime    = $this->post('starttime');
        $endtime      = $this->post('endtime');
        $type         = $this->post('type');
        $created_at   = date('Y-m-d H:i:s');
        $timezone     = $this->post('timezone');

        if(!is_array($worker_id) AND !empty($worker_id)){
            $worker_id = explode(',', $worker_id);
        }

        if(!is_array($group_id) AND !empty($group_id)){
            $group_id = explode(',', $group_id);
        }
        # insert worktime
        $insert = array(
            'title'         => isset($title) ? $title : '',
            'des'           => isset($des) ? $des : '',
            'starttime'     => isset($starttime) ? $starttime : '',
            'endtime'       => isset($endtime) ? $endtime : '',
            'type'          => isset($type) ? $type : '',
            'created_at'    => $created_at,
        );
        $flag = $this->db->insert(TBL_WORKTIME, $insert);
        if($flag != TRUE){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Insert worktime fail !'
            );
            return  $this->response($return, REST_CODE_PARAM_ERR);
        }
        # id worktime insert
        $id_worktime = $this->db->insert_id();
        # get list worker
        $worker = array();
        $this->db->where('deleted_at', NULL);
        $worker = $this->db->get(TBL_WORKER)->result_array();
        # insert worktime detail
        $insert_batch = array();

        $worker_group = array();
        $this->db->where('deleted_at', NULL);
        $this->db->where_in('group_id', $group_id);
        $worker_group = $this->db->get(TBL_WORKER_GROUP)->result_array();

        $id_worker_in_group = array();

        if(is_array($group_id) AND !empty($group_id)){
            foreach ($group_id as $key => $value) {
                foreach ($worker_group as $_key => $_value) {
                    if($_value['group_id'] == $value){

                        $id_worker_in_group[] = $_value['worker_id'];

                        $insert_batch[] = array(
                            'id_worktime' => $id_worktime,
                            'id_group'    => $value,
                            'id_worker'   => $_value['worker_id'],
                            'starttime'   => isset($starttime) ? $starttime : '',
                            'endtime'     => isset($endtime) ? $endtime : '',
                            'type'        => isset($type) ? $type : '',
                            'created_at'  => date('Y-m-d H:i:s'),
                        );
                        unset($worker_group[$_key]);
                    }
                }
            }
        }
        
        // check worker exits in group
        if(is_array($worker_id) AND !empty($worker_id)){
            foreach ($worker_id as $key => $value) {
                if(!in_array($value, $id_worker_in_group)){
                    $insert_batch[] = array(
                        'id_worktime' => $id_worktime,
                        'id_group'    => 0,
                        'id_worker'   => $value,
                        'starttime'   => isset($starttime) ? $starttime : '',
                        'endtime'     => isset($endtime) ? $endtime : '',
                        'type'        => isset($type) ? $type : '',
                        'created_at'  => date('Y-m-d H:i:s'),
                    );
                }
            }    
        }

        if(!is_array($insert_batch) OR empty($insert_batch) OR !isset($insert_batch)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Data insert empty'
            );
            return  $this->response($return, REST_CODE_PARAM_ERR);
        }
        
        $flag = $this->db->insert_batch(TBL_WORKTIME_DETAIL, $insert_batch);
        if($flag === TRUE){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
                'data'    => $id_worktime,
            );
            return  $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Insert worktime detail fail !'
            );
            return  $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    function index_put(){
        $id           = $this->put('id');
        $title        = $this->put('title');
        $des          = $this->put('des');
        $_group_id     = $this->put('group_id');
        $_worker_id    = $this->put('worker_id');
        $starttime    = $this->put('starttime');

        $endtime      = $this->put('endtime');
        $type         = $this->put('type');
        $updated_at   = date('Y-m-d H:i:s');
        $timezone     = $this->put('timezone');

        if(!isset($id) OR empty($id)){
            if($flag != TRUE) {
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'id empty !'
                );
                return  $this->response($return, REST_CODE_PARAM_ERR);
            }
        }
        $worker_id = array();
        if(!is_array($_worker_id) AND !empty($_worker_id)){
            $worker_id = explode(',', trim($_worker_id));
        }
        $group_id = array();
        if(!is_array($_group_id) AND !empty($_group_id)){
            $group_id = explode(',', trim($_group_id));
        }

        # update worktime

        $update = array(
            'title'         => isset($title) ? $title : '',
            'des'           => isset($des) ? $des : '',
            'starttime'     => isset($starttime) ? $starttime : '',
            'endtime'       => isset($endtime) ? $endtime : '',
            'type'          => isset($type) ? $type : '',
            'updated_at'    => $updated_at,
        );

        #print_r($update);die;

        $this->db->where('id', $id);
        $flag = $this->db->update(TBL_WORKTIME, $update);
        if($flag != TRUE){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Update worktime fail !'
            );
            return  $this->response($return, REST_CODE_PARAM_ERR);
        }

        # update worktime detail

        $this->db->where('id_worktime', $id);
        $flag = $this->db->update(TBL_WORKTIME_DETAIL, array('deleted_at' => date('Y-m-d H:i:s')));

        if($flag !== TRUE){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Update worktime detail fail !'
            );
            return  $this->response($return, REST_CODE_PARAM_ERR);
        }

        # insert new
        # insert worktime detail

        $insert_batch = array();

        $worker_group = array();
        $this->db->where('deleted_at', NULL);
        $this->db->where_in('group_id', $group_id);
        $worker_group = $this->db->get(TBL_WORKER_GROUP)->result_array();

        $id_worker_in_group = array();

        if(is_array($group_id) AND !empty($group_id)){
            foreach ($group_id as $key => $value) {
                foreach ($worker_group as $_key => $_value) {
                    if($_value['group_id'] == $value){

                        $id_worker_in_group[] = $_value['worker_id'];

                        $insert_batch[] = array(
                            'id_worktime' => $id,
                            'id_group'    => $value,
                            'id_worker'   => $_value['worker_id'],
                            'starttime'   => isset($starttime) ? $starttime : '',
                            'endtime'     => isset($endtime) ? $endtime : '',
                            'type'        => isset($type) ? $type : '',
                            'created_at'  => date('Y-m-d H:i:s'),
                        );
                        unset($worker_group[$_key]);
                    }
                }
            }    
        }
        
        // check worker exits in group
        if(is_array($worker_id) AND !empty($worker_id)){
            foreach ($worker_id as $key => $value) {
                if(!in_array($value, $id_worker_in_group)){
                    $insert_batch[] = array(
                        'id_worktime' => $id,
                        'id_group'    => 0,
                        'id_worker'   => $value,
                        'starttime'   => isset($starttime) ? $starttime : '',
                        'endtime'     => isset($endtime) ? $endtime : '',
                        'type'        => isset($type) ? $type : '',
                        'created_at'  => date('Y-m-d H:i:s'),
                    );
                }
            }
        }

        

        $flag = $this->db->insert_batch(TBL_WORKTIME_DETAIL, $insert_batch);

        if($flag === TRUE){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
                'data'    => $id,
            );
            return  $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Insert worktime detail fail !'
            );
            return  $this->response($return, REST_CODE_PARAM_ERR);
        }
    }

    function index_bank_put(){
        $id           = $this->put('id');
        $title        = $this->put('title');
        $des          = $this->put('des');
        $_group_id     = $this->put('group_id');
        $_worker_id    = $this->put('worker_id');
        $starttime    = $this->put('starttime');

        $endtime      = $this->put('endtime');
        $type         = $this->put('type');
        $updated_at   = date('Y-m-d H:i:s');
        $timezone     = $this->put('timezone');

        $this->db->trans_start();

        if(!isset($id) OR empty($id)){
            if($flag != TRUE) {
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'id empty !'
                );
                return  $this->response($return, REST_CODE_PARAM_ERR);
            }
        }
        $worker_id = array();
        if(!is_array($_worker_id) AND !empty($_worker_id)){
            $worker_id = explode(',', trim($_worker_id));
        }
        $group_id = array();
        if(!is_array($_group_id) AND !empty($_group_id)){
            $group_id = explode(',', trim($_group_id));
        }
        # update worktime

        $update = array(
            'title'         => isset($title) ? $title : '',
            'des'           => isset($des) ? $des : '',
            'starttime'     => isset($starttime) ? $starttime : '',
            'endtime'       => isset($endtime) ? $endtime : '',
            'type'          => isset($type) ? $type : '',
            'updated_at'    => $updated_at,
        );

        #print_r($update);die;

        $this->db->where('id', $id);
        $flag = $this->db->update(TBL_WORKTIME, $update);
        if($flag != TRUE){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Update worktime fail !'
            );
            return  $this->response($return, REST_CODE_PARAM_ERR);
        }
        # update worktime detail
        $id_worker_detail = array();
        $id_group_detail  = array();
        $this->db->where('deleted_at', NULL);
        $this->db->where('id_worktime', $id);
        $worktime_detail = $this->db->get(TBL_WORKTIME_DETAIL)->result_array();
        foreach ($worktime_detail as $key => $value) {
            if(isset($value['id_group']) AND intval($value['id_group']) > 0 AND !in_array($value['id_group'], $id_group_detail)){
                $id_group_detail[] = $value['id_group'];
            } else{
                if(isset($value['id_worker']) AND intval($value['id_worker']) > 0){
                    $id_worker_detail[] = $value['id_worker'];
                }
            }
        }

        # check insert, delete
        $id_worker_delete = array();
        $id_group_delete  = array();
        $id_worker_insert = array();
        $id_group_insert  = array();
        
        if(isset($id_worker_detail) AND !empty($id_worker_detail)){
            foreach ($id_worker_detail as $key => $value) {
                if(!in_array($value, $worker_id)){
                    $id_worker_delete[] = $value;
                    unset($id_worker_detail[$key]);
                }
            }
        }

        if(isset($worker_id) AND !empty($worker_id)){
            foreach ($worker_id as $key => $value) {
                if(!in_array($value, $id_worker_detail)){
                    $id_worker_insert[] = $value;
                }
            }
        }

        if(isset($id_group_detail) AND !empty($id_group_detail)){
            foreach ($id_group_detail as $key => $value) {
                if(!in_array($value, $group_id)){
                    $id_group_delete[] = $value;
                    unset($id_group_detail[$key]);
                }
            }    
        }
        
        if(isset($group_id) AND !empty($group_id)){
            foreach ($group_id as $key => $value) {
                if(!in_array($value, $id_group_detail)){
                    $id_group_insert[] = $value;
                }
            }
        }

        # data insert
        $insert_data = array();
        if(is_array($id_worker_insert) AND !empty($id_worker_insert)){
            foreach ($id_worker_insert as $key => $value) {
                $insert_data[] = array(
                    'id_worktime' => $id,
                    'id_group'    => 0,
                    'id_worker'   => $value,
                    'created_at'  => date('Y-m-d H:i:s'),
                );
            }
        }

        $worker_group = array();
        if(is_array($id_group_insert) AND !empty($id_group_insert)){
            $this->db->where('deleted_at', NULL);
            $this->db->where_in('group_id', $id_group_insert);
            $worker_group = $this->db->get(TBL_WORKER_GROUP)->result_array();
        }

        if(is_array($id_worker_insert) AND !empty($id_worker_insert)){
            foreach ($id_group_insert as $key => $value) {
                foreach ($worker_group as $_key => $_value) {
                    if($_value['group_id'] == $value){
                        $insert_data[] = array(
                            'id_worktime' => $id_worktime,
                            'id_group'    => $value,
                            'id_worker'   => $_value['worker_id'],
                            'starttime'   => isset($starttime) ? $starttime : '',
                            'endtime'     => isset($endtime) ? $endtime : '',
                            'type'        => isset($type) ? $type : '',
                            'created_at'  => date('Y-m-d H:i:s'),
                        );
                        unset($worker_group[$_key]);
                    }
                }
            }    
        }
        

        if(is_array($insert_data) AND !empty($insert_data)){
            $flag = $this->db->insert_batch(TBL_WORKTIME_DETAIL,$insert_data);
            if($flag !== TRUE){
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'Insert new worktime detail fail !'
                );
                return  $this->response($return, REST_CODE_PARAM_ERR);
            }
        }
        # data delete

        $update_data = array();
        foreach ($worktime_detail as $key => $value) {
            if( (in_array($value['id_worker'], $id_worker_delete) OR in_array($value['id_group'], $id_group_delete)) AND ($value['id_worktime'] ==  $id) ){
                $update_data[] = array(
                    'id' => $value['id'],
                    'deleted_at' => date('Y-m-d H:i:s')
                );
            }
        }

        if(is_array($update_data) AND !empty($update_data)){
            $flag = $this->db->update_batch(TBL_WORKTIME_DETAIL,$update_data,'id');
            if($flag !== TRUE){
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'Update worktime detail fail !'
                );
                return  $this->response($return, REST_CODE_PARAM_ERR);
            }
        }

        $return = array(
            'version' => config_item('api_version'),
            'status'  => STATUS_SUCCESS,
            'data'    => $id
        );
        return  $this->response($return,REST_CODE_OK);

        $this->db->trans_complete();
    }

    function index_delete(){
        $id = $this->delete('id');
        if(!isset($id) OR empty($id)){
            if($flag != TRUE) {
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'id empty !'
                );
                return  $this->response($return, REST_CODE_PARAM_ERR);
            }
        }

        $this->db->where('id', $id);
        $flag = $this->db->update(TBL_WORKTIME, array('deleted_at'=> date('Y-m-d H:i:s')));

        if($flag != TRUE) {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Update fail !'
            );
            return  $this->response($return, REST_CODE_PARAM_ERR);
        }

        $this->db->where('id_worktime', $id);
        $flag = $this->db->update(TBL_WORKTIME_DETAIL, array('deleted_at'=> date('Y-m-d H:i:s')));
        if($flag != TRUE){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Update delete detail fail !'
            );
            return  $this->response($return, REST_CODE_PARAM_ERR);
        }

        $return = array(
            'version' => config_item('api_version'),
            'status'  => STATUS_SUCCESS,
            'data'    => $id
        );
        return  $this->response($return,REST_CODE_OK);
    }

    function worktime_check_get(){
        $starttime  = $this->get('starttime');
        $endtime    = $this->get('endtime');

        if(!$starttime OR !$endtime){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Starttime or endtime fail!'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
        $worktime = array();
        $id_worktime = array();

        $this->db->where('deleted_at', NULL);
        $where = "";
        $where .= "(starttime >= '" . date('Y-m-d H:i:s', strtotime($starttime)) . "' AND starttime <= '" . date('Y-m-d H:i:s', strtotime($endtime)) . "')";
        $where .= " OR";
        $where .= " (starttime <= '" . date('Y-m-d H:i:s', strtotime($starttime)) . "' AND endtime >= '" . date('Y-m-d H:i:s', strtotime($endtime)) . "')";
        $where .= " OR";
        $where .= " (endtime >= '" . date('Y-m-d H:i:s', strtotime($starttime)) . "' AND endtime <= '" . date('Y-m-d H:i:s', strtotime($endtime)) . "')";
        $this->db->where($where);
        $worktime = $this->db->get(TBL_WORKTIME)->result_array();

        #print_r($worktime);die();

        if(is_array($worktime) AND !empty($worktime)){
            foreach ($worktime as $key => $val) {
                $id_worktime[] = $val['id'];
            }
        }
        #print_r($id_worktime);die();
        # Get worker assign
        $id_worker_assign = array();
        $id_group_assign = array();
        if(is_array($id_worktime) AND !empty($id_worktime)){

            $worktime_detail = array();
            $this->db->where('deleted_at', NULL);
            $this->db->where_in('id_worktime', $id_worktime);
            $worktime_detail = $this->db->get(TBL_WORKTIME_DETAIL)->result_array();

            if(is_array($worktime_detail) AND !empty($worktime_detail)){
                foreach ($worktime_detail as $key => $val) {
                    if(isset($val['id_group']) AND (intval($val['id_group']) > 0) AND !in_array($val['id_group'], $id_group_assign)){
                        $id_group_assign[] = $val['id_group'];
                    } else {
                        if(isset($val['id_worker']) AND (intval($val['id_worker']) > 0) AND !in_array($val['id_worker'], $id_worker_assign)) {
                            $id_worker_assign[] = $val['id_worker'];
                        }
                    }
                }
            }
        }

        $worker = array();
        $this->db->where('deleted_at', NULL);
        if(is_array($id_worker_assign) AND !empty($id_worker_assign)){
            $this->db->where_not_in('id', $id_worker_assign);
        }
        $worker = $this->db->get(TBL_WORKER)->result_array();

        $group = array();
        $this->db->where('deleted_at', NULL);
        if(is_array($id_group_assign) AND !empty($id_group_assign)){
            $this->db->where_not_in('id', $id_group_assign);
        }
        $group = $this->db->get(TBL_GROUP)->result_array();

        $return = array(
            'version' => config_item('api_version'),
            'status'  => STATUS_SUCCESS,
            'worker'  => $worker,
            'group'   => $group
        );
        return  $this->response($return,REST_CODE_OK);
    }
}
