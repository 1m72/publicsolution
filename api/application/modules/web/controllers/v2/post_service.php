<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
class Post_service extends REST_Controller{
    private $id_module  = 1;
    private $_id_city   = 1;

    function __construct(){
        parent:: __construct();
        if (isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->_id_city = intval($this->_tenantId);
        } else {
            $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function index_get(){
        $limit = $this->get('limit');
        if(!isset($limit) OR ($limit == false)){
            $limit = 150;
        }
        $offset = $this->get('offset');
        if(!isset($offset) OR ($offset == false)){
            $offset = 0;
        }

        $day = $this->get('time');

        $total = 0;

        $this->db->start_cache();

        if($day != false){
            $start = $day . ' 00:00:00';
            $end   = $day . ' 23:59:59';
            $this->db->where('request_at >=', $start);
            $this->db->where('request_at <=', $end);
        }
        $this->db->from(TBL_POST_SERVICE_LOG);
        $this->db->order_by('request_at', 'desc');
        $this->db->stop_cache();

        $total = $this->db->count_all_results();

        $this->db->limit($limit,$offset);
        $data = $this->db->get()->result_array();

        $this->db->flush_cache();

        if(isset($data) AND !empty($data) AND is_array($data)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
                'data'    => $data,
                'total'   => $total
            );
            return $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Data empty !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function convert_post_service_get(){
        $data = $this->_db_global->get(TBL_3RD_API_LOGS)->result_array();
        $data_city = array();
        if(!isset($data) OR empty($data)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Data empty !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        foreach ($data as $key => $value) {
            $id_city = isset($value['id_city']) ? $value['id_city'] : false;
            $item = array(
                'uri'            => $value['uri'],
                'request_data'   => str_replace(get_server_path(), '', $value['attachments']),
                //'request_extra'  => $value['request_log'],
                'request_at'     => $value['requested_at'],
                'response_data'  => $value['response_data'],
                'response_extra' => $value['extra'],
                'status'         => $value['status'],
                'retry'          => $value['retry'],
                'created_at'     => $value['created_at']
            );
            $data_city[$id_city][] = $item;
        }

        #insert
        if(!isset($data_city) OR empty($data_city)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Data by city empty !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        $msg = array();

        foreach ($data_city as $key => $value) {
            $db_connect  = connect_db(false, $key);
            $flag        = $db_connect->insert_batch(TBL_POST_SERVICE_LOG, $value);
            if($flag === true){
                $msg[] = "Insert OK - id_city : " . $key . " Count : " . count($value);
            } else {
                $msg[] = "Insert FAIL - id_city : " . $key . " Count : " . count($value);
            }
        }

        $return = array(
            'version' => config_item('api_version'),
            'status'  => STATUS_SUCCESS,
            'msg'     => $msg
        );
        return $this->response($return,REST_CODE_OK);
    }
}