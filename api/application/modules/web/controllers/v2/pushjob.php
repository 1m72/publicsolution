<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

class Pushjob extends REST_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('file');
	}
	/*
	function test(){
		#echo 'a';die;
		$lat = 48.078031;
		$lon = 11.8794905;
		$file = "http://nominatim.kloon.net/reverse?format=json&zoom=17&addressdetails=1&lat={$lat}&lon={$lon}";
		#echo $file;die;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $file);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		#curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
		curl_setopt($ch, CURLOPT_TIMEOUT,1000);
		$result = curl_exec($ch);
		curl_close($ch);
		echo $result;
	}
	*/

	function index_get(){
		# get cron status
		$cron_status_array 	= array();
		$this->_db_global->select('id, id_city, data');
		$this->_db_global->where('type'		, 'activity');
		#$this->_db_global->where('status'	, 'pending');
		$this->_db_global->order_by('id'	, 'desc');
		$cron_status_array = $this->_db_global->get(TBL_CRON)->result_array();
		$cron_array = array();
		if(is_array($cron_status_array) AND !empty($cron_status_array)){
			foreach ($cron_status_array as $cron_key => $cron_value) {
				$path_file 	= isset($cron_value['data']) ? $cron_value['data'] : FALSE;
				$json_text 	= read_file($path_file);
				$json_data 	= json_decode($json_text, true);
				$id_task 	= isset($json_data['task_id']) ? $json_data['task_id'] : FALSE;
				if($id_task != FALSE){
					$cron_array[] = array(
						'id' 		=> isset($cron_value['id']) 	   ? $cron_value['id']         : FALSE,
						'id_city'   => isset($cron_value['id_city'])   ? $cron_value['id_city']    : FALSE,
						'data'		=> isset($cron_value['data']) 	   ? $cron_value['data']       : FALSE,
						'id_task' 	=> $id_task,
					);
				}
			}
		}
		#
		$city_result = array();
		$this->_db_global->where('deleted_at', NULL);
		$this->_db_global->order_by('id', 'desc');
		$city_result = $this->_db_global->get(TBL_CITY)->result_array();
		if(is_array($city_result) AND !empty($city_result)){
			$data_return = array();
			foreach ($city_result as $key => $value) {
				$data_city = array();
				$id_city = isset($value['id']) ? $value['id'] : FALSE;
				$db_connect = $this->connect_db(FALSE,$id_city);
				if ($db_connect === false) {
					write_log('Connect fail _ id_city : ' . $id_city , 'push_job_status');
				} else {
					# get gps status
					$gps_status_array 	= array();
					$db_connect->select('id, id_city, id_task');
					$db_connect->where('deleted_at'	, NULL);
					#$db_connect->where('status'		, 'pending');
					$db_connect->order_by('id'		, 'desc');
					$gps_status_array = $db_connect->get(TBL_WORKER_GPS)->result_array();
					# check
					if(is_array($gps_status_array) AND !empty($gps_status_array)){
						foreach ($gps_status_array as $gps_key => $gps_value) {
							$id_task = isset($gps_value['id_task']) ? $gps_value['id_task'] : FALSE;
							foreach ($cron_array as $cron_key => $cron_value) {
								if(isset($cron_value['id_task']) AND ($cron_value['id_task'] == $id_task)){
									$data_city['gps_pending'][] = array(
										'id_city'   => $id_city,
										'id_gps'	=> $gps_value['id'],
										'id_task'	=> $id_task,
										'data'		=> $cron_value['data'],
									);
									unset($cron_array[$cron_key]);
								}
							}
						}
						if(is_array($cron_array) AND !empty($cron_array)){
							foreach ($cron_array as $cron_key => $cron_value) {
								if($cron_value['id_city'] == $id_city){
									$data_city['not_push_job'][] = array(
										'id_city'   => $cron_value['id_city'],
										'id_gps'    => FALSE,
										'id_task'   => $cron_value['id_task'],
										'data'      => $cron_value['data'],
									);
									unset($cron_array[$cron_key]);
								}
							}
						}
					}
				}
				$data_return[$id_city] = $data_city;
			}
			if(is_array($data_return) AND !empty($data_return)){
				$return = array(
					'version'   => config_item('api_version'),
					'status'    => STATUS_SUCCESS,
					'data'      => $data_return
				);
				return  $this->response($return,REST_CODE_OK);
			} else {
				$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'data empty',
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}
			#var_dump($data_return);
			/*
			if(is_array($data_return) AND !empty($data_return)){
				foreach ($data_return as $root_key => $root_value) {
					if(count($root_value) > 0){
						foreach ($root_value as $item_key => $item_value) {
							echo $item_value['id_gps'] . ' | ' . $item_value['id_task'] . ' | ' . $item_value['data'] . '<br>';
						}
					}
				}
			}
			*/
		} else {
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function index_post(){
		$this->load->helper('pheanstalk');

		$id_city    = $this->post('id_city');
		$type       = 'activity';
		$data       = $this->post('data');
		$status     = CRON_STATUS_PENDING;
		$created    = date('Y-m-d H:i:s');

		if(!isset($id_city) OR empty($id_city)){
			$return = array(
				'version'   => config_item('api_version'),
				'status'    => STATUS_FAIL,
				'msg'       => 'id_city empty'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		if(!isset($data) OR empty($data)){
			$return = array(
				'version'   => config_item('api_version'),
				'status'    => STATUS_FAIL,
				'msg'       => 'data empty'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		$item = array(
			'id_city'       => $id_city,
			'type'          => $type,
			'data'          => $data,
			'status'        => $status,
			'created_at'    => $created,
		);
		push_job('upload_tour_data', json_encode($item));
		write_log('add push job _ data :'.json_encode($item), 'add_push_job');
		$return = array(
			'version'   => config_item('api_version'),
			'status'    => STATUS_SUCCESS
		);
		return  $this->response($return,REST_CODE_OK);
	}

	function connect_db($db_config = array(), $id_city = false) {
		if ($id_city !== false) {
			$city = $this->_db_global->get_where(TBL_CITY, array('id' => $id_city))->row();
			if (is_object($city) AND !empty($city)) {
				$id_config = isset($city->id_config) ? $city->id_config : false;
				if ($id_config !== false) {
					$tmp = $this->_db_global->select('database')->get_where(TBL_CITY_CONFIG, array('id' => $id_config))->row();
					$db_config = json_decode($tmp->database);
				} else {
					write_log("Connect to db fail _ City info not found _ id_city = {$id_city}", 'fn_connect_db');
					return false;
				}
			} else {
				write_log("Connect to db fail _ City info not found _ id_city = {$id_city}", 'fn_connect_db');
				return false;
			}
		}

		if ((is_array($db_config) OR is_object($db_config)) AND !empty($db_config)) {
			$db_config = json_decode(json_encode($db_config));
			$config = array(
				'hostname' => isset($db_config->hostname) ? $db_config->hostname : config_item('hostname'),
				'username' => isset($db_config->username) ? $db_config->username : config_item('username'),
				'password' => isset($db_config->password) ? $db_config->password : config_item('password'),
				'database' => isset($db_config->database) ? $db_config->database : config_item('database'),
				'port'     => isset($db_config->port)     ? $db_config->port     : config_item('db_port'),
				'dbprefix' => config_item('db_prefix'),
				'dbdriver' => config_item('db_dbdriver'),
				'pconnect' => config_item('db_pconnect'),
				'db_debug' => config_item('db_db_debug'),
				'cache_on' => config_item('db_cache_on'),
				'cachedir' => config_item('db_cachedir'),
				'char_set' => config_item('db_char_set'),
				'dbcollat' => config_item('db_dbcollat')
			);

			$db            = $this->load->database($config, true);
			$db_connection = $db->initialize();
			if (!$db_connection) {
				return false;
			}
			return $db;
		}
	}

	function add_que_service_get(){
		$this->load->helper('pheanstalk');
		$id_city = $this->input->get('id_city');
		$id_gps  = $this->input->get('id_gps');
		$job = array(
			'id_city' => $id_city,
			'id_gps'  => $id_gps
		);
		push_job(QUE_SERVICE_POST, json_encode($job));
		echo "OK";
	}
}
