<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

class Group extends REST_Controller{

    private $id_module  = 1;
    private $_id_city   = 1;

    function __construct(){

        parent:: __construct();

        if (isset($this->_tenantId) && !empty($this->_tenantId)){
            $this->_id_city = intval($this->_tenantId);
        } else {
            $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'tenantId not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function index_get(){
        $id = $this->get('id');
        $is_parent = $this->get('is_parent');
        # get group

        $this->db->where('deleted_at', NULL);
        if(isset($id) AND !empty($id) AND ($id != false)){
            $this->db->where('id', $id);
        } else {
            if(($is_parent != false) AND isset($is_parent) AND !empty($is_parent) AND (intval($is_parent) > 0)){
                $this->db->where('id_parent', 0);
            }
        }
        $this->db->order_by('name', 'asc');
        $group = $this->db->get(TBL_GROUP)->result_array();

        if(!isset($group) OR !is_array($group) OR empty($group)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'data group empty !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        if(($is_parent == false) OR !isset($is_parent) OR empty($is_parent) OR (intval($is_parent) < 1)){
            # get worker by group
            $this->db->where('deleted_at', NULL);
            $worker_group = $this->db->get(TBL_WORKER_GROUP)->result_array();

            if(isset($worker_group) AND !empty($worker_group)){
                foreach ($group as $key => $value) {
                    $id_group = isset($value['id']) ? intval($value['id']) : fail;

                    $id_worker = array();

                    foreach ($worker_group as $_key => $_value) {
                        if( isset($_value['group_id']) AND (intval($_value['group_id']) == $id_group) ){
                            $id_worker[] = $_value['worker_id'];

                            unset($worker_group[$_key]);
                        }
                    }

                    $group[$key]['id_worker'] = $id_worker;
                }
            }
        }

        if(isset($group) AND is_array($group) AND !empty($group)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
                'data'    => $group

            );
            return  $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'data group not found !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function index_post(){
        $name      = $this->post('name');
        $id_worker = $this->post('id_worker');
        $id_parent = $this->post('id_parent');

        # insert new group

        $group = array(
            'id_parent'  => (isset($id_parent) AND ($id_parent != false)) ? $id_parent : 0,
            'name'       => isset($name) ? $name : '',
            'created_at' => date('Y-m-d H:i:s')
        );

        $flag = $this->db->insert(TBL_GROUP, $group);

        if($flag != TRUE){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'insert group fail !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        $id_group = $this->db->insert_id();

        if(isset($id_worker) AND $id_worker != false AND !empty($id_worker) AND !is_array($id_worker)){
            $id_worker = explode(',', $id_worker);

            # insert worker group

            $group_worker = array();

            foreach ($id_worker as $key => $value) {
                $group_worker[] = array(
                    'worker_id'  => $value,
                    'group_id'   => $id_group,
                    'created_at' => date('Y-m-d H:i:s')
                );
            }

            if(!is_array($group_worker) OR empty($group_worker)){
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'data insert group_worker fail !'
                );
                return  $this->response($return,REST_CODE_PARAM_ERR);
            }

            $flag = $this->db->insert_batch(TBL_WORKER_GROUP, $group_worker);

            if($flag !== TRUE){
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'insert group_worker fail !'
                );
                return  $this->response($return,REST_CODE_PARAM_ERR);
            }
        }

        $return = array(
            'version' => config_item('api_version'),
            'status'  => STATUS_SUCCESS,
            'data'    => $id_group,
            'group'   => $group,
            'worker'  => isset($group_worker) ? $group_worker : array()

        );
        return  $this->response($return,REST_CODE_OK);
    }

    function index_put(){
        $id        = $this->put('id');
        $id_parent = $this->put('id_parent');
        $name      = $this->put('name');
        $id_worker = $this->put('id_worker');

        # update group

        $group = array(
            'id_parent'  => (isset($id_parent) AND ($id_parent != false)) ? $id_parent : 0,
            'name'       => isset($name) ? $name : '',
            'updated_at' => date('Y-m-d H:i:s')
        );

        $this->db->where('id', $id);
        $flag = $this->db->update(TBL_GROUP, $group);

        if($flag != TRUE){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'insert group fail !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        # update delete group

        $this->db->where('group_id', $id);
        $flag = $this->db->update(TBL_WORKER_GROUP, array('deleted_at' => date('Y-m-d H:i:s')));

        if($flag != TRUE){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'update delete group_worker fail !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        if(isset($id_worker) AND $id_worker != false AND !empty($id_worker) AND !is_array($id_worker)){
            $id_worker = explode(',', $id_worker);

            # insert new

            $group_worker = array();

            foreach ($id_worker as $key => $value) {
                $group_worker[] = array(
                    'worker_id'  => $value,
                    'group_id'   => $id,
                    'created_at' => date('Y-m-d H:i:s')
                );
            }

            if(!is_array($group_worker) OR empty($group_worker)){
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'data insert group_worker fail !'
                );
                return  $this->response($return,REST_CODE_PARAM_ERR);
            }

            $flag = $this->db->insert_batch(TBL_WORKER_GROUP, $group_worker);

            if($flag !== TRUE){
                $return = array(
                    'version' => config_item('api_version'),
                    'status'  => STATUS_FAIL,
                    'msg'     => 'insert group_worker fail !'
                );
                return  $this->response($return,REST_CODE_PARAM_ERR);
            }
        }

        $return = array(
            'version' => config_item('api_version'),
            'status'  => STATUS_SUCCESS,
            'data'    => $id

        );
        return  $this->response($return,REST_CODE_OK);
    }

    function index_delete(){
        $id = $this->delete('id');
        # delete group
        $this->db->where('id', $id);
        $flag = $this->db->update(TBL_GROUP, array('deleted_at' => date('Y-m-d H:i:s')));

        if($flag != TRUE){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'update fail !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        # delete group_worker
        $this->db->where('group_id', $id);
        $flag = $this->db->update(TBL_WORKER_GROUP , array('deleted_at' => date('Y-m-d H:i:s')));
        if($flag != TRUE){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'delete group_activity fail !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
        # return
        $return = array(
            'version' => config_item('api_version'),
            'status'  => STATUS_SUCCESS,
            'data'    => $id
        );
        return  $this->response($return,REST_CODE_OK);
    }

    function remove_worker_in_group_delete(){
        $id_worker = $this->delete('id_worker');
        $id_group  = $this->delete('id_group');

        $this->db->where('worker_id', $id_worker);
        $this->db->where('group_id', $id_group);

        $flag = $this->db->update(TBL_WORKER_GROUP, array('deleted_at' => date('Y-m-d H:i:s')));

        if($flag !== false){
            # return
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_SUCCESS,
                'data'    => $id
            );
            return  $this->response($return,REST_CODE_OK);
        } else {
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'delete worker fail !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
    }

    function worker_by_group_get(){
        $group_id = $this->get('id');
        # get group
        $group = array();
        $id_group = array();
        $name_group = array();
        $this->db->where('deleted_at', NULL);
        if(isset($group_id) AND !empty($group_id)){
            $this->db->where('id', $group_id);
        }
        $group = $this->db->get(TBL_GROUP)->result_array();
        if(!is_array($group) OR empty($group)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Data group empty !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
        foreach ($group as $key => $value) {
            $id_group[] = $value['id'];
            $name_group[$value['id']] = ($value['name']) ? $value['name'] : '';
        }
        # get worker by group
        $worker_group = array();
        $id_worker_group = array();
        $this->db->where('deleted_at', NULL);
        $this->db->where_in('group_id', $id_group);
        $worker_group = $this->db->get(TBL_WORKER_GROUP)->result_array();
        if(!is_array($worker_group) OR empty($worker_group)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Worker group empty !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
        foreach ($worker_group as $key => $value) {
            $id_worker_group[] = $value['worker_id'];
        }
        # get worker
        $worker_by_group = array();
        $this->db->where('deleted_at', NULL);
        $this->db->where_in('id', $id_worker_group);
        $worker_by_group = $this->db->get(TBL_WORKER)->result_array();
        if(!is_array($worker_by_group) OR empty($worker_by_group)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Worker by group empty !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }
        # response
        $data = array();
        foreach ($worker_group as $key => $value) {
            $_worker_id = $value['worker_id'];
            $_group_id = $value['group_id'];

            if(!isset($data[$_group_id]) AND empty($data[$_group_id])){
                $group_name = $name_group[$_group_id];
                $data[$_group_id]['id']         = $_group_id;
                $data[$_group_id]['name']       = $group_name;
            }
            if(isset($data[$_group_id]) AND !empty($data[$_group_id])){
                $worker_item = array();
                foreach ($worker_by_group as $_key => $_val) {
                    if($_val['id'] == $_worker_id){
                        $worker_item = $_val;
                        //unset($worker_by_group[$_key]);
                    }
                }
                $data[$_group_id]['worker'][]   = $worker_item;
            }
        }

        $return = array(
            'version' => config_item('api_version'),
            'status'  => STATUS_SUCCESS,
            'data'    => (isset($group_id) AND !empty($group_id) AND isset($data[$group_id])) ? $data[$group_id] : $data
        );
        return  $this->response($return,REST_CODE_OK);
    }

    function group_by_group_get(){
        $id = $this->get('id');

        if($id === false OR !isset($id)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'id empty !'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);
        }

        $this->db->where('deleted_at', NULL);
        $this->db->where('id_parent', $id);

        $group = $this->db->get(TBL_GROUP)->result_array();

        if(!isset($group) OR empty($group)){
            $return = array(
                'version' => config_item('api_version'),
                'status'  => STATUS_FAIL,
                'msg'     => 'Data group empty'
            );
            return  $this->response($return,REST_CODE_PARAM_ERR);               
        }

        $return = array(
            'version' => config_item('api_version'),
            'status'  => STATUS_SUCCESS,
            'data'    => $group
        );
        return  $this->response($return,REST_CODE_OK);
    }
}