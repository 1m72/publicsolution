<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
class Gps extends REST_Controller{
	private $_id_city   = 1;
	function __construct(){
		parent:: __construct();
		if (isset($this->_tenantId) && !empty($this->_tenantId)){
			$this->load->model('gps_model','gpsm');
			$this->_id_city = intval($this->_tenantId);
		} else {
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function add_push_job_post(){
		$id_gps = $this->post('id_gps');
		if($id_gps){
			# Pheanstald lib
			$this->load->helper('pheanstalk');

			# Get gps detail
			$gps_detail = array();
			$this->db->where('id', $id_gps);
			$gps_detail = $this->db->get(TBL_WORKER_GPS)->row_array();
			$id_cron = isset($gps_detail['id_cron']) ? $gps_detail['id_cron'] : '';
			# Get cron detail
			$cron_detail = array();
			$this->_db_global->where('id', $id_cron);
			$cron_detail = $this->_db_global->get(TBL_CRON)->row_array();
			$data = isset($cron_detail['data']) ? $cron_detail['data'] : '';

			$job = array(
				'id_city'   => $this->_id_city,
				'id_cron'   => $id_cron,
				'data'      => $data
			);
			# Delete activiti
			$this->db->where('id_wkgps', $id_gps);
			$flag = $this->db->delete(TBL_WORKER_ACTIVITY);
			if($flag){
				$job_flag = push_job(QUE_SYNC, json_encode($job));
				$return = array(
					'version'   => config_item('api_version'),
					'status'    => STATUS_SUCCESS,
				);
				return $this->response($return,REST_CODE_OK);
			} else {
				$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Delete worker_activity not found !'
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}
		} else {
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'Id gps not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function record_gps_get(){
		$id_module = $this->get('id_module');
		if(!isset($id_module) OR empty($id_module)){
			$id_module = FALSE;
		}

		$data_return = array();
		# color activity
		$color = 'black';

		# id machine
		$id_machine_array = array();
		$id_machine = trim($this->get('machine'));
		if(isset($id_machine) AND !empty($id_machine)){
			$id_machine_array = convert_param_array($id_machine);
		}

		# id worker
		$id_worker_array = array();
		$id_worker = trim($this->get('worker'));
		if(isset($id_worker) AND !empty($id_worker)){
			$id_worker_array = convert_param_array($id_worker);
		}
		# street name
		$street_name = trim($this->get('street'));
		# day
		$day = $this->get('day');
		# limit
		$limit = (intval($this->get('limit')) === 0)?20:intval($this->get('limit'));
		$offset = $this->get('offset');
		if(!isset($offset) || empty($offset)) {
			$offset = 0;
		}
		# Get data activiti
		$activiti_data = array();
		$this->db->where('deleted_at',NULL);
		$activiti_data = $this->db->get(TBL_ACTIVITIES)->result_array();
		if( !is_array($activiti_data) OR empty($activiti_data) ){
			$return = array(
					'version'   => config_item('api_version'),
					'status'    => STATUS_FAIL,
					'msg'       => 'Data activity empty!'
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
		$_activiti_data = array();
		foreach ($activiti_data as $key => $value) {
			$_activiti_data[$value['id']] = isset($value['color']) ? $value['color'] : '';
		}
		# Get data machine
		$machine_data = array();
		$this->db->where('deleted_at',NULL);
		$machine_data = $this->db->get(TBL_MACHINE)->result_array();
		if( !is_array($machine_data) OR empty($machine_data) ){
			$return = array(
				'version'   => config_item('api_version'),
				'status'    => STATUS_FAIL,
				'msg'       => 'Data machine empty!'
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
		$_machine_data = array();
		foreach ($machine_data as $key => $value) {
			$_machine_data[$value['id']] = $value['name'];
		}
		# Get data worker
		$worker_data = array();
		$this->db->where('deleted_at',NULL);
		$worker_data = $this->db->get(TBL_WORKER)->result_array();
		if( !is_array($worker_data) OR empty($worker_data) ){
			$return = array(
					'version'   => config_item('api_version'),
					'status'    => STATUS_FAIL,
					'msg'       => 'Data worker empty!'
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
		$_worker_data = array();
		foreach ($worker_data as $key => $value) {
			$_worker_data[$value['id']] = array(
				'first_name' => isset($value['first_name']) ? $value['first_name'] : '',
				'last_name' => isset($value['last_name']) ? $value['last_name'] : '',
			);
		}

		$where = array(
			'id_module'  => isset($id_module) ? $id_module : '',
			'id_machine' => isset($id_machine_array) ? $id_machine_array  : array(),
			'id_worker'  => isset($id_worker_array) ? $id_worker_array : array(),
			'street'     => isset($street_name) ? $street_name : '',
			'day'        => isset($day) ? $day : '',
			'limit'      => isset($limit) ? $limit : 20,
			'offset'     => isset($offset)? $offset : 0,
		);
		$gps_result = $this->gpsm->record_gps($where);
		if( !isset($gps_result) OR empty($gps_result) ){
			$return = array(
					'version'   => config_item('api_version'),
					'status'    => STATUS_FAIL,
					'msg'       => 'Data GPS empty !'
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
		$gps_total = $this->gpsm->record_gps($where,TRUE);
		# array id gps
		$id_wk_gps_array    = $this->_array_colum($gps_result,'id');
		# group_activity
		$group_activity = $this->gpsm->group_activity($id_wk_gps_array);
		$_group_activity = array();
		if(is_array($group_activity) AND !empty($group_activity)){
			foreach ($group_activity as $key => $value) {
				$_group_activity[$value['id_wkgps']][] = array(
					'lat' => $value['latitude'],
					'lon' => $value['longtitude'],
					'col' => $_activiti_data[$value['id_activity']],
				);
			}
		}
		foreach ($gps_result as $gps_key => $gps_value) {
			$id_module = isset($gps_value['app']) ? $gps_value['app'] : FALSE;
			if($id_module != FALSE){
				$detail_module = array();
				$detail_module = $this->db->get_where(TBL_MODULES,array('id' => $id_module))->row_array();
				if(!isset($detail_module) OR empty($detail_module)){
					$return = array(
							'version' => config_item('api_version'),
							'status'  => STATUS_FAIL,
							'msg'     => 'Data module not found !'
					);
					return  $this->response($return,REST_CODE_PARAM_ERR);
				}
				$module_name = isset($detail_module['title']) ? $detail_module['title'] : FALSE;
				# GPS - id , ontime,position, machin, worker
				$gps_id             = $gps_value['id'];
				$gps_ontime         = $gps_value['ontime'];
				$gps_id_machine     = $gps_value['id_machine'];
				$gps_mc_name        = isset($_machine_data[$gps_id_machine]) ? $_machine_data[$gps_id_machine] : '';

				$gps_id_wk          = $gps_value['id_worker'];
				$gps_wk_first_name  = isset($_worker_data[$gps_id_wk]['first_name']) ? $_worker_data[$gps_id_wk]['first_name'] : '';
				$gps_wk_last_name   = isset($_worker_data[$gps_id_wk]['last_name']) ? $_worker_data[$gps_id_wk]['last_name'] : '';

				$gps_position       = '';
				if(isset($gps_value['position_clean']) AND !empty($gps_value['position_clean'])){
					$gps_position       = $gps_value['position_clean'];
				} else {
					$gps_position       = $gps_value['position'];
				}

				$gps_position       = trim($gps_position);
				$gps_position       = trim($gps_position, ',');
				$gps_position       = trim(trim($gps_position, 'LINESTRING('));
				$gps_position       = trim(trim($gps_position, ')'));
				$gps_status         = $gps_value['status'];
				$activity_by_gps = isset($_group_activity[$gps_id]) ? $_group_activity[$gps_id] : array();
				if(is_array($activity_by_gps) AND !empty($activity_by_gps)){
					$_count = 0;
					$_check = '';
					foreach ($activity_by_gps as $key => $value) {
						$color = isset($value['col']) ? $value['col'] : 'black';
						if($_count < 1){
							$gps_position = 'color '.$color.','.$gps_position;
						} else {
							$check = ',' . $value['lat'] . ' ' . $value['lon'] . ' ';
							$check2 = ',' . $value['lat'] . ' ' . $value['lon'] . ',';
							if($_check != $check AND $_check != $check2){
								$first = -1;
								$first = $this->gpsm->lastIndexOf($gps_position,$check);
								if($first < 0){
									$_check = $check2;
									$first = $this->gpsm->lastIndexOf($gps_position,$check2);
								} else {
									$_check = $check;
								}
								if($first != -1){
									$gps_position = substr($gps_position, 0,$first).','.'color '.$color.substr($gps_position,$first);
								}
							}
						}
						$_count++;
					}
				}
				# Add result
				$gps_position = 'LINESTRING('.$gps_position.')';
				$data_return[] = array(
					'id'            => $gps_id,
					'ontime'        => $gps_ontime,
					'status'        => $gps_status,
					'mcname'        => $gps_mc_name,
					'id_worker'     => $gps_id_wk,
					'first_name'    => $gps_wk_first_name,
					'last_name'     => $gps_wk_last_name,
					'position'      => $gps_position
				);
			}
		}# end for gps
		$return = array(
				'version'   => config_item('api_version'),
				'status'    => STATUS_SUCCESS,
				'total'     => $gps_total,
				'data'      => $data_return
		);
		#_debug($return);die;
		return $this->response($return,REST_CODE_OK);
	}

	function record_detail_by_gps_get(){
		$id_wkgps = $this->get('id_wkgps');
		if(!isset($id_wkgps) OR empty($id_wkgps)){
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'id gps empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		# Get data activiti
		$activiti_data = array();
		$this->db->where('deleted_at',NULL);
		$activiti_data = $this->db->get(TBL_ACTIVITIES)->result_array();
		if( !is_array($activiti_data) OR empty($activiti_data) ){
			$return = array(
					'version'   => config_item('api_version'),
					'status'    => STATUS_FAIL,
					'msg'       => 'Data activity empty!'
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
		$_activiti_data = array();
		foreach ($activiti_data as $key => $value) {
			$_activiti_data[$value['id']] = array(
				'name'  => isset($value['name']) ? $value['name'] : '',
				'color' => isset($value['color']) ? $value['color'] : '',
			);
		}

		$activity_result = array();
		$activity_result = $this->gpsm->record_activity($id_wkgps);
		if(!is_array($activity_result) OR empty($activity_result)){
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'activity by gps empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		$infor_result = $this->gpsm->information_gps($id_wkgps);
		/*
		if(!is_array($infor_result) OR empty($infor_result)){
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'information by gps empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}*/
		# information
		$informations = array();
		foreach ($infor_result as $infor_key => $infor_value) {
			$infor_id_gps   = $infor_value['id_gps'];
			if($infor_id_gps == $id_wkgps){
				$infor_lat  = $infor_value['lat'];
				$infor_lon  = $infor_value['lon'];
				$infor_type = $infor_value['type'];
				$infor_data = $infor_value['data'];
				$infor_time = $infor_value['time'];
				$infor_message  = 0;
				$infor_image    = 0;
				$infor_voice    = 0;
				$infor_video    = 0;
				//$infor_data     = array();
				switch ($infor_type) {
					case INFO_MESSAGE:
						$infor_message = 1;
						break;
					case INFO_IMAGE:
						$infor_image = 1;
						break;
					case INFO_VOICE:
						$infor_voice = 1;
						break;
					case INFO_VIDEO:
						$infor_video = 1;
						break;
					default:
						break;
				}

				if( isset($informations[$id_wkgps.$infor_lat.$infor_lon.$infor_time]) AND !empty($informations[$infor_lat.$infor_lon]) ){
					$infor_message  = $informations[$infor_lat.$infor_lon]['message']  + $infor_message;
					$infor_image    = $informations[$infor_lat.$infor_lon]['image']    + $infor_image;
					$infor_voice    = $informations[$infor_lat.$infor_lon]['voice']    + $infor_voice;
					$infor_video    = $informations[$infor_lat.$infor_lon]['video']    + $infor_video;
					$infor_data     = $informations[$infor_lat.$infor_lon]['data'];
				}
				$informations[$id_wkgps.$infor_lat.$infor_lon.$infor_time] = array(
					'time'      => $infor_time,
					'gps_id'    => $infor_id_gps,
					'lat'       => $infor_lat,
					'lon'       => $infor_lon,
					'message'   => $infor_message,
					'image'     => $infor_image,
					'voice'     => $infor_voice,
					'video'     => $infor_video,
					'data'      => $infor_data
				);
				unset($infor_result[$infor_key]);
			}# end gps_id  infor_id_gps
		}
		# activity
		$activiti_end_time = FALSE;
		$activitys = array();
		$activiti_duration_total = 0;
		foreach ($activity_result as $key => $value) {
			# ACTIVITI - start time, end time, position, activiti, street
			$activiti_id_wkgps  = $value['id_wkgps'];
			if($activiti_id_wkgps == $id_wkgps){
				$activiti_position  = '';
				$latitude           = $value['latitude'];
				$latitude_explode   = explode(',',$latitude);
				$longtitude         = $value['longtitude'];
				$longtitude_explode = explode(',',$longtitude);
				if( count($latitude_explode) == count($longtitude_explode) ){
					for ($i=0; $i < count($latitude_explode); $i++) {
						if($i < 1)
							$activiti_position = $latitude_explode[$i] . ' ' . $longtitude_explode[$i];
						else
							$activiti_position = $activiti_position . ',' . $latitude_explode[$i] . ' ' . $longtitude_explode[$i];
					}
				}

				$activiti_date               = date('Y-m-d',strtotime($value['starttime']));
				$activiti_start              = date('H:i:s',strtotime($value['starttime']));
				$activiti_end                = date('H:i:s',strtotime($value['endtime']));
				# Check start end time
				if($activiti_end_time != FALSE){
					if( (strtotime($activiti_start) > strtotime($activiti_end_time)) AND ((strtotime($activiti_start) - strtotime($activiti_end_time)) > 1) ){
						$activiti_start = date('H:i:s',strtotime($activiti_end_time) + 1);
					}
				}
				$activiti_end_time = $activiti_end;

				//$activiti_duration           = intval($value['duration']);
				$activiti_duration           = strtotime($value['endtime']) - strtotime($value['starttime']);
				$activiti_duration_total     += $activiti_duration;
				$activiti_duration_total_tmp = sprintf("%02d%s%02d%s%02d",floor($activiti_duration_total/3600), ':', ($activiti_duration_total/60)%60, ':', $activiti_duration_total%60);
				$activiti_duration           = sprintf("%02d%s%02d%s%02d",floor($activiti_duration/3600), ':', ($activiti_duration/60)%60, ':', $activiti_duration%60);
				$activiti_street             = isset($value['street']) ? $value['street'] : '';
				$activiti_id                 = $value['id_activity'];
				$activiti_name               = isset($_activiti_data[$activiti_id]['name']) ? $_activiti_data[$activiti_id]['name'] : '';
				$activiti_color              = isset($_activiti_data[$activiti_id]['color']) ? $_activiti_data[$activiti_id]['color'] : '';

				# Infor mation activiti
				$activiti_message   = 0;
				$activiti_image     = 0;
				$activiti_voice     = 0;
				$activiti_video     = 0;

				foreach ($informations as $infor_key => $infor_value) {
					if((strtotime($infor_value['time']) >= strtotime($activiti_date . ' ' . $activiti_start) AND (strtotime($infor_value['time']) <= strtotime($activiti_date . ' ' . $activiti_end)) )) {
						$activiti_message   = $activiti_message + $infor_value['message'];
						$activiti_image     = $activiti_image + $infor_value['image'];
						$activiti_voice     = $activiti_voice + $infor_value['voice'];
						$activiti_video     = $activiti_video + $infor_value['video'];
					}
				}
				$activitys[] = array(
					'id_wkgps'       => $id_wkgps,
					'latitude'       => isset($latitude_explode[0]) ? $latitude_explode[0] : '',
					'longtitude'     => isset($longtitude_explode[0]) ? $longtitude_explode[0] : '',
					'starttime'      => $activiti_date . ' ' . $activiti_start,
					'endtime'        => $activiti_date . ' ' . $activiti_end,
					'date'           => isset($activiti_date) ? $activiti_date : '',
					'start'          => isset($activiti_start) ? $activiti_start : '',
					'end'            => isset($activiti_end) ? $activiti_end : '',
					'duration'       => isset($activiti_duration) ? $activiti_duration : '',
					'duration_total' => isset($activiti_duration_total_tmp) ? $activiti_duration_total_tmp : '',
					'street'         => isset($activiti_street) ? $activiti_street : '',
					'color'          => isset($activiti_color) ? $activiti_color : '',
					'acname'         => isset($activiti_name) ? $activiti_name : '',
					'position'       => isset($activiti_position) ? $activiti_position : '',
					'message'        => $activiti_message,
					'image'          => $activiti_image,
					'voice'          => $activiti_voice,
					'video'          => $activiti_video,
				);
				unset($activity_result[$key]);
			}
		}
		$return = array(
			'version'     => config_item('api_version'),
			'status'      => STATUS_SUCCESS,
			'data'    => array(
				'activity' => $activitys,
				'information' => $informations
			)
		);
		#_debug($return);die;
		return $this->response($return,REST_CODE_OK);
	}

	function index_get(){
		$id_module = $this->get('id_module');
		if(!isset($id_module) OR empty($id_module)){
			$id_module = FALSE;
		}

		$data_return = array();
		# color activity
		$color = 'black';

		# id city
		$id_city_array = array();
		$id_city = trim($this->get('id_city'));
		if(isset($id_city) AND !empty($id_city)){
			$id_city_array = convert_param_array($id_city);
		}
		if(!is_array($id_city_array) OR empty($id_city_array)){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Id_city not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		# id machine
		$id_machine_array = array();
		$id_machine = trim($this->get('machine'));
		if(isset($id_machine) AND !empty($id_machine)){
			$id_machine_array = convert_param_array($id_machine);
		}

		# id worker
		$id_worker_array = array();
		$id_worker = trim($this->get('worker'));
		if(isset($id_worker) AND !empty($id_worker)){
			$id_worker_array = convert_param_array($id_worker);
		}

		# street name
		$street_name = trim($this->get('street'));

		# day
		$day = $this->get('day');

		# limit
		$limit = (intval($this->get('limit')) === 0)?100:intval($this->get('limit'));
		$offset = $this->get('offset');
		if(!isset($offset) || empty($offset)) {
			$offset = 0;
		}

		if( (isset($id_city_array) && !empty($id_city_array)) || (isset($id_machine_array) && !empty($id_machine_array)) || (isset($id_worker_array) && !empty($id_worker_array)) || (isset($day) && !empty($day)) ){
			# Get data activiti
			$activiti_data = array();
			$this->db->where('deleted_at',NULL);
			$activiti_data = $this->db->get(TBL_ACTIVITIES)->result_array();
			# Get data machine
			$machine_data = array();
			$this->db->where('deleted_at',NULL);
			$machine_data = $this->db->get(TBL_MACHINE)->result_array();
			# Get data worker
			$worker_data = array();
			$this->db->where('deleted_at',NULL);
			$worker_data = $this->db->get(TBL_WORKER)->result_array();
			#return $this->response($activiti_data);
			if( (!is_array($activiti_data) OR empty($activiti_data)) OR (!is_array($machine_data) OR empty($machine_data)) OR (!is_array($worker_data) OR empty($worker_data)) ){
				$return = array(
						'version'   => config_item('api_version'),
						'status'    => STATUS_FAIL,
						'msg'       => 'Data activity empty!'
				);
				return $this->response($return,REST_CODE_PARAM_ERR);
			}
			# record gps
			$gps_result = $this->gpsm->record_gps_get_where($id_city_array, $id_module, $id_machine_array, $id_worker_array, $street_name, $day, $limit, $offset);
			if( !isset($gps_result) OR empty($gps_result) ){
				$return = array(
						'version'   => config_item('api_version'),
						'status'    => STATUS_FAIL,
						'msg'       => 'Data GPS empty !'
				);
				return $this->response($return,REST_CODE_PARAM_ERR);
			}
			$gps_total = $this->gpsm->record_gps_get_where($id_city_array, $id_module, $id_machine_array, $id_worker_array, $street_name, $day, $limit, $offset, TRUE);

			# array id gps
			$id_wk_gps_array    = $this->_array_colum($gps_result,'id');
			# get activity by gps
			$activiti_result    = $this->gpsm->activiti_get_where($id_wk_gps_array);
			# get infor by gps
			$infor_result       = $this->gpsm->infor_get_where($id_wk_gps_array);
			#var_dump($infor_result);die;
			foreach ($gps_result as $gps_key => $gps_value) {
				$id_module = isset($gps_value['app']) ? $gps_value['app'] : FALSE;
				if($id_module != FALSE){
					$detail_module = array();
					$detail_module = $this->db->get_where(TBL_MODULES,array('id' => $id_module))->row_array();
					if(!isset($detail_module) OR empty($detail_module)){
						$return = array(
								'version' => config_item('api_version'),
								'status'  => STATUS_FAIL,
								'msg'     => 'Data module not found !'
						);
						return  $this->response($return,REST_CODE_PARAM_ERR);
					}
					$module_name = isset($detail_module['title']) ? $detail_module['title'] : FALSE;
					# GPS - id , ontime,position, machin, worker
					$gps_id             = $gps_value['id'];
					$gps_ontime         = $gps_value['ontime'];
					$gps_id_machine     = $gps_value['id_machine'];
					$gps_mc_name        = '';
					foreach ($machine_data as $key => $value) {
						if( isset($value['id']) AND ($value['id'] == $gps_id_machine) ){
							$gps_mc_name = isset($value['name']) ? $value['name'] : '';
							break;
						}
					}
					$gps_id_wk          = $gps_value['id_worker'];
					$gps_wk_first_name  = '';
					$gps_wk_last_name   = '';
					foreach ($worker_data as $key => $value) {
						if( isset($value['id']) AND ($value['id'] == $gps_id_wk) ){
							$gps_wk_first_name  = isset($value['first_name']) ? $value['first_name'] : '';
							$gps_wk_last_name   = isset($value['last_name']) ? $value['last_name'] : '';
							break;
						}
					}
					$gps_position       = '';
					if(isset($gps_value['position_clean']) AND !empty($gps_value['position_clean'])){
						$gps_position       = $gps_value['position_clean'];
					} else {
						$gps_position       = $gps_value['position'];
					}

					$gps_position       = trim($gps_position);
					$gps_position       = trim($gps_position, ',');
					$gps_position       = trim(trim($gps_position, 'LINESTRING('));
					$gps_position       = trim(trim($gps_position, ')'));
					$gps_status         = $gps_value['status'];

					# Information - gps_id, lat, lon
					$information_array = array();
					foreach ($infor_result as $infor_key => $infor_value) {
						$infor_id_gps   = $infor_value['id_gps'];
						if($infor_id_gps == $gps_id){
							$infor_lat  = $infor_value['lat'];
							$infor_lon  = $infor_value['lon'];
							$infor_type = $infor_value['type'];
							$infor_data = $infor_value['data'];
							$infor_time = $infor_value['time'];
							$infor_message  = 0;
							$infor_image    = 0;
							$infor_voice    = 0;
							$infor_video    = 0;
							//$infor_data     = array();
							switch ($infor_type) {
								case INFO_MESSAGE:
									$infor_message = 1;
									break;
								case INFO_IMAGE:
									$infor_image = 1;
									break;
								case INFO_VOICE:
									$infor_voice = 1;
									break;
								case INFO_VIDEO:
									$infor_video = 1;
									break;
								default:
									break;
							}

							if( isset($information_array[$gps_id.$infor_lat.$infor_lon.$infor_time]) AND !empty($information_array[$infor_lat.$infor_lon]) ){
								$infor_message  = $information_array[$infor_lat.$infor_lon]['message']  + $infor_message;
								$infor_image    = $information_array[$infor_lat.$infor_lon]['image']    + $infor_image;
								$infor_voice    = $information_array[$infor_lat.$infor_lon]['voice']    + $infor_voice;
								$infor_video    = $information_array[$infor_lat.$infor_lon]['video']    + $infor_video;
								$infor_data     = $information_array[$infor_lat.$infor_lon]['data'];
							}
							/*
							$infor_data[]   = array(
								'time'  => $infor_time,
								'data'  => $infor_data
							);
							*/
							$information_array[$gps_id.$infor_lat.$infor_lon.$infor_time] = array(
								'time'      => $infor_time,
								'gps_id'    => $infor_id_gps,
								'lat'       => $infor_lat,
								'lon'       => $infor_lon,
								'message'   => $infor_message,
								'image'     => $infor_image,
								'voice'     => $infor_voice,
								'video'     => $infor_video,
								'data'  => $infor_data
							);
							unset($infor_result[$infor_key]);
						}# end gps_id  infor_id_gps
					}

					# Activiti
					$activiti_end_time = FALSE;
					$activiti_array = array();
					$activiti_duration_total = 0;
					foreach ($activiti_result as $activiti_key => $activiti_value) {
						# ACTIVITI - start time, end time, position, activiti, street
						$activiti_id_wkgps  = $activiti_value['id_wkgps'];
						if($activiti_id_wkgps == $gps_id){
							$activiti_position  = '';
							$latitude           = $activiti_value['latitude'];
							$latitude_explode   = explode(',',$latitude);
							$longtitude         = $activiti_value['longtitude'];
							$longtitude_explode = explode(',',$longtitude);
							if( count($latitude_explode) == count($longtitude_explode) ){
								for ($i=0; $i < count($latitude_explode); $i++) {
									if($i < 1)
										$activiti_position = $latitude_explode[$i] . ' ' . $longtitude_explode[$i];
									else
										$activiti_position = $activiti_position . ',' . $latitude_explode[$i] . ' ' . $longtitude_explode[$i];
								}
							}

							$activiti_date               = date('Y-m-d',strtotime($activiti_value['starttime']));
							$activiti_start              = date('H:i:s',strtotime($activiti_value['starttime']));
							$activiti_end                = date('H:i:s',strtotime($activiti_value['endtime']));
							# Check start end time
							if($activiti_end_time != FALSE){
								if( (strtotime($activiti_start) > strtotime($activiti_end_time)) AND ((strtotime($activiti_start) - strtotime($activiti_end_time)) > 1)){
									$activiti_start = date('H:i:s',strtotime($activiti_end_time) + 1);
								}
							}
							$activiti_end_time = $activiti_end;

							//$activiti_duration           = intval($activiti_value['duration']);
							$activiti_duration           = strtotime($activiti_value['endtime']) - strtotime($activiti_value['starttime']);
							$activiti_duration_total     += $activiti_duration;
							$activiti_duration_total_tmp = sprintf("%02d%s%02d%s%02d",floor($activiti_duration_total/3600), ':', ($activiti_duration_total/60)%60, ':', $activiti_duration_total%60);
							$activiti_duration           = sprintf("%02d%s%02d%s%02d",floor($activiti_duration/3600), ':', ($activiti_duration/60)%60, ':', $activiti_duration%60);
							$activiti_street             = isset($activiti_value['street']) ? $activiti_value['street'] : '';
							$activiti_id                 = $activiti_value['id_activity'];
							$activiti_name               = '';
							$activiti_color              = '';
							foreach ($activiti_data as $key => $value) {
								if( isset($value['id']) AND ($value['id'] == $activiti_id) ){
									$activiti_name  = isset($value['name']) ? $value['name'] : '';
									$activiti_color = isset($value['color']) ? $value['color'] : '#000000';
									break;
								}
							}

							# Infor mation activiti
							$activiti_message   = 0;
							$activiti_image     = 0;
							$activiti_voice     = 0;
							$activiti_video     = 0;
							#var_dump($infor_result);die;
							#var_dump($activiti_value['id_wkgps']);die;

							foreach ($information_array as $infor_key => $infor_value) {
								if( (strtotime($infor_value['time']) >= strtotime($activiti_date . ' ' . $activiti_start)) AND (strtotime($infor_value['time']) <= strtotime($activiti_date . ' ' . $activiti_end)) ){
									$activiti_message   = $activiti_message + $infor_value['message'];
									$activiti_image     = $activiti_image + $infor_value['image'];
									$activiti_voice     = $activiti_voice + $infor_value['voice'];
									$activiti_video     = $activiti_video + $infor_value['video'];
									#unset($information_array[$infor_key]);
								}
							}
							$activiti_array[] = array(
								'id_wkgps'       => $gps_id,
								'latitude'       => isset($latitude_explode[0]) ? $latitude_explode[0] : '',
								'longtitude'     => isset($longtitude_explode[0]) ? $longtitude_explode[0] : '',
								'starttime'      => $activiti_date . ' ' . $activiti_start,
								'endtime'        => $activiti_date . ' ' . $activiti_end,
								'date'           => isset($activiti_date) ? $activiti_date : '',
								'start'          => isset($activiti_start) ? $activiti_start : '',
								'end'            => isset($activiti_end) ? $activiti_end : '',
								'duration'       => isset($activiti_duration) ? $activiti_duration : '',
								'duration_total' => isset($activiti_duration_total_tmp) ? $activiti_duration_total_tmp : '',
								'street'         => isset($activiti_street) ? $activiti_street : '',
								'color'          => isset($activiti_color) ? $activiti_color : '',
								'acname'         => isset($activiti_name) ? $activiti_name : '',
								'position'       => isset($activiti_position) ? $activiti_position : '',
								'message'        => $activiti_message,
								'image'          => $activiti_image,
								'voice'          => $activiti_voice,
								'video'          => $activiti_video,
							);
							unset($activiti_result[$activiti_key]);
						}# end if gpg_id = activiti_id_gps
					}
					# position activiti
					$_check = '';
					$count_activiti = 0;
					foreach ($activiti_array as $position_key => $position_value) {
						$color = (isset($position_value['color']) && !empty($position_value['color'])) ? $position_value['color'] : 'black';
						$check = $position_value['latitude'].' '.$position_value['longtitude'];
						if($count_activiti < 1){
							$gps_position = 'color '.$color.','.$gps_position;
						} else {
							if($_check != $check){
								$_check = $check;
								$first = $this->gpsm->lastIndexOf($gps_position,$check);
								$gps_position = substr($gps_position, 0,$first).'color '.$color.','.substr($gps_position,$first);
							}
						}
						$count_activiti++;
					}
					# Add result
					$gps_position = 'LINESTRING('.$gps_position.')';
					$data_return[] = array(
						'id'            => $gps_id,
						'ontime'        => $gps_ontime,
						'status'        => $gps_status,
						'mcname'        => $gps_mc_name,
						'id_worker'     => $gps_id_wk,
						'first_name'    => $gps_wk_first_name,
						'last_name'     => $gps_wk_last_name,
						'position'      => $gps_position,
						'activity'      => $activiti_array,
						'information'   => $information_array
					);
				}
			}# end for gps
			$return = array(
					'version'   => config_item('api_version'),
					'status'    => STATUS_SUCCESS,
					'total'     => $gps_total,
					'data'      => $data_return
			);
			#_debug($return);die;
			return $this->response($return,REST_CODE_OK);
		} else{# en if where
			$return = array(
					'version'   => config_item('api_version'),
					'status'    => STATUS_FAIL
			);
			return $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function _array_colum($data = array(),$field = ''){
		$return = array();
		if(isset($field) && !empty($field)){
			if(is_array($data) AND !empty($data)){
				if(isset($data[0][$field]) && !empty($data[0][$field])){
					foreach ($data as $key => $value) {
						$return[] = $value[$field];
					}
				}
			}
		}
		return $return;
	}

	function _array_where($data = array(), $where = array()){
		$return = array();
		if(is_array($data) AND !empty($data)){
			if(is_array($where) AND !empty($where)){
				foreach ($data as $key => $value) {

				}
			} else {
				$return = $data;
			}
		}
		return $return;
	}
}