<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
class Machine extends REST_Controller{
	private $id_module  = 1;
	private $_id_city   = 1;

	function __construct(){
		parent:: __construct();

		$this->load->model('push_server_update_model','model_server_update');

		if (isset($this->_tenantId) && !empty($this->_tenantId)){
			$this->_id_city = intval($this->_tenantId);
		} else {
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function activities_machine_get(){
		$id_machine = $this->get('id_machine');
		if(!isset($id_machine) OR empty($id_machine)){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'id_machine not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		# get machine-module
		$machine_module = array();
		$this->db->where('id_machine'   ,$id_machine);
		$this->db->where('deleted_at'   ,NULL);
		$machine_module = $this->db->get('machine_module')->result_array();
		if(!isset($machine_module) OR empty($machine_module)){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Data machine-module empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		# get id-machine-module
		$id_machine_module = FALSE;
		foreach ($machine_module as $key => $value) {
			$id_machine_module[] = $value['id'];
		}
		isset($machine_module['id']) ? $machine_module['id'] : FALSE;
		if($id_machine_module == FALSE){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Data id_machine_module empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		# get machine - module-activiti
		$machine_module_activiti = array();
		$this->db->where_in('id_machine_module',$id_machine_module);
		$this->db->where('deleted_at'       ,NULL);
		$machine_module_activiti = $this->db->get('machine_module_activity')->result_array();

		if(is_array($machine_module_activiti) AND !empty($machine_module_activiti)){
			$return_module_activiti = array();
			foreach ($machine_module as $key => $value) {
				$return_id_module           = $value['id_module'];
				$return_id_mechine_module   = $value['id'];
				$activiti_item = array();
				foreach ($machine_module_activiti as $activiti_key => $activiti_value) {
					if($activiti_value['id_machine_module'] == $return_id_mechine_module){
						$activiti_item['id_module']			= $return_id_module;
						$activiti_item['activiti'][]        = $activiti_value['id_activity'];
						$activiti_item['activiti_default']  = $value['id_default'];
						unset($machine_module_activiti[$activiti_key]);
					}
				}
				if(is_array($activiti_item) AND !empty($activiti_item)){
					$return_module_activiti[] = $activiti_item;
				}
			}
			$return = array(
					'version'   => config_item('api_version'),
					'status'    => STATUS_SUCCESS,
					'data'      => $return_module_activiti
			);
			return  $this->response($return,REST_CODE_OK);
		} else {
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Data machine_module_activity empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function addon_machine_get(){
		$id_machine = $this->get('id_machine');
		if(!isset($id_machine) OR empty($id_machine)){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'id_machine not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		# get machine-module
		$machine_module = array();
		$this->db->where('id_machine'   ,$id_machine);
		$this->db->where('deleted_at'   ,NULL);
		$machine_module = $this->db->get('machine_module')->result_array();
		if(!isset($machine_module) OR empty($machine_module)){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Data machine-module empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		# id-machine-module
		$id_machine_module = FALSE;
		foreach ($machine_module as $key => $value) {
			$id_machine_module[] = $value['id'];
		}
		isset($machine_module['id']) ? $machine_module['id'] : FALSE;
		if($id_machine_module == FALSE){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Data id_machine_module empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		# get machine-addon-module by id_machine_module
		$machine_addon_module = array();
		if(is_array($id_machine_module) AND !empty($id_machine_module)){
			$this->db->where('deleted_at' , NULL);
			$this->db->where_in('id_machine_module' , $id_machine_module);
			$machine_addon_module = $this->db->get(TBL_MACHINE_ADDON_MODULE)->result_array();
		}

		# id-addon-module
		$id_addon_module = array();
		if(is_array($machine_addon_module) AND !empty($machine_addon_module)){
			foreach ($machine_addon_module as $key => $value) {
				$id_addon_module[] = $value['id_addon_module'];
			}
		}

		# get addon-module-activities
		$addon_module = array();
		if(is_array($id_addon_module) AND !empty($id_addon_module)){
			$this->db->where('deleted_at' , NULL);
			$this->db->where_in('id' , $id_addon_module);
			$addon_module = $this->db->get(TBL_ADDON_MODULE)->result_array();
		}

		# return
		$return_data = array();
		foreach ($machine_module as $key => $value) {
			$return_id_module           = $value['id_module'];
			$return_id_mechine_module   = $value['id'];
			$addon_item = array();
			# addon
			foreach ($addon_module as $addon_key => $addon_value) {
				if($addon_value['id_module'] == $return_id_module){
					$addon_item['addon'][] = $addon_value['id_addon'];
					unset($addon_module[$addon_key]);
				}
			}
			if(is_array($addon_item) AND !empty($addon_item)){
				$return_data[$return_id_module] = $addon_item;
			}
		}
		$return = array(
				'version'   => config_item('api_version'),
				'status'    => STATUS_SUCCESS,
				'data'      => $return_data
		);
		return  $this->response($return,REST_CODE_OK);
	}

	# get
	function index_get(){
		$id_machine = $this->get('id_machine');
		if(!isset($id_machine) OR empty($id_machine)){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'id_machine not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		# get machine-module
		$machine_module = array();
		$this->db->where('id_machine'   ,$id_machine);
		$this->db->where('deleted_at'   ,NULL);
		$machine_module = $this->db->get('machine_module')->result_array();
		if(!isset($machine_module) OR empty($machine_module)){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Data machine-module empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		# id-machine-module
		$id_machine_module = FALSE;
		foreach ($machine_module as $key => $value) {
			$id_machine_module[] = $value['id'];
		}
		isset($machine_module['id']) ? $machine_module['id'] : FALSE;
		if($id_machine_module == FALSE){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Data id_machine_module empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		# get machine - module-activiti
		$machine_module_activiti = array();
		if(is_array($id_machine_module) AND !empty($id_machine_module)){
			$this->db->where('deleted_at'       ,NULL);
			$this->db->where_in('id_machine_module',$id_machine_module);
			$machine_module_activiti = $this->db->get(TBL_MACHINE_MODULE_ACTIVITI)->result_array();
		}

		# get machine-addon-module by id_machine_module
		$machine_addon_module = array();
		if(is_array($id_machine_module) AND !empty($id_machine_module)){
			$this->db->where('deleted_at' , NULL);
			$this->db->where_in('id_machine_module' , $id_machine_module);
			$machine_addon_module = $this->db->get(TBL_MACHINE_ADDON_MODULE)->result_array();
		}

		# id-addon-module
		$id_addon_module = array();
		if(is_array($machine_addon_module) AND !empty($machine_addon_module)){
			foreach ($machine_addon_module as $key => $value) {
				$id_addon_module[] = $value['id_addon_module'];
			}
		}

		# get addon-module-activities
		$addon_module = array();
		if(is_array($id_addon_module) AND !empty($id_addon_module)){
			$this->db->where('deleted_at' , NULL);
			$this->db->where_in('id' , $id_addon_module);
			$addon_module = $this->db->get(TBL_ADDON_MODULE)->result_array();
		}

		# return
		$return_data = array();
		foreach ($machine_module as $key => $value) {
			$return_id_module           = $value['id_module'];
			$return_id_mechine_module   = $value['id'];
			$activiti_addon_item = array();
			# activities
			foreach ($machine_module_activiti as $activiti_key => $activiti_value) {
				if($activiti_value['id_machine_module'] == $return_id_mechine_module){
					$activiti_addon_item['activiti'][]          = $activiti_value['id_activity'];
					$activiti_addon_item['activiti_default']    = $value['id_default'];
					unset($machine_module_activiti[$activiti_key]);
				}
			}

			# addon
			foreach ($addon_module as $addon_key => $addon_value) {
				if($addon_value['id_module'] == $return_id_module){
					$activiti_addon_item['addon'][] = $addon_value['id_addon'];
					unset($addon_module[$addon_key]);
				}
			}
			$return_data[$return_id_module] = $activiti_addon_item;
		}
		$return = array(
				'version'   => config_item('api_version'),
				'status'    => STATUS_SUCCESS,
				'data'      => $return_data
		);
		return  $this->response($return,REST_CODE_OK);
	}

	# post
	function index_post(){
		$this->load->helper('pheanstalk');
		$job = array(
			"id_city"       => $this->_id_city,
			"controllers"   => "machine",
			"type"          => "post"
		);
		push_job(QUE_SERVER_UPDATE, json_encode($job));


		#$machine_id  = $this->put('id');
		$identifier  = $this->post('identifier');
		$id_city     = $this->post('id_city');
		$nfc_code    = $this->post('nfc_code');
		$name        = $this->post('name');
		$description = $this->post('description');
		$module      = $this->post('module');

		#insert machine
		$data_machine_insert = array(
			'id_ref'        => isset($id_ref)       ? $id_ref       : '',
			'id_city'       => isset($id_city)      ? $id_city      : '',
			'identifier'    => isset($identifier)   ? $identifier   : '',
			'nfc_code'      => isset($nfc_code)     ? $nfc_code     : '',
			'name'          => isset($name)         ? $name         : '',
			'created_at'    => date('Y-m-d H:i:s')
		);
		$flag = $this->db->insert(TBL_MACHINE,$data_machine_insert);
		$machine_id     = FALSE;
		if($flag != FALSE){
			$machine_id = $this->db->insert_id();
		} else {
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Insert table : machine fail !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		if($machine_id == FALSE)
		{
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'machine_id fail !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		# module
		if(!isset($module) OR empty($module)){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Module Empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		$machine_module_activiti = array();
		$machine_addon_module = array();
		foreach ($module as $key => $value) {
			$id_module          = isset($value['id_module']) ? $value['id_module'] : '';
			$addon              = isset($value['addon']) ? $value['addon'] : array();
			$activity           = isset($value['activity']) ? $value['activity'] : array();
			$activity_default   = isset($value['activity_default']) ? $value['activity_default'] : '';
			# machin-module
			$machine_molule = array(
				'id_machine'    => $machine_id,
				'id_module'     => $id_module,
				'id_default'    => $activity_default,
				'created_at'    => date('Y-m-d H:s:i')
			);
			$flag = $this->db->insert(TBL_MACHINE_MODULE ,$machine_molule);
			$id_machine_module = FALSE;
			if($flag != FALSE){
				$id_machine_module = $this->db->insert_id();
			} else {
				$return = array(
						'version'   => config_item('api_version'),
						'status'    => STATUS_FAIL,
						'msg'       => 'Insert table : machine_module fail !',
						'query'     => $this->db->last_query()
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}

			# machine-module-activiti
			foreach ($activity as $activity_key => $activity_value) {
				$machine_module_activiti[] = array(
					'id_machine_module' => $id_machine_module,
					'id_activity'       => $activity_value,
					'created_at'        => date('Y-m-d H:s:i')
				);
			}

			# addon-module
			$addon_module = array();
			if(is_array($addon) AND !empty($addon)){
				$this->db->where('deleted_at'   , NULL);
				$this->db->where_in('id_module'	, $id_module);
				$this->db->where_in('id_addon'  , $addon);
				$addon_module = $this->db->get(TBL_ADDON_MODULE)->result_array();
			}

			# machine-addon-module
			if(is_array($addon_module) AND !empty($addon_module)){
				foreach ($addon_module as $addon_module_key => $addon_module_value) {
					$machine_addon_module[] = array(
						'id_machine_module' => $id_machine_module,
						'id_addon_module'   => $addon_module_value['id'],
						'deleted_at'        => date('Y-m-d H:i:s')
					);
				}
			}
		}

		# insert machine_module_activiti
		if(is_array($machine_module_activiti) AND !empty($machine_module_activiti)){
			$flag = $this->db->insert_batch(TBL_MACHINE_MODULE_ACTIVITI , $machine_module_activiti);
			if($flag === FALSE){
				$return = array(
						'version'   => config_item('api_version'),
						'status'    => STATUS_FAIL,
						'msg'       => 'Insert table : machine_module_activiti - fail !',
						'query'     => $this->db->last_query()
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}
		}

		# insert machine_addon_module
		if(is_array($machine_addon_module) AND !empty($machine_addon_module)){
			$flag = $this->db->insert_batch(TBL_MACHINE_ADDON_MODULE , $machine_addon_module);
			if($flag === FALSE){
				$return = array(
						'version'   => config_item('api_version'),
						'status'    => STATUS_FAIL,
						'msg'       => 'Insert table : machine_addon_module - fail !',
						'query'     => $this->db->last_query()
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}
		}
		$push_server = $this->model_server_update->server_update();
		# return
		$return = array(
				'version'   => config_item('api_version'),
				'status'    => STATUS_SUCCESS
		);
		return  $this->response($return,REST_CODE_OK);
	}

	# put
	function index_put(){
		$this->load->helper('pheanstalk');
		$job = array(
			"id_city"       => $this->_id_city,
			"controllers"   => "machine",
			"type"          => "put"
		);
		push_job(QUE_SERVER_UPDATE, json_encode($job));


		$machine_id  = $this->put('id');
		$identifier  = $this->put('identifier');
		$id_city     = $this->put('id_city');
		$nfc_code    = $this->put('nfc_code');
		$name        = $this->put('name');
		$description = $this->put('description');
		$module      = $this->put('module');
		//print_r($module);
		//die();
		#insert machine
		$data_machine = array(
			'id_ref'        => isset($id_ref)       ? $id_ref       : '',
			'id_city'       => isset($id_city)      ? $id_city      : '',
			'identifier'    => isset($identifier)   ? $identifier   : '',
			'nfc_code'      => isset($nfc_code)     ? $nfc_code     : '',
			'name'          => isset($name)         ? $name         : '',
			'updated_at'    => date('Y-m-d H:i:s')
		);
		$this->db->where('id' , $machine_id);
		$flag = $this->db->update(TBL_MACHINE,$data_machine);
		if($flag == FALSE){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Update machine fail !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		# delete
		$delete_machine_molule          = array();
		$_id_machine_molule             = array();
		$delete_machine_module_activiti = array();
		$delete_machine_addon_module    = array();
		# get id machin-module
		$this->db->where('deleted_at' , NULL);
		$this->db->where('id_machine' , $machine_id);
		$delete_machine_molule = $this->db->get(TBL_MACHINE_MODULE)->result_array();
		if(is_array($delete_machine_molule) AND !empty($delete_machine_molule)){
			foreach ($delete_machine_molule as $key => $value) {
				$_id_machine_molule[] = $value['id'];
			}
		}
		if(is_array($_id_machine_molule) AND !empty($_id_machine_molule)){
			# delete machin-module
			$this->db->where('deleted_at' , NULL);
			$this->db->where('id_machine' , $machine_id);
			$flag = $this->db->update(TBL_MACHINE_MODULE, array('deleted_at' => date('Y-m-d H:i:s')));
			if($flag == FALSE){
				$return = array(
					'version'   => config_item('api_version'),
					'status'    => STATUS_FAIL,
					'msg'       => 'updated ' . TBL_MACHINE_MODULE . ' fail !',
					'query'     => $this->db->last_query()
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}

			# delete machin-module-activities
			$this->db->where('deleted_at' , NULL);
			$this->db->where_in('id_machine_module' , $_id_machine_molule);
			$flag = $this->db->update(TBL_MACHINE_MODULE_ACTIVITI, array('deleted_at' => date('Y-m-d H:i:s')));
			if($flag == FALSE){
				$return = array(
					'version'   => config_item('api_version'),
					'status'    => STATUS_FAIL,
					'msg'       => 'updated ' . TBL_MACHINE_MODULE_ACTIVITI . ' fail !',
					'query'     => $this->db->last_query()
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}

			# delete machin-addon-module
			$this->db->where('deleted_at' , NULL);
			$this->db->where_in('id_machine_module' , $_id_machine_molule);
			$flag = $this->db->update(TBL_MACHINE_ADDON_MODULE, array('deleted_at' => date('Y-m-d H:i:s')));
			if($flag == FALSE){
				$return = array(
					'version'   => config_item('api_version'),
					'status'    => STATUS_FAIL,
					'msg'       => 'updated ' . TBL_MACHINE_ADDON_MODULE . ' fail !',
					'query'     => $this->db->last_query()
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}
		}
		# module
		if(!isset($module) OR empty($module)){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Module Empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		$machine_module_activiti        = array();
		$machine_addon_module           = array();
		foreach ($module as $key => $value) {
			$id_module          = isset($value['id_module']) ? $value['id_module'] : '';
			$addon              = isset($value['addon']) ? $value['addon'] : array();
			$activity           = isset($value['activity']) ? $value['activity'] : array();
			$activity_default   = isset($value['activity_default']) ? $value['activity_default'] : '';

			# machin-module
			$machine_molule = array(
				'id_machine'    => $machine_id,
				'id_module'     => $id_module,
				'id_default'    => $activity_default,
				'created_at'    => date('Y-m-d H:s:i')
			);
			$flag = $this->db->insert(TBL_MACHINE_MODULE ,$machine_molule);
			$id_machine_module = FALSE;
			if($flag != FALSE){
				$id_machine_module = $this->db->insert_id();
			} else {
				$return = array(
						'version'   => config_item('api_version'),
						'status'    => STATUS_FAIL,
						'msg'       => 'Insert table : machine_module fail !',
						'query'     => $this->db->last_query()
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}

			# machine-module-activiti
			foreach ($activity as $activity_key => $activity_value) {
				$machine_module_activiti[] = array(
					'id_machine_module' => $id_machine_module,
					'id_activity'       => $activity_value,
					'created_at'        => date('Y-m-d H:s:i')
				);
			}

			# addon-module
			$addon_module = array();
			if(is_array($addon) AND !empty($addon)){
				$this->db->where('deleted_at'   , NULL);
				$this->db->where('id_module'    , $id_module);
				$this->db->where_in('id_addon'  , $addon);
				$addon_module = $this->db->get(TBL_ADDON_MODULE)->result_array();
			}
			# machine-addon-module
			if(is_array($addon_module) AND !empty($addon_module)){
				foreach ($addon_module as $addon_module_key => $addon_module_value) {
					$machine_addon_module[] = array(
						'id_machine_module' => $id_machine_module,
						'id_addon_module'   => $addon_module_value['id'],
						'created_at'        => date('Y-m-d H:i:s')
					);
				}
			}
		}

		# insert machine_module_activiti
		if(is_array($machine_module_activiti) AND !empty($machine_module_activiti)){
			$flag = $this->db->insert_batch(TBL_MACHINE_MODULE_ACTIVITI , $machine_module_activiti);
			if($flag === FALSE){
				$return = array(
						'version'   => config_item('api_version'),
						'status'    => STATUS_FAIL,
						'msg'       => 'Insert table : machine_module_activiti - fail !',
						'query'     => $this->db->last_query()
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}
		}

		# insert machine_addon_module
		if(is_array($machine_addon_module) AND !empty($machine_addon_module)){
			$flag = $this->db->insert_batch(TBL_MACHINE_ADDON_MODULE , $machine_addon_module);
			if($flag === FALSE){
				$return = array(
						'version'   => config_item('api_version'),
						'status'    => STATUS_FAIL,
						'msg'       => 'Insert table : machine_addon_module - fail !',
						'query'     => $this->db->last_query()
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}
		}
		$push_server = $this->model_server_update->server_update();
		# return
		$return = array(
				'version'   => config_item('api_version'),
				'status'    => STATUS_SUCCESS
		);
		return  $this->response($return,REST_CODE_OK);
	}
}
