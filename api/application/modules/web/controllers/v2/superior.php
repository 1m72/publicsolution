<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';

class Superior extends REST_Controller{
	private $id_module  = 1;
	private $_id_city   = 1;

	function __construct() {
		parent:: __construct();

		if (isset($this->_tenantId) && !empty($this->_tenantId)) {
			$this->_id_city = intval($this->_tenantId);
		} else {
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	// Get
	function index_get() {
		$id_worker = $this->get('id_worker');
		/*
		if (!isset($id_worker) OR empty($id_worker)) {
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'Data id_worker empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		*/
		$data = array();
		// get detail worker
		$worker_detail = array();
		$this->db->where('deleted_at'   , NULL);
		if (isset($id_worker) AND !empty($id_worker)) {
			$this->db->where('id'       , $id_worker);
		}
		#$worker_detail = $this->db->get(TBL_WORKER)->row_array();
		$worker_detail = $this->db->get(TBL_WORKER)->result_array();
		if (!isset($worker_detail) OR empty($worker_detail)) {
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Data worker empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		foreach ($worker_detail as $key => $value) {
			$item = array();
			$item['worker'] = $value;
			$worker_id = $value['id'];
			#
			$activi_result = array();
			$this->db->select('id_activity');
			$this->db->where('deleted_at'   , NULL);
			$this->db->where('id_employee'  , $worker_id);
			$activi_result = $this->db->get(TBL_WORKER_ACTIVITY_SAVED)->result_array();
			$activity = array();
			foreach ($activi_result as $key => $value) {
				$activity[] = $value['id_activity'];
			}
			$item['activity'] = $activity;
			// get module
			$module_result = array();
			$this->db->select('id_module');
			$this->db->where('deleted_at'   , NULL);
			$this->db->where('id_worker'    , $worker_id);
			$module_result = $this->db->get(TBL_WORKER_MODULES)->result_array();
			$module = array();
			foreach ($module_result as $key => $value) {
				$module[] = $value['id_module'];
			}
			$item['module'] = $module;
			$data[] = $item;
		}
		// return
		if (isset($id_worker) AND !empty($id_worker)) {
			$data = $data[0];
		}
		$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_SUCCESS,
				'data'    => $data
		);
		return  $this->response($return,REST_CODE_OK);
	}

	// Post
	function index_post() {
		$this->load->helper('pheanstalk');
		$job = array(
			'id_city'  => get_tenant_id(),
			'app'      => APP_FASTTASK_TEXT,
			'msg_key'  => 'server_update',
			'msg_body' => 'superior',
		);
		push_job(QUE_SERVER_UPDATE, json_encode($job));


		$id_ref         = $this->post('id_ref');
		$id_city        = $this->post('id_city');
		$id_company     = $this->post('id_company');
		$id_teamleader  = $this->post('id_teamleader');
		$nfc_code       = $this->post('nfc_code');
		$personal_code  = $this->post('personal_code');
		$first_name     = $this->post('first_name');
		$last_name      = $this->post('last_name');
		$address        = $this->post('address');
		$phone          = $this->post('phone');
		$birthday       = $this->post('birthday');
		$price          = $this->post('price');
		$id_activity    = $this->post('activity');
		$id_default     = $this->post('activity_default');
		$module         = $this->post('module');
		$data = array(
				'id_ref'        => $id_ref,
				'id_city'       => $id_city,
				'id_company'    => $id_company,
				'id_teamleader' => $id_teamleader,
				'nfc_code'      => $nfc_code,
				'personal_code' => $personal_code,
				'first_name'    => $first_name,
				'last_name'     => $last_name,
				'id_default'    => $id_default,
				'address'       => $address,
				'phone'         => $phone,
				'birthday'      => $birthday,
				'price'         => $price,
				'created_at'    => date('Y-m-d H:i:s')
		);
		$flag = $this->db->insert(TBL_WORKER,$data);
		$id_worker = FALSE;
		if ($flag != FALSE) {
			$id_worker = $this->db->insert_id();
		} else {
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'Insert worker fail !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		// insert activiti
		if (isset($id_activity) && !empty($id_activity)) {
			$activity_data  = array();
			foreach ($id_activity as $ac_item)
			{
				$item = array(
					'id_employee' => $id_worker,
					'id_activity' => $ac_item,
					'created_at'  => date('Y-m-d H:i:s')
				);
				$activity_data[] = $item;
			}
			if (is_array($activity_data) AND !empty($activity_data)) {
				$flag = $this->db->insert_batch(TBL_WORKER_ACTIVITY_SAVED,$activity_data);
			}
		}
		// insert moduls
		if (is_array($module) AND !empty($module)) {
			$module_data = array();
			foreach ($module as $key => $value) {
				$module_data[] = array(
					'id_worker'     => $id_worker,
					'id_module'     => $value,
					'created_at'    => date('Y-m-d H:i:s')
				);
			}
			if (is_array($module_data) AND !empty($module_data)) {
				$flag = $this->db->insert_batch(TBL_WORKER_MODULES,$module_data);
			}
		}

		// return
		$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_SUCCESS
		);
		return  $this->response($return,REST_CODE_OK);
	}

	// Put
	function index_put() {
		$this->load->helper('pheanstalk');
		$job = array(
			'id_city'  => get_tenant_id(),
			'app'      => APP_FASTTASK_TEXT,
			'msg_key'  => 'server_update',
			'msg_body' => 'superior',
		);
		push_job(QUE_SERVER_UPDATE, json_encode($job));

		$id_worker      = $this->put('id');
		$id_ref         = $this->put('id_ref');
		$id_city        = $this->put('id_city');
		$id_company     = $this->put('id_company');
		$id_teamleader  = $this->put('id_teamleader');
		$nfc_code       = $this->put('nfc_code');
		$personal_code  = $this->put('personal_code');
		$first_name     = $this->put('first_name');
		$last_name      = $this->put('last_name');
		$address        = $this->put('address');
		$phone          = $this->put('phone');
		$birthday       = $this->put('birthday');
		$price          = $this->put('price');
		$id_activity    = $this->put('activity');
		$id_default     = $this->put('activity_default');
		$module         = $this->put('module');
		$data = array(
			'id_ref'        => $id_ref,
			'id_city'       => $id_city,
			'id_company'    => $id_company,
			'id_teamleader' => $id_teamleader,
			'nfc_code'      => $nfc_code,
			'personal_code' => $personal_code,
			'first_name'    => $first_name,
			'last_name'     => $last_name,
			'id_default'    => $id_default,
			'address'       => $address,
			'phone'         => $phone,
			'birthday'      => $birthday,
			'price'         => $price,
			'updated_at'    => date('Y-m-d H:i:s')
		);
		$this->db->where('id',$id_worker);
		$flag = $this->db->update(TBL_WORKER,$data);
		if ($flag != TRUE) {
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'Update worker fail !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		// update deleted_at activiti
		$this->db->where('id_employee'  , $id_worker);
		$flag = $this->db->update(TBL_WORKER_ACTIVITY_SAVED, array('deleted_at' => date('Y-m-d H:i:s')));
		if ($flag != TRUE) {
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'Update worker_activity_saved fail !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		// update deleted_at module
		$this->db->where('id_worker'    , $id_worker);
		$flag = $this->db->update(TBL_WORKER_MODULES, array('deleted_at' => date('Y-m-d H:i:s')));
		if ($flag != TRUE) {
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'Update worker_module fail !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		// insert activiti
		if (isset($id_activity) && !empty($id_activity)) {
			// $array_activity = explode(',', $id_activity);
			$activity_data = array();
			foreach ($id_activity as $ac_item)
			{
				$item = array(
					'id_employee'   => $id_worker,
					'id_activity'   => $ac_item,
					'created_at'    => date('Y-m-d H:i:s')
				);
				$activity_data[] = $item;
			}
			if (is_array($activity_data) AND !empty($activity_data)) {
				$flag = $this->db->insert_batch(TBL_WORKER_ACTIVITY_SAVED,$activity_data);
			}
		}
		// insert moduls
		if (is_array($module) AND !empty($module)) {
			$module_data = array();
			foreach ($module as $key => $value) {
				$module_data[] = array(
					'id_worker'     => $id_worker,
					'id_module'     => $value,
					'created_at'    => date('Y-m-d H:i:s')
				);
			}
			if (is_array($module_data) AND !empty($module_data)) {
				$flag = $this->db->insert_batch(TBL_WORKER_MODULES,$module_data);
			}
		}

		// return
		$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_SUCCESS
		);
		return  $this->response($return,REST_CODE_OK);
	}

	// Delete
	function index_delete() {
		$this->load->helper('pheanstalk');
		$job = array(
			'id_city'  => get_tenant_id(),
			'app'      => APP_FASTTASK_TEXT,
			'msg_key'  => 'server_update',
			'msg_body' => 'superior',
		);
		push_job(QUE_SERVER_UPDATE, json_encode($job));

		$idworker = intval($this->delete('id_worker'));
		$wk_gps = $this->db->get_where(TBL_WORKER_GPS,array('id_worker'=>$idworker))->result_array();
		#start if $wk_gps
		if (count($wk_gps)>0)
		{
			foreach ($wk_gps as $key => $value) {
				if (count($this->db->get_where(TBL_WORKER_INFORMATION,array('id_worker_gps'=>$value['id']))->result_array())>0) {
					$update_wk_info = $this->wk_info->update(TBL_WORKER_INFORMATION,array('id_worker_gps'=>$value['id']),array('deleted_at'=>date('Y-m-d H:i:s')));
					if (!$update_wk_info) {
						$return = array(
								'version'  => config_item('api_version'),
								'status'   => STATUS_FAIL
						);
						return  $this->response($return,REST_CODE_PARAM_ERR);
					}
				}
				if (count($this->db->get_where(TBL_WORKER_ACTIVITY,array('id_wkgps'=>$value['id']))->result_array())>0) {
					$update_wk_acti = $this->wk_acti->update(TBL_WORKER_ACTIVITY,array('id_wkgps'=>$value['id']),array('deleted_at'=>date('Y-m-d H:i:s')));
					if (!$update_wk_acti) {
						$return = array(
								'version'  => config_item('api_version'),
								'status'   => STATUS_FAIL
						);
						return  $this->response($return,REST_CODE_PARAM_ERR);
					}
				}

			}

			$update_wk_gps = $this->wk_gps->update(TBL_WORKER_GPS,array('id_worker'=>$idworker),array('deleted_at'=>date('Y-m-d H:i:s')));
			if (!$update_wk_gps) {
				$return = array(
						'version'  => config_item('api_version'),
						'status'   => STATUS_FAIL
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}
		}

		if (!$this->wk->update(TBL_WORKER,array('id'=>$idworker),array('deleted_at'=>date('Y-m-d H:i:s')))) {
			// apdate active nfc
			$this->db->where('id',$idworker);
			$wk_detail = $this->db->get(TBL_WORKER)->row_array();
			$_nfc_code = isset($wk_detail['nfc_code']) ? $wk_detail['nfc_code'] : '';
			if (isset($_nfc_code) && !empty($_nfc_code)) {
				$this->db->where('nfc_code',$_nfc_code);
				$this->db->update(TBL_NFC,array('active'=>NFC_UNACTIVE,'deleted_at'=>date('Y-m-d H:i:s')));
			}
			// return
			$return = array(
				'version'  => config_item('api_version'),
				'status'   => STATUS_FAIL
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		$return = array(
				'version'  => config_item('api_version'),
				'status'   => STATUS_SUCCESS
		);
		return $this->response($return,REST_CODE_OK);
	}

	// activiti by employye
	function activiti_by_employee_get() {
		$employye_id = $this->get('employye_id');
		if (!isset($employye_id) OR empty($employye_id)) {
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'employye_id empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		// get activitis
		$activitis = array();
		$this->db->where('deleted_at' , NULL);
		$activitis = $this->db->get(TBL_ACTIVITIES)->result_array();
		if (!is_array($activitis) OR empty($activitis)) {
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'Data activitis empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		// get activiti by employee
		$employee_activitis = array();
		$this->db->where('deleted_at'   , NULL);
		$this->db->where('id_employee'  , $employye_id);
		$employee_activitis = $this->db->get(TBL_WORKER_ACTIVITY_SAVED)->result_array();
		if (!is_array($employee_activitis) OR empty($employee_activitis)) {
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'Data employee activitis empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		// response
		$data_activiti = array();
		foreach ($employee_activitis as $key => $value) {
			$data_activiti[] = isset($value['id_activity']) ? $value['id_activity'] : FALSE;
		}
		$data = array();
		foreach ($activitis as $key => $value) {
			if (isset($value['id']) AND in_array($value['id'], $data_activiti)) {
				$data[] = array(
					'id'    => $value['id'],
					'name'  => $value['name'],
					);
			}
		}
		$return = array(
			'version'   => config_item('api_version'),
			'status'    => STATUS_SUCCESS,
			'data'      => $data
		);
		return  $this->response($return,REST_CODE_OK);
	}

	function worker_by_activity_get() {
		$activity = $this->get('activity');
		if (isset($activity) AND !is_array($activity)) {
			$activity = explode(',', $activity);
		}

		if (!is_array($activity) OR empty($activity)) {
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'param fail !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		$this->db->where('deleted_at', NULL);
		$this->db->where_in('id_activity', $activity);
		$worker_activity = $this->db->get(TBL_WORKER_ACTIVITY_SAVED)->result_array();

		$id_worker = array();
		if (isset($worker_activity) AND !empty($worker_activity)) {
			foreach ($worker_activity as $key => $value) {
				if (isset($value['id_employee']) AND !in_array($value['id_employee'], $id_worker)) {
					$id_worker[] = $value['id_employee'];
				}
			}
		}

		if (!is_array($id_worker) OR empty($id_worker)) {
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'id_worker empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		$this->db->where('deleted_at', NULL);
		$this->db->where_in('id', $id_worker);
		$worker = $this->db->get(TBL_WORKER)->result_array();

		if (isset($worker) AND !empty($worker)) {
			$return = array(
				'version'   => config_item('api_version'),
				'status'    => STATUS_SUCCESS,
				'data'      => $worker
			);
			return  $this->response($return,REST_CODE_OK);
		} else {
			$return = array(
				'version' => config_item('api_version'),
				'status'  => STATUS_FAIL,
				'msg'     => 'Data worker empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}
}
