<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/controllers/REST_Controller.php';
class Addon extends REST_Controller{
	private $id_module 	= 1;
	private $_id_city	= 1;

	function __construct(){
		parent:: __construct();

		$this->load->model('push_server_update_model','model_server_update');

		if (isset($this->_tenantId) && !empty($this->_tenantId)){
			$this->_id_city = intval($this->_tenantId);
		} else {
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'tenantId not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	function addon_module_get(){
		$id_module = $this->get('id_module');
		$id_module_data = array();
		if(isset($id_module) AND !empty($id_module)){
			$id_module_data = explode(',', $id_module);
		}
		# Get Addon
		$addon = array();
		$this->db->where('deleted_at' , NULL);
		$addon = $this->db->get(TBL_ADDON)->result_array();
		if(!isset($addon) OR empty($addon)){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Addon empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		# Get Addon-module
		$addon_module = array();
		$this->db->where('deleted_at' , NULL);
		if (is_array($id_module_data) AND !empty($id_module_data)) {
			$this->db->where_in('id_module' , $id_module_data);
		}
		$addon_module = $this->db->get(TBL_ADDON_MODULE)->result_array();
		# id-addon
		$data = array();
		if(is_array($addon_module) AND !empty($addon_module)){
			foreach ($addon_module as $key => $value) {
				$id_addon = $value['id_addon'];
				foreach ($addon as $addon_key => $addon_value) {
					if($addon_value['id'] == $id_addon){
						$addon_value['id_module'] = $value['id_module'];
						$data[] = $addon_value;
					}
				}
			}
		}
		if(is_array($data) AND !empty($data)){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_SUCCESS,
					'data'    => $data
			);
			return  $this->response($return,REST_CODE_OK);
		} else {
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Data empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	# get
	function index_get(){
		$id_addon = $this->get('id');
		if(!isset($id_addon) OR empty($id_addon)){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'addon_id not found !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		# get machine-module
		$addon_module = array();
		$this->db->where('id_addon'		,$id_addon);
		#$this->db->where('id_module'	,$this->id_module);
		$this->db->where('deleted_at'	,NULL);
		$addon_module = $this->db->get(TBL_ADDON_MODULE)->result_array();
		if(!isset($addon_module) OR empty($addon_module)){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Data addon-module empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		# get id-machine-module
		$id_addon_module = FALSE;
		foreach ($addon_module as $key => $value) {
			$id_addon_module[] = $value['id'];
		}
		if($id_addon_module == FALSE){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Data id_addon_module empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		# get machine - module-activiti
		$addon_module_activiti = array();
		$this->db->where_in('id_addon_module'	,$id_addon_module);
		$this->db->where('deleted_at'		,NULL);
		$addon_module_activiti = $this->db->get(TBL_ADDON_MODULE_ACTIVITI)->result_array();
		if(is_array($addon_module_activiti) AND !empty($addon_module_activiti)){
			$return_module_activiti = array();
			foreach ($addon_module as $key => $value) {
				$return_id_module 		= $value['id_module'];
				$return_id_addon_module	= $value['id'];
				$activiti_item = array();
				foreach ($addon_module_activiti as $addon_key => $addon_value) {
					if($addon_value['id_addon_module'] == $return_id_addon_module){
						$activiti_item['activiti'][] 		= $addon_value['id_activity'];
						unset($addon_module_activiti[$addon_key]);
					}
				}
				$return_module_activiti[$return_id_module] = $activiti_item;
			}
			$return = array(
					'version' 	=> config_item('api_version'),
					'status'  	=> STATUS_SUCCESS,
					'data'		=> $return_module_activiti
			);
			return  $this->response($return,REST_CODE_OK);
		} else {
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Data addon_module_activity empty !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
	}

	# post
	function index_post(){
		$this->load->helper('pheanstalk');
		$job = array(
			"id_city"       => $this->_id_city,
			"controllers"   => "addon",
			"type"          => "post"
		);
		push_job(QUE_SERVER_UPDATE, json_encode($job));


		$addon_name       	= $this->post('name');
		$identifier 	= $this->post('identifier');
		$addon_module 	= $this->post('module');
		# add new addon
		$flag = $this->db->insert('addon',array(
				'identifier' 	=> isset($identifier) ? $identifier : '',
				'name' 			=> isset($addon_name) ? $addon_name : '',
				'created_at' 	=> date('Y-m-d H:i:s')
			));
		$id_addon		= FALSE;
		if($flag != FALSE){
			$id_addon = $this->db->insert_id();
		} else {
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Insert table : addon fail !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		if($id_addon == FALSE){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'addon_id fail !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		# insert module
		if($addon_module == FALSE){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'addon_module fail !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		$data_addon_molule_activiti = array();
		foreach ($addon_module as $key => $value) {
			$id_module 	= $key;
			$activiti 	= $value['activiti'];
			$data_addon_molule = array(
				'id_addon'		=> $id_addon,
				'id_module'		=> $id_module,
				'created_at'	=> date('Y-m-d H:s:i')
			);
			$flag = $this->db->insert(TBL_ADDON_MODULE,$data_addon_molule);
			$id_addon_module = FALSE;
			if($flag != FALSE){
				$id_addon_module = $this->db->insert_id();
			} else {
				$return = array(
						'version' => config_item('api_version'),
						'status'  => STATUS_FAIL,
						'msg'     => 'Insert table : addon_module fail !'
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}
			# create new record addon-module-activiti
			if(is_array($activiti) AND !empty($activiti)){
				foreach ($activiti as $activiti_key => $activiti_value) {
					$data_addon_molule_activiti[] = array(
						'id_addon_module'	=> $id_addon_module,
						'id_activity'		=> $activiti_value,
						'created_at'		=> date('Y-m-d H:s:i')
					);
				}

			}
		}
		if(is_array($data_addon_molule_activiti) AND !empty($data_addon_molule_activiti)){
			$flag = $this->db->insert_batch(TBL_ADDON_MODULE_ACTIVITI,$data_addon_molule_activiti);
			if($flag === FALSE){
				$return = array(
						'version' => config_item('api_version'),
						'status'  => STATUS_FAIL,
						'msg'     => 'Insert table : addon_molule - fail !'
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			} else {
				$push_server = $this->model_server_update->server_update();
				$return = array(
						'version' => config_item('api_version'),
						'status'  => STATUS_SUCCESS,
				);
				return  $this->response($return,REST_CODE_OK);
			}
		} else {
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Data activiti empty !'
			);
			return  $this->response($return,REST_CODE_OK);
		}
	}

	# put
	function index_put(){
		$this->load->helper('pheanstalk');
		$job = array(
			"id_city"       => $this->_id_city,
			"controllers"   => "addon",
			"type"          => "put"
		);
		push_job(QUE_SERVER_UPDATE, json_encode($job));


		$id_addon       = $this->put('id');
		$addon_name     = $this->put('name');
		$identifier 	= $this->put('identifier');
		$addon_module 	= $this->put('module');
		# update addon
		$this->db->where('id',$id_addon);
		$flag = $this->db->update('addon',array(
				'identifier' 	=> isset($identifier) ? $identifier : '',
				'name' 			=> isset($addon_name) ? $addon_name : '',
				'updated_at' 	=> date('Y-m-d H:i:s')
			));
		if($flag == FALSE){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Insert table : addon fail !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}
		#update delete addon-module and machine-module-activiti
		$addon_module_update = array();
		$this->db->where('id_addon'		,$id_addon);
		# $this->db->where('id_module'	,$this->id_module);
		$this->db->where('deleted_at'	,NULL);
		$addon_module_update = $this->db->get(TBL_ADDON_MODULE)->result_array();
		$id_addon_module_update = array();
		if(is_array($addon_module_update) AND !empty($addon_module_update)){
			foreach ($addon_module_update as $key => $value) {
				$id_addon_module_update[] = $value['id'];
			}
			if($id_addon_module_update != FALSE){
				$this->db->where_in('id',$id_addon_module_update);
				$this->db->update(TBL_ADDON_MODULE,array('deleted_at'=>date('Y-m-d H:i:s')));

				$this->db->where_in('id_addon_module',$id_addon_module_update);
				$this->db->update(TBL_ADDON_MODULE_ACTIVITI,array('deleted_at'=>date('Y-m-d H:i:s')));
			}
		}
		# insert module
		if($addon_module == FALSE){
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'addon_module fail !'
			);
			return  $this->response($return,REST_CODE_PARAM_ERR);
		}

		$data_addon_molule_activiti = array();
		foreach ($addon_module as $key => $value) {
			$id_module 	= $key;
			$activiti 	= $value['activiti'];
			$data_addon_molule = array(
				'id_addon'		=> $id_addon,
				'id_module'		=> $id_module,
				'created_at'	=> date('Y-m-d H:s:i')
			);
			$flag = $this->db->insert(TBL_ADDON_MODULE,$data_addon_molule);
			$id_addon_module = FALSE;
			if($flag != FALSE){
				$id_addon_module = $this->db->insert_id();
			} else {
				$return = array(
						'version' => config_item('api_version'),
						'status'  => STATUS_FAIL,
						'msg'     => 'Insert table : addon_module fail !'
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			}
			# create new record addon-module-activiti
			if(is_array($activiti) AND !empty($activiti)){
				foreach ($activiti as $activiti_key => $activiti_value) {
					$data_addon_molule_activiti[] = array(
						'id_addon_module'	=> $id_addon_module,
						'id_activity'		=> $activiti_value,
						'created_at'		=> date('Y-m-d H:s:i')
					);
				}
			}
		}
		if(is_array($data_addon_molule_activiti) AND !empty($data_addon_molule_activiti)){
			$flag = $this->db->insert_batch(TBL_ADDON_MODULE_ACTIVITI,$data_addon_molule_activiti);
			if($flag === FALSE){
				$return = array(
						'version' => config_item('api_version'),
						'status'  => STATUS_FAIL,
						'msg'     => 'Insert table : addon_molule - fail !'
				);
				return  $this->response($return,REST_CODE_PARAM_ERR);
			} else {
				$push_server = $this->model_server_update->server_update();
				$return = array(
						'version' => config_item('api_version'),
						'status'  => STATUS_SUCCESS,
				);
				return  $this->response($return,REST_CODE_OK);
			}
		} else {
			$return = array(
					'version' => config_item('api_version'),
					'status'  => STATUS_FAIL,
					'msg'     => 'Data activiti empty !'
			);
			return  $this->response($return,REST_CODE_OK);
		}
	}
}