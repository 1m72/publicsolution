<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* @author     : Le Thi Nhung (le.nhung@kloon.vn)
	* @create     : 2013-11-05
	* @modifed    : Chien Tran - 2013-12-11 (Update global db connection)
	*/
class User_model extends MY_Model{

	function create_user_post($data){
		$this->_db_global->insert(TBL_USERS,$data);
	}
	function user_get($iduser = array(),$email =array(),$idcity = array(), $idcompany = array(),$idusergroup = array(),$limit = 100,$offset = 0)
	{
		$field_select = 'id, id_city, id_company,id_user_group,username,email,extra_email,salt,deleted_at,updated_at,created_at';
		$this->_db_global->where('deleted_at',NULL);

		if(isset($iduser) && !empty($iduser))
			$this->_db_global->where_in('id',$iduser);

		if(isset($email) && !empty($email))
			$this->_db_global->where_in('email',$email);

		if(isset($idcity) && !empty($idcity))
			$this->_db_global->where_in('id_city',$idcity);

		if(isset($idcompany) && !empty($idcompany))
			$this->_db_global->where_in('id_company',$idcompany);

		if(isset($idusergroup) && !empty($idusergroup))
			$this->_db_global->where_in('id_user_group',$idusergroup);

		$this->_db_global->limit($limit,$offset);
		$this->_db_global->select($field_select);
		return $this->_db_global->get(TBL_USERS)->result_array();
	}

	function update($table,$where,$data)
	{
		if(isset($where) && !empty($where))
		{
			foreach($where as $key=>$value)
			{
				$this->_db_global->where($key,$value);
			}
		}
		if($this->_db_global->update($table,$data))
			return true;
		else
			return false;
	}
}