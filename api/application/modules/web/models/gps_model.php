<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-11-05
 * */
class Gps_model extends CI_Model
{
	function record_gps($where = array(), $type = FALSE){
		$return = array();
		if(is_array($where) AND !empty($where)){
			$id_module 	= isset($where['id_module']) 	? $where['id_module'] 	: '';
			$id_machine = isset($where['id_machine'])	? $where['id_machine'] 	: '';
			$id_worker 	= isset($where['id_worker']) 	? $where['id_worker'] 	: '';
			$street 	= isset($where['street'])    	? $where['street'] 		: '';
			$day 		= isset($where['day']) 	 	 	? $where['day'] 		: '';
			$limit 		= isset($where['limit'])     	? $where['limit'] 		: '';
			$offset 	= isset($where['offset'])    	? $where['offset'] 		: '';
			# street where
			$id_wkgps = array();
			if(isset($street) && !empty($street)){
				$this->db->select('id_wkgps');
				$this->db->where('deleted_at',NULL);
				$this->db->like('street', $street);
				$this->db->group_by('id_wkgps');
				$result_street = $this->db->get(TBL_WORKER_ACTIVITY)->result_array();
				if(isset($result_street) && !empty($result_street)){
					foreach ($result_street as $key => $value){
						$id_wkgps[] = $value['id_wkgps'];
					}
				}
			}
			# query
			$this->db->select('gps.*');
			$this->db->from(TBL_WORKER_GPS.' as gps');

			$this->db->where('gps.deleted_at',NULL);

			if(is_array($id_wkgps) && isset($id_wkgps) && !empty($id_wkgps)){
				$this->db->where_in('gps.id',$id_wkgps);
			}

			if($id_module != FALSE){
				$this->db->where('gps.app',$id_module);
			}

			if(isset($id_machine) && !empty($id_machine)){
				$this->db->where_in('gps.id_machine',$id_machine);
			}

			if(isset($id_worker) && !empty($id_worker)){
				$this->db->where_in('gps.id_worker',$id_worker);
			}

			if(isset($day) && !empty($day))
			{
				$this->db->where('gps.ontime >=',date('Y-m-d H:i:s',strtotime($day.' 00:00:00')));
				$this->db->where('gps.ontime <=',date('Y-m-d H:i:s',strtotime($day.' 23:59:59')));
			}
			if($type == FALSE){
				$this->db->order_by('gps.ontime','desc');
				$this->db->limit($limit,$offset);
				$return = array();
				$return = $this->db->get()->result_array();
				return $return;
			} else {
				$total = 0;
				$total = $this->db->count_all_results();
				return $total;
			}
		}
		return $return;
	}
	function group_activity($id_wkgps = array()){
		$return = array();
		if(is_array($id_wkgps) AND !empty($id_wkgps)){
			// $this->db->select('GROUP_CONCAT(id ORDER BY id ASC) as id, id_wkgps, id_activity, latitude, longtitude');
			// $this->db->where_in('id_wkgps', $id_wkgps);
			// $this->db->group_by(array('id_wkgps','id_task_activity'));
			// $return = $this->db->get(TBL_WORKER_ACTIVITY)->result_array();

			$sql = "SELECT GROUP_CONCAT(id ORDER BY id ASC) as id, id_wkgps, id_activity, SUBSTRING_INDEX(GROUP_CONCAT(latitude ORDER BY id ASC), ',', 1) AS latitude, SUBSTRING_INDEX(GROUP_CONCAT(longtitude ORDER BY id ASC), ',', 1) AS longtitude FROM " . TBL_WORKER_ACTIVITY . " WHERE `id_wkgps` IN (" .implode($id_wkgps, ','). ") GROUP BY `id_wkgps`, `id_task_activity`, `group_street`";
			$return = $this->db->query($sql)->result_array();
		}
		return $return;
	}

	function record_activity($id_wkgps = FALSE){
		$return = array();
		if($id_wkgps != FALSE){
			$this->db->query('SET SESSION group_concat_max_len = 1000000000');
			$this->db->select("wac.id_wkgps,wac.id_activity, GROUP_CONCAT(wac.latitude  ORDER BY wac.id) as latitude, GROUP_CONCAT(wac.longtitude  ORDER BY wac.id) as longtitude ,min(wac.starttime) as starttime,max(wac.endtime) as endtime, (max(wac.endtime) - min(wac.starttime)) as duration,wac.street,wac.group_street");
			$this->db->from(TBL_WORKER_ACTIVITY.' as wac');
			$this->db->where('wac.deleted_at',NULL);
			$this->db->where('wac.id_wkgps',$id_wkgps);
			$this->db->order_by('wac.id','asc');
			$this->db->group_by('wac.id_wkgps,wac.id_activity,wac.id_task_activity,wac.street,wac.group_street');
			$return = $this->db->get()->result_array();
		}
		return $return;
	}

	function information_gps($id_wkgps = FALSE){
		$return = array();
		if($id_wkgps != FALSE){
			$this->db->select('infor.id,infor.id_worker_gps as id_gps,infor.id_wk_activity as id_wk_ac,infor.data,infor.lat,infor.lon,infor.time,infor.type');
			$this->db->from(TBL_WORKER_INFORMATION.' as infor');
			$this->db->where('infor.id_worker_gps',$id_wkgps);
			$return = $this->db->get()->result_array();
		}
		return $return;
	}

	function record_gps_get_where($id_city = array(), $id_module = FALSE, $id_machine = array(), $id_worker = array(), $street = NULL, $day = NULL, $limit = 50, $offset = 0, $type = FALSE){
		# street where
		$street_where = array();
		if(isset($street) && !empty($street)){
			$this->db->select('id_wkgps');
			$this->db->where('deleted_at',NULL);
			$this->db->like('street', $street);
			$this->db->group_by('id_wkgps');
			$result_street = $this->db->get(TBL_WORKER_ACTIVITY)->result_array();
			if(isset($result_street) && !empty($result_street)){
				foreach ($result_street as $key => $value){
					$street_where[] = $value['id_wkgps'];
				}
			}
		}
		$this->db->select('gps.*');
		$this->db->from(TBL_WORKER_GPS.' as gps');

		$this->db->where('gps.deleted_at',NULL);

		if(is_array($street_where) && isset($street_where) && !empty($street_where)){
			$this->db->where_in('gps.id',$street_where);
		}

		if(isset($id_city) && !empty($id_city)){
			$this->db->where_in('gps.id_city',$id_city);
		}

		if($id_module != FALSE){
			$this->db->where('gps.app',$id_module);
		}

		if(isset($id_machine) && !empty($id_machine)){
			$this->db->where_in('gps.id_machine',$id_machine);
		}

		if(isset($id_worker) && !empty($id_worker)){
			$this->db->where_in('gps.id_worker',$id_worker);
		}

		if(isset($day) && !empty($day))
		{
			$this->db->where('gps.ontime >=',date('Y-m-d H:i:s',strtotime($day.' 00:00:00')));
			$this->db->where('gps.ontime <=',date('Y-m-d H:i:s',strtotime($day.' 23:59:59')));
		}
		if($type == FALSE){
			$this->db->order_by('gps.ontime','desc');
			$this->db->limit($limit,$offset);
			$return = array();
			$return = $this->db->get()->result_array();
			return $return;
		} else {
			$total = 0;
			$total = $this->db->count_all_results();
			return $total;
		}
	}

	function activiti_get_where($id_wk_gps = array()){
		$return = array();
		if(is_array($id_wk_gps) AND !empty($id_wk_gps)){
			# Set max lenght group_concat function
			$this->db->query('SET SESSION group_concat_max_len = 1000000000');

			$this->db->select("wac.id_wkgps,wac.id_activity, GROUP_CONCAT(wac.latitude  ORDER BY wac.id) as latitude, GROUP_CONCAT(wac.longtitude  ORDER BY wac.id) as longtitude ,min(wac.starttime) as starttime,max(wac.endtime) as endtime, (max(wac.endtime) - min(wac.starttime)) as duration,wac.street,wac.group_street");
			$this->db->from(TBL_WORKER_ACTIVITY.' as wac');
			$this->db->where('wac.deleted_at',NULL);
			$this->db->where_in('wac.id_wkgps',$id_wk_gps);
			$this->db->order_by('wac.id','asc');
			$this->db->group_by('wac.id_wkgps,wac.id_activity,wac.id_task_activity,wac.street,wac.group_street');
			$return = $this->db->get()->result_array();
		}
		return $return;
	}

	function infor_get_where($id_wk_gps = array()){
		$return = array();
		if( is_array($id_wk_gps) AND !empty($id_wk_gps) ){
			$this->db->select('infor.id,infor.id_worker_gps as id_gps,infor.id_wk_activity as id_wk_ac,infor.data,infor.lat,infor.lon,infor.time,infor.type');
			$this->db->from(TBL_WORKER_INFORMATION.' as infor');
			$this->db->where_in('infor.id_worker_gps',$id_wk_gps);
			$return = $this->db->get()->result_array();
		}
		return $return;
	}

	function get_gps_by_id($id_gps = 0){
		$this->db->select('gps.*,mc.name as mcname,wk.first_name, wk.last_name');
		$this->db->from(TBL_WORKER_GPS.' as gps');
		$this->db->join(TBL_MACHINE.' as mc','mc.id = gps.id_machine');
		$this->db->join(TBL_WORKER.' as wk','wk.id = gps.id_worker');

		$this->db->where('gps.deleted_at',NULL);
		$this->db->where('mc.deleted_at',NULL);
		$this->db->where('wk.deleted_at',NULL);
		$this->db->where('gps.id',$id_gps);

		$this->db->order_by('gps.ontime','desc');
		return $this->db->get()->result_array();
	}
	function get_gps($machine = array(),$worker = array(),$day = NULL,$city = array(),$street = '',$limit = 100,$offset = 0)
	{
		$street_where = array();
		if(isset($street) && !empty($street)){
			$this->db->select('id_wkgps');
			$this->db->where('deleted_at',NULL);
			$this->db->like('street', $street);
			$this->db->group_by('id_wkgps');
			$result_street = $this->db->get(TBL_WORKER_ACTIVITY)->result_array();
			if(isset($result_street) && !empty($result_street)){
				foreach ($result_street as $item_street){
					array_push($street_where, $item_street['id_wkgps']);
				}
			} else {
				array_push($street_where, 0);
			}
		}
		#
		$this->db->select('gps.*,mc.name as mcname,wk.first_name, wk.last_name');
		$this->db->from(TBL_WORKER_GPS.' as gps');
		$this->db->join(TBL_MACHINE.' as mc','mc.id = gps.id_machine');
		$this->db->join(TBL_WORKER.' as wk','wk.id = gps.id_worker');

		$this->db->where('gps.deleted_at',NULL);
		$this->db->where('mc.deleted_at',NULL);
		$this->db->where('wk.deleted_at',NULL);

		if(is_array($street_where) && isset($street_where) && !empty($street_where)){
			$this->db->where_in('gps.id',$street_where);
		}

		if(isset($city) && !empty($city))
			$this->db->where_in('gps.id_city',$city);

		if(isset($machine) && !empty($machine))
			$this->db->where_in('gps.id_machine',$machine);

		if(isset($worker) && !empty($worker))
			$this->db->where_in('gps.id_worker',$worker);

		if(isset($day) && !empty($day))
		{
			$this->db->where('gps.ontime >=',date('Y-m-d H:i:s',strtotime($day.' 00:00:00')));
			$this->db->where('gps.ontime <=',date('Y-m-d H:i:s',strtotime($day.' 23:59:59')));
		}

		$this->db->order_by('gps.ontime','desc');
		$this->db->limit($limit,$offset);

		return $this->db->get()->result_array();//TBL_WORKER_GPS
	}

	function activity_gps($id_wkgps = 0)
	{
		$this->db->select('wac.*,ac.color,ac.name as acname');
		$this->db->from(TBL_WORKER_ACTIVITY.' as wac');
		$this->db->join(TBL_ACTIVITIES.' as ac','ac.id = wac.id_activity');
		$this->db->where('ac.deleted_at',NULL);
		if(isset($id_wkgps) && !empty($id_wkgps) && $id_wkgps > 0)
			$this->db->where('wac.id_wkgps',$id_wkgps);
		$this->db->order_by('wac.starttime','asc');
		return $this->db->get()->result_array();
	}

	function lastIndexOf($string,$item){
		$index=strpos(strrev($string),strrev($item));
		if ($index){
			$index=strlen($string)-strlen($item)-$index;
			return $index;
		}
		else
			return -1;
	}

	function activity_get($id_wkgps = 0, $type= NULL)
	{
		// var_dump($this->db);die;
		$this->db->select('wac.id,wac.id_city,wac.id_wkgps,wac.id_activity,wac.id_task,wac.latitude,wac.longtitude,wac.starttime,wac.endtime,
				ac.color,ac.name as acname,wc.first_name,wc.last_name,mc.name as mcname,wac.street');
		$this->db->from(TBL_WORKER_ACTIVITY.' as wac');
		$this->db->join(TBL_ACTIVITIES.' as ac','ac.id = wac.id_activity');
		$this->db->join(TBL_WORKER_GPS.' as wgps','wgps.id = wac.id_wkgps');
		$this->db->join(TBL_WORKER.' as wc','wc.id = wgps.id_worker');
		$this->db->join(TBL_MACHINE.' as mc','mc.id = wgps.id_machine');

		$this->db->where('wac.deleted_at',NULL);
		$this->db->where('ac.deleted_at',NULL);
		$this->db->where('wc.deleted_at',NULL);
		$this->db->where('mc.deleted_at',NULL);

		if(isset($id_wkgps) && !empty($id_wkgps) && $id_wkgps > 0)
			$this->db->where('wac.id_wkgps',$id_wkgps);
		if(isset($type) && !empty($type))
		{
			$this->db->order_by('wac.starttime','desc');
			$this->db->group_by('wac.latitude,wac.longtitude');
		} else {
			$this->db->order_by('wac.starttime','asc');
		}

		return $this->db->get()->result_array();
	}

	function get_activity_by_worker($id_wkgps = 0, $type= NULL){
		$this->db->select('wac.id_wkgps,wac.id_activity,wac.id_task,wac.id_task_activity,min(wac.starttime) as starttime,max(wac.endtime) as endtime,wac.latitude,wac.longtitude,wac.street,wac.group_street,
				ac.color,ac.name as acname,wc.first_name,wc.last_name,mc.name as mcname');
		$this->db->from(TBL_WORKER_ACTIVITY.' as wac');
		$this->db->join(TBL_ACTIVITIES.' as ac','ac.id = wac.id_activity');
		$this->db->join(TBL_WORKER_GPS.' as wgps','wgps.id = wac.id_wkgps');
		$this->db->join(TBL_WORKER.' as wc','wc.id = wgps.id_worker');
		$this->db->join(TBL_MACHINE.' as mc','mc.id = wgps.id_machine');

		$this->db->where('wac.deleted_at',NULL);
		$this->db->where('ac.deleted_at',NULL);
		$this->db->where('wc.deleted_at',NULL);
		$this->db->where('mc.deleted_at',NULL);

		if(isset($id_wkgps) && !empty($id_wkgps) && $id_wkgps > 0)
			$this->db->where('wac.id_wkgps',$id_wkgps);

		$this->db->order_by('wac.id','asc');
		$this->db->group_by('wac.id_wkgps,wac.id_activity,wac.id_task_activity,wac.street,wac.group_street');

		return $this->db->get()->result_array();
	}

	function information_get($id_wkgps = 0){
		$this->db->select('infor.id,infor.id_worker_gps as id_gps,infor.id_wk_activity as id_wk_ac,infor.data,infor.lat,infor.lon,infor.time,infor.type,wk.first_name,wk.last_name,mc.name as mc_name');
		$this->db->from(TBL_WORKER_INFORMATION.' as infor');

		$this->db->join(TBL_WORKER_GPS.' as gps', 'gps.id = infor.id_worker_gps');
		$this->db->join(TBL_WORKER.' as wk','wk.id = gps.id_worker');
		$this->db->join(TBL_MACHINE.' as mc','mc.id = gps.id_machine');

		$this->db->where('gps.id',$id_wkgps);
		$this->db->where('wk.deleted_at',NULL);
		$this->db->where('mc.deleted_at',NULL);

		return $this->db->get()->result_array();
	}
	/*
	* @description: update worker-gps
	* @function   : update
	* @author     : Le Thi Nhung (le.nhung@kloon.vn)
	* @create     : 2013-12-10
	*/
	function update($table,$where,$data)
	{
		if(isset($where) && !empty($where))
		{
			foreach($where as $key=>$value)
			{
				$this->db->where($key,$value);
			}
		}
		if($this->db->update($table,$data))
			return true;
		else
			return false;
	}
}