<?php
class Infor_model extends CI_Model{

	function getinfo($id_worker_gps = NULL, $id_wk_activiti = NULL, $lat = NULL, $lon = NULL)
	{
		$this->db->select('info.*,wkac.starttime,wkac.endtime,wk.ontime,wk.id_worker');
		$this->db->from(TBL_WORKER_GPS.' as wk');
		$this->db->join(TBL_WORKER_INFORMATION.' as info','wk.id = info.id_worker_gps','left');
		$this->db->join(TBL_WORKER_ACTIVITY.' as wkac','wk.id = wkac.id_wkgps','left');

		if(isset($id_worker_gps) && !empty($id_worker_gps))
			$this->db->where('wk.id',$id_worker_gps);
		if(isset($lat) && !empty($lat) && isset($lon) && !empty($lon)){
			$this->db->where('info.lat',$lat);
			$this->db->where('info.lon',$lon);
		} else{
			if(isset($id_wk_activiti) && !empty($id_wk_activiti))
				$this->db->where('info.id_wk_activity',$id_wk_activiti);
		}

		$this->db->order_by("wk.ontime", "asc");
		$this->db->group_by('info.data,info.type');

		return $this->db->get()->result_array();
	}

	function get_obj_worker($id_worker_gps = 0)
	{
		$this->db->select('wk.id as id_wk,wk.first_name,wk.last_name');
		$this->db->from(TBL_WORKER_GPS.' as wk_gps');
		$this->db->join(TBL_WORKER.' as wk','wk.id = wk_gps.id_worker');
		$this->db->where('wk_gps.id',$id_worker_gps);
		return $this->db->get()->result_array();
	}

	function get_company_by_city($cityid = NULL)
	{
		if(isset($cityid) && !empty($cityid))
		{
			$result = $this->db->get_where(TBL_COMPANY,array('id_city '=>$cityid))->result_array();
			if(isset($result) && !empty($result))
				return $result[0]['id'];
			else
				return 0;
		}
		else
			return 0;
	}
}
?>