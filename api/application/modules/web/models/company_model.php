<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-11-04
 * */
class Company_model extends CI_Model{
	/*
	 * @create 2013-11-11
	 * */
	function getcompany($id_array_company = array(),$id_array_city = array(),$limit = 100,$offset = 0,$order = NULL)
	{
		$this->db->where('deleted_at',NULL);
		if(isset($id_array_company) && !empty($id_array_company))
			$this->db->where_in('id',$id_array_company);
		
		if(isset($id_array_city) && !empty($id_array_city))
			$this->db->where_in('id_city',$id_array_city);
		
		if(isset($order) && !empty($order)){
			foreach ($order as $key=>$value){
				$this->db->order_by($key,$value);
			}
		}
		
		$this->db->limit($limit,$offset);
		
		return $this->db->get(TBL_COMPANY)->result_array();
	}
	
	function update($table,$where,$data)
	{
		if(isset($where) && !empty($where))
		{
			foreach($where as $key=>$value)
			{
				$this->db->where($key,$value);
			}
		}
		if($this->db->update($table,$data))
			return true;
		else
			return false;
	}
}
?>