<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * @author huongpm<phung.manh.huong>
 * @create 2013-11-06
 * */
class Machine_model extends CI_Model{
	function get_machine($cityid = array(),$machineid = array(),$limit = 100,$offset = 0)
	{
		$this->db->where('deleted_at',NULL);
		if(isset($machineid) && !empty($machineid))
			$this->db->where_in('id',$machineid);
		else
		{
			if(isset($cityid) && !empty($cityid))
				$this->db->where_in('id_city',$cityid);
		}
		
		$this->db->limit($limit,$offset);
		
		return $this->db->get(TBL_MACHINE)->result_array();
	}
	
	function link_nfc_machine($machineid = NULL,$nfc_code = NULL)
	{
		$this->db->where('id',$machineid);
		if($this->db->update(TBL_MACHINE,array('nfc_code'=>$nfc_code,'updated_at'=>date('Y-m-d H:i:s'))))
			return true;
		else
			return false;
	}
}
?>