<?php
class Devices_model extends CI_Model{
	function get($array_city_id = array(),$limit = 100,$offset = 0)
	{
		$this->db->where('deleted_at',NULL);
		if(isset($array_city_id) && !empty($array_city_id))
			$this->db->where_in('id_city',$array_city_id);
		
		$this->db->limit($limit,$offset);
		
		return $this->db->get(TBL_DEVICES)->result_array();
	}
}
?>