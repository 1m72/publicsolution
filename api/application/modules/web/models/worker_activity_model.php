<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * @author Le Thi Nhung<le.nhung@kloon.vn>
 * @create 2013-12-10
 * */
class Worker_activity_model extends CI_Model{
	/*
	* @description: update worker_activity
	* @function   : update
	* @author     : Le Thi Nhung (le.nhung@kloon.vn)
	* @create     : 2013-12-10
	*/	
	function update($table,$where,$data)
	{
		if(isset($where) && !empty($where))
		{
			foreach($where as $key=>$value)
			{
				$this->db->where($key,$value);
			}
		}
		if($this->db->update($table,$data))
			return true;
		else
			return false;
	}
}
?>