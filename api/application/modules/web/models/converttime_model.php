<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
    /*
    * @author     : Le Thi Nhung (le.nhung@kloon.vn)
    * @create     : 2014-03-06
    */
class Converttime_model extends MY_Model{
    /*
    * @description: convert all field datetime to gmt
    * @function   : converttime_update
    * @author     : Le Thi Nhung (le.nhung@kloon.vn)
    * @create     : 2014-03-06
    */
    function converttime_update($hour)
    {
        $table_all     = $this->_db_global->list_tables();
        foreach ($table_all as $key => $table)
        {
            $field_of_table  = $this->_db_global->field_data($table);
            $field_date      = array();
            foreach ($field_of_table as $field)
            {
               if($field->type=='datetime')
               {
                    array_push($field_date, $field->name);
               }
            }

            if(!empty($field_date)){
                $field_date_select = implode(',', $field_date);
                $this->_db_global->select($field_date_select.",id");
                $result =  $this->_db_global->get($table)->result_array();

                $data = array();
                if (isset($result) AND !empty($result)) {
                    foreach ($result as $key => $value) {
                        foreach ($field_date as $key1 => $value1) {
                            $datetime    = $result[$key][$value1];
                            $converttime = new DateTime($datetime);
                            $GMT  = date('Y-m-d H:i:s', strtotime($hour.' hour', $converttime->getTimestamp()));
                            $data += array(
                                $value1=>$GMT,
                            );
                        }
                        $this->_db_global->where('id',$result[$key]['id']);
                        $this->_db_global->update($table,$data);
                        $data = array();
                   }
                }
            }
        }
    }
    function converttime1_update1($table,$field_select)
    {
        $this->_db_global->select($field_select.",id");
        $result =  $this->_db_global->get($table)->result_array();
        $field_select = explode(',',$field_select);
        $data = array();
        foreach ($result as $key => $value) {
            foreach ($field_select as $key1 => $value1) {
                $datetime    = $result[$key][$value1];
                $converttime = new DateTime($datetime);
                $GMT  = date('Y-m-d H:i:s', strtotime('- 7 hour', $converttime->getTimestamp()));
                $data += array(
                    $value1=>$GMT,
                );
            }
            $this->_db_global->where('id',$result[$key]['id']);
            $this->_db_global->update($table,$data);
            $data = array();
       }
    }
}