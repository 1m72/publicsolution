<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 *@author huongpm<phung.manh.huong@kloon.vn>
 *@create 2013-11-04
 * */
class City_model extends CI_Model{

	function getcity($idcity = array(),$limit = 100,$offset = 0)
	{
		$this->_db_global->where('deleted_at',NULL);
		if(isset($idcity) && !empty($idcity))
			$this->_db_global->where_in('id',$idcity);

		$this->_db_global->limit($limit,$offset);

		return $this->_db_global->get(TBL_CITY)->result_array();
	}

	function update($table,$where,$data)
	{
		if(isset($where) && !empty($where))
		{
			foreach($where as $key=>$value)
			{
				$this->_db_global->where($key,$value);
			}
		}
		if($this->_db_global->update($table,$data))
			return true;
		else
			return false;
	}
}
?>