<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
    /*
    * @author     : Le Thi Nhung (le.nhung@kloon.vn)
    * @create     : 2013-12-31
    */
class User_group_model extends MY_Model{

    function create_user_group_post($data){
        $this->_db_global->insert(TBL_USER_GROUP,$data);
    }
    function user_group_get($id_usergroup = array(),$limit = 100,$offset = 0)
    {
        $field_select = 'id, name,deleted_at,updated_at,created_at';
        $this->_db_global->where('deleted_at',NULL);

        if(isset($id_usergroup) && !empty($id_usergroup))
            $this->_db_global->where_in('id',$id_usergroup);

        $this->_db_global->limit($limit,$offset);
        $this->_db_global->select($field_select);
        return $this->_db_global->get(TBL_USER_GROUP)->result_array();
    }

    function update($table,$where,$data)
    {
        if(isset($where) && !empty($where))
        {
            foreach($where as $key=>$value)
            {
                $this->_db_global->where($key,$value);
            }
        }
        if($this->_db_global->update($table,$data))
            return true;
        else
            return false;
    }
}