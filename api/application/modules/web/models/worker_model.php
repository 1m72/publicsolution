<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * @author huongpm<phung.manh.huong@kloon.vn>
 * @create 2013-11-05
 * */
class Worker_model extends CI_Model{
	/*
	 * @update 2013-11-11
	 * */
	function worker_get($idcity = array(), $idcompany = array(),$idworker = array(),$limit = 100,$offset = 0)
	{
		$this->db->where('deleted_at',NULL);
		
		if(isset($idcity) && !empty($idcity))
			$this->db->where_in('id_city',$idcity);
		
		if(isset($idcompany) && !empty($idcompany))
			$this->db->where_in('id_company',$idcompany);
		
		if(isset($idworker) && !empty($idworker))
			$this->db->where_in('id',$idworker);
		
		$this->db->limit($limit,$offset);
		
		return $this->db->get(TBL_WORKER)->result_array();
	}
	
	function link_nfc_worker($worker_id = NULL, $nfc_code = NULL)
	{
		$this->db->where('id',$worker_id);
		if($this->db->update(TBL_WORKER,array('nfc_code'=>$nfc_code,'updated_at'=>date('Y-m-d H:i:s'))))
			return true;
		else
			return false;
	}
	
	function link_worker_teamlead($worker_id = NULL,$teamlead_id = NULL)
	{
		$this->db->where('id',$worker_id);
		if($this->db->update(TBL_WORKER,array('id_teamleader'=>$teamlead_id,'updated_at'=>date('Y-m-d H:i:s'))))
			return true;
		else
			return false;
	}
	/*
	* @description: update worker
	* @function   : update
	* @author     : Le Thi Nhung (le.nhung@kloon.vn)
	* @create     : 2013-12-10
	*/	
	function update($table,$where,$data)
	{
		if(isset($where) && !empty($where))
		{
			foreach($where as $key=>$value)
			{
				$this->db->where($key,$value);
			}
		}
		if($this->db->update($table,$data))
			return true;
		else
			return false;
	}
}
?>