<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Auth model
 *
 * @author Chien Tran <tran.duc.chien@kloon.vn>
 */
class Auth_model extends MY_Model{
    function login($username, $password) {
        $checker = $this->_db_global->get_where(TBL_USERS, array('username' => $username))->row_array();
        if ( is_array($checker) AND !empty($checker) ) {
            $salt          = $checker['salt'];
            $hash_password = $this->hash_password($password, $salt);
            if ($checker['password'] === $hash_password) {
                $id_city = isset($checker['id_city']) ? $checker['id_city'] : '';
                $result = $this->_db_global->get_where(TBL_CITY,array('id'=>$id_city))->row_array();
                $lat = isset($result['lat']) ? $result['lat'] : '';
                $lon = isset($result['lon']) ? $result['lon'] : '';
                /*unset($checker['salt']);
                unset($checker['password']);*/
                unset($checker['forgot_code']);
                unset($checker['deleted_at']);

                $checker['lat'] = $lat;
                $checker['lon'] = $lon;

                return $checker;
            }
        }
        return false;
    }

    /**
     * Generate password hash from raw + salt
     * @param  string $raw  Password in raw
     * @param  string $salt Salt for security
     * @return string       Password hashed
     */
    function hash_password($raw = '', $salt = '') {
        return md5(md5($raw) . $salt);
    }
}
?>