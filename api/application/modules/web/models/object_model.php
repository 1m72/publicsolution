<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 *@author huongpm<phung.manh.huong@kloon.vn>
 *@create 2013-11-22
 * */
class Object_model extends CI_Model{
	
	function get($id_city_array = array(),$id_parent_array = array(),$id_object_array = array(),$id_un_object_array = array())
	{
		$this->db->where('deleted_at',NULL);
		
		if(isset($id_city_array) && !empty($id_city_array))
			$this->db->where_in('id_city',$id_city_array);
		
		if(isset($id_parent_array) && !empty($id_parent_array))
			$this->db->where_in('id_parent',$id_parent_array);
		
		if(isset($id_object_array) && !empty($id_object_array))
			$this->db->where_in('id',$id_object_array);
		
		if(isset($id_un_object_array) && !empty($id_un_object_array))
			$this->db->where_not_in('id',$id_un_object_array);
		//$this->db->limit($limit,$offset);
		
		return $this->db->get(TBL_OBJECT)->result_array();
	}
}
?>