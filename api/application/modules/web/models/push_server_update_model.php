<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Push_server_update_Model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    /*
     * @description
     * @function : push_service_update
     * @author : huongpm<phung.manh.huong@kloon.vn>
     * @create : 2014-08-26
     * @route
     **/
    function server_update(){
        $return     = array();

        # constants
        $message    = GOOGLE_MESSAGE;
        $key        = GOOGLE_API_KEY;
        $url        = GOOGLE_URL;
        # get list device
        $this->db->where("deleted_at", null);
        $this->db->where("actived", 1);
        $device_list = $this->db->get(TBL_DEVICES)->result_array();

        if(isset($device_list) AND !empty($device_list) AND is_array($device_list)){
            $registrationid = array();
            foreach ($device_list as $key => $value) {
                $device = $value['id_device'];
                if(isset($value['id_unique']) AND !empty($value['id_unique'])){
                    $registrationid[] = $value['id_unique'];
                }
            }

            if(isset($registrationid) AND !empty($registrationid) AND is_array($registrationid)){
                write_log("Registrationid : " . json_encode($registrationid), 'google_service');

                $fields = array(
                    'registration_ids'  => $registrationid,
                    'data'              => array("server_update" => $message)
                );

                $headers = array(
                        'Content-Type:application/json',
                        'Authorization:key='.$key
                );

                // Open connection
                $ch = curl_init();
                // Set the url, number of POST vars, POST data
                curl_setopt( $ch, CURLOPT_URL, $url );
                curl_setopt( $ch, CURLOPT_POST, true );
                curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields));
                // Execute post
                $result = curl_exec($ch);

                // Log
                write_log("Message : " . $result, 'google_service');

                // Close connection
                curl_close($ch);

                if(isset($result->success) AND $result->success > 0){
                    $return = $device;
                }
            }
        }
        # return
        return $return;
    }
}
