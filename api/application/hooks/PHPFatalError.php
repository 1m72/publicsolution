<?php
class PHPFatalError {
    public function setHandler() {
        register_shutdown_function('handleShutdown');
    }
}

function handleShutdown() {
    if (($error = error_get_last())) {
        // ob_start();
        // echo "<pre>";
        // var_dump($error);
        // echo "</pre>";
        $message = ob_get_clean();
        // var_dump($message);
        // sendEmail($message);

        ob_start();
        $msg = array(
            'status'  => 'error',
            'message' => 'Internal application error!',
        );
        if (isset($_GET['debug']) == '1') {
            $msg['debug'] = array(
                'msg'   => strip_tags($message),
                'extra' => $error
            );
        }
        echo json_encode($msg);
        ob_flush();
        exit();
    }
}