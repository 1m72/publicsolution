<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    /**
     * Resolve street name from lat, lon
     * @param  string $latitude
     * @param  string $longtitude
     * @param  string $service
     * @return string
     */
    if (!function_exists('resolve_street_name')) {
        function resolve_street_name($latitude, $longtitude, $service = 'kloon') {
            $services = config_item('resolve_services');

            if (!isset($services[$service])) {
                $service = $services['mapquest'];
            } else {
                $service = $services[$service];
            }

            $street = '';
            if ((isset($latitude) && !empty($latitude)) && (isset($longtitude) && !empty($longtitude))) {
                $service_url    = '';
                $service_type   = $service;
                $count  = 0;
                $status = FALSE;
                while ( ($count < 3) AND ($status != TRUE) ) {
                    switch ($count) {
                        case 1:
                            $service_type = 'mapquest';
                            $service_url  = $services['mapquest'] . '&lat=' . $latitude . '&lon=' . $longtitude;
                            break;

                        case 2:
                            $service_type = 'openstreetmap';
                            $service_url  = $services['openstreetmap'] . '&lat=' . $latitude . '&lon=' . $longtitude;
                            break;

                        default:
                            $service_type = 'kloon';
                            $service_url  = $services['kloon'] . '&lat=' . $latitude . '&lon=' . $longtitude;
                            break;
                    }

                    $street = '';
                    if ($service_url != FALSE) {
                        $street_data = '';
                        $ch = curl_init();
                        $ch_timeout            = 15000;
                        $ch_connection_timeout = 2000;
                        curl_setopt($ch, CURLOPT_URL, $service_url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        #curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
                        curl_setopt($ch, CURLOPT_TIMEOUT, $ch_timeout);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, $ch_connection_timeout);
                        $street_data = curl_exec($ch);
                        curl_close($ch);

                        $street_array = json_decode($street_data,true);
                        if (isset($street_array['address']) && !empty($street_array['address'])) {
                            $street_detail = $street_array['address'];
                            if(isset($street_detail['road']) && !empty($street_detail['road'])){
                                $street_name = isset($street_detail['road']) ? $street_detail['road'] : '';
                                $street      = $street_name;
                            } else {}
                        }
                    }

                    # Logs
                    write_log("Synchronize controller: {$service_type}: {$latitude} - {$longtitude} {$street}", 'resolve_street_name');

                    if( (isset($street) AND !empty($street)) OR ($count >= 3) ){
                        $status = TRUE;
                    }
                    $count++;

                    usleep(100);
                }
            }
            return $street;
        }
    }
