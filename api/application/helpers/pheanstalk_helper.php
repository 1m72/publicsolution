<?php
require_once(APPPATH.'libraries/pheanstalk/pheanstalk_init.php');

if (!function_exists('push_job')){
	/**
	 * Push job to queue tube
	 * @param  string $tube queue name
	 * @param  string $data params json_data
	 * @return [type]       [description]
	 */
	function push_job($tube = '', $data = ''){
		if($tube == '') return FALSE;

		try {
			if ($tube == QUE_SERVER_UPDATE) {
				$data    = json_decode($data);
				$CI      = get_instance();
				$id_city = isset($data->id_city) ? intval($data->id_city) : false;
				$id_city = $id_city ? $id_city : get_tenant_id();
				$queue_record = array(
					'id_city'          => $id_city,
					'type'             => 'gcm',
					'uri'              => getenv('GCM_URL'),
					'extra'            => json_encode($data),
					'retry'            => 0,
					'created_at'       => date('Y-m-d H:i:s'),
					'status'           => CRON_STATUS_PENDING,
					// 'request_log'   => '',
					// 'attachments'   => '',
					// 'response_code' => '',
					// 'response_data' => '',
					// 'requested_at'  => '',
					// 'extra'         => '',
				);
				$status = $CI->_db_global->insert(TBL_3RD_API_LOGS, $queue_record);

				if ($status !== false) {
					$data->log_id = $CI->_db_global->insert_id();
				}
				$data = json_encode($data);
			}

			$pheanstalk = pt_connect();
			$return = $pheanstalk->useTube($tube)->put($data);

			write_log('push_job : ' .ENVIRONMENT . ' _ ' . $data, 'beanstalk');
			return $return;
		} catch (\Exception $e) {
			return true;
		}
	}
}

if (!function_exists('pt_connect')) {
	function pt_connect() {
		$CI = get_instance();
		if (isset($CI->queue_pheanstalkd)) {
			write_log('Get connect to : ' . getenv('QUEUE_PHEANSTALK_HOST'), 'beanstalk');
			return $CI->queue_pheanstalkd;
		} else {
			$queue = new Pheanstalk(getenv('QUEUE_PHEANSTALK_HOST'));
			$CI->queue_pheanstalkd = $queue;
			write_log('Connected : ' . getenv('QUEUE_PHEANSTALK_HOST'), 'beanstalk');
			return $queue;
		}
	}
}
