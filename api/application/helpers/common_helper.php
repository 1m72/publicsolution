<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    if (!function_exists('get_lastupdate')){
        /**
         * [get_lastupdate description]
         * @param  [type] $table        string
         * @param  [type] $update_field string
         * @return [type]               array
         * @author Chien Tran <tran.duc.chien@kloon.vn>
         */
        function get_lastupdate($table = NULL, $update_field = NULL) {
            if ( is_null($table) OR is_null($update_field) ) {
                return false;
            }

            $CI = get_instance();
            $table_exist_checker = $CI->db->table_exists($table);
            if ($table_exist_checker) {
                $field_exist_checker = $CI->db->field_exists($update_field, $table);
                if ($field_exist_checker) {
                    $CI->db->select($update_field);
                    $CI->db->order_by($update_field, 'DESC');
                    $CI->db->limit(1);
                    $result = $CI->db->get($table)->row_array();
                    $result = is_array($result) && isset($result[$update_field]) ? $result[$update_field] : NULL;
                    if ( !is_null($result) AND (date('Y-m-d H:i:s', strtotime($result)) == $result) ) {
                        return $result;
                    }

                    # Get by created_at
                    $created_at          = 'created_at';
                    $field_exist_checker = $CI->db->field_exists($created_at, $table);
                    if ($field_exist_checker) {
                        $CI->db->select($created_at);
                        $CI->db->order_by($update_field, 'DESC');
                        $CI->db->limit(1);
                        $result = $CI->db->get($table)->row_array();
                        if (is_array($result) && !empty($result)) {
                            return $result[$created_at];
                        }
                    }
                    return false;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    if (!function_exists('server_update')) {
        function server_update(){
            $CI = get_instance();

            $return     = array();
            # constants
            $message    = GOOGLE_MESSAGE;
            $key        = GOOGLE_API_KEY;
            $url        = GOOGLE_URL;
            # get list device
            $CI->db->where("deleted_at", null);
            $CI->db->where("actived", 1);
            $device_list = $CI->db->get(TBL_DEVICES)->result_array();

            if(isset($device_list) AND !empty($device_list) AND is_array($device_list)){
                $registrationid = array();
                foreach ($device_list as $key => $value) {
                    $device = $value['id_device'];
                    $registrationid[] = $value['id_unique'];
                }

                if(isset($registrationid) AND !empty($registrationid) AND is_array($registrationid)){
                    $fields = array(
                        'registration_ids'  => $registrationid, //array($registrationid),
                        'data'              => array("server_update" => $message)
                    );

                    $headers = array(
                            'Content-Type:application/json',
                            'Authorization:key='.$key
                    );

                    // Open connection
                    $ch = curl_init();
                    // Set the url, number of POST vars, POST data
                    curl_setopt( $ch, CURLOPT_URL, $url );
                    curl_setopt( $ch, CURLOPT_POST, true );
                    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                    curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields));
                    // Execute post
                    $result = curl_exec($ch);
                    // Close connection
                    curl_close($ch);
                    $result = $registrationid;
                }
            }
            # return
            return $result;
        }
    }

    if (!function_exists('remove_zero')) {
        function remove_zero($array = array()) {
            if ( is_array($array) AND !empty($array) ) {
                foreach ($array as $key => $value) {
                    if ($value == 0) {
                        unset($array[$key]);
                    }
                }
                return array_values($array);
            }
            return false;
        }
    }

    if (!function_exists('convert_param_array')) {
        /*
         * @author huongpm<phung.manh.huong@kloon.vn>
         * @convert param list to array
         * @return array
         * */
        function convert_param_array($input = NULL, $key = ','){
            $return = array();
            if(isset($input) && !empty($input)){
                $explode = explode($key, $input);
                if (is_array($explode) AND !empty($explode)) {
                    foreach ($explode as $item){
                        array_push($return, $item);
                    }
                }
            }
            return $return;
        }
    }

    if (!function_exists('get_server_path')) {
        function get_server_path(){
            return dirname($_SERVER["DOCUMENT_ROOT"] . $_SERVER['SCRIPT_NAME']).'/';
        }
    }

    if (!function_exists('img_rotate')) {
        function img_rotate($full_path = '',$rotate = 0){
            $status     = TRUE;
            $message    = 'rotate completed';
            $CI =& get_instance();
            $CI->load->library('image_lib');
            $config['source_image'] = $full_path;
            $config['rotation_angle'] = 360 - $rotate;
            $CI->image_lib->initialize($config);
            if (!$CI->image_lib->rotate()){
                $status     = FALSE;
                $message    = $CI->image_lib->display_errors();
            }
            return array('status' => $status, 'message' => $message);
        }
    }

    if (!function_exists('connect_db')) {
        function connect_db($db_config = array(), $id_city = false) {
            $CI =& get_instance();

            if ($id_city !== false) {
                $city = $CI->_db_global->get_where(TBL_CITY, array('id' => $id_city))->row();
                if (is_object($city) AND !empty($city)) {
                    $id_config = isset($city->id_config) ? $city->id_config : false;
                    if ($id_config !== false) {
                        $tmp = $CI->_db_global->select('database')->get_where(TBL_CITY_CONFIG, array('id' => $id_config))->row();
                        $db_config = json_decode($tmp->database);
                    } else {
                        write_log("Connect to db fail _ City info not found _ id_city = {$id_city}", 'fn_connect_db');
                        return false;
                    }
                } else {
                    write_log("Connect to db fail _ City info not found _ id_city = {$id_city}", 'fn_connect_db');
                    return false;
                }
            }

            if ((is_array($db_config) OR is_object($db_config)) AND !empty($db_config)) {
                $db_config = json_decode(json_encode($db_config));
                $config = array(
                    'hostname' => isset($db_config->hostname) ? $db_config->hostname : config_item('hostname'),
                    'username' => isset($db_config->username) ? $db_config->username : config_item('username'),
                    'password' => isset($db_config->password) ? $db_config->password : config_item('password'),
                    'database' => isset($db_config->database) ? $db_config->database : config_item('database'),
                    'port'     => isset($db_config->port)     ? $db_config->port     : config_item('db_port'),
                    'dbprefix' => config_item('db_prefix'),
                    'dbdriver' => config_item('db_dbdriver'),
                    'pconnect' => config_item('db_pconnect'),
                    'db_debug' => config_item('db_db_debug'),
                    'cache_on' => config_item('db_cache_on'),
                    'cachedir' => config_item('db_cachedir'),
                    'char_set' => config_item('db_char_set'),
                    'dbcollat' => config_item('db_dbcollat')
                );

                $db            = $CI->load->database($config, true);
                $db_connection = $db->initialize();
                if (!$db_connection) {
                    return false;
                }
                return $db;
            }
        }
    }

    if (!function_exists('convert_timezone')) {
        /**
         * Convert datetime by timezone
         * @param  int $timestamp   timestampt
         * @param  string $timezone CI timezone identifier
         * @return int              timestamp
         */
        function convert_timezone($timestamp = NULL, $timezone = NULL) {
            if (is_null($timestamp) || is_null($timezone)) {
                log_message('error', 'Timestamp or Timezone is invalid');
                return false;
            }

            $CI =& get_instance();
            $CI->load->helper('date');
            $daylight_saving = TRUE;
            return gmt_to_local($timestamp, $timezone, $daylight_saving);
        }
    }

    # ascii_code
    if (!function_exists('convert_ascii_code')) {
        /**
         * Convert datetime by timezone
         * @param  int $timestamp   timestampt
         * @param  string $timezone CI timezone identifier
         * @return int              timestamp
         */
        function convert_ascii_code($str) {
            $return = '';
            $ascii_code = array(
                '%A2' => '¢',
                '%A3' => '£',
                '%A4' => '€',
                '%A5' => '¥',
                '%B0' => '°',
                '%BC' => '¼',
                '%BC' => 'Œ',
                '%BD' => '½',
                '%BD' => 'œ',
                '%BE' => '¾',
                '%BE' => 'Ÿ',
                '%A1' => '¡',
                '%AB' => '«',
                '%BB' => '»',
                '%BF' => '¿',
                '%C0' => 'À',
                '%C1' => 'Á',
                '%C2' => 'Â',
                '%C3' => 'Ã',
                '%C4' => 'Ä',
                '%C5' => 'Å',
                '%C6' => 'Æ',
                '%C7' => 'Ç',
                '%C8' => 'È',
                '%C9' => 'É',
                '%CA' => 'Ê',
                '%CB' => 'Ë',
                '%CC' => 'Ì',
                '%CD' => 'Í',
                '%CE' => 'Î',
                '%CF' => 'Ï',
                '%D0' => 'Ð',
                '%D1' => 'Ñ',
                '%D2' => 'Ò',
                '%D3' => 'Ó',
                '%D4' => 'Ô',
                '%D5' => 'Õ',
                '%D6' => 'Ö',
                '%D8' => 'Ø',
                '%D9' => 'Ù',
                '%DA' => 'Ú',
                '%DB' => 'Û',
                '%DC' => 'Ü',
                '%DD' => 'Ý',
                '%DE' => 'Þ',
                '%DF' => 'ß',
                '%E0' => 'à',
                '%E1' => 'á',
                '%E2' => 'â',
                '%E3' => 'ã',
                '%E4' => 'ä',
                '%E5' => 'å',
                '%E6' => 'æ',
                '%E7' => 'ç',
                '%E8' => 'è',
                '%E9' => 'é',
                '%EA' => 'ê',
                '%EB' => 'ë',
                '%EC' => 'ì',
                '%ED' => 'í',
                '%EE' => 'î',
                '%EF' => 'ï',
                '%F0' => 'ð',
                '%F1' => 'ñ',
                '%F2' => 'ò',
                '%F3' => 'ó',
                '%F4' => 'ô',
                '%F5' => 'õ',
                '%F6' => 'ö',
                '%F8' => 'ø',
                '%F9' => 'ù',
                '%FA' => 'ú',
                '%FB' => 'û',
                '%FC' => 'ü',
                '%FD' => 'ý',
                '%FE' => 'þ',
                '%FF' => 'ÿ',
                '%0A' => "\n",
                '%0D' => "\r"
            );
            $key_array = array();
            preg_match_all('/./us', $str, $key_array);
            foreach ($key_array[0] as $key => $value) {
                $return .= (array_search($value, $ascii_code) !== FALSE) ? array_search($value, $ascii_code) : $value;
            }
            return $return;
        }
    }

    if (!function_exists('create_url')) {
        function create_url($key) {
            $CI =& get_instance();
            $CI->load->config('url_reserved');
            $url_reserved = $CI->config->item('url_reserved');
            return isset($url_reserved[$key]) ? base_url($url_reserved[$key]) : base_url($key);
        }
    }

    if (!function_exists('get_value')) {
        function get_value($key = '', $data = array(), $default = false) {
            if (is_array($data) OR is_object($data)) {
                $data = json_decode(json_encode($data), true);
            }
            return isset($data[$key]) ? $data[$key] : $default;
        }
    }

    /**
     * This file is part of the array_column library
     *
     * For the full copyright and license information, please view the LICENSE
     * file that was distributed with this source code.
     *
     * @copyright Copyright (c) 2013 Ben Ramsey <http://benramsey.com>
     * @license http://opensource.org/licenses/MIT MIT
     */
    if (!function_exists('array_column')) {

        /**
         * Returns the values from a single column of the input array, identified by
         * the $columnKey.
         *
         * Optionally, you may provide an $indexKey to index the values in the returned
         * array by the values from the $indexKey column in the input array.
         *
         * @param array $input A multi-dimensional array (record set) from which to pull
         *                     a column of values.
         * @param mixed $columnKey The column of values to return. This value may be the
         *                         integer key of the column you wish to retrieve, or it
         *                         may be the string key name for an associative array.
         * @param mixed $indexKey (Optional.) The column to use as the index/keys for
         *                        the returned array. This value may be the integer key
         *                        of the column, or it may be the string key name.
         * @return array
         */
        function array_column($input = null, $columnKey = null, $indexKey = null)
        {
            // Using func_get_args() in order to check for proper number of
            // parameters and trigger errors exactly as the built-in array_column()
            // does in PHP 5.5.
            $argc = func_num_args();
            $params = func_get_args();

            if ($argc < 2) {
                trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
                return null;
            }

            if (!is_array($params[0])) {
                trigger_error('array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given', E_USER_WARNING);
                return null;
            }

            if (!is_int($params[1])
                && !is_float($params[1])
                && !is_string($params[1])
                && $params[1] !== null
                && !(is_object($params[1]) && method_exists($params[1], '__toString'))
            ) {
                trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
                return false;
            }

            if (isset($params[2])
                && !is_int($params[2])
                && !is_float($params[2])
                && !is_string($params[2])
                && !(is_object($params[2]) && method_exists($params[2], '__toString'))
            ) {
                trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
                return false;
            }

            $paramsInput = $params[0];
            $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;

            $paramsIndexKey = null;
            if (isset($params[2])) {
                if (is_float($params[2]) || is_int($params[2])) {
                    $paramsIndexKey = (int) $params[2];
                } else {
                    $paramsIndexKey = (string) $params[2];
                }
            }

            $resultArray = array();

            foreach ($paramsInput as $row) {

                $key = $value = null;
                $keySet = $valueSet = false;

                if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
                    $keySet = true;
                    $key = (string) $row[$paramsIndexKey];
                }

                if ($paramsColumnKey === null) {
                    $valueSet = true;
                    $value = $row;
                } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
                    $valueSet = true;
                    $value = $row[$paramsColumnKey];
                }

                if ($valueSet) {
                    if ($keySet) {
                        $resultArray[$key] = $value;
                    } else {
                        $resultArray[] = $value;
                    }
                }

            }

            return $resultArray;
        }
    }

    if (!function_exists('get_tenant_id')) {
        function get_tenant_id() {
            $CI = get_instance();
            if (isset($CI->_tenantId) AND !is_null($CI->_tenantId)) {
                $CI->_tenantId = isset($_GET['id_city']) ? $_GET['id_city'] : false;
            } else {
                $all_headers = getallheaders();
                $header_key = 'Authorization';
                if (isset($all_headers[$header_key])) {
                    $tenantId = explode('|', $CI->encrypt->decode($all_headers[$header_key]));
                    if ( is_array($tenantId) AND isset($tenantId[0]) ) {
                        $CI->_tenantId = $tenantId[0];
                    }
                }
            }

            return intval($CI->_tenantId);
        }
    }
