<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    if (!function_exists('_debug')){
        /**
         * Debug output HTML for array
         * @param  array        $array_data (array debug)
         * @return output HTML  Output to client
         * @author chientran <tran.duc.chien@kloon.vn>
         */
        function _debug($array_data = array()){
            echo '<pre>';
            print_r($array_data);
            echo '</pre>';
        }
    }

    if (!function_exists('write_log')){
        /**
         * Write log to file
         *
         * @param string $error_message : Log Message
         * @author chientran@vietnambiz.com
         */
        function write_log($error_message = '', $type = 'api'){
            date_default_timezone_set('Asia/Ho_Chi_Minh');

            /* Log path */
            $root_path = $_SERVER['DOCUMENT_ROOT'] . $_SERVER['SCRIPT_FILENAME'];
            $root_path = dirname($root_path);
            $log_path  = APPPATH . 'logs/';
            $log_name  = 'log-'.date('Y-m-d', time()) . '_'.$type.'.php';
            $log_file  = $log_path . $log_name;

            $data = '';

            /* Create file if haven't exist */
            if (!file_exists($log_file)){
                $data .= "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>".PHP_EOL;
                $file_handler = @fopen($log_file, 'w');
                @fclose($file_handler);
            }

            /* Data for write to file */
            $current_time = gmdate('Y/m/d H:i:s', time());
            $data .= $current_time . ' : ';
            $data .= $error_message;
            if (isset($_SERVER['REMOTE_ADDR'])) {
                $data .= ' _ Remote IP '.$_SERVER['REMOTE_ADDR'];
            }

            # Router log
            $CI = get_instance();
            $data .= ' _ '.$CI->router->fetch_module().'/'.$CI->router->class.'/'.$CI->router->method;
            #$data .= ' _ '.$CI->router->class.'/'.$CI->router->method;

            $data .= PHP_EOL;

            /* Write data to file */
            @file_put_contents($log_file, $data, FILE_APPEND);
        }
    }

    if (!function_exists('check_db_connection')) {
        function check_db_connection($db_config, $return_instance = FALSE) {
            if ( !isset($db_config['dbdriver']) OR !isset($db_config['hostname']) OR !isset($db_config['username']) OR !isset($db_config['password']) OR !isset($db_config['database']) ) {
                return false;
            } else {
                $dbdriver = $db_config['dbdriver'];
                $hostname = $db_config['hostname'];
                $username = $db_config['username'];
                $password = $db_config['password'];
                $database = $db_config['database'];
                $port     = isset($db_config['port']) ? $db_config['port'] : NULL;

                // prep the DSN string
                $dsn = "{$dbdriver}://{$username}:{$password}@{$hostname}/{$database}";
                if($port !== NULL) {
                    $dsn .="?port={$port}";
                }

                // Load the database and dbutil
                $CI =& get_instance();
                $connection_backup = $CI->db;
                $CI->db = $CI->load->database($dsn, TRUE);
                $CI->load->dbutil();

                // check the connection details
                $check = $CI->dbutil->database_exists($database);

                # Reset Connection
                $CI->db->close();
                $CI->db = $connection_backup;

                # Return status
                if ($return_instance) {
                    return $check ? $CI->db : FALSE;
                } else {
                    return $check;
                }
            }
        }
    }