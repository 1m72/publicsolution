<?php
class worker {
	function __construct() {
		// Init
		require('db.php');

		// Helpers
		include('helpers/common_helper.php');
	}

	function _sync_activity($file_path = false, $dbconnect = null, $id_city = 0, $id_cron = 0) {
		$status = true;
		$msg    = '';
		if ($file_path AND !file_exists($file_path)) {
			$file_path = get_server_path() . $file_path;
		}
		// Check file path
		if (!$file_path OR (file_exists($file_path) == false)) {
			write_log('File not exist '.json_encode($file_path), 'tour_extract_sync_activity');
			$return = array(
				'status' => false,
				'msg'    => 'File not exist'
			);
			return $return;
		}

		// Check database connect
		if (!isset($dbconnect) OR empty($dbconnect)) {
			write_log('Connect not exist '.json_encode($dbconnect), 'tour_extract_sync_activity');
			$return = array(
				'status' => false,
				'msg'    => 'Connect not exist'
			);
			return $return;
		}

		// Read content from file
		$json_text = read_file($file_path);
		if (!$json_text OR empty($json_text)) {
			write_log('Can not read file '.json_encode($file_path), 'tour_extract_sync_activity');
			$return = array(
				'status' => false,
				'msg'    => 'Can not read file'
			);
			return $return;
		}

		// Decode data
		$json_data = json_decode($json_text, true);

		if (!is_array($json_data)) {
			write_log('File data is format invalid '.json_encode($json_data), 'tour_extract_sync_activity');
			$return = array(
				'status' => false,
				'msg'    => 'File data is format invalid'
			);
			return $return;
		}

		// Transaction start
		$dbconnect->trans_start();

		// Params
		$id_task     = isset($json_data['task_id'])     ? $json_data['task_id']     : '';
		$id_employee = isset($json_data['employee_id']) ? $json_data['employee_id'] : '';
		$id_machine  = isset($json_data['machine_id'])  ? $json_data['machine_id']  : '';
		$module_id   = isset($json_data['module_id'])   ? $json_data['module_id']   : 1;
		$module_id   = (intval($module_id) <= 0)        ? 1                         : $module_id;
		$end_time    = isset($json_data['end_time'])    ? $json_data['end_time']    : '';
		$start_time  = isset($json_data['start_time'])  ? $json_data['start_time']  : '';
		$id_gps      = FALSE;
		$gps         = $dbconnect->get_where(TBL_WORKER_GPS, array('id_task' => $id_task))->row_array();

		if (is_array($gps) AND !empty($gps)) {
			$id_gps = isset($gps['id']) ? $gps['id'] : FALSE;
		}

		if (empty($id_task) || !isset($id_task)) {
			$msg = 'Task id invalid '.json_encode($id_task);
			// echo "{$msg}\n";
			write_log($msg, 'tour_extract_sync_activity');

			$return = array(
				'status' => false,
				'msg'    => 'Task id invalid'
			);
			write_log('---> Result check task : ' . json_encode($return), 'tour_extract_sync_activity');
		}
		write_log("Check id_gps exist :  " . ($id_gps === false ? "NO EXIST" : "EXIST"), 'tour_extract_sync_activity');

		// Prepare gps detail record
		if ($id_gps === FALSE) {
			$gps_data = array(
				'id_city'    => $id_city,
				'id_task'    => $id_task,
				'id_cron'    => $id_cron,
				'id_object'  => 0,
				'id_worker'  => $id_employee,
				'id_machine' => $id_machine,
				'app'        => $module_id,
				'ontime'     => $start_time,
				'created_at' => date('Y-m-d H:i:s')
			);
			$db_flag  = $dbconnect->insert(TBL_WORKER_GPS, $gps_data);

			// Update id_task to table cron
			$this->db->where('data', str_replace(get_server_path(), '', $file_path));
			$cron_db_flag = $this->db->update(TBL_CRON, array('id_task' => $id_task, 'id_worker_gps' => $dbconnect->insert_id()));

			if ($db_flag !== false) {
				$id_gps         = $dbconnect->insert_id();
				$gps_data['id'] = $id_gps;
				$gps            = $gps_data;
				$msg = "Insert new gps {$id_gps}";
				// echo "{$msg}\n";
				write_log($msg, 'tour_extract_sync_activity');
			} else {
				$return = array(
					'status' => false,
					'msg'    => 'Cant not insert new gps _ Query: ' .$dbconnect->last_query(). ' _ Data: ' . json_encode($gps_data)
				);
				write_log(json_encode($return), 'tour_extract_sync_activity');
				return $return;
			}
		}

		// Check id_gps
		if (is_null($id_gps) OR (intval($id_gps) <= 0)) {
			$return = array(
				'status' => false,
				'msg'    => 'id_gps is invalid_Value: ' . $id_gps
			);
			write_log(json_encode($return), 'tour_extract_sync_activity');
		}

		// All activities
		$activities = array();
		if (!isset($json_data['activities']) OR empty($json_data['activities'])) {
			$return = array(
				'status' => false,
				'msg'    => 'Activity data is not exist'
			);
			write_log(json_encode($return), 'tour_extract_sync_activity');
		}

		$activities = walking_nodes($json_data['activities'], 'activityid', 'activitie');

		if (!is_array($activities) OR empty($activities)) {
			$return = array(
				'status' => false,
				'msg'    => 'Activity data parse is invalid_Data: ' . json_encode($activities)
			);
			write_log(json_encode($return), 'tour_extract_sync_activity');
		}

		// Walking activities elements
		$coordinates = array();
		$coordinate_string = '';

		$count = 0;

		foreach ($activities as $key_activity => $activity) {
			$count++;
			$activity_id               = isset($activity['activityid'])     ? trim($activity['activityid'])     : '';
			$activity_starttime        = isset($activity['starttime'])      ? trim($activity['starttime'])      : '';
			$activity_endtime          = isset($activity['endtime'])        ? trim($activity['endtime'])        : '';
			$activity_task_activity_id = isset($activity['taskactivityid']) ? trim($activity['taskactivityid']) : '';

			// Locations process
			$locations = array();
			if (isset($activity['locations']) && !empty($activity['locations'])) {
				$locations = $activity['locations'];
				$locations = walking_nodes($locations, 'lat', 'location');
				if (is_array($locations) AND !empty($locations)) {
					foreach ($locations as $key_location => $value_location) {
						$lat  = isset($value_location['lat'])  ? $value_location['lat']  : '';
						$lon  = isset($value_location['lon'])  ? $value_location['lon']  : '';
						$time = isset($value_location['time']) ? $value_location['time'] : '';
						$coordinates[] = array(
							'id_city'          => $id_city,
							'id_wkgps'         => $id_gps,
							'id_activity'      => $activity_id,
							'id_task'          => $id_task,
							'id_task_activity' => $activity_task_activity_id,
							'latitude'         => $lat,
							'longtitude'       => $lon,
							'starttime'        => $time,
							'endtime'          => $time,
							'created_at'       => date('Y-m-d H:i:s')
						);
						$coordinate_string .= "{$lat} {$lon} {$time},";
					}
				}
			}

			if (is_array($coordinates) AND !empty($coordinates)) {
				# insert last location
				if($count == count($activities)){
					$last_time = $coordinates[count($coordinates) - 1]['starttime'];

					if(strtotime($end_time) < strtotime($last_time)){
						// check information
						$dbconnect->where("id_task", $id_task);
						$dbconnect->where("id_worker_gps", $id_gps);
						$dbconnect->where("time > ", $last_time);
						$dbconnect->order_by("time","desc");

						$last_infor = $dbconnect->get(TBL_WORKER_INFORMATION)->result_array();

						if(isset($last_infor) AND !empty($last_infor)){
							$last_item = $last_infor[count($last_infor) - 1];
							$last_lat = $last_item["lat"];
							$last_lon = $last_item["lon"];
							$last_time = $last_item["time"];

							$coordinates[] = array(
								'id_city'          => $id_city,
								'id_wkgps'         => $id_gps,
								'id_activity'      => $activity_id,
								'id_task'          => $id_task,
								'id_task_activity' => $activity_task_activity_id,
								'latitude'         => $last_lat,
								'longtitude'       => $last_lon,
								'starttime'        => $last_time,
								'endtime'          => $last_time,
								'created_at'       => date('Y-m-d H:i:s')
							);
							$coordinate_string .= "{$last_lat} {$last_lon} {$last_time},";
						}
					} else {
						$coordinates[] = array(
							'id_city'          => $id_city,
							'id_wkgps'         => $id_gps,
							'id_activity'      => $activity_id,
							'id_task'          => $id_task,
							'id_task_activity' => $activity_task_activity_id,
							'latitude'         => $coordinates[count($coordinates) - 1]['latitude'],
							'longtitude'       => $coordinates[count($coordinates) - 1]['longtitude'],
							'starttime'        => $end_time,
							'endtime'          => $end_time,
							'created_at'       => date('Y-m-d H:i:s')
						);
					}
				}
				// $coordinates[] = $coordinates[count($coordinates) - 1];
				// $coordinates[count($coordinates) - 1]['starttime'] = $activity_endtime;
				// $coordinates[count($coordinates) - 1]['endtime'] = $activity_endtime;

				$db_flag = $dbconnect->insert_batch(TBL_WORKER_ACTIVITY, $coordinates);
				$coordinates = array();
				if ($db_flag === false) {
					write_log("Insert batch activities : FAIL", 'tour_extract_sync_activity');
				} else {
					write_log("Insert batch activities : OK", 'tour_extract_sync_activity');
					$total_activity = count($coordinates);
					$dbconnect->where('id', $id_gps);
					$flag = $dbconnect->update(TBL_WORKER_GPS, array('total_activity' => $total_activity));
					if($flag == false){
						write_log("Update total_activity : FAIL", 'tour_extract_sync_activity');
					} else {
						write_log("Update total_activity : OK", 'tour_extract_sync_activity');
					}
				}
			}
		}

		# Update worker_gps.position
		$coordinate_string = trim($coordinate_string, ',');
		$dbconnect->where('id', $id_gps);
		$dbconnect->set('position', $coordinate_string);
		$dbconnect->set('updated_at', date('Y-m-d H:i:s'));
		$db_flag = $dbconnect->update(TBL_WORKER_GPS);
		if ($db_flag === false) {
			write_log("Update position for worder_gps {$id_gps} FAIL", 'sync_activity');
		}

		// Transaction end
		$dbconnect->trans_complete();

		$return = array(
			'status'    => true,
			'id_wkgps'  => $id_gps,
			'msg'       => 'The data has been successfully process'
		);

		write_log('---> End result : ' . json_encode($return), 'tour_extract_sync_activity');
		return $return;
	}

	function run() {
		// start - extract_tour_data
		sleep(3);

		// Set limit timeout : 300 seconds = 5 minutes
		ini_set('max_execution_time', 300);

		// Libs
		require_once(BASEPATH.'libraries/pheanstalk/pheanstalk_init.php');
		require_once(BASEPATH.'helpers/file_helper.php');

		// Get jobs
		$pheanstalk        = new Pheanstalk(getenv('QUEUE_PHEANSTALK_HOST'));
		$pheanstalk_status = $pheanstalk->getConnection()->isServiceListening(); // true or false
		if ($pheanstalk_status === false) {
			write_log('Pheanstalk connection error _ ' . json_encode($pheanstalk), 'pheanstalk');
			die();
		}

		$start = time();
		while(1) {
			$job      = $pheanstalk->watchOnly(QUE_SYNC)->ignore('default')->reserve();
			$job_data = json_decode($job->getData(), true);
			$pheanstalk->delete($job);
			if(!isset($job_data) || empty($job_data)){
				write_log('Data invalid ', 'tour_extract');
				return false;
			}

			// file path
			$file_path = isset($job_data['data']) ? $job_data['data'] : '';
			write_log("file_path : {$file_path}", 'tour_extract');
			if (!empty($file_path) AND !file_exists($file_path)) {
				$file_path = get_server_path() . $file_path;
			}

			// id_cron
			$id_cron = isset($job_data['id_cron']) ? $job_data['id_cron'] : '';
			write_log("id_cron : {$id_cron}", 'tour_extract');

			// city id
			$id_city = isset($job_data['id_city']) ? $job_data['id_city'] : '';
			echo "<br/> id_city : {$id_city} \n";

			// connect
			$db_connect = connect_db(null, $id_city);

			// activity
			$result = $this->_sync_activity($file_path, $db_connect, $id_city, $id_cron);
			print_r($result);

			$update = array();
			if (isset($result['status']) AND $result['status']) {
				$update['status']   = CRON_STATUS_SUCCESS;
				$update['last_run'] = date('Y-m-d H:i:s');
				// Push job to resolve street name
				$id_wkgps   = isset($result['id_wkgps']) ? $result['id_wkgps'] : false;
				$job = array(
					'id_city'   => $id_city,
					'id_wkgps'  => $id_wkgps,
					'id_cron'   => $id_cron
				);
				$job_flag = push_job(QUE_RESOLVE_STREET_NAME, json_encode($job));
				$msg = "Push job to queue ".QUE_RESOLVE_STREET_NAME." : " . ($job_flag === false ? 'FAIL' : 'OK');
				echo "{$msg}\n";
				write_log($msg, 'tour_extract');
			} else {
				$update['status'] = CRON_STATUS_ERROR;
				if ( isset($result['msg']) ) {
					$update['note'] = $result['msg'];
				}
			}
			$this->db->where('id', $id_cron);
			$db_flag = $this->db->update(TBL_CRON, $update);
			$msg = "Update cron status : " . ($db_flag === false ? 'FAIL' : 'OK');
			echo "{$msg}\n";
			write_log($msg, 'tour_extract');
		}
		// end - extract_tour_datass
	}
}

// Init and run worker
$worker = new worker();
$worker->run();
