<?php
class worker {
	function __construct() {
		// Init
		require('db.php');

		// Helpers
		require_once('helpers/common_helper.php');
	}

	/**
	 * Extract email
	 * @return [type] [description]
	 * @author Chien Tran <tran.duc.chien@kloon.vn>
	 *
	 * Beanstalk data structure : <id_email>
	 */
	function run() {
		// sleep(5);

		// Pheanstalk library
		require_once(BASEPATH.'libraries/pheanstalk/pheanstalk_init.php');

		// Email config
		require_once(APPPATH.'config/email_template.php');
		$email_template = $config;

		// Beanstalk Init
		$pheanstalk = new Pheanstalk(getenv('QUEUE_PHEANSTALK_HOST'));

		// get config
		global $config;
		$config = array_merge($config, $email_template);

		$start = time();
		while((time() - $start) < 300) {
			$job          = $pheanstalk->watchOnly(QUE_EMAIL_EXTRACT)->ignore('default')->reserve();
			$job_data_raw = $job->getData();
			$job_data     = $job_data_raw;
			$pheanstalk->delete($job);
			$this->db->reconnect();
			write_log("Get and delete job : {$job_data}", 'email_extract');

			if ($job_data) {
				write_log("Email extract {$job_data}", 'email_extract');

				// id_email
				$id_email = $job_data;

				// Email template
				$email_template = $config['email_template_sync_notification'];

				$this->db->where('status', EMAIL_STATUS_WAITING);
				$this->db->where('id', $id_email);
				$this->db->limit(1);
				$data_extract = $this->db->get(TBL_EMAIL)->result_array();

				// ectract email
				if (isset($data_extract) && !empty($data_extract)) {
					// Email Account
					$email_account_config = $config['email_account'];
					$email_account        = $email_account_config['noreply'];

					$array_extract        = array();
					foreach ($data_extract as $extract) {
						$data_detail = json_decode($extract['data'], true);
						if ($data_detail) {
							$id_city        = isset($data_detail['id_city']) ? $data_detail['id_city'] : false;
							$id_gps         = isset($data_detail['id_gps']) ? $data_detail['id_gps'] : false;
							$id_information = isset($data_detail['id_information']) ? $data_detail['id_information'] : false;
							$id_module      = isset($data_detail['module_id']) ? $data_detail['module_id'] : false;
							$street         = isset($data_detail['street']) ? $data_detail['street'] : false;
							$id_email       = $extract['id'];

							// Logs
							write_log("---> Extracting email {$id_email} _ city: {$id_city}", 'email_extract');

							// db connect
							$db_connect = connect_db(null, $id_city);
							if ($db_connect === false) {
								write_log("Connect to database FAIL _ city: {$id_city}", 'email_extract');
							} else {
								// mail address
								$_to_emmail = array();
								$_cc        = array();
								$_bcc       = array();

								// get mail city
								$_city = $this->db->get_where(TBL_CITY, array('id' => $id_city))->row_array();
								if(is_array($_city) AND !empty($_city)){
									$extra_email = isset($_city['email_notification']) ? $_city['email_notification'] : FALSE;
									if ($extra_email) {
										$extra_email = explode(';', $extra_email);
										$_cc         = array_merge($_cc, $extra_email);
									}
								}

								// get user
								$_user_array = $this->db->get_where(TBL_USERS, array('id_city' => $id_city))->result_array();
								if (is_array($_user_array) && !empty($_user_array)) {
									foreach ($_user_array as $value) {
										$email       = isset($value['email']) ? $value['email'] : FALSE;
										if ($email) {
											$_to_emmail[] = $email;
										}
									}
								}

								// get module
								$module_detail = array();
								$module_detail = $db_connect->get_where(TBL_MODULES, array('id' => $id_module))->row_array();
								$module_email  = '';
								if(is_array($module_detail) AND !empty($module_detail)){
									$module_email = isset($module_detail['email']) ? $module_detail['email'] : '';
								}

								// email
								$_to_emmail = implode(';', array_unique($_to_emmail)) . ';' . $module_email;
								$_cc        = implode(';', array_unique($_cc));

								$taskid = $data_detail['taskid'];

								// Employee
								$employeeid       = $data_detail['employeeid'];
								$_employee_name   = '';
								$_employee_detail = $db_connect->get_where(TBL_WORKER, array('id' => $employeeid))->row_array();
								if (is_array($_employee_detail) && !empty($_employee_detail)) {
									$_employee_name = isset($_employee_detail['first_name']) ? $_employee_detail['first_name'] : '';
									$_employee_name .= isset($_employee_detail['last_name']) ? (' ' . $_employee_detail['last_name']) : '';
								}

								// Machine
								$machineid       = $data_detail['machineid'];
								$_machine_name   = '';
								$_machine_detail = $db_connect->get_where(TBL_MACHINE, array('id' => $machineid))->row_array();
								if (is_array($_machine_detail) && !empty($_machine_detail)) {
									$_machine_name = isset($_machine_detail['machine_code']) ? $_machine_detail['machine_code'] : '';
									$_machine_name .= isset($_machine_detail['name']) ? (' '.$_machine_detail['name']) : '';
								}
								$type       = $data_detail['type'];
								$data       = $data_detail['data'];
								$created_at = $data_detail['ontime'];
								$lat        = isset($data_detail['lat']) ? urldecode(trim($data_detail['lat'])) : '';
								$lon        = isset($data_detail['lon']) ? urldecode(trim($data_detail['lon'])) : '';
								if(($lat == '0.0') AND ($lon == '0.0')){
									$url_infor  = "{Unknown gps}";
								} else {
									// $url_infor  = $config['webapp_link'] . "sys/information/?id_city={$id_city}&id={$id_information}&id_gps={$id_gps}&lat={$lat}&lon={$lon}";
									$url_infor  = '(<a href="' . $config['webapp_link'] . "sys/information/?id_city={$id_city}&id={$id_information}&id_gps={$id_gps}&lat={$lat}&lon={$lon}" . '" target="_blank" style="font-style: italic;">Klicken Sie hier, um auf der Karte zu sehen</a>)';

								}

								// Email from
								$from_email = $email_account['email'];
								$from_name  = $email_account['display_name'];

								// Reply to
								$reply_email = $email_account['email'];
								$reply_name  = $email_account['display_name'];

								// Email To
								$to_email   = trim($_to_emmail,';');
								$to_name    = $_employee_name;

								// Email message
								$subject = isset($email_template['subject']) ? $email_template['subject'] : '';
								$subject = str_replace('[MODULE_NAME]', $module_detail['title'], $subject);
								$message = isset($email_template['message']) ? $email_template['message'] : '';
								$message = str_replace('[WORKER]', $_employee_name, $message);
								$message = str_replace('[MACHINE]', $_machine_name, $message);
								$message = str_replace('[TIME]', $created_at, $message);
								if (empty($lat) OR empty($lon)) {
									$message = str_replace('[GPS_COONDINATE]', '', $message);
									$message = str_replace('[GPS_COONDINATE_LINK]', '', $message);
								} else {
									$message = str_replace('[GPS_COONDINATE]', "{$lat} - {$lon}", $message);
									$message = str_replace('[GPS_COONDINATE_LINK]', $url_infor, $message);
								}
								$message = str_replace('[STREETNAME]', $street, $message);

								if ($type != INFO_MESSAGE){
									$attach = $data;
								} else {
									$message = str_replace('[INFO_MSG]', $data, $message);
								}
								$message = str_replace('[INFO_MSG]', '', $message);

								$tmp = array(
									'id'          => $id_email,
									'status'      => EMAIL_STATUS_PENDING,
									'from_email'  => $from_email,
									'from_name'   => $from_name,
									'reply_email' => $reply_email,
									'reply_name'  => $reply_name,
									'subject'     => $subject,
									'message'     => $message,
								);

								if (isset($to_email) AND (!empty($to_email)))  {
									$tmp['to_email'] = $to_email;
								} else {
									write_log('Email extract error_ID: '.$id_email, 'email');
									$tmp['status'] = EMAIL_STATUS_ERROR;
								}

								if (isset($to_name) AND (!empty($to_name)))  {
									$tmp['to_name'] = $to_name;
								}

								if (isset($_cc) AND (!empty($_cc)))  {
									$tmp['cc'] = trim($_cc,';');
								}

								if (isset($_bcc) AND (!empty($_bcc)))  {
									$tmp['bcc'] = $_bcc;
								}

								if (isset($attach) AND (!empty($attach)))  {
									$tmp['attach'] = $attach;
								}

								$array_extract[] = $tmp;

								// Logs
								write_log(json_encode($tmp), 'email_extract');
							}
						} else {
							$array_extract[] = array(
								'id'          => $id_email,
								'from_email'  => $email_account['email'],
								'from_name'   => $email_account['display_name'],
								'reply_email' => $email_account['email'],
								'reply_name'  => $email_account['display_name'],
								'status'      => EMAIL_STATUS_PENDING
							);
						}
					}

					// update extract
					if (isset($array_extract) && !empty($array_extract)) {
						foreach ($array_extract as $key => $value) {
							$this->db->where('id' , $value['id']);
							$email_flag = $this->db->update(TBL_EMAIL, $value);
							write_log('Update batch email data _ Query: '. $this->db->last_query(), 'email_extract');
							if($email_flag !== FALSE){
								write_log('OK data ' . $value['id'], 'email_extract');

								push_job(QUE_EMAIL_SEND , $value['id']);
								write_log('push_job OK ' . $value['id'], 'email_extract');
							} else {
								write_log('FAIL data ' . $value['id'], 'email_extract');
							}
						}
						$email_flag = $this->db->update_batch(TBL_EMAIL, $array_extract, 'id');
						write_log('Update batch email data _ Query: '. $this->db->last_query(), 'email_extract');
						if ($email_flag !== FALSE) {
							write_log('FAIL', 'email_extract');
							$return = array(
								'version' => $config['api_version'],
								'status'  => STATUS_SUCCESS,
								'msg'     => 'Email has been extracted'
							);
							return $return;
						} else {
							write_log('OK', 'email_extract');
							$return = array(
								'version' => $config['api_version'],
								'status'  => STATUS_FAIL,
								'msg'     => 'Has an error when extract email'
							);
							return $return;
						}
					}
				} else {
					write_log('Email extract is empty', 'email_extract');
					$return = array(
						'version' => $config['api_version'],
						'status'  => STATUS_FAIL,
						'msg'     => 'Email extract is empty'
					);
					return $return;
				}
			} else {
				// Logs
				write_log("---> Email job data empty _ Data: {$job_data_raw}", 'email_extract');
			}

			sleep(1);
		}
	}
}

// Init and run worker
$worker = new worker();
$worker->run();
