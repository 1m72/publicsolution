<?php
# push_job
if (!function_exists('push_job')){
	function push_job($tube = '', $data = ''){
		require_once(BASEPATH.'libraries/pheanstalk/pheanstalk_init.php');
		if($tube == '') {
			return FALSE;
		}

		$pheanstalk = new Pheanstalk(getenv('QUEUE_PHEANSTALK_HOST'));
		return $pheanstalk->useTube($tube)->put($data);
	}
}

# get_server_path
if (!function_exists('get_server_path')) {
	function get_server_path(){
		return dirname(dirname(dirname(__FILE__))) . '/';
	}
}

# connect_db
if (!function_exists('connect_db')) {
	function connect_db($db = NULL,$id_city = false) {
		if ($id_city !== false) {
			$CI = get_instance();
			if (is_null($CI->db->conn_id) OR (!$CI->db->conn_id)) {
				require_once(BASEPATH . 'database/DB.php');
				require_once(APPPATH.'config/'.ENVIRONMENT.'/database.php');

				// Init default db
				$db_config = $db['default'];
				$CI->db    = DB($db_config);
			} else {
				$CI->db->reconnect();
			}

			# get city detail
			$CI->db->where('deleted_at', NULL);
			$CI->db->where('id', $id_city);
			$city = $CI->db->get(TBL_CITY)->row_array();
			$id_config = isset($city['id_config']) ? $city['id_config'] : FALSE;
			if($id_config === FALSE){
				return false;
			}

			# get config detail
			$CI->db->where('deleted_at', NULL);
			$CI->db->where('id_city', $id_city);
			$CI->db->where('id', $id_config);
			$configDetail = $CI->db->get(TBL_CITY_CONFIG)->row_array();
			$config = isset($configDetail['database']) ? json_decode($configDetail['database'],true) : FALSE;
			if($config == FALSE){
				return false;
			}
			$db_config = array(
				'hostname' => isset($config['hostname']) ? $config['hostname'] : '',
				'username' => isset($config['username']) ? $config['username'] : '',
				'password' => isset($config['password']) ? $config['password'] : '',
				'database' => isset($config['database']) ? $config['database'] : '',
			);
			return db_connect($db_config);
		}

		return false;
	}
}

if (!function_exists('convert_ascii_code')) {
	/**
	 * Convert datetime by timezone
	 * @param  int $timestamp   timestampt
	 * @param  string $timezone CI timezone identifier
	 * @return int              timestamp
	 */
	function convert_ascii_code($str) {
		$return = '';
		$ascii_code = array(
			'%A2' => '¢',
			'%A3' => '£',
			'%A4' => '€',
			'%A5' => '¥',
			'%B0' => '°',
			'%BC' => '¼',
			'%BC' => 'Œ',
			'%BD' => '½',
			'%BD' => 'œ',
			'%BE' => '¾',
			'%BE' => 'Ÿ',
			'%A1' => '¡',
			'%AB' => '«',
			'%BB' => '»',
			'%BF' => '¿',
			'%C0' => 'À',
			'%C1' => 'Á',
			'%C2' => 'Â',
			'%C3' => 'Ã',
			'%C4' => 'Ä',
			'%C5' => 'Å',
			'%C6' => 'Æ',
			'%C7' => 'Ç',
			'%C8' => 'È',
			'%C9' => 'É',
			'%CA' => 'Ê',
			'%CB' => 'Ë',
			'%CC' => 'Ì',
			'%CD' => 'Í',
			'%CE' => 'Î',
			'%CF' => 'Ï',
			'%D0' => 'Ð',
			'%D1' => 'Ñ',
			'%D2' => 'Ò',
			'%D3' => 'Ó',
			'%D4' => 'Ô',
			'%D5' => 'Õ',
			'%D6' => 'Ö',
			'%D8' => 'Ø',
			'%D9' => 'Ù',
			'%DA' => 'Ú',
			'%DB' => 'Û',
			'%DC' => 'Ü',
			'%DD' => 'Ý',
			'%DE' => 'Þ',
			'%DF' => 'ß',
			'%E0' => 'à',
			'%E1' => 'á',
			'%E2' => 'â',
			'%E3' => 'ã',
			'%E4' => 'ä',
			'%E5' => 'å',
			'%E6' => 'æ',
			'%E7' => 'ç',
			'%E8' => 'è',
			'%E9' => 'é',
			'%EA' => 'ê',
			'%EB' => 'ë',
			'%EC' => 'ì',
			'%ED' => 'í',
			'%EE' => 'î',
			'%EF' => 'ï',
			'%F0' => 'ð',
			'%F1' => 'ñ',
			'%F2' => 'ò',
			'%F3' => 'ó',
			'%F4' => 'ô',
			'%F5' => 'õ',
			'%F6' => 'ö',
			'%F8' => 'ø',
			'%F9' => 'ù',
			'%FA' => 'ú',
			'%FB' => 'û',
			'%FC' => 'ü',
			'%FD' => 'ý',
			'%FE' => 'þ',
			'%FF' => 'ÿ',
			'%0A' => "\n",
			'%0D' => "\r"
		);
		$key_array = array();
		preg_match_all('/./us', $str, $key_array);
		foreach ($key_array[0] as $key => $value) {
			$return .= (array_search($value, $ascii_code) !== FALSE) ? array_search($value, $ascii_code) : $value;
		}
		return $return;
	}
}

if (!function_exists('resolve_street_name')) {
	function resolve_street_name($latitude, $longtitude, $service = 'kloon') {
		global $config;
		$services = $config['resolve_services'];

		if (!isset($services[$service])) {
			$service = $services['mapquest'];
		} else {
			$service = $services[$service];
		}

		$street = '';
		if ((isset($latitude) && !empty($latitude)) && (isset($longtitude) && !empty($longtitude))) {
			$service_url    = '';
			$service_type   = $service;
			$count  = 0;
			$status = FALSE;
			while ( ($count < 3) AND ($status != TRUE) ) {
				switch ($count) {
					case 1:
						$service_type = 'mapquest';
						$service_url  = $services['mapquest'] . '&lat=' . $latitude . '&lon=' . $longtitude;
						break;

					case 2:
						$service_type = 'openstreetmap';
						$service_url  = $services['openstreetmap'] . '&lat=' . $latitude . '&lon=' . $longtitude;
						break;

					default:
						$service_type = 'kloon';
						$service_url  = $services['kloon'] . '&lat=' . $latitude . '&lon=' . $longtitude;
						break;
				}

				$street = '';
				if ($service_url != FALSE) {
					$street_data = '';
					$ch = curl_init();
					$ch_timeout            = 15000;
					$ch_connection_timeout = 2000;
					curl_setopt($ch, CURLOPT_URL, $service_url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					#curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
					curl_setopt($ch, CURLOPT_TIMEOUT, $ch_timeout);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, $ch_connection_timeout);
					$street_data = curl_exec($ch);
					curl_close($ch);

					$street_array = json_decode($street_data,true);
					if (isset($street_array['address']) && !empty($street_array['address'])) {
						$street_detail = $street_array['address'];
						if(isset($street_detail['road']) && !empty($street_detail['road'])){
							$street_name = isset($street_detail['road']) ? $street_detail['road'] : '';
							$street      = $street_name;
						} else {}
					}
				}

				# Logs
				write_log("Synchronize controller: {$service_type}: {$latitude} - {$longtitude} {$street}", 'resolve_street_name');

				if( (isset($street) AND !empty($street)) OR ($count >= 3) ){
					$status = TRUE;
				}
				$count++;

				usleep(100);
			}
		}
		return $street;
	}
}

if (!function_exists('walking_nodes')) {
	function walking_nodes($data = array(), $key_checker = '', $key_value = '') {
		$return = array();
		# Has one
		if (isset($data[$key_value]) && isset($data[$key_value][$key_checker]) || isset($data[$key_checker])) {
			$return[] = $data[$key_value];
		} else {
			# Has many
			if (!empty($key_value) && isset($data[$key_value]) && !empty($data)) {
				$return = $data[$key_value];
			} else {
				if(isset($data) && !empty($data)){
					foreach ($data as $key => $value) {
						$return[] = $value;
					}
				}
			}
		}
		return $return;
	}
}

if (!function_exists('get_app_config')) {
	function init_app_config($key = '') {
		switch ($key) {
			case 'database':
				# code...
				break;

			case 'constant':
				# code...
				break;

			default:
				# code...
				break;
		}
	}
}

if (!function_exists('get_value')) {
	function get_value($key = '', $data = array(), $default = false) {
		if (is_array($data) OR is_object($data)) {
			$data = json_decode(json_encode($data), true);
		}
		return isset($data[$key]) ? $data[$key] : $default;
	}
}