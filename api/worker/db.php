<?php
error_reporting(E_ALL);

// Composer autoloader
require_once( dirname(dirname(__FILE__)) . '/vendor/autoload.php');

// Load Dotenv
$dotenv = new Dotenv\Dotenv(dirname(__DIR__));
$dotenv->load();

// Set timezone
date_default_timezone_set('Europe/Berlin');

define('DB_DEBUG', true);
define('DB_LOAD_FORGE', false);

// This should be the base path to the database folder
if ( ! defined('BASEPATH')) {
    define('BASEPATH', pathinfo(__FILE__, PATHINFO_DIRNAME).'/');
}

if ( ! defined('APPPATH')) {
    $path = dirname(pathinfo(__FILE__, PATHINFO_DIRNAME)).'/application/';
    define('APPPATH', $path);
}

if ( ! defined('ENVIRONMENT')) {
    $path = is_callable('getenv') ? getenv('APP_ENV') : 'production';
    define('ENVIRONMENT', $path);
}

function write_log($error_message = '', $type = ''){
    $root_path = dirname($_SERVER["DOCUMENT_ROOT"] . $_SERVER['SCRIPT_NAME']).'/';
    $log_path  = $root_path . 'logs/';
    $log_name  = date('Y-m-d', time()) . (empty($type) ? '' : ("_$type")).'.log';
    $log_file  = $log_path . $log_name;
    $data      = '';

    /* Create file if haven't exist */
    if (!file_exists($log_file)){
        $file_handler = @fopen($log_file, 'w');
        @fclose($file_handler);
    }

    /* Data for write to file */
    $current_time = gmdate('Y-m-d H:i:s', time());
    $data .= $current_time . ' : ';
    $data .= $error_message;
    if (isset($_SERVER['REMOTE_ADDR'])) {
        $data .= ' _ Remote IP '.$_SERVER['REMOTE_ADDR'];
    }
    $data .= PHP_EOL;

    @file_put_contents($log_file, $data, FILE_APPEND);
}

function get_instance() {
    global $db;
    $item = new stdClass();
    if (isset($db)) {
        $item->db = $db;
        return ($item);
    } else {
        return (null);
    }
}

function log_message($level = 'error', $message, $php_error = FALSE) {
    if (DB_DEBUG) {
        echo "$message\n";
    };
}

function show_error($message, $status_code = 500, $heading = 'An Error Was Encountered') {
    if (DB_DEBUG) {
        echo "$message\n";
    };
}

function db_connect($db_config = null) {
    if (!is_null($db_config)) {
        $db_config_default             = array();
        $db_config_default['autoinit'] = true;
        $db_config_default['db_debug'] = false;
        // $db_config_default['hostname'] = "localhost";
        // $db_config_default['username'] = "myusername";
        // $db_config_default['password'] = "mypassword";
        // $db_config_default['database'] = "mydatabase";
        $db_config_default['dbdriver'] = "mysqli";
        $db_config_default['dbprefix'] = "";
        $db_config_default['pconnect'] = FALSE;
        $db_config_default['db_debug'] = TRUE;
        $db_config_default['cache_on'] = FALSE;
        $db_config_default['cachedir'] = "";
        $db_config_default['char_set'] = "utf8";
        $db_config_default['dbcollat'] = "utf8_general_ci";
    }

    $db_config = json_decode(json_encode($db_config), true);
    $db_config = array_merge($db_config_default, $db_config);

    $db_connect = DB($db_config);
    if (isset($db_connect->conn_id) AND !empty($db_connect->conn_id)) {
        return $db_connect;
    }
    write_log("DB connect fail _ DEBUG username: {$db_connect->username}, password: {$db_connect->password}, hostname: {$db_connect->hostname}, database: {$db_connect->database}, dbprefix: {$db_connect->dbprefix}, char_set: {$db_connect->char_set}, dbcollat: {$db_connect->dbcollat}", 'db');
    return false;
}

function test_connection($config) {
    $config = json_decode(json_encode($config), true);
}

// Open the config file
require_once (BASEPATH . 'database/DB.php');

// App configs
global $db, $config;
include_once(APPPATH.'config/database.php');
include_once(APPPATH.'config/constants.php');
include_once(APPPATH.'config/MY_config.php');

// Load DB Forge
if (DB_LOAD_FORGE) {
    require_once(BASEPATH . 'database/DB_forge.php');
    require_once(BASEPATH . 'database/DB_utility.php');
    require_once(BASEPATH . 'database/drivers/' . $db->dbdriver . '/' . $db->dbdriver . '_utility.php');
    require_once(BASEPATH . 'database/drivers/' . $db->dbdriver . '/' . $db->dbdriver . '_forge.php');
    $class = 'CI_DB_' . $db->dbdriver . '_forge';
    $dbforge = new $class();
}

// Init default db
$db_config = $db['default'];
$this->db  = DB($db_config);

$db = $this->db;
if (isset($this->db->error_msg) AND !empty($this->db->error_msg)) {
    write_log($this->db->error_msg . "_DEBUG username: {$this->db->username}, password: {$this->db->password}, hostname: {$this->db->hostname}, database: {$this->db->database}, dbprefix: {$this->db->dbprefix}, char_set: {$this->db->char_set}, dbcollat: {$this->db->dbcollat}", 'db');
    if (DB_DEBUG) {
        die($this->db->error_msg);
    }
}
