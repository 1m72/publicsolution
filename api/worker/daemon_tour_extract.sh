#!/bin/bash
# @date: 14 Feb 2014
# @author: tran.duc.chien@kloon.vn

CURRENT_PATH=`dirname $(readlink -f $0)`
ROOT_PATH="$CURRENT_PATH"
APP_ENV="$2"

TOTAL_WORKER=3
WORKER_NAME="tour_extract"
LOG_FILE=$CURRENT_PATH"/logs/daemon_$WORKER_NAME.log"
SCRIPT_FILE=$ROOT_PATH"/worker_$WORKER_NAME.php"
SCRIPT_NAME=$APP_ENV"_$WORKER_NAME"

start() {
        echo 'Starting...'

        for (( i=1; i<=$TOTAL_WORKER; i++ ))
        do
            name=$SCRIPT_NAME'_'$i
            daemon -n $name -X /usr/bin/php $SCRIPT_FILE -r -L 10 -A 50 -O $LOG_FILE -E $LOG_FILE
        done

        echo 'Done !'
}

stop() {
        echo 'Stoping...'

        pid=`ps aux | grep "$SCRIPT_FILE" | grep -v grep | awk '{print $2}'`
        if [ "$pid" != "" ]; then
            # echo $pid
            kill -9 $pid
        fi

        echo 'Done !'
}

### main logic ###
case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart)
        stop
        start
        ;;
  *)
        echo $"Usage: $0 {start|stop|restart}"
        exit 1
esac

exit 0