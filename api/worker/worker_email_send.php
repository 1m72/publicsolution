<?php
class worker {
	function __construct() {
		// Init
		require('db.php');

		// Helpers
		require_once('helpers/common_helper.php');
	}

	/**
	 * Send email
	 * @return [type] [description]
	 * @author Chien Tran <tran.duc.chien@kloon.vn>
	 *
	 * Jobs structure : <id_email>
	 */
	function send(){
		sleep(5);

		// Library
		require_once(BASEPATH.'libraries/pheanstalk/pheanstalk_init.php');
		require_once(BASEPATH.'libraries/PHPMailer/PHPMailerAutoload.php');

		// Email template config
		include(APPPATH.'config/email_template.php');
		$email_template = $config;

		// Global config
		global $config;
		$config = array_merge($config, $email_template);

		// Beanstalk Init
		$pheanstalk = new Pheanstalk(getenv('QUEUE_PHEANSTALK_HOST'));

		// Exit after x minute : fix memory leak
		$start = time();
		while((time() - $start) < 300) {
			$job          = $pheanstalk->watchOnly(QUE_EMAIL_SEND)->ignore('default')->reserve();
			$job_data_raw = $job->getData();
			$job_data     = $job_data_raw;
			$pheanstalk->delete($job);
			$this->db->reconnect();

			if (!empty($job_data)) {
				// Logs
				write_log("---> Sending email {$job_data} : ", 'email_send');

				// PHP Mailer
				$mailer = new PHPMailer(true);
				$mailer->isHTML(true);
				$mailer->CharSet = "UTF-8";

				// Reconnect mysql
				$this->db->reconnect();
				$this->db->initialize();

				// Default email information
				$email_noreply_account = isset($config['email_account']['noreply']) ? $config['email_account']['noreply'] : false;
				$default_from_name     = isset($email_noreply_account['email']) ? isset($email_noreply_account['email']) : 'BIS-Office System';
				$default_from_email    = isset($email_noreply_account['display_name']) ? isset($email_noreply_account['display_name']) : 'BIS-Office System';

				// Email information
				$job_data   = $this->db->get_where(TBL_EMAIL, array('id' => $job_data))->row_array();
				$from_email = isset($job_data['from_email']) ? $job_data['from_email'] : $default_from_email;
				$from_name  = isset($job_data['from_name'])  ? $job_data['from_name']  : $default_from_email;
				$to_email   = isset($job_data['to_email'])   ? $job_data['to_email']   : '';
				$to_name    = isset($job_data['to_name'])    ? $job_data['to_name']    : $to_email;
				$cc         = isset($job_data['cc'])         ? $job_data['cc']         : '';
				$bcc        = isset($job_data['bcc'])        ? $job_data['bcc']        : '';

				// Email to
				$to_email_checker = false;
				$to_email = explode(';', $to_email);
				if (is_array($to_email) AND !empty($to_email)) {
					foreach ($to_email as $key => $value) {
						if (filter_var($from_email, FILTER_VALIDATE_EMAIL)) {
							$to_email_checker = true;
							$mailer->addAddress($value, $to_name);
							write_log("Email to: {$value}", 'email_send');
						}
					}
				}

				if ($to_email_checker) {
					// Email from
					if (filter_var($from_email, FILTER_VALIDATE_EMAIL)) {
						$mailer->setFrom($from_email, $from_name);
						write_log("Email from: {$from_email}", 'email_send');
					} else {
						write_log("Email from: {$from_email}", 'email_send');
					}

					// Email reply to
					$reply_email = (isset($job_data['reply_email']) AND filter_var($job_data['reply_email'], FILTER_VALIDATE_EMAIL) ) ? $job_data['reply_email'] : $default_from_email;
					$reply_name  = (isset($job_data['reply_name'])  AND !empty($job_data['reply_name']) ) ? $job_data['reply_name'] : $default_from_name;
					if (filter_var($reply_email, FILTER_VALIDATE_EMAIL)) {
						$mailer->addReplyTo($reply_email, $reply_name);
						write_log("Email reply to: {$reply_email}", 'email_send');
					} else {
						write_log("Email reply to wrong: {$reply_email}", 'email_send');
					}

					// Email CC
					if ($cc AND !empty($cc)) {
						$cc = explode(';', $cc);
						if (is_array($cc) AND !empty($cc)) {
							foreach ($cc as $key => $value) {
								if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
									$mailer->AddCC($value, $value);
									write_log("Add email cc: {$value}", 'email_send');
								} else {
									write_log("Email cc wrong: {$value}", 'email_send');
								}
							}
						}
					}

					// Email BCC
					if ($bcc AND !empty($bcc)) {
						$bcc = explode(';', $bcc);
						if (is_array($bcc) AND !empty($bcc)) {
							foreach ($bcc as $key => $value) {
								if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
									$mailer->AddBCC($value, $value);
									write_log("Add email bcc: {$value}", 'email_send');
								} else {
									write_log("Email bcc wrong: {$value}", 'email_send');
								}
							}
						}

						$email_admin_default = array('tran.duc.chien@kloon.vn', 'phung.manh.huong@kloon.vn');
						$email_admin = isset($config['email_admin']) ? $config['email_admin'] : $email_admin_default;
						if ($email_admin AND is_array($email_admin) AND !empty($email_admin)) {
							foreach ($email_admin as $key => $value) {
								if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
									$mailer->AddBCC($value, $value);
									write_log("Add email admin bcc: {$value}", 'email_send');
								} else {
									write_log("Email bcc admin wrong: {$value}", 'email_send');
								}
							}
						}
					}

					// Email subject
					$email_subject = isset($job_data['subject']) ? $job_data['subject'] : '';
					$mailer->Subject = $email_subject;
					write_log("Email subject: {$email_subject}", 'email_send');

					// Email message HTML
					$email_message = isset($job_data['message']) ? $job_data['message'] : '';
					$mailer->msgHTML($email_message);

					// Email attach
					$attachs = (isset($job_data['attach']) AND (!empty($job_data['attach'])) ) ? explode(';', $job_data['attach']) : false;
					if (is_array($attachs) AND (!empty($attachs))) {
						foreach ($attachs as $key => $value) {
							$path = get_server_path() . '/' . $value;
							if (file_exists($path)) {
								$mailer->addAttachment($path);
								write_log("Email attach: {$path}", 'email_send');
							}
						}
					}

					// Send email
					try {
						$result = $mailer->send();
						if ($result !== false){
							write_log('OK', 'email_send');

							// Update status success
							$this->db->where('id', $job_data['id']);
							$db_flag = $this->db->update(
								TBL_EMAIL,
								array(
									'status'      => EMAIL_STATUS_SUCCESS,
									'from_email'  => $from_email,
									'from_name'   => $from_name,
									'reply_email' => $default_from_email,
									'send_at'     => date('Y-m-d H:i:s')
								)
							);

							if ($db_flag === false) {
								write_log("Update email  {$id_email} FAIL _ Query : " . $this->db->last_query(), 'email_send');
							}
						} else {
							write_log('Send mail ERROR' . $email->ErrorInfo, 'email_send');

							// Update status error and log error
							$this->db->where('id', $job_data['id']);
							$this->db->set('re_send', 're_send+1', FALSE);
							$this->db->set('status', EMAIL_STATUS_ERROR);
							$this->db->set('from_email', $default_from_email);
							$this->db->set('reply_email', empty($job_data['reply_email']) ? $default_from_email : $job_data['reply_email']);
							write_log('Send mail FAIL _ Email ID : '.$job_data['id'].' _ Message:'.json_encode($mailer).' _ email_config: '.json_encode($email_config).' _ Job data : '.json_encode($job_data), 'email_send');
							$db_flag = $this->db->update(TBL_EMAIL);
							if ($db_flag === false) {
								write_log("Update email  {$id_email} FAIL _ Query : " . $this->db->last_query(), 'email_send');
							}
						}
					} catch (phpmailerException $e) {
						write_log('Send mail ERROR _ Debug: ' . $e->errorMessage(), 'email_send');
					}
				} else {
					write_log("ERROR email_to is required _ Data: " . $job_data_raw, 'email_send');
				}
			} else {
				// Logs
				write_log("---> Email job data empty _ Data: " . $job_data_raw, 'email_send');
			}
			sleep(1);
		}
	}

	/**
	 * Send an email for testing
	 * @return [type]
	 */
	function test() {
		require BASEPATH . 'libraries/PHPMailer/PHPMailerAutoload.php';

		$mailer = new PHPMailer();
		$mailer->isSendmail();
		$mailer->setFrom('noreply@bis-office.com', 'BIS-Office');
		$mailer->addReplyTo('noreply@bis-office.com', 'BIS-Office');
		$mailer->addAddress('chientd1989@yopmail.com', 'Chien Tran');
		$mailer->addAddress('chientd1989@yahoo.com.vn', 'Chien Tran');
		$mailer->addAddress('br.tdchien@yahoo.com.vn', 'Chien Tran');
		$mailer->Subject = 'Test send mail';
		$mailer->msgHTML("This is a full-text message body");
		$mailer->AltBody = 'This is a plain-text message body';
		// $mailer->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
		// $mailer->addAttachment('images/phpmailer_mini.png');

		if (!$mailer->send()) {
			echo "Mailer Error: " . $mailer->ErrorInfo;
		} else {
			echo "Message sent!";
		}
	}

	function run() {
		global $argc, $argv;

		$argc = intval($argc);
		if ($argc > 1) {
			$type = isset($argv[1]) ? $argv[1] : false;
			switch ($type) {
				case 'test':
					$this->test();
					break;

				default:
					$this->send();
					break;
			}
		} else {
			$this->send();
		}
	}
}

// Init and run worker
$worker = new worker();
$worker->run();
