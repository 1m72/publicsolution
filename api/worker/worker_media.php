<?php
class worker {
	function __construct() {
		// Init
		require('db.php');

		// Helpers
		include('helpers/common_helper.php');
	}

	/**
	 * Convert audio file
	 * @param  boolean $file_path : Path file convert
	 * @param  boolean $path_new  : Path new file
	 * @return boolean            : Result
	 * @created : 28 Feb 2-14
	 * @author Chien Tran <tran.duc.chien@kloon.vn>
	 */
	function convert_audio($file_path = false, $path_new = false, $rename = false){
		if (!$file_path) {
			write_log("---> Converting file fail. file_path is required", 'media');
			return false;
		}

		write_log("---> Converting file : {$file_path}", 'media');

		if (!file_exists($file_path)) {
			$file_path = get_server_path() .'/'. $file_path;
		}

		if (!file_exists($file_path)) {
			write_log("File not exists : {$file_path}", 'media');
			return false;
		}

		// Lib
		require APPPATH.'/libraries/class.audio_streamer.php';

		// Converting
		try {
			// $sound    = new audio_streamer($file_path);
			// $new_file = $sound->save();

			// Save new file
			$new_file = str_replace('.3gp', '.mp3', $file_path);
			$log = exec_shell('/usr/bin/ffmpeg -i {$bak_file} {$new_file}');
			write_log($log, 'media');
			write_log("Save new file : {$bak_file} -> {$new_file}", 'media');

			// Backup old file
			$bak_file = str_replace('.3gp', '_bak.3gp', $file_path);
			write_log("Backup file : {$file_path} -> {$bak_file}", 'media');
			rename($file_path, $bak_file);

			// Rename new file
			rename($new_file, $file_path);

			// Chmod files
			write_log("Chmod files", 'media');
			@chmod($bak_file, 0644);
			@chmod($file_path, 0644);

			return true;
		} catch (Exception $e) {
			write_log(json_encode($e), 'media');
			return false;
		}
	}

	function convert_audio_v2($file_path = false, $path_new = false, $rename = false) {
		if (!$file_path) {
			write_log("---> Converting file fail. file_path is required", 'media');
			return false;
		}

		if (!file_exists($file_path)) {
			$file_path = get_server_path() .'/'. $file_path;
		}

		write_log("---> Converting file : {$file_path}", 'media');

		if (!file_exists($file_path)) {
			write_log("File not exists : {$file_path}", 'media');
			return false;
		}

		// Converting
		try {

			$bak_file = str_replace('.3gp', '_bak.3gp', $file_path);
			$new_file = str_replace('.3gp', '.mp3', $file_path);
			$result   = shell_exec("ffmpeg -y -i {$file_path} -ar 32k -f mp3 {$new_file}");

			@chmod($bak_file, 0755);
			@chmod($file_path, 0755);
			@chmod($new_file, 0755);

			// Backup old file
			$bak_file = str_replace('.3gp', '_bak.3gp', $file_path);
			write_log("Backup file : {$file_path} -> {$bak_file}", 'media');
			rename($file_path, $bak_file);

			// Save new file
			write_log("Save new file : {$new_file} -> {$file_path}", 'media');
			write_log($result, 'media');
			rename($new_file, $file_path);

			// Chmod files
			write_log("Chmod files", 'media');
			@chmod($bak_file, 0644);
			@chmod($file_path, 0644);

			return true;
		} catch (Exception $e) {
			write_log(json_encode($e), 'media');
			return false;
		}
	}

	/**
	 * Convert video file
	 * @param  boolean $file_path : Path file convert
	 * @param  boolean $path_new  : Path new file
	 * @return boolean            : Result
	 * @date   28 Feb 2014
	 * @author Chien Tran <tran.duc.chien@kloon.vn>
	 */
	function convert_video($file_path = false, $path_new = false, $rename = false){
		return true;
	}

	/**
	 * Rotate image file
	 * @param  boolean $file_path : Path file rotate
	 * @param  boolean $rotate    : Degree
	 * @param  boolean $path_new  : Path new file
	 * @param  boolean $rename    : New name
	 * @return [type]             : Result
	 */
	function rotate_image($file_path = false, $rotate = false, $path_new = false, $rename = false) {
		if (!$file_path) {
			write_log("---> Rotate image fail. file_path is required", 'media');
			return false;
		}

		write_log("---> Rotate {$rotate} image : {$file_path}", 'media');

		if (!file_exists($file_path)) {
			$file_path = get_server_path() .'/'. $file_path;
		}

		if (!file_exists($file_path)) {
			$file_path = get_server_path() . $file_path;
		}

		if (!file_exists($file_path)) {
			write_log("File not exists : {$file_path}", 'media');
			return false;
		}

		if (!$rotate) {
			write_log("Rotate param is required : {$file_path}", 'media');
			return false;
		} else {
			$rotate = floatval($rotate);
		}

		$result = $this->_rotate_image($file_path, $rotate);
		write_log($result['message'], 'media');
		if (isset($result['status']) AND ($result['status'] == true)) {
			return true;
		} else {
			return false;
		}
	}

	function _rotate_image($filename = '', $degrees = 0) {
		$degrees = 360 - floatval($degrees);

		// Load
		$source = imagecreatefromjpeg($filename);

		// Rotate
		$rotate = imagerotate($source, $degrees, 0);

		// Output
		$result = imagejpeg($rotate, $filename);

		// Free the memory
		imagedestroy($source);
		imagedestroy($rotate);

		$return = array(
			'status'  => $result,
			'message' => "Image {$filename} has been rotate {$degrees}"
		);
		return $return;
	}

	function run() {
		// sleep(5);

		// Pheanstalk lib
		require_once(APPPATH.'libraries/pheanstalk/pheanstalk_init.php');

		// Pheanstalk init
		$pheanstalk = new Pheanstalk(getenv('QUEUE_PHEANSTALK_HOST'));

		// Exit after x minute : fix memory leak
		$start = time();
		while(1) {
			$job        = $pheanstalk->watchOnly(QUE_MEDIA)->ignore('default')->reserve();
			$job_data   = json_decode($job->getData(), true);
			$pheanstalk->delete($job);
			$this->db->reconnect();

			$id   = isset($job_data['id'])   ? $job_data['id']   : false;
			$path = isset($job_data['path']) ? $job_data['path'] : false;
			$type = isset($job_data['type']) ? $job_data['type'] : false;

			// Check params
			if ( (!$path) OR (!$type) ) {
				write_log("Param is invalid : {$id} _ {$type} _ {$path}", 'media');
				return false;
			}

			// Check exists
			// if (!file_exists($path)) {
			//     $path = get_server_path() . $path;
			// }

			// Log
			write_log("Start process type {$type} : {$id} {$path}", 'media');

			$result = false;
			switch ($type) {
				case INFO_VIDEO:
					$result = $this->convert_video($path);
					break;

				case INFO_VOICE:
					$result = $this->convert_audio_v2($path);
					break;

				case INFO_IMAGE:
					$rotate = isset($job_data['rotate']) ? $job_data['rotate'] : false;
					$result = $this->rotate_image($path, $rotate);
					break;

				default:
					write_log("Convert default case {$type} : {$id} {$path}", 'media');
					break;
			}
			write_log("End convert", 'media');

			// Update status
			$this->db->where('type', $type);
			// $this->db->where('data', $path);
			$this->db->where('id', $id);
			$this->db->set('status', (($result === false) ? INFO_STATUS_ERROR : INFO_STATUS_SUCCESS));
			$this->db->set('last_run', date('Y-m-d H:i:s'));
			$db_flag = $this->db->update(TBL_CRON);
			write_log("DB update status " . ($db_flag === false ? 'OK' : 'FAIL'), 'media');

			sleep(1);
		}
	}
}

// Init and run worker
$worker = new worker();
$worker->run();
