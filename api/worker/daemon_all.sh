#!/bin/bash
CURRENT_PATH=`dirname $(readlink -f $0)`

case "$2" in
    dev)
        APP_ENV='dev'
        ;;

    test)
        APP_ENV='test'
        ;;

    pro)
        APP_ENV='pro'
        ;;

    *)
        echo 'Using second param for environment'
        exit
        ;;
esac

# echo $APP_ENV
# exit

start() {
    echo 'Starting...'
    $CURRENT_PATH/daemon_email_extract.sh start $APP_ENV
    $CURRENT_PATH/daemon_email_send.sh start $APP_ENV
    $CURRENT_PATH/daemon_gcm.sh start $APP_ENV
    $CURRENT_PATH/daemon_media.sh start $APP_ENV
    $CURRENT_PATH/daemon_object_location.sh start $APP_ENV
    $CURRENT_PATH/daemon_post_service.sh start $APP_ENV
    $CURRENT_PATH/daemon_street.sh start $APP_ENV
    $CURRENT_PATH/daemon_tour_extract.sh start $APP_ENV
    echo 'Done !'
}

stop() {
    echo 'Stoping...'
    $CURRENT_PATH/daemon_email_extract.sh stop $APP_ENV
    $CURRENT_PATH/daemon_email_send.sh stop $APP_ENV
    $CURRENT_PATH/daemon_gcm.sh stop $APP_ENV
    $CURRENT_PATH/daemon_media.sh stop $APP_ENV
    $CURRENT_PATH/daemon_object_location.sh stop $APP_ENV
    $CURRENT_PATH/daemon_post_service.sh stop $APP_ENV
    $CURRENT_PATH/daemon_street.sh stop $APP_ENV
    $CURRENT_PATH/daemon_tour_extract.sh stop $APP_ENV
    echo 'Done !'
}

### main logic ###
case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        stop
        start
        ;;
    *)
        echo $"Usage: $0 {start|stop|restart}"
        exit 1
esac

exit 0