<?php
class worker {
    function __construct() {
        // Init
        require('db.php');

        // Helpers
        include('helpers/common_helper.php');
    }

    function run() {
        // start - extract_tour_data
        // sleep(3);

        // Set limit timeout : 300 seconds = 5 minutes
        ini_set('max_execution_time', 300);

        // Libs
        require_once(BASEPATH.'libraries/pheanstalk/pheanstalk_init.php');
        require_once(BASEPATH.'helpers/file_helper.php');

        // Get jobs
        $pheanstalk        = new Pheanstalk(getenv('QUEUE_PHEANSTALK_HOST'));
        $pheanstalk_status = $pheanstalk->getConnection()->isServiceListening(); // true or false
        if ($pheanstalk_status === false) {
            write_log('Pheanstalk connection error _ ' . json_encode($pheanstalk), 'pheanstalk');
            die('Pheanstalk connection error');
        }

        $start = time();
        $counter = 1;
        while($counter == 1) {
            $counter++;
            $job      = $pheanstalk->watchOnly(QUE_OBJECT_LOCATION)->ignore('default')->reserve();
            $job_data = json_decode($job->getData(), true);
            $pheanstalk->delete($job);
            if(!isset($job_data) || empty($job_data)){
                write_log('Data invalid ', 'object_locations');
                return false;
            }

            // file path
            $file_path = isset($job_data['data']) ? $job_data['data'] : '';
            write_log("file_path : {$file_path}", 'object_locations');
            if (!empty($file_path) AND !file_exists($file_path)) {
                $file_path = get_server_path() . $file_path;
            }

            // city id
            $id_city = isset($job_data['id_city']) ? $job_data['id_city'] : '';
            echo PHP_EOL . "id_city : {$id_city}" . PHP_EOL;

            // connect
            $db_connect = connect_db(null, $id_city);

            $data_json = file_get_contents($file_path);
            $data_json = json_decode($data_json, true);
            if (is_array($data_json) AND !empty($data_json)) {
                $object_ids = array();
                foreach ($data_json as $key => $value) {
                    $object_ids[] = get_value('object_id', $value);
                }

                $object_list = array();
                if (is_array($object_ids) AND !empty($object_ids)) {
                    $tmp = $db_connect->select('id, position')->where_in('id', $object_ids)->get(TBL_OBJECT)->result_array();
                    if (!empty($tmp)) {
                        foreach ($tmp as $key => $value) {
                            $object_list[$value['id']] = $value['position'];
                        }
                    }
                }
                // die;

                $batch_data = array();
                foreach ($data_json as $key => $value) {
                    $object_id       = get_value('object_id', $value);
                    $locations       = get_value('locations', $value);
                    $location_string = '';
                    if (is_array($locations) AND !empty($locations)) {
                        foreach ($locations as $location) {
                            $location_string .= get_value('lat', $location) . ' ' . get_value('lon', $location) . ',';
                        }
                    }
                    $location_string = trim($location_string, ',');

                    if (isset($object_list[$object_id])) {
                        $location_string = str_replace(')', '', $object_list[$object_id]) . (',' . $location_string . ')');
                        $batch_data[] = array(
                            'id'       => $object_id,
                            'position' => $location_string
                        );
                    }
                }
                if (!empty($batch_data)) {
                    $result = $db_connect->update_batch(TBL_OBJECT, $batch_data, 'id');
                    if ($result !== false) {
                        echo 'Success' . PHP_EOL;
                    } else {
                        echo 'Fail' . PHP_EOL;
                    }
                } else {
                    echo 'Batch_data empty' . PHP_EOL;
                }
            }
        }
    }
}

// Init and run worker
$worker = new worker();
$worker->run();
