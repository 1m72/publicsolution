<?php
class gcm {
    function __construct() {
        // Init
        require('db.php');

        // Helpers
        require_once('helpers/common_helper.php');
    }

    function gcm_server_update($registrationid = array(), $msg_key = 'server_update', $msg_body = GOOGLE_MESSAGE, $log_id = false, $app = APP_WINTER_TEXT) {
        if (isset($registrationid) AND !empty($registrationid) AND is_array($registrationid)) {
            $key = ($app == APP_FASTTASK_TEXT) ? getenv('GCM_KEY_FASTTASK') : getenv('GCM_KEY_WINTERDIENTS');
            $url = GOOGLE_URL;

            $fields = array(
                'registration_ids' => $registrationid,
                'data'             => array(
                    $msg_key => $msg_body
                )
            );

            $headers = array(
                'Content-Type:application/json',
                'Authorization:key='.$key
            );

            // Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Request at
            $requested_at = date('Y-m-d H:i:s');

            // Execute post
            $result             = curl_exec($ch);
            $curl_error_no      = curl_errno($ch);
            $curl_response_info = curl_getinfo($ch);
            $curl_status_code   = isset($curl_response_info['http_code']) ? $curl_response_info['http_code'] : '';
            $curl_status        = ($curl_status_code == 200) ? 'Success' : 'Fail';
            write_log('Result : ' . $result, 'gcm');

            // Close connection
            curl_close($ch);

            // Third party Log
            if ($log_id) {
                $CI = get_instance();

                $log = $CI->db->get_where(TBL_3RD_API_LOGS, array('id' => $log_id))->row_array();
                if (!empty($log)) {
                    $request_data = json_decode(get_value('request_data', $log));
                    if (is_array($request_data)) {
                        $request_data['registrationid'] = $registrationid;
                    } else {
                        $request_data = $fields;
                    }
                    $data = array(
                        'response_code' => $curl_status_code,
                        'response_data' => $result,
                        'status'        => $curl_status,
                        'request_data'  => json_encode(array('rquest_data' => $request_data, 'key' => $key, 'app' => $app, 'msg_key' => $msg_key, 'msg_body' => $msg_body)),
                        'requested_at'  => $requested_at,
                    );
                    $db_flag = $CI->db->where('id', $log_id)->update(TBL_3RD_API_LOGS, $data);
                } else {
                    $log_data = array(
                        'id_city'        => $CI->_tenantId,
                        'uri'            => $url,
                        'request_data'   => json_encode(array('rquest_data' => $fields, 'key' => $key, 'app' => $app, 'msg_key' => $msg_key, 'msg_body' => $msg_body)),
                        'response_code'  => $curl_status_code,
                        'response_data'  => $result,
                        'requested_at'   => $requested_at,
                        'created_at'     => date('Y-m-d H:i:s'),
                        'status'         => $curl_status,
                        // 'request_log' => '',
                        // 'retry'       => '',
                        // 'attachments' => '',
                    );
                    $db_flag = $CI->db->insert(TBL_3RD_API_LOGS, $log_data);
                    if ($db_flag === false) {
                        write_log('Add log fail_Data: ' . json_encode($log_data), 'worker_gcm');
                    } else {
                        write_log('worker_gcm');
                    }
                }
            }

            return;
        } else {
            write_log('registrationid empty', 'gcm');
            return false;
        }
    }

    /**
     * Worker for Google cloud message
     * Job data : id_city, app, msg_key, msg_body, log_id
     * @return [type]
     */
    function run() {
        sleep(3);

        // Set limit timeout : 300 seconds = 5 minutes
        ini_set('max_execution_time', 300);

        // Pheanstalk lib
        require_once(APPPATH.'libraries/pheanstalk/pheanstalk_init.php');

        // Pheanstalk init
        $pheanstalk = new Pheanstalk(getenv('QUEUE_PHEANSTALK_HOST'));

        // Get jobs
        $pheanstalk_status = $pheanstalk->getConnection()->isServiceListening(); // true or false
        if ($pheanstalk_status === false) {
            $msg = 'Pheanstalk connection error _ ' . json_encode($pheanstalk);
            write_log($msg, 'pheanstalk');
            die($msg);
        }

        // Exit after x minute : fix memory leak
        $start = time();
        while(1) {
            write_log("Start post service", 'post_service');
            $job        = $pheanstalk->watchOnly(QUE_SERVER_UPDATE)->ignore('default')->reserve();
            $job_data   = json_decode($job->getData(), true);
            $pheanstalk->delete($job);
            $this->db->reconnect();

            if (!isset($job_data) || empty($job_data)) {
                write_log('Data invalid ', 'post_service');
                return false;
            }

            $id_city  = get_value('id_city', $job_data);
            $app      = get_value('app', $job_data);
            $app      = in_array($app, array(APP_WINTER_TEXT, APP_FASTTASK_TEXT)) ? $app : APP_WINTER_TEXT;

            if (!isset($id_city) OR empty($id_city)) {
                write_log('id_city empty', 'gcm');
                return false;
            }
            # connect
            $db_connect = connect_db(null, $id_city);
            $db_connect->where("deleted_at", null);
            $db_connect->where("actived", 1);
            $db_connect->where('app', $app);
            $db_connect->where('notification', true);
            $devices = $db_connect->get(TBL_DEVICES)->result_array();

            if (!isset($devices) OR empty($devices) OR !is_array($devices)) {
                write_log('Data devices error : ' . json_encode($devices), 'gcm');
                return false;
            }

            $registrationid = array();
            foreach ($devices as $key => $value) {
                if (isset($value["id_unique"]) AND !empty($value["id_unique"])) {
                    $registrationid[] = $value["id_unique"];
                }
            }

            if (!isset($registrationid) OR empty($registrationid) OR !is_array($registrationid)) {
                write_log('Registrationid error : ' . json_encode($registrationid), 'gcm');
                return false;
            }

            // Set msg_body for winterdients app
            $msg_key  = get_value('msg_key', $job_data);
            $msg_body = get_value('msg_body', $job_data);
            $log_id   = get_value('log_id', $job_data);
            if ($app == APP_WINTER_TEXT) {
                $msg_body = implode($registrationid, ',');
            }

            // Call Google cloud message
            $this->gcm_server_update($registrationid, $msg_key, $msg_body, $log_id, $app);

            sleep(1);
        }
    }
}

$gcm = new gcm();
$gcm->run();
