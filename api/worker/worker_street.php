<?php
class worker {
	private $_service_provider          = array();
	private $_service_provider_selected = '';
	private $_service_provider_fails    = array();

	function __construct() {
		// Init
		require('db.php');

		// Helpers
		include('helpers/common_helper.php');

		// Service provider
		$this->_service_provider = array(
			'bis'           => 'http://nominatim.bis-office.com/reverse.php?format=json&zoom=17&addressdetails=1',
			'kloon'         => 'http://nominatim.kloon.net/reverse.php?format=json&zoom=17&addressdetails=1',
			'mapquest'      => 'http://open.mapquestapi.com/nominatim/v1/reverse.php?format=json&zoom=17',
			'openstreetmap' => 'http://nominatim.openstreetmap.org/reverse?format=json&zoom=17&addressdetails=1',
			'gisgraphy'     => 'http://services.gisgraphy.com/street/streetsearch?format=json&from=1&to=1',
		);

		foreach ($this->_service_provider as $key => $value) {
			$this->_service_provider_fails[$key] = 0;
		}
	}

	// resolve_street_name
	function resolve_street_name($latitude, $longtitude, $service = 'kloon') {
		$services = $this->_service_provider;

		if (!isset($services[$service])) {
			$service = $services['mapquest'];
		} else {
			$service = $services[$service];
		}

		$street = '';
		if ((isset($latitude) && !empty($latitude)) && (isset($longtitude) && !empty($longtitude))) {
			$service_url    = '';
			$service_type   = $service;
			$count  = 0;
			$status = FALSE;
			while ( ($count < count($this->_service_provider)) AND ($status != TRUE) ) {
				switch ($count) {
					case 1:
						$service_type = 'mapquest';
						$service_url  = $services['mapquest'] . '&lat=' . $latitude . '&lon=' . $longtitude;
						break;

					case 2:
						$service_type = 'bis';
						$service_url  = $services['kloon'] . '&lat=' . $latitude . '&lon=' . $longtitude;
						break;

					case 3:
						$service_type = 'openstreetmap';
						$service_url  = $services['openstreetmap'] . '&lat=' . $latitude . '&lon=' . $longtitude;
						break;

					case 4:
						$service_type = 'gisgraphy';
						$service_url  = $services['gisgraphy'] . '&latitude=' . $latitude . '&longitude=' . $longtitude;
						break;

					default:
						$service_type = 'kloon';
						$service_url  = $services['kloon'] . '&lat=' . $latitude . '&lon=' . $longtitude;
						break;
				}
				$this->_service_provider_selected = $service_type;
				$street = $this->curl_read_content($service_url, $service_type);

				// Logs
				write_log("{$service_type}: {$latitude} - {$longtitude} {$street}", 'street_resolv');

				// Logs service fails
				if (empty($street) AND isset($this->_service_provider_fails[$service_type])) {
					$this->_service_provider_fails[$service_type] = intval($this->_service_provider_fails[$service_type]) + 1;
				}

				if( (isset($street) AND !empty($street)) OR ($count >= count($this->_service_provider)) ){
					$status = TRUE;
				}
				$count++;

				usleep(100);
			}
		}
		return $street;
	}

	// curl_read_content
	function curl_read_content($link = FALSE, $service = 'kloon', $timeout = 2500){
		$street = '';
		if($link != FALSE){
			$street_data = '';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $link);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			#curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
			curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 2000);
			$street_data = curl_exec($ch);
			curl_close($ch);

			$street_array = json_decode($street_data,true);
			if (isset($street_array['address']) && !empty($street_array['address'])) {
				$street_detail = $street_array['address'];
				if(isset($street_detail['road']) && !empty($street_detail['road'])){
					$street_name = isset($street_detail['road']) ? $street_detail['road'] : '';
					$street      = $street_name;
				} else {
					if(isset($street_detail['address26']) && !empty($street_detail['address26'])){
						$street_name = isset($street_detail['address26']) ? $street_detail['address26'] : '';
						$street      = $street_name;
					}
				}
			}
		}
		return $street;
	}

	/**
	 * Update group_street field for grouping
	 * @param  [type] $id_city
	 * @param  [type] $id_wkgps
	 * @return [type]
	 */
	function group_street($id_city = null, $id_wkgps = null, $db_connect = null) {
		if (is_null($id_city)) {
			$msg = 'Requried id_city';
			write_log($msg, 'street_clean_unnecessary');
			return false;
		}

		if (is_null($db_connect)) {
			$db_connect = connect_db(null, $id_city);
		}

		if (!$db_connect) {
			$msg = array(
				'status' => false,
				'msg'    => 'Connect fail'
			);
			write_log(json_encode($msg), 'street_clean_unnecessary');
			return false;
		}

		if (is_null($id_wkgps)) {
			$db_connect->select('id_wkgps');
			$db_connect->where('group_street', 0);
			$data = $db_connect->get(TBL_WORKER_ACTIVITY)->row();
			if (isset($data->id_wkgps)) {
				$id_wkgps = $data->id_wkgps;
			} else {
				write_log('id_wkgps is required', 'street_clean_unnecessary');
				return false;
			}
		}
		write_log("---> id_wkgps : ".$id_wkgps, 'street_clean_unnecessary');

		# Activites
		$db_connect->where('deleted_at', NULL, false);
		$db_connect->where('id_wkgps', $id_wkgps);
		$db_connect->order_by('id', 'asc');
		$activities = $db_connect->get(TBL_WORKER_ACTIVITY)->result_array();

		if (is_array($activities) AND !empty($activities)) {
			write_log('------------------------------------------', 'street_clean_unnecessary');
			$batch_data = array();
			if (is_array($activities) AND !empty($activities)) {
				$counter = 1;
				$last_street = '';
				foreach ($activities as $key => $value) {
					$current_street = trim($value['street']);
					if (empty($last_street)) {
						$last_street = $current_street;
					}
					$street_checker = array();
					if (is_array($value) AND !empty($value)) {
						if ($last_street != $current_street) {
							$counter++;
						}
						$last_street = $current_street;
						$batch_data[] = array(
							'id'           => $value['id'],
							'group_street' => $counter
						);
					}
				}

				write_log('---> Batch data :', 'street_clean_unnecessary');
				if (is_array($batch_data) AND !empty($batch_data)) {
					$msg = '---> Update batch worker_activity : ';
					$db_flag = $db_connect->update_batch(TBL_WORKER_ACTIVITY, $batch_data, 'id');
					if ($db_flag === false) {
						$msg .= 'FAIL _ Query : '. $this->db->last_query();
					} else {
						$msg .= 'OK _ Query : '. $this->db->last_query();
					}
					write_log($msg, 'street_group_street');
				}
			}
		}

		# Close db connection
		$db_connect->close();
	}

	function clean_unnecessary_streets_v2($id_city = null, $id_wkgps = null, $db_connect = null){
		if ( is_null($id_city) OR is_null($id_wkgps) ) {
			write_log('id_city or id_wkgps can not null', 'street_clean_unnecessary');
			return false;
		}

		if (is_null($db_connect)) {
			$db_connect = connect_db(null, $id_city);
		}

		# Logs
		write_log("Start clean_unnecessary_streets2 _ id_city: {$id_city} _ id_wkgps: {$id_wkgps}", 'street_clean_unnecessary');

		# get activiti group
		$activiti_result = array();

		# Set max lenght group_concat function
		$db_connect->query('SET SESSION group_concat_max_len = 1000000000');

		$db_connect->select("wac.id_task_activity, group_concat(wac.id ORDER BY wac.id) as id_group, group_concat(wac.street_old  ORDER BY wac.id) as street_name_old,wac.id_wkgps,wac.id_activity,min(wac.starttime) as starttime,max(wac.endtime) as endtime, (max(wac.endtime) - min(wac.starttime)) as duration,wac.street,wac.group_street");
		$db_connect->from(TBL_WORKER_ACTIVITY.' as wac');
		$db_connect->where('wac.deleted_at',NULL);
		$db_connect->where('wac.id_wkgps',$id_wkgps);
		$db_connect->order_by('wac.id','asc');
		$db_connect->group_by('wac.id_wkgps,wac.id_activity,wac.id_task_activity,wac.street,wac.group_street');
		$activiti_result = $db_connect->get()->result_array();
		$group_street = NULL;
		$is_first     = FALSE;
		$count        = count($activiti_result);
		$limit_time   = 3;
		for ($i=0; $i < $count; $i++) {
			$duration = strtotime($activiti_result[$i]['endtime']) - strtotime($activiti_result[$i]['starttime']);
			if($duration < $limit_time){#$activiti_result[$i]['duration']
				if($i < ($count - 1)){
					$check_duration = FALSE;
					for ($j=($i + 1); $j < $count; $j++) {
						$_duration = strtotime($activiti_result[$j]['endtime']) - strtotime($activiti_result[$j]['starttime']);
						if ($_duration > ($limit_time - 1) ) {
							for ($k = $i; $k < $j; $k++) {
								$activiti_result[$k]['street_old']       = $activiti_result[$k]['street'];
								$activiti_result[$k]['id_activity']      = $activiti_result[$j]['id_activity'];
								$activiti_result[$k]['id_task_activity'] = $activiti_result[$j]['id_task_activity'];
								$activiti_result[$k]['street']           = $activiti_result[$j]['street'];
								$activiti_result[$k]['group_street']     = $activiti_result[$j]['group_street'];
								$activiti_result[$k]['flag']             = 'x';
							}
							$check_duration = TRUE;
							$i = $j;
							break;
						}
					}
					if($check_duration != TRUE){
						if($i > 0){
							for ($k = $i; $k < $count; $k++) {
								$activiti_result[$k]['street_old']       = $activiti_result[$k]['street'];
								$activiti_result[$k]['id_activity']      = $activiti_result[$i - 1]['id_activity'];
								$activiti_result[$k]['id_task_activity'] = $activiti_result[$i - 1]['id_task_activity'];
								$activiti_result[$k]['street']           = $activiti_result[$i - 1]['street'];
								$activiti_result[$k]['group_street']     = $activiti_result[$i - 1]['group_street'];
								$activiti_result[$k]['flag']             = 'x';
							}
						} else {
							$db_connect->where('id_wkgps', $id_wkgps);
							$flag = $db_connect->update(TBL_WORKER_ACTIVITY, array('deleted_at' => date('Y-m-d H:i:s')));
						}
					}
				} else {
					$activiti_result[$i]['street_old']       = $activiti_result[$i]['street'];
					$activiti_result[$i]['id_activity']      = $activiti_result[$i - 1]['id_activity'];
					$activiti_result[$i]['id_task_activity'] = $activiti_result[$i - 1]['id_task_activity'];
					$activiti_result[$i]['street']           = $activiti_result[$i - 1]['street'];
					$activiti_result[$i]['group_street']     = $activiti_result[$i - 1]['group_street'];
					$activiti_result[$i]['flag']             = 'x';
				}
			}
		}

		for($i = 0; $i < count($activiti_result); $i++) {
			if($i > 0){
				if ( (strtolower(trim($activiti_result[$i]['street'])) == strtolower(trim($activiti_result[$i - 1]['street']))) OR (trim($activiti_result[$i]['street']) == '') ){
					$activiti_result[$i]['group_street'] = $activiti_result[$i - 1]['group_street'];
					$activiti_result[$i]['flag']         = 'x';
				}
			}
		}

		$update_array = array();
		foreach ($activiti_result as $key => $value) {
			$flag = isset($value['flag']) ? TRUE : FALSE;
			if($flag){
				$id_group           = $value['id_group'];
				$id_array           = explode(',', $id_group);
				$street_name_old    = $value['street_name_old'];
				$street_array       = explode(',', $street_name_old);
				foreach ($id_array as $id_key => $id_value) {
					if (intval($id_value) > 0) {
						$_street_old    = isset($value['street_old']) ? $value['street_old'] : $value['street'];
						$update_array[] = array(
							'id'               => $id_value,
							'id_activity'      => $value['id_activity'],
							'id_task_activity' => $value['id_task_activity'],
							'street'           => $value['street'],
							'street_old'       => (isset($street_array[$id_key]) ? $street_array[$id_key] : '').'-'.$_street_old,
							'group_street'     => $value['group_street'],
							'updated_at'       => date('Y-m-d H:i:s')
						);
						write_log("ID: {$id_value} - Street: {$value['street']}", 'street_clean_unnecessary');
					} else {
						write_log("ID: {$id_value} - ID invalid: {$value['street']}", 'street_clean_unnecessary');
					}
				}
			}
		}

		if(is_array($update_array) AND !empty($update_array)){
			$flag = $db_connect->update_batch(TBL_WORKER_ACTIVITY,$update_array,'id');
			if ($flag === FALSE) {
				write_log("FAIL", 'street_clean_unnecessary');
				return false;
			} else {
				write_log("OK", 'street_clean_unnecessary');
				$job = array(
					'id_city'  => $id_city,
					'id_wkgps' => $id_wkgps
				);
				push_job(QUE_CLEAN_POINT, json_encode($job));
				write_log('Add queue ' . QUE_CLEAN_POINT . ' success data : ' .json_encode($job), 'street_clean_unnecessary');
				return true;
			}
		}else {
			write_log("OK - Data update invalid", 'street_clean_unnecessary');
			return false;
		}
	}

	function run() {
		// start - extract_tour_data
		sleep(3);

		// Set limit timeout : 600 seconds = 10 minutes
		ini_set('max_execution_time', 600);

		// Libs
		require_once(BASEPATH.'libraries/pheanstalk/pheanstalk_init.php');

		// Get jobs
		$pheanstalk        = new Pheanstalk(getenv('QUEUE_PHEANSTALK_HOST'));
		$pheanstalk_status = $pheanstalk->getConnection()->isServiceListening(); // true or false
		if ($pheanstalk_status === false) {
			$msg = 'Pheanstalk connection error _ ' . json_encode($pheanstalk);
			write_log($msg, 'pheanstalk');
			die($msg);
		}

		$starttime = time();
		while(1) {
			write_log("Start get street name", 'street');

			# Pheanstalk jobs
			$job        = $pheanstalk->watchOnly(QUE_RESOLVE_STREET_NAME)->ignore('default')->reserve();
			$job_data   = json_decode($job->getData(), true);
			$pheanstalk->delete($job);
			write_log('Job deleted _ Job data: ' . json_encode($job_data), 'street');

			$id_city    = isset($job_data['id_city'])  ? $job_data['id_city']  : false;
			$id_wkgps   = isset($job_data['id_wkgps']) ? $job_data['id_wkgps'] : false;
			$id_cron    = isset($job_data['id_cron'])  ? $job_data['id_cron']  : false;
			write_log("id_city: {$id_city}, id_wkgps: {$id_wkgps}, id_cron: {$id_cron}", 'street');

			if (($id_city !== false) AND ($id_wkgps !== false)) {
				// connect
				$db_connect = connect_db(null, $id_city);
				write_log("Init connection to city {$id_city} database : " . (($db_connect === false) ? 'FAIL' : 'OK'), 'street');

				if ($db_connect === false) {
					continue;
				}

				$db_connect->where('street', '');
				$db_connect->where('id_wkgps', $id_wkgps);
				$result = $db_connect->get(TBL_WORKER_ACTIVITY)->result_array();
				if (is_array($result) AND !empty($result)) {
					$total      = count($result);
					$counter    = 0;
					$batch_data = array();
					$_index     = 0;
					foreach ($result as $key => $value) {
						$counter++;
						$_index++;
						$street_name = $this->resolve_street_name($value['latitude'], $value['longtitude']);
						$batch_data[] = array(
							'id'                 => $value['id'],
							'street'             => $street_name,
							'street_resolved_at' => date('Y-m-d H:i:s'),
							'street_resolved_by' => $this->_service_provider_selected,
							'updated_at'         => date('Y-m-d H:i:s')
						);

						# Logs
						write_log("{$counter} / {$total} : {$value['latitude']} _ {$value['longtitude']} _ {$street_name}", 'street');

						# update
						if($_index == 500){
							if (is_array($batch_data) AND !empty($batch_data)) {
								$db_flag = $db_connect->update_batch(TBL_WORKER_ACTIVITY, $batch_data, 'id');
								if ($db_flag === false) {
									$msg = "Batch update 500 record FAIL";
									write_log("{$msg} _ City: {$id_city} _ id_wkgps: {$id_wkgps} _ Query: ".$db_connect->last_query(), 'street');
									echo "{$msg}\n" . PHP_EOL;
								} else {
									$msg = "Batch update 500 record OK";
									write_log($msg, 'street');
									echo "{$msg}\n" . PHP_EOL;
								}
								$batch_data = array();
								$_index = 0;
							}
						}
					}

					if (is_array($batch_data) AND !empty($batch_data)) {
						$db_flag = $db_connect->update_batch(TBL_WORKER_ACTIVITY, $batch_data, 'id');
						if ($db_flag === false) {
							$msg = "Batch update worker_activity FAIL";
							write_log("{$msg} _ City: {$id_city} _ id_wkgps: {$id_wkgps} _ Query: ".$db_connect->last_query(), 'street');
							echo "{$msg} \n" . PHP_EOL;
						} else {
							$msg = "Batch update worker_activity OK";
							write_log($msg, 'street');
							echo "{$msg}\n" . PHP_EOL;
						}
					}
				} else {
					write_log('worker_activity is empty', 'street');
				}

				# group_street
				$flag = $this->group_street($id_city, $id_wkgps);
				write_log('---> group_street' . (($flag === false) ? 'FAIL' : 'OK'), 'street');

				# clean_unnecessary_streets_v2
				$flag = $this->clean_unnecessary_streets_v2($id_city, $id_wkgps);
				write_log('---> clean_unnecessary_streets_v2' . (($flag === false) ? 'FAIL' : 'OK'), 'street');

				# Update worker_gps status
				$db_connect->where('id', $id_wkgps);
				$db_flag = $db_connect->update(TBL_WORKER_GPS, array('status' => CRON_STATUS_SUCCESS));
				if ($db_flag === false) {
					write_log("Update worker_gps status FAIL _ city: {$id_city} _ id_wkgps: {$id_wkgps}", 'street');
				}

				# update cron status
				$db_flag = $this->db->where('id', $id_cron)->update(TBL_CRON, array('status' => CRON_STATUS_SUCCESS));
				if ($db_flag === false) {
					write_log("Update cron status FAIL _ city: {$id_city} _ id_cron: {$id_cron} _ Query : " . $this->db->last_query(), 'street');
				} else {
					# delete data
					# $id_city,$id_wkgps,$id_cron
					$extract_detail = array();
					$this->db->where('id_city'  , $id_city);
					$this->db->where('id_wk_gps', $id_wkgps);
					$this->db->where('id_cron'  , $id_cron);
					$extract_detail = $this->db->get(TBL_EXTRACT_TOURS)->row_array();
					$extract_tour_data = FALSE;
					if(is_array($extract_detail) AND !empty($extract_detail)){
						$extract_tour_data = isset($extract_detail['data']) ? $extract_detail['data'] : FALSE;

						# delete
						$this->db->where('id_city'   , $id_city);
						$this->db->where('id_wk_gps' , $id_wkgps);
						$this->db->where('id_cron'   , $id_cron);
						$db_flag = $this->db->delete(TBL_EXTRACT_TOURS);
						if ($db_flag == false) {
							write_log('Update extract tour fail _ SQL : ' . $this->db->last_query(), 'street');
						}
					} else {
						write_log('Tour empty data : ' . json_encode(array('id_city'=>$id_city,'id_wk_gps'=>$id_wkgps,'id_cron'=>$id_cron)), 'street_add_push_job');
					}

					if($extract_tour_data != FALSE){
						$this->db->where('id_city', $id_city);
						$this->db->where('id_cron', $id_cron);
						$this->db->where('data'   , $extract_tour_data);
						$this->db->delete(TBL_CRON_PUS_JOBS);
					}
				}
				# Close db connection
				$db_connect->close();
			}

			sleep(2);
		}
	}
}

// Init and run worker
$worker = new worker();
$worker->run();
