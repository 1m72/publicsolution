<?php
class Cron {
    function __construct() {
        // Init
        require('db.php');

        // Helpers
        require_once('helpers/common_helper.php');
    }

    /**
     * Generate NFC email
     * @return [type] [description]
     * @author Chien Tran <tran.duc.chien@kloon.vn>
     */
    function generate() {
        $db_flag       = $this->db->where('is_used', 0)->update(TBL_NFC, array('id_city' => 0));
        $nfc_available = $this->db->select('id')->where('is_used', 0)->count_all_results(TBL_NFC);
        $nfc_limit     = 5000;
        if ($nfc_available < $nfc_limit) {
            $nfc_required = $nfc_limit;
            $nfc_lastest  = $this->db->select('MAX(`id`) as max_id', false)->get(TBL_NFC)->row();
            $nfc_max_id   = isset($nfc_lastest->max_id) ? $nfc_lastest->max_id : 1;
            $data         = array();
            for ($i=1; $i <= $nfc_required; $i++) {
                $tmp = $nfc_max_id + $i;
                $data[] = array(
                    'id'         => $tmp,
                    'id_city'    => 0,
                    'nfc_code'   => md5($tmp),
                    'is_used'    => 0,
                    'created_at' => date('Y-m-d H:i:s')
                );
            }
            $db_flag = $this->db->insert_batch(TBL_NFC, $data);
            $msg = ($db_flag !== false) ? "{$nfc_required} nfc_code has been generate successfuly." : "";
        } else {
            $msg = "{$nfc_available} is available. No need generate more nfc_code.";
        }
        write_log($msg, 'cron_nfc');
        echo "$msg\n";
    }
}

// Init and run
$cron = new Cron();
$cron->generate();
