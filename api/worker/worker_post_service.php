<?php
class worker {
	function __construct() {
		// Init
		require('db.php');

		// Helpers
		require_once('helpers/common_helper.php');
	}

	function _save_to_files($file_name, $data) {
		global $config;
		$path = get_server_path() . $config['upload_dir'] . 'service/' . date('Y/m/d') . '/';
		if (!is_dir($path)) {
			@umask(0000);
			@mkdir ($path, 0755, TRUE);
		}
		$file_path = $path.$file_name;

		include_once('helpers/file_helper.php');

		if (!write_file($file_path, $data)) {
			write_log('Save file fail _ Path : '. $file_path, 'post_service');
			return false;
		}
		return $file_path;
	}

	function get_file_path($file_name = '', $ontime) {
		global $config;
		$path      = get_server_path() . $config['upload_dir'] . 'service/' . date('Y/m/d', strtotime($ontime)) . '/';
		$file_path = $path.$file_name;
		return $file_path;
	}

	function _post($url, $dir_path, $data, $_worker, $_module, $_ontime, $id_service, $extra = array()){
		$return   = array();
		$eol      = "\r\n";
		$boundary = 'AaB03x';
		$index    = 0;
		foreach ($data as $key => $val) {
			print_r($val).$eol;
			$current_time       = time();
			$index++;
			$_file_name_content = date('YmdHis',strtotime($_ontime)) . $index .'$1$.msg';
			write_log('File .msg : ' . $_file_name_content, 'post_service');
			$_name_file = '';
			$_text_comment = '';
			foreach ($val['data'] as $_key => $_val) {
				if($_val['type'] != INFO_MESSAGE){
					$file_path = $_val['path'] . $_val['data'];
					if (file_exists($file_path) AND is_readable($file_path)) {
						switch ($_val['type']) {
							case INFO_IMAGE:
								$_name_file .= urlencode($id_service . '-' . $_val['id'] . '.jpg'). ';';
								break;

							case INFO_VOICE:
								$_name_file .= urlencode($id_service . '-' . $_val['id'] . '.mp3'). ';';
								break;

							case INFO_VIDEO:
								$_name_file .= urlencode($id_service . '-' . $_val['id'] . '.mp4'). ';';
								break;

							default:
								break;
						}
					}
				} else {
					$_text_comment .= $_val['data'] . ' ';
				}
			}
			$_text_comment = rtrim($_text_comment, ' ');
			$body = '';
			$body = '--' . $boundary . $eol;
			$body .= 'Content-Disposition: form-data; name="datei"; filename="' . $_file_name_content . '"' . $eol; # file content
			$body .= 'Content-Transfer-Encoding: binary' . $eol;
			$body .= 'Content-Type: application/octet-stream' . $eol . $eol;

			$body .= 'PS.Info: ' . convert_ascii_code($val['street']) . $eol;
			$body .= 'PSType=PS.Info' . $eol;
			$body .= 'Formular=AUFTRAG.HTM' . $eol . $eol;
			$body .= 'ps_auftraggeber=' . convert_ascii_code($_module) . $eol;
			$body .= 'ps_sachbearbeiter=' . convert_ascii_code($_worker) . $eol;
			$body .= 'ps_gihhaus=' . $eol;
			$body .= 'ps_sendweg=EMAIL' . $eol;
			$body .= 'ps_email=' . $eol;
			$body .= 'ps_gps=' . $val['gps'] . $eol;
			$body .= 'ps_giaufext=' . convert_ascii_code($val['street']) . $eol;
			$body .= 'ps_zieldatum=' . $eol;
			$body .= 'ps_gimemo=' . convert_ascii_code($_text_comment) . $eol;
			$body .= 'ps_belegex=' . $id_service . $eol; # id ????
			$body .= 'ps_datum=' . date('d.m.Y',strtotime($_ontime)) . $eol;
			$body .= 'ps_anlagen=' . $_name_file . $eol;
			$body .= 'ps_giaugr=' . $eol;
			$body .= 'ps_giaufisn=' . $eol;
			$body .= 'ps_gimatch=' . convert_ascii_code($val['street']) . $eol;
			$body .= '--' . $boundary . $eol;
			$body .= 'Content-Disposition: form-data; name="target"' . $eol . $eol;

			$body .= 'ab/' . $dir_path . '/an/' . $eol;
			$body .= '--' . $boundary . $eol;
			$body .= 'Content-Disposition: form-data; name="Kunde"' . $eol . $eol;

			$body .= $dir_path . $eol;
			$body .= '--' . $boundary . '--' . $eol . $eol;

			// Log .msg content
			write_log('content .msg : ' . $body, 'post_service');

			// Save .msg file
			// $path_msg_file      = date('YmdHis',strtotime($_ontime)) . $index . '.txt';
			// $_file_name_content = date('YmdHis',strtotime($_ontime)) . $index . '$1$.msg';
			$log_file_path = $this->_save_to_files($_file_name_content, $body);

			$params = array(
				'http' => array(
					'method'          => 'POST',
					'header'          => 'Content-Type: multipart/form-data; boundary=AaB03x' . $eol,
					'content'         => $body
				)
			);
			$ctx             = stream_context_create($params);
			$response        = @file_get_contents($url, FILE_TEXT, $ctx);
			$response_header = isset($http_response_header) ? $http_response_header : false;
			$return[] = array(
				'requested_at'    => $current_time,
				'response'        => $response,
				'response_header' => $response_header,
				'path'            => $log_file_path,
				'id_city'         => isset($extra['id_city']) ? $extra['id_city'] : 0
			);
			$count = 1;
			foreach ($val['data'] as $_key => $_val) {
				if($_val['type'] != INFO_MESSAGE){
					$count++;
					$_file_name_content = date('YmdHis',strtotime($_ontime)) . $index .'$' . $count . '$.att';
					$body = '';
					$body = '--' . $boundary . $eol;
					$body .= 'Content-Disposition: form-data; name="datei"; filename="' . $_file_name_content . '"' . $eol; # file content
					$body .= 'Content-Transfer-Encoding: binary' . $eol;
					$body .= 'Content-Type: application/octet-stream' . $eol . $eol;

					$file_path = $_val['path'] . $_val['data'];
					if (file_exists($file_path) AND is_readable($file_path)) {
						$file_name = '';
						switch ($_val['type']) {
							case INFO_IMAGE:
								$file_name = $id_service . '-' . $_val['id'] . '.jpg';
								break;

							case INFO_VOICE:
								$file_name = $id_service . '-' . $_val['id'] . '.mp3';
								break;

							case INFO_VIDEO:
								$file_name = $id_service . '-' . $_val['id'] . '.mp4';
								break;

							default:
								break;
						}

						$body .= $file_name;
						$lenght_data = strlen($file_name);
						for ($i=80; $i > $lenght_data ; $i--) {
							$body .= ' ';
						}
						//-----------------------
						$handle = fopen($file_path, "rb");
						$contents = fread($handle, filesize($file_path));
						$body .= $contents . $eol;
						//-----------------------
						$body .= '--' . $boundary . $eol;
						$body .= 'Content-Disposition: form-data; name="target"' . $eol . $eol;

						$body .= 'ab/' . $dir_path . '/an/' . $eol;
						$body .= '--' . $boundary . $eol;
						$body .= 'Content-Disposition: form-data; name="Kunde"' . $eol . $eol;

						$body .= $dir_path . $eol;
						$body .= '--' . $boundary . '--' . $eol . $eol;

						// Log
						$log_file_path = $this->_save_to_files($_file_name_content, $body);

						$params = array(
							'http' => array(
								'method' => 'POST',
								'header' => 'Content-Type: multipart/form-data; boundary=AaB03x' . $eol,
								'content' => $body
							)
						);

						$ctx = stream_context_create($params);
						$response = @file_get_contents($url, FILE_TEXT, $ctx);
						$response_header = isset($http_response_header) ? $http_response_header : false;
						$return[] = array(
							'requested_at'    => $current_time,
							'response'        => $response,
							'response_header' => $response_header,
							'path'            => $log_file_path,
							'id_city'         => isset($extra['id_city']) ? $extra['id_city'] : 0
						);
					} else {
						write_log('File not exist : ' . $file_path, 'post_service');
					}
				}
			}
		}

		// Log to DB
		if (is_array($return) AND !empty($return)) {
			$id_city = false;
			$data    = array();
			foreach ($return as $key => $value) {
				$data[] = array(
					'uri'            => $url,
					'request_data'   => isset($value['path']) ? str_replace(get_server_path(), '', $value['path']) : '',
					'request_extra'  => '',
					'request_at'     => isset($value['requested_at']) ? date('Y-m-d H:i:s', $value['requested_at']) : '',
					'response_data'  => isset($value['response']) ? $value['response'] : '',
					'response_extra' => json_encode(
						array(
							'response_header' => $value['response_header']
						)
					),
					'created_at'     => date('Y-m-d H:i:s'),
					// 'status'         => '',
					// 'retry'          => '',
				);

				if ( ($id_city === false) AND (isset($value['id_city'])) AND (intval($value['id_city']) > 0) ) {
					$id_city = intval($value['id_city']);
				}
			}

			// Save to DB
			if ($id_city !== false) {
				// db connect
				$db_connect = connect_db(null, $id_city);

				if ($db_connect !== false) {
					// Save to db
					$db_flag = $db_connect->insert_batch(TBL_POST_SERVICE_LOG, $data);

					if ($db_flag === false) {
						write_log('Insert batch logs : FAIL '. $db_connect->last_query(), 'post_service');
					} else {
						write_log('Insert batch logs : OK ', 'post_service');
					}
				} else {
					write_log('Connect DB : FAIL _ id_city: ' . $id_city, 'post_service');
				}

			}
		}

		return $return;
	}

	function run() {
		sleep(5);

		// Set limit timeout : 300 seconds = 5 minutes
		ini_set('max_execution_time', 300);

		// Pheanstalk lib
		require_once(APPPATH.'libraries/pheanstalk/pheanstalk_init.php');

		// Pheanstalk init
		$pheanstalk = new Pheanstalk(getenv('QUEUE_PHEANSTALK_HOST'));

		// Get jobs
		$pheanstalk_status = $pheanstalk->getConnection()->isServiceListening(); // true or false
		if ($pheanstalk_status === false) {
			$msg = 'Pheanstalk connection error _ ' . json_encode($pheanstalk);
			write_log($msg, 'pheanstalk');
			die($msg);
		}

		// Exit after x minute : fix memory leak
		$start = time();
		while(1) {
			write_log("Start post service", 'post_service');
			$job        = $pheanstalk->watchOnly(QUE_SERVICE_POST)->ignore('default')->reserve();
			$job_data   = json_decode($job->getData(), true);
			$pheanstalk->delete($job);
			$this->db->reconnect();

			if(!isset($job_data) || empty($job_data)){
				write_log('Data invalid ', 'post_service');
				return false;
			}

			$id_city = isset($job_data['id_city']) ? $job_data['id_city'] : '';
			$id_gps  = isset($job_data['id_gps']) ? $job_data['id_gps'] : '';
			$id_service = $id_city . '-' . $id_gps;
			write_log('data - id_city : ' . $id_city . ' & id_gps : ' . $id_gps, 'post_service');
			# get city
			$this->db->where('deleted_at', NULL);
			$this->db->where('id', $id_city);
			$city_detail = $this->db->get(TBL_CITY)->row_array();
			if(!is_array($city_detail) OR empty($city_detail)){
				write_log('City detail empty Id_city: ' . $id_city . ' & id_gps : ' . $id_gps, 'post_service');
				return;
			}
			$infor_service = isset($city_detail['post_service']) ? $city_detail['post_service'] : '';
			if(!isset($infor_service) OR empty($infor_service)){
				write_log('Information post service empty Id_city: ' . $id_city . ' & id_gps : ' . $id_gps, 'post_service');
				return;
			}
			$json_decode = json_decode($infor_service, true);
			$url_service = isset($json_decode['url']) ? $json_decode['url'] : '';
			$dir_path = isset($json_decode['dir_path']) ? $json_decode['dir_path'] : '';
			if( (!isset($url_service) OR empty($url_service)) OR (!isset($dir_path) OR empty($dir_path)) ){
				write_log('Url or dir path empty Id_city: ' . $id_city . ' & id_gps : ' . $id_gps, 'post_service');
				return;
			}
			write_log('Url : ' . $url_service . ' & path : ' . $dir_path, 'post_service');
			# connect
			$db_connect = connect_db(null, $id_city);

			# get infor worker, machin
			$gps_detail = array();
			$db_connect->where('id',$id_gps);
			$gps_detail = $db_connect->get(TBL_WORKER_GPS)->row_array();
			if(!is_array($gps_detail) OR empty($gps_detail)){
				echo "gps empty";
				return;
			}
			$id_worker = isset($gps_detail['id_worker']) ? $gps_detail['id_worker'] : '';
			$id_module = isset($gps_detail['app']) ? $gps_detail['app'] : '';
			$ontime    = isset($gps_detail['ontime']) ? $gps_detail['ontime'] : '';

			# get name module
			$module_name   = '';
			$module_detail = array();
			$db_connect->where('id',$id_module);
			$module_detail = $db_connect->get(TBL_MODULES)->row_array();
			if(is_array($module_detail) AND !empty($module_detail)){
				$module_name = $module_detail['title'];
			}
			# get name worker
			$worker_name = '';
			$worker_detail = array();
			$db_connect->where('id',$id_worker);
			$worker_detail = $db_connect->get(TBL_WORKER)->row_array();
			if(is_array($worker_detail) AND !empty($worker_detail)){
				$worker_name = $worker_detail['first_name'] . ' ' . $worker_detail['last_name'];
			}
			# get information
			$information_gps = array();
			$information     = array();
			$db_connect->where('id_worker_gps',$id_gps);
			$information = $db_connect->get(TBL_WORKER_INFORMATION)->result_array();
			foreach ($information as $key => $val) {
				$detail_lat  = $val['lat'];
				$detail_lon  = $val['lon'];
				$detail_type = $val['type'];
				$detail_data = $val['data'];
				$detail_time = $val['time'];
				if(!isset($information_gps[$detail_lat . '-' . $detail_lon]['gps']) OR empty($information_gps[$detail_lat . '-' . $detail_lon]['gps'])){
					$information_gps[$detail_lat . '-' . $detail_lon]['gps'] = $detail_lat . ' ' . $detail_lon;
				}
				if(!isset($information_gps[$detail_lat . '-' . $detail_lon]['street']) OR empty($information_gps[$detail_lat . '-' . $detail_lon]['street'])){
					$street = resolve_street_name($detail_lat, $detail_lon);
					$information_gps[$detail_lat . '-' . $detail_lon]['street'] = $street;
				}
				$information_gps[$detail_lat . '-' . $detail_lon]['data'][] = array(
					'id'   => $val['id'],
					'time' => $detail_time,
					'type' => $detail_type,
					'data' => $detail_data,
					'path' => get_server_path() . '/uploads/' . $id_city . '/' . date('Y/m/d', strtotime($ontime)) . '/' . $id_worker . '/'
				);
			}
			write_log('data information : ' . json_encode($information_gps), 'post_service');
			$url    = $url_service;//'http://www.auftragsbuch.info/ps_file.php';
			$return = $this->_post($url, $dir_path, $information_gps, $worker_name, $module_name, $ontime, $id_service, array('id_city' => $id_city));
			write_log('id_city: '. $id_city . ' _ Data: ' . json_encode($return), 'post_service');
		}
	}

	function run_2() {
		require_once(APPPATH.'config/config_global.php');
		$id_city = 6;
		$id_gps  = 704;
		$id_service = $id_city . '-' . $id_gps;

		write_log('data - id_city : ' . $id_city . ' & id_gps : ' . $id_gps, 'post_service');
		# get city
		$this->db->where('deleted_at', NULL);
		$this->db->where('id', $id_city);
		$city_detail = $this->db->get(TBL_CITY)->row_array();
		write_log('data city detail : ' . json_encode($city_detail), 'post_service');

		if(!is_array($city_detail) OR empty($city_detail)){
			write_log('City detail empty Id_city: ' . $id_city . ' & id_gps : ' . $id_gps, 'post_service');
			return;
		}
		$infor_service = isset($city_detail['post_service']) ? $city_detail['post_service'] : '';
		if(!isset($infor_service) OR empty($infor_service)){
			write_log('Information post service empty Id_city: ' . $id_city . ' & id_gps : ' . $id_gps, 'post_service');
			return;
		}
		$json_decode = json_decode($infor_service, true);
		$url_service = isset($json_decode['url']) ? $json_decode['url'] : '';
		$dir_path = isset($json_decode['dir_path']) ? $json_decode['dir_path'] : '';
		if( (!isset($url_service) OR empty($url_service)) OR (!isset($dir_path) OR empty($dir_path)) ){
			write_log('Url or dir path empty Id_city: ' . $id_city . ' & id_gps : ' . $id_gps, 'post_service');
			return;
		}

		# connect
		$db_connect = connect_db(null, $id_city);
		# get infor worker, machin
		$gps_detail = array();
		$db_connect->where('id',$id_gps);
		$gps_detail = $db_connect->get(TBL_WORKER_GPS)->row_array();
		if(!is_array($gps_detail) OR empty($gps_detail)){
			echo "gps empty";
			return;
		}
		$id_worker = isset($gps_detail['id_worker']) ? $gps_detail['id_worker'] : '';
		$id_module = isset($gps_detail['app']) ? $gps_detail['app'] : '';
		$ontime    = isset($gps_detail['ontime']) ? $gps_detail['ontime'] : '';

		# get name module
		$module_name   = '';
		$module_detail = array();
		$db_connect->where('id',$id_module);
		$module_detail = $db_connect->get(TBL_MODULES)->row_array();
		if(is_array($module_detail) AND !empty($module_detail)){
			$module_name = $module_detail['title'];
		}
		# get name worker
		$worker_name = '';
		$worker_detail = array();
		$db_connect->where('id',$id_worker);
		$worker_detail = $db_connect->get(TBL_WORKER)->row_array();
		if(is_array($worker_detail) AND !empty($worker_detail)){
			$worker_name = $worker_detail['first_name'] . ' ' . $worker_detail['last_name'];
		}
		# get information
		$information_gps = array();
		$information     = array();
		$db_connect->where('id_worker_gps',$id_gps);
		$information = $db_connect->get(TBL_WORKER_INFORMATION)->result_array();
		foreach ($information as $key => $val) {
			$detail_lat  = $val['lat'];
			$detail_lon  = $val['lon'];
			$detail_type = $val['type'];
			$detail_data = $val['data'];
			$detail_time = $val['time'];
			if(!isset($information_gps[$detail_lat . '-' . $detail_lon]['gps']) OR empty($information_gps[$detail_lat . '-' . $detail_lon]['gps'])){
				$information_gps[$detail_lat . '-' . $detail_lon]['gps'] = $detail_lat . ' ' . $detail_lon;
			}
			if(!isset($information_gps[$detail_lat . '-' . $detail_lon]['street']) OR empty($information_gps[$detail_lat . '-' . $detail_lon]['street'])){
				//
				$street = resolve_street_name($detail_lat, $detail_lon);
				$information_gps[$detail_lat . '-' . $detail_lon]['street'] = $street;
			}
			$information_gps[$detail_lat . '-' . $detail_lon]['data'][] = array(
				'id'   => $val['id'],
				'time' => $detail_time,
				'type' => $detail_type,
				'data' => $detail_data,
				'path' => get_server_path() . '/uploads/' . $id_city . '/' . date('Y/m/d', strtotime($ontime)) . '/' . $id_worker . '/'
			);
		}
		write_log('data information : ' . json_encode($information_gps), 'post_service');
		$url    = $url_service;//'http://www.auftragsbuch.info/ps_file.php';
		$return = $this->_post($url, $dir_path, $information_gps, $worker_name, $module_name, $ontime, $id_service);
		write_log('id_city: '. $id_city . ' _ Data: ' . json_encode($return), 'post_service');
	}
}

$worker = new worker();
$worker->run();